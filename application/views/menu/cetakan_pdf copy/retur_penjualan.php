<?php
function rupiah($angka)
{

	$hasil_rupiah = number_format($angka);
	// $hasil_rupiah = "Rp. " . number_format($angka);
	return $hasil_rupiah;
}

function tanggal_indo($tanggal)
{
	$bulan = [
		1 => 'Januari',
		'Februari',
		'Maret',
		'April',
		'Mei',
		'Juni',
		'Juli',
		'Agustus',
		'September',
		'Oktober',
		'November',
		'Desember'
	];

	$pecahkan = explode('-', $tanggal);

	return $pecahkan[0] . ' ' . $bulan[(int)$pecahkan[1]] . ' ' . $pecahkan[2];
}
?>

<style>
	* {
		font-family: 'Courier New', Courier, monospace;
		font-weight: bold;
	}

	.cf:before,
	.cf:after {
		content: " ";
		/* 1 */
		display: table;
		/* 2 */
	}

	.cf:after {
		clear: both;
	}

	/**
 * For IE 6/7 only
 * Include this rule to trigger hasLayout and contain floats.
 */
	.cf {
		*zoom: 1;
	}

	table {
		font-family: Verdana, Arial, Helvetica, sans-serif;
		border-collapse: collapse;
		width: 100%;
	}

	th {
		border: 1px solid #333;
		text-align: left;
		padding: 8px;
	}

	/* tr:nth-child(even) {
		background-color: #dddddd;
	} */

	div {
		font-size: 12;
		/* border: 1px solid #68478D; */
	}
</style>
<title>Retur Penjualan - <?= $rp->nomor ?></title>
<div class="row cf">
	<div class="col" style="float: left; width: 50%; text-align: left;">
		<div style="font-size: 16px;">
			<b>PT. BEVOS PRIMA CENTER</b>
		</div>
	</div>
</div>

<div class="row cf">
	<div class="col" style="float: left; width: 50%; font-weight: bold; text-align: left;">
		<div style="font-size: 16px;">
			<b>RETUR PENJUALAN</b>
		</div>
	</div>
	<div class="col" style="float: right; width: 50%; text-align: right;">
		<div style="font-size: 13px; text-transform: uppercase;">
			<?= $vrp->nama_toko ?>
		</div>
	</div>
</div>
<div class="row cf">
	<div class="col" style="float: right; width: 50%; text-align: right;">
		<div style="font-size: 12px; text-transform: uppercase;">
			<?= $vrp->alamat ?>
		</div>
	</div>
</div>
<div class="row cf" style="margin-bottom: 2px;">
	<div class="col cf" style="float: left; width: 50%; text-align: left;">
		<div style="font-size: 13px; width: 45%; float: left;">
			No. Retur
		</div>
		<div style="font-size: 13px; width: 55%; float: right;">
			: <?= $vrp->nomor ?>
		</div>
	</div>
	<div class="col" style="float: right; width: 50%; text-align: right;">
		<div style="font-size: 13px; text-transform: uppercase;">
			<?= $vrp->kota ?>
		</div>
	</div>

</div>

<div class="row cf" style="margin-bottom: 2px;">
	<div class="col cf" style="float: left; width: 50%; text-align: left;">
		<div style="font-size: 13px; width: 45%; float: left;">
			Tanggal
		</div>
		<div style="font-size: 13px; width: 55%; float: right;">
			: <?= tanggal_indo(date('d-n-y'), strtotime($vrp->tanggal)) ?>
		</div>
	</div>
	<div class="col" style="float: right; width: 50%; text-align: right;">
		<div style="font-size: 13px; text-transform: uppercase;">
			<?php if (!empty($vrp->notelp)) : ?>
				<?= $vrp->notelp ?>
			<?php elseif (!empty($vrp->nohp)) : ?>
				<?= $vrp->nohp ?>
			<?php endif; ?>
		</div>
	</div>
</div>

<table style="margin-bottom: 35px;" cellpadding="7" cellspacing="0">
	<thead>
		<tr>
			<th style="text-align: center; font-size: 12px;">No</th>
			<!-- <th style="text-align: center; font-size: 12px;">Banyaknya</th> -->
			<th style="text-align: center; font-size: 12px;">Nama Produk</th>
			<th style="text-align: center; font-size: 12px;">Qty</th>
			<th style="text-align: center; font-size: 12px;">Harga</th>
			<th style="text-align: center; font-size: 12px;">Disc 1</th>
			<th style="text-align: center; font-size: 12px;">Disc 2</th>
			<th style="text-align: center; font-size: 12px;">Total Harga</th>
		</tr>
	</thead>
	<tbody>
		<?php
		$no = 1;
		foreach ($vrpd as $val) : ?>
			<tr>
				<td style="font-size: 12px;"><?= $no++ ?></td>
				<td style="font-size: 12px;"><?= $val->namabarang ?></td>
				<td style="font-size: 12px;"><?= $val->qty ?>&nbsp;<?= $val->namasatuan ?></td>
				<td style="font-size: 12px; text-align: right;"><?= rupiah($val->harga) ?></td>
				<td style="font-size: 12px; text-align: center;"><?= $val->discbarang ?>%</td>
				<td style="font-size: 12px; text-align: center;"><?= $val->discpaket ?>%</td>
				<td style="font-size: 12px; text-align: right;"><?= rupiah($val->total) ?></td>
			</tr>
		<?php endforeach; ?>
	</tbody>
</table>
<hr>
<div style="float: left; font-size: 13px; width: 20%;">
	Keterangan :
</div>
<table border="0" cellspacing="0" cellpadding="0" style="margin-bottom: 15px;">
	<?php foreach ($vrpd as $val) : ?>
		<tr>
			<td style="width: 75%; text-align: right; font-size: 12px; font-weight: bold;">SUB TOTAL&nbsp;</td>
			<td style="width: 1%; font-size: 12px; font-weight: bold;"> : </td>
			<td style="text-align: right; font-size: 12px; width: 20%;"><?= rupiah($val->total) ?></td>
		</tr>
		<tr>
			<td style="width: 75%; text-align: right; font-size: 12px; font-weight: bold;">DISKON&nbsp;</td>
			<td style="width: 1%; font-size: 12px; font-weight: bold;"> : </td>
			<td style="text-align: right; font-size: 12px; width: 20%;"><?= rupiah($val->total - $val->dpp) ?></td>
		</tr>
		<tr>
			<td style="width: 75%; text-align: right; font-size: 12px; font-weight: bold;">DPP&nbsp;</td>
			<td style="width: 1%; font-size: 12px; font-weight: bold;"> : </td>
			<td style="text-align: right; font-size: 12px; width: 20%;"><?= rupiah($val->dpp) ?></td>
		</tr>
		<tr>
			<td style="width: 75%; text-align: right; font-size: 12px; font-weight: bold;">PPN&nbsp;</td>
			<td style="width: 1%; font-size: 12px; font-weight: bold;"> : </td>
			<td style="text-align: right; font-size: 12px; width: 20%;"><?= rupiah($val->ppn) ?></td>
		</tr>
		<tr>
			<td style="width: 75%; text-align: right; font-size: 12px; font-weight: bold;">TOTAL&nbsp;</td>
			<td style="width: 1%; font-size: 12px; font-weight: bold;"> : </td>
			<td style="text-align: right; font-size: 12px; width: 20%; font-weight: bold;"><?= rupiah($val->grandtotal) ?></td>
		</tr>
	<?php endforeach; ?>
</table>

</div>