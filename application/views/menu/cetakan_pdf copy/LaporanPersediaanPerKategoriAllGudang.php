<?php
function FormatRupiah($angka)
{

	$hasil_rupiah = number_format($angka);
	// $hasil_rupiah = "Rp. " . number_format($angka);
	return $hasil_rupiah;
}

function tanggal_indo($tanggal)
{
	$bulan = [
		1 => 'Januari',
		'Februari',
		'Maret',
		'April',
		'Mei',
		'Juni',
		'Juli',
		'Agustus',
		'September',
		'Oktober',
		'November',
		'Desember'
	];

	$pecahkan = explode('-', $tanggal);

	return $pecahkan[0] . ' ' . $bulan[(int)$pecahkan[1]] . ' ' . $pecahkan[2];
}
?>
<style>
	* {
		font-family: 'Courier New', Courier, monospace;
		/* font-weight: bold; */
	}

	.cf:before,
	.cf:after {
		content: " ";
		/* 1 */
		display: table;
		/* 2 */
	}

	.cf:after {
		clear: both;
	}

	/**
 * For IE 6/7 only
 * Include this rule to trigger hasLayout and contain floats.
 */
	.cf {
		*zoom: 1;
	}

	table {
		/* font-family: Verdana, Arial, Helvetica, sans-serif; */
		border-collapse: collapse;
		width: 100%;
	}

	th {
		border: 1px solid #333;
		text-align: left;
		padding: 8px;
	}

	div {
		font-size: 12;
		/* border: 1px solid #68478D; */
	}
</style>
<title>LAPORAN PERSEDIAN BARANG | <?= tanggal_indo(date('d-m-Y', strtotime($tglawal))) ?> - <?= tanggal_indo(date('d-m-Y', strtotime($tglakhir))) ?></title>

<body>
	<!-- Text halaman & text yang hanya dihalaman terakhir -->
	<script type="text/php">
		/// Halaman
        if (isset($pdf)) {
            $x = 440;
            $y = 77.4;
            $size = 8;
            $color = array(0,0,0);
            $text = "{PAGE_NUM} / {PAGE_COUNT}";
            $font = $fontMetrics->get_font("helvetica");
            $pdf->page_text($x, $y, $text, $font, $size, $color, 0.0, 0.0, 0.0);
        }
        /// End Halaman
        </script>
	<div class="row cf" style="margin-bottom: 2px;">
		<div class="col cf" style="float: left; width: 50%; text-align: left;">
			<div style="font-size: 13px; width: 20%; float: left;">
				GUDANG

			</div>
			<div style="font-size: 13px; width: 80%; float: right;">
				: SEMUA GUDANG
			</div>
		</div>
		<div class="col" style="float: right; width: 50%; text-align: right;">
			<div style="font-size: 13px; width: 45%; float: left;">
			</div>
			<div style="font-size: 13px; width: 55%; float: right;">
				<!-- Halaman
                : 1 / 1 -->
			</div>
		</div>
	</div>
	<div class="row cf" style="margin-bottom: 2px;">
		<div class="col cf" style="float: left; width: 50%; text-align: left;">
			<div style="font-size: 13px; width: 20%; float: left;">
				KATEGORI
			</div>
			<div style="font-size: 13px; width: 80%; float: right;">
				: <?php echo $merkawal->nama ?> <sup style="font-size: 10px;">s</sup>/<sub style="font-size: 10px;">d</sub> <?php echo $merkakhir->nama ?>
			</div>
		</div>
		<div class="col" style="float: right; width: 50%; text-align: right;">
			<div style="font-size: 13px; width: 45%; float: left;">
			</div>
			<div style="font-size: 13px; width: 55%; float: right;">
				Halaman
				:
			</div>
		</div>
		<br>
		<div class="row cf" style="margin-bottom: 2px;">
			<div class="col cf" style="float: left; width: 80%; text-align: left;">
				<div style="font-size: 13px; width: 12.5%; float: left;">
					PERIODE
				</div>
				<div style="font-size: 13px; width: 80%; float: left;">
					: <?= date('d-m-Y', strtotime($tglawal)) ?> <sup style="font-size: 10px;">s</sup>/<sub style="font-size: 10px;">d</sub> <?= date('d-m-Y', strtotime($tglakhir)) ?>
				</div>
			</div>
			<div class="col" style="float: right; width: 50%; text-align: right;">
				<div style="font-size: 13px; width: 45%; float: left;">
				</div>
				<div style="font-size: 13px; width: 55%; float: right;">
					Dicetak
					: <?= date('d-m-Y') ?>
				</div>
			</div>
		</div>
		<br>

		<table style="font-size: 10px; text-align:justify;">
			<thead>
				<tr>
					<th>Periode</th>
					<th>Barcode</th>
					<th>Nama Produk</th>
					<th>Qty</th>
					<th>Satuan</th>
					<th>Komisi</th>
					<th>Harga Jual</th>
					<th>Total</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($persediaan as $values) : ?>
				<tr>
					<td colspan="8" style="font-weight: bold; font-size: 14px;">KATEGORI : <?= $val->namamerk ?></td>
				</tr>
				<?php $kodemerk = $values->merk ?>
				<?php
				$grandtotal = 0;
				$sum = 0;
				foreach ($report as $val) :
				?>
					<?php if($val->merk == $kodemerk) : ?>
					<tr>
						<td style="text-align: center;"><?= $val->periode ?></td>
						<td style="text-align: center;"><?= $val->kodebarang ?></td>
						<td style="text-align: center;"><?= $val->namabarang ?></td>
						<td style="text-align: left;"><?= $val->qtyawal ?></td>
						<td style="text-align: right;"><?= $val->kodesatuan ?></td>
						<td style="text-align: right;"><?= $val->komisi ?>%</td>
						<td style="text-align: right;"><?= FormatRupiah($val->$hargajual) ?></td>
						<td style="text-align: right;"><?= FormatRupiah($val->$total) ?></td>
						<?php $grandtotal += $val->total++; ?>
					</tr>
					<tr>
						<td colspan="8">
							<hr>
						</td>
					</tr>
					<tr style="float: right; text-align: right; font-weight: bold; font-size: 12px;">
						<td colspan="7">Subtotal : </td>
						<?php
						if ($val->tanggal == $val->tanggal) {
							$sum = 0;
							$totalqty += $val->qty++;
						}
						?>
						<td colspan="1"><?= FormatRupiah($sum) ?></td>
						<td colspan="7">Total : </td>
						<?php
						if ($val->tanggal == $val->tanggal) {
							$sum = 0;
							$sum += $val->total++;
						}
						?>
						<td colspan="1"><?= FormatRupiah($sum) ?></td>
					</tr>
					<?php endif; ?>
				<?php endforeach; ?>
				<tr style="float: right; text-align: right; font-weight: bold; font-size: 12px;">
					<td colspan="7">Grand Total : </td>
					<td colspan="1"><?= FormatRupiah($grandtotal) ?></td>
				</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
</body>
