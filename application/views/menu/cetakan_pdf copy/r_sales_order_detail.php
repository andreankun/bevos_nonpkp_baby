<?php
function FormatRupiah($angka)
{

    $hasil_rupiah = number_format($angka);
    // $hasil_rupiah = "Rp. " . number_format($angka);
    return $hasil_rupiah;
}

function tanggal_indo($tanggal)
{
    $bulan = [
        1 => 'Januari',
        'Februari',
        'Maret',
        'April',
        'Mei',
        'Juni',
        'Juli',
        'Agustus',
        'September',
        'Oktober',
        'November',
        'Desember'
    ];

    $pecahkan = explode('-', $tanggal);

    return $pecahkan[0] . ' ' . $bulan[(int)$pecahkan[1]] . ' ' . $pecahkan[2];
}
?>
<style>
    * {
        font-family: 'Courier New', Courier, monospace;
        /* font-weight: bold; */
    }

    .cf:before,
    .cf:after {
        content: " ";
        /* 1 */
        display: table;
        /* 2 */
    }

    .cf:after {
        clear: both;
    }

    /**
 * For IE 6/7 only
 * Include this rule to trigger hasLayout and contain floats.
 */
    .cf {
        *zoom: 1;
    }

    table {
        /* font-family: Verdana, Arial, Helvetica, sans-serif; */
        border-collapse: collapse;
        width: 100%;
    }

    th {
        border: 1px solid #333;
        text-align: left;
        padding: 8px;
    }

    div {
        font-size: 12;
        /* border: 1px solid #68478D; */
    }
</style>
<title>LAPORAN SALES ORDER DETAIL | <?= tanggal_indo(date('d-m-Y', strtotime($tglawal))) ?> - <?= tanggal_indo(date('d-m-Y', strtotime($tglakhir))) ?></title>

<body>
    <!-- <script type="text/php">
        if (isset($pdf)) {
            $x = 515;
            $y = 80;
            $size = 8;
            $color = array(0,0,0);
            $text = "{PAGE_NUM} / {PAGE_COUNT}";
            $font = $fontMetrics->get_font("Courier");
            $pdf->page_text($x, $y, $text, $font, $size, $color, 0.0, 0.0, 0.0);
        }
    </script> -->
    <!-- <-?php foreach ($reportrow as $header) : ?> -->
    <h4 style="text-align: center;">LAPORAN SALES ORDER DETAIL</h4>
    <div class="row cf" style="margin-bottom: 2px;">
        <div class="col cf" style="float: left; width: 50%; text-align: left;">
            <div style="font-size: 13px; width: 20%; float: left;">
                CABANG
            </div>
            <div style="font-size: 13px; width: 80%; float: right;">
                : <?= $row->nama ?>
            </div>
        </div>
        <div class="col" style="float: right; width: 50%; text-align: right;">
            <div style="font-size: 13px; width: 45%; float: left;">
            </div>
            <div style="font-size: 13px; width: 55%; float: right; margin-right: 86px;">
                <!-- Halaman : -->
            </div>
        </div>
    </div>
    <div class="row cf" style="margin-bottom: 2px;">
        <div class="col cf" style="float: left; width: 50%; text-align: left;">
            <div style="font-size: 13px; width: 20%; float: left;">
                PERIODE
            </div>
            <div style="font-size: 13px; width: 80%; float: right;">
                : <?= date('d-m-Y', strtotime($tglawal)) ?> <sup style="font-size: 10px;">s</sup>/<sub style="font-size: 10px;">d</sub> <?= date('d-m-Y', strtotime($tglakhir)) ?>
            </div>
        </div>
        <div class="col" style="float: right; width: 50%; text-align: right;">
            <div style="font-size: 13px; width: 45%; float: left;">
            </div>
            <div style="font-size: 13px; width: 55%; float: right;">
                Dicetak
                : <?= date('d-m-Y') ?>
            </div>
        </div>
    </div>
    <br>
    <!-- <-?php endforeach; ?> -->

    <table style="font-size: 10px; text-align:justify;" width="100%">
        <thead>
            <tr>
                <th>No.</th>
                <th>Kode</th>
                <th>Nama Produk</th>
                <th>Qty</th>
                <th>Satuan</th>
                <th>Harga</th>
                <th>Diskon</th>
                <th>Total</th>
            </tr>
        </thead>
        <tbody>
            <?php if (!empty($report)) : ?>
                <?php foreach ($report as $val) : ?>
                    <?php $nomor = $val->nomor; ?>
                    <?php foreach ($reportrow as $vals) : ?>
                        <?php if ($vals->nomor == $nomor) : ?>
                            <tr>
                                <td colspan="3" style="font-weight: bold;">No. Order : <?= $vals->nomor ?></td>
                                <?php if ($vals->jenisjual == '1' || $vals->jenisjual == '3' || $vals->jenisjual == '4') : ?>
                                    <td colspan="3" style="font-weight: bold;">TUNAI/KREDIT : TUNAI</td>
                                <?php else : ?>
                                    <td colspan="3" style="font-weight: bold;">TUNAI/KREDIT : KREDIT</td>
                                <?php endif; ?>
                                <td colspan="2" style="font-weight: bold;">Pelanggan : <?= $vals->namapelanggan ?></td>
                            </tr>
                            <tr>
                                <td colspan="3" style="font-weight: bold;">Tanggal : <?= date('d-m-Y', strtotime($vals->tanggal)) ?></td>
                                <td colspan="3" style="font-weight: bold;">Sales : <?= $vals->namasalesman ?></td>
                                <td colspan="2" style="font-weight: bold;">Toko : <?= $vals->nama_toko ?></td>
                            </tr>
                            <tr>
                                <td colspan="3" style="font-weight: bold;">Gudang : <?= $vals->namagudang ?></td>
                                <td colspan="3" style="font-weight: bold;">User : <?= $vals->pemakai ?></td>
                                <td colspan="2" style="font-weight: bold;">Alamat : <?= $vals->pengiriman ?></td>
                            </tr>
                        <?php endif; ?>
                    <?php endforeach; ?>
                    <?php
                    $no = 1;
                    $total = 0;
                    $diskonlevel = 0;
                    $subtotal = 0;
                    $subtotals = 0;
                    $diskonpromo = 0;
                    $totaldiskonpromo = 0;
                    $diskoncash = 0;
                    $diskoncustomer = 0;
                    $potongan = 0;
                    $dpp = 0;
                    $ppn = 0;
                    $grandtotal = 0;
                    foreach ($detail as $row) :
                        $total = $row->harga * $row->qty;
                        $diskonlevel = $row->discbrgitem;
                        $subtotal = $total - $diskonlevel;
                        // $diskonpromo = $row->qty * $row->discpaketpromo;
                        $diskonpromo = $row->discpaketpromo;
                        $diskoncash = $row->nilaicash;
                        $diskoncustomer = $row->nilaicustomer;
                    ?>
                        <?php if ($row->nomor == $nomor) :
                        ?>
                            <tr>
                                <td><?= $no++; ?></td>
                                <td><?= $row->kodebarang; ?></td>
                                <td><?= $row->namabarang; ?></td>
                                <td style="text-align:right;"><?= $row->qty; ?></td>
                                <td style="text-align:left;"><?= $row->namasatuan; ?></td>
                                <td style="text-align:right;"><?= FormatRupiah($row->harga); ?></td>
                                <td style="text-align:right;"><?= FormatRupiah($diskonlevel); ?></td>
                                <td style="text-align:right;"><?= FormatRupiah($subtotal); ?></td>
                            </tr>
                            <?php
                            $subtotals = $subtotals + $subtotal;
                            $totaldiskonpromo = $totaldiskonpromo + $diskonpromo;
                            $potongan = round(($totaldiskonpromo + $diskoncash + $diskoncustomer) / 10) * 10;
                            $dpp = $subtotals - $potongan;
                            $ppn = ($dpp * $konfigurasi->ppn) / 100;
                            $grandtotal = $dpp + $ppn;
                            ?>
                        <?php endif; ?>
                    <?php endforeach; ?>
                    <tr>
                        <td colspan="8">
                            <hr>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="7" style="text-align: right;">Subtotal : </td>
                        <td colspan="1" style="text-align: right;"><?= FormatRupiah($subtotals); ?></td>
                    </tr>
                    <tr>
                        <td colspan="7" style="text-align: right;">Potongan : </td>
                        <td colspan="1" style="text-align: right;"><?= FormatRupiah($potongan); ?></td>
                    </tr>
                    <tr>
                        <td colspan="7" style="text-align: right;">DPP : </td>
                        <td colspan="1" style="text-align: right;"><?= FormatRupiah($dpp); ?></td>
                    </tr>
                    <tr>
                        <td colspan="7" style="text-align: right;">PPN : </td>
                        <td colspan="1" style="text-align: right;"><?= FormatRupiah($ppn); ?></td>
                    </tr>
                    <tr>
                        <td colspan="7" style="text-align: right;">Total : </td>
                        <td colspan="1" style="text-align: right;"><?= FormatRupiah($grandtotal); ?></td>
                    </tr>
                    <tr>
                        <td colspan="8">
                            <hr>
                        </td>
                    </tr>
                <?php endforeach; ?>
            <?php else : ?>
                <tr>
                    <td colspan="8"></td>
                </tr>
            <?php endif; ?>
        </tbody>
    </table>
</body>