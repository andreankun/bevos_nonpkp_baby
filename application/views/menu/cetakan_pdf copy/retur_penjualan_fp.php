<?php
function rupiah($angka)
{

	$hasil_rupiah = number_format($angka);
	// $hasil_rupiah = "Rp. " . number_format($angka);
	return $hasil_rupiah;
}

function tanggal_indo($tanggal)
{
	$bulan = [
		1 => 'Januari',
		'Februari',
		'Maret',
		'April',
		'Mei',
		'Juni',
		'Juli',
		'Agustus',
		'September',
		'Oktober',
		'November',
		'Desember'
	];

	$pecahkan = explode('-', $tanggal);

	return $pecahkan[0] . ' ' . $bulan[(int)$pecahkan[1]] . ' ' . $pecahkan[2];
}
?>

<style>
	* {
		font-family: 'Courier New', Courier, monospace;
		font-weight: bold;
	}

	.cf:before,
	.cf:after {
		content: " ";
		/* 1 */
		display: table;
		/* 2 */
	}

	.cf:after {
		clear: both;
	}

	/**
 * For IE 6/7 only
 * Include this rule to trigger hasLayout and contain floats.
 */
	.cf {
		*zoom: 1;
	}

	table {
		font-family: Verdana, Arial, Helvetica, sans-serif;
		border-collapse: collapse;
		width: 100%;
	}

	th {
		border: 1px solid #333;
		text-align: left;
		padding: 8px;
	}

	td {
		border: 1px solid #333;
		text-align: left;
		padding: 8px;
	}

	/* tr:nth-child(even) {
		background-color: #dddddd;
	} */

	div {
		font-size: 12;
		/* border: 1px solid #68478D; */
	}
</style>
<title>Nota Retur - <?= $rp->nomor ?></title>
<table style="margin-bottom: 35px;" cellpadding="7" cellspacing="0">
	<tbody>
		<tr>
			<td colspan="5">
				<div class="row cf" style="margin-bottom: 10px;">
					<div class="col" style="float: left; width: 60%; font-weight: bold; text-align: right;">
						<div style="font-size: 16px;">
							<b>RETUR PENJUALAN</b>
						</div>
					</div>
					<div class="col" style="float: right; width: 50%; text-align: right;">
						<div style="font-size: 13px; text-transform: uppercase;">
							REF: <?= $vrp->nomor ?>
						</div>
					</div>
				</div>
				<div class="row cf" style="margin-bottom: 10px;">
					<div class="col" style="float: left; width: 100%; font-weight: bold; text-align: center;">
						<div style="font-size: 13px;">
							<b>Nomor : <?= $vrp->nomor ?>
						</div>
					</div>
					<div class="col" style="float: right; width: 50%; text-align: right;">
						<div style="font-size: 13px; text-transform: uppercase;">
							<?= tanggal_indo(date('d-m-Y')) ?>
						</div>
					</div>
				</div>
			</td>
		</tr>
		<tr>
			<td colspan="5">
				<div class="row cf" style="margin-bottom: 10px;">
					<div class="col" style="float: left; width: 50%; font-weight: lighter; text-align: left;">
						<div style="font-size: 13px;">
							Atas Faktur Pajak Nomor: <?= $vrp->nofakturpajak ?>
						</div>
					</div>
					<div class="col" style="float: right; font-weight: lighter; width: 50%; text-align: right;">
						<div style="font-size: 13px; text-transform: uppercase;">
							INVOICE NO: <?= $vrp->noinvoice ?>
						</div>
					</div>
				</div>
				<div class="row cf" style="margin-bottom: 10px;">
					<div class="col" style="float: left; width: 50%; font-weight: lighter; text-align: left;">
						<div style="font-size: 13px;">
							Tanggal : <?= tanggal_indo(date('d-m-Y')) ?>
						</div>
					</div>
				</div>
				<div class="row cf" style="margin-bottom: 10px;">
					<div class="col" style="float: left; width: 50%; font-weight: lighter; text-align: left;">
						<div style="font-size: 13px;">
							PEMBELI
						</div>
					</div>
				</div>
				<div class="row cf" style="margin-bottom: 10px;">
					<div class="col" style="float: left; width: 50%; font-weight: lighter; text-align: left;">
						<div style="font-size: 13px;">
							Nama
						</div>
					</div>
					<div class="col" style="float: right; font-weight: lighter; width: 70%; text-align: left;">
						<div style="font-size: 13px;">
							: <?= $vrp->nama_toko ?>
						</div>
					</div>
				</div>
				<div class="row cf" style="margin-bottom: 10px;">
					<div class="col" style="float: left; width: 50%; font-weight: lighter; text-align: left;">
						<div style="font-size: 13px;">
							Alamat
						</div>
					</div>
					<div class="col" style="float: right; font-weight: lighter; width: 70%; text-align: left;">
						<div style="font-size: 13px;">
							: <?= $vrp->alamat ?>
						</div>
					</div>
				</div>
				<div class="row cf" style="margin-bottom: 10px;">
					<div class="col" style="float: left; width: 50%; font-weight: lighter; text-align: left;">
						<div style="font-size: 13px;">
							NPWP
						</div>
					</div>
					<div class="col" style="float: right; font-weight: lighter; width: 70%; text-align: left;">
						<div style="font-size: 13px;">
							: <?php
								if ($vrp->npwp != "") {
									echo $vrp->npwp;
								} else {
									echo '-';
								}
								?>
						</div>
					</div>
				</div>
			</td>
		</tr>
		<tr>
			<td colspan="5">
				<div class="row cf" style="margin-bottom: 10px;">
					<div class="col" style="float: left; width: 50%; font-weight: lighter; text-align: left;">
						<div style="font-size: 13px;">
							KEPADA PENJUAL
						</div>
					</div>
				</div>
				<div class="row cf" style="margin-bottom: 10px;">
					<div class="col" style="float: left; width: 50%; font-weight: lighter; text-align: left;">
						<div style="font-size: 13px;">
							Nama
						</div>
					</div>
					<div class="col" style="float: right; font-weight: lighter; width: 70%; text-align: left;">
						<div style="font-size: 13px;">
							: PT <?= $config->namaperusahaan ?>
						</div>
					</div>
				</div>
				<div class="row cf" style="margin-bottom: 10px;">
					<div class="col" style="float: left; width: 50%; font-weight: lighter; text-align: left;">
						<div style="font-size: 13px;">
							Alamat
						</div>
					</div>
					<div class="col" style="float: right; font-weight: lighter; width: 70%; text-align: left;">
						<div style="font-size: 13px;">
							: <?= $config->alamatperusahaan ?>
						</div>
					</div>
				</div>
				<div class="row cf" style="margin-bottom: 10px;">
					<div class="col" style="float: left; width: 50%; font-weight: lighter; text-align: left;">
						<div style="font-size: 13px;">
							NPWP
						</div>
					</div>
					<div class="col" style="float: right; font-weight: lighter; width: 70%; text-align: left;">
						<div style="font-size: 13px;">
							: <?= $config->npwp ?>
						</div>
					</div>
				</div>
			</td>
		</tr>
	</tbody>


</table>
<table style="margin-bottom: 35px;" cellpadding="" cellspacing="0">
	<thead>
		<tr>
			<th style="text-align: left; font-size: 12px;">No</th>
			<th style="text-align: left; font-size: 12px;">Macam dan Jenis Barang Kena Pajak (BKP)</th>
			<th style="text-align: left; font-size: 12px;">Kuantum</th>
			<th style="text-align: left; font-size: 12px;">Harga Satuan Menurut Faktur Pajak</th>
			<th style="text-align: left; font-size: 12px;">Harga Jual BKP</th>
		</tr>
	</thead>
	<tbody>
		<?php
		$no = 1;
		foreach ($vrpd as $val) : ?>
			<tr>
				<td style="font-size: 12px;"><?= $no++ ?></td>
				<td style="font-size: 12px;"><?= $val->namabarang ?></td>
				<td style="font-size: 12px;"><?= $val->qty ?></td>
				<td style="font-size: 12px; text-align: right;"><?= rupiah($val->harga) ?></td>
				<td style="font-size: 12px; text-align: right;"><?= rupiah($val->total) ?>
				</td>
			</tr>
		<?php endforeach; ?>
	</tbody>
</table>
<table style="margin-bottom: 35px;" cellpadding="7" cellspacing="0">
	<tr>
		<th style="font-size: 12px;">Jumlah Harga Jual BKP yang di kembalikan</th>
		<td style="font-size: 12px; text-align: right;" colspan="4">
			<?= rupiah($vrp->grandtotal) ?>
		</td>
	</tr>
	<tr>
		<th style="font-size: 12px;">Dikurangin Potongan Harga</th>
		<?php $sum = 0;
		foreach ($vrpd as $cell) : ?>
			<?php $sum += $cell->discbrgitem + $cell->discpaketitem ?>
		<?php endforeach ?>
		<td style="font-size: 12px; text-align: right;" colspan="4">
			<?= rupiah($sum) ?>
		</td>
	</tr>
	<tr>
		<th style="font-size: 12px;">PPN Yang diminta kembali</th>
		<td style="font-size: 12px; text-align: right;" colspan="4">
			<?= rupiah($vrp->ppn) ?>
		</td>
	</tr>
	<tr>
		<th style="font-size: 12px;">PPnBM yang diminta kembali</th>
		<td style="font-size: 12px;" colspan="4">

		</td>
	</tr>
	<tr>
		<td colspan="5">
			<div class="row cf" style="margin-bottom: 10px;">
				<div class="col" style="float: right; width: 30%; font-weight: bold; text-align: right;">
					<div style="font-size: 13px;">
						JAKARTA, <?= tanggal_indo(date('d-m-Y')) ?>
					</div>
				</div>
				<br>
				<br>
				<div class="col" style="float: right; width: 20%; text-align: center;">
					<div style="font-size: 13px; text-transform: uppercase;">
						PEMBELI
					</div>
				</div>
				<br>
				<br>
				<br>
				<div class="col" style="float: right; width: 30%; text-align: right;">
					<div style="font-size: 13px; text-transform: uppercase;">
						(.....................................)
					</div>
				</div>
				<br>
				<br>
			</div>
		</td>
	</tr>
	<tr>
		<td colspan="5" style="font-size: 12px;">
			Lembar ke-1 untuk PKP Penjual
			<br>
			Lembar Ke-2 untuk Pembeli
		</td>
	</tr>
</table>