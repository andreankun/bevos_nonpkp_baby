<?php
function FormatRupiah($angka)
{

	$hasil_rupiah = number_format($angka);
	// $hasil_rupiah = "Rp. " . number_format($angka);
	return $hasil_rupiah;
}

function tanggal_indo($tanggal)
{
	$bulan = [
		1 => 'Januari',
		'Februari',
		'Maret',
		'April',
		'Mei',
		'Juni',
		'Juli',
		'Agustus',
		'September',
		'Oktober',
		'November',
		'Desember'
	];

	$pecahkan = explode('-', $tanggal);

	return $pecahkan[0] . ' ' . $bulan[(int)$pecahkan[1]] . ' ' . $pecahkan[2];
}
?>
<style>
	* {
		font-family: 'Courier New', Courier, monospace;
		/* font-weight: bold; */
	}

	.cf:before,
	.cf:after {
		content: " ";
		/* 1 */
		display: table;
		/* 2 */
	}

	.cf:after {
		clear: both;
	}

	/**
 * For IE 6/7 only
 * Include this rule to trigger hasLayout and contain floats.
 */
	.cf {
		*zoom: 1;
	}

	table {
		/* font-family: Verdana, Arial, Helvetica, sans-serif; */
		border-collapse: collapse;
		width: 100%;
	}

	th {
		border: 1px solid #333;
		text-align: left;
		padding: 8px;
	}

	div {
		font-size: 12;
		/* border: 1px solid #68478D; */
	}
</style>
<title>LAPORAN DAFTAR JENIS PEMBAYARAN | <?= tanggal_indo(date('d-m-Y', strtotime($tglawal))) ?> - <?= tanggal_indo(date('d-m-Y', strtotime($tglakhir))) ?></title>

<body>
	<!-- Text halaman & text yang hanya dihalaman terakhir -->
	<script type="text/php">
		/// Halaman
        if (isset($pdf)) {
            $x = 440;
            $y = 77.4;
            $size = 8;
            $color = array(0,0,0);
            $text = "{PAGE_NUM} / {PAGE_COUNT}";
            $font = $fontMetrics->get_font("helvetica");
            $pdf->page_text($x, $y, $text, $font, $size, $color, 0.0, 0.0, 0.0);
        }
        /// End Halaman
        </script>
	<div class="row cf" style="margin-bottom: 2px;">
		<div class="col cf" style="float: left; width: 50%; text-align: left;">
			<div style="font-size: 13px; width: 20%; float: left;">
				CABANG

			</div>
			<div style="font-size: 13px; width: 80%; float: right;">
				: <?= $cabang ?>
			</div>
		</div>
		<div class="col" style="float: right; width: 50%; text-align: right;">
			<div style="font-size: 13px; width: 45%; float: left;">
			</div>
			<div style="font-size: 13px; width: 55%; float: right;">
				<!-- Halaman
                : 1 / 1 -->
			</div>
		</div>
	</div>
	<div class="row cf" style="margin-bottom: 2px;">
		<div class="col" style="float: right; width: 50%; text-align: right;">
			<div style="font-size: 13px; width: 45%; float: left;">
			</div>
			<div style="font-size: 13px; width: 55%; float: right;">
				Halaman
				:
			</div>
		</div>
		<br>
		<div class="row cf" style="margin-bottom: 2px;">
			<div class="col cf" style="float: left; width: 50%; text-align: left;">
				<div style="font-size: 13px; width: 20%; float: left;">
					PERIODE
				</div>
				<div style="font-size: 13px; width: 80%; float: right;">
					: <?= date('d-m-Y', strtotime($tglawal)) ?> <sup style="font-size: 10px;">s</sup>/<sub style="font-size: 10px;">d</sub> <?= date('d-m-Y', strtotime($tglakhir)) ?>
				</div>
			</div>
			<div class="col" style="float: right; width: 50%; text-align: right;">
				<div style="font-size: 13px; width: 45%; float: left;">
				</div>
				<div style="font-size: 13px; width: 55%; float: right;">
					Dicetak
					: <?= date('d-m-Y') ?>
				</div>
			</div>
		</div>
		<br>

		<table style="font-size: 10px; text-align:justify;">
			<thead>
				<tr>
					<th rowspan="2" style="text-align: center;">NO</th>
					<th rowspan="2" style="text-align: center;">Nama Toko</th>
					<th rowspan="2" style="text-align: center;">Saldo Awal</th>
					<th rowspan="2" style="text-align: center;">Piutang</th>
					<th colspan="4" style="text-align: center;">Terbayar</th>
					<th rowspan="2" style="text-align: center;">Saldo Akhir</th>
					<tr>
						<th style="text-align: center;">Transfer</th>
						<th style="text-align: center;">Tunai</th>
						<th style="text-align: center;">Deposit</th>
						<th style="text-align: center;">Pembulatan</th>
					</tr>
					
				</tr>
			</thead>
			<tbody>
				<?php 
				$no = 1;
				$piutang = 0;
				$saldo_akhir = 0;
				$terbayar = 0;
				$saldoawal = 0;
				$totalpiutang = 0;
				$totaltunai = 0;
				foreach($report as $val): ?>
				<?php 
				$totaltunai = $totaltunai + $val->tunai;
				$totalpiutang = $totalpiutang + $val->piutang;
				$saldoawal = $saldoawal + $val->saldoawal;
				$total_piutang = $val->saldoawal + $val->piutang;
				$terbayar = $val->tunai + $val->deposit + $val->pembulatan;
				$saldo_akhir = $total_piutang - $terbayar;
				?>
					<tr>
						<td><?= $no++ ?></td>
						<td style="text-align: left;"><?= $val->namacustomer ?></td>
						<td style="text-align: right;"><?= $val->saldoawal ?></td>
						<td style="text-align: right;"><?= $val->piutang ?></td>
						<td style="text-align: right;">0</td>
						<td style="text-align: right;"><?= $val->tunai ?></td>
						<td style="text-align: right;"><?= $val->deposit ?></td>
						<td style="text-align: right;"><?= $val->pembulatan ?></td>
						<td style="text-align: right;"><?= $saldo_akhir ?></td>
					</tr>
				<?php endforeach; ?>
				<?php $no = 1;
				$piutang = 0;
				$saldo_akhir = 0;
				$terbayar = 0;
				$totaltransfer = 0;
				 foreach($row as $vals): ?>
				 <?php 
				 $totaltransfer = $totaltransfer + $vals->transfer;
				$total_piutang = $vals->saldoawal + $vals->piutang;
				$terbayar = $vals->tunai + $vals->deposit + $vals->pembulatan;
				$saldo_akhir = $total_piutang - $terbayar;
				?>
				 <tr>
						<td><?= $no++ ?></td>
						<td style="text-align: left;"><?= $vals->namacustomer ?></td>
						<td style="text-align: right;"><?= $vals->saldoawal ?></td>
						<td style="text-align: right;"><?= $vals->piutang ?></td>
						<td style="text-align: right;"><?= $vals->transfer ?></td>
						<td style="text-align: right;">0</td>
						<td style="text-align: right;"><?= $vals->deposit ?></td>
						<td style="text-align: right;"><?= $vals->pembulatan ?></td>
						<td style="text-align: right;"><?= $saldo_akhir ?></td>
					</tr>
				<?php endforeach; ?>
				<tr>
					<td colspan="9">
						<hr>
					</td>
				</tr>
				<tr>
					<td colspan="2" style="text-align: right; font-weight: bold; font-size: 12px;">Total:</td>
					<td><?= $saldoawal ?></td>
					<td><?= $totalpiutang ?></td>
					<td><?= $totaltransfer ?></td>
					<td><?= $totaltunai ?></td>
				</tr>
			</tbody>
		</table>
</body>
