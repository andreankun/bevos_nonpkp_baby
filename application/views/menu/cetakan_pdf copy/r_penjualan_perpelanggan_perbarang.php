<?php
function FormatRupiah($angka)
{

    $hasil_rupiah = number_format($angka);
    // $hasil_rupiah = "Rp. " . number_format($angka);
    return $hasil_rupiah;
}

function tanggal_indo($tanggal)
{
    $bulan = [
        1 => 'Januari',
        'Februari',
        'Maret',
        'April',
        'Mei',
        'Juni',
        'Juli',
        'Agustus',
        'September',
        'Oktober',
        'November',
        'Desember'
    ];

    $pecahkan = explode('-', $tanggal);

    return $pecahkan[0] . ' ' . $bulan[(int)$pecahkan[1]] . ' ' . $pecahkan[2];
}
?>
<style>
    * {
        font-family: 'Courier New', Courier, monospace;
        /* font-weight: bold; */
    }

    .cf:before,
    .cf:after {
        content: " ";
        /* 1 */
        display: table;
        /* 2 */
    }

    .cf:after {
        clear: both;
    }

    /**
 * For IE 6/7 only
 * Include this rule to trigger hasLayout and contain floats.
 */
    .cf {
        *zoom: 1;
    }

    table {
        /* font-family: Verdana, Arial, Helvetica, sans-serif; */
        border-collapse: collapse;
        width: 100%;
    }

    th {
        border: 1px solid #333;
        text-align: left;
        padding: 8px;
    }

    div {
        font-size: 12;
        /* border: 1px solid #68478D; */
    }
</style>
<title>LAPORAN PENJUALAN PERPELANGGAN & PERBARANG | <?= tanggal_indo(date('d-m-Y', strtotime($tglawal))) ?> - <?= tanggal_indo(date('d-m-Y', strtotime($tglakhir))) ?></title>

<body>
    <!-- <script type="text/php">
        if (isset($pdf)) {
            $x = 515;
            $y = 80;
            $size = 8;
            $color = array(0,0,0);
            $text = "{PAGE_NUM} / {PAGE_COUNT}";
            $font = $fontMetrics->get_font("Courier");
            $pdf->page_text($x, $y, $text, $font, $size, $color, 0.0, 0.0, 0.0);
        }
    </script> -->
    <h4 style="text-align: center;">LAPORAN PENJUALAN PERPELANGGAN & PERBARANG</h4>
    <div class="row cf" style="margin-bottom: 2px;">
        <div class="col cf" style="float: left; width: 50%; text-align: left;">
            <div style="font-size: 13px; width: 20%; float: left;">
                PERIODE
            </div>
            <div style="font-size: 13px; width: 80%; float: right;">
                : <?= date('d-m-Y', strtotime($tglawal)) ?> <sup style="font-size: 10px;">s</sup>/<sub style="font-size: 10px;">d</sub> <?= date('d-m-Y', strtotime($tglakhir)) ?>
            </div>
        </div>
        <div class="col" style="float: right; width: 50%; text-align: right;">
            <div style="font-size: 13px; width: 45%; float: left;">
            </div>
            <div style="font-size: 13px; width: 55%; float: right; margin-right: 86px;">
                <!-- Halaman
                : -->
            </div>
        </div>
    </div>
    <div class="row cf" style="margin-bottom: 2px;">
        <div class="col cf" style="float: left; width: 50%; text-align: left;">
            <div style="font-size: 13px; width: 20%; float: left;">
                PELANGGAN
            </div>
            <div style="font-size: 13px; width: 80%; float: right;">
                : <?= $penjualanperpelangganperbarangrow->namapelanggan ?>
            </div>
        </div>
        <div class="col" style="float: right; width: 50%; text-align: right;">
            <div style="font-size: 13px; width: 45%; float: left;">
            </div>
            <div style="font-size: 13px; width: 55%; float: right;">

            </div>
        </div>
    </div>
    <div class="row cf" style="margin-bottom: 2px;">
        <div class="col cf" style="float: left; width: 50%; text-align: left;">
            <div style="font-size: 13px; width: 20%; float: left;">
                NAMA TOKO
            </div>
            <div style="font-size: 13px; width: 80%; float: right;">
                : <?= $penjualanperpelangganperbarangrow->nama_toko ?>
            </div>
        </div>
        <div class="col" style="float: right; width: 50%; text-align: right;">
            <div style="font-size: 13px; width: 45%; float: left;">
            </div>
            <div style="font-size: 13px; width: 55%; float: right;">

            </div>
        </div>
    </div>
    <div class="row cf" style="margin-bottom: 2px;">
        <div class="col cf" style="float: left; width: 50%; text-align: left;">
            <div style="font-size: 13px; width: 20%; float: left;">
                BARANG
            </div>
            <div style="font-size: 13px; width: 80%; float: right;">
                : <?= $penjualanperpelangganperbarangrow->namabarang ?>
            </div>
        </div>
        <div class="col" style="float: right; width: 50%; text-align: right;">
            <div style="font-size: 13px; width: 45%; float: left;">
            </div>
            <div style="font-size: 13px; width: 55%; float: right;">
                Dicetak
                : <?= date('d-m-Y') ?>
            </div>
        </div>
    </div>
    <br>

    <table style="font-size: 10px; text-align:justify;">
        <thead>
            <tr>
                <th>No.Faktur</th>
                <th>Nama Barang</th>
                <th>Qty</th>
                <th>Harga</th>
                <th>Disc</th>
                <th>Jumlah</th>
            </tr>
        </thead>
        <tbody>
            <?php if (!empty($penjualanperpelangganperbarang)) : ?>
                <?php
                $total = 0;
                $grandtotal = 0;
                foreach ($penjualanperpelangganperbarang as $value) :
                ?>
                    <tr>
                        <td colspan="6" style="font-size: 14px; font-weight: bold;">Tanggal : <?= date('d-m-Y', strtotime($value->tanggal)) ?></td>
                    </tr>
                    <?php $tanggal = $value->tanggal; ?>
                    <?php
                    foreach ($penjualanperpelangganperbarangdetail as $val) : ?>
                        <?php if ($val->tanggal == $tanggal) :
                        ?>
                            <tr>
                                <td style="text-align: left;"><?= $val->nomor ?></td>
                                <td style="text-align: left;"><?= $val->namabarang ?></td>
                                <td style="text-align: right;"><?= FormatRupiah($val->qty) ?></td>
                                <td style="text-align: right;"><?= FormatRupiah($val->harga) ?></td>
                                <td style="text-align: right;"><?= $val->discbarang ?>%</td>
                                <td style="text-align: right;"><?= FormatRupiah($val->total) ?></td>
                            </tr>
                            <?php $total = $total + $val->total; ?>
                        <?php endif; ?>
                        <?php $grandtotal = $grandtotal + $val->total; ?>
                    <?php endforeach; ?>
                    <tr>
                        <td colspan="6">
                            <hr>
                        </td>
                    </tr>
                    <tr style="float: right; text-align: right; font-weight: bold;">
                        <td colspan="5">Total : </td>
                        <td colspan="1"><?= FormatRupiah($total) ?></td>
                    </tr>
                <?php endforeach; ?>
                <tr style="float: right; text-align: right; font-weight: bold;">
                    <td colspan="5">Grand Total : </td>
                    <td colspan="1"><?= FormatRupiah($grandtotal) ?></td>
                </tr>
            <?php else : ?>
                <tr>
                    <td style="text-align: center;" colspan="8">Data tidak tersedia.</td>
                </tr>
            <?php endif; ?>
        </tbody>
    </table>
</body>