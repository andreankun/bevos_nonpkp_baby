<?php
function FormatRupiah($angka)
{

    $hasil_rupiah = number_format($angka);
    // $hasil_rupiah = "Rp. " . number_format($angka);
    return $hasil_rupiah;
}

function tanggal_indo($tanggal)
{
    $bulan = [
        1 => 'Januari',
        'Februari',
        'Maret',
        'April',
        'Mei',
        'Juni',
        'Juli',
        'Agustus',
        'September',
        'Oktober',
        'November',
        'Desember'
    ];

    $pecahkan = explode('-', $tanggal);

    return $pecahkan[0] . ' ' . $bulan[(int)$pecahkan[1]] . ' ' . $pecahkan[2];
}
?>
<style>
    * {
        font-family: 'Courier New', Courier, monospace;
        /* font-weight: bold; */
    }

    .cf:before,
    .cf:after {
        content: " ";
        /* 1 */
        display: table;
        /* 2 */
    }

    .cf:after {
        clear: both;
    }

    /**
 * For IE 6/7 only
 * Include this rule to trigger hasLayout and contain floats.
 */
    .cf {
        *zoom: 1;
    }

    table {
        /* font-family: Verdana, Arial, Helvetica, sans-serif; */
        border-collapse: collapse;
        width: 100%;
    }

    th {
        border: 1px solid #333;
        text-align: left;
        padding: 8px;
    }

    div {
        font-size: 12;
        /* border: 1px solid #68478D; */
    }
</style>
<title>LAPORAN PENJUALAN PER SALESMAN | <?= tanggal_indo(date('d-m-Y', strtotime($tglawal))) ?> - <?= tanggal_indo(date('d-m-Y', strtotime($tglakhir))) ?></title>

<body>
    <script type="text/php">
        if (isset($pdf)) {
            $x = 515;
            $y = 94;
            $size = 8;
            $color = array(0,0,0);
            $text = "{PAGE_NUM} / {PAGE_COUNT}";
            $font = $fontMetrics->get_font("Courier");
            $pdf->page_text($x, $y, $text, $font, $size, $color, 0.0, 0.0, 0.0);
        }
    </script>
    <h4 style="text-align: center;">LAPORAN PENJUALAN PER SALESMAN</h4>
    <div class="row cf" style="margin-bottom: 2px;">
        <div class="col cf" style="float: left; width: 50%; text-align: left;">
            <div style="font-size: 13px; width: 30%; float: left;">
                CABANG
            </div>
            <div style="font-size: 13px; width: 70%; float: right;">
                : <?= $row->nama ?>
            </div>
        </div>
        <div class="col" style="float: right; width: 50%; text-align: right;">
            <div style="font-size: 13px; width: 45%; float: left;">
            </div>
            <div style="font-size: 13px; width: 55%; float: right; margin-right: 86px;">
            </div>
        </div>
    </div>
    <div class="row cf" style="margin-bottom: 2px;">
        <div class="col cf" style="float: left; width: 50%; text-align: left;">
            <div style="font-size: 13px; width: 30%; float: left;">
                PERIODE
            </div>
            <div style="font-size: 13px; width: 70%; float: right;">
                : <?= date('d-m-Y', strtotime($tglawal)) ?> <sup style="font-size: 10px;">s</sup>/<sub style="font-size: 10px;">d</sub> <?= date('d-m-Y', strtotime($tglakhir)) ?>
            </div>
        </div>
        <div class="col" style="float: right; width: 50%; text-align: right;">
            <div style="font-size: 13px; width: 45%; float: left;">
            </div>
            <div style="font-size: 13px; width: 55%; float: right; margin-right: 86px;">
                Halaman
                :
            </div>
        </div>
    </div>
    <div class="row cf" style="margin-bottom: 2px;">
        <div class="col cf" style="float: left; width: 50%; text-align: left;">
            <div style="font-size: 13px; width: 30%; float: left;">
                NAMA SALES
            </div>
            <div style="font-size: 13px; width: 70%; float: right;">
                : <?= $reportrow->namasalesman ?>
            </div>
        </div>
        <div class="col" style="float: right; width: 50%; text-align: right;">
            <div style="font-size: 13px; width: 45%; float: left;">
            </div>
            <div style="font-size: 13px; width: 55%; float: right;">
                Dicetak
                : <?= date('d-m-Y') ?>
            </div>
        </div>
    </div>
    <br>

    <table style="font-size: 10px; text-align:justify;">
        <thead>
            <tr>
                <th>No.</th>
                <th>Faktur</th>
                <th>Tanggal</th>
                <th>Sales Order</th>
                <th>Pelanggan</th>
                <th>Total Penjualan</th>
                <th>Pembayaran</th>
                <th>Saldo</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $no = 1;
            $grandtotal = 0;
            foreach ($report as $val) : ?>
                <tr>
                    <td style="text-align: center;"><?= $no++; ?></td>
                    <td style="text-align: left;"><?= $val->nomor ?></td>
                    <td style="text-align: left;"><?= date('d-m-Y', strtotime($val->tanggal)) ?></td>
                    <td style="text-align: left;"><?= $val->nomorso ?></td>
                    <td style="text-align: left;"><?= $val->namapelanggan ?></td>
                    <td style="text-align: right;"><?= FormatRupiah($val->grandtotal) ?></td>
                    <td></td>
                    <td style="text-align: right;"><?= FormatRupiah($val->grandtotal) ?></td>
                </tr>
                <?php $grandtotal = $grandtotal + $val->grandtotal; ?>
            <?php endforeach; ?>
            <tr>
                <td colspan="8">
                    <hr>
                </td>
            </tr>
            <tr style="float: right; text-align: right; font-weight: bold;">
                <td colspan="5">Grand Total : </td>
                <td colspan="1"><?= FormatRupiah($grandtotal) ?></td>
                <td colspan="1"></td>
                <td colspan="1"><?= FormatRupiah($grandtotal) ?></td>
            </tr>
        </tbody>
    </table>
</body>