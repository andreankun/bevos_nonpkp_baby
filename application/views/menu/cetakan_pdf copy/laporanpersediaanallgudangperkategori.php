<?php
function FormatRupiah($angka)
{

	$hasil_rupiah = number_format($angka);
	// $hasil_rupiah = "Rp. " . number_format($angka);
	return $hasil_rupiah;
}

function tanggal_indo($tanggal)
{
	$bulan = [
		1 => 'Januari',
		'Februari',
		'Maret',
		'April',
		'Mei',
		'Juni',
		'Juli',
		'Agustus',
		'September',
		'Oktober',
		'November',
		'Desember'
	];

	$pecahkan = explode('-', $tanggal);

	return $pecahkan[0] . ' ' . $bulan[(int)$pecahkan[1]] . ' ' . $pecahkan[2];
}
?>
<style>
	* {
		font-family: 'Courier New', Courier, monospace;
		/* font-weight: bold; */
	}

	.cf:before,
	.cf:after {
		content: " ";
		/* 1 */
		display: table;
		/* 2 */
	}

	.cf:after {
		clear: both;
	}

	/**
 * For IE 6/7 only
 * Include this rule to trigger hasLayout and contain floats.
 */
	.cf {
		*zoom: 1;
	}

	table {
		/* font-family: Verdana, Arial, Helvetica, sans-serif; */
		border-collapse: collapse;
		width: 100%;
	}

	th {
		border: 1px solid #333;
		text-align: left;
		padding: 8px;
	}

	div {
		font-size: 12;
		/* border: 1px solid #68478D; */
	}
</style>
<title>LAPORAN PERSEDIAN BARANG SEMUA GUDANG PER - KATEGORI | <?= tanggal_indo(date('d-m-Y', strtotime($tglawal))) ?> - <?= tanggal_indo(date('d-m-Y', strtotime($tglakhir))) ?></title>

<body>
	<!-- Text halaman & text yang hanya dihalaman terakhir -->
	<!-- <script type="text/php">
		/// Halaman
        if (isset($pdf)) {
            $x = 440;
            $y = 77.4;
            $size = 8;
            $color = array(0,0,0);
            $text = "{PAGE_NUM} / {PAGE_COUNT}";
            $font = $fontMetrics->get_font("helvetica");
            $pdf->page_text($x, $y, $text, $font, $size, $color, 0.0, 0.0, 0.0);
        }
        /// End Halaman
        </script> -->
		<h4 style="text-align: center;">LAPORAN PERSEDIAN BARANG SEMUA GUDANG PER - KATEGORI</h4>
	<div class="row cf" style="margin-bottom: 2px;">
		<div class="col cf" style="float: left; width: 50%; text-align: left;">
			<div style="font-size: 13px; width: 20%; float: left;">
				GUDANG

			</div>
			<div style="font-size: 13px; width: 80%; float: right;">
				: SEMUA GUDANG
			</div>
		</div>
		<div class="col" style="float: right; width: 50%; text-align: right;">
			<div style="font-size: 13px; width: 45%; float: left;">
			</div>
			<div style="font-size: 13px; width: 55%; float: right;">
				<!-- Halaman
                : 1 / 1 -->
			</div>
		</div>
	</div>
	<div class="row cf" style="margin-bottom: 2px;">
		<div class="col cf" style="float: left; width: 50%; text-align: left;">
			<div style="font-size: 13px; width: 20%; float: left;">
				KATEGORI
			</div>
			<div style="font-size: 13px; width: 80%; float: right;">
				: <?php echo $merkawal->nama ?> <sup style="font-size: 10px;">s</sup>/<sub style="font-size: 10px;">d</sub> <?php echo $merkakhir->nama ?>
			</div>
		</div>
		<div class="col" style="float: right; width: 50%; text-align: right;">
			<div style="font-size: 13px; width: 45%; float: left;">
			</div>
			<div style="font-size: 13px; width: 55%; float: right;">
				Halaman
				:
			</div>
		</div>
		<br>
		<div class="row cf" style="margin-bottom: 2px;">
			<div class="col cf" style="float: left; width: 80%; text-align: left;">
				<div style="font-size: 13px; width: 12.5%; float: left;">
					PERIODE
				</div>
				<div style="font-size: 13px; width: 80%; float: left;">
					: <?= date('d-m-Y', strtotime($tglawal)) ?> <sup style="font-size: 10px;">s</sup>/<sub style="font-size: 10px;">d</sub> <?= date('d-m-Y', strtotime($tglakhir)) ?>
				</div>
			</div>
			<div class="col" style="float: right; width: 50%; text-align: right;">
				<div style="font-size: 13px; width: 45%; float: left;">
				</div>
				<div style="font-size: 13px; width: 55%; float: right;">
					Dicetak
					: <?= date('d-m-Y') ?>
				</div>
			</div>
		</div>
		<br>

		<table style="font-size: 10px; text-align:justify;">
			<thead>
				<tr>
					<th>Periode</th>
					<th>Barcode</th>
					<th>Nama Produk</th>
					<th>Qty</th>
					<th>Satuan</th>
					<th>Komisi</th>
					<th>Harga Jual</th>
					<th>Total</th>
				</tr>
			</thead>
			<tbody>
			<?php 
				foreach ($persediaan as $val) : ?>
				
					<?php $merk = $val->merk ?>
					<?php $namamerk = $val->namamerk ?>
					<tr>
						<td colspan="8" style="font-weight: bold;">KATEGORI : <?= $val->namamerk ?></td>
					</tr>
					<?php 
					$subqty = 0;
					$subtotal = 0;
					foreach ($report as $vals) : ?>
						<?php if ($vals->merk == $merk) : ?>
							<tr>
								<td style="text-align: left;"><?= $vals->periode ?></td>
								<td style="text-align: left;"><?= $vals->kodebarang ?></td>
								<td style="text-align: left;"><?= $vals->namabarang ?></td>
								<td style="text-align: right;"><?= FormatRupiah($vals->qty) ?></td>
								<td style="text-align: left;"><?= $vals->satuan ?></td>
								<td style="text-align: left;"><?= $vals->komisi ?> %</td>
								<td style="text-align: right;"><?= FormatRupiah($vals->hargajual) ?></td>
								<td style="text-align: right;"><?= FormatRupiah($vals->total) ?></td>
							</tr>
							<?php $subqty = $subqty+$vals->qty ?>
							<?php $subtotal = $subtotal+$vals->total ?>
						<?php endif ?>
						
					<?php endforeach; ?>
					<tr>
						<td colspan="8">
							<hr>
						</td>
					</tr>
					<tr>
						<td colspan="3" style="text-align: right;">Subtotal : </td>
						<td style="text-align: right;"><?= FormatRupiah($subqty) ?></td>
						<td colspan="3" style="text-align: right;"><?= FormatRupiah($subtotal) ?></td>
					</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
</body>
