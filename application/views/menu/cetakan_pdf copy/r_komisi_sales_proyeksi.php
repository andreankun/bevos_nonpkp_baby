<?php
function FormatRupiah($angka)
{

    $hasil_rupiah = number_format($angka);
    // $hasil_rupiah = "Rp. " . number_format($angka);
    return $hasil_rupiah;
}

function tanggal_indo($tanggal)
{
    $bulan = [
        1 => 'Januari',
        'Februari',
        'Maret',
        'April',
        'Mei',
        'Juni',
        'Juli',
        'Agustus',
        'September',
        'Oktober',
        'November',
        'Desember'
    ];

    $pecahkan = explode('-', $tanggal);

    return $pecahkan[0] . ' ' . $bulan[(int)$pecahkan[1]] . ' ' . $pecahkan[2];
}
?>
<style>
    * {
        font-family: 'Courier New', Courier, monospace;
        /* font-weight: bold; */
    }

    .cf:before,
    .cf:after {
        content: " ";
        /* 1 */
        display: table;
        /* 2 */
    }

    .cf:after {
        clear: both;
    }

    /**
 * For IE 6/7 only
 * Include this rule to trigger hasLayout and contain floats.
 */
    .cf {
        *zoom: 1;
    }

    table {
        /* font-family: Verdana, Arial, Helvetica, sans-serif; */
        border-collapse: collapse;
        width: 100%;
    }

    th {
        border: 1px solid #333;
        text-align: left;
        padding: 8px;
    }

    div {
        font-size: 12;
        /* border: 1px solid #68478D; */
    }
</style>
<title>LAPORAN KOMISI SALES PROYEKSI | <?= tanggal_indo(date('d-m-Y', strtotime($tglawal))) ?> - <?= tanggal_indo(date('d-m-Y', strtotime($tglakhir))) ?></title>

<body>
    <!-- <script type="text/php">
        if (isset($pdf)) {
            $x = 515;
            $y = 80;
            $size = 8;
            $color = array(0,0,0);
            $text = "{PAGE_NUM} / {PAGE_COUNT}";
            $font = $fontMetrics->get_font("Courier");
            $pdf->page_text($x, $y, $text, $font, $size, $color, 0.0, 0.0, 0.0);
        }
    </script> -->
    <!-- <-?php foreach ($reportrow as $header) : ?> -->
    <h4 style="text-align: center;">LAPORAN KOMISI SALES PROYEKSI</h4>
    <div class="row cf" style="margin-bottom: 2px;">
        <div class="col cf" style="float: left; width: 50%; text-align: left;">
            <div style="font-size: 13px; width: 20%; float: left;">
                CABANG
            </div>
            <div style="font-size: 13px; width: 80%; float: right;">
                : <?= $row->nama ?>
            </div>
        </div>
        <div class="col" style="float: right; width: 50%; text-align: right;">
            <div style="font-size: 13px; width: 45%; float: left;">
            </div>
            <div style="font-size: 13px; width: 55%; float: right; margin-right: 86px;">
                <!-- Halaman : -->
            </div>
        </div>
    </div>
    <div class="row cf" style="margin-bottom: 2px;">
        <div class="col cf" style="float: left; width: 50%; text-align: left;">
            <div style="font-size: 13px; width: 20%; float: left;">
                SALES
            </div>
            <div style="font-size: 13px; width: 80%; float: right;">
                : <?= $reportrow->namasalesman ?>
            </div>
        </div>
        <div class="col" style="float: right; width: 50%; text-align: right;">
            <div style="font-size: 13px; width: 45%; float: left;">
            </div>
            <div style="font-size: 13px; width: 55%; float: right; margin-right: 86px;">
                <!-- Halaman : -->
            </div>
        </div>
    </div>
    <div class="row cf" style="margin-bottom: 2px;">
        <div class="col cf" style="float: left; width: 50%; text-align: left;">
            <div style="font-size: 13px; width: 20%; float: left;">
                PERIODE
            </div>
            <div style="font-size: 13px; width: 80%; float: right;">
                : <?= date('d-m-Y', strtotime($tglawal)) ?> <sup style="font-size: 10px;">s</sup>/<sub style="font-size: 10px;">d</sub> <?= date('d-m-Y', strtotime($tglakhir)) ?>
            </div>
        </div>
        <div class="col" style="float: right; width: 50%; text-align: right;">
            <div style="font-size: 13px; width: 45%; float: left;">
            </div>
            <div style="font-size: 13px; width: 55%; float: right;">
                Dicetak
                : <?= date('d-m-Y') ?>
            </div>
        </div>
    </div>
    <br>
    <!-- <-?php endforeach; ?> -->

    <table style="font-size: 10px; text-align:justify;" width="100%">
        <thead>
            <tr>
                <th style="text-align:center;">Tanggal</th>
                <th style="text-align:center;">Customer</th>
                <th style="text-align:center;">No. Faktur</th>
                <th style="text-align:center;">Kode</th>
                <th style="text-align:center;">Nama Produk</th>
                <th style="text-align:center;">Qty</th>
                <th style="text-align:center;">Harga</th>
                <th style="text-align:center;">Diskon</th>
                <th style="text-align:center;">Total</th>
                <th style="text-align:center;">Komisi</th>
                <th style="text-align:center;">Persen</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $grandtotal = 0;
            $grandtotalkomisi = 0;
            foreach ($detail as $val) :
                $nilaicash = 0;
                $nilaicustomer = 0;
                $promo = 0;
                $barangitem = 0;
                $totaldiskon = 0;
                $total = 0;
                $barangitem = $barangitem + $val->discbrgitem;
                $promo = $promo + $val->discpaketitem;
                $nilaicustomer = $nilaicustomer + $val->nilaicustomer;
                $nilaicash = $nilaicash + $val->nilaicash;
                // $totaldiskon = $totaldiskon + $barangitem + $promo + $nilaicustomer + $nilaicash;
                $totaldiskon = $totaldiskon + $barangitem + $promo;
                $total = $total  + (($val->qty * $val->harga) - $totaldiskon);
            ?>
                <tr>
                    <td><?= date('d-m-Y', strtotime($val->tanggal)) ?></td>
                    <td><?= $val->namapelanggan ?></td>
                    <td><?= $val->nomor ?></td>
                    <td><?= $val->kodebarang ?></td>
                    <td><?= $val->namabarang ?></td>
                    <td style="text-align: right;"><?= FormatRupiah($val->qty) ?></td>
                    <td style="text-align: right;"><?= FormatRupiah($val->harga) ?></td>
                    <td style="text-align: right;"><?= FormatRupiah($totaldiskon) ?></td>
                    <td style="text-align: right;"><?= FormatRupiah($total) ?></td>
                    <td style="text-align: right;"><?= FormatRupiah($total * ($val->komisi / 100)) ?></td>
                    <td style="text-align: center;"><?= $val->komisi ?></td>
                </tr>
            <?php
                $grandtotal = $grandtotal + $total;
                $grandtotalkomisi = $grandtotalkomisi + ($total * ($val->komisi / 100));
            endforeach;
            ?>
            <tr>
                <td colspan="11">
                    <hr>
                </td>
            </tr>
            <tr>
                <td colspan="8" style="text-align: right; font-weight: bold;">Grand Total :</td>
                <td style="text-align: right;"><?= FormatRupiah($grandtotal) ?></td>
                <td style="text-align: right;"><?= FormatRupiah($grandtotalkomisi) ?></td>
            </tr>
        </tbody>
    </table>
</body>