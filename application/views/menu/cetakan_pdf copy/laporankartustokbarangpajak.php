<?php
function FormatRupiah($angka)
{

	$hasil_rupiah = number_format($angka);
	// $hasil_rupiah = "Rp. " . number_format($angka);
	return $hasil_rupiah;
}

function tanggal_indo($tanggal)
{
	$bulan = [
		1 => 'Januari',
		'Februari',
		'Maret',
		'April',
		'Mei',
		'Juni',
		'Juli',
		'Agustus',
		'September',
		'Oktober',
		'November',
		'Desember'
	];

	$pecahkan = explode('-', $tanggal);

	return $pecahkan[0] . ' ' . $bulan[(int)$pecahkan[1]] . ' ' . $pecahkan[2];
}
?>
<style>
	* {
		font-family: 'Courier New', Courier, monospace;
		/* font-weight: bold; */
	}

	.cf:before,
	.cf:after {
		content: " ";
		/* 1 */
		display: table;
		/* 2 */
	}

	.cf:after {
		clear: both;
	}

	/**
 * For IE 6/7 only
 * Include this rule to trigger hasLayout and contain floats.
 */
	.cf {
		*zoom: 1;
	}

	table {
		/* font-family: Verdana, Arial, Helvetica, sans-serif; */
		border-collapse: collapse;
		width: 100%;
	}

	th {
		border: 1px solid #333;
		text-align: left;
		padding: 8px;
	}

	div {
		font-size: 12;
		/* border: 1px solid #68478D; */
	}
</style>
<title>LAPORAN KARTU STOK BARANG | <?= tanggal_indo(date('d-m-Y', strtotime($tglawal))) ?> - <?= tanggal_indo(date('d-m-Y', strtotime($tglakhir))) ?></title>

<body>
	<!-- Text halaman & text yang hanya dihalaman terakhir -->
	<!-- <script type="text/php">
		/// Halaman
        if (isset($pdf)) {
            $x = 440;
            $y = 77.4;
            $size = 8;
            $color = array(0,0,0);
            $text = "{PAGE_NUM} / {PAGE_COUNT}";
            $font = $fontMetrics->get_font("helvetica");
            $pdf->page_text($x, $y, $text, $font, $size, $color, 0.0, 0.0, 0.0);
        }
        /// End Halaman
        </script> -->
	<div class="row cf" style="margin-bottom: 2px;">
		<div class="col cf" style="float: left; width: 50%; text-align: left;">
			<div style="font-size: 13px; width: 20%; float: left;">
				GUDANG

			</div>
			<div style="font-size: 13px; width: 80%; float: right;">
				: <?= $gudang ?>
			</div>
		</div>
		<div class="col" style="float: right; width: 50%; text-align: right;">
			<div style="font-size: 13px; width: 45%; float: left;">
			</div>
			<div style="font-size: 13px; width: 55%; float: right;">
				<!-- Halaman
                : 1 / 1 -->
			</div>
		</div>
	</div>
	<div class="row cf" style="margin-bottom: 2px;">
		<div class="col cf" style="float: left; width: 50%; text-align: left;">
			<div style="font-size: 13px; width: 20%; float: left;">
				Barang
			</div>
			<div style="font-size: 13px; width: 80%; float: right;">
				: <?php echo $barang ?>
			</div>
		</div>
		<div class="col" style="float: right; width: 50%; text-align: right;">
			<div style="font-size: 13px; width: 45%; float: left;">
			</div>
			<div style="font-size: 13px; width: 55%; float: right;">
				Halaman
				:
			</div>
		</div>
		<br>
		<div class="row cf" style="margin-bottom: 2px;">
			<div class="col cf" style="float: left; width: 50%; text-align: left;">
				<div style="font-size: 13px; width: 20%; float: left;">
					PERIODE
				</div>
				<div style="font-size: 13px; width: 80%; float: right;">
					: <?= date('d-m-Y', strtotime($tglawal)) ?> <sup style="font-size: 10px;">s</sup>/<sub style="font-size: 10px;">d</sub> <?= date('d-m-Y', strtotime($tglakhir)) ?>
				</div>
			</div>
			<div class="col" style="float: right; width: 50%; text-align: right;">
				<div style="font-size: 13px; width: 45%; float: left;">
				</div>
				<div style="font-size: 13px; width: 55%; float: right;">
					Dicetak
					: <?= date('d-m-Y') ?>
				</div>
			</div>
		</div>
		<br>

		<table style="font-size: 10px; text-align:justify;">
			<thead>
				<tr>
					<th>Tanggal</th>
					<th>Keterangan</th>
					<th>Nomor Transaksi</th>
					<th>Sales</th>
					<th>Qty Masuk</th>
					<th>Qty Keluar</th>
					<th>Mutasi Saldo</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td colspan="6">Saldo Awal</td>
					<td><?= $kartustok->saldo_awal ?></td>
					<?php $barang = $kartustok->kodebarang ?>
				</tr>
				<?php
				$saldoawal = 0;
				$mutasi_saldo = 0;
				$qtykeluar = 0;
				// $barang = "";
				foreach ($kartulist as $val) : ?>
					<?php if ($val->kodebarang == $barang) : ?>
						<?php $saldoawal = $kartustok->saldo_awal ?>
						<?php $qtykeluar = $qtykeluar + $val->qtykeluar; ?>
						<?php $mutasi_saldo = $saldoawal - $qtykeluar ?>
						<tr>
							<td><?= date('d-m-Y', strtotime($val->tanggalinvoice)) ?></td>
							<td style="text-align: left;">Penjualan <?= $val->namapelanggan ?></td>
							<td><?= $val->nomor ?></td>
							<td><?= $val->namasalesman ?></td>
							<td>0</td>
							<td><?= $val->qtykeluar ?></td>
							<td><?= $mutasi_saldo ?></td>
						</tr>
					<?php endif; ?>
				<?php endforeach; ?>
				<?php $saldoawal = 0;
				$mutasi_saldo = 0;
				$qtykeluar = 0;
				foreach ($report as $value) : ?>
					<?php $saldoawal = $kartustok->saldo_awal ?>
					<?php $qtykeluar = $qtykeluar + $val->qtykeluar; ?>
					<?php $mutasi_saldo = $saldoawal - $qtykeluar ?>
					<tr>
						<td><?= date('d-m-Y', strtotime($value->tanggaltransfer)) ?></td>
						<td style="text-align: left;">Barang Keluar Ke: <?= $value->keterangan ?></td>
						<td><?= $value->nomor ?></td>
						<td></td>
						<td>0</td>
						<td><?= $value->qtykeluar ?></td>
						<td><?= $mutasi_saldo ?></td>
					</tr>
				<?php endforeach; ?>
				<?php
				$saldoawal = 0;
				$mutasi_saldo = 0;
				$qtymasuk = 0;
				foreach ($Kartulist2 as $vals) : ?>
					<?php $saldoawal = $kartustok->saldo_awal ?>
					<?php $saldoawal = $kartustok->saldo_awal ?>
					<?php $qtymasuk = $qtymasuk + $vals->qtymasuk; ?>
					<?php $mutasi_saldo = $saldoawal + $qtymasuk ?>
					<tr>
						<td><?= date('d-m-Y', strtotime($vals->tanggalpenerimaan)) ?></td>
						<td style="text-align: left;">PM <?= $vals->namasupplier ?></td>
						<td><?= $vals->nomorpo ?></td>
						<td></td>
						<td><?= $vals->qtymasuk ?></td>
						<td>0</td>
						<td><?= $mutasi_saldo ?></td>
					</tr>
				<?php endforeach; ?>
				<?php
				$saldoawal = 0;
				$mutasi_saldo = 0;
				$qtykeluar = 0;
				foreach ($Kartulist3 as $valr) : ?>
					<?php $saldoawal = $kartustok->saldo_awal ?>
					<?php $qtykeluar = $qtykeluar + $valr->qty; ?>
					<?php $mutasi_saldo = $saldoawal + $qtykeluar ?>
					<tr>
						<td><?= date('d-m-Y', strtotime($valr->tanggal)) ?></td>
						<td style="text-align: left;">Barang Buang <?php if($valr->jenis == "1"){
							echo "Barang Rusak";
						} ?></td> <?= $valr->keterangan ?></td>
						<td><?= $valr->nomor ?></td>
						<td></td>
						<td>0</td>
						<td><?= $valr->qty ?></td>
						<td><?= $mutasi_saldo ?></td>
					</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
</body>