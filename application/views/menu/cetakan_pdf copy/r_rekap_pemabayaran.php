<?php
function FormatRupiah($angka)
{

    $hasil_rupiah = number_format($angka);
    // $hasil_rupiah = "Rp. " . number_format($angka);
    return $hasil_rupiah;
}

function tanggal_indo($tanggal)
{
    $bulan = [
        1 => 'Januari',
        'Februari',
        'Maret',
        'April',
        'Mei',
        'Juni',
        'Juli',
        'Agustus',
        'September',
        'Oktober',
        'November',
        'Desember'
    ];

    $pecahkan = explode('-', $tanggal);

    return $pecahkan[0] . ' ' . $bulan[(int)$pecahkan[1]] . ' ' . $pecahkan[2];
}
?>
<style>
    * {
        font-family: 'Courier New', Courier, monospace;
        /* font-weight: bold; */
    }

    .cf:before,
    .cf:after {
        content: " ";
        /* 1 */
        display: table;
        /* 2 */
    }

    .cf:after {
        clear: both;
    }

    /**
 * For IE 6/7 only
 * Include this rule to trigger hasLayout and contain floats.
 */
    .cf {
        *zoom: 1;
    }

    table {
        /* font-family: Verdana, Arial, Helvetica, sans-serif; */
        border-collapse: collapse;
        width: 100%;
    }

    th {
        border: 1px solid #333;
        text-align: left;
        padding: 8px;
    }

    div {
        font-size: 12;
        /* border: 1px solid #68478D; */
    }
</style>
<title>LAPORAN REKAP PEMBAYARAN | <?= tanggal_indo(date('d-m-Y', strtotime($tglawal))) ?> - <?= tanggal_indo(date('d-m-Y', strtotime($tglakhir))) ?></title>

<body>
    <!-- <script type="text/php">
        if (isset($pdf)) {
            $x = 515;
            $y = 108;
            $size = 8;
            $color = array(0,0,0);
            $text = "{PAGE_NUM} / {PAGE_COUNT}";
            $font = $fontMetrics->get_font("Courier");
            $pdf->page_text($x, $y, $text, $font, $size, $color, 0.0, 0.0, 0.0);
        }
    </script> -->
    <h4 style="text-align: center;">LAPORAN REKAP PEMBAYARAN</h4>
    <div class="row cf" style="margin-bottom: 2px;">
        <div class="col cf" style="float: left; width: 50%; text-align: left;">
            <div style="font-size: 13px; width: 30%; float: left;">
                CABANG
            </div>
            <div style="font-size: 13px; width: 70%; float: right;">
                : <?= $row->nama ?>
            </div>
        </div>
        <div class="col" style="float: right; width: 50%; text-align: right;">
            <div style="font-size: 13px; width: 45%; float: left;">
            </div>
            <div style="font-size: 13px; width: 55%; float: right; margin-right: 86px;">
            </div>
        </div>
    </div>
    <div class="row cf" style="margin-bottom: 2px;">
        <div class="col cf" style="float: left; width: 50%; text-align: left;">
            <div style="font-size: 13px; width: 30%; float: left;">
                PERIODE
            </div>
            <div style="font-size: 13px; width: 70%; float: right;">
                : <?= date('d-m-Y', strtotime($tglawal)) ?> <sup style="font-size: 10px;">s</sup>/<sub style="font-size: 10px;">d</sub> <?= date('d-m-Y', strtotime($tglakhir)) ?>
            </div>
        </div>
        <div class="col" style="float: right; width: 50%; text-align: right;">
            <div style="font-size: 13px; width: 45%; float: left;">
            </div>
            <div style="font-size: 13px; width: 55%; float: right;">
                Dicetak
                : <?= date('d-m-Y') ?>
            </div>
        </div>
    </div>
    <br>

    <table style="font-size: 12px; text-align:justify;">
        <thead>
            <tr>
                <th style="text-align: center;">No.</th>
                <th style="text-align: center;">Faktur</th>
                <th style="text-align: center;">Tanggal</th>
                <th style="text-align: center;">Info Pembayaran</th>
                <th style="text-align: center;">Jumlah</th>
            </tr>
        </thead>
        <tbody>
            <!-- <-?php
            foreach ($list as $val) :
                $jenis = $val->kodeaccount;
            ?> -->
            <?php if ($list->kodeaccount == '102001' || $list->kodeaccount == '102002' || $list->kodeaccount == '102003' || $list->kodeaccount == '102004' || $list->kodeaccount == '102005' || $list->kodeaccount == '102006' || $list->kodeaccount == '102007' || $list->kodeaccount == '102012' || $list->kodeaccount == '102013' || $list->kodeaccount == '102014' && $list->accountalokasi == '') : ?>
                <tr>
                    <td colspan="5" style="font-weight: bold;">Pembayaran : TRANSFER</td>
                </tr>
            <?php elseif ($list->kodeaccount == '102001' || $list->kodeaccount == '102002' || $list->kodeaccount == '102003' || $list->kodeaccount == '102004' || $list->kodeaccount == '102005' || $list->kodeaccount == '102006' || $list->kodeaccount == '102007' || $list->kodeaccount == '102012' || $list->kodeaccount == '102013' || $list->kodeaccount == '102014' && $list->accountalokasi == '103999') : ?>
                <tr>
                    <td colspan="5" style="font-weight: bold;">Pembayaran : DEPOSIT</td>
                </tr>
            <?php else : ?>
                <tr>
                    <td colspan="5" style="font-weight: bold;">Pembayaran : TUNAI</td>
                </tr>
            <?php endif; ?>
            <?php
            $no = 1;
            $totalif = 0;
            $grandtotal = 0;
            foreach ($report as $value) : ?>
                <?php if ($value->nilaipiutang - $value->nilaialokasi - $value->nilaipenerimaan == 0) : ?>
                    <tr>
                        <td style="text-align: center;"><?= $no++ ?></td>
                        <td><?= $value->nomor ?></td>
                        <td><?= date('d-m-Y', strtotime($value->tanggal)) ?></td>
                        <td><?= $value->namapelanggan ?></td>
                        <td style="text-align: right;"><?= FormatRupiah($value->terbayar) ?></td>
                    </tr>
                    <?php $totalif = $totalif + $value->terbayar; ?>
                <?php endif; ?>
            <?php endforeach; ?>
            <?php $grandtotal = $grandtotal + $totalif; ?>
            <tr>
                <td colspan="5">
                    <hr>
                </td>
            </tr>
            <tr>
                <td colspan="4" style="text-align:right; font-weight: bold;">
                    Total :
                </td>
                <td colspan="1" style="text-align:right;">
                    <?= FormatRupiah($totalif) ?>
                </td>
            </tr>
            <tr>
                <td colspan="4" style="text-align:right; font-weight: bold;">
                    Grand Total :
                </td>
                <td colspan="1" style="text-align:right;">
                    <?= FormatRupiah($grandtotal) ?>
                </td>
            </tr>
            <!-- <-?php endforeach; ?> -->
        </tbody>
    </table>
</body>