<?php
function rupiah($angka)
{

    $hasil_rupiah = number_format($angka);
    // $hasil_rupiah = "Rp. " . number_format($angka);
    return $hasil_rupiah;
}

function tanggal_indo($tanggal)
{
    $bulan = [
        1 => 'Januari',
        'Februari',
        'Maret',
        'April',
        'Mei',
        'Juni',
        'Juli',
        'Agustus',
        'September',
        'Oktober',
        'November',
        'Desember'
    ];

    $pecahkan = explode('-', $tanggal);

    return $pecahkan[0] . ' ' . $bulan[(int)$pecahkan[1]] . ' ' . $pecahkan[2];
}
?>

<style>
    * {
        font-family: 'Courier New', Courier, monospace;
        font-weight: bold;
    }

    .cf:before,
    .cf:after {
        content: " ";
        /* 1 */
        display: table;
        /* 2 */
    }

    .cf:after {
        clear: both;
    }

    /**
 * For IE 6/7 only
 * Include this rule to trigger hasLayout and contain floats.
 */
    .cf {
        *zoom: 1;
    }

    table {
        font-family: Verdana, Arial, Helvetica, sans-serif;
        border-collapse: collapse;
        width: 100%;
    }

    th {
        border: 1px solid #333;
        text-align: left;
        padding: 8px;
    }

    /* tr:nth-child(even) {
		background-color: #dddddd;
	} */

    div {
        font-size: 12;
        /* border: 1px solid #68478D; */
    }
</style>
<title>Transfer Stok - <?= $vtf->nomor ?></title>
<!-- <title>Surat Jalan Gudang - </title> -->
<!-- <div class="row cf">
	<div class="col cf" style="float: right; width: 100%; text-align: right;">
		<div style="font-size: 10px;">
			<?php if (!empty($vsj->revisike)) : ?>
				[RR-<?= $vsj->revisike ?>]
			<?php else : ?>
				<?= '' ?>
			<?php endif; ?>
		</div>
	</div>
</div> -->
<div class="row cf" style="margin-bottom: 10px;">
    <div class="col" style="float: left; width: 50%; text-align: left;">
        <div style="font-size: 16px;">
            <b>PT. BEVOS PRIMA CENTER</b>
        </div>
    </div>
    <div class="col" style="float: right; width: 50%; text-align: right;">
        <div style="font-size: 13px;">
            <!-- SO# <-?= $vsj->nomorso ?> -->
        </div>
    </div>
</div>

<div class="row cf" style="margin-bottom: 10px;">
    <div class="col" style="float: left; width: 50%; font-weight: bold; text-align: left;">
        <div style="font-size: 16px;">
            TRANSFER STOK
        </div>
    </div>
    <div class="col" style="float: right; width: 50%; text-align: right;">
        <div style="font-size: 13px; text-transform: uppercase;">
            <!-- Jakarta, <?= tanggal_indo(date('d-m-Y')) ?> -->
        </div>
    </div>
</div>

<div class="row cf" style="margin-bottom: 2px;">
    <div class="col cf" style="float: left; width: 40%; text-align: left;">
        <div style="font-size: 13px; width: 35%; float: left;">
            GUDANG
        </div>
        <div style="font-size: 13px; width: 65%; float: right;">
            <!-- : <?= $asal->namagudangasal ?> -->
            : <?= $asal->namacabangasal ?>
        </div>
    </div>
    <div class="col" style="float: right; width: 60%; text-align: right;">
        <div style="font-size: 13px; text-transform: uppercase;">
            <!-- <-?= $vsj->namacustomer ?> -->
        </div>
    </div>
</div>

<div class="row cf" style="margin-bottom: 2px;">
    <div class="col cf" style="float: left; width: 40%; text-align: left;">
        <div style="font-size: 13px; width: 35%; float: left;">
            NO. TF
        </div>
        <div style="font-size: 13px; width: 65%; float: right;">
            : <?= $vtf->nomor ?>
        </div>
    </div>
    <div class="col" style="float: right; width: 60%; text-align: right;">
        <div style="font-size: 13px; text-transform: uppercase;">
            <!-- <-?= $vsj->alamat ?> -->
        </div>
    </div>
</div>

<div class="row cf" style="margin-bottom: 2px;">
    <div class="col cf" style="float: left; width: 40%; text-align: left;">
        <div style="font-size: 13px; width: 35%; float: left;">
            TANGGAL
        </div>
        <div style="font-size: 13px; width: 65%; float: right;">
            <!-- : <-?= $vsj->lokasi ?>&nbsp;(<-?= $vsj->kodesalesman ?>) -->
            : <?= date('d-m-Y', strtotime($vtf->tanggal)) ?>
        </div>
    </div>
    <div class="col" style="float: right; width: 60%; text-align: right;">
        <div style="font-size: 13px; text-transform: uppercase;">
            <!-- <-?= $vsj->kota ?> -->
        </div>
    </div>
</div>

<div class="row cf" style="margin-bottom: 5px;">
    <div class="col" style="float: right; text-align: right; width: 100%;">
        <div style="font-size: 13px;">
            <!-- <-?php if (!empty($vsj->nohp)) : ?>
				<-?= $vsj->nohp ?>
			<-?php elseif (!empty($vsj->notelp)) : ?>
				<-?= $vsj->notelp ?>
			<-?php else : ?>
				<-?= '' ?>
			<-?php endif; ?> -->
        </div>
    </div>
</div>

<table cellspacing="0" cellpadding="3" border="0" width="100%">
    <thead>
        <tr>
            <th style="text-align: center; font-size: 12px;">No.</th>
            <th style="text-align: center; font-size: 12px;">Banyaknya</th>
            <th style="text-align: center; font-size: 12px;">Nama Produk</th>
        </tr>
    </thead>
    <tbody>
        <?php $no = 1;
        foreach ($vtfd as $values) : ?>
            <tr>
                <td style="font-size: 12px; text-align: center;"><?= $no++ ?></td>
                <td style="font-size: 12px; text-align: center;"><?= $values->qty ?> <?= $values->namasatuan ?></td>
                <td style="font-size: 12px; text-align: left;"><?= $values->namabarang ?></td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<hr>
<div class="row cf">
	<div class="col-sm-8" style="float: left; width: 80%; font-size: 12px;">
		<b style="float: left;"></b> <b style="float: left;"><?= $tujuan->namacabangtujuan ?></b>
	</div>
</div>
<div class="row cf">
	<div class="col-sm-8" style="float: right; width: 20%; font-size: 12px;">
		<?php $sum = 0;
		foreach ($vtfd as $cell) : ?>
			<?php $sum += $cell->qty++ ?>
		<?php endforeach ?>
		<b style="float: left;">Total Qty : </b> <b style="float: right;"><?= rupiah($sum) ?></b>
	</div>
</div>
<div class="row cf">
    <div class="col cf" style="float: left; width: 30%;">
        <div style="font-size: 13px;">
            Diterima,
        </div>
    </div>
    <div class="col cf" style="float: left; width: 30%;">
        <div style="font-size: 13px;">
            Sopir,
        </div>
    </div>
    <div class="col cf" style="float: left; width: 30%;">
        <div style="font-size: 13px;">
            Hormat Kami,
        </div>
    </div>
</div>

<!-- <div class="row cf">
	<div class="col cf" style="float: right; width: 100%; text-align: right;">
		<div style="font-size: 10px;">
			<?php if ($vsj->reprint == 't') : ?>
				[R]
			<?php else : ?>
				<?= '' ?>
			<?php endif; ?>
		</div>
	</div>
</div> -->