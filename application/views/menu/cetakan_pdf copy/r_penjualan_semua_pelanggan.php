<?php
function FormatRupiah($angka)
{

    $hasil_rupiah = number_format($angka);
    // $hasil_rupiah = "Rp. " . number_format($angka);
    return $hasil_rupiah;
}

function tanggal_indo($tanggal)
{
    $bulan = [
        1 => 'Januari',
        'Februari',
        'Maret',
        'April',
        'Mei',
        'Juni',
        'Juli',
        'Agustus',
        'September',
        'Oktober',
        'November',
        'Desember'
    ];

    $pecahkan = explode('-', $tanggal);

    return $pecahkan[0] . ' ' . $bulan[(int)$pecahkan[1]] . ' ' . $pecahkan[2];
}
?>
<style>
    * {
        font-family: 'Courier New', Courier, monospace;
        /* font-weight: bold; */
    }

    .cf:before,
    .cf:after {
        content: " ";
        /* 1 */
        display: table;
        /* 2 */
    }

    .cf:after {
        clear: both;
    }

    /**
 * For IE 6/7 only
 * Include this rule to trigger hasLayout and contain floats.
 */
    .cf {
        *zoom: 1;
    }

    table {
        /* font-family: Verdana, Arial, Helvetica, sans-serif; */
        border-collapse: collapse;
        width: 100%;
    }

    th {
        border: 1px solid #333;
        text-align: left;
        padding: 8px;
    }

    tr {
        line-height: 1.5rem;
    }

    div {
        font-size: 12;
        /* border: 1px solid #68478D; */
    }
</style>
<title>LAPORAN REKAP PENJUALAN SEMUA PELANGGAN | <?= tanggal_indo(date('d-m-Y', strtotime($tglawal))) ?> - <?= tanggal_indo(date('d-m-Y', strtotime($tglakhir))) ?></title>

<body>
    <!-- <script type="text/php">
        if (isset($pdf)) {
            $x = 515;
            $y = 80;
            $size = 8;
            $color = array(0,0,0);
            $text = "{PAGE_NUM} / {PAGE_COUNT}";
            $font = $fontMetrics->get_font("Courier");
            $pdf->page_text($x, $y, $text, $font, $size, $color, 0.0, 0.0, 0.0);
        }
    </script> -->
    <h4 style="text-align: center;">LAPORAN REKAP PENJUALAN SEMUA PELANGGAN</h4>
    <div class="row cf" style="margin-bottom: 2px;">
        <div class="col cf" style="float: left; width: 50%; text-align: left;">
            <div style="font-size: 13px; width: 20%; float: left;">
                CABANG
            </div>
            <div style="font-size: 13px; width: 80%; float: right;">
                : <?= $row->nama ?>
            </div>
        </div>
        <div class="col" style="float: right; width: 50%; text-align: right;">
            <div style="font-size: 13px; width: 45%; float: left;">
            </div>
            <div style="font-size: 13px; width: 55%; float: right; margin-right: 86px;">
                <!-- Halaman
                : -->
            </div>
        </div>
    </div>
    <div class="row cf" style="margin-bottom: 2px;">
        <div class="col cf" style="float: left; width: 50%; text-align: left;">
            <div style="font-size: 13px; width: 20%; float: left;">
                PERIODE
            </div>
            <div style="font-size: 13px; width: 80%; float: right;">
                : <?= date('d-m-Y', strtotime($tglawal)) ?> <sup style="font-size: 10px;">s</sup>/<sub style="font-size: 10px;">d</sub> <?= date('d-m-Y', strtotime($tglakhir)) ?>
            </div>
        </div>
        <div class="col" style="float: right; width: 50%; text-align: right;">
            <div style="font-size: 13px; width: 45%; float: left;">
            </div>
            <div style="font-size: 13px; width: 55%; float: right;">
                Dicetak
                : <?= date('d-m-Y') ?>
            </div>
        </div>
    </div>
    <table style="font-size: 10px;" width="100%">
        <thead>
            <tr>
                <th>No.</th>
                <th>Invoice</th>
                <th>Pelanggan</th>
                <th>Tanggal</th>
                <th>Subtotal</th>
                <th>Diskon</th>
                <th>DPP</th>
                <th>PPN</th>
                <th>Total</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $no = 1;
            $subtotal = 0;
            $grandtotal = 0;
            $total_diskon = 0;
            $dpp = 0;
            $hasilppn = 0;
            $hsubtotal = 0;
            $htotal_diskon = 0;
            $hdpp = 0;
            $hhasilppn = 0;
            $hgrandtotal = 0;
            foreach ($report as $vals) :
                $subtotal = $vals->subtotal - $vals->discbrgitem;
                $total_diskon = $vals->discpaketitem + $vals->nilaicash + $vals->nilaicustomer;
                $dpp = $vals->dpp;
                // print_r($ppn->ppn);
                // die();
                $hasilppn = floor($vals->dpp * ($ppn->ppn / 100));
                $grandtotal = $vals->dpp + $hasilppn;
            ?>
                <tr>
                    <td style="text-align: center;"><?= $no++; ?></td>
                    <td style="text-align: left;"><?= $vals->nomor ?></td>
                    <td style="text-align: left;"><?= $vals->nama_toko ?></td>
                    <td style="text-align: center;"><?= date('d-m-Y', strtotime($vals->tanggal)) ?></td>
                    <td style="text-align: right;"><?= FormatRupiah($subtotal) ?></td>
                    <?php if ($total_diskon == 0) : ?>
                        <td style="text-align: right;"><?= '' ?></td>
                    <?php else : ?>
                        <td style="text-align: right;"><?= FormatRupiah($total_diskon) ?></td>
                    <?php endif; ?>
                    <td style="text-align: right;"><?= FormatRupiah($dpp) ?></td>
                    <td style="text-align: right;"><?= FormatRupiah($hasilppn) ?></td>
                    <td style="text-align: right;"><?= FormatRupiah($grandtotal) ?></td>
                    <?php $hsubtotal = $hsubtotal + $subtotal; ?>
                    <?php $htotal_diskon = $htotal_diskon + $total_diskon; ?>
                    <?php $hdpp = $hdpp + $dpp; ?>
                    <?php $hhasilppn = $hhasilppn + $hasilppn; ?>
                    <?php $hgrandtotal = $hgrandtotal + $grandtotal; ?>
                </tr>
            <?php endforeach; ?>
            <tr>
                <td colspan="9">
                    <hr>
                </td>
            </tr>
            <tr style="float: right; text-align: right; font-weight: bold; font-size: 10px; text-transform: uppercase;">
                <td colspan="4">Grand Total : </td>
                <td colspan="1"><?= FormatRupiah($hsubtotal) ?></td>
                <td colspan="1"><?= FormatRupiah($htotal_diskon) ?></td>
                <td colspan="1"><?= FormatRupiah($hdpp) ?></td>
                <td colspan="1"><?= FormatRupiah($hhasilppn) ?></td>
                <td colspan="1"><?= FormatRupiah($hgrandtotal) ?></td>
            </tr>
        </tbody>
    </table>
</body>