<?php 
    function rupiah($angka){
        
        $hasil_rupiah = "Rp. " . number_format($angka);
        return $hasil_rupiah;
    
    }
?>
<title>
	Laporan Dokumen : <?= date('d M Y', strtotime($tglawal))?> - <?= date('d M Y', strtotime($tglakhir))?>
</title>
<style>
    @page {
        /* margin: 0.5cm 0cm 0cm 0cm; */
        margin: 5.5cm 0cm 6cm 0cm;
    }
    @media print {
        .con1,.con2,.con3 {
            page-break-inside: avoid;
        }
    }
    body {
        text-align: center;
    }
    header {
        position: fixed;
        display: block;
        top: 0cm;
        left: 0cm;
        right: 0cm;
        width: 100%;
        height: 20%;
        margin: -5cm 0cm 0cm 0cm;
        /** Extra personal styles **/

        line-height: 1.5cm;
    }
    footer {
        position: fixed; 
        bottom: 0cm; 
        left: 0cm; 
        right: 0cm;
        height: 25%;
        margin: 0cm 0cm -2cm 0cm;

        /** Extra personal styles **/
        text-align: center;
        line-height: 1.5cm;
    }
    
    .theader,
    .tcontent {
        width: 100%;
        padding: 0cm 1cm 0.5cm 1cm;
    }
    .theader-logo {
        position: relative;
        width: 80px;
        height: auto;
    }
    .theader td,
    .tcontent td,
    .theader th,
    .tcontent th {
        vertical-align: middle;
        text-align: left;
        font-size: 12px;
        line-height: 0.5cm;
        padding: 2px;
    }
    .thcontent th {
        background: #dee2e6;
        border: 2px solid #fff;
    }
    .tbcontent td {
        border: 1px solid #dee2e6;
        border-top: 0px;
        padding: 0.2em 0.5em;
    }
    table {
        border-collapse: collapse;
        break-before:auto;
        font-family: helvetica !important;
    }
    tr {
        break-inside:avoid;
        break-before:auto;
    }
    thead {
        display:table-header-group;
    }
    tfoot {
        display:table-footer-group;
    }
</style>
<body>
    <header>
        <table class="theader">
            <tr>
                <td rowspan="2" width="10%">
                    <img class="theader-logo" src="./assets/img/c3-logo.png">
                </td>
                <td width="60%" style="line-height: 0cm; vertical-align: top;">
                    <h2>PT. CAHAYA CEMERLANG CHEMINDO</h2>
                </td>
                <!-- <td width="30%">
					Jakarta, <?= date('d  M  Y') ?>
                </td> -->
            </tr>
            <tr>
                <td width="70%">
                    Gedung IKM Lt. 3A Jl. Daan Mogot No.6<br>
                    Wijaya Kusuma, Grogol Petamburan, Jakarta Barat
                </td>
            </tr>
        </table>
        <table class="theader">
            <tr>
                <td colspan="3" style="line-height: 0.05cm;">
                    <h2 align="center">LAPORAN DOKUMEN</h2>
					<p align="center">Periode : <?= date('d M Y', strtotime($tglawal)) ?> - <?= date('d M Y', strtotime($tglakhir)) ?></p>
                </td>
            </tr>
        </table>
    </header>
    <!-- <footer>
        <table class="tcontent">
            <thead>
                <tr>
                    <th style="text-align: center;" width="25%">
                        <span>Admin Sales</span>
                    </th>
                    <th style="text-align: center;" width="25%">
                        <span>Gudang</span>
                    </th>
                    <th style="text-align: center;" width="25%">
                        <span>Pengantar</span>
                    </th>
                    <th style="text-align: center;" width="25%">
                        <span>Yang Menerima</span>
                    </th>
                </tr>
            </thead>
            <tr>
                <td style="text-align: center;">
                    <br>
                    <br>
                    <br>
                    <br>
                    <span>____________________________</span>
                </td>
                <td style="text-align: center;">
                    <br>
                    <br>
                    <br>
                    <br>
                    <span>____________________________</span>
                </td>
                <td style="text-align: center;">
                    <br>
                    <br>
                    <br>
                    <br>
                    <span>____________________________</span>
                </td>
                <td style="text-align: center;">
                    <br>
                    <br>
                    <br>
                    <br>
                    <span>____________________________</span>
                </td>
            </tr>
        </table>
        <table class="tcontent">
            <tr>
                <td width="20%" style="font-size: 10px;">
                    <span>Putih: Penagihan</span>
                </td>
                <td width="20%" style="font-size: 10px;">
                    <span>Merah: Pembeli</span>
                </td>
                <td width="20%" style="font-size: 10px;">
                    <span>Kuning: Admin Sales</span>
                </td>
                <td width="20%" style="font-size: 10px;">
                    <span>Biru: Gudang</span>
                </td>
                <td width="20%" style="font-size: 10px;">
                    <span>Hijau: Accounting</span>
                </td>
            </tr>
        </table>
        <table class="tcontent">
            <tr>
                <td width="20%" style="font-size: 10px;">
                    <span>Pemakai: M Helmi Hadiansyah</span>
                </td>
                <td width="20%" style="font-size: 10px;">
                    <span>Tgl Entry: 14-08-2021</span>
                </td>
            </tr>
        </table>
    </footer>  -->
    <table class="tcontent">
        <thead class="thcontent">
            <tr>
				<th style="text-align: center;">
                    <b>No.</b>
                </th>
                <th style="text-align: center;">
                    <b>STATUS</b>
                </th>
                <th style="text-align: center;">
                    <b>NOMOR SO</b>
                </th>
                <th style="text-align: center;">
                    <b>TANGGAL SO</b>
                </th>
                <th style="text-align: center;">
                    <b>NOMOR PO</b>
                </th>
				<th style="text-align: center;">
					<b>TANGGAL PO</b>
				</th>
				<th style="text-align: center;">
					<b>JENIS ORDER</b>
				</th>
				<th style="text-align: center;">
                    <b>NOMOR CUSTOMER</b>
                </th>
				<th style="text-align: center;">
                    <b>NAMA CUSTOMER</b>
                </th>
				<th style="text-align: center;">
                    <b>PLAFON ORDER</b>
                </th>
				<th style="text-align: center;">
                    <b>PLAFON OVERDUE</b>
                </th>
                <th style="text-align: center;">
                    <b>TOTAL TRANSAKSI</b>
                </th>
                <!-- <th style="text-align: center;">
                    <b>TOTAL</b>
                </th> -->
            </tr>
        </thead>
        <tbody class="tbcontent">
			<?php $no = 1; $sum = 0; foreach($report_so as $cell) : ?>
            <tr>
				<td>
					<?= $no++ ?>
                </td>
                <td style="font-size: 11px;">
					<?= $cell->transaksi ?>
                </td>
                <td style="font-size: 11px;">
					<?= $cell->nomorso ?>
                </td>
                <td style="font-size: 11px;">
					<?= date('d-m-Y', strtotime($cell->tanggalso)) ?>
                </td>
                <td style="font-size: 11px;">
					<?= $cell->nomorpo ?>
                </td>
                <td style="font-size: 11px">
					<?= date('d-m-Y', strtotime($cell->tanggalpo)) ?>
                </td>
				<td style="font-size: 11px;">
					<?= $cell->jenisorder ?>
				</td>
				<td style="font-size: 11px;">
					<?= $cell->nocustomer ?>
                </td>
				<td style="font-size: 11px;">
					<?= $cell->namacustomer ?>
                </td>
				<td style="font-size: 11px;">
					<?= $cell->plafonorder ?>
                </td>
				<td style="font-size: 11px;">
					<?= $cell->plafonoverdue ?>
                </td>
                <td style="text-align: center; font-size: 11px;">
					<?= rupiah($cell->grandtotal) ?>
                </td>
            </tr>
			<?php endforeach; ?>
        </tbody>
    </table>

	<table width="98%" style="margin-top: 5px;">
            <td width="70%" style="vertical-align: top;">

            </td>

            <td width="30%" style="vertical-align: top;">
                <table width="100%" style="font-size: 10; padding: 10px; margin-top: 5px; border-radius: 4px; border: thin solid grey;">
                    <tr>
						<td>
							<span style="font-weight: bold;">GRAND TOTAL :</span>
                        </td>
						
                        <td>
							<?php $sum = 0; foreach($report_so as $cell):?>
							<?php $sum += $cell->grandtotal++ ?> 
							<?php endforeach ?>
                            <span style="color: #000000; font-weight: bold;"><?= rupiah($sum) ?>,-</span>
                        </td>
                    </tr>
                </table>
            </td>
        </table>
    
</body>
