<?php
function FormatRupiah($angka)
{

	$hasil_rupiah = number_format($angka);
	// $hasil_rupiah = "Rp. " . number_format($angka);
	return $hasil_rupiah;
}

function tanggal_indo($tanggal)
{
	$bulan = [
		1 => 'Januari',
		'Februari',
		'Maret',
		'April',
		'Mei',
		'Juni',
		'Juli',
		'Agustus',
		'September',
		'Oktober',
		'November',
		'Desember'
	];

	$pecahkan = explode('-', $tanggal);

	return $pecahkan[0] . ' ' . $bulan[(int)$pecahkan[1]] . ' ' . $pecahkan[2];
}
?>
<style>
	* {
		font-family: 'Courier New', Courier, monospace;
		/* font-weight: bold; */
	}

	.cf:before,
	.cf:after {
		content: " ";
		/* 1 */
		display: table;
		/* 2 */
	}

	.cf:after {
		clear: both;
	}

	/**
 * For IE 6/7 only
 * Include this rule to trigger hasLayout and contain floats.
 */
	.cf {
		*zoom: 1;
	}

	table {
		/* font-family: Verdana, Arial, Helvetica, sans-serif; */
		border-collapse: collapse;
		width: 100%;
	}

	th {
		border: 1px solid #333;
		text-align: left;
		padding: 8px;
	}

	tr {
		line-height: 1.5rem;
	}

	div {
		font-size: 12;
		/* border: 1px solid #68478D; */
	}
</style>
<title>LAPORAN PENJUALAN SEMUA PELANGGAN | <?= tanggal_indo(date('d-m-Y', strtotime($tglawal))) ?> - <?= tanggal_indo(date('d-m-Y', strtotime($tglakhir))) ?></title>

<body>
	<script type="text/php">
		if (isset($pdf)) {
            $x = 515;
            $y = 80;
            $size = 8;
            $color = array(0,0,0);
            $text = "{PAGE_NUM} / {PAGE_COUNT}";
            $font = $fontMetrics->get_font("Courier");
            $pdf->page_text($x, $y, $text, $font, $size, $color, 0.0, 0.0, 0.0);
        }
    </script>
	<h4 style="text-align: center;">LAPORAN PIUTANG JATUH TEMPO</h4>
	<div class="row cf" style="margin-bottom: 2px;">
		<div class="col cf" style="float: left; width: 50%; text-align: left;">
			<div style="font-size: 13px; width: 20%; float: left;">
				CABANG
			</div>
			<div style="font-size: 13px; width: 80%; float: right;">
				: <?= $cabang ?>
			</div>
		</div>
	</div>
	<div class="row cf" style="margin-bottom: 2px;">
		<div class="col" style="float: right; width: 50%; text-align: right;">
			<div style="font-size: 13px; width: 45%; float: left;">
			</div>
			<div style="font-size: 13px; width: 55%; float: right; margin-right: 86px;">
				Halaman
				:
			</div>
		</div>
	</div>
	<div class="row cf" style="margin-bottom: 2px;">
		<div class="col cf" style="float: left; width: 50%; text-align: left;">
			<div style="font-size: 13px; width: 20%; float: left;">
				PERIODE
			</div>
			<div style="font-size: 13px; width: 80%; float: right;">
				: <?= date('d-m-Y', strtotime($tglawal)) ?> <sup style="font-size: 10px;">s</sup>/<sub style="font-size: 10px;">d</sub> <?= date('d-m-Y', strtotime($tglakhir)) ?>
			</div>
		</div>
		<div class="col" style="float: right; width: 50%; text-align: right;">
			<div style="font-size: 13px; width: 45%; float: left;">
			</div>
			<div style="font-size: 13px; width: 55%; float: right;">
				Dicetak
				: <?= date('d-m-Y') ?>
			</div>
		</div>
	</div>
	<br>

	<table style="font-size: 10px; text-align:left;" width="100%">
		<thead>
			<tr>
				<th>No</th>
				<th>No.Faktur</th>
				<th>Tanggal</th>
				<th>Jatuh Tempo</th>
				<th>Piutang</th>
				<th>Terbayar</th>
				<th>Saldo</th>
			</tr>
		</thead>
		<tbody>
			<?php
			$totalsemua = 0;
			$totalterbayarasemua = 0;
			$totalsaldosemua = 0;
			$totalpiutang = 0;
			$totalterbayar = 0;
			$totalsaldo = 0;
			foreach ($row as $val) : ?>
				<?php $totalsemua = $totalsemua + $totalpiutang ?>
				<?php $totalterbayarasemua = $totalterbayarasemua + $totalterbayar ?>
				<?php $totalsaldosemua = $totalsaldosemua + $totalsaldo ?>
				<?php $tanggal = $val->tanggal ?>
				<?php $nama = $val->nama ?>
				<tr>
					<td style="font-weight: bold; font-size: 11px; "><?= $val->nama ?></td>
				</tr>
				<?php
				$no = 1;
				$saldo = 0;
				$totalpiutang = 0;
				$totalterbayar = 0;
				$totalsaldo = 0;
				foreach ($report as $vals) : ?>
					<?php if ($vals->tanggal == $tanggal && $vals->nama == $nama) : ?>
						<?php $saldo = $vals->piutang - $vals->terbayar ?>
						<?php $totalpiutang = $totalpiutang + $vals->piutang; ?>
						<?php $totalsaldo = $totalsaldo + $saldo ?>
						<?php $totalterbayar = $totalterbayar + $vals->terbayar ?>
						<tr>
							<td style="font-weight: bold; font-size: 11px; "><?= $no++ ?></td>
							<td style="font-weight: bold; font-size: 11px; "><?= $vals->nomor ?></td>
							<td style="font-weight: bold; font-size: 11px; "><?= date('d-m-Y', strtotime($vals->tanggal)) ?></td>
							<td style="font-weight: bold; font-size: 11px; "><?= date('d-m-Y', strtotime($vals->tgljthtempo)) ?></td>
							<td style="font-weight: bold; font-size: 11px; "><?= FormatRupiah($vals->piutang) ?></td>
							<td style="font-weight: bold; font-size: 11px; "><?= FormatRupiah($vals->terbayar) ?></td>
							<td style="font-weight: bold; font-size: 11px; "><?= FormatRupiah($saldo) ?></td>
						</tr>
					<?php endif; ?>
				<?php endforeach; ?>
				<tr>
					<td colspan="7">
						<hr>
					</td>
				</tr>
				<tr>
					<td colspan="4" style="font-weight: bold; font-size: 11px; text-align: right;">Total :</td>
					<td colspan="2" style="font-weight: bold; font-size: 11px;"><?= FormatRupiah($totalpiutang) ?></td>
					<td colspan="2" style="font-weight: bold; font-size: 11px; text-align: left;"><?= FormatRupiah($totalsaldo) ?></td>
				</tr>
			<?php endforeach; ?>
			<tr>

				<td colspan="4" style="font-weight: bold; font-size: 11px; text-align: right;">Grandtotal :</td>
				<td colspan="" style="font-weight: bold; font-size: 11px; text-align: left;"><?= FormatRupiah($totalsemua) ?></td>
				<td colspan="" style="font-weight: bold; font-size: 11px; text-align: left;"><?= FormatRupiah($totalterbayarasemua) ?></td>
				<td colspan="" style="font-weight: bold; font-size: 11px; text-align: left;"><?= FormatRupiah($totalsaldosemua) ?></td>
			</tr>
		</tbody>
	</table>
</body>
