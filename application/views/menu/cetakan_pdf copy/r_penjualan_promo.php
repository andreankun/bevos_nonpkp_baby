<?php
function FormatRupiah($angka)
{

    $hasil_rupiah = number_format($angka);
    // $hasil_rupiah = "Rp. " . number_format($angka);
    return $hasil_rupiah;
}

function tanggal_indo($tanggal)
{
    $bulan = [
        1 => 'Januari',
        'Februari',
        'Maret',
        'April',
        'Mei',
        'Juni',
        'Juli',
        'Agustus',
        'September',
        'Oktober',
        'November',
        'Desember'
    ];

    $pecahkan = explode('-', $tanggal);

    return $pecahkan[0] . ' ' . $bulan[(int)$pecahkan[1]] . ' ' . $pecahkan[2];
}
?>
<style>
    * {
        font-family: 'Courier New', Courier, monospace;
        /* font-weight: bold; */
    }

    .cf:before,
    .cf:after {
        content: " ";
        /* 1 */
        display: table;
        /* 2 */
    }

    .cf:after {
        clear: both;
    }

    /**
 * For IE 6/7 only
 * Include this rule to trigger hasLayout and contain floats.
 */
    .cf {
        *zoom: 1;
    }

    table {
        /* font-family: Verdana, Arial, Helvetica, sans-serif; */
        border-collapse: collapse;
        width: 100%;
    }

    th {
        border: 1px solid #333;
        text-align: left;
        padding: 8px;
    }

    div {
        font-size: 12;
        /* border: 1px solid #68478D; */
    }
</style>
<title>LAPORAN PENJUALAN PROMO | <?= tanggal_indo(date('d-m-Y', strtotime($tglawal))) ?> - <?= tanggal_indo(date('d-m-Y', strtotime($tglakhir))) ?></title>

<body>
    <!-- <script type="text/php">
        if (isset($pdf)) {
            $x = 515;
            $y = 80;
            $size = 8;
            $color = array(0,0,0);
            $text = "{PAGE_NUM} / {PAGE_COUNT}";
            $font = $fontMetrics->get_font("Courier");
            $pdf->page_text($x, $y, $text, $font, $size, $color, 0.0, 0.0, 0.0);
        }
    </script> -->
    <!-- <-?php foreach ($reportrow as $header) : ?> -->
    <h4 style="text-align: center;">LAPORAN PENJUALAN PROMO</h4>
    <div class="row cf" style="margin-bottom: 2px;">
        <div class="col cf" style="float: left; width: 50%; text-align: left;">
            <div style="font-size: 13px; width: 20%; float: left;">
                <!-- CABANG -->
            </div>
            <div style="font-size: 13px; width: 80%; float: right;">
                <!-- : <-?= $row->nama ?> -->
            </div>
        </div>
        <div class="col" style="float: right; width: 50%; text-align: right;">
            <div style="font-size: 13px; width: 45%; float: left;">
            </div>
            <div style="font-size: 13px; width: 55%; float: right; margin-right: 86px;">
                <!-- Halaman : -->
            </div>
        </div>
    </div>
    <div class="row cf" style="margin-bottom: 2px;">
        <div class="col cf" style="float: left; width: 50%; text-align: left;">
            <div style="font-size: 13px; width: 20%; float: left;">
                PERIODE
            </div>
            <div style="font-size: 13px; width: 80%; float: right;">
                : <?= date('d-m-Y', strtotime($tglawal)) ?> <sup style="font-size: 10px;">s</sup>/<sub style="font-size: 10px;">d</sub> <?= date('d-m-Y', strtotime($tglakhir)) ?>
            </div>
        </div>
        <div class="col" style="float: right; width: 50%; text-align: right;">
            <div style="font-size: 13px; width: 45%; float: left;">
            </div>
            <div style="font-size: 13px; width: 55%; float: right;">
                Dicetak
                : <?= date('d-m-Y') ?>
            </div>
        </div>
    </div>
    <br>
    <!-- <-?php endforeach; ?> -->

    <table style="font-size: 10px; text-align:justify;" width="100%">
        <thead>
            <tr>
                <th style="text-align:center;">No.</th>
                <th style="text-align:center;">Kode</th>
                <th style="text-align:center;" colspan="3">Nama Produk</th>
                <th style="text-align:center;">Qty</th>
                <th style="text-align:center;">Satuan</th>
                <th style="text-align:center;">Harga</th>
                <th style="text-align:center;">Diskon</th>
                <th style="text-align:center;">Total</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($report as $val) : ?>
                <?php $nomor = $val->nomor; ?>
                <?php foreach ($reportrow as $vals) : ?>
                    <?php if ($vals->nomor == $nomor) : ?>
                        <tr>
                            <td colspan="2">No.Faktur : <?= $vals->nomor; ?></td>
                            <td colspan="2">Tanggal : <?= date('d-m-Y', strtotime($vals->tanggal)) ?></td>
                            <td colspan="2">Sales : <?= $vals->namasalesman ?></td>
                            <td colspan="4">Promo : <?= $vals->namapromo ?></td>
                        </tr>
                        <tr>
                            <td colspan="10">
                                <hr>
                            </td>
                        </tr>
                    <?php endif; ?>
                <?php endforeach; ?>
                <?php
                $no = 1;
                $total = 0;
                $subtotal = 0;
                $subtotals = 0;
                $diskonlevel = 0;
                $diskonpromo = 0;
                $totaldiskonpromo = 0;
                $diskoncash = 0;
                $diskoncustomer = 0;
                $potongan = 0;
                $dpp = 0;
                $grandtotal = 0;
                $ppn = 0;
                foreach ($detail as $key => $value) : ?>
                    <?php if ($value->nomor == $nomor) :
                        $total = $value->harga * $value->qty;
                        $diskonlevel = $value->discbrgitem;
                        $subtotal = $total - $diskonlevel;
                        $diskonpromo = $value->qty * $value->discpaketitem;
                        $diskoncash = $value->nilaicash;
                        $diskoncustomer = $value->nilaicustomer;
                    ?>
                        <tr>
                            <td style="text-align: left;"><?= $no++; ?></td>
                            <td style="text-align: left;"><?= $value->kodebarang; ?></td>
                            <td style="text-align: left;" colspan="3"><?= $value->namabarang; ?></td>
                            <td style="text-align: right;"><?= $value->qty; ?></td>
                            <td style="text-align: left;"><?= $value->namasatuan; ?></td>
                            <td style="text-align: right;"><?= FormatRupiah($value->harga); ?></td>
                            <td style="text-align: right;"><?= FormatRupiah($diskonlevel); ?></td>
                            <td style="text-align: right;"><?= FormatRupiah($subtotal); ?></td>
                        </tr>
                        <?php
                        $subtotals = $subtotals + $subtotal;
                        $totaldiskonpromo = $totaldiskonpromo + $diskonpromo;
                        $potongan = round(($totaldiskonpromo + $diskoncash + $diskoncustomer) / 10) * 10;
                        $dpp = $subtotals - $potongan;
                        $ppn = ($dpp * $konfigurasi->ppn) / 100;
                        $grandtotal = $dpp + $ppn;
                        ?>
                    <?php endif; ?>
                <?php endforeach; ?>
                <!-- <tr>
                    <td colspan="10">
                        <hr>
                    </td>
                </tr> -->
                <tr>
                    <td colspan="2" style="border-left: 1px solid #333; border-top: 1px solid #333; border-bottom: 1px solid #333; padding: 4px;">Subtotal : <?= FormatRupiah($subtotals); ?></td>
                    <td colspan="2" style="border-top: 1px solid #333; border-bottom: 1px solid #333; padding: 4px;">Potongan : <?= FormatRupiah($potongan); ?></td>
                    <td colspan="2" style="border-top: 1px solid #333; border-bottom: 1px solid #333; padding: 4px;">DPP : <?= FormatRupiah($dpp); ?></td>
                    <td colspan="2" style="border-top: 1px solid #333; border-bottom: 1px solid #333; padding: 4px;">PPN : <?= FormatRupiah($ppn); ?></td>
                    <td colspan="2" style="border-right: 1px solid #333; border-top: 1px solid #333; border-bottom: 1px solid #333; padding: 4px;">Total : <?= FormatRupiah($grandtotal); ?></td>
                </tr>
                <!-- <tr>
                    <td colspan="10">
                        <hr>
                    </td>
                </tr> -->
                <tr>
                    <td colspan="10"><br></td>
                </tr>
            <?php endforeach; ?>
        </tbody>

    </table>
</body>