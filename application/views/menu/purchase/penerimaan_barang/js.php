<script type="text/javascript">
    $(document).ready(function() {
        var kodecabang = $('#cabang').val()
        var ppnkonf = 0;

        function RefreshLayar() {
            location.reload(true);
        };

        function LoadKonfigurasi() {
            $.ajax({
                url: "<?= base_url('purchase/PenerimaanBarang/LoadKonfigurasi') ?>",
                method: "POST",
                dataType: "json",
                async: false,
                data: {},
                success: function(data) {
                    for (var i = 0; i < data.length; i++) {
                        ppnkonf = data[i].ppn;
                    }
                }
            }, false);
        }

        function ClearScreen() {
            LoadKonfigurasi();
            $('#cancel').prop("disabled", true);
            $('#update').prop("disabled", true);
            $('#save').prop("disabled", false);

            $('#nomorpb').val("");
            $('#tanggalpb').val("<?= date('Y-m-d') ?>");
            $('#nomorpo').prop('disabled', false).val("");
            $('#tanggalpo').prop('disabled', false).val("<?= date('Y-m-d') ?>");
            $('#nomorpib').prop('disabled', false).val("");
            // $('#tanggalpib').prop('disabled', false).val("<?= date('Y-m-d') ?>");
            $('#nomorsupplier').val("");
            $('#namasupplier').val("");
            $('#kodegd').val("");
            $('#namagd').val("");
            $('#dpp').val(0);
            $('#ppn').val(0);
            $('#grandtotal').val(0);
            $('#catatan').prop('disabled', false).val("");

            $('#keterangan').val("");

            $('#kodebrg').val("");
            $('#namabrg').val("");
            $('#qty').prop('disabled', false).val(0);
            $('#price').prop('disabled', false).val(0);
            $('#disc').prop('disabled', false).val(0);
            $('#discitem').val(0);
            $('#total').val(0);

            $('#tb_detail').empty();

            $('#carisupplier').show();
            $('#carigudang').show();
            $('#caribarang').show();
            $('#adddetail').show();
        }

        function CurrentDate(input) {
            var date = new Date(input);
            var yyyy = date.getFullYear();
            var mm = date.getMonth() + 1;
            var dd = date.getDate();
            var hrs = date.getHours();
            var mnt = date.getMinutes();


            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }
            if (hrs < 10) {
                hrs = '0' + hrs;
            }
            if (mnt < 10) {
                mnt = '0' + mnt;
            }
            return yyyy + '' + mm;
        };

        $('#tanggalpo').datepicker({
            format: "yyyy-mm-dd",
            autoclose: true,
            todayHighlight: true,
            // startDate: new Date()
        });

        // $('#tanggalpib').datepicker({
        //     format: "yyyy-mm-dd",
        //     autoclose: true,
        //     todayHighlight: true,
        //     // startDate: new Date()
        // });

        /* Format Rupiah */
        function FormatRupiah(angka, prefix) {
            if (!angka) {
                return '';
            }
            var vangka = angka.toString();
            var number_string = vangka.replace(/[^.\d]/g, '').replace(/[^\w\s]/gi, '').toString(),
                split = number_string.split('.'),
                sisa = split[0].length % 3,
                rupiah = split[0].substr(0, sisa),
                ribuan = split[0].substr(sisa).match(/\d{3}/gi);

            // tambahkan titik jika yang di input sudah menjadi angka ribuan
            if (ribuan) {
                separator = sisa ? ',' : '';
                rupiah += separator + ribuan.join(',');
            }

            rupiah = split[1] != undefined ? rupiah + '.' + split[1] : rupiah;
            return prefix == undefined ? rupiah : (rupiah ? rupiah : '');
        };

        /* DeFormat Rupiah */
        function DeFormatRupiah(angka) {
            var result = angka.replace(/(\d+)\Dp/gi, '');

            return result;
        };

        function FormatDate(input) {
            var date = new Date(input);
            var yyyy = date.getFullYear();
            var mm = date.getMonth() + 1;
            var dd = date.getDate();
            var hrs = date.getHours();
            var mnt = date.getMinutes();

            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }
            if (hrs < 10) {
                hrs = '0' + hrs;
            }
            if (mnt < 10) {
                mnt = '0' + mnt;
            }
            return yyyy + '-' + mm + '-' + dd;
        };

        function getDigitToFormat(val) {
            if (val < 10) {
                val = '0' + val;
            }
            return val.toString();
        };

        function HitungSummary() {
            var table = document.getElementById('t_detail');
            var sublist = 0;
            var subtotal = 0;
            var dpp = 0;
            var ppn = 0;
            var grandtotal = 0;
            for (var r = 1, n = table.rows.length; r < n; r++) {
                for (var c = 0, m = table.rows[r].cells.length; c < m - 1; c++) {
                    sublist = parseFloat(DeFormatRupiah(table.rows[r].cells[6].innerHTML).replace(",", "").replace(",", "").replace(",", "").replace(",", "").replace(",", ""));
                }
                subtotal += parseFloat(sublist);
            }
            console.log(subtotal);
            if (subtotal > 0) {
                dpp = subtotal;
                ppn = Math.round(parseFloat(dpp) * (ppnkonf / 100));
                grandtotal = parseFloat(dpp) + parseFloat(ppn);
            }


            $('#dpp').val(FormatRupiah(dpp.toString(), ''));
            $('#ppn').val(FormatRupiah(ppn.toString(), ''));
            $('#grandtotal').val(FormatRupiah(grandtotal.toString(), ''));
        };


        // $('#disc').mask('99.99%', {
        //     reverse: true
        // });

        /* Format Rupiah Saat Event Keyup */
        $('#qty').keyup(function() {
            $('#qty').val(FormatRupiah($(this).val()));
            hitungdetail();
        })

        function hitungdetail() {
            var qty = DeFormatRupiah($('#qty').val()).replace(",", "").replace(",", "").replace(",", "").replace(",", "");
            var harga = 0;
            var discitem = $('#disc').val();
            var discpertitem = 0;
            var subtotal = 0;


            harga = DeFormatRupiah($('#price').val()).replace(",", "").replace(",", "").replace(",", "").replace(",", "").replace(",", "");
            if (discitem > 0) {
                discpertitem = parseFloat(harga) * parseFloat(discitem) / 100;
            }
            subtotal = ((parseFloat(harga) - parseFloat(discpertitem)) * parseFloat(qty));
            $('#qty').val(FormatRupiah(qty.toString()));
            $('#disc').val(FormatRupiah(discitem.toString()));
            $('#discitem').val(FormatRupiah(discpertitem.toString()));
            $('#total').val(FormatRupiah(subtotal.toString()));
        }

        $('#price').keyup(function() {
            $('#price').val(FormatRupiah($(this).val()));
            hitungdetail();
        })
        $('#disc').keyup(function() {
            $('#disc').val(FormatRupiah($(this).val()));
            hitungdetail();
        })

        $('#qty').keyup(function() {
            $('#qty').val(FormatRupiah($(this).val()));
            hitungdetail();
        })

        function BarangInputDetail() {
            $('#price').val(0)
            $('#qty').val(0);
            $('#disc').val(0);
            $('#discitem').val(0);
            $('#total').val(0);
        }


        function hitungdetailedit() {
            // console.log("A");
            var qty = DeFormatRupiah($('#chgqty').val()).replace(",", "").replace(",", "").replace(",", "").replace(",", "");
            var harga = 0;
            var discitem = $('#chgdisc').val();
            var discpertitem = 0;
            var subtotal = 0;


            harga = DeFormatRupiah($('#chgprice').val()).replace(",", "").replace(",", "").replace(",", "").replace(",", "").replace(",", "");
            if (discitem > 0) {
                discpertitem = parseFloat(harga) * parseFloat(discitem) / 100;
            }
            subtotal = ((parseFloat(harga) - parseFloat(discpertitem)) * parseFloat(qty));
            $('#chgqty').val(FormatRupiah(qty.toString()));
            $('#chgdisc').val(FormatRupiah(discitem.toString()));
            $('#chgdiscperitem').val(FormatRupiah(discpertitem.toString()));
            $('#chgsubtotal').val(FormatRupiah(subtotal.toString()));
        }
        $('#chgprice').keyup(function() {
            $('#chgprice').val(FormatRupiah($(this).val()));
            hitungdetailedit();
        })
        $('#chgdisc').keyup(function() {
            $('#chgdisc').val(FormatRupiah($(this).val()));
            hitungdetailedit();
        })

        $('#chgqty').keyup(function() {
            $('#chgqty').val(FormatRupiah($(this).val()));
            hitungdetailedit();
        })


        function ValidasiSave(datadetail) {
            var tanggalpo = $('#tanggalpo').val();
            var nomorsupplier = $('#nomorsupplier').val();
            var namasupplier = $('#namasupplier').val();
            var kodegudang = $('#kodegd').val();
            var namagudang = $('#namagd').val();
            var dpp = $('#dpp').val();
            var ppn = $('#ppn').val();
            var grandtotal = $('#grandtotal').val();
            var catatan = $('#catatan').val();

            if (tanggalpo == '' || tanggalpo == 0) {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Tanggal PO tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#tanggalpo').focus();
                var result = false;
            } else if (nomorsupplier == '' || nomorsupplier == 0) {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Nomor Supplier tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#nomorsupplier').focus();
                var result = false;
            } else if (namasupplier == '' || namasupplier == 0) {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Nama Supplier tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#namasupplier').focus();
                var result = false;
            } else if (kodegudang == '' || kodegudang == 0) {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Kode Gudang tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#kodegd').focus();
                var result = false;
            } else if (dpp == '') {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'DPP tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#dpp').focus();
                var result = false;
            } else if (ppn == '') {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'PPN tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#ppn').focus();
                var result = false;
            } else if (grandtotal == '') {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Grandtotal tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#grandtotal').focus();
                var result = false;
            }
            // else if (catatan == '' || catatan == 0) {
            //     Swal.fire({
            //         title: 'Informasi',
            //         icon: 'info',
            //         html: 'Catatan tidak boleh kosong.',
            //         showCloseButton: true,
            //         width: 350,
            //     });
            //     $('#catatan').focus();
            //     var result = false;
            // }
            else if (datadetail.length == 0) {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Data Detail tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#kodebrg').focus();
                var result = false;
            } else {
                var result = true;
            }
            return result;
        }

        /* Validasi Data Add Detail */
        function ValidasiAddDetail(kodebarang) {
            var table = document.getElementById('t_detail');
            var kodebarang = $('#kodebrg').val();
            var namabarang = $('#namabrg').val();
            var qty = $('#qty').val();
            var harga = $('#price').val();
            var total = $('#total').val();
            var diskon = $('#chgdisc').val();
            var diskonitem = $('#discitem').val();

            if (kodebarang == '' || kodebarang == 0) {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Kode Barang tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#kodebrg').focus();
                return "gagal";
            } else if (namabarang == '' || namabarang == 0) {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Nama Barang tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#namabrg').focus();
                return "gagal";
            } else if (qty == '' || qty == 0) {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Qty tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#qty').focus();
                return "gagal";
                // } else if (harga == '' || harga == 0) {
                //     Swal.fire({
                //         title: 'Informasi',
                //         icon: 'info',
                //         html: 'Harga tidak boleh kosong.',
                //         showCloseButton: true,
                //         width: 350,
                //     });
                //     $('#price').focus();
                //     return "gagal";
                // } else if (diskon == '') {
                //     Swal.fire({
                //         title: 'Informasi',
                //         icon: 'info',
                //         html: 'Diskon tidak boleh kosong.',
                //         showCloseButton: true,
                //         width: 350,
                //     });
                //     $('#diskon').focus();
                //     return "gagal";
            } else if (diskon > 100) {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Diskon tidak boleh lebih dari 100.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#diskon').focus();
                return "gagal";
            } else if (diskonitem == '') {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Diskon per Item tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#discitem').focus();
                return "gagal";
            } else if (total == '') {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Total tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#total').focus();
                return "gagal";
            }
            // else {
            //     for (var r = 1, n = table.rows.length; r < n; r++) {
            //         var string = "";
            //         for (var c = 0, m = table.rows[r].cells.length; c < m; c++) {
            //             if (c == 0) {
            //                 if (table.rows[r].cells[c].innerHTML == kodebarang) {
            //                     Swal.fire({
            //                         title: 'Informasi',
            //                         icon: 'info',
            //                         html: 'Data ini sudah diinput.',
            //                         showCloseButton: true,
            //                         width: 350,
            //                     });
            //                     return "gagal";
            //                 }
            //             }
            //         }
            //     }
            // }
            return "sukses"
        }

        function InsertDataDetail(kodebarang, namabarang, harga, qty, diskon, discitem, total, statusfind) {

            var row = "";
            if (statusfind == "find") {
                row =
                    '<tr id="' + kodebarang + '">' +
                    '<td nowrap>' + kodebarang + '</td>' +
                    '<td nowrap>' + namabarang + '</td>' +
                    '<td nowrap>' + FormatRupiah(qty) + '</td>' +
                    '<td nowrap>' + FormatRupiah(harga) + '</td>' +
                    '<td nowrap>' + diskon + '</td>' +
                    '<td nowrap>' + FormatRupiah(discitem) + '</td>' +
                    '<td nowrap>' + FormatRupiah(total) + '</td>' +
					'<td nowrap style="text-align: center; width: 200px;">' +
                    '<button data-table="' + kodebarang + '" data-toggle="modal" data-target="#changeharga" class="editdetail btn btn-sm btn-success m-1 p-1"><i class="fas fa-edit"></i> Edit</button>' +
                    '<button data-table="' + kodebarang + '" class="deldetail btn btn-sm btn-danger m-1 p-1"><i class="fa fa-trash mr-1"></i> Delete</button>' +
                    '</td>' +
                    '</tr>';
            } else {
                row =
                    '<tr id="' + kodebarang + '">' +
                    '<td nowrap>' + kodebarang + '</td>' +
                    '<td nowrap>' + namabarang + '</td>' +
                    '<td nowrap>' + FormatRupiah(qty) + '</td>' +
                    '<td nowrap>' + FormatRupiah(harga) + '</td>' +
                    '<td nowrap>' + diskon + '</td>' +
                    '<td nowrap>' + FormatRupiah(discitem) + '</td>' +
                    '<td nowrap>' + FormatRupiah(total) + '</td>' +
                    '<td nowrap style="text-align: center; width: 200px;">' +
                    '<button data-table="' + kodebarang + '" data-toggle="modal" data-target="#changeharga" class="editdetail btn btn-sm btn-success m-1 p-1"><i class="fas fa-edit"></i> Edit</button>' +
                    '<button data-table="' + kodebarang + '" class="deldetail btn btn-sm btn-danger m-1 p-1"><i class="fa fa-trash mr-1"></i> Delete</button>' +
                    '</td>' +
                    '</tr>';
            }

            $('#tb_detail').append(row);
            HitungSummary();
        };

        /* Declare Toast */
        const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000,
            timerProgressBar: true,
            didOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
            }
        });

        /* Cari Data Supplier*/
        document.getElementById("carisupplier").addEventListener("click", function(event) {
            event.preventDefault();
            $('#t_supplier').DataTable({
                "destroy": true,
                "searching": true,
                "processing": true,
                "serverSide": true,
                "lengthChange": true,
                "pageLength": 5,
                "lengthMenu": [5, 10, 25, 50],
                "order": [],
                "ajax": {
                    "url": "<?= base_url('masterdata/Supplier/CariDataSupplier'); ?>",
                    "method": "POST",
                    "data": {
                        nmtb: "glbm_supplier",
                        field: {
                            nomor: "nomor",
                            nama: "nama",
                            alamat: "alamat",
                            statuspkp: "statuspkp",
                            npwp: "npwp",
                            alamatnpwp: "alamatnpwp",
                            notlp: "notlp"
                        },
                        sort: "nomor",
                        where: {
                            nomor: "nomor",
                            nama: "nama",
                            alamat: "alamat",
                            npwp: "npwp",
                            alamatnpwp: "alamatnpwp"
                        },
                        // value: "aktif = true AND kodecabang = '" + $('#cabang').val() + "'"
                        value: "aktif = true"
                    },
                },
                "columnDefs": [{
                    "targets": 4,
                    "data": "statuspkp",
                    "render": function(data, type, row, meta) {
                        return (row[4] == 't') ? 'Ya' : 'Tidak';
                    }
                }],
            });
        });

        /*Get Data Supplier*/
        $(document).on('click', ".searchsupplier", function() {
            var nomor = $(this).attr("data-id");
            $.ajax({
                url: "<?= base_url('masterdata/Supplier/DataSupplier'); ?>",
                method: "POST",
                dataType: "json",
                async: false,
                data: {
                    nomor: nomor
                },
                success: function(data) {
                    for (var i = 0; i < data.length; i++) {
                        $('#nomorsupplier').val(data[i].nomor.trim());
                        $('#namasupplier').val(data[i].nama.trim());
                    }
                }
            }, false);
        });

        /* Cari Data Gudang*/
        document.getElementById("carigudang").addEventListener("click", function(event) {
            event.preventDefault();
            $('#t_gudang').DataTable({
                "destroy": true,
                "searching": true,
                "processing": true,
                "serverSide": true,
                "lengthChange": true,
                "pageLength": 5,
                "lengthMenu": [5, 10, 25, 50],
                "order": [],
                "ajax": {
                    "url": "<?= base_url('masterdata/Gudang/CariDataGudang'); ?>",
                    "method": "POST",
                    "data": {
                        nmtb: "glbm_gudang",
                        field: {
                            kode: "kode",
                            nama: "nama",
                            alamat: "alamat",
                            kodecabang: "kodecabang"
                        },
                        sort: "kode",
                        where: {
                            kode: "kode"
                        },
                        // value: "aktif = true"
                        value: "aktif = true and kodecabang = '" + kodecabang + "'"
                    },
                }
            });
        });

        /*Get Data Gudang*/
        $(document).on('click', ".searchgudang", function() {
            var kode = $(this).attr("data-id");
            DataGudang(kode);
        });

        function DataGudang(kode) {
            $.ajax({
                url: "<?= base_url('masterdata/Gudang/DataGudang'); ?>",
                method: "POST",
                dataType: "json",
                async: false,
                data: {
                    kode: kode
                },
                success: function(data) {
                    for (var i = 0; i < data.length; i++) {
                        $('#kodegd').val(data[i].kode.trim());
                        $('#namagd').val(data[i].nama.trim());
                    }
                }
            }, false);
        }

        document.getElementById("caribarang").addEventListener("click", function(event) {
            event.preventDefault();
            // var kodegudang = $('#kodegd').val(); 
            var currentDate = CurrentDate(new Date());
            $('#t_barang').DataTable({
                "destroy": true,
                "searching": true,
                "processing": true,
                "serverSide": true,
                "lengthChange": true,
                "pageLength": 5,
                "lengthMenu": [5, 10, 25, 50],
                "order": [],
                "ajax": {
                    "url": "<?= base_url('masterdata/Barang/CariDataBarang'); ?>",
                    "method": "POST",
                    "data": {
                        nmtb: "cari_brg",
                        field: {
                            kodebarang: "kodebarang",
                            namabarang: "namabarang",
                            namasatuan: "namasatuan",
                            namamerk: "namamerk",
                            // lokasi: "lokasi"
                        },
                        sort: "kodebarang",
                        where: {
                            kodebarang: "kodebarang",
                            namabarang: "namabarang",
                            namasatuan: "namasatuan",
                            namamerk: "namamerk",
                            // lokasi: "lokasi"
                        },
                        value: "aktif = true"
                        // value: "aktif = true AND kodecabang = '" + $('#cabang').val() + "' AND kodegudang = '" + $('#kodegd').val() + "'"
                    },
                }
            });
        });

        $(document).on('click', ".searchbarang", function() {
            var kode = $(this).attr("data-id");
            BarangInputDetail();
            $.ajax({
                url: "<?= base_url('masterdata/Barang/DataBarang'); ?>",
                method: "POST",
                dataType: "json",
                async: false,
                data: {
                    kode: kode
                },
                success: function(data) {
                    for (var i = 0; i < data.length; i++) {
                        $('#kodebrg').val(data[i].kode.trim());
                        $('#namabrg').val(data[i].nama.trim());
                        // $('#price').val(0);
                    }
                }
            });
        });

        $("#adddetail").click(function() {
            var kodebarang = $('#kodebrg').val();
            var namabarang = $('#namabrg').val();
            var qty = $('#qty').val();
            var harga = $('#price').val();
            var diskon = $('#disc').val();
            var diskonitem = $('#discitem').val();
            var total = $('#total').val();
            if (ValidasiAddDetail(kodebarang) == "sukses") {
                var row = "";
                row =
                    '<tr id="' + kodebarang + '">' +
                    '<td nowrap>' + kodebarang + '</td>' +
                    '<td nowrap>' + namabarang + '</td>' +
                    '<td nowrap>' + qty + '</td>' +
                    '<td nowrap>' + harga + '</td>' +
                    '<td nowrap>' + diskon + '</td>' +
                    '<td nowrap>' + diskonitem + '</td>' +
                    '<td nowrap>' + total + '</td>' +
                    '<td nowrap style="text-align: center; width: 200px;">' +
                    '<button data-table="' + kodebarang + '" data-toggle="modal" data-target="#changeharga" class="editdetail btn btn-sm btn-sm btn-success m-1 p-1"><i class="fas fa-edit"></i> Edit</button>' +
                    '<button data-table="' + kodebarang + '" class="deldetail btn btn-sm btn-sm btn-danger m-1 p-1"><i class="fa fa-trash mr-1"></i> Delete</button>' +
                    '</td>' +
                    '</tr>';
                $('#tb_detail').append(row);
                HitungSummary();
                $('#kodebrg').val("");
                $('#namabrg').val("");
                $('#qty').val(0);
                $('#price').val(0);
                $('#disc').val(0);
                $('#discitem').val(0);
                $('#total').val(0);
            }
        });

        $(document).on('click', '.editdetail', function() {
            var table = document.getElementById('t_detail');
            var tr = document.getElementById($(this).attr("data-table"));
            var td = tr.getElementsByTagName("td");
            for (var c = 0, m = table.rows[0].cells.length; c < m - 1; c++) {
                var chgth = table.rows[0].cells[c].innerHTML.replace(/ /g, '').replace(/\s/g, '').toLowerCase();
                var valuetd = td[c].innerHTML;
                $('#chg' + chgth).val(valuetd);
            }
        });

        $(document).on('click', '#changeharga', function() {
            // var kode = $('#chgkode').val(); 
            var kodebarang = $('#chgkode').val();
            var namabarang = $('#chgnama').val();
            var harga = $('#chgprice').val();
            var qty = $('#chgqty').val();
            var diskon = $('#chgdisc').val();
            var discitem = $('#chgdiscperitem').val();
            var total = $('#chgsubtotal').val();
            $('#' + kodebarang).remove();
            InsertDataDetail(kodebarang, namabarang, harga, qty, diskon, discitem, total, "");
        });

        $(document).on('click', '.deldetail', function() {
            var id = $(this).attr("data-table");
            $('#' + id).remove();
            HitungSummary();
        });

        function AmbilDataDetail() {
            var table = document.getElementById('t_detail');
            var arr2 = [];
            for (var r = 1, n = table.rows.length; r < n; r++) {
                var string = "";
                for (var c = 0, m = table.rows[r].cells.length; c < m - 1; c++) {
                    if (c == 0) {
                        string = "{" + table.rows[0].cells[c].innerHTML + " : '" + table.rows[r].cells[c].innerHTML + "'";
                    } else {
                        string = string + ", " + table.rows[0].cells[c].innerHTML + " : '" + table.rows[r].cells[c].innerHTML + "'";
                    }
                }
                string = string + "}";
                var obj = JSON.stringify(eval('(' + string + ')'));
                var arr = $.parseJSON(obj);
                arr2.push(arr);
            }
            return arr2;
        };

        document.getElementById("clear").addEventListener("click", function(event) {
            event.preventDefault();
            ClearScreen();
        })

        /* Save */
        document.getElementById('save').addEventListener("click", function(event) {
            event.preventDefault();
            var nomorpb = $('#nomorpb').val();
            var tanggalpb = $('#tanggalpb').val();
            var nomorpo = $('#nomorpo').val();
            var tanggalpo = $('#tanggalpo').val();
            var nomorpib = $('#nomorpib').val();
            // var tanggalpib = $('#tanggalpib').val();
            var nomorsupplier = $('#nomorsupplier').val();
            var namasupplier = $('#namasupplier').val();
            var kodegudang = $('#kodegd').val();
            var namagudang = $('#namagd').val();
            var dpp = $('#dpp').val();
            var ppn = $('#ppn').val();
            var grandtotal = $('#grandtotal').val();
            var catatan = $('#catatan').val();

            var datadetail = AmbilDataDetail();

            if (ValidasiSave(datadetail) == true) {
                $.ajax({
                    url: "<?= base_url("purchase/PenerimaanBarang/Save") ?>",
                    method: "POST",
                    dataType: "json",
                    async: true,
                    data: {
                        tanggalpb: tanggalpb,
                        nomorpo: nomorpo,
                        tanggalpo: tanggalpo,
                        nomorpib: nomorpib,
                        // tanggalpib: tanggalpib,
                        nomorsupplier: nomorsupplier,
                        namasupplier: namasupplier,
                        kodegudang: kodegudang,
                        namagudang: namagudang,
                        catatan: catatan,
                        dpp: dpp,
                        ppn: ppn,
                        grandtotal: grandtotal,
                        datadetail: datadetail,
                        kodecabang: kodecabang,
                        // namagudang: namagudang
                    },
                    success: function(data) {
                        if (data.nomor != "") {
                            Toast.fire({
                                icon: 'success',
                                title: data.message
                            });
                            $('#nomorpb').val(data.nomor);
                            $('#tanggalpb').prop('disabled', true);
                            $('#nomorpo').prop('disabled', true);
                            $('#tanggalpo').prop('disabled', true);
                            $('#nomorsupplier').prop('disabled', true);
                            $('#namasupplier').prop('disabled', true);
                            $('#kodegd').prop('disabled', true);
                            $('#namagd').prop('disabled', true);
                            $('#dpp').prop('disabled', true);
                            $('#ppn').prop('disabled', true);
                            $('#grandtotal').prop('disabled', true);
                            $('#catatan').prop('disabled', true);

                            $('#kodebrg').prop('disabled', true);
                            $('#namabrg').prop('disabled', true);
                            $('#qty').prop('disabled', true);
                            $('#price').prop('disabled', true);
                            $('#disc').prop('disabled', true);
                            $('#discitem').prop('disabled', true);
                            $('#total').prop('disabled', true);

                            $('#carisupplier').hide();
                            $('#carigudang').hide();
                            $('#caribarang').hide();
                            $('#adddetail').hide();
                            $('#cancel').prop("disabled", false);
                            $('#save').prop("disabled", true);
                        } else {
                            Toast.fire({
                                icon: 'error',
                                title: data.message
                            });
                        }
                    }
                }, false)
            }
        })

        /*Cari Data Penerimaan Barang*/
        document.getElementById("find").addEventListener("click", function(event) {
            event.preventDefault();
            $('#tb_detail').empty();
            $('#t_penerimaanbarang').DataTable({
                "destroy": true,
                "searching": true,
                "processing": true,
                "serverSide": true,
                "lengthChange": true,
                "pageLength": 5,
                "lengthMenu": [5, 10, 25, 50],
                "order": [],
                "ajax": {
                    "url": "<?= base_url('purchase/PenerimaanBarang/CariDataPenerimaanBarang'); ?>",
                    "method": "POST",
                    "data": {
                        nmtb: "srvt_penerimaanbarang",
                        field: {
                            nomor: "nomor",
                            tanggal: "tanggal",
                            nomorpo: "nomorpo",
                            tglpo: "tglpo",
                            nosupplier: "nosupplier",
                            namasupplier: "namasupplier",
                            kodegudang: "kodegudang",
                            // namagudang: "namagudang"
                        },
                        sort: "nomor",
                        where: {
                            nomor: "nomor",
                            nomorpo: "nomorpo",
                            nosupplier: "nosupplier",
                            namasupplier: "namasupplier",
                            kodegudang: "kodegudang",
                            // namagudang: "namagudang"
                        },
                        value: "batal = false AND kodecabang = '" + kodecabang + "'"
                    },
                },
                "columnDefs": [{
                    "targets": 2,
                    "render": $.fn.dataTable.render.moment('YYYY-MM-DD HH:mm:ss', 'D MMM YY'),
                }, {
                    "targets": 4,
                    "render": $.fn.dataTable.render.moment('YYYY-MM-DD HH:mm:ss', 'D MMM YY'),
                }]
            });
        })

        $(document).on('click', ".searchpb", function() {
            var nomor = $(this).attr("data-id");
            GetData(nomor);
            $('#save').prop('disabled', true);
            $('#update').prop('disabled', false);
        });

        function GetData(nomor) {
            $.ajax({
                url: "<?= base_url('purchase/PenerimaanBarang/DataPenerimaanBarang'); ?>",
                method: "POST",
                dataType: "json",
                async: false,
                data: {
                    nomor: nomor
                },
                success: function(data) {
                    for (var i = 0; i < data.length; i++) {
                        $('#nomorpb').val(data[i].nomor.trim());
                        $('#tanggalpb').val(FormatDate(data[i].tanggal.trim()));
                        $('#nomorpo').val(data[i].nomorpo.trim());
                        $('#tanggalpo').val(FormatDate(data[i].tglpo.trim()));
                        $('#nomorpib').val(data[i].nomorpib.trim());
                        // $('#tanggalpib').val(FormatDate(data[i].tglpib.trim()));
                        $('#nomorsupplier').val(data[i].nosupplier.trim());
                        $('#namasupplier').val(data[i].namasupplier.trim());
                        $('#kodegd').val(data[i].kodegudang.trim());
                        DataGudang(data[i].kodegudang.trim());
                        // $('#namagd').val(data[i].namagudang.trim());
                        $('#catatan').val(data[i].catatan.trim());
                        $('#dpp').val(FormatRupiah(data[i].dpp.trim()));
                        $('#ppn').val(FormatRupiah(data[i].ppn.trim()));
                        $('#grandtotal').val(FormatRupiah(data[i].total.trim()));
                    }
                    GetDataDetail(nomor);
                    $('#cancel').prop('disabled', false);
                    $('#save').prop('disabled', true);
                }
            });
        };

        function GetDataDetail(nomor) {
            $.ajax({
                url: "<?= base_url('purchase/PenerimaanBarang/DataPenerimaanBarangDetail'); ?>",
                method: "POST",
                dataType: "json",
                async: false,
                data: {
                    nomor: nomor
                },
                success: function(data) {
                    for (var i = 0; i < data.length; i++) {
                        // var kode = data[i].kode;
                        var kodebarang = data[i].kodebarang;
                        var namabarang = data[i].namabarang.trim();
                        var harga = data[i].harga.trim();
                        var qty = data[i].qty.trim();
                        var diskon = data[i].disc.trim();
                        var discitem = data[i].discitem.trim();
                        var total = data[i].total.trim();

                        InsertDataDetail(kodebarang, namabarang, harga, qty, diskon, discitem, total, "find");
                    }
                }
            });
        };

        /* Update */
        document.getElementById('update').addEventListener("click", function(event) {
            event.preventDefault();
            var nomorpb = $('#nomorpb').val();
            // var tanggalpb = $('#tanggalpb').val();
            // var nomorpo = $('#nomorpo').val();
            // var tanggalpo = $('#tanggalpo').val();
            var nomorpib = $('#nomorpib').val();
            // var tanggalpib = $('#tanggalpib').val();
            var nomorsupplier = $('#nomorsupplier').val();
            var namasupplier = $('#namasupplier').val();
            var kodegudang = $('#kodegd').val();
            // var namagudang = $('#namagd').val();
            // var dpp = $('#dpp').val();
            // var ppn = $('#ppn').val();
            // var grandtotal = $('#grandtotal').val();
            // var catatan = $('#catatan').val();

            var datadetail = AmbilDataDetail();

            // if (ValidasiSave(datadetail) == true) {
            $.ajax({
                url: "<?= base_url("purchase/PenerimaanBarang/Update") ?>",
                method: "POST",
                dataType: "json",
                async: true,
                data: {
                    nomorpb: nomorpb,
                    // nomorpo: nomorpo,
                    // tanggalpo: tanggalpo,
                    nomorpib: nomorpib,
                    // tanggalpib: tanggalpib,
                    nomorsupplier: nomorsupplier,
                    namasupplier: namasupplier,
                    kodegudang: kodegudang,
                    // namagudang: namagudang,
                    // catatan: catatan,
                    // dpp: dpp,
                    // ppn: ppn,
                    // grandtotal: grandtotal,
                    datadetail: datadetail,
                    kodecabang: kodecabang,
                    // namagudang: namagudang
                },
                success: function(data) {
                    if (data.nomor != "") {
                        Toast.fire({
                            icon: 'success',
                            title: data.message
                        });
                        $('#cancel').prop("disabled", false);
                        $('#save').prop("disabled", true);
                    } else {
                        Toast.fire({
                            icon: 'error',
                            title: data.message
                        });
                    }
                }
            }, false)
            // }
        })

        document.getElementById('okcancel').addEventListener("click", function(event) {
            event.preventDefault();
            var nomorpb = $('#nomorpb').val();
            var keterangan = $('#keteranganbatal').val();
            var datadetail = AmbilDataDetail();
            var kodegudang = $('#kodegd').val();
            if (nomorpb == "") {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Pilih Dulu Nomor Yang Ingin dibatalkan',
                    showCloseButton: true,
                    width: 350,
                });
            } else if (keterangan == "") {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Keterangan Batal Tidak Boleh Kosong',
                    showCloseButton: true,
                    width: 350,
                });
            } else {
                $.ajax({
                    url: "<?= base_url("purchase/PenerimaanBarang/Cancel") ?>",
                    method: "POST",
                    dataType: "json",
                    async: true,
                    data: {
                        nomorpb: nomorpb,
                        kodegudang: kodegudang,
                        kodecabang: kodecabang,
                        keterangan: keterangan,
                        datadetail: datadetail
                    },
                    success: function(data) {
                        console.log(data);
                        if (data.nomor != "") {
                            Toast.fire({
                                icon: 'success',
                                title: data.message
                            });
                            RefreshLayar();
                        } else {
                            Toast.fire({
                                icon: 'error',
                                title: data.message
                            });
                        }
                    }
                }, false)
            }
        })

        ClearScreen();
    });
</script>
