<div class="col-xl-12 mb-2">
	<div class="modal-header" style="padding: 5px;">
		<div class="col-xl-12">
			<div class="row">
				<div class="col-md-4">
					<h2>
						<span class="logo-menu"><?= $icons ?></span>
						<span class="text-uppercase"><?= $title ?></span>
					</h2>
				</div>
				<div class="col-md-8">
					<div class="form-group" style="float: right;">
						<button id="save" class="btn btn-sm btn-dark mb-1 mt-1 mr-1 ml-1" style="margin-bottom: 0px;"><i class="far fa-save" style="margin-right: 10px;"></i>Save</button>
						<button id="update" class="btn btn-sm btn-dark mb-1 mt-1 mr-1 ml-1" style="margin-bottom: 0px;"><i class="far fa-money-check-edit" style="margin-right: 10px;"></i>Update</button>
						<button data-toggle="modal" data-target="#findpenerimaanbarang" id="find" class="btn btn-sm btn-dark mb-1 mt-1 mr-1 ml-1" style="margin-bottom: 0px;"><i class="far fa-file-search" style="margin-right: 10px;"></i>Find</button>
						<button id="cancel" class="btn btn-sm btn-danger m-1" data-toggle="modal" data-target="#modalcancel"><i class="far fa-times-circle" style="margin-right: 10px;"></i>Cancel</button>
						<button id="clear" class="btn btn-sm btn-dark mb-1 mt-1 mr-1 ml-1" style="margin-bottom: 0px;"><i class="far fa-refresh" style="margin-right: 10px;"></i>Clear</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="col-md-12 table-responsive hidescroll" style="height: 555px; overflow-y: scroll;">
	<div class="row">
		<div class="col-12 col-md-12 col-lg-12 col-xl-6">
			<div class="modal-content mb-2">
				<div class="modal-header p-2">
					<h5 class="mb-0">DATA DOKUMEN</h5>
				</div>
				<div class="modal-body">
					<div class="row mb-2 mt-2">
						<div class="col-sm-6 input-group">
							<div class="col-sm-6 p-0 input-group-prepend">
								<span class="col-sm-12 input-group-text">Nomor PB</span>
							</div>
							<input class="form-control" type="text" name="nomorpb" id="nomorpb" maxlength="50" placeholder="Nomor PB" readonly required />
						</div>
						<div class="col-sm-6 input-group box">
							<div class="col-sm-6 p-0 input-group-prepend">
								<span class="col-sm-12 input-group-text">Tanggal PB</span>
							</div>
							<input class="form-control" type="text" name="tanggalpb" id="tanggalpb" maxlength="50" value="<?= date('Y-m-d') ?>" placeholder="Tanggal PB" readonly required />
							<div class="input-group-text btn-dark">
								<span class="input-group-addon">
									<i class="fa fa-calendar-alt text-white"></i>
								</span>
							</div>
						</div>
					</div>
					<div class="row mb-2">
						<div class="col-sm-6 input-group">
							<div class="col-sm-6 p-0 input-group-prepend">
								<span class="col-sm-12 input-group-text">Nomor PO</span>
							</div>
							<input class="form-control" type="text" name="nomorpo" id="nomorpo" maxlength="50" placeholder="Nomor PO" required />
							<!-- <div class="input-group-text btn-dark" data-toggle="modal" data-target="#findso" id="cariso">
								<span class="input-group-addon">
									<i class="fa fa-search text-white"></i>
								</span>
							</div> -->
						</div>
						<div class="col-sm-6 input-group box">
							<div class="col-sm-6 p-0 input-group-prepend">
								<span class="col-sm-12 input-group-text">Tanggal PO</span>
							</div>
							<input class="form-control" type="text" name="tanggalpo" id="tanggalpo" maxlength="50" value="<?= date('Y-m-d') ?>" required />
							<div class="input-group-text btn-dark">
								<span class="input-group-addon">
									<i class="fa fa-calendar-alt text-white"></i>
								</span>
							</div>
						</div>
					</div>
					<div class="row mb-2">
						<div class="col-sm-12 input-group">
							<div class="col-sm-3 p-0 input-group-prepend">
								<span class="col-sm-12 input-group-text">Nomor PIB</span>
							</div>
							<input class="form-control" type="text" name="nomorpib" id="nomorpib" maxlength="50" placeholder="Nomor PIB" required />
							<!-- <div class="input-group-text btn-dark" data-toggle="modal" data-target="#findso" id="cariso">
								<span class="input-group-addon">
									<i class="fa fa-search text-white"></i>
								</span>
							</div> -->
						</div>
						<!-- <div class="col-sm-6 input-group box">
							<div class="col-sm-6 p-0 input-group-prepend">
								<span class="col-sm-12 input-group-text">Tanggal PIB</span>
							</div>
							<input class="form-control" type="text" name="tanggalpib" id="tanggalpib" maxlength="50" value="<?= date('Y-m-d') ?>" required />
							<div class="input-group-text btn-dark">
								<span class="input-group-addon">
									<i class="fa fa-calendar-alt text-white"></i>
								</span>
							</div>
						</div> -->
					</div>
					<div class="row mb-2">
						<div class="col-sm-6 input-group">
							<div class="col-sm-6 p-0 input-group-prepend">
								<span class="col-sm-12 input-group-text">Nomor Supplier</span>
							</div>
							<input class="form-control" type="text" name="nomorsupplier" id="nomorsupplier" maxlength="50" placeholder="Nomor Supplier" readonly required />
							<div class="input-group-text btn-dark" data-toggle="modal" data-target="#findsupplier" id="carisupplier">
								<span class="input-group-addon">
									<i class="fa fa-search text-white"></i>
								</span>
							</div>
						</div>
						<div class="col-sm-6 input-group">
							<div class="col-sm-6 p-0 input-group-prepend">
								<span class="col-sm-12 input-group-text">Nama Supplier</span>
							</div>
							<input class="form-control" type="text" name="namasupplier" id="namasupplier" maxlength="50" placeholder="Nama Supplier" readonly required />
							<input class="form-control" type="hidden" name="cabang" id="cabang" maxlength="50" value="<?php echo $this->session->userdata('mycabang') ?>" readonly required />
						</div>
					</div>
					<div class="row mb-2">
						<div class="col-sm-6 input-group box">
							<div class="col-sm-6 p-0 input-group-prepend">
								<span class="col-sm-12 input-group-text">Kode Gudang</span>
							</div>
							<input class="form-control" type="text" name="kodegd" id="kodegd" maxlength="50" placeholder="Kode Gudang" readonly required />
							<div class="input-group-text btn-dark" data-toggle="modal" data-target="#findgudang" id="carigudang">
								<span class="input-group-addon">
									<i class="fa fa-search text-white"></i>
								</span>
							</div>
						</div>
						<div class="col-sm-6 input-group box">
							<div class="col-sm-6 p-0 input-group-prepend">
								<span class="col-sm-12 input-group-text">Nama Gudang</span>
							</div>
							<input class="form-control" type="text" name="namagd" id="namagd" maxlength="50" placeholder="Nama Gudang" readonly required />
						</div>
					</div>
					<div class="row mb-2">
						<div class="col-sm-12 input-group">
							<div class="col-sm-3 p-0 input-group-prepend">
								<span class="col-sm-12 input-group-text">Catatan</span>
							</div>
							<textarea name="catatan" id="catatan" rows="1" class="form-control" placeholder="Catatan"></textarea>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-12 col-md-12 col-lg-12 col-xl-6">
			<div class="modal-content mb-2">
				<div class="modal-body">
					<div class="row mb-2 mt-2">
						<div class="col-sm-6 input-group">
							<div class="col-sm-6 p-0 input-group-prepend">
								<span class="col-sm-12 input-group-text">DPP</span>
							</div>
							<input class="form-control" type="text" name="dpp" id="dpp" maxlength="50" placeholder="0" readonly required />
						</div>
					</div>
					<div class="row mb-2">
						<div class="col-sm-6 input-group">
							<div class="col-sm-6 p-0 input-group-prepend">
								<span class="col-sm-12 input-group-text">PPN</span>
							</div>
							<input class="form-control" type="text" name="ppn" id="ppn" maxlength="50" placeholder="0" readonly required />
						</div>
					</div>
					<div class="row mb-2">
						<div class="col-sm-6 input-group">
							<div class="col-sm-6 p-0 input-group-prepend">
								<span class="col-sm-12 input-group-text">Grand Total</span>
							</div>
							<input class="form-control" type="text" name="grandtotal" id="grandtotal" maxlength="50" placeholder="0" readonly required />
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-12 col-md-12 col-lg-12 col-xl-12">
			<div class="modal-content mb-2">
				<div class="modal-header p-2">
					<h5 class="mb-0">DATA DETAIL DOKUMEN</h5>
				</div>
				<div class="modal-body">
					<div class="row mb-2 mt-2">
						<div class="col-sm-6 input-group">
							<div class="col-sm-6 p-0 input-group-prepend">
								<span class="col-sm-12 input-group-text">Kode Barang</span>
							</div>
							<input class="form-control" type="text" name="kodebrg" id="kodebrg" maxlength="50" placeholder="Kode Barang" readonly required />
							<div class="input-group-text btn-dark" data-toggle="modal" data-target="#findbarang" id="caribarang">
								<span class="input-group-addon">
									<i class="fa fa-search text-white"></i>
								</span>
							</div>
						</div>
						<div class="col-sm-6 input-group box">
							<div class="col-sm-6 p-0 input-group-prepend">
								<span class="col-sm-12 input-group-text">Nama Barang</span>
							</div>
							<input class="form-control" type="text" name="namabrg" id="namabrg" maxlength="50" placeholder="Nama Barang" required readonly />
						</div>
					</div>
					<div class="row mb-2">
						<div class="col-sm-6 input-group box">
							<div class="col-sm-6 p-0 input-group-prepend">
								<span class="col-sm-12 input-group-text">Quantity</span>
							</div>
							<input class="form-control" type="text" name="qty" id="qty" maxlength="10" placeholder="0" required />
						</div>
						<div class="col-sm-6 input-group">
							<div class="col-sm-6 p-0 input-group-prepend">
								<span class="col-sm-12 input-group-text">Price</span>
							</div>
							<input class="form-control" type="text" name="price" id="price" maxlength="50" placeholder="0" required />
						</div>
					</div>
					<div class="row mb-2">
						<div class="col-sm-6 input-group box">
							<div class="col-sm-6 p-0 input-group-prepend">
								<span class="col-sm-12 input-group-text">Discount(%)</span>
							</div>
							<input class="form-control" type="text" name="discs" id="disc" max="9999" placeholder="0" required />
						</div>
						<div class="col-sm-6 input-group">
							<div class="col-sm-6 p-0 input-group-prepend">
								<span class="col-sm-12 input-group-text">Discount/Item</span>
							</div>
							<input class="form-control" type="text" name="discitem" id="discitem" maxlength="50" placeholder="0" readonly required />
						</div>
					</div>
					<div class="row mb-2">
						<div class="col-sm-6 input-group box">
							<div class="col-sm-6 p-0 input-group-prepend">
								<span class="col-sm-12 input-group-text">Total Price</span>
							</div>
							<input class="form-control" type="text" name="total" id="total" maxlength="50" placeholder="0" readonly required />
							<div class="input-group-text btn-dark" id="adddetail">
								<span class="input-group-addon">
									<i class="far fa-plus text-white"></i>
								</span>
							</div>
						</div>
					</div>
					<div class="row my-3">
						<div class="table-responsive">
							<table id="t_detail" class="table table-bordered table-striped table-default mb-0 dt-responsive display nowrap" style="width:100%;">
								<thead style="background-color: #68478D; color: #eee;">
									<tr style="line-height: 0.5 cm; ">
										<th style="text-align: center; ">
											Kode
										</th>
										<th style="text-align: center; ">
											Nama
										</th>
										<th style="text-align: center; ">
											Qty
										</th>
										<th style="text-align: center; ">
											Price
										</th>
										<th style="text-align: center; ">
											Disc
										</th>
										<th>
											DiscPerItem
										</th>
										<th style="text-align: center; ">
											Subtotal
										</th>
										<th style="text-align: center; ">
											Action
										</th>
									</tr>
								</thead>
								<tbody class="tbody-scroll scroller" id="tb_detail"></tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="changeharga">
	<div class="modal-dialog modal-md">
		<div class="modal-content" style="border-radius: 5px;">
			<div class="modal-header" style="padding: 10px; color: #000;">
				<h7 class="modal-title" style="margin-left: 5px;">UBAH DETAIL</h7>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="row mb-2">
					<div class="col-sm-6 input-group">
						<div class="col-sm-6 p-0 input-group-prepend">
							<span class="col-sm-12 input-group-text">Kode</span>
						</div>
						<input class="form-control" type="text" name="chgkode" id="chgkode" maxlength="50" placeholder="Kode" readonly required />
					</div>
					<div class="col-sm-6 input-group box">
						<div class="col-sm-6 p-0 input-group-prepend">
							<span class="col-sm-12 input-group-text">Nama Barang</span>
						</div>
						<input class="form-control" type="text" name="chgnama" id="chgnama" maxlength="50" placeholder="Nama Barang" required readonly />
					</div>
				</div>
				<div class="row mb-2">
					<div class="col-sm-6 input-group">
						<div class="col-sm-6 p-0 input-group-prepend">
							<span class="col-sm-6 input-group-text">Qty</span>
						</div>
						<input class="form-control" type="text" name="chgqty" id="chgqty" maxlength="10" placeholder="Qty" required />
					</div>
					<div class="col-sm-6 input-group">
						<div class="col-sm-6 p-0 input-group-prepend">
							<span class="col-sm-6 input-group-text">Harga</span>
						</div>
						<input class="form-control" type="text" name="chgprice" id="chgprice" maxlength="50" placeholder="Harga" required />
					</div>
				</div>
				<div class="row mb-2">
					<div class="col-sm-6 input-group">
						<div class="col-sm-6 p-0 input-group-prepend">
							<span class="col-sm-6 input-group-text">Diskon</span>
						</div>
						<input class="form-control" type="text" name="chgdisc" id="chgdisc" max="5" placeholder="Diskon" required />
					</div>
					<div class="col-sm-6 input-group">
						<div class="col-sm-6 p-0 input-group-prepend">
							<span class="col-sm-6 input-group-text">Disc. /Item</span>
						</div>
						<input class="form-control" type="text" name="chgdiscperitem" id="chgdiscperitem" maxlength="50" placeholder="Disc. /Item" required />
					</div>
				</div>
				<div class="row mb-2">
					<div class="col-sm-12 input-group">
						<div class="col-sm-3 p-0 input-group-prepend">
							<span class="col-sm-12 input-group-text">Total</span>
						</div>
						<input class="form-control" type="text" name="chgsubtotal" id="chgsubtotal" maxlength="50" placeholder="Total" readonly required />
						<div class="input-group-text btn-dark" id="changedetail" data-dismiss="modal">
							<span class="input-group-addon">
								<i class="fa fa-random" style="color: #fff;"></i>
							</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="findpenerimaanbarang">
	<div class="modal-dialog modal-lg">
		<div class="modal-content" style="border-radius: 5px;">
			<div class="modal-header" style="padding: 10px; color: #000;">
				<h7 class="modal-title" style="margin-left: 5px;">Data Penerimaan Barang</h7>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="table-responsive">
					<table id="t_penerimaanbarang" class="table table-bordered table-striped dt-responsive display nowrap" style="width:100%">
						<thead class="thead-dark">
							<tr>
								<th width="10">Action</th>
								<th width="150">Nomor PB</th>
								<th width="150">Tanggal PB</th>
								<th width="150">Nomor PO</th>
								<th width="150">Tanggal PO</th>
								<th width="150">No. Supplier</th>
								<th width="150">Nama Supplier</th>
								<th width="150">Kode Gudang</th>
								<!-- <th width="150">Nama Gudang</th> -->
							</tr>
						</thead>
						<tbody></tbody>

					</table>

				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="findbarang">
	<div class="modal-dialog modal-lg">
		<div class="modal-content" style="border-radius: 5px;">
			<div class="modal-header" style="padding: 10px; color: #000;">
				<h7 class="modal-title" style="margin-left: 5px;">Data Barang</h7>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="table-responsive">
					<table id="t_barang" class="table table-bordered table-striped dt-responsive display nowrap" style="width:100%">
						<thead class="thead-dark">
							<tr>
								<th width="10">Action</th>
								<th width="150">Kode</th>
								<th width="150">Nama</th>
								<th width="150">Satuan</th>
								<th width="150">Merk</th>
								<!-- <th width="150">Barcode 1</th>
								<th width="150">Barcode 2</th> -->
							</tr>
						</thead>
						<tbody></tbody>

					</table>

				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="findgudang">
	<div class="modal-dialog modal-lg">
		<div class="modal-content" style="border-radius: 5px;">
			<div class="modal-header" style="padding: 10px; color: #000;">
				<h7 class="modal-title" style="margin-left: 5px;">Data Gudang</h7>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="table-responsive">
					<table id="t_gudang" class="table table-bordered table-striped dt-responsive display nowrap" style="width:100%">
						<thead class="thead-dark">
							<tr>
								<th width="10">Action</th>
								<th width="150">Kode Gudang</th>
								<th width="150">Nama Gudang</th>
								<th width="150">Alamat</th>
								<th width="150">Kode Cabang</th>
							</tr>
						</thead>
						<tbody></tbody>
					</table>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="findsupplier">
	<div class="modal-dialog modal-lg">
		<div class="modal-content" style="border-radius: 5px;">
			<div class="modal-header" style="padding: 10px; color: #000;">
				<h7 class="modal-title" style="margin-left: 5px;">Data Supplier</h7>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="table-responsive">
					<table id="t_supplier" class="table table-bordered table-striped dt-responsive display nowrap" style="width:100%">
						<thead class="thead-dark">
							<tr>
								<th width="10">Action</th>
								<th width="150">No. Supplier</th>
								<th width="150">Nama Supplier</th>
								<th width="150">Alamat</th>
								<th width="150">Status PKP</th>
								<th width="150">NPWP</th>
								<th width="150">Alamat NPWP</th>
							</tr>
						</thead>
						<tbody></tbody>

					</table>

				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="modalcancel">
	<div class="modal-dialog modal-md">
		<div class="modal-content" style="border-radius: 5px;">
			<div class="modal-header" style="padding: 10px;">
				<h5 class="modal-title" style="margin-left: 5px; color: #f44336;"><i class="far fa-info-circle mr-1"></i>CANCEL DOKUMEN</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<center>
					<h5 class="modal-title"></i>Apakah Anda Yakin ?</h5>
				</center>
				<div class="row mb-2">
					<div class="col-sm-12 input-group box">
						<!-- <input class="col-sm-12 form-control" type="hidden" name="cancelnopr" id="cancelnopr" maxlength="50" placeholder="Nomor PR" readonly required/> -->
					</div>
				</div>
				<div class="row mb-2">
					<div class="col-sm-12 input-group box">
						<div class="col-sm-3 p-0 input-group-prepend">
							<span class="col-sm-12 input-group-text">Alasan Batal</span>
						</div>
						<textarea name="keteranganbatal" id="keteranganbatal" class="form-control" placeholder="Alasan Batal ..."></textarea>
					</div>
				</div>
			</div>
			<div class="modal-footer p-2">
				<button id="okcancel" type="button" class="btn btn-dark mb-0" data-dismiss="modal"><i class="far fa-check-circle mr-1"></i>Confirm</button>
			</div>
		</div>
	</div>
</div>