<?php
function FormatRupiah($angka)
{

	$hasil_rupiah = number_format($angka);
	// $hasil_rupiah = "Rp. " . number_format($angka);
	return $hasil_rupiah;
}

function tanggal_indo($tanggal)
{
	$bulan = [
		1 => 'Januari',
		'Februari',
		'Maret',
		'April',
		'Mei',
		'Juni',
		'Juli',
		'Agustus',
		'September',
		'Oktober',
		'November',
		'Desember'
	];

	$pecahkan = explode('-', $tanggal);

	return $pecahkan[0] . ' ' . $bulan[(int)$pecahkan[1]] . ' ' . $pecahkan[2];
}
function ClearChr($param)
{
	return $result = str_replace(array('\'', '"', ',', ';', '<', '>', '_', '-', ' ', '.', '!', '?', '/', '=', '+', ']', '[', '{', '}', '@', '#', '$', '%', '^', '&', "*", '(', ')'), '', $param);
}
?>
<style>
	* {
		font-family: 'Courier New', Courier, monospace;
		/* font-weight: bold; */
	}

	.cf:before,
	.cf:after {
		content: " ";
		/* 1 */
		display: table;
		/* 2 */
	}

	.cf:after {
		clear: both;
	}

	/**
 * For IE 6/7 only
 * Include this rule to trigger hasLayout and contain floats.
 */
	.cf {
		*zoom: 1;
	}

	table {
		/* font-family: Verdana, Arial, Helvetica, sans-serif; */
		border-collapse: collapse;
		width: 100%;
	}

	th {
		border: 1px solid #333;
		text-align: left;
		padding: 8px;
	}

	tr {
		line-height: 1.5rem;
	}

	div {
		font-size: 12;
		/* border: 1px solid #68478D; */
	}
</style>
<title>LAPORAN REKAP PENJUALAN SEMUA PELANGGAN | <?= tanggal_indo(date('d-m-Y', strtotime($tglawal))) ?> - <?= tanggal_indo(date('d-m-Y', strtotime($tglakhir))) ?></title>

<body>
	<!-- <script type="text/php">
		if (isset($pdf)) {
            $x = 515;
            $y = 80;
            $size = 8;
            $color = array(0,0,0);
            $text = "{PAGE_NUM} / {PAGE_COUNT}";
            $font = $fontMetrics->get_font("Courier");
            $pdf->page_text($x, $y, $text, $font, $size, $color, 0.0, 0.0, 0.0);
        }
    </script> -->
	<h4 style="text-align: center;">LAPORAN OUTSTANDING INVOICE</h4>
	<div class="row cf" style="margin-bottom: 2px;">
		<div class="col cf" style="float: left; width: 50%; text-align: left;">
			<div style="font-size: 13px; width: 20%; float: left;">
				CABANG
			</div>
			<div style="font-size: 13px; width: 80%; float: right;">
				: <?= $cabang ?>
			</div>
		</div>
	</div>
	<div class="row cf" style="margin-bottom: 2px;">
		<div class="col cf" style="float: left; width: 50%; text-align: left;">
			<div style="font-size: 13px; width: 20%; float: left;">
			</div>
			<div style="font-size: 13px; width: 80%; float: right;">
			</div>
		</div>
		<div class="col" style="float: right; width: 50%; text-align: right;">
			<div style="font-size: 13px; width: 45%; float: left;">
			</div>
			<!-- <div style="font-size: 13px; width: 55%; float: right; margin-right: 86px;">
				Halaman
				:
			</div> -->
		</div>
	</div>
	<div class="row cf" style="margin-bottom: 2px;">
		<div class="col cf" style="float: left; width: 50%; text-align: left;">
			<div style="font-size: 13px; width: 20%; float: left;">
				PERIODE
			</div>
			<div style="font-size: 13px; width: 80%; float: right;">
				: <?= date('d-m-Y', strtotime($tglawal)) ?> <sup style="font-size: 10px;">s</sup>/<sub style="font-size: 10px;">d</sub> <?= date('d-m-Y', strtotime($tglakhir)) ?>
			</div>
		</div>
		<div class="col" style="float: right; width: 50%; text-align: right;">
			<div style="font-size: 13px; width: 45%; float: left;">
			</div>
			<div style="font-size: 13px; width: 55%; float: right;">
				Dicetak
				: <?= date('d-m-Y') ?>
			</div>
		</div>
	</div>

	<table style="font-size: 10px; text-align:justify;" width="100%">
		<thead>
			<tr>
				<th>No</th>
				<th>No.Faktur</th>
				<th>Tanggal</th>
				<th>Jatuh Tempo</th>
				<th>Piutang</th>
				<th>Terbayar</th>
				<th>Saldo</th>
			</tr>
		</thead>
		<tbody>
			<?php
			$totalsemua = 0;
			$total = 0;
			$terbayar = 0;
			$totalsemuasaldo = 0;
			$totalterbayar = 0;
			foreach ($row as $val) : ?>
				<?php $totalsemua = $totalsemua + $total ?>
				<?php $namatoko = $val->namatoko ?>
				<tr>
					<td style="font-weight: bold; font-size: 11px;" colspan="4">Toko : <?= $val->namatoko ?></td>
					<td style="font-weight: bold; font-size: 11px;" colspan="3">Sales : <?= $val->nama ?></td>
				</tr>
				<?php
				$no = 1;
				$saldo = 0;
				$terbayar = 0;
				$total = 0;
				$totalsaldo = 0;
				foreach ($report as $vals) : ?>
					<?php if ($vals->nilaipiutang - $vals->terbayar != 0) : ?>
						<?php if ($vals->namatoko == $namatoko) : ?>
							<?php $totalterbayar = $totalterbayar + $terbayar ?>
							<?php $totalsemuasaldo = $totalsemuasaldo + $totalsaldo ?>
							<?php $saldo = $vals->nilaipiutang - $vals->terbayar ?>
							<?php $terbayar = $vals->terbayar ?>
							<?php $total = $total + $vals->nilaipiutang ?>
							<?php $totalsaldo = $totalsaldo + $saldo ?>
							<tr>
								<td><?= $no++ ?></td>
								<td><?= $vals->nomor ?></td>
								<td><?= date('d-m-Y', strtotime($vals->tanggal)) ?></td>
								<td><?= date('d-m-Y', strtotime($vals->tgljthtempo)) ?></td>
								<td style="text-align: right;"><?= FormatRupiah($vals->nilaipiutang) ?></td>
								<td style="text-align: right;"><?= FormatRupiah($terbayar) ?></td>
								<td style="text-align: right;"><?= FormatRupiah($saldo) ?></td>
							</tr>
						<?php endif; ?>
					<?php endif; ?>
				<?php endforeach; ?>
				<tr>
					<td colspan="7">
						<hr>
					</td>
				</tr>
				<tr>
					<td colspan="4" style="font-size: 12px; font-weight: bold; text-align: right;"> Total :</td>
					<td style="text-align: right;"><?= FormatRupiah($total) ?></td>
					<td colspan="2" style="text-align: right;"><?= FormatRupiah($totalsaldo) ?></td>
				</tr>
			<?php endforeach; ?>
			<tr>
				<td colspan="4" style="font-size: 12px; font-weight: bold; text-align: right;">Grandtotal : </td>
				<td style="text-align: right;"><?= FormatRupiah($totalsemua) ?></td>
				<td style="text-align: right;"><?= FormatRupiah($totalterbayar) ?></td>
				<td style="text-align: right;"><?= FormatRupiah($totalsemuasaldo) ?></td>
			</tr>
		</tbody>
	</table>
</body>