<?php
function FormatRupiah($angka)
{

	$hasil_rupiah = number_format($angka);
	// $hasil_rupiah = "Rp. " . number_format($angka);
	return $hasil_rupiah;
}

function tanggal_indo($tanggal)
{
	$bulan = [
		1 => 'Januari',
		'Februari',
		'Maret',
		'April',
		'Mei',
		'Juni',
		'Juli',
		'Agustus',
		'September',
		'Oktober',
		'November',
		'Desember'
	];

	$pecahkan = explode('-', $tanggal);

	return $pecahkan[0] . ' ' . $bulan[(int)$pecahkan[1]] . ' ' . $pecahkan[2];
}
?>
<style>
	* {
		font-family: 'Courier New', Courier, monospace;
		/* font-weight: bold; */
	}

	.cf:before,
	.cf:after {
		content: " ";
		/* 1 */
		display: table;
		/* 2 */
	}

	.cf:after {
		clear: both;
	}

	/**
 * For IE 6/7 only
 * Include this rule to trigger hasLayout and contain floats.
 */
	.cf {
		*zoom: 1;
	}

	table {
		/* font-family: Verdana, Arial, Helvetica, sans-serif; */
		border-collapse: collapse;
		width: 100%;
	}

	th {
		border: 1px solid #333;
		text-align: left;
		padding: 8px;
	}

	div {
		font-size: 12;
		/* border: 1px solid #408080; */
	}
</style>
<title>LAPORAN KARTU STOK BARANG | <?= tanggal_indo(date('d-m-Y', strtotime($tglawal))) ?> - <?= tanggal_indo(date('d-m-Y', strtotime($tglakhir))) ?></title>

<body>
	<!-- Text halaman & text yang hanya dihalaman terakhir -->
	<!-- <script type="text/php">
		/// Halaman
        if (isset($pdf)) {
            $x = 440;
            $y = 77.4;
            $size = 8;
            $color = array(0,0,0);
            $text = "{PAGE_NUM} / {PAGE_COUNT}";
            $font = $fontMetrics->get_font("helvetica");
            $pdf->page_text($x, $y, $text, $font, $size, $color, 0.0, 0.0, 0.0);
        }
        /// End Halaman
        </script> -->
	<div class="row cf" style="margin-bottom: 2px;">
		<div class="col cf" style="float: left; width: 50%; text-align: left;">
			<div style="font-size: 13px; width: 20%; float: left;">
				GUDANG

			</div>
			<div style="font-size: 13px; width: 80%; float: right;">
				: <?= $gudang->nama ?>
			</div>
		</div>
		<div class="col" style="float: right; width: 50%; text-align: right;">
			<div style="font-size: 13px; width: 45%; float: left;">
			</div>
			<div style="font-size: 13px; width: 55%; float: right;">
				<!-- Halaman
                : 1 / 1 -->
			</div>
		</div>
	</div>
	<div class="row cf" style="margin-bottom: 2px;">
		<div class="col cf" style="float: left; width: 50%; text-align: left;">
			<div style="font-size: 13px; width: 20%; float: left;">
				PERIODE
			</div>
			<div style="font-size: 13px; width: 80%; float: right;">
				: <?= date('d-m-Y', strtotime($tglawal)) ?> <sup style="font-size: 10px;">s</sup>/<sub style="font-size: 10px;">d</sub> <?= date('d-m-Y', strtotime($tglakhir)) ?>
			</div>
		</div>
		<div class="col" style="float: right; width: 50%; text-align: right;">
			<div style="font-size: 13px; width: 45%; float: left;">
			</div>
			<div style="font-size: 13px; width: 55%; float: right;">
				<!-- Halaman
				: -->
			</div>
		</div>
	</div>
	<div class="row cf" style="margin-bottom: 2px;">
		<div class="col cf" style="float: left; width: 50%; text-align: left;">
			<div style="font-size: 13px; width: 20%; float: left;">
				KODE
			</div>
			<div style="font-size: 13px; width: 80%; float: right;">
				: <?php echo $barang->kode ?>
			</div>
		</div>
		<div class="col" style="float: right; width: 50%; text-align: right;">
			<div style="font-size: 13px; width: 45%; float: left;">
			</div>
			<div style="font-size: 13px; width: 55%; float: right;">
				Dicetak
				: <?= date('d-m-Y') ?>
			</div>
		</div>
	</div>
	<div class="row cf" style="margin-bottom: 2px;">
		<div class="col cf" style="float: left; width: 100%; text-align: left;">
			<div style="font-size: 13px; width: 10%; float: left;">
				NAMA
			</div>
			<div style="font-size: 13px; width: 90%; float: right;">
				: <?php echo $barang->nama ?>
			</div>
		</div>
	</div>

	<table style="font-size: 10px; text-align:justify;">
		<thead>
			<tr>
				<th>Tanggal</th>
				<th>Keterangan</th>
				<th>Nomor Transaksi</th>
				<th>Sales</th>
				<th>Qty Masuk</th>
				<th>Qty Keluar</th>
				<th>Mutasi Saldo</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td colspan="6">Saldo Awal</td>
				<td style="text-align:right">
					<?php if (!empty($kartustok)) {
						echo $kartustok->qtyawal;
					} else {
						echo 0;
					}  ?>< /td>
			</tr>
			<?php
			$saldoawal = 0;
			$mutasisaldo = 0;
			$totalmutasi = 0;
			$qtykeluar = 0;
			$qtymasuk = 0;
			foreach ($report as $val) : ?>
				<?php if (!empty($kartustok)) :
					$saldoawal = $kartustok->qtyawal
				?> <?php else :
					$saldoawal = 0;
				endif; ?>
				<?php $qtykeluar = $qtykeluar + $val->qtykeluar ?>
				<?php $qtymasuk = $qtymasuk + $val->qtymasuk ?>
				<?php $totalmutasi = $saldoawal + $qtymasuk - $qtykeluar ?>
				<tr>
					<td><?= date('d-m-Y', strtotime($val->tanggal)) ?></td>
					<td style="text-align:left;"><?= $val->subketerangan . ' ' . $val->keterangan ?></td>
					<td style="text-align:left"><?= $val->nomor ?></td>
					<td style="text-align:left"><?= $val->namasalesman ?></td>
					<td style="text-align:right"><?= $val->qtymasuk ?></td>
					<td style="text-align:right"><?= $val->qtykeluar ?></td>
					<td style="text-align:right"><?= $totalmutasi ?></td>
				</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
</body>