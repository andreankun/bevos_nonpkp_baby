<?php
function rupiah($angka)
{

	$hasil_rupiah = number_format($angka);
	// $hasil_rupiah = "Rp. " . number_format($angka);
	return $hasil_rupiah;
}

function tanggal_indo($tanggal)
{
	$bulan = [
		1 => 'Januari',
		'Februari',
		'Maret',
		'April',
		'Mei',
		'Juni',
		'Juli',
		'Agustus',
		'September',
		'Oktober',
		'November',
		'Desember'
	];

	$pecahkan = explode('-', $tanggal);

	return $pecahkan[0] . ' ' . $bulan[(int)$pecahkan[1]] . ' ' . $pecahkan[2];
}

// $kodegudang = $this->session->userdata('mygudang');
?>

<style>
	* {
		font-family: 'Courier New', Courier, monospace;
		font-weight: bold;
	}

	.cf:before,
	.cf:after {
		content: " ";
		/* 1 */
		display: table;
		/* 2 */
	}

	.cf:after {
		clear: both;
	}

	/**
 * For IE 6/7 only
 * Include this rule to trigger hasLayout and contain floats.
 */
	.cf {
		*zoom: 1;
	}

	table {
		font-family: Verdana, Arial, Helvetica, sans-serif;
		border-collapse: collapse;
		width: 100%;
	}

	th {
		border: 1px solid #333;
		text-align: left;
		padding: 8px;
	}

	/* tr:nth-child(even) {
		background-color: #dddddd;
	} */

	div {
		font-size: 12;
		/* border: 1px solid #68478D; */
	}
</style>
<title>Barang Rusak - <?= $bag->nomor ?></title>
<div class="row cf" style="margin-bottom: 10px;">
	<div class="col" style="float: left; width: 50%; text-align: left;">
		<div style="font-size: 16px;">
			<b>BABY</b>
		</div>
	</div>
	<div class="col" style="float: right; width: 50%; text-align: right;">
		<div style="font-size: 13px;">

		</div>
	</div>
</div>

<div class="row cf" style="margin-bottom: 10px;">
	<div class="col" style="float: left; width: 50%; font-weight: bold; text-align: left;">
		<div style="font-size: 16px;">
			<?php if($bag->jenis == 1){
				echo 'BARANG HILANG';
			} else if($bag->jenis == 2){
				echo 'BARANG DITEMUKAN';
			} else if($bag->jenis == 3){
				echo 'BARANG RUSAK';
			} else if($bag->jenis == 4) {
				echo 'BARANG BUANG';
			} else if($bag->jenis == 5){
				echo 'BARANG PRODUKSI';
			} else {
				echo 'BARANG PARTISIPASI';
			} ?>
		</div>
	</div>
	<div class="col" style="float: right; width: 50%; text-align: right;">
		<div style="font-size: 13px; text-transform: uppercase;">
			<!-- Jakarta, <?= tanggal_indo(date('d-m-Y')) ?> -->
		</div>
	</div>
</div>

<div class="row cf" style="margin-bottom: 2px;">
	<div class="col cf" style="float: left; width: 30%; text-align: left;">
		<div style="font-size: 13px; width: 30%; float: left;">
			Gudang
		</div>
		<div style="font-size: 13px; width: 70%; float: right;">
			: <?= $bag->lokasi ?>
		</div>
	</div>
	<div class="col" style="float: right; width: 70%; text-align: right;">
		<div style="font-size: 13px; text-transform: uppercase;">

		</div>
	</div>
</div>

<div class="row cf" style="margin-bottom: 2px;">
	<div class="col cf" style="float: left; width: 45%; text-align: left;">
		<div style="font-size: 13px; width: 30%; float: left;">
			No. BR
		</div>
		<div style="font-size: 13px; width: 80%; float: right;">
			: <?php echo $bag->nomor ?>
		</div>
	</div>
	<div class="col" style="float: right; width: 70%; text-align: right;">
		<div style="font-size: 13px; text-transform: uppercase;">

		</div>
	</div>
</div>

<div class="row cf" style="margin-bottom: 2px;">
	<div class="col cf" style="float: left; width: 30%; text-align: left;">
		<div style="font-size: 13px; width: 30%; float: left;">
			Tanggal
		</div>
		<div style="font-size: 13px; width: 70%; float: right;">
			: <?= tanggal_indo(date('d-n-y', strtotime($bag->tanggal)))  ?>
		</div>
	</div>
	<div class="col" style="float: right; width: 60%; text-align: right;">
		<div style="font-size: 12px; width: 75%; float: left;">
			DICETAK
		</div>
		<div style="font-size: 12px; width: 25%; float: right;">
			<!-- : <-?= $vsj->lokasi ?>&nbsp;(<-?= $vsj->kodesalesman ?>) -->
			: <?= date('d-m-Y') ?>
		</div>
	</div>
</div>

<div class="row cf" style="margin-bottom: 5px;">
	<div class="col" style="float: right; text-align: right; width: 100%;">
		<div style="font-size: 13px;">

		</div>
	</div>
</div>

<table cellspacing="0" cellpadding="4" border="0" width="100%">
	<thead>
		<tr>
			<th style="text-align: center; font-size: 13px;">No.</th>
			<th style="text-align: center; font-size: 13px;">Banyaknya</th>
			<th style="text-align: center; font-size: 13px;">Nama Produk</th>
		</tr>
	</thead>
	<tbody>
		<?php $no = 1;
		foreach ($bagd as $values) : ?>
			<tr>
				<td style="font-size: 12px; text-align: center;"><?= $no++ ?></td>
				<td style="font-size: 12px; text-align: center;"><?= $values->qty ?> <?= $values->kodesatuan ?></td>
				<td style="font-size: 12px; text-align: left;"><?= $values->namabarang ?></td>
			</tr>
		<?php endforeach; ?>
	</tbody>
</table>
<hr>
<div class="row">
	<div class="col-sm-8" style="float: right; width: 20%; font-size: 12px;">
		<?php $sum = 0;
		foreach ($bagd as $cell) : ?>
			<?php $sum += $cell->qty++ ?>
		<?php endforeach ?>
		<b style="float: left;">Total Qty : </b> <b style="float: right;"><?= rupiah($sum) ?></b>
	</div>
	<div class="col-sm-4" style="font-size: 12px; width: 70%; text-transform: uppercase;">
	</div>
</div>


<div class="row" style="margin-bottom: 15px;">
	<div class="col-sm-4" style="font-size: 12px; width: 40%; text-transform: uppercase;">
		<?= $bag->keterangan ?>
	</div>
</div>

<div class="row" style="margin-bottom: 15px;">
	<div class="col-sm-4" style="font-size: 12px; width: 40%; text-transform: uppercase;">
		<?php if ($bag->approve == 't') : ?>
			[APPROVED]
		<?php else : ?>

		<?php endif; ?>
	</div>
</div>

<div style="float: left; font-size: 13px; width: 20%;">
	Mengetahui,
</div>
<!-- <div style="float: left; font-size: 13px; width: 20%;">
	Sopir,
</div>
<div style="float: left; font-size: 13px; width: 20%;">
	Hormat Kami,
</div> -->
