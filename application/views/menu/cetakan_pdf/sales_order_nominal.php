<?php
function rupiah($angka)
{
    $hasil_rupiah = number_format($angka);
    // $hasil_rupiah = "Rp. " . number_format($angka);
    return $hasil_rupiah;
}

function tanggal_indo($tanggal)
{
    $bulan = [
        1 => 'Januari',
        'Februari',
        'Maret',
        'April',
        'Mei',
        'Juni',
        'Juli',
        'Agustus',
        'September',
        'Oktober',
        'November',
        'Desember'
    ];

    $pecahkan = explode('-', $tanggal);

    return $pecahkan[0] . ' ' . $bulan[(int)$pecahkan[1]] . ' ' . $pecahkan[2];
}
?>

<style>
    * {
        font-family: 'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif;
        /* font-family: 'Courier New', Courier, monospace; */
        /* font-weight: bold; */
    }

    @page {
		margin-top: 25px;
	}

    .cf:before,
    .cf:after {
        content: " ";
        /* 1 */
        display: table;
        /* 2 */
    }

    .cf:after {
        clear: both;
    }

    /**
 * For IE 6/7 only
 * Include this rule to trigger hasLayout and contain floats.
 */
    .cf {
        *zoom: 1;
    }

    table {
        /* font-family: Verdana, Arial, Helvetica, sans-serif; */
        border-collapse: collapse;
        width: 100%;
    }

    th {
        border: 1px solid #333;
        text-align: left;
        padding: 3px;
    }

    /* tr:nth-child(even) {
		background-color: #dddddd;
	} */

    div {
        font-size: 12;
        /* border: 1px solid #68478D; */
    }
</style>
<title>Sales Order - <?= $so->nomor ?></title>
<div class="row cf" style="margin-bottom: 10px;">
    <div class="col" style="float: left; width: 50%; text-align: left;">
        <div style="font-size: 13pt;">
            <b><?=$vso->namabisnis ?></b>
        </div>
    </div>
    <div class="col" style="float: right; width: 50%; text-align: right;">
        <div style="font-size: 12px;">
            <!-- SO# <-?= $inv->nomorso ?> -->
        </div>
    </div>
</div>

<div class="row cf" style="margin-bottom: 2px;">
    <div class="col" style="width: 100%; font-weight: bold; text-align: center;">
        <div style="font-size: 13pt;">
            SALES ORDER
        </div>
    </div>
</div>

<div class="row cf" style="margin-bottom: 2px;">
    <div class="col" style="float: right; width: 100%; text-align: right;">
        <div style="font-size: 12px; text-transform: uppercase;">
            Jakarta, <?= tanggal_indo(date('d-m-Y')) ?>
        </div>
    </div>
</div>

<div class="row cf" style="margin-bottom: 2px;">
    <div class="col cf" style="float: left; width:50%; text-align: left;">
        <div style="font-size: 12px; width: 45%; float: left;">
            NO. SALES ORDER
        </div>
        <div style="font-size: 12px; width: 55%; float: right;">
            : <?= $vso->nomor ?>
        </div>
    </div>
    <div class="col" style="float: right; width: 50%; text-align: right;">
        <div style="font-size: 12px; text-transform: uppercase;">
            <?= $vso->nama_toko ?>
        </div>
    </div>
</div>

<div class="row cf" style="margin-bottom: 2px;">
    <div class="col cf" style="float: left; width: 50%; text-align: left;">
        <div style="font-size: 12px; width: 45%; float: left;">
            <!-- JATUH TEMPO -->
            KODE
        </div>
        <div style="font-size: 12px; width: 55%; float: right;">
            <!-- : <-?= tanggal_indo(date('d-n-Y', strtotime($vso->tgljthtempo))) ?> -->
            : <?= $vso->kodesalesman ?>
        </div>
    </div>
    <div class="col" style="float: right; width: 50%; text-align: right;">
        <div style="font-size: 12px; text-transform: uppercase;">
            <?= $vso->pengiriman ?>
        </div>
    </div>
</div>

<div class="row cf" style="margin-bottom: 2px;">
    <div class="col cf" style="float: left; width: 50%; text-align: left;">
        <div style="font-size: 12px; width: 45%; float: left;">
            KATEGORI
        </div>
        <div style="font-size: 12px; width: 55%; float: right;">
            : <?= $vso->lokasi ?>
        </div>
    </div>
    <div class="col" style="float: right; width: 50%; text-align: right;">
        <div style="font-size: 12px; text-transform: uppercase;">
            <?= $vso->kota ?>
        </div>
    </div>
</div>

<div class="row cf" style="margin-bottom: 5px;">
    <div class="col cf" style="float: left; width: 50%; text-align: left;">
        <div style="font-size: 12px; width: 45%; float: left;">
        </div>
        <div style="font-size: 12px; width: 55%; float: right;">
        </div>
    </div>
    <div class="col" style="float: right; text-align: right; width: 50%;">
        <div style="font-size: 12px;">
            <?php if (!empty($vso->nohp)) : ?>
                <?= $vso->nohp ?>
            <?php elseif (!empty($vso->notelp)) : ?>
                <?= $vso->notelp ?>
            <?php else : ?>
                <?= '' ?>
            <?php endif; ?>
        </div>
    </div>
</div>

<!-- <br> -->
<table style="margin-bottom: 0;" cellpadding="4" cellspacing="0">
    <thead>
        <tr>
            <th style="text-align: center; font-size: 13px;">NO.</th>
            <th style="text-align: center; font-size: 13px;">NAMA BARANG</th>
            <th style="text-align: center; font-size: 13px;">QTY</th>
            <th style="text-align: center; font-size: 13px;">HARGA</th>
            <th style="text-align: center; font-size: 13px;">DISC 1</th>
            <th style="text-align: center; font-size: 13px;">DISC 2</th>
            <th style="text-align: center; font-size: 13px;">TOTAL HARGA</th>
        </tr>
    </thead>
    <tbody>
        <?php $no = 1;
        $ttlharga = 0;
        $ttldiskonpromo = 0;
        $ttldiskoncash = 0;
        $ttldiskoncustomer = 0;
        $totalharga = 0;
        $diskonpromo = 0;
        $diskoncash = 0;
        $diskoncustomer = 0;
        $diskonrp = 0;
        foreach ($vsod as $values) : ?>
            <?php
            $ttlharga = (($values->qty * $values->harga) - $values->discbrgitem - $values->discpaketpromo);
            $totalharga = $totalharga + $ttlharga;

            // $ttldiskonpromo = $values->discpaketpromo;
            // $diskonpromo = $diskonpromo + $ttldiskonpromo;
            // print_r($ttldiskonpromo);
            // die();

            $ttldiskoncash = $values->nilaicash;
            // $diskoncash = $diskoncash + $ttldiskoncash;

            $ttldiskoncustomer = $values->nilaicustomer;
            // $diskoncustomer = $diskoncustomer + $ttldiskoncustomer;

            $diskonrp = $ttldiskoncash + $ttldiskoncustomer;
            ?>
            <tr>
                <td style="font-size: 12px; text-align: center;"><?= $no++ ?></td>
                <td style="font-size: 12px; text-align: left;"><?= $values->namabarang ?></td>
                <td style="font-size: 12px; text-align: center;"><?= $values->qty ?> <?= $values->namasatuan ?></td>
                <td style="font-size: 12px; text-align: right;"><?= rupiah($values->harga) ?></td>
                <td style="font-size: 12px; text-align: center;"><?= $values->discbarang ?>%</td>
                <td style="font-size: 12px; text-align: center;"><?= $values->discpromo ?>%</td>
                <td style="font-size: 12px; text-align: right;"><?= rupiah($ttlharga) ?></td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<hr>

<table border="0" cellspacing="0" cellpadding="0" style="margin-bottom: 15px;">
    <tr>
        <td style="width: 15%; font-size: 13px;">PAKET PROMO</td>
        <td style="width: 1%; font-size: 13px;">:</td>
        <td style="font-size: 13px;"><?= $vso->namapromo ?></td>
        <td style="width: 16%; text-align: right; font-size: 13px;">SUB TOTAL RP&nbsp;</td>
        <td style="width: 1%; font-size: 13px;"> : </td>
        <td style="text-align: right; font-size: 13px; width: 20%; font-weight: bold;"><?= rupiah($totalharga) ?></td>
    </tr>
    <tr>
        <td style="width: 15%; font-size: 13px;">KETERANGAN</td>
        <td style="width: 1%; font-size: 13px;">: </td>
        <td style="font-size: 13px;">
            <?php if ($vso->jenisjual == '1') {
                echo '[TUNAI]';
            } else if($vso->jenisjual == '2') {
                echo '[KREDIT]';
            } else if($vso->jenisjual == '3'){
                echo '[COD]';
            } else {
                echo '[TUNAI 0%]';
            }
            ?>
        </td>
        <td style="width: 15%; text-align: right; font-size: 13px;">DISKON RP&nbsp;</td>
        <td style="width: 1%; font-size: 13px;"> : </td>
        <td style="text-align: right; font-size: 13px; width: 20%; font-weight: bold;"><?= rupiah(round($diskonrp / 10) * 10) ?></td>
    </tr>
    <tr>
        <td style="width: 15%; font-size: 13px;"></td>
        <td style="width: 1%; font-size: 13px;"></td>
        <td></td>
        <td style="width: 15%; text-align: right; font-size: 13px;">DPP&nbsp;</td>
        <td style="width: 1%; font-size: 13px;"> : </td>
        <td style="text-align: right; font-size: 13px; width: 20%; font-weight: bold;"><?= rupiah($vso->dpp) ?></td>
    </tr>
    <tr>
        <td style="text-align: left; font-size: 12px; font-weight: bold;" colspan="3">GIRO/CEK/TRANSFER KE PT. BEVOS PRIMA CENTER, <?= $vso->namaaccount ?> : <?= $vso->norekening ?></td>
        <td style="width: 15%; text-align: right; font-size: 13px;">PPN&nbsp;</td>
        <td style="width: 1%; font-size: 13px;"> : </td>
        <td style="text-align: right; font-size: 13px; width: 20%; font-weight: bold;"><?= rupiah($vso->ppn) ?></td>
    </tr>
    <tr>
        <td style="width: 15%; font-size: 13px;"></td>
        <td style="width: 1%; font-size: 13px;"></td>
        <td></td>
        <td style="width: 15%; text-align: right; font-size: 13px; font-weight: bold;">TOTAL RP&nbsp;</td>
        <td style="width: 1%; font-size: 13px; font-weight: bold;"> : </td>
        <td style="text-align: right; font-size: 13px; width: 20%; font-weight: bold;"><?= rupiah($vso->grandtotal) ?></td>
    </tr>
</table>