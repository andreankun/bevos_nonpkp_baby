<?php
function FormatRupiah($angka)
{

    $hasil_rupiah = number_format($angka);
    // $hasil_rupiah = "Rp. " . number_format($angka);
    return $hasil_rupiah;
}

function tanggal_indo($tanggal)
{
    $bulan = [
        1 => 'Januari',
        'Februari',
        'Maret',
        'April',
        'Mei',
        'Juni',
        'Juli',
        'Agustus',
        'September',
        'Oktober',
        'November',
        'Desember'
    ];

    $pecahkan = explode('-', $tanggal);

    return $pecahkan[0] . ' ' . $bulan[(int)$pecahkan[1]] . ' ' . $pecahkan[2];
}
?>
<style>
    /* * { */
    * {
        /* font-family: 'Courier New', Courier, monospace; */
        font-family: 'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif;
        /* font-weight: bold; */
    }

    .cf:before,
    .cf:after {
        content: " ";
        /* 1 */
        display: table;
        /* 2 */
    }

    .cf:after {
        clear: both;
    }

    /**
 * For IE 6/7 only
 * Include this rule to trigger hasLayout and contain floats.
 */
    .cf {
        *zoom: 1;
    }

    table {
        /* font-family: Verdana, Arial, Helvetica, sans-serif; */
        border-collapse: collapse;
        width: 100%;
    }

    th {
        border: 1px solid #333;
        text-align: left;
        padding: 3px;
    }

    div {
        font-size: 12;
        /* border: 1px solid #68478D; */
    }
</style>
<title>LAPORAN REKAP PENJUALAN PELANGGAN | <?= tanggal_indo(date('d-m-Y', strtotime($tglawal))) ?> - <?= tanggal_indo(date('d-m-Y', strtotime($tglakhir))) ?></title>

<body>
    <h4 style="text-align: center;">LAPORAN REKAP PENJUALAN PELANGGAN</h4>
    <div class="row cf" style="margin-bottom: 2px;">
        <div class="col cf" style="float: left; width: 50%; text-align: left;">
            <div style="font-size: 13px; width: 20%; float: left;">
                CABANG
            </div>
            <div style="font-size: 13px; width: 80%; float: right;">
                : <?= $cabang->nama ?>
            </div>
        </div>
        <div class="col" style="float: right; width: 50%; text-align: right;">
            <div style="font-size: 13px; width: 45%; float: left;">
            </div>
            <div style="font-size: 13px; width: 55%; float: right;">
                <!-- Halaman
                : 1 / 1 -->
            </div>
        </div>
    </div>
    <div class="row cf" style="margin-bottom: 2px;">
        <div class="col cf" style="float: left; width: 50%; text-align: left;">
            <div style="font-size: 13px; width: 20%; float: left;">
                PERIODE
            </div>
            <div style="font-size: 13px; width: 80%; float: right;">
                : <?= date('d-m-Y', strtotime($tglawal)) ?> <sup style="font-size: 10px;">s</sup>/<sub style="font-size: 10px;">d</sub> <?= date('d-m-Y', strtotime($tglakhir)) ?>
            </div>
        </div>
        <div class="col" style="float: right; width: 50%; text-align: right;">
            <div style="font-size: 13px; width: 45%; float: left;">
            </div>
            <div style="font-size: 13px; width: 55%; float: right;">
                Dicetak
                : <?= date('d-m-Y') ?>
            </div>
        </div>
    </div>
    <br>

    <table style="font-size: 13px;" width="100%" cellspacing="0" cellpadding="3">
        <thead>
            <tr>
                <th style="text-align: center;">No.</th>
                <th style="text-align: center;">Nama Pelanggan</th>
                <th style="text-align: center;">Nama Toko</th>
                <th style="text-align: center;">Subtotal</th>
                <th style="text-align: center;">Diskon</th>
                <th style="text-align: center;">DPP</th>
                <th style="text-align: center;">PPN</th>
                <th style="text-align: center;">Total</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $no = 1;
            $subtotal = 0;
            $grandtotal = 0;
            $total_diskon = 0;
            $dpp = 0;
            $hasilppn = 0;
            $hsubtotal = 0;
            $htotal_diskon = 0;
            $hdpp = 0;
            $hhasilppn = 0;
            $hgrandtotal = 0;
            foreach ($report as $val) :
                $subtotal = $val->subtotal - $val->discbrgitem - $val->discpaketitem;
                $total_diskon = $val->nilaicash + $val->nilaicustomer;
                $dpp = $val->dpp;
                $hasilppn = floor($val->dpp * ($ppn->ppn / 100));
                $grandtotal = $val->dpp + $hasilppn;
            ?>
                <tr>
                    <td style="text-align: center;"><?= $no++; ?></td>
                    <td style="text-align: left;"><?= $val->namapelanggan ?></td>
                    <td style="text-align: left;"><?= $val->nama_toko ?></td>
                    <td style="text-align: right;"><?= FormatRupiah($subtotal) ?></td>
                    <?php if ($total_diskon == 0) : ?>
                        <td style="text-align: right;"><?= '' ?></td>
                    <?php else : ?>
                        <td style="text-align: right;"><?= FormatRupiah($total_diskon) ?></td>
                    <?php endif; ?>
                    <td style="text-align: right;"><?= FormatRupiah($dpp) ?></td>
                    <td style="text-align: right;"><?= FormatRupiah($hasilppn) ?></td>
                    <td style="text-align: right;"><?= FormatRupiah($grandtotal) ?></td>
                    <?php $hsubtotal = $hsubtotal + $subtotal; ?>
                    <?php $htotal_diskon = $htotal_diskon + $total_diskon; ?>
                    <?php $hdpp = $hdpp + $dpp; ?>
                    <?php $hhasilppn = $hhasilppn + $hasilppn; ?>
                    <?php $hgrandtotal = $hgrandtotal + $grandtotal; ?>
                </tr>
            <?php endforeach; ?>
            <tr>
                <td colspan="8">
                    <hr>
                </td>
            </tr>
            <tr>
                <td style="text-align: right;" colspan="3">Grand Total : </td>
                <td style="text-align: right;" colspan="1"><?= FormatRupiah($hsubtotal) ?></td>
                <td style="text-align: right;" colspan="1"><?= FormatRupiah($htotal_diskon) ?></td>
                <td style="text-align: right;" colspan="1"><?= FormatRupiah($hdpp) ?></td>
                <td style="text-align: right;" colspan="1"><?= FormatRupiah($hhasilppn) ?></td>
                <td style="text-align: right;" colspan="1"><?= FormatRupiah($hgrandtotal) ?></td>
            </tr>
        </tbody>
    </table>
</body>