<?php
function FormatRupiah($angka)
{

	$hasil_rupiah = number_format($angka);
	// $hasil_rupiah = "Rp. " . number_format($angka);
	return $hasil_rupiah;
}

function tanggal_indo($tanggal)
{
	$bulan = [
		1 => 'Januari',
		'Februari',
		'Maret',
		'April',
		'Mei',
		'Juni',
		'Juli',
		'Agustus',
		'September',
		'Oktober',
		'November',
		'Desember'
	];

	$pecahkan = explode('-', $tanggal);

	return $pecahkan[0] . ' ' . $bulan[(int)$pecahkan[1]] . ' ' . $pecahkan[2];
}
?>
<style>
	* {
		font-family: 'Courier New', Courier, monospace;
		/* font-weight: bold; */
	}

	.cf:before,
	.cf:after {
		content: " ";
		/* 1 */
		display: table;
		/* 2 */
	}

	.cf:after {
		clear: both;
	}

	/**
 * For IE 6/7 only
 * Include this rule to trigger hasLayout and contain floats.
 */
	.cf {
		*zoom: 1;
	}

	table {
		/* font-family: Verdana, Arial, Helvetica, sans-serif; */
		border-collapse: collapse;
		width: 100%;
	}

	th {
		border: 1px solid #333;
		text-align: left;
		padding: 8px;
	}

	div {
		font-size: 12;
		/* border: 1px solid #408080; */
	}
</style>
<title>LAPORAN KARTU STOK SEMUA | <?= tanggal_indo(date('d-m-Y', strtotime($tglawal))) ?> - <?= tanggal_indo(date('d-m-Y', strtotime($tglakhir))) ?></title>

<body>
	<!-- Text halaman & text yang hanya dihalaman terakhir -->
	<!-- <script type="text/php">
		/// Halaman
        if (isset($pdf)) {
            $x = 440;
            $y = 77.4;
            $size = 8;
            $color = array(0,0,0);
            $text = "{PAGE_NUM} / {PAGE_COUNT}";
            $font = $fontMetrics->get_font("helvetica");
            $pdf->page_text($x, $y, $text, $font, $size, $color, 0.0, 0.0, 0.0);
        }
        /// End Halaman
        </script> -->
		<h4 style="text-align: center;">LAPORAN KARTU STOK SEMUA GUDANG PAJAK</h4>
	<div class="row cf" style="margin-bottom: 2px;">
		<div class="col cf" style="float: left; width: 50%; text-align: left;">
			<div style="font-size: 13px; width: 20%; float: left;">
				CABANG

			</div>
			<div style="font-size: 13px; width: 80%; float: right;">
				: <?= $cabang->nama ?>
			</div>
		</div>
		<div class="col" style="float: right; width: 50%; text-align: right;">
			<div style="font-size: 13px; width: 45%; float: left;">
			</div>
			<div style="font-size: 13px; width: 55%; float: right;">
				<!-- Halaman
                : 1 / 1 -->
			</div>
		</div>
	</div>
	<div class="row cf" style="margin-bottom: 2px;">
		<div class="col cf" style="float: left; width: 50%; text-align: left;">
			<div style="font-size: 13px; width: 20%; float: left;">
				Kategori
			</div>
			<div style="font-size: 13px; width: 80%; float: right;">
				: <?php echo $merkawal->nama ?> S/D  <?php echo $merkakhir->nama ?>
			</div>
		</div>
		<div class="col" style="float: right; width: 50%; text-align: right;">
			<div style="font-size: 13px; width: 45%; float: left;">
			</div>
			<div style="font-size: 13px; width: 55%; float: right;">
				Halaman
				:
			</div>
		</div>
		<br>
		<div class="row cf" style="margin-bottom: 2px;">
			<div class="col cf" style="float: left; width: 50%; text-align: left;">
				<div style="font-size: 13px; width: 20%; float: left;">
					PERIODE
				</div>
				<div style="font-size: 13px; width: 80%; float: right;">
					: <?= date('d-m-Y', strtotime($tglawal)) ?> <sup style="font-size: 10px;">s</sup>/<sub style="font-size: 10px;">d</sub> <?= date('d-m-Y', strtotime($tglakhir)) ?>
				</div>
			</div>
			<div class="col" style="float: right; width: 50%; text-align: right;">
				<div style="font-size: 13px; width: 45%; float: left;">
				</div>
				<div style="font-size: 13px; width: 55%; float: right;">
					Dicetak
					: <?= date('d-m-Y') ?>
				</div>
			</div>
		</div>
		<br>

		<table style="font-size: 10px; text-align:justify;">
		<thead>
		<tr>
			<th>NO</th>
			<th>KATEGORI</th>
			<th>KODE</th>
			<th>NAMA</th>
			<th>SALDO AWAL</th>
			<th>QTY MASUK</th>
			<th>QTY KELUAR</th>
			<th>SALDO AKHIR</th>
		</tr>
	</thead>
	<tbody>
		<?php 
		$no = 1;
		$saldo_akhir = 0;
		foreach($kartustok as $val): ?>
		<?php $saldo_akhir = $val->qtyawal + $val->qtymasuk - $val->qtykeluar - $val->qtypick ?>
			<tr>
				<td><?= $no++ ?></td>
				<td><?= $val->namamerk ?></td>
				<td><?= $val->kodebarang ?></td>
				<td><?= $val->namabarang ?></td>
				<td><?= $val->qtyawal ?></td>
				<td><?= $val->qtymasuk ?></td>
				<td><?= $val->qtykeluar ?></td>
				<td><?= $saldo_akhir ?></td>
			</tr>
		<?php endforeach; ?>
	</tbody>
</body>
