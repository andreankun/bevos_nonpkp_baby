<?php
function rupiah($angka)
{

	$hasil_rupiah = number_format($angka);
	// $hasil_rupiah = "Rp. " . number_format($angka);
	return $hasil_rupiah;
}

function tanggal_indo($tanggal)
{
	$bulan = [
		1 => 'Januari',
		'Februari',
		'Maret',
		'April',
		'Mei',
		'Juni',
		'Juli',
		'Agustus',
		'September',
		'Oktober',
		'November',
		'Desember'
	];

	$pecahkan = explode('-', $tanggal);

	return $pecahkan[0] . ' ' . $bulan[(int)$pecahkan[1]] . ' ' . $pecahkan[2];
}
?>

<style>
	* {
		font-family: 'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif;
		/* font-family: Arial, Arial, Helvetica, sans-serif; */
		/* */
		/* font-weight: bold; */
	}

	@page {
		margin-top: 25px;
	}

	.cf:before,
	.cf:after {
		content: " ";
		/* 1 */
		display: table;
		/* 2 */
	}

	.cf:after {
		clear: both;
	}

	/**
 * For IE 6/7 only
 * Include this rule to trigger hasLayout and contain floats.
 */
	.cf {
		*zoom: 1;
	}

	table {
		font-family: Arial, Arial, Helvetica, sans-serif;
		border-collapse: collapse;
		width: 100%;
	}

	th {
		border: 1px solid #333;
		text-align: left;
		padding: 3px;
	}

	/* tr:nth-child(even) {
		background-color: #dddddd;
	} */

	div {
		font-size: 12;
		/* border: 1px solid #68478D; */
	}
</style>
<title>Surat Jalan - <?= $sj->nomor ?></title>
<div class="row cf">
	<div class="col cf" style="float: right; width: 100%; text-align: right;">
		<div style="font-size: 10px;">
			<?php if (!empty($vsj->revisike)) : ?>
				[RR-<?= $vsj->revisike ?>]
			<?php else : ?>
				<?= '' ?>
			<?php endif; ?>
		</div>
	</div>
</div>
<div class="row cf" style="margin-bottom: 10px;">
	<div class="col" style="float: left; width: 50%; text-align: left;">
		<div style="font-size: 13pt;">
			<b><?= $vsj->namabisnis ?></b>
		</div>
	</div>
	<div class="col" style="float: right; width: 50%; text-align: right;">
		<div style="font-size: 13px;">
			SO# <?= $vsj->nomorso ?>
		</div>
		<div style="font-size: 13px;">
			[<?= $vsj->nomorgb.$vsj->rekeningtf ?>]
		</div>
	</div>
</div>

<div class="row cf" style="margin-bottom: 2px;">
	<div class="col" style="width: 100%; font-weight: bold; text-align: center;">
		<div style="font-size: 13pt;">
			SURAT JALAN
		</div>
	</div>
</div>

<div class="row cf" style="margin-bottom: 2px;">
	<div class="col" style="float: right; width: 100%; text-align: right;">
		<div style="font-size: 12px; text-transform: uppercase;">
			Jakarta, <?= tanggal_indo(date('d-n-Y', strtotime($vsj->tanggal))) ?>
		</div>
	</div>
</div>

<div class="row cf" style="margin-bottom: 2px;">
	<div class="col cf" style="float: left; width: 40%; text-align: left;">
		<div style="font-size: 13px; width: 35%; float: left;">
			NO. FAKTUR
		</div>
		<div style="font-size: 13px; width: 65%; float: right;">
			: <?= $vsj->noinvoice ?>
		</div>
	</div>
	<div class="col" style="float: right; width: 60%; text-align: right;">
		<div style="font-size: 14px; text-transform: uppercase;">
			<?= $vsj->namacustomer ?>
		</div>
	</div>
</div>

<div class="row cf" style="margin-bottom: 2px;">
	<div class="col cf" style="float: left; width: 40%; text-align: left;">
		<div style="font-size: 13px; width: 35%; float: left;">
			NO. SURAT
		</div>
		<div style="font-size: 13px; width: 65%; float: right;">
			: <?= $vsj->nomorsj ?>
		</div>
	</div>
	<div class="col" style="float: right; width: 60%; text-align: right;">
		<div style="font-size: 13px; text-transform: uppercase; text-align: right;">
			<?= $vsj->alamat ?>
		</div>
	</div>
</div>

<div class="row cf" style="margin-bottom: 2px;">
	<div class="col cf" style="float: left; width: 40%; text-align: left;">
		<div style="font-size: 13px; width: 35%; float: left;">
			KATEGORI
		</div>
		<div style="font-size: 13px; width: 65%; float: right;">
			: <?= $vsj->lokasi ?>&nbsp;(<?= $vsj->kodesalesman ?>)
		</div>
	</div>
	<div class="col" style="float: right; width: 60%; text-align: right;">
		<div style="font-size: 13px; text-transform: uppercase;">
			<?= $vsj->kota ?>
		</div>
	</div>
</div>

<div class="row cf" style="margin-bottom: 5px;">
	<div class="col" style="float: right; text-align: right; width: 100%;">
		<div style="font-size: 13px;">
			<?php if (!empty($vsj->nohp)) : ?>
				<?= $vsj->nohp ?>
			<?php elseif (!empty($vsj->notelp)) : ?>
				<?= $vsj->notelp ?>
			<?php else : ?>
				<?= '' ?>
			<?php endif; ?>
		</div>
	</div>
</div>

<table style="margin-bottom: 0;" cellpadding="4" cellspacing="0">
	<thead>
		<tr>
			<th style="text-align: center; font-size: 13px; width: 25">No</th>
			<th style="text-align: center; font-size: 13px; width: 55">Banyaknya</th>
			<th style="text-align: center; font-size: 13px;">Nama Produk</th>
		</tr>
	</thead>
	<tbody>
		<?php $no = 1;
		foreach ($vsjd as $values) : ?>
			<tr>
				<td style="font-size: 12px; text-align: center;"><?= $no++ ?></td>
				<td style="font-size: 12px; text-align: center;"><?= $values->qty ?> <?= $values->namasatuan ?></td>
				<td style="font-size: 12px; text-align: left;"><?= $values->namabarang ?></td>
			</tr>
		<?php endforeach; ?>
	</tbody>
</table>
<hr>
<div class="row">
	<div class="col-sm-8" style="float: right; width: 20%; font-size: 13px; font-weight: bold;">
		<?php $sum = 0;
		foreach ($vsjd as $cell) : ?>
			<?php $sum += $cell->qty++ ?>
		<?php endforeach ?>
		<b style="float: left; font-weight: bold;">Total Qty : </b> <b style="float: right;"><?= rupiah($sum) ?></b>
	</div>
	<div class="col-sm-4" style="font-size: 13px; width: 70%; text-transform: uppercase;">
		TUJUAN PENGIRIMAN : <?= $vsj->nama_toko ?>
	</div>

</div>

<div class="row" style="margin-bottom: 5px;">
	<div class="col-sm-4" style="font-size: 13px; width: 40%; text-transform: uppercase;">
		<?= $vsj->pengiriman ?>
		<!-- <br>
		<-?= $vsj->kota ?>&nbsp;<-?= $vsj->kodepos ?> -->
	</div>
</div>

<div class="row cf">
	<div class="col cf" style="float: left; width: 30%;">
		<div style="font-size: 11px;">
			Diterima,
		</div>
	</div>
	<div class="col cf" style="float: left; width: 30%;">
		<div style="font-size: 11px;">
			Sopir,
		</div>
	</div>
	<div class="col cf" style="float: left; width: 30%;">
		<div style="font-size: 11px;">
			Hormat Kami,
		</div>
	</div>
</div>

<!-- <div class="row cf">
	<div class="col cf" style="float: right; width: 100%; text-align: right;">
		<div style="font-size: 10px;">
			<?php if ($vsj->reprint == 't') : ?>
				[R]
			<?php else : ?>
				<?= '' ?>
			<?php endif; ?>
		</div>
	</div>
</div> -->
