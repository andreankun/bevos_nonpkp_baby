<?php
function FormatRupiah($angka)
{

    $hasil_rupiah = number_format($angka);
    // $hasil_rupiah = "Rp. " . number_format($angka);
    return $hasil_rupiah;
}

function tanggal_indo($tanggal)
{
    $bulan = [
        1 => 'Januari',
        'Februari',
        'Maret',
        'April',
        'Mei',
        'Juni',
        'Juli',
        'Agustus',
        'September',
        'Oktober',
        'November',
        'Desember'
    ];

    $pecahkan = explode('-', $tanggal);

    return $pecahkan[0] . ' ' . $bulan[(int)$pecahkan[1]] . ' ' . $pecahkan[2];
}
?>
<style>
    * {
        font-family: 'Courier New', Courier, monospace;
        /* font-weight: bold; */
    }

    .cf:before,
    .cf:after {
        content: " ";
        /* 1 */
        display: table;
        /* 2 */
    }

    .cf:after {
        clear: both;
    }

    /**
 * For IE 6/7 only
 * Include this rule to trigger hasLayout and contain floats.
 */
    .cf {
        *zoom: 1;
    }

    table {
        /* font-family: Verdana, Arial, Helvetica, sans-serif; */
        border-collapse: collapse;
        width: 100%;
    }

    th {
        border: 1px solid #333;
        text-align: left;
        padding: 8px;
    }

    div {
        font-size: 12;
        /* border: 1px solid #68478D; */
    }
</style>
<title>LAPORAN OUTSTANDING SALES ORDER </title>

<body>
    <h4 style="text-align: center;">LAPORAN OUTSTANDING SALES ORDER</h4>
    <div class="row cf" style="margin-bottom: 2px;">
        <div class="col cf" style="float: left; width: 50%; text-align: left;">
            <div style="font-size: 13px; width: 20%; float: left;">
                CABANG
            </div>
            <?php if (!empty($row)) : ?>
                <div style="font-size: 13px; width: 80%; float: right;">
                    : <?= $row->nama ?>
                </div>
            <?php else : ?>
                <div style="font-size: 13px; width: 80%; float: right;">
                    : SEMUA CABANG
                </div>
            <?php endif; ?>
        </div>
        <div class="col" style="float: right; width: 50%; text-align: right;">
            <div style="font-size: 13px; width: 45%; float: left;">
            </div>
            <div style="font-size: 13px; width: 55%; float: right;">
                <!-- Halaman
                : 1 / 1 -->
            </div>
        </div>
    </div>
    <div class="row cf" style="margin-bottom: 2px;">
        <div class="col cf" style="float: left; width: 50%; text-align: left;">
            <div style="font-size: 13px; width: 20%; float: left;">
                <!-- PERIODE -->
            </div>
            <div style="font-size: 13px; width: 80%; float: right;">
                <!-- : <-?= date('d-m-Y', strtotime($tglawal)) ?> <sup style="font-size: 10px;">s</sup>/<sub style="font-size: 10px;">d</sub> <-?= date('d-m-Y', strtotime($tglakhir)) ?> -->
            </div>
        </div>
        <div class="col" style="float: right; width: 50%; text-align: right;">
            <div style="font-size: 13px; width: 45%; float: left;">
            </div>
            <div style="font-size: 13px; width: 55%; float: right;">
                Dicetak
                : <?= date('d-m-Y') ?>
            </div>
        </div>
    </div>
    <br>

    <table style="font-size: 10px;" width="100%">
        <thead>
            <tr>
                <th style="text-align: center;">No.</th>
                <th style="text-align: center;">Sales Order</th>
                <th style="text-align: center;">Tanggal</th>
                <th style="text-align: center;">Nama Toko</th>
                <th style="text-align: center;">DPP</th>
                <th style="text-align: center;">PPN</th>
                <th style="text-align: center;">Total</th>
                <th style="text-align: center;">Cabang</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $no = 1;
            $grandtotal = 0;
            foreach ($report as $val) : ?>
                <tr>
                    <td style="text-align: center;"><?= $no++; ?></td>
                    <td style="text-align: center;"><?= $val->nomor ?></td>
                    <td style="text-align: center;"><?= date('d-m-Y', strtotime($val->tanggal)) ?></td>
                    <td style="text-align: left;"><?= $val->namapelanggan ?></td>
                    <td style="text-align: right;"><?= FormatRupiah($val->dpp) ?></td>
                    <td style="text-align: right;"><?= FormatRupiah($val->ppn) ?></td>
                    <td style="text-align: right;"><?= FormatRupiah($val->grandtotal) ?></td>
                    <td style="text-align: right;"><?= $val->namacabang ?></td>
                    <!-- <td style="text-align: right;"><-?= $val->discbarang ?>%</td>
                    <td style="text-align: right;"><-?= FormatRupiah($val->total) ?></td> -->
                    <?php $grandtotal += $val->grandtotal++; ?>
                </tr>
            <?php endforeach; ?>
            <tr>
                <td colspan="8">
                    <hr>
                </td>
            </tr>
            <tr style="text-align: right; font-weight: bold; font-size: 10px;">
                <td style="text-align: right;" colspan="6">Grand Total : </td>
                <td style="text-align: right;" colspan="1"><?= FormatRupiah($grandtotal) ?></td>
                <td style="text-align: right;" colspan="1"></td>
            </tr>
        </tbody>
    </table>
</body>