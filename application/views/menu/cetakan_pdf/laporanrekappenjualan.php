<?php
function FormatRupiah($angka)
{

	$hasil_rupiah = number_format($angka);
	// $hasil_rupiah = "Rp. " . number_format($angka);
	return $hasil_rupiah;
}

function tanggal_indo($tanggal)
{
	$bulan = [
		1 => 'Januari',
		'Februari',
		'Maret',
		'April',
		'Mei',
		'Juni',
		'Juli',
		'Agustus',
		'September',
		'Oktober',
		'November',
		'Desember'
	];

	$pecahkan = explode('-', $tanggal);

	return $pecahkan[0] . ' ' . $bulan[(int)$pecahkan[1]] . ' ' . $pecahkan[2];
}
?>
<style>
	* {
		font-family: 'Courier New', Courier, monospace;
		/* font-weight: bold; */
	}

	.cf:before,
	.cf:after {
		content: " ";
		/* 1 */
		display: table;
		/* 2 */
	}

	.cf:after {
		clear: both;
	}

	/**
 * For IE 6/7 only
 * Include this rule to trigger hasLayout and contain floats.
 */
	.cf {
		*zoom: 1;
	}

	table {
		/* font-family: Verdana, Arial, Helvetica, sans-serif; */
		border-collapse: collapse;
		width: 100%;
	}

	th {
		border: 1px solid #333;
		text-align: left;
		padding: 8px;
	}

	tr {
		line-height: 1.5rem;
	}

	div {
		font-size: 12;
		/* border: 1px solid #68478D; */
	}
</style>
<title>LAPORAN PENJUALAN SEMUA PELANGGAN | <?= tanggal_indo(date('d-m-Y', strtotime($tglawal))) ?> - <?= tanggal_indo(date('d-m-Y', strtotime($tglakhir))) ?></title>

<body>
	<script type="text/php">
		if (isset($pdf)) {
            $x = 515;
            $y = 80;
            $size = 8;
            $color = array(0,0,0);
            $text = "{PAGE_NUM} / {PAGE_COUNT}";
            $font = $fontMetrics->get_font("Courier");
            $pdf->page_text($x, $y, $text, $font, $size, $color, 0.0, 0.0, 0.0);
        }
    </script>
	<h4 style="text-align: center;">LAPORAN REKAP PENJUALAN</h4>
	<div class="row cf" style="margin-bottom: 2px;">
		<div class="col cf" style="float: left; width: 50%; text-align: left;">
			<div style="font-size: 13px; width: 20%; float: left;">
				CABANG
			</div>
			<div style="font-size: 13px; width: 80%; float: right;">
				: <?= $cabang ?>
			</div>
		</div>
	</div>
	<div class="row cf" style="margin-bottom: 2px;">
		<div class="col" style="float: right; width: 50%; text-align: right;">
			<div style="font-size: 13px; width: 45%; float: left;">
			</div>
			<div style="font-size: 13px; width: 55%; float: right; margin-right: 86px;">
				Halaman
				:
			</div>
		</div>
	</div>
	<div class="row cf" style="margin-bottom: 2px;">
		<div class="col cf" style="float: left; width: 50%; text-align: left;">
			<div style="font-size: 13px; width: 20%; float: left;">
				PERIODE
			</div>
			<div style="font-size: 13px; width: 80%; float: right;">
				: <?= date('d-m-Y', strtotime($tglawal)) ?> <sup style="font-size: 10px;">s</sup>/<sub style="font-size: 10px;">d</sub> <?= date('d-m-Y', strtotime($tglakhir)) ?>
			</div>
		</div>
		<div class="col" style="float: right; width: 50%; text-align: right;">
			<div style="font-size: 13px; width: 45%; float: left;">
			</div>
			<div style="font-size: 13px; width: 55%; float: right;">
				Dicetak
				: <?= date('d-m-Y') ?>
			</div>
		</div>
	</div>
	<br>

	<table style="font-size: 10px; text-align:justify;" width="100%">
		<thead>
			<tr>
				<!-- <th>Tanggal</th> -->
				<th>No.Invoice</th>
				<th>Pelanggan</th>
				<th>Tanggal</th>
				<th>Subtotal</th>
				<th>Diskon</th>
				<th>DPP</th>
				<th>PPN</th>
				<th>Total</th>
			</tr>
		</thead>
		<tbody>
			<?php
			foreach ($report as $values) :
			?>
				<tr>
					<td style="text-align: left; font-weight: bold;">Pemabayaran : <?php if($values->jenisjual == 1 ){
						echo "Tunai";
					} else if($values->jenisjual == 2){
						echo "Kredit";
					} else if($values->jenisjual == 3){
						echo "COD";
					} else {
						echo "Tunai 0%";
					} ?></td>
				</tr>
				<?php $jenisjual = $values->jenisjual ?>
				<?php foreach ($rekap as $val) : 
					?>
					<?php if ($val->jenisjual == $jenisjual) : ?>

						<tr>
							<td style="text-align: center;"><?= $val->nomor ?></td>
							<td style="text-align: center;"><?= $val->namapelanggan ?></td>
							<td style="text-align: center;"><?= date('d-m-y', strtotime($val->tanggal)) ?></td>
							<td style="text-align: center;"><?= $val->total ?></td>
							<td style="text-align: center;"></td>
							<td style="text-align: center;"></td>
							<td style="text-align: center;"></td></td>
							<td style="text-align: center;"></td>
						</tr>
					<?php endif; ?>
				<?php endforeach; ?>
				<tr>
					<td colspan="9">
						<hr>
					</td>
					
				</tr>
				<tr>
				<td colspan="5" style="text-align: right;">Total :</td>
					<td colspan="1"></td>
				</tr>
				<tr>
					<td colspan="5" style="text-align: right;">GrandTotal : </td>
					<td colspan="1"></td>
				</tr>
			<?php endforeach; ?>

		</tbody>
	</table>
</body>
