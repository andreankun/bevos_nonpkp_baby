<?php
function FormatRupiah($angka)
{

    $hasil_rupiah = number_format($angka);
    // $hasil_rupiah = "Rp. " . number_format($angka);
    return $hasil_rupiah;
}

function tanggal_indo($tanggal)
{
    $bulan = [
        1 => 'Januari',
        'Februari',
        'Maret',
        'April',
        'Mei',
        'Juni',
        'Juli',
        'Agustus',
        'September',
        'Oktober',
        'November',
        'Desember'
    ];

    $pecahkan = explode('-', $tanggal);

    return $pecahkan[0] . ' ' . $bulan[(int)$pecahkan[1]] . ' ' . $pecahkan[2];
}
?>
<style>
    * {
        font-family: 'Courier New', Courier, monospace;
        /* font-weight: bold; */
    }

    .cf:before,
    .cf:after {
        content: " ";
        /* 1 */
        display: table;
        /* 2 */
    }

    .cf:after {
        clear: both;
    }

    /**
 * For IE 6/7 only
 * Include this rule to trigger hasLayout and contain floats.
 */
    .cf {
        *zoom: 1;
    }

    table {
        /* font-family: Verdana, Arial, Helvetica, sans-serif; */
        border-collapse: collapse;
        width: 100%;
    }

    th {
        border: 1px solid #333;
        text-align: left;
        padding: 8px;
    }

    div {
        font-size: 12;
        /* border: 1px solid #68478D; */
    }
</style>
<title>LAPORAN KARTU PIUTANG USAHA | <?= tanggal_indo(date('d-m-Y', strtotime($tglawal))) ?> - <?= tanggal_indo(date('d-m-Y', strtotime($tglakhir))) ?></title>

<body>
    <!-- <script type="text/php">
        if (isset($pdf)) {
            $x = 515;
            $y = 80;
            $size = 8;
            $color = array(0,0,0);
            $text = "{PAGE_NUM} / {PAGE_COUNT}";
            $font = $fontMetrics->get_font("Courier");
            $pdf->page_text($x, $y, $text, $font, $size, $color, 0.0, 0.0, 0.0);
        }
    </script> -->
    <h4 style="text-align: center;">LAPORAN KARTU PIUTANG USAHA</h4>
    <div class="row cf" style="margin-bottom: 2px;">
        <div class="col cf" style="float: left; width: 50%; text-align: left;">
            <div style="font-size: 13px; width: 20%; float: left;">
                CABANG
            </div>
            <div style="font-size: 13px; width: 80%; float: right;">
                : <?= $row->nama ?>
            </div>
        </div>
        <div class="col" style="float: right; width: 50%; text-align: right;">
            <div style="font-size: 13px; width: 45%; float: left;">
            </div>
            <div style="font-size: 13px; width: 55%; float: right; margin-right: 86px;">
                <!-- Halaman
                : -->
            </div>
        </div>
    </div>
    <div class="row cf" style="margin-bottom: 2px;">
        <div class="col cf" style="float: left; width: 50%; text-align: left;">
            <div style="font-size: 13px; width: 20%; float: left;">
                PERIODE
            </div>
            <div style="font-size: 13px; width: 80%; float: right;">
                : <?= date('d-m-Y', strtotime($tglawal)) ?> <sup style="font-size: 10px;">s</sup>/<sub style="font-size: 10px;">d</sub> <?= date('d-m-Y', strtotime($tglakhir)) ?>
            </div>
        </div>
        <div class="col" style="float: right; width: 50%; text-align: right;">
            <div style="font-size: 13px; width: 45%; float: left;">
            </div>
            <div style="font-size: 13px; width: 55%; float: right;">
                Dicetak
                : <?= date('d-m-Y') ?>
            </div>
        </div>
    </div>
    <br>

    <table style="font-size: 12px; text-align:justify;">
        <thead>
            <tr>
                <th>No.</th>
                <th>No.Faktur</th>
                <th>Tanggal</th>
                <th>Jatuh Tempo</th>
                <th>Piutang</th>
                <th>Terbayar</th>
                <th>Mutasi Saldo</th>
            </tr>
        </thead>
        <tbody>
            <?php if (!empty($list)) : ?>
                <?php
                $saldoawal = 0;
                $nilaipiutang = 0;
                $nilaipenerimaan = 0;
                $nilaialokasi = 0;
                foreach ($list as $val) :
                    $nopelanggan = $val->nopelanggan;
                ?>
                    <tr>
                        <td style="text-align: left; font-weight: bold;" colspan="5"><?= $val->nama_toko; ?></td>
                        <td style="text-align: right; font-weight: bold;" colspan="1">Saldo Awal:</td>
                        <?php if ($val->nilaipembulatan > 0) : ?>
                            <td style="text-align: right; font-weight: bold;" colspan="1"><?= FormatRupiah($val->saldoawal + $val->nilaipiutang - $val->terbayar - $val->nilaipembulatan); ?></td>
                        <?php else : ?>
                            <td style="text-align: right; font-weight: bold;" colspan="1"><?= FormatRupiah($val->saldoawal + $val->nilaipiutang - $val->terbayar - $val->nilaipembulatan); ?></td>
                        <?php endif; ?>
                    </tr>
                    <?php
                    $no = 1;
                    foreach ($report as $value) : ?>
                        <?php if ($value->nopelanggan == $nopelanggan) :
                        ?>
                            <tr>
                                <td style="text-align: center;"><?= $no++ ?></td>
                                <td style="text-align: left;"><?= $value->nomor ?></td>
                                <td style="text-align: right;"><?= date('d-m-Y', strtotime($value->tanggal)) ?></td>
                                <td style="text-align: right;"><?= date('d-m-Y', strtotime($value->tgljthtempo)) ?></td>
                                <td style="text-align: right;"><?= FormatRupiah($value->nilaipiutang) ?></td>
                                <td style="text-align: right;"><?= FormatRupiah($value->nilaialokasi + $value->nilaipenerimaan) ?></td>
                                <td style="text-align: right;"><?= FormatRupiah($value->nilaipiutang - $value->nilaipenerimaan - $value->nilaialokasi) ?></td>
                            </tr>
                        <?php endif; ?>
                    <?php endforeach; ?>
                    <tr>
                        <td colspan="7">
                            <br>
                        </td>
                    </tr>
                <?php endforeach; ?>
            <?php else : ?>
                <tr>
                    <td style="text-align: center;" colspan="7">Data tidak tersedia.</td>
                </tr>
            <?php endif; ?>
        </tbody>
    </table>
</body>