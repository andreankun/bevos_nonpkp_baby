<?php
function rupiah($angka)
{

	$hasil_rupiah = number_format($angka);
	// $hasil_rupiah = "Rp. " . number_format($angka);
	return $hasil_rupiah;
}

function tanggal_indo($tanggal)
{
	$bulan = [
		1 => 'Januari',
		'Februari',
		'Maret',
		'April',
		'Mei',
		'Juni',
		'Juli',
		'Agustus',
		'September',
		'Oktober',
		'November',
		'Desember'
	];

	$pecahkan = explode('-', $tanggal);

	return $pecahkan[0] . ' ' . $bulan[(int)$pecahkan[1]] . ' ' . $pecahkan[2];
}
?>
<style>
	* {
		font-family: 'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif;
		/* font-family: Arial, Helvetica, sans-serif; */
		/* font-family: 'Courier New', Courier, monospace; */
		/* font-weight: bold; */
	}

	@page {
		margin-top: 25px;
	}

	.cf:before,
	.cf:after {
		content: " ";
		/* 1 */
		display: table;
		/* 2 */
	}

	.cf:after {
		clear: both;
	}

	/**
 * For IE 6/7 only
 * Include this rule to trigger hasLayout and contain floats.
 */
	.cf {
		*zoom: 1;
	}

	table {
		/* font-family: Verdana, Arial, Helvetica, sans-serif; */
		border-collapse: collapse;
		width: 100%;
	}

	th {
		border: 1px solid #333;
		text-align: left;
		padding: 3px;
	}

	div {
		font-size: 12;
		/* border: 1px solid #68478D; */
	}
</style>
<title>Sales Order - <?= $so->nomor ?></title>

<div class="row cf">
	<div class="col-sm-4" style="float: right; font-size: 12px;">
		<?php if ($so->jenisjual == 1) {
			echo "Tunai";
		} else if ($so->jenisjual == 2) {
			echo "Kredit";
		} ?>
	</div>
</div>

<div class="row cf">
	<div class="col" style="width: 100%; float: left; text-align: left;">
		<div style="font-size: 13pt;">
			<h3>PROFORMA INVOICE</h3>
		</div>
	</div>
</div>
<div class="row cf">
	<div class="col" style="float: right; text-align: right; width: 100%; text-transform: uppercase;">
		<div style="font-size: 12px;">
			<?= $so->nama ?>
		</div>
	</div>
</div>

<div class="row cf" style="margin-bottom: 2px;">
	<div class="col cf" style="float: left; width: 50%; text-align: left;">
		<div style="font-size: 12px; width: 45%; float: left;">
			NO. SALES ORDER
		</div>
		<div style="font-size: 12px; width: 55%; float: right;">
			: <?= $so->nomor ?>
		</div>
	</div>
	<div class="col" style="float: right; width: 50%; text-align: right;">
		<div style="font-size: 12px; text-transform: uppercase;">
			<?= $so->alamat ?>
		</div>
	</div>
</div>

<div class="row cf" style="margin-bottom: 2px;">
	<div class="col cf" style="float: left; width: 50%; text-align: left;">
		<div style="font-size: 12px; width: 45%; float: left;">
			NO. PROFORMA
		</div>
		<div style="font-size: 12px; width: 55%; float: right;">
			: <?= substr($konfigurasi->kode, 0, 1) ?><?= $so->nomor ?>
		</div>
	</div>
	<div class="col" style="float: right; width: 50%; text-align: right;">
		<div style="font-size: 12px; text-transform: uppercase;">
			<?= $so->kota ?>
		</div>
	</div>
</div>

<div class="row cf" style="margin-bottom: 2px;">
	<div class="col cf" style="float: left; width: 50%; text-align: left;">
		<div style="font-size: 12px; width: 45%; float: left;">
			KATEGORI
		</div>
		<div style="font-size: 12px; width: 55%; float: right;">
			: <?= $so->namagudang ?>
		</div>
	</div>
	<div class="col" style="float: right; width: 50%; text-align: right;">
		<div style="font-size: 12px; text-transform: uppercase;">
			<?php if (!empty($so->notelp)) : ?>
				<?= $so->notelp ?>
			<?php elseif (!empty($so->nohp)) : ?>
				<?= $so->nohp ?>
			<?php endif; ?>
		</div>
	</div>
</div>
<table style="margin-bottom: 0;" cellpadding="4" cellspacing="0">
	<thead>
		<tr>
			<th style="text-align: center; font-size: 13px;">No</th>
			<th style="text-align: center; font-size: 13px;">Banyaknya</th>
			<th style="text-align: center; font-size: 13px;">Nama Produk</th>
		</tr>
	</thead>
	<tbody>
		<?php
		$no = 1;
		foreach ($sod as $values) :
		?>
			<tr>
				<td style="font-size: 12px; text-align: center;"><?= $no++ ?></td>
				<td style="font-size: 12px; text-align: center;"><?= $values->qty ?>&nbsp;<?= $values->namasatuan ?></td>
				<td style="font-size: 12px;"><?= $values->namabarang ?></td>
			</tr>
		<?php endforeach; ?>
	</tbody>
</table>
<hr>
<div class="row">
	<div class="col-sm-8" style="float: right; width: 20%; font-size: 12px;">
		<?php $sum = 0;
		foreach ($sod as $cell) : ?>
			<?php $sum += $cell->qty++ ?>
		<?php endforeach ?>
		<b style="float: left;">Total Qty : </b> <b style="float: right;"><?= rupiah($sum) ?></b>
	</div>
	<div class="col-sm-4" style="font-size: 12px; width: 70%; text-transform: uppercase;">
		TUJUAN PENGIRIMAN : <?= $so->nama_toko ?>
	</div>
</div>

<div class="row">
	<div class="col-sm-4" style="font-size: 12px; text-transform: uppercase; width: 50%;">
		<?= $so->pengiriman ?>
	</div>
</div>
<div class="row">
	<div class="col-sm-4" style="font-size: 12px;">
		PAKET PROMO : <?= $so->namapromo ?>
	</div>
</div>

<div class="row">
	<div class="col-sm-4" style="font-size: 12px;">
		Keterangan : <?= $so->keterangan ?>
	</div>
</div>