<?php
function FormatRupiah($angka)
{

    $hasil_rupiah = number_format($angka);
    // $hasil_rupiah = "Rp. " . number_format($angka);
    return $hasil_rupiah;
}

function tanggal_indo($tanggal)
{
    $bulan = [
        1 => 'Januari',
        'Februari',
        'Maret',
        'April',
        'Mei',
        'Juni',
        'Juli',
        'Agustus',
        'September',
        'Oktober',
        'November',
        'Desember'
    ];

    $pecahkan = explode('-', $tanggal);

    return $pecahkan[0] . ' ' . $bulan[(int)$pecahkan[1]] . ' ' . $pecahkan[2];
}
?>
<style>
    * {
        font-family: 'Courier New', Courier, monospace;
        /* font-weight: bold; */
    }

    .cf:before,
    .cf:after {
        content: " ";
        /* 1 */
        display: table;
        /* 2 */
    }

    .cf:after {
        clear: both;
    }

    /**
 * For IE 6/7 only
 * Include this rule to trigger hasLayout and contain floats.
 */
    .cf {
        *zoom: 1;
    }

    table {
        /* font-family: Verdana, Arial, Helvetica, sans-serif; */
        border-collapse: collapse;
        width: 100%;
    }

    th {
        border: 1px solid #333;
        text-align: left;
        padding: 8px;
    }

    div {
        font-size: 12;
        /* border: 1px solid #68478D; */
    }
</style>
<title>LAPORAN PIUTANG PER PELANGGAN | <?= tanggal_indo(date('d-m-Y', strtotime($tglawal))) ?> - <?= tanggal_indo(date('d-m-Y', strtotime($tglakhir))) ?></title>

<body>
    <!-- <script type="text/php">
        if (isset($pdf)) {
            $x = 515;
            $y = 108;
            $size = 8;
            $color = array(0,0,0);
            $text = "{PAGE_NUM} / {PAGE_COUNT}";
            $font = $fontMetrics->get_font("Courier");
            $pdf->page_text($x, $y, $text, $font, $size, $color, 0.0, 0.0, 0.0);
        }
    </script> -->
    <h4 style="text-align: center;">LAPORAN PIUTANG PER PELANGGAN</h4>
    <div class="row cf" style="margin-bottom: 2px;">
        <div class="col cf" style="float: left; width: 50%; text-align: left;">
            <div style="font-size: 13px; width: 30%; float: left;">
                CABANG
            </div>
            <div style="font-size: 13px; width: 70%; float: right;">
                : <?= $row->nama ?>
            </div>
        </div>
        <div class="col" style="float: right; width: 50%; text-align: right;">
            <div style="font-size: 13px; width: 45%; float: left;">
            </div>
            <div style="font-size: 13px; width: 55%; float: right; margin-right: 86px;">
            </div>
        </div>
    </div>
    <div class="row cf" style="margin-bottom: 2px;">
        <div class="col cf" style="float: left; width: 50%; text-align: left;">
            <div style="font-size: 13px; width: 30%; float: left;">
                PELANGGAN
            </div>
            <div style="font-size: 13px; width: 70%; float: right;">
                : <?= $pelanggan->nama ?>
            </div>
        </div>
        <div class="col" style="float: right; width: 50%; text-align: right;">
            <div style="font-size: 13px; width: 45%; float: left;">
            </div>
            <div style="font-size: 13px; width: 55%; float: right; margin-right: 86px;">
            </div>
        </div>
    </div>
    <div class="row cf" style="margin-bottom: 2px;">
        <div class="col cf" style="float: left; width: 50%; text-align: left;">
            <div style="font-size: 13px; width: 30%; float: left;">
                <!-- NAMA TOKO -->
            </div>
            <div style="font-size: 13px; width: 70%; float: right;">
                <!-- : <?= $penjualanperpelangganrow->nama_toko ?> -->
            </div>
        </div>
        <div class="col" style="float: right; width: 50%; text-align: right;">
            <div style="font-size: 13px; width: 45%; float: left;">
            </div>
            <div style="font-size: 13px; width: 55%; float: right; margin-right: 86px;">
                <!-- Halaman
                : -->
            </div>
        </div>
    </div>
    <div class="row cf" style="margin-bottom: 2px;">
        <div class="col cf" style="float: left; width: 50%; text-align: left;">
            <div style="font-size: 13px; width: 30%; float: left;">
                PERIODE
            </div>
            <div style="font-size: 13px; width: 70%; float: right;">
                : <?= date('d-m-Y', strtotime($tglawal)) ?> <sup style="font-size: 10px;">s</sup>/<sub style="font-size: 10px;">d</sub> <?= date('d-m-Y', strtotime($tglakhir)) ?>
            </div>
        </div>
        <div class="col" style="float: right; width: 50%; text-align: right;">
            <div style="font-size: 13px; width: 45%; float: left;">
            </div>
            <div style="font-size: 13px; width: 55%; float: right;">
                Dicetak
                : <?= date('d-m-Y') ?>
            </div>
        </div>
    </div>
    <br>

    <table style="font-size: 12px; text-align:justify;">
        <thead>
            <tr>
                <th style="text-align: center;">No.Faktur</th>
                <th style="text-align: center;">Tanggal</th>
                <th style="text-align: center;">Jatuh Tempo</th>
                <th style="text-align: center;">Piutang</th>
                <th style="text-align: center;">Tgl. Bayar</th>
                <th style="text-align: center;">Terbayar</th>
                <th style="text-align: center;">Mutasi Saldo</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $saldoawal = 0;
            $nilaipiutang = 0;
            $nilaipenerimaan = 0;
            $nilaialokasi = 0;
            foreach ($report as $val) :
                $nilaipiutang = $nilaipiutang + $val->nilaipiutang;
                $nilaipenerimaan = $nilaipenerimaan + $val->nilaipenerimaan;
                $nilaialokasi = $nilaialokasi + $val->nilaialokasi;
                $saldoawal = $nilaipiutang - $nilaialokasi - $nilaipenerimaan;
            ?>
            <?php endforeach; ?>
            <tr>
                <td colspan="5"></td>
                <td style="text-align: center; font-weight: bold;" colspan="1">Saldo Awal:</td>
                <td style="text-align: right; font-weight: bold;" colspan="1"><?= FormatRupiah($saldoawal) ?></td>
            </tr>
            <tr>
                <td>
                    <br>
                </td>
            </tr>
            <?php
            $totalpiutang = 0;
            $totalbayar = 0;
            foreach ($report as $value) :
            ?>
                <tr>
                    <td style="text-align: left;"><?= $value->nomor; ?></td>
                    <td style="text-align: left;"><?= date('d-m-Y', strtotime($value->tanggal)); ?></td>
                    <td style="text-align: left;"><?= date('d-m-Y', strtotime($value->tgljthtempo)); ?></td>
                    <td style="text-align: right;"><?= FormatRupiah($value->nilaipiutang); ?></td>
                    <td style="text-align: right;" colspan="3"><?= FormatRupiah($value->nilaipiutang); ?></td>
                </tr>
                <tr>
                    <?php if (!empty($value->tglbayar)) : ?>
                        <td style="text-align:right;" colspan="5"><?= date('d-m-Y', strtotime($value->tglbayar)); ?></td>
                    <?php else : ?>
                        <td style="text-align:right;" colspan="5"><?= '' ?></td>
                    <?php endif; ?>
                    <td style="text-align:right;"><?= FormatRupiah($value->terbayar) ?></td>
                    <td style="text-align:right;"><?= FormatRupiah($value->mutasisaldo) ?></td>
                </tr>
                <tr>
                    <td>
                        <br>
                    </td>
                </tr>
            <?php
                $totalpiutang = $totalpiutang + $value->nilaipiutang;
                $totalbayar = $totalbayar + $value->terbayar;
            endforeach;
            ?>
            <tr>
                <td colspan="7">
                    <hr>
                </td>
            </tr>
            <tr>
                <td colspan="3" style="font-weight: bold; text-align: right;">Total</td>
                <td colspan="1" style="text-align: right;"><?= FormatRupiah($totalpiutang) ?></td>
                <td colspan="2" style="text-align: right;"><?= FormatRupiah($totalbayar) ?></td>
            </tr>
        </tbody>
    </table>
</body>