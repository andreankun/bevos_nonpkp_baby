<?php
function FormatRupiah($angka)
{

	$hasil_rupiah = number_format($angka);
	// $hasil_rupiah = "Rp. " . number_format($angka);
	return $hasil_rupiah;
}

function tanggal_indo($tanggal)
{
	$bulan = [
		1 => 'Januari',
		'Februari',
		'Maret',
		'April',
		'Mei',
		'Juni',
		'Juli',
		'Agustus',
		'September',
		'Oktober',
		'November',
		'Desember'
	];

	$pecahkan = explode('-', $tanggal);

	return $pecahkan[0] . ' ' . $bulan[(int)$pecahkan[1]] . ' ' . $pecahkan[2];
}
?>
<style>
	* {
		font-family: 'Courier New', Courier, monospace;
		/* font-weight: bold; */
	}

	.cf:before,
	.cf:after {
		content: " ";
		/* 1 */
		display: table;
		/* 2 */
	}

	.cf:after {
		clear: both;
	}

	/**
 * For IE 6/7 only
 * Include this rule to trigger hasLayout and contain floats.
 */
	.cf {
		*zoom: 1;
	}

	table {
		/* font-family: Verdana, Arial, Helvetica, sans-serif; */
		border-collapse: collapse;
		width: 100%;
	}

	th {
		border: 1px solid #333;
		text-align: left;
		padding: 8px;
	}

	div {
		font-size: 12;
		/* border: 1px solid #68478D; */
	}
</style>
<title>LAPORAN REKAP PENJUALAN | <?= tanggal_indo(date('d-m-Y', strtotime($tglawal))) ?> - <?= tanggal_indo(date('d-m-Y', strtotime($tglakhir))) ?></title>

<body>
	<!-- Text halaman & text yang hanya dihalaman terakhir -->
	<!-- <script type="text/php">
		/// Halaman
        if (isset($pdf)) {
            $x = 440;
            $y = 77.4;
            $size = 8;
            $color = array(0,0,0);
            $text = "{PAGE_NUM} / {PAGE_COUNT}";
            $font = $fontMetrics->get_font("helvetica");
            $pdf->page_text($x, $y, $text, $font, $size, $color, 0.0, 0.0, 0.0);
        }
        /// End Halaman
        </script> -->
	<div class="row cf" style="margin-bottom: 2px;">
		<div class="col cf" style="float: left; width: 50%; text-align: left;">
			<div style="font-size: 13px; width: 20%; float: left;">
				CABANG
			</div>
			<div style="font-size: 13px; width: 80%; float: right;">
				: <?= $cabang ?>
			</div>
		</div>
		<div class="col" style="float: right; width: 50%; text-align: right;">
			<div style="font-size: 13px; width: 45%; float: left;">
			</div>
			<div style="font-size: 13px; width: 55%; float: right;">
				<!-- Halaman
                : 1 / 1 -->
			</div>
		</div>
	</div>
	<div class="row cf" style="margin-bottom: 2px;">
		<div class="col" style="float: right; width: 50%; text-align: right;">
			<div style="font-size: 13px; width: 45%; float: left;">
			</div>
			<div style="font-size: 13px; width: 55%; float: right;">
				Halaman
				:
			</div>
		</div>
		<div class="row cf" style="margin-bottom: 2px;">
			<div class="col cf" style="float: left; width: 80%; text-align: left;">
				<div style="font-size: 13px; width: 12.5%; float: left;">
					PERIODE
				</div>
				<div style="font-size: 13px; width: 80%; float: left;">
					: <?= date('d-m-Y', strtotime($tglawal)) ?> <sup style="font-size: 10px;">s</sup>/<sub style="font-size: 10px;">d</sub> <?= date('d-m-Y', strtotime($tglakhir)) ?>
				</div>
			</div>
			<div class="col" style="float: right; width: 50%; text-align: right;">
				<div style="font-size: 13px; width: 45%; float: left;">
				</div>
				<div style="font-size: 13px; width: 55%; float: right;">
					Dicetak
					: <?= date('d-m-Y') ?>
				</div>
			</div>
		</div>
		<br>

		<table style="font-size: 10px; text-align:justify;">
			<thead>
				<tr>
					<th>No</th>
					<th>Invoice</th>
					<th>Pelanggan</th>
					<th>Tanggal</th>
					<th>Subtotal</th>
					<th>Diskon</th>
					<th>DPP</th>
					<th>PPN</th>
					<th>Total</th>
				</tr>
			</thead>
			<tbody>
			<?php 
				foreach ($reportrow as $val) : ?>
				
					<?php $jenisjual = $val->jenisjual ?>
					<!-- <?php $namamerk = $val->namamerk ?> -->
					<tr>
						<td colspan="8" style="font-weight: bold; font-size: 12px">Methode Pembayaran : <?php if($jenisjual == 1){
							echo "TUNAI";
						} else if($jenisjual == 2) {
							echo "KREDIT";
						} else if($jenisjual == 3) {
							echo "COD";
						} else {
							echo "TUNAI 0%";
						} ?></td>
					</tr>
					<?php 
					$total = 0;
					$totaldiskon1 = 0;
					$totaldpp = 0;
					$totalppn = 0;
					$grandtotal = 0;
					$no = 1;
					$diskon = 0;
					$totaldiskon = 0;
					foreach ($reportlist as $vals) : ?>
						<?php if ($vals->jenisjual == $jenisjual) : ?>
							<!-- <?php $total = $vals->qty + $vals->hargajual ?> -->
							<?php $totaldiskon = $vals->disccash + $vals->disccustomer?>
							<?php $diskon = $vals->total * $totaldiskon / 100 ?>
							<tr>
								<td style="text-align: left;"><?= $no++ ?></td>
								<td style="text-align: left;"><?= $vals->nomor ?></td>
								<td style="text-align: left;"><?= $vals->nama ?></td>
								<td style="text-align: left;"><?= date('d-m-Y', strtotime($vals->tanggal)) ?></td>
								<td style="text-align: right;"><?= FormatRupiah($vals->total) ?></td>
								<td style="text-align: right;"><?= FormatRupiah($diskon) ?></td>
								<td style="text-align: right;"><?= FormatRupiah($vals->dpp) ?></td>
								<td style="text-align: right;"><?= FormatRupiah($vals->ppn) ?></td>
								<td style="text-align: right;"><?= FormatRupiah($vals->grandtotal) ?></td>
							</tr>
							<?php $total = $total + $vals->total ?>
							<?php $totaldiskon1 = $totaldiskon1 + $diskon ?>
							<?php $totaldpp = $totaldpp + $vals->dpp ?>
							<?php $totalppn = $totalppn + $vals->ppn ?>
							<?php $grandtotal = $grandtotal + $vals->grandtotal ?>
						<?php endif ?>
						
					<?php endforeach; ?>
					<tr>
						<td colspan="9">
							<hr>
						</td>
					</tr>
					<tr>
						<td colspan="4" style="text-align: right;">total : </td>
						<td style="text-align: right;"><?= FormatRupiah($total) ?></td>
						<td style="text-align: right;"><?= FormatRupiah($totaldiskon1) ?></td>
						<td style="text-align: right;"><?= FormatRupiah($totaldpp) ?></td>
						<td style="text-align: right;"><?= FormatRupiah($totalppn) ?></td>
						<td style="text-align: right;"><?= FormatRupiah($grandtotal) ?></td>
					</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
</body>
