<?php
function FormatRupiah($angka)
{

    $hasil_rupiah = number_format($angka);
    // $hasil_rupiah = "Rp. " . number_format($angka);
    return $hasil_rupiah;
}

function tanggal_indo($tanggal)
{
    $bulan = [
        1 => 'Januari',
        'Februari',
        'Maret',
        'April',
        'Mei',
        'Juni',
        'Juli',
        'Agustus',
        'September',
        'Oktober',
        'November',
        'Desember'
    ];

    $pecahkan = explode('-', $tanggal);

    return $pecahkan[0] . ' ' . $bulan[(int)$pecahkan[1]] . ' ' . $pecahkan[2];
}
?>
<style>
    * {
        font-family: 'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif;
        /* font-family: 'Courier New', Courier, monospace; */
        /* font-weight: bold; */
    }

    @page {
        margin-top: 25px;
    }

    .cf:before,
    .cf:after {
        content: " ";
        /* 1 */
        display: table;
        /* 2 */
    }

    .cf:after {
        clear: both;
    }

    /**
 * For IE 6/7 only
 * Include this rule to trigger hasLayout and contain floats.
 */
    .cf {
        *zoom: 1;
    }

    table {
        /* font-family: Verdana, Arial, Helvetica, sans-serif; */
        border-collapse: collapse;
        width: 100%;
    }

    th {
        border: 1px solid #333;
        text-align: left;
        padding: 8px;
    }

    div {
        font-size: 12;
        /* border: 1px solid #68478D; */
    }
</style>
<title>LAPORAN REKAP PENJUALAN PER PELANGGAN | <?= tanggal_indo(date('d-m-Y', strtotime($tglawal))) ?> - <?= tanggal_indo(date('d-m-Y', strtotime($tglakhir))) ?></title>

<body>
    <!-- <script type="text/php">
        if (isset($pdf)) {
            $x = 515;
            $y = 80;
            $size = 8;
            $color = array(0,0,0);
            $text = "{PAGE_NUM} / {PAGE_COUNT}";
            $font = $fontMetrics->get_font("Courier");
            $pdf->page_text($x, $y, $text, $font, $size, $color, 0.0, 0.0, 0.0);
        }
    </script> -->
    <h4 style="text-align: center;">LAPORAN REKAP PENJUALAN PER PELANGGAN</h4>
    <div class="row cf" style="margin-bottom: 2px;">
        <div class="col cf" style="float: left; width: 50%; text-align: left;">
            <div style="font-size: 13px; width: 30%; float: left;">
                CABANG
            </div>
            <div style="font-size: 13px; width: 70%; float: right;">
                : <?= $row->nama ?>
            </div>
        </div>
        <div class="col" style="float: right; width: 50%; text-align: right;">
            <div style="font-size: 13px; width: 45%; float: left;">
            </div>
            <div style="font-size: 13px; width: 55%; float: right; margin-right: 86px;">
            </div>
        </div>
    </div>
    <div class="row cf" style="margin-bottom: 2px;">
        <div class="col cf" style="float: left; width: 50%; text-align: left;">
            <div style="font-size: 13px; width: 30%; float: left;">
                PERIODE
            </div>
            <div style="font-size: 13px; width: 70%; float: right;">
                : <?= date('d-m-Y', strtotime($tglawal)) ?> <sup style="font-size: 10px;">s</sup>/<sub style="font-size: 10px;">d</sub> <?= date('d-m-Y', strtotime($tglakhir)) ?>
            </div>

        </div>
        <div class="col" style="float: right; width: 50%; text-align: right;">
            <div style="font-size: 13px; width: 45%; float: left;">
            </div>
            <div style="font-size: 13px; width: 55%; float: right; margin-right: 86px;">
            </div>
        </div>
    </div>
    <div class="row cf" style="margin-bottom: 2px;">
        <div class="col cf" style="float: left; width: 50%; text-align: left;">
            <div style="font-size: 13px; width: 30%; float: left;">
                PELANGGAN
            </div>
            <?php if (!empty($rekappenjualanperpelangganrow)) : ?>
                <div style="font-size: 13px; width: 70%; float: right;">
                    : <?= $rekappenjualanperpelangganrow->namapelanggan ?>
                </div>
            <?php else : ?>
                <div style="font-size: 13px; width: 70%; float: right;">
                    : -
                </div>
            <?php endif; ?>
        </div>
        <div class="col" style="float: right; width: 50%; text-align: right;">
            <div style="font-size: 13px; width: 45%; float: left;">
            </div>
            <div style="font-size: 13px; width: 55%; float: right; margin-right: 86px;">
                <!-- Halaman
                : -->
            </div>
        </div>
    </div>
    <div class="row cf" style="margin-bottom: 2px;">
        <div class="col cf" style="float: left; width: 50%; text-align: left;">
            <div style="font-size: 13px; width: 30%; float: left;">
                NAMA TOKO
            </div>
            <?php if (!empty($rekappenjualanperpelangganrow)) : ?>
                <div style="font-size: 13px; width: 70%; float: right;">
                    : <?= $rekappenjualanperpelangganrow->nama_toko ?>
                </div>
            <?php else : ?>
                <div style="font-size: 13px; width: 70%; float: right;">
                    : -
                </div>
            <?php endif; ?>
        </div>
        <div class="col" style="float: right; width: 50%; text-align: right;">
            <div style="font-size: 13px; width: 45%; float: left;">
            </div>
            <div style="font-size: 13px; width: 55%; float: right;">
                Dicetak
                : <?= date('d-m-Y') ?>
            </div>
        </div>
    </div>
    <!-- <br> -->

    <table style="font-size: 10px; text-align:justify;">
        <thead>
            <tr>
                <th style="text-align: center;">No.</th>
                <th style="text-align: center;">INVOICE</th>
                <!-- <th style="text-align: center;">NAMA PELANGGAN</th>
                <th style="text-align: center;">NAMA TOKO</th> -->
                <th style="text-align: center;">TANGGAL</th>
                <th style="text-align: center;">SUBTOTAL</th>
                <th style="text-align: center;">DISKON</th>
                <th style="text-align: center;">DPP</th>
                <th style="text-align: center;">PPN</th>
                <th style="text-align: center;">TOTAL</th>
            </tr>
        </thead>
        <tbody>
            <?php if (!empty($rekappenjualanperpelangganlist)) : ?>
                <?php
                $no = 1;
                foreach ($rekappenjualanperpelangganlist as $value) :
                ?>
                    <tr>
                        <?php if ($value->jenisjual == 2 || $value->jenisjual == 3) : ?>
                            <td colspan="8" style="font-size: 14px; font-weight: bold;">Metode Pembayaran : KREDIT</td>
                        <?php else : ?>
                            <td colspan="8" style="font-size: 14px; font-weight: bold;">Metode Pembayaran : TUNAI</td>
                        <?php endif; ?>
                    </tr>
                    <?php $jenisjual = $value->jenisjual; ?>
                    <?php
                    $gsubtotal = 0;
                    $gdiskon = 0;
                    $gdpp = 0;
                    $gppn = 0;
                    $ggrandtotal = 0;
                    $totals_diskon = 0;
                    $subtotals = 0;
                    $fsubtotals = 0;
                    $fdiskon = 0;
                    $fdpp = 0;
                    $fppn = 0;
                    $fgrandtotal = 0;
                    $hasilppn = 0;
                    $grandtotal = 0;
                    $dpp = 0;
                    foreach ($rekappenjualanperpelanggan as $val) :
                        $subtotals = $val->subtotal;
                        $totals_diskon = $val->nilaicustomer + $val->nilaicash;
                        $dpp = $val->dpp;
                        $hasilppn = $val->ppn;
                        $grandtotal = $val->dpp + $hasilppn;
                    ?>
                        <?php if ($val->jenisjual == $jenisjual) : ?>
                            <tr>
                                <td style="font-size: 12px; text-align: center;"><?= $no++ ?></td>
                                <td style="font-size: 12px; text-align: left;"><?= $val->nomor ?></td>
                                <!-- <td style="font-size: 12px; text-align: left;"><?= $val->namapelanggan ?></td>
                                <td style="font-size: 12px; text-align: left;"><?= $val->nama_toko ?></td> -->
                                <td style="font-size: 12px; text-align: right;"><?= date('d-m-Y', strtotime($val->tanggal)) ?></td>
                                <td style="font-size: 12px; text-align: right;"><?= FormatRupiah($subtotals) ?></td>
                                <td style="font-size: 12px; text-align: right;"><?= FormatRupiah($totals_diskon) ?></td>
                                <td style="font-size: 12px; text-align: right;"><?= FormatRupiah($dpp) ?></td>
                                <td style="font-size: 12px; text-align: right;"><?= FormatRupiah($hasilppn) ?></td>
                                <td style="font-size: 12px; text-align: right;"><?= FormatRupiah($grandtotal) ?></td>
                            </tr>
                            <?php $gsubtotal = $gsubtotal + $subtotals; ?>
                            <?php $gdiskon = $gdiskon + $totals_diskon; ?>
                            <?php $gdpp = $gdpp + $dpp; ?>
                            <?php $gppn = $gppn + $hasilppn; ?>
                            <?php $ggrandtotal = $ggrandtotal + $grandtotal; ?>
                        <?php endif; ?>
                        <?php
                        $fsubtotals = $fsubtotals + $subtotals;
                        $fdiskon = $fdiskon + $totals_diskon;
                        $fdpp = $fdpp + $dpp;
                        $fppn = $fppn + $hasilppn;
                        $fgrandtotal = $fgrandtotal + $grandtotal;
                        ?>
                    <?php endforeach; ?>
                    <tr>
                        <td colspan="8">
                            <hr>
                        </td>
                    </tr>
                    <tr style="float: right; text-align: right; font-weight: bold;">
                        <td colspan="3">Total : </td>
                        <td colspan="1"><?= FormatRupiah($gsubtotal) ?></td>
                        <td colspan="1"><?= FormatRupiah($gdiskon) ?></td>
                        <td colspan="1"><?= FormatRupiah($gdpp) ?></td>
                        <td colspan="1"><?= FormatRupiah($gppn) ?></td>
                        <td colspan="1"><?= FormatRupiah($ggrandtotal) ?></td>
                    </tr>
                    <tr>
                        <td colspan="8">
                            <hr>
                        </td>
                    </tr>
                <?php endforeach; ?>
                <tr style="float: right; text-align: right; font-weight: bold;">
                    <td colspan="3">Grand Total : </td>
                    <td colspan="1"><?= FormatRupiah($fsubtotals) ?></td>
                    <td colspan="1"><?= FormatRupiah($fdiskon) ?></td>
                    <td colspan="1"><?= FormatRupiah($fdpp) ?></td>
                    <td colspan="1"><?= FormatRupiah($fppn) ?></td>
                    <td colspan="1"><?= FormatRupiah($fgrandtotal) ?></td>
                </tr>
            <?php else : ?>
                <tr>
                    <td style="text-align: center;" colspan="8">Data tidak tersedia.</td>
                </tr>
            <?php endif; ?>
        </tbody>
    </table>
</body>