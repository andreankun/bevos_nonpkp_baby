<?php
function rupiah($angka)
{
	$hasil_rupiah = number_format($angka);
	// $hasil_rupiah = "Rp. " . number_format($angka);
	return $hasil_rupiah;
}

function tanggal_indo($tanggal)
{
	$bulan = [
		1 => 'Januari',
		'Februari',
		'Maret',
		'April',
		'Mei',
		'Juni',
		'Juli',
		'Agustus',
		'September',
		'Oktober',
		'November',
		'Desember'
	];

	$pecahkan = explode('-', $tanggal);

	return $pecahkan[0] . ' ' . $bulan[(int)$pecahkan[1]] . ' ' . $pecahkan[2];
}
?>

<style>
	* {
		font-family: 'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif;
		/* font-family: 'Courier New', Courier, monospace; */
		/* font-family: Arial, Helvetica, sans-serif; */
		/* */
		/* font-weight: bold; */
	}

	@page {
		margin-top: 25px;
	}

	.cf:before,
	.cf:after {
		content: " ";
		/* 1 */
		display: table;
		/* 2 */
	}

	.cf:after {
		clear: both;
	}

	/**
 * For IE 6/7 only
 * Include this rule to trigger hasLayout and contain floats.
 */
	.cf {
		*zoom: 1;
	}

	table {
		/* font-family: Verdana, Arial, Helvetica, sans-serif; */
		border-collapse: collapse;
		width: 100%;
	}

	th {
		border: 1px solid #333;
		text-align: left;
		padding: 3px;
	}

	/* tr:nth-child(even) {
		background-color: #dddddd;
	} */

	div {
		font-size: 11;
		/* border: 1px solid #68478D; */
	}
</style>
<title>Invoice - <?= $inv->nomor ?></title>
<div class="row cf">
	<div class="col cf" style="float: right; width: 100%; text-align: right;">
		<div style="font-size: 13px;">
			<?php if (!empty($vinv->revisike)) : ?>
				[RR-<?= $vinv->revisike ?>]
			<?php else : ?>
				<?= '' ?>
			<?php endif; ?>
		</div>
	</div>
</div>
<div class="row cf" style="margin-bottom: 5px;">
	<div class="col" style="float: left; width: 50%; text-align: left;">
		<div style="font-size: 13pt;">
			<b><?= $vinv->namabisnis ?></b>
		</div>
	</div>
	<div class="col" style="float: right; width: 50%; text-align: right;">
		<div style="font-size: 12px;">
			SO# <?= $inv->nomorso ?>
		</div>
		<div style="font-size: 12px;">
			[<?=$vinv->nomorgb.$vinv->rekeningtf ?>]
		</div>
	</div>
</div>

<div class="row cf" style="margin-bottom: 2px;">
	<div class="col" style="width: 100%; font-weight: bold; text-align: center;">
		<div style="font-size: 13pt;">
			INVOICE
		</div>
	</div>
</div>

<div class="row cf" style="margin-bottom: 2px;">
	<div class="col" style="float: right; width: 100%; text-align: right;">
		<div style="font-size: 12px; text-transform: uppercase;">
			<?= $vinv->namacabang ?>, <?= tanggal_indo(date('d-n-Y', strtotime($vinv->tanggal))) ?>
		</div>
	</div>
</div>

<div class="row cf" style="margin-bottom: 2px;">
	<div class="col cf" style="float: left; width:50%; text-align: left; ">
		<div style="font-size: 12px; width: 25%; float: left;">
			NO. FAKTUR
		</div>
		<div style="font-size: 12px; width: 75%; float: right;">
			: <?= $vinv->nomor ?>
		</div>
	</div>
	<div class="col" style="float: right; width: 50%; text-align: right;">
		<div style="font-size: 12px; text-transform: uppercase;">
			<?= $vinv->namapelanggan ?>
		</div>
	</div>
</div>

<div class="row cf" style="margin-bottom: 2px;">
	<div class="col cf" style="float: left; width: 50%; text-align: left;">
		<div style="font-size: 12px; width: 25%; float: left;">
			JATUH TEMPO
		</div>
		<div style="font-size: 12px; width: 75%; float: right; text-transform: uppercase;">
			: <?= tanggal_indo(date('d-n-Y', strtotime($vinv->tgljthtempo))) ?>
		</div>
	</div>
	<div class="col" style="float: right; width: 50%; text-align: right;">
		<div style="font-size: 12px; text-transform: uppercase; text-align: right;">
			<?= $vinv->alamat ?>
		</div>
	</div>
</div>

<div class="row cf" style="margin-bottom: 2px;">
	<div class="col cf" style="float: left; width: 50%; text-align: left;">
		<div style="font-size: 12px; width: 25%; float: left;">
			KODE
		</div>
		<div style="font-size: 12px; width: 75%; float: right;">
			: <?= $vinv->kodesalesman ?>
		</div>
	</div>
	<div class="col" style="float: right; width: 50%; text-align: right;">
		<div style="font-size: 12px; text-transform: uppercase;">
			<?php if (!empty($vinv->kodepos)) : ?>
				<?= $vinv->kodepos ?>
			<?php else : ?>
				-
			<?php endif; ?>
		</div>
	</div>
</div>

<div class="row cf">
	<div class="col cf" style="float: left; width: 50%; text-align: left;">
		<div style="font-size: 12px; width: 25%; float: left;">
			KATEGORI
		</div>
		<div style="font-size: 12px; width: 75%; float: right;">
			: <?= $vinv->lokasi ?>
		</div>
	</div>
	<div class="col" style="float: right; text-align: right; width: 50%;">
		<div style="font-size: 12px;">
			<?php if (!empty($vinv->nohp)) : ?>
				<?= $vinv->nohp ?>
			<?php elseif (!empty($vinv->notelp)) : ?>
				<?= $vinv->notelp ?>
			<?php else : ?>
				<?= '' ?>
			<?php endif; ?>
		</div>
	</div>
</div>
<table style="margin-bottom: 0" cellpadding="4" cellspacing="0">
	<thead>
		<tr>
			<th style="text-align: center; font-size: 13px;">NO.</th>
			<th style="text-align: center; font-size: 13px;">NAMA BARANG</th>
			<th style="text-align: center; font-size: 13px;">QTY</th>
			<th style="text-align: center; font-size: 13px;">HARGA</th>
			<th style="text-align: center; font-size: 13px;">DISC 1</th>
			<th style="text-align: center; font-size: 13px;">DISC 2</th>
			<th style="text-align: center; font-size: 13px;">TOTAL HARGA</th>
		</tr>
	</thead>
	<tbody>
		<?php $no = 1;
		$ttlharga = 0;
		$ttldiskonpromo = 0;
		$ttldiskoncash = 0;
		$ttldiskoncustomer = 0;
		$diskonpromo = 0;
		$diskoncash = 0;
		$diskoncustomer = 0;
		$diskonrp = 0;
		$totalharga = 0;
		foreach ($vinvd as $values) : ?>
			<?php
			// $ttlharga = (($values->qty * $values->harga) - $values->discbrgitem - $values->discpaketitem);
			$ttlharga = $values->total;

			// $ttldiskonpromo = $values->qty * $values->discpaketitem;
			// $ttldiskonpromo = $values->discpaketitem;
			// $diskonpromo = $diskonpromo + $ttldiskonpromo;
			// print_r($ttldiskonpromo);
			// die();

			$ttldiskoncash = $values->nilaicash;
			// $diskoncash = $diskoncash + $ttldiskoncash;

			$ttldiskoncustomer = $values->nilaicustomer;
			// $diskoncustomer = $diskoncustomer + $ttldiskoncustomer;

			$diskonrp = $ttldiskoncash + $ttldiskoncustomer;
			?>
			<tr>
				<td style="font-size: 12px; text-align: center;"><?= $no++ ?></td>
				<td style="font-size: 12px; text-align: left;"><?= $values->namabarang ?></td>
				<td style="font-size: 12px; text-align: center;"><?= $values->qty ?> <?= $values->namasatuan ?></td>
				<td style="font-size: 12px; text-align: right;"><?= rupiah($values->harga) ?></td>
				<td style="font-size: 12px; text-align: center;"><?= $values->discbarang ?>%</td>
				<td style="font-size: 12px; text-align: center;"><?= $values->discpaket ?>%</td>
				<td style="font-size: 12px; text-align: right;"><?= rupiah($ttlharga) ?></td>
			</tr>
		<?php
			$totalharga = $totalharga + $ttlharga;
		endforeach; ?>
	</tbody>
</table>
<hr>

<table border="0" cellspacing="0" cellpadding="0" style="margin-bottom: 13px;">
	<tr>
		<td style="width: 15%; font-size: 13px;">PAKET PROMO</td>
		<td style="width: 1%; font-size: 13px;">:</td>
		<td style="font-size: 13px;"><?= $vinv->namapromo ?></td>
		<td style="width: 10%; text-align: right; font-size: 13px;">SUB TOTAL RP&nbsp;</td>
		<td style="width: 1%; font-size: 13px;"> : </td>
		<td style="text-align: right; font-size: 13px; width: 15%; font-weight: bold;"><?= rupiah($totalharga) ?></td>
	</tr>
	<tr>
		<td style="width: 15%; font-size: 13px;">KETERANGAN</td>
		<td style="width: 1%; font-size: 13px;">: </td>
		<td style="font-size: 13px;">
			<?php if ($vinv->jenisjual == '1' || $vinv->jenisjual == '4') {
				echo '[TUNAI]';
			} else if($vinv->jenisjual == '2') {
				echo '[KREDIT]';
			} else {
				echo '[COD]';
			}
			?>
		</td>
		<td style="width: 15%; text-align: right; font-size: 13px;">DISKON RP&nbsp;</td>
		<td style="width: 1%; font-size: 13px;"> : </td>
		<td style="text-align: right; font-size: 13px; width: 15%; font-weight: bold;"><?= rupiah(round($diskonrp / 10) * 10) ?></td>
	</tr>
	<tr>
		<td style="width: 15%; font-size: 13px;"></td>
		<td style="width: 1%; font-size: 13px;"></td>
		<td></td>
		<td style="width: 15%; text-align: right; font-size: 13px;">DPP&nbsp;</td>
		<td style="width: 1%; font-size: 13px;"> : </td>
		<td style="text-align: right; font-size: 13px; width: 15%; font-weight: bold;"><?= rupiah($vinv->dpp) ?></td>
	</tr>
	<tr>
		<td style="text-align: left; font-size: 13px; font-weight: bold;" colspan="3">GIRO/CEK/TRANSFER KE <?= $vinv->namaaccount ?> : <?= $vinv->norekening ?></td>
		<td style="width: 15%; text-align: right; font-size: 13px;">TOTAL RP&nbsp;</td>
		<td style="width: 1%; font-size: 13px;">:</td>
		<td style="text-align: right; font-size: 13px; width: 15%; font-weight: bold;"><?= rupiah($vinv->grandtotal) ?></td>
	</tr>
	<tr>
		<td style="width: 15%; font-size: 13px;"></td>
		<td style="width: 1%; font-size: 13px;"></td>
		<td></td>
		<td style="width: 15%; text-align: right; font-size: 13px; font-weight: bold;"></td>
		<td style="width: 1%; font-size: 13px; font-weight: bold;">  </td>
		<td style="text-align: right; font-size: 13px; width: 15%; font-weight: bold;"></td>
	</tr>
</table>

<div class="row cf">
	<div class="col cf" style="float: left; width: 30%;">
		<div style="font-size: 11px;">
			Diterima,
		</div>
	</div>
	<div class="col cf" style="float: left; width: 30%;">
		<div style="font-size: 11px;">
			Sopir,
		</div>
	</div>
	<div class="col cf" style="float: left; width: 30%;">
		<div style="font-size: 11px;">
			Hormat Kami,
		</div>
	</div>
</div>

<!-- <div class="row cf">
	<div class="col cf" style="float: right; width: 100%; text-align: right;">
		<div style="font-size: 13px;">
			[R]
		</div>
	</div>
</div> -->
