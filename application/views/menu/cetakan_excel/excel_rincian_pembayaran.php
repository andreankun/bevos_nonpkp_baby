<?php

header("Content-type: application/vnd-ms-excel");

header("Content-Disposition: attachment; filename=Laporan Rincian Pembayaran.xls");

header("Pragma: no-cache");

header("Expires: 0");

function FormatRupiah($angka)
{
    $hasil_rupiah = number_format($angka, 0, '.', ',');
    return $hasil_rupiah;
}
?>
<h4 style="text-align: center;">LAPORAN RINCIAN PEMBAYARAN</h4>
CABANG : <?php echo $row->nama ?>
<br>
PERIODE : <?php echo $tglawal ?> S/D <?php echo $tglakhir ?>
<br>
<table border="1" width="100%">
    <thead>
        <tr>
            <th>Tanggal</th>
            <th>Nama Toko</th>
            <th>Info Pembayaran</th>
            <th>Jumlah</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $total = 0;
        foreach ($list as $val) :
            $nomor = $val->nomor;
        ?>
            <?php foreach ($report as $value) : ?>
                <?php if ($value->nomor == $nomor) : ?>
                    <tr>
                        <td style="text-align: right;"><?= date('d/m/Y', strtotime($value->tanggal)) ?></td>
                        <td><?= $value->namacustomer ?></td>
                        <td><?= $value->nomor ?></td>
                        <td style="text-align: right;"><?= FormatRupiah($value->nilaipenerimaan + $value->nilaialokasi) ?></td>
                    </tr>
                    <tr>
                        <td colspan="2" style="text-align: right;">Rincian Pembayaran :</td>
                    </tr>
                    <tr>
                        <td colspan="2" style="text-align: right;"><?= $value->noinvoice ?></td>
                        <td style="text-align: right;"><?= FormatRupiah($value->nilaipenerimaan + $value->nilaialokasi) ?></td>
                    </tr>
                <?php
                    $total = $total + $value->nilaipenerimaan + $value->nilaialokasi;
                endif; ?>
            <?php
            endforeach;
            ?>
        <?php endforeach; ?>
        <tr>
            <td colspan="3" style="text-align: right; font-weight: bold;">Total Pembayaran :</td>
            <td style="text-align: right;"><?= FormatRupiah($total) ?></td>
        </tr>
    </tbody>
</table>