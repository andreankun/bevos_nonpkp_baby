<?php

header("Content-type: application/vnd-ms-excel");

header("Content-Disposition: attachment; filename=Laporan Rekap Pembayaran.xls");

header("Pragma: no-cache");

header("Expires: 0");

function FormatRupiah($angka)
{
    $hasil_rupiah = number_format($angka, 0, '.', ',');
    return $hasil_rupiah;
}
?>
<h4 style="text-align: center;">LAPORAN REKAP PEMBAYARAN</h4>
CABANG : <?php echo $row->nama ?>
<br>
PERIODE : <?php echo $tglawal ?> S/D <?php echo $tglakhir ?>
<br>
<table border="1" width="100%">
    <thead>
        <tr>
            <th>No.</th>
            <th>Pembayaran</th>
            <th>No. Pembayaran</th>
            <th>Faktur</th>
            <th>Tanggal</th>
            <th>Info Pembayaran</th>
            <th>Nama Toko</th>
            <th>Kode Sales</th>
            <th>Nama Sales</th>
            <th>Jumlah</th>
        </tr>
    </thead>
    <tbody>
        <!-- <-?php
            foreach ($list as $val) :
                $jenis = $val->kodeaccount;
            ?> -->
        <?php if ($list->kodeaccount == '102001' || $list->kodeaccount == '102002' || $list->kodeaccount == '102003' || $list->kodeaccount == '102004' || $list->kodeaccount == '102005' || $list->kodeaccount == '102006' || $list->kodeaccount == '102007' || $list->kodeaccount == '102012' || $list->kodeaccount == '102013' || $list->kodeaccount == '102014' && $list->accountalokasi == '') : ?>
            <tr>
                <td colspan="5" style="font-weight: bold;">Pembayaran : TRANSFER</td>
            </tr>
        <?php elseif ($list->kodeaccount == '102001' || $list->kodeaccount == '102002' || $list->kodeaccount == '102003' || $list->kodeaccount == '102004' || $list->kodeaccount == '102005' || $list->kodeaccount == '102006' || $list->kodeaccount == '102007' || $list->kodeaccount == '102012' || $list->kodeaccount == '102013' || $list->kodeaccount == '102014' && $list->accountalokasi == '103999') : ?>
            <tr>
                <td colspan="5" style="font-weight: bold;">Pembayaran : DEPOSIT</td>
            </tr>
        <?php else : ?>
            <tr>
                <td colspan="5" style="font-weight: bold;">Pembayaran : TUNAI</td>
            </tr>
        <?php endif; ?>
        <?php
        $no = 1;
        $totalif = 0;
        $grandtotal = 0;
        foreach ($report as $value) : ?>
            <!-- <-?php if ($value->nilaipiutang - $value->nilaialokasi - $value->nilaipenerimaan == 0) : ?>
               
            <-?php endif; ?> -->
            <tr>
                    <td style="text-align: center;"><?= $no++ ?></td>
                    <td><?php if($value->kodeaccount == '101'){
                        echo "Transfer BCA";
                    } else {
                        echo "Pemabayaran";
                    } ?></td>
                    <td><?= $value->nomorpenerimaan ?></td>
                    <td><?= $value->nomor ?></td>
                    <td style="text-align: right;"><?= date('d/m/Y', strtotime($value->tglbayar)) ?></td>
                    <td><?= $value->namapelanggan ?></td>
                    <td><?= $value->nama_toko ?></td>
                    <td><?= $value->kodesalesman ?></td>
                    <td><?= $value->namasalesman ?></td>
                    <td style="text-align: right;"><?= FormatRupiah($value->terbayar) ?></td>
                </tr>
                <?php $totalif = $totalif + $value->terbayar; ?>
        <?php endforeach; ?>
        <?php $grandtotal = $grandtotal + $totalif; ?>
        <tr>
            <td colspan="9" style="text-align:right; font-weight: bold;">
                Total :
            </td>
            <td colspan="1" style="text-align:right;">
                <?= FormatRupiah($totalif) ?>
            </td>
        </tr>
        <tr>
            <td colspan="9" style="text-align:right; font-weight: bold;">
                Grand Total :
            </td>
            <td colspan="1" style="text-align:right;">
                <?= FormatRupiah($grandtotal) ?>
            </td>
        </tr>
        <!-- <-?php endforeach; ?> -->
    </tbody>
</table>