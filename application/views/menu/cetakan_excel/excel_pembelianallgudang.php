<?php

header("Content-type: application/vnd-ms-excel");

header("Content-Disposition: attachment; filename=LAPORAN PENERIMAAN BARANG PER-GUDANG.xls");

header("Pragma: no-cache");

header("Expires: 0");

?>
<h3 align="center">LAPORAN PENERIMAAN BARANG</h3>
<table border="0" width="100%">
	<tr>
		<th>Cabang : </th>
		<td><?= $cabang->nama ?></td>
	</tr>
	<tr>
		<th>PERIODE : </th>
		<td><?php echo $tglawal ?> S/D <?php echo $tglakhir ?></td>
	</tr>
</table>

<table border="1" width="100%">

    <thead>

        <tr>

            <th>NO</th>
            <th>NO.LPB</th>
            <th>NO.SJ</th>
            <th>TANGGAL</th>
            <th>KODE BARANG</th>
            <th>NAMA BARANG</th>
            <th>QTY</th>
            <th>SATUAN</th>
            <th>SUPPLIER</th>
            <th>KDGUDANG</th>
            <th>KDSUPPLIER</th>

        </tr>

    </thead>

    <tbody>
		<?php 
		$no = 1;
		foreach($report as $val): ?>
			<tr>
				<td><?= $no++ ?></td>
				<td><?= $val->nomorpo ?></td>
				<td><?= $val->nomorsj ?></td>
				<td><?= date('d/m/Y', strtotime($val->tanggal)) ?></td>
				<td><?= $val->kodebarang ?></td>
				<td><?= $val->namabarang ?></td>
				<td><?= $val->qty ?></td>
				<td><?= $val->kodesatuan ?></td>
				<td><?= $val->namasupplier ?></td>
				<td><?= $val->kodegudang ?></td>
				<td><?= $val->nosupplier ?></td>
			</tr>
		<?php endforeach; ?>
    </tbody>

</table>
