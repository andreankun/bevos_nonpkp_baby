<?php

header("Content-type: application/vnd-ms-excel");

header("Content-Disposition: attachment; filename=Laporan Outstanding Invoice.xls");

header("Pragma: no-cache");

header("Expires: 0");

function FormatRupiah($angka)
{
    $hasil_rupiah = number_format($angka, 0, '.', ',');
    return $hasil_rupiah;
}
?>
<h4 style="text-align: center;">LAPORAN OUTSTANDING INVOICE</h4>
CABANG : <?php echo $cabang->nama ?>
<br>
PERIODE : <?php echo $tglawal ?> S/D <?php echo $tglakhir ?>
<br>
<table border="1" width="100%">
    <thead>
        <tr>
            <th>No</th>
            <th>No.Faktur</th>
            <th>Tanggal</th>
            <th>Jatuh Tempo</th>
            <th>Piutang</th>
            <th>Terbayar</th>
            <th>Saldo</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $totalsemua = 0;
        $total = 0;
        $terbayar = 0;
        $totalsemuasaldo = 0;
        $totalterbayar = 0;
        foreach ($row as $val) : ?>
            <?php $totalsemua = $totalsemua + $total ?>
            <?php $tanggal = $val->tanggal ?>
            <?php if ($val->nilaipiutang - $val->terbayar > 0) : ?>
                <tr>
                    <td style="font-weight: bold; font-size: 11px;" colspan="4">Toko : <?= $val->namatoko ?></td>
                    <td style="font-weight: bold; font-size: 11px;" colspan="3">Sales : <?= $val->nama ?></td>
                </tr>
            <?php endif; ?>
            <?php
            $no = 1;
            $saldo = 0;
            $terbayar = 0;
            $total = 0;
            $totalsaldo = 0;
            foreach ($report as $vals) : ?>
                <?php if ($vals->nilaipiutang - $vals->terbayar > 0) : ?>
                    <?php if ($vals->tanggal == $tanggal) : ?>
                        <?php $totalterbayar = $totalterbayar + $terbayar ?>
                        <?php $totalsemuasaldo = $totalsemuasaldo + $totalsaldo ?>
                        <?php $saldo = $vals->nilaipiutang - $vals->terbayar ?>
                        <?php $terbayar = $vals->terbayar ?>
                        <?php $total = $total + $vals->nilaipiutang ?>
                        <?php $totalsaldo = $totalsaldo + $saldo ?>
                        <tr>
                            <td><?= $no++ ?></td>
                            <td><?= $vals->nomor ?></td>
                            <td><?= date('d-m-Y', strtotime($vals->tanggal)) ?></td>
                            <td><?= date('d-m-Y', strtotime($vals->tgljthtempo)) ?></td>
                            <td style="text-align: right;"><?= FormatRupiah($vals->nilaipiutang) ?></td>
                            <td style="text-align: right;"><?= FormatRupiah($terbayar) ?></td>
                            <td style="text-align: right;"><?= FormatRupiah($saldo) ?></td>
                        </tr>
                    <?php endif; ?>
                <?php endif; ?>
            <?php endforeach; ?>
            <tr>
                <td colspan="4" style="font-size: 12px; font-weight: bold; text-align: right;"> Total :</td>
                <td style="text-align: right;"><?= FormatRupiah($total) ?></td>
                <td colspan="2" style="text-align: right;"><?= FormatRupiah($totalsaldo) ?></td>
            </tr>
        <?php endforeach; ?>
        <tr>
            <td colspan="4" style="font-size: 12px; font-weight: bold; text-align: right;">Grandtotal : </td>
            <td style="text-align: right;"><?= FormatRupiah($totalsemua) ?></td>
            <td style="text-align: right;"><?= FormatRupiah($totalterbayar) ?></td>
            <td style="text-align: right;"><?= FormatRupiah($totalsemuasaldo) ?></td>
        </tr>
    </tbody>
</table>