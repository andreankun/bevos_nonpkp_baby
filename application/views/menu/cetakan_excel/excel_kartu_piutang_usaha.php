<?php

header("Content-type: application/vnd-ms-excel");

header("Content-Disposition: attachment; filename=Laporan Kartu Piutang Usaha.xls");

header("Pragma: no-cache");

header("Expires: 0");

function FormatRupiah($angka)
{
    $hasil_rupiah = number_format($angka, 0, '.', ',');
    return $hasil_rupiah;
}
?>
<h4 style="text-align: center;">LAPORAN KARTU PIUTANG USAHA</h4>
CABANG : <?php echo $row->nama ?>
<br>
PERIODE : <?php echo $tglawal ?> S/D <?php echo $tglakhir ?>
<br>
<table border="1" width="100%">
    <thead>
        <tr>
            <th>No.</th>
            <th>No.Faktur</th>
            <th>Tanggal</th>
            <th>Jatuh Tempo</th>
            <th>Piutang</th>
            <th>Terbayar</th>
            <th>Mutasi Saldo</th>
        </tr>
    </thead>
    <tbody>
        <?php if (!empty($list)) : ?>
            <?php
            $saldoawal = 0;
            $nilaipiutang = 0;
            $nilaipenerimaan = 0;
            $nilaialokasi = 0;
            foreach ($list as $val) :
                $nopelanggan = $val->nopelanggan;
            ?>
                <tr>
                    <td style="text-align: left; font-weight: bold;" colspan="5"><?= $val->nama_toko; ?></td>
                    <td style="text-align: right; font-weight: bold;" colspan="1">Saldo Awal:</td>
                    <td style="text-align: right; font-weight: bold;" colspan="1"><?= FormatRupiah($val->saldoawal + $val->nilaipiutang - $val->terbayar - $val->nilaipembulatan); ?></td>
                </tr>
                <?php
                $no = 1;
                foreach ($report as $value) : ?>
                    <?php if ($value->nopelanggan == $nopelanggan) :
                    ?>
                        <tr>
                            <td style="text-align: center;"><?= $no++ ?></td>
                            <td style="text-align: left;"><?= $value->nomor ?></td>
                            <td style="text-align: right;"><?= date('d-m-Y', strtotime($value->tanggal)) ?></td>
                            <td style="text-align: right;"><?= date('d-m-Y', strtotime($value->tgljthtempo)) ?></td>
                            <td style="text-align: right;"><?= FormatRupiah($value->nilaipiutang) ?></td>
                            <td style="text-align: right;"><?= FormatRupiah($value->nilaialokasi + $value->nilaipenerimaan) ?></td>
                            <td style="text-align: right;"><?= FormatRupiah($value->nilaipiutang - $value->nilaipenerimaan - $value->nilaialokasi) ?></td>
                        </tr>
                    <?php endif; ?>
                <?php endforeach; ?>
                <tr>
                    <td colspan="7">
                        <br>
                    </td>
                </tr>
            <?php endforeach; ?>
        <?php else : ?>
            <tr>
                <td style="text-align: center;" colspan="7">Data tidak tersedia.</td>
            </tr>
        <?php endif; ?>
    </tbody>
</table>