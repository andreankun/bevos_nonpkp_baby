<?php

header("Content-type: application/vnd-ms-excel");

header("Content-Disposition: attachment; filename=Laporan Daftar Piutang.xls");

header("Pragma: no-cache");

header("Expires: 0");

function FormatRupiah($angka)
{
    $hasil_rupiah = number_format($angka, 0, '.', ',');
    return $hasil_rupiah;
}
?>
<h4 style="text-align: center;">LAPORAN DAFTAR PIUTANG</h4>
CABANG : <?php echo $row->nama ?>
<br>
PERIODE : <?php echo $tglawal ?> S/D <?php echo $tglakhir ?>
<br>
<table border="1" width="100%">
    <thead>
        <tr>
            <th style="text-align: center;">No.</th>
            <th style="text-align: center;">Nama Pelanggan</th>
            <th style="text-align: center;">Nama Toko</th>
            <th style="text-align: center;">Saldo Awal</th>
            <th style="text-align: center;">Piutang</th>
            <th style="text-align: center;">Terbayar</th>
            <th style="text-align: center;">Saldo Akhir</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $no = 1;
        $totalsaldoawal = 0;
        $totalpiutang = 0;
        $totalterbayar = 0;
        $totalsaldoakhir = 0;
        foreach ($report as $val) :
        ?>
            <tr>
                <td style="text-align: center;"><?= $no++; ?></td>
                <td><?= $val->namapelanggan; ?></td>
                <td><?= $val->nama_toko; ?></td>
                <td style="text-align: right;"><?= FormatRupiah($val->saldoawal) ?></td>
                <td style="text-align: right;"><?= FormatRupiah($val->nilaipiutang) ?></td>
                <td style="text-align: right;"><?= FormatRupiah($val->terbayar) ?></td>
                <td style="text-align: right;"><?= FormatRupiah(($val->saldoawal + $val->nilaipiutang) - ($val->terbayar + $val->nilaipembulatan)) ?></td>
            </tr>
        <?php
            $totalsaldoawal = $totalsaldoawal + $val->saldoawal;
            $totalpiutang = $totalpiutang + $val->nilaipiutang;
            $totalterbayar = $totalterbayar + $val->terbayar;
            $totalsaldoakhir = $totalsaldoakhir + ($val->saldoawal + $val->nilaipiutang) - ($val->terbayar + $val->nilaipembulatan);
        endforeach;
        ?>
        <tr>
            <td colspan="7">
                <hr>
            </td>
        </tr>
        <tr>
            <td colspan="3" style="text-align: right; font-weight: bold;">Total</td>
            <td style="text-align: right;"><?= FormatRupiah($totalsaldoawal) ?></td>
            <td style="text-align: right;"><?= FormatRupiah($totalpiutang) ?></td>
            <td style="text-align: right;"><?= FormatRupiah($totalterbayar) ?></td>
            <td style="text-align: right;"><?= FormatRupiah($totalsaldoakhir) ?></td>
        </tr>
    </tbody>
</table>