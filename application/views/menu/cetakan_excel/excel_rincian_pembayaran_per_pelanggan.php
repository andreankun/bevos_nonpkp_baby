<?php

header("Content-type: application/vnd-ms-excel");

header("Content-Disposition: attachment; filename=Laporan Rincian Pembayaran Per Pelanggan.xls");

header("Pragma: no-cache");

header("Expires: 0");

function FormatRupiah($angka)
{
    $hasil_rupiah = number_format($angka, 0, '.', ',');
    return $hasil_rupiah;
}
?>
<h4 style="text-align: center;">LAPORAN RINCIAN PEMBAYARAN PER PELANGGAN</h4>
CABANG : <?php echo $row->nama ?>
<br>
PELANGGAN : <?php echo $reportrow->nama ?>
<br>
PERIODE : <?php echo $tglawal ?> S/D <?php echo $tglakhir ?>
<br>
<table border="1" width="100%">
    <thead>
        <tr>
            <th>Tanggal</th>
            <th>No. Penerimaan</th>
            <th>Info Pembayaran</th>
            <th>Jumlah</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $total = 0;
        foreach ($list as $val) :
            $nomor = $val->nomor;
        ?>
            <?php foreach ($report as $value) : ?>
                <?php if ($value->nomor == $nomor) : ?>
                    <tr>
                        <td><?= date('d-m-Y', strtotime($value->tanggal)) ?></td>
                        <td><?= $value->nomor ?></td>
                        <td><?= $value->nama ?></td>
                        <td style="text-align: right;"><?= FormatRupiah($value->nilaipenerimaan) ?></td>
                    </tr>
                    <tr>
                        <td colspan="2" style="text-align: right;"><?= $value->noinvoice ?></td>
                        <?php if ($value->kodeaccount == '102001' || $value->kodeaccount == '102002' || $value->kodeaccount == '102003' || $value->kodeaccount == '102004' || $value->kodeaccount == '102005' || $value->kodeaccount == '102006' || $value->kodeaccount == '102007' || $value->kodeaccount == '102012' || $value->kodeaccount == '102013' || $value->kodeaccount == '102014' && $value->accountalokasi == '') : ?>
                            <td style="text-align: center;">TRANSFER BCA</td>
                        <?php elseif ($value->kodeaccount == '102001' || $value->kodeaccount == '102002' || $value->kodeaccount == '102003' || $value->kodeaccount == '102004' || $value->kodeaccount == '102005' || $value->kodeaccount == '102006' || $value->kodeaccount == '102007' || $value->kodeaccount == '102012' || $value->kodeaccount == '102013' || $value->kodeaccount == '102014' && $value->accountalokasi == '103999') : ?>
                            <td style="text-align: center;">DEPOSIT</td>
                        <?php else : ?>
                            <td style="text-align: center;">TUNAI</td>
                        <?php endif; ?>
                        <td style="text-align: left;"><?= FormatRupiah($value->nilaipenerimaan) ?></td>
                    </tr>
                <?php
                    $total = $total + $value->nilaipenerimaan;
                endif; ?>
            <?php
            endforeach;
            ?>
        <?php endforeach; ?>
        <tr>
            <td colspan="3" style="text-align: right; font-weight: bold;">Total Pembayaran :</td>
            <td style="text-align: right;"><?= FormatRupiah($total) ?></td>
        </tr>
    </tbody>
</table>