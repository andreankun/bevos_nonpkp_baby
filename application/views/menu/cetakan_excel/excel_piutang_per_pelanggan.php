<?php

header("Content-type: application/vnd-ms-excel");

header("Content-Disposition: attachment; filename=Laporan Piutang Per Pelanggan.xls");

header("Pragma: no-cache");

header("Expires: 0");

function FormatRupiah($angka)
{
    $hasil_rupiah = number_format($angka, 0, '.', ',');
    return $hasil_rupiah;
}
?>
<h4 style="text-align: center;">LAPORAN PIUTANG PER PELANGGAN</h4>
CABANG : <?php echo $row->nama ?>
<br>
PELANGGAN : <?php echo $reportrow->namapelanggan ?>
<br>
PERIODE : <?php echo $tglawal ?> S/D <?php echo $tglakhir ?>
<br>
<table border="1" width="100%">
    <thead>
        <tr>
            <th>No.Faktur</th>
            <th>Tanggal</th>
            <th>Jatuh Tempo</th>
            <th>Piutang</th>
            <th>Tgl. Bayar</th>
            <th>Terbayar</th>
            <th>Mutasi Saldo</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $saldoawal = 0;
        $nilaipiutang = 0;
        $nilaipenerimaan = 0;
        $nilaialokasi = 0;
        foreach ($report as $val) :
            $nilaipiutang = $nilaipiutang + $val->nilaipiutang;
            $nilaipenerimaan = $nilaipenerimaan + $val->nilaipenerimaan;
            $nilaialokasi = $nilaialokasi + $val->nilaialokasi;
            $saldoawal = $nilaipiutang - $nilaialokasi - $nilaipenerimaan;
        ?>
        <?php endforeach; ?>
        <tr>
            <td colspan="5"></td>
            <td style="text-align: center; font-weight: bold;" colspan="1">Saldo Awal:</td>
            <td style="text-align: right; font-weight: bold;" colspan="1"><?= FormatRupiah($saldoawal) ?></td>
        </tr>
        <?php
        foreach ($report as $value) :
        ?>
            <tr>
                <td style="text-align: left;"><?= $value->nomor; ?></td>
                <td style="text-align: left;"><?= date('d-m-Y', strtotime($value->tanggal)); ?></td>
                <td style="text-align: left;"><?= date('d-m-Y', strtotime($value->tgljthtempo)); ?></td>
                <td style="text-align: right;"><?= FormatRupiah($value->nilaipiutang); ?></td>
                <td style="text-align: right;"></td>
                <td style="text-align: right;">0</td>
                <td style="text-align: right;"><?= FormatRupiah($value->nilaipiutang); ?></td>
            </tr>
            <tr>
                <?php if (!empty($value->tglbayar)) : ?>
                    <td style="text-align:right;" colspan="5"><?= date('d-m-Y', strtotime($value->tglbayar)); ?></td>
                <?php else : ?>
                    <td style="text-align:right;" colspan="5"><?= '' ?></td>
                <?php endif; ?>
                <td style="text-align:right;"><?= FormatRupiah($value->terbayar) ?></td>
                <td style="text-align:right;"><?= FormatRupiah($value->mutasisaldo) ?></td>
            </tr>
        <?php
        endforeach;
        ?>
    </tbody>
</table>