<div class="col-xl-12 mb-2">
	<div class="modal-header" style="padding: 5px;">
		<div class="col-xl-12">
			<div class="row">
				<div class="col-md-5">
					<h2>
						<span class="logo-menu"><?= $icons ?></span>
						<span class="text-uppercase"><?= $title ?></span>
					</h2>
				</div>
				<div class="col-md-7">
					<div class="form-group" style="float: right;">
						<!-- <button id="save" class="btn btn-sm btn-dark mb-1 mt-1 mr-1 ml-1" style="margin-bottom: 0px;"><i class="far fa-save" style="margin-right: 10px;"></i>Save</button>
						<button id="update" class="btn btn-sm btn-dark mb-1 mt-1 mr-1 ml-1" style="margin-bottom: 0px;"><i class="far fa-money-check-edit" style="margin-right: 10px;"></i>Update</button>
						<button data-toggle="modal" data-target="#findsuratjalan" id="find" class="btn btn-sm btn-dark mb-1 mt-1 mr-1 ml-1" style="margin-bottom: 0px;"><i class="far fa-file-search" style="margin-right: 10px;"></i>Find</button> -->
						<!-- <button id="clear" class="btn btn-sm btn-dark mb-1 mt-1 mr-1 ml-1" style="margin-bottom: 0px;"><i class="far fa-refresh" style="margin-right: 10px;"></i>Clear</button> -->
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="col-md-12 table-responsive hidescroll" style="height: 555px; overflow-y: scroll;">
    <div class="row">
        <!-- <div class="col-12 col-md-12 col-lg-12 col-xl-6">
            <div class="modal-content mb-2">
                <div class="modal-body">
                    <div class="row mb-2">
                        <div class="col-sm-12 input-group box">
                            <div class="col-sm-3 p-0 input-group-prepend">
                                <span class="col-sm-12 input-group-text">Jenis Dokumen</span>
                            </div>
                            <select class="form-control" style="height: 29px; padding: 0.175rem 0.50rem;" id="jenisdokumen">
                                <option value="">Pilih Jenis Dokumen</option>
                                <option value="1">Faktur</option>
                                <option value="2">Surat Jalan</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <button id="cari_data" class="btn btn-dark" style="width: 100%;">Cari Data&nbsp;<i class="fa fa-search text-white"></i></button>
                    </div>
                </div>
            </div>
        </div> -->
        <div class="col-12 col-md-12 col-lg-12 col-xl-12">
            <div class="modal-content mb-2">
                <div class="modal-body">
                    <div class="row my-3 faktur">
                        <div class="table-responsive">
                            <table id="t_faktur" class="table table-bordered table-striped table-default dt-responsive display nowrap" style="width:100%;">
                                <thead style="background-color: #68478D; color: #eee;">
                                    <tr style="line-height: 0.5 cm; ">
                                        <th style="text-align: center; ">
                                            Action
                                        </th>
                                        <th style="text-align: center; ">
                                            Nomor Global
                                        </th>
										<th style="text-align: center; ">
                                            Nomor Invoice
                                        </th>
                                        <th style="text-align: center; ">
                                            Tanggal Invoice
                                        </th>
                                        <th style="text-align: center; ">
                                            Nomor Surat Jalan
                                        </th>
                                        <th style="text-align: center; ">
                                            Nomor Sales Order
                                        </th>
                                        <th style="text-align: center; ">
                                            Tanggal Jatuh Tempo
                                        </th>
                                        <th style="text-align: center; ">
                                            Nama Pelanggan
                                        </th>
                                        <th style="text-align: center; ">
                                            Cabang
                                        </th>
                                        <th style="text-align: center; ">
                                            Gudang
                                        </th>
                                    </tr>
                                </thead>
                                <tbody class="tbody-scroll scroller text-center" id="tb_faktur"></tbody>
                            </table>
                        </div>
                    </div>
                    <!-- <div class="row my-3 suratjalan">
                        <div class="table-responsive">
                            <table id="t_suratjalan" class="table table-bordered table-striped table-default dt-responsive display nowrap" style="width:100%;">
                                <thead style="background-color: #68478D; color: #eee;">
                                    <tr style="line-height: 0.5 cm; ">
                                        <th style="text-align: center; ">
                                            Action
                                        </th>
                                        <th style="text-align: center; ">
                                            Nomor Surat Jalan
                                        </th>
                                        <th style="text-align: center; ">
                                            Tanggal Surat Jalan
                                        </th>
                                        <th style="text-align: center; ">
                                            Nomor Invoice
                                        </th>
                                        <th style="text-align: center; ">
                                            Nama Pelanggan
                                        </th>
                                        <th style="text-align: center; ">
                                            Cabang
                                        </th>
                                        <th style="text-align: center; ">
                                            Gudang
                                        </th>
                                    </tr>
                                </thead>
                                <tbody class="tbody-scroll scroller text-center" id="tb_suratjalan"></tbody>
                            </table>
                        </div>
                    </div> -->
                </div>
            </div>
        </div>
    </div>
</div>

<!-- <div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="findsuratjalan">
	<div class="modal-dialog modal-lg">
		<div class="modal-content" style="border-radius: 5px;">
			<div class="modal-header" style="padding: 10px; color: #000;">
				<h7 class="modal-title" style="margin-left: 5px;">Data Surat Jalan</h7>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="table-responsive">
					<table id="t_suratjalan" class="table table-bordered table-striped dt-responsive display nowrap" style="width:100%">
						<thead class="thead-dark">
							<tr>
								<th width="10">Action</th>
								<th width="150">Nomor Surat Jalan</th>
								<th width="150">Tanggal Surat Jalan</th>
								<th width="150">Nama Pelanggan</th>
								<th width="150">Nomor Invoice</th>
								<th width="150">Tanggal Invoice</th>
								<th width="150">Lokasi</th>
							</tr>
						</thead>
						<tbody></tbody>

					</table>

				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div> -->
