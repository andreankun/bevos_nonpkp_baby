<?php

class Invoice_model extends CI_Model
{
	public function GetMaxNomor($nomor = "")
	{
		$this->db->select_max('nomor');
		$this->db->where("left(nomor,12)", $nomor);
		return $this->db->get("srvt_invoice")->row();
	}

	function DataInvBatal($nomor = "")
	{
		return $this->db->query("SELECT cancel.*, inv.nofakturpajak
		FROM hist_cancel cancel
		LEFT JOIN srvt_invoice inv ON inv.nomor = cancel.noinvoicebatal
		WHERE cancel.noglobal = '" . $nomor . "' AND cancel.statuspakai = false")->result();
	}

	public function GetMaxNomorSJ($nomor = "")
	{
		$this->db->select_max('nomor');
		$this->db->where("left(nomor,10)", $nomor);
		return $this->db->get("srvt_suratjalan")->row();
	}

	public function CekKodeCabang($cabang){
		return $this->db->query("SELECT *FROM srvt_invoice WHERE kodecabang = '".$cabang."'")->result();
	}

	public function GetMaxNomorPKP($nomor = "", $cabang = "")
	{
		$this->db->select('nomor');
		$this->db->where("SUBSTRING(nomor,1,4)", $nomor);
		// $this->db->where('kodecabang', $cabang);
		$this->db->order_by("SUBSTRING(nomor,7,13) DESC");
		$this->db->limit(1);		
		return $this->db->get("srvt_invoice")->row();
	}

	public function GetMaxNomorPKP1($nomor = "", $cabang ="")
	{
		$this->db->select('nomor');
		$this->db->where("SUBSTRING(nomor,1,3)", $nomor);
		// $this->db->where('kodecabang', $cabang);
		$this->db->order_by("SUBSTRING(nomor,6,13) DESC");
		$this->db->limit(1);		
		return $this->db->get("srvt_invoice")->row();
	}

	public function GetMaxNomorGlobal($nomor = ""){
		$this->db->select('nomorgb');
		$this->db->where("SUBSTRING(nomorgb,1,1)", $nomor);
		$this->db->order_by("SUBSTRING(nomorgb,5,13) DESC");
		$this->db->limit(1);		
		return $this->db->get("srvt_invoice")->row();
	}

	function MaxRevisiKe($nomor = "")
	{
		return $this->db->query("SELECT COALESCE(MAX(revisike),0) revisike 
		FROM hist_cancel  		
		WHERE  noinvoice = '" . $nomor . "'")->result();
	}


	function CekInvoice($nomor = "")
	{
		return $this->db->query("SELECT * FROM srvt_invoice  WHERE nomor = '" . $nomor . "'")->result();
	}

	function GetDataSO($nomor = "")
	{
		return $this->db->query("SELECT *FROM srvt_salesorder WHERE nomor = '" . $nomor . "' AND batal = false")->result();
	}
	function CekInvoiceDetail($kodebarang = "", $nomor = "")
	{
		return $this->db->query("SELECT * FROM srvt_invoicedetail WHERE kodebarang = '" . $kodebarang . "' AND nomor = '" . $nomor . "'")->result();
	}

	function DataInvoice($nomor = "")
	{
		return $this->db->query("SELECT * FROM cari_invsj WHERE nomorgb = '" . $nomor . "'")->result();
	}

	function DataInvoiceDetail($nomor = "", $cabang = "")
	{
		return $this->db->query("SELECT * FROM cari_invsj WHERE nomorgb = '" . $nomor . "'")->result();
	}

	function DataPacking($nomor = "")
	{
		return $this->db->query("SELECT * FROM cari_pcktoinvoice WHERE nopacking = '" . $nomor . "'")->result();
	}

	function DataPckDetail($nomor = "")
	{
		return $this->db->query("SELECT * FROM cari_packingtoinvoice WHERE nopacking = '" . $nomor . "'")->result();
	}

	// function DataPackingDetail($nomor = "", $kodebarang = "")
	// {
	// 	return $this->db->query("SELECT * FROM cari_packingtoinvoice WHERE nopacking = '" . $nomor . "' AND kodebarang = '" . $kodebarang . "'")->result();
	// }

	function DataInventory($periode = "", $kodegudang = "", $kodebarang = "")
	{
		return $this->db->query("SELECT * FROM srvt_inventorystok WHERE periode = '" . $periode . "' AND kodegudang = '" . $kodegudang . "' AND kodebarang = '" . $kodebarang . "'")->row();
	}

	function SaveData($data = "")
	{
		return $this->db->insert('srvt_invoice', $data);
	}

	function SaveDataDetail($data = "")
	{
		return $this->db->insert('srvt_invoicedetail', $data);
	}

	function SaveDataSJ($data = "")
	{
		return $this->db->insert('srvt_suratjalan', $data);
	}

	function SaveDataSJDetail($data = "")
	{
		return $this->db->insert('srvt_suratjalandetail', $data);
	}

	function SaveDataAR($data = "")
	{
		return $this->db->insert('srvt_piutang', $data);
	}

	function UpdateData($nomor = "", $data = "")
	{
		$this->db->where('nomorgb', $nomor);
		return $this->db->update('srvt_invoice', $data);
	}

	function UpdateStatusSO($nomor = "", $data = "")
	{
		$this->db->where('nomor', $nomor);
		return $this->db->update('srvt_salesorder', $data);
	}

	function UpdateStatusPacking($nomor = "", $data = "")
	{
		$this->db->where('nomor', $nomor);
		return $this->db->update('srvt_packingbarang', $data);
	}

	function UpdateDataDetail($nomor = "", $data = "")
	{
		$this->db->where('nomor', $nomor);
		return $this->db->update('srvt_invoicedetail', $data);
	}

	function UpdateDataSJ($nomor = "", $data = "")
	{
		$this->db->where('nomor', $nomor);
		return $this->db->update('srvt_suratjalan', $data);
	}

	function UpdateDataSJDetail($nomor = "", $data = "")
	{
		$this->db->where('nomor', $nomor);
		return $this->db->update('srvt_suratjalandetail', $data);
	}

	function UpdateDataAR($nomor = "", $data = "")
	{
		$this->db->where('nomor', $nomor);
		return $this->db->update('srvt_piutang', $data);
	}

	function UpdateDataPacking($nomor = "", $data = "")
	{
		$this->db->where('nomor', $nomor);
		return $this->db->update('srvt_packingbarang', $data);
	}
	function UpdateDataInventory($table = "", $setdata = "", $where = "")
	{
		// return $this->db->set($dataupdate);
		// $this->db->where($where);
		// $this->db->update($table);

		return $this->db->query("UPDATE " . $table . " SET " . $setdata . " WHERE " . $where);
	}


	function DeleteDetail($nomor = "")
	{
		$this->db->where('nomor', $nomor);
		return $this->db->delete('srvt_invoicedetail');
	}

	function DeleteSJDetail($nomor = "")
	{
		$this->db->where('nomor', $nomor);
		return $this->db->delete('srvt_suratjalandetail');
	}
}
