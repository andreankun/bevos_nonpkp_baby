<script type="text/javascript">
    $(document).ready(function() {

        function LoadGudang() {
			$.ajax({
				url: "<?= base_url('sales/SalesOrder/LoadGudang') ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					cabang: $('#cabang').val()
				},
				success: function(data) {
					for (var i = 0; i < data.length; i++) {
						$("#lokasi").append('<option value = ' + data[i].kode + ' >' + data[i].nama + '</option>');
					}
				}
			}, false);
		}

        function ClearScreen() {
            $('#lokasi').empty();
			$("#lokasi").append('<option value = "" >Pilih Gudang</option>');
			LoadGudang();
            $('#nomorsj').val("");
            $('#tanggalsj').val("<?= date('Y-m-d') ?>");
            $('#noinvoice').val("");
            $('#tanggalinv').val("<?= date('Y-m-d') ?>");
            $('#lokasi').prop("disabled", false).val("");

            $('#tb_detail').empty();

            $('#cariinvoice').show();

            $('#save').prop('disabled', false);
            $('#update').prop('disabled', true);
        }

        function ValidasiSave(datadetail) {
            var tanggalsj = $('#tanggalsj').val();
            var noinvoice = $('#noinvoice').val();
            var tanggalinv = $('#tanggalinv').val();
            var lokasi = $('#lokasi').val();

            if (tanggalsj == '' || tanggalsj == 0) {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Tanggal Surat Jalan tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#tanggalsj').focus();
                var result = false;
            } else if (noinvoice == '' || noinvoice == 0) {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Nomor Invoice tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#noinvoice').focus();
                var result = false;
            } else if (tanggalinv == '' || tanggalinv == 0) {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Tanggal Invoice tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#tanggalinv').focus();
                var result = false;
            } else if (lokasi == '' || lokasi == 0) {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Lokasi tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#lokasi').focus();
                var result = false;
            } else if (datadetail.length == 0 || datadetail.length == '0') {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Data Detail tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                var result = false;
            } else {
                var result = true;
            }
            return result;
        }

        /*Format Date*/
        function FormatDate(input) {
            var date = new Date(input);
            var yyyy = date.getFullYear();
            var mm = date.getMonth() + 1;
            var dd = date.getDate();
            var hrs = date.getHours();
            var mnt = date.getMinutes();


            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }
            if (hrs < 10) {
                hrs = '0' + hrs;
            }
            if (mnt < 10) {
                mnt = '0' + mnt;
            }
            return yyyy + '-' + mm + '-' + dd;
        };

        /* Declare Toast */
        const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000,
            timerProgressBar: true,
            didOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
            }
        });

        /* Format Rupiah */
        function FormatRupiah(angka, prefix) {
            if (!angka) {
                return '';
            }
            var vangka = angka.toString();
            var number_string = vangka.replace(/[^.\d]/g, '').replace(/[^\w\s]/gi, '').toString(),
                split = number_string.split('.'),
                sisa = split[0].length % 3,
                rupiah = split[0].substr(0, sisa),
                ribuan = split[0].substr(sisa).match(/\d{3}/gi);

            // tambahkan titik jika yang di input sudah menjadi angka ribuan
            if (ribuan) {
                separator = sisa ? ',' : '';
                rupiah += separator + ribuan.join(',');
            }

            rupiah = split[1] != undefined ? rupiah + '.' + split[1] : rupiah;
            return prefix == undefined ? rupiah : (rupiah ? rupiah : '');
        };


        // document.getElementById("cariinvoice").addEventListener("click", function(event) {
        //     event.preventDefault();
        //     $('#tb_detail').empty();
        //     $('#t_invoice').DataTable({
        //         "destroy": true,
        //         "searching": true,
        //         "processing": true,
        //         "serverSide": true,
        //         "lengthChange": true,
        //         "pageLength": 5,
        //         "lengthMenu": [5, 10, 25, 50],
        //         "order": [],
        //         "ajax": {
        //             "url": "<?= base_url('sales/Invoice/CariDataInvoice'); ?>",
        //             "method": "POST",
        //             "data": {
        //                 nmtb: "cari_invsj",
        //                 field: {
        //                     nomor: "nomor",
        //                     tanggal: "tanggal",
        //                     tgljthtempo: "tgljthtempo",
        //                     namapelanggan: "namapelanggan"
        //                 },
        //                 sort: "nomor",
        //                 where: {
        //                     nomor: "nomor"
        //                 },
        //                 value: "nomor Not In (SELECT noinvoice FROM srvt_suratjalan)"
        //             },
        //         },
        //         "columnDefs": [{
        //                 "targets": 2,
        //                 "render": $.fn.dataTable.render.moment('YYYY-MM-DD HH:mm:ss', 'D MMM YY')
        //             },
        //             {
        //                 "targets": 3,
        //                 "render": $.fn.dataTable.render.moment('YYYY-MM-DD HH:mm:ss', 'D MMM YY')
        //             }
        //         ]
        //     });
        // })

        $(document).on('click', ".searchinvoice", function() {
            var nomor = $(this).attr("data-id");
            GetDataInvoice(nomor);
        });

        function GetDataInvoice(nomor) {
            $.ajax({
                url: "<?= base_url('sales/Invoice/DataInvoice'); ?>",
                method: "POST",
                dataType: "json",
                async: false,
                data: {
                    nomor: nomor
                },
                success: function(data) {
                    for (var i = 0; i < data.length; i++) {
                        $('#noinvoice').val(data[i].nomor.trim());
                        $('#tanggalinv').val(FormatDate(data[i].tanggal.trim()));
                    }
                    GetDataInvoiceDetail(nomor);
                }
            });
        };

        function InsertDataDetail(kodebarang, namabarang, qty) {

            var row = "";
            row =
                '<tr id="' + kodebarang + '">' +
                '<td>' + kodebarang + '</td>' +
                '<td>' + namabarang + '</td>' +
                '<td>' + FormatRupiah(qty) + '</td>' +
                '</tr>';
            $('#tb_detail').append(row);
        };

        function GetDataInvoiceDetail(nomor) {
            $.ajax({
                url: "<?= base_url('sales/Invoice/DataInvoiceDetail'); ?>",
                method: "POST",
                dataType: "json",
                async: false,
                data: {
                    nomor: nomor
                },
                success: function(data) {
                    for (var i = 0; i < data.length; i++) {
                        // var kode = data[i].kode;
                        var kodebarang = data[i].kodebarang.trim();
                        var namabarang = data[i].namabarang.trim();
                        var qty = data[i].qty.trim();

                        InsertDataDetail(kodebarang, namabarang, qty);
                    }
                }
            });
        };

        function AmbilDataDetail() {
            var table = document.getElementById('t_detail');
            var arr2 = [];
            for (var r = 1, n = table.rows.length; r < n; r++) {
                var string = "";
                for (var c = 0, m = table.rows[r].cells.length; c < m; c++) {
                    if (c == 0) {
                        string = "{" + table.rows[0].cells[c].innerHTML + " : '" + table.rows[r].cells[c].innerHTML + "'";
                    } else {
                        string = string + ", " + table.rows[0].cells[c].innerHTML + " : '" + table.rows[r].cells[c].innerHTML + "'";
                    }
                }
                string = string + "}";
                var obj = JSON.stringify(eval('(' + string + ')'));
                var arr = $.parseJSON(obj);
                arr2.push(arr);
            }
            return arr2;
        };

        document.getElementById("save").addEventListener("click", function(event) {
            event.preventDefault();
            var nomorsj = $('#nomorsj').val();
            var tanggalsj = $('#tanggalsj').val();
            var noinvoice = $('#noinvoice').val();
            var tanggalinv = $('#tanggalinv').val();
            var lokasi = $('#lokasi').val();

            var datadetail = AmbilDataDetail();

            if (ValidasiSave(datadetail) == true) {
                $.ajax({
                    url: "<?= base_url("sales/SuratJalan/Save") ?>",
                    method: "POST",
                    dataType: "json",
                    async: true,
                    data: {
                        nomorsj: nomorsj,
                        tanggalinv: tanggalinv,
                        noinvoice: noinvoice,
                        tanggalsj: tanggalsj,
                        lokasi: lokasi,
                        datadetail: datadetail
                    },
                    success: function(data) {
                        if (data.nomor != "") {
                            Toast.fire({
                                icon: 'success',
                                title: data.message
                            });
                            $('#nomorsj').val(data.nomor);
                            $('#tanggalsj').prop("disabled", true);
                            $('#noinvoice').prop("disabled", true);
                            $('#tanggalinv').prop("disabled", true);
                            $('#lokasi').prop("disabled", true);

                            $('#cariinvoice').hide();

                            $('#save').prop('disabled', true);
                            $('#update').prop('disabled', true);
                        } else {
                            Toast.fire({
                                icon: 'error',
                                title: data.message
                            });
                        }
                    }
                }, false)
            }
        })

        document.getElementById("find").addEventListener("click", function(event) {
            event.preventDefault();
            $('#tb_detail').empty();
            $('#t_suratjalan').DataTable({
                "destroy": true,
                "searching": true,
                "processing": true,
                "serverSide": true,
                "lengthChange": true,
                "pageLength": 5,
                "lengthMenu": [5, 10, 25, 50],
                "order": [],
                "ajax": {
                    "url": "<?= base_url('sales/SuratJalan/CariDataSuratJalan'); ?>",
                    "method": "POST",
                    "data": {
                        nmtb: "cari_suratjalan",
                        field: {
                            nomor: "nomor",
                            tanggal: "tanggal",
                            namapelanggan: "namapelanggan",
                            noinvoice: "noinvoice",
                            tglinvoice: "tglinvoice",
                            lokasi: "lokasi"
                        },
                        sort: "nomor",
                        where: {
                            nomor: "nomor"
                        }
                    },
                },
                "columnDefs": [{
                        "targets": 2,
                        "render": $.fn.dataTable.render.moment('YYYY-MM-DD HH:mm:ss', 'D MMM YY')
                    },
                    {
                        "targets": 5,
                        "render": $.fn.dataTable.render.moment('YYYY-MM-DD HH:mm:ss', 'D MMM YY')
                    }
                ]
            });
        })

        $(document).on('click', ".searchsj", function() {
            var nomor = $(this).attr("data-id");
            GetData(nomor);
        });

        function GetData(nomor) {
            $.ajax({
                url: "<?= base_url('sales/SuratJalan/DataSuratJalan'); ?>",
                method: "POST",
                dataType: "json",
                async: false,
                data: {
                    nomor: nomor
                },
                success: function(data) {
                    for (var i = 0; i < data.length; i++) {
                        $('#nomorsj').val(data[i].nomor.trim());
                        $('#tanggalsj').val(FormatDate(data[i].tanggal.trim()));
                        $('#noinvoice').val(data[i].noinvoice.trim());
                        $('#tanggalinv').val(FormatDate(data[i].tglinvoice.trim()));
                        $('#lokasi').val(data[i].kodegudang.trim());
                    }
                    GetDataDetail(nomor);
                    $('#update').prop('disabled', false);
                    $('#save').prop('disabled', true);
                }
            });
        };

        function GetDataDetail(nomor) {
            $.ajax({
                url: "<?= base_url('sales/SuratJalan/DataSuratJalanDetail'); ?>",
                method: "POST",
                dataType: "json",
                async: false,
                data: {
                    nomor: nomor
                },
                success: function(data) {
                    for (var i = 0; i < data.length; i++) {
                        // var kode = data[i].kode;
                        var kodebarang = data[i].kodebarang.trim();
                        var namabarang = data[i].namabarang.trim();
                        var qty = data[i].qty.trim();

                        InsertDataDetail(kodebarang, namabarang, qty);
                    }
                }
            });
        };

        document.getElementById("update").addEventListener("click", function(event) {
            event.preventDefault();
            event.preventDefault();
            var nomorsj = $('#nomorsj').val();
            var tanggalsj = $('#tanggalsj').val();
            var noinvoice = $('#noinvoice').val();
            var tanggalinv = $('#tanggalinv').val();
            var lokasi = $('#lokasi').val();

            var datadetail = AmbilDataDetail();

            if (ValidasiSave(datadetail) == true) {
                $.ajax({
                    url: "<?= base_url("sales/SuratJalan/Update") ?>",
                    method: "POST",
                    dataType: "json",
                    async: true,
                    data: {
                        nomorsj: nomorsj,
                        tanggalinv: tanggalinv,
                        noinvoice: noinvoice,
                        tanggalsj: tanggalsj,
                        lokasi: lokasi,
                        datadetail: datadetail
                    },
                    success: function(data) {
                        if (data.nomor != "") {
                            Toast.fire({
                                icon: 'success',
                                title: data.message
                            });
                            $('#nomorsj').val(data.nomor);
                            $('#tanggalsj').prop("disabled", true);
                            $('#noinvoice').prop("disabled", true);
                            $('#tanggalinv').prop("disabled", true);
                            $('#lokasi').prop("disabled", true);

                            $('#cariinvoice').hide();

                            $('#save').prop('disabled', true);
                            $('#update').prop('disabled', true);
                        } else {
                            Toast.fire({
                                icon: 'error',
                                title: data.message
                            });
                        }
                    }
                }, false)
            }
        })

        document.getElementById("clear").addEventListener("click", function(event) {
            event.preventDefault();
            ClearScreen();
        })

        ClearScreen();
    })
</script>