<script type="text/javascript">
	$(document).ready(function() {
		function ClearScreen() {
			$('#nomor').prop('disabled', true);
			$('#tanggal').prop('disabled', true);
			$('#catatan').val('');
			$('#keterangan').val('');
			$('#nilainominal').val('');
			$('#nilaiongkir').val('');
			$('.nosj').hide();
			$('#tanggalongkir').prop('disabled', false);
			$('#cancel').prop('disabled', true);
			$('#update').prop('disabled', true);
		}

		// $('#tanggalongkir').onclick(function(){
		// 	$(this).val($(this).val());
		// 	var tglongkir = $(this).val();
		// 	console.log(tglongkir)
		// })

		// $('#tanggalongkir').onchange(function(){
		// 	var ongkir = $(this).val($(this).val());
		// })

		// Ongkir();

		// Format Tanggal DatePicker
		$('#tanggalongkir').datepicker({
			format: "yyyy-mm-dd",
			autoclose: true,
			todayHighlight: true,
		});

		$('#tanggalongkir').datepicker({
			format: 'mm-dd-yyyy',
		}).on('changeDate', function(e) {
			$('#tglongkir').val(e.format()); 
			date = new Date(e.format());
			console.log($('#top').val());
			// top = $()
			var tanggal = date + Number($('#top').val());
			var tgljthtempo = date.setDate(date.getDate() + Number($('#top').val()));
			console.log(date.toISOString().slice(0,10));
			$('#tgljthtempo').val(date.toISOString().slice(0,10));
			console.log(tanggal);
			// refreshdata(e.format());
		});

		$('#tgljatuhtempo').datepicker({
			format: "yyyy-mm-dd",
			autoclose: true,
			todayHighlight: true
		});

		// Function Foramt Rupiah
		/* Format Rupiah */
		function FormatRupiah(angka, prefix) {
			if (!angka) {
				return '';
			}
			var vangka = angka.toString();
			var number_string = vangka.replace(/[^.\d]/g, '').replace(/[^\w\s]/gi, '').toString(),
				split = number_string.split('.'),
				sisa = split[0].length % 3,
				rupiah = split[0].substr(0, sisa),
				ribuan = split[0].substr(sisa).match(/\d{3}/gi);

			// tambahkan titik jika yang di input sudah menjadi angka ribuan
			if (ribuan) {
				separator = sisa ? ',' : '';
				rupiah += separator + ribuan.join(',');
			}

			rupiah = split[1] != undefined ? rupiah + '.' + split[1] : rupiah;
			return prefix == undefined ? rupiah : (rupiah ? rupiah : '');
		};

		// Penggunaan Function Format Rupiah
		$('#nilaiongkir').keyup(function() {
			$(this).val(FormatRupiah($(this).val()));
		})

		$('#nilainominal').keyup(function() {
			$(this).val(FormatRupiah($(this).val()));
		})

		/* Declare Toast */
		const Toast = Swal.mixin({
			toast: true,
			position: 'top-end',
			showConfirmButton: false,
			timer: 3000,
			timerProgressBar: true,
			didOpen: (toast) => {
				toast.addEventListener('mouseenter', Swal.stopTimer)
				toast.addEventListener('mouseleave', Swal.resumeTimer)
			}
		});

		/*Format Date*/
		function FormatDate(input) {
			var date = new Date(input);
			var yyyy = date.getFullYear();
			var mm = date.getMonth() + 1;
			var dd = date.getDate();
			var hrs = date.getHours();
			var mnt = date.getMinutes();


			if (dd < 10) {
				dd = '0' + dd;
			}
			if (mm < 10) {
				mm = '0' + mm;
			}
			if (hrs < 10) {
				hrs = '0' + hrs;
			}
			if (mnt < 10) {
				mnt = '0' + mnt;
			}
			return yyyy + '-' + mm + '-' + dd;
		};

		function ValidasiSave() {
			var tanggal = $('#tanggal').val();
			var catatan = $('#catatan').val();
			// var nilainominal = $('#nilainominal').val();
			var keterangan = $('#keterangan').val();
			var tanggalongkir = $('#tanggalongkir').val();
			var nilaiongkir = $('#nilaiongkir').val();

			if (tanggal == "") {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Tanggal Sistem boleh kosong.',
					showCloseButton: true,
					width: 350,
				});
				$('#tanggal').focus();
				var result = false;
			} else if (catatan == "") {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Catatan Tidak boleh kosong.',
					showCloseButton: true,
					width: 350,
				});
				$('#catatan').focus();
				var result = false;
			}
			// else if(nilainominal == "" || nilainominal == 0) {
			// 	Swal.fire({
			//         title: 'Informasi',
			//         icon: 'info',
			//         html: 'Nilai Nominal Tidak boleh kosong.',
			//         showCloseButton: true,
			//         width: 350,
			//     });
			//     $('#nilainominal').focus();
			// 	var result = false;
			// } 
			else if (keterangan == "") {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Keterangan Ongkir Tidak boleh kosong.',
					showCloseButton: true,
					width: 350,
				});
				$('#keterangan').focus();
				var result = false;
			} else if (nilaiongkir == "" || nilaiongkir == 0) {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Nilai Ongkir Tidak boleh kosong.',
					showCloseButton: true,
					width: 350,
				});
				$('#nilaiongkir').focus();
				var result = false;
			} else {
				var result = true;
			}
			return result;
		}

		function ValidasiUpdate() {
			var tanggal = $('#tanggal').val();
			// var catatan = $('#catatan').val();
			// var nilainominal = $('#nilainominal').val();
			var keterangan = $('#keterangan').val();
			var tanggalongkir = $('#tanggalongkir').val();
			var nilaiongkir = $('#nilaiongkir').val();

			if (tanggal == "") {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Tanggal Sistem boleh kosong.',
					showCloseButton: true,
					width: 350,
				});
				$('#tanggal').focus();
				var result = false;
			}
			// else if(catatan == ""){
			// 	Swal.fire({
			//         title: 'Informasi',
			//         icon: 'info',
			//         html: 'Catatan Tidak boleh kosong.',
			//         showCloseButton: true,
			//         width: 350,
			//     });
			//     $('#catatan').focus();
			// 	var result = false;
			// } 
			// else if(nilainominal == "" || nilainominal == 0) {
			// 	Swal.fire({
			//         title: 'Informasi',
			//         icon: 'info',
			//         html: 'Nilai Nominal Tidak boleh kosong.',
			//         showCloseButton: true,
			//         width: 350,
			//     });
			//     $('#nilainominal').focus();
			// 	var result = false;
			// } 
			else if (keterangan == "") {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Keterangan Ongkir Tidak boleh kosong.',
					showCloseButton: true,
					width: 350,
				});
				$('#keterangan').focus();
				var result = false;
			} else if (nilaiongkir == '') {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Nilai Ongkir Tidak boleh kosong.',
					showCloseButton: true,
					width: 350,
				});
				$('#nilaiongkir').focus();
				var result = false;
			} else {
				var result = true;
			}
			return result;
		}

		/* Cari Data Customer*/
		document.getElementById("caricustomer").addEventListener("click", function(event) {
			event.preventDefault();
			$('#tb_detail').empty();
			$('#t_pelanggan').DataTable({
				"destroy": true,
				"searching": true,
				"processing": true,
				"serverSide": true,
				"lengthChange": true,
				"pageLength": 5,
				"lengthMenu": [5, 10, 25, 50],
				"order": [],
				"autoWidth": false,
				"ajax": {
					"url": "<?= base_url('masterdata/Customer/CariDataCustomer'); ?>",
					"method": "POST",
					"data": {
						nmtb: "cari_customer",
						field: {
							nomor: "nomor",
							aktif: "aktif",
							nama_toko: "nama_toko",
							nama: "nama",
							limit: "limit",
							status: "status",
							namagrup: "namagrup"
						},
						sort: "nomor",
						where: {
							nomor: "nomor",
							nama: "nama",
							nama_toko: "nama_toko"
						},
						value: "kodecabang = '" + $('#cabang').val() + "'"
					},
				},
				"columnDefs": [{
						"targets": 2,
						"data": "aktif",
						"render": function(data, type, row, meta) {
							return (row[2] == 't') ? 'Aktif' : 'Tidak Aktif';
						}
					},
					{
						"targets": 5,
						"data": "limit",
						"render": function(data, type, row, meta) {
							return (row[5] == 0) ? 0 : FormatRupiah(row[5].toString());
						}
					}
				],
			});
		});

		/*Get Data Customer*/
		$(document).on('click', ".searchcustomer", function() {
			var nomor = $(this).attr("data-id");;
			$.ajax({
				url: "<?= base_url('masterdata/Customer/DataCustomer'); ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					nomor: nomor
				},
				success: function(data) {
					for (var i = 0; i < data.length; i++) {
						$('#nopelanggan').val(data[i].nomor.trim());
						$('#namapelanggan').val(data[i].nama_toko.trim());
						$('#kodeprefix').val(data[i].kodeprefix.trim());
						// date = new Date($('#tglongkir').val());
						// console.log(data[i].jumlah.trim());
						// date.setDate(date.getDate() + Number(data[i].jumlah.trim()));
						// $('#tgljthtempo').val($.datepicker.formatDate('yy-mm-dd', date));
						$('#top').val(data[i].jumlah.trim());
						// dataRekeningDetail(data[i].rekeningtf.trim())

					}
					// GetDataDetail(nomor);
					// $('#statuspkp').prop('disabled', false);
					// $('#save').prop('disabled', true);
					// $('#update').prop('disabled', false);
					// $('#nomor').prop('disabled', true);
					// $('.aktif').show();
				}
			}, false);
		});

		/* Cari Data Customer*/
		document.getElementById("find").addEventListener("click", function(event) {
			event.preventDefault();
			$('#tb_detail').empty();
			$('#t_ongkir').DataTable({
				"destroy": true,
				"searching": true,
				"processing": true,
				"serverSide": true,
				"lengthChange": true,
				"pageLength": 5,
				"lengthMenu": [5, 10, 25, 50],
				"order": [],
				"autoWidth": false,
				"ajax": {
					"url": "<?= base_url('sales/PencatatanOngkir/CariDataPencatatanOngkir'); ?>",
					"method": "POST",
					"data": {
						nmtb: "cari_ongkir",
						field: {
							nomor: "nomor",
							namapelanggan: "namapelanggan",
							nilainominal: "nilainominal",
							nilaiongkir: "nilaiongkir"
						},
						sort: "nomor",
						where: {
							nomor: "nomor",
							namapelanggan: "namapelanggan"
						},
						value: "kodecabang = '" + $('#cabang').val() + "' AND batal = false"
					},
				},
				"columnDefs": [{
						"targets": 4,
						"data": "nilaiongkir",
						"render": function(data, type, row, meta) {
							return (row[4] == 0) ? 0 : FormatRupiah(row[4].toString());
						}
					},
					{
						"targets": 3,
						"data": "nilainominal",
						"render": function(data, type, row, meta) {
							return (row[3] == 0) ? 0 : FormatRupiah(row[3].toString());
						}
					}
				],
			});
		});

		/*Get Data*/
		$(document).on('click', ".searchongkir", function() {
			var nomor = $(this).attr("data-id");;
			$.ajax({
				url: "<?= base_url('sales/PencatatanOngkir/DataPencatatanOngkir'); ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					nomor: nomor
				},
				success: function(data) {
					for (var i = 0; i < data.length; i++) {
						$('#nomor').val(data[i].nomor.trim());
						$('#tanggal').val(FormatDate(data[i].tanggal.trim()));
						$('#catatan').val(data[i].catatan.trim());
						$('#nilainominal').val(FormatRupiah(data[i].nilainominal.trim()));
						$('#keterangan').val(data[i].keterangan.trim());
						$('#tanggalongkir').val(FormatDate(data[i].tanggalongkir.trim()));
						$('#nilaiongkir').val(FormatRupiah(data[i].nilaiongkir.trim()));
						$('#nopelanggan').val(data[i].nopelanggan.trim());
						$('#namapelanggan').val(data[i].namapelanggan.trim());
						if (data[i].nilaipenerimaan.trim() > 0) {
							$('#cancel').prop('disabled', true);
							$('#update').hide();
						} else {
							$('#cancel').prop('disabled', false);
						}
					}
					$('#save').prop('disabled', true);
					$('#update').prop('disabled', false);
					$('#nilainominal').prop('disabled', true);
					$('#nilaiongkir').prop('disabled', false);
					$('#nopelanggan').prop('disabled', true);
					$('#namapelanggan').prop('disabled', true);
					$('#caricustomer').hide();
					$('.aktif').show();
				}
			}, false);
		});


		document.getElementById('save').addEventListener("click", function(event) {
			event.preventDefault();
			var nomor = $('#nomor').val();
			var tanggal = $('#tanggal').val();
			// var catatan = $('#catatan').val();
			var kodeprefix = $('#kodeprefix').val();
			var keterangan = $('#keterangan').val();
			var tanggalongkir = $('#tanggalongkir').val();
			var nilaiongkir = $('#nilaiongkir').val();
			var kodecabang = $('#cabang').val();
			var nopelanggan = $('#nopelanggan').val();
			var namapelanggan = $('#namapelanggan').val();
			var tgljthtempo = $('#tgljthtempo').val();

			if (ValidasiSave() == true) {
				$.ajax({
					url: "<?= base_url("sales/PencatatanOngkir/Save") ?>",
					method: "POST",
					dataType: "json",
					async: true,
					data: {
						nomor: nomor,
						tanggal: tanggal,
						// catatan: catatan,
						kodeprefix: kodeprefix,
						keterangan: keterangan,
						tanggalongkir: tanggalongkir,
						nilaiongkir: nilaiongkir,
						kodecabang: kodecabang,
						nopelanggan: nopelanggan,
						namapelanggan: namapelanggan,
						tgljthtempo: tgljthtempo
					},
					success: function(data) {
						console.table(data);
						if (data.nomor != "" && data.nomorsj != "") {
							Toast.fire({
								icon: 'success',
								title: data.message
							});
							console.log(data.nomorsj);
							$('#nomor').val(data.nomor);
							$('#nosj').val(data.nomorsj);
							$('#tanggal').prop('disabled', true);
							$('#catatan').prop('disabled', true);
							// $('#nilainominal').prop('disabled', true);
							$('#keterangan').prop('disabled', true);
							$('#tanggalongkir').prop('disabled', true);
							$('#nilaiongkir').prop('disabled', true);
							$('#noepalnggan').prop('disabled', true);
							$('#namapelanggan').prop('disabled', true);
							// $('input[name="statuspkp"]').prop('disabled', true);
							// $('#npwp').prop('disabled', true);
							// $('#ktp').prop('disabled', true);
							// $('#email').prop('disabled', true);
							// $('#status').prop('disabled', true);
							// $('#limit').prop('disabled', true);
							// $('#alamatcek').prop('disabled', true);

							$('#update').prop("disabled", true);
							$('#save').prop("disabled", true);
							$('.nosj').show();
							CetakInvoice(data.nomor);
							CetakSuratJalan(data.nomorsj);
						} else {
							Toast.fire({
								icon: 'error',
								title: data.message
							});
						}
					}
				});
			}
		})

		function CetakInvoice(nomor) {
			// console.log(nomor);
			window.open("<?= base_url() ?>cetak_pdf/Ongkir/Print/" + nomor);
		}

		function CetakSuratJalan(nomorsj) {
			// console.log(nomor);
			window.open("<?= base_url() ?>cetak_pdf/SuratJalanOngkir/Print/" + nomorsj);
		}

		document.getElementById('okcancel').addEventListener("click", function(event) {
			event.preventDefault();
			var nomor = $('#nomor').val();
			// var nomorso = $('#nomorso').val();
			// var nopacking = $('#nopacking').val();
			var keterangan = $('#keteranganbatal').val();
			// var tanggalinv = $('#tanggalinv').val();
			// var datadetail = AmbilDataDetail();
			if (nomor == "") {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Pilih Dulu Nomor Yang Ingin dibatalkan',
					showCloseButton: true,
					width: 350,
				});
			} else if (keterangan == "") {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Keterangan Batal Tidak Boleh Kosong',
					showCloseButton: true,
					width: 350,
				});
			} else {
				$.ajax({
					url: "<?= base_url("sales/PencatatanOngkir/Cancel") ?>",
					method: "POST",
					dataType: "json",
					async: true,
					data: {
						nomor: nomor,
						keterangan: keterangan,
					},
					success: function(data) {
						console.log(data);
						if (data.nomor != "") {
							Toast.fire({
								icon: 'success',
								title: data.message
							});
							location.reload(true);
						} else {
							Toast.fire({
								icon: 'error',
								title: data.message
							});
						}
					}
				}, false)
			}
		})

		/* Update */
		document.getElementById('update').addEventListener("click", function(event) {
			event.preventDefault();
			var nomor = $('#nomor').val();
			var tanggal = $('#tanggal').val();
			var catatan = $('#catatan').val();
			var nilainominal = $('#nilainominal').val();
			var keterangan = $('#keterangan').val();
			var tanggalongkir = $('#tanggalongkir').val();
			var nilaiongkir = $('#nilaiongkir').val();
			var kodecabang = $('#cabang').val();
			var nopelanggan = $('#nopelanggan').val();
			var namapelanggan = $('#namapelanggan').val();

			if (ValidasiUpdate() == true) {
				$.ajax({
					url: "<?= base_url("sales/PencatatanOngkir/Update") ?>",
					method: "POST",
					dataType: "json",
					async: true,
					data: {
						nomor: nomor,
						tanggal: tanggal,
						catatan: catatan,
						// nilainominal:nilainominal,
						keterangan: keterangan,
						tanggalongkir: tanggalongkir,
						nilaiongkir: nilaiongkir,
						kodecabang: kodecabang,
						nopelanggan: nopelanggan,
						namapelanggan: namapelanggan
					},
					success: function(data) {
						if (data.kode != "") {
							Toast.fire({
								icon: 'success',
								title: data.message
							});
							$('#nomor').val(data.nomor);
							$('#tanggal').prop('disabled', true);
							$('#catatan').prop('disabled', true);
							$('#nilainominal').prop('disabled', true);
							$('#keterangan').prop('disabled', true);
							$('#tanggalongkir').prop('disabled', true);
							$('#nilaiongkir').prop('disabled', true);
							$('#noepalnggan').prop('disabled', true);
							$('#namapelanggan').prop('disabled', true);

							$('#update').prop("disabled", true);
							$('#save').prop("disabled", true);
							$('#cancel').prop("disabled", true);
						} else {
							Toast.fire({
								icon: 'error',
								title: data.message
							});
						}
					}
				}, false)
			}

		});

		document.getElementById("clear").addEventListener("click", function(event) {
			event.preventDefault();
			location.reload(true);
		})

		ClearScreen();
	})
</script>
