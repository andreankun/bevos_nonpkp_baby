<div class="col-12 col-xl-12 mb-2">
	<div class="modal-header" style="padding: 5px;">
		<div class="col-12 col-md-12">
			<div class="row">
				<div class="col-5 col-md-5">
					<h2>
						<span class="logo-menu"><?= $icons ?></span>
						<span class="text-uppercase"><?= $title ?></span>
					</h2>
				</div>
				<div class="col-7 col-md-7">
					<div class="form-group" style="float: right;">
						<button id="save" class="btn btn-sm btn-dark m-1" style="margin-bottom: 0px;"><i class="far fa-save" style="margin-right: 10px;"></i>Save</button>
						<button id="update" class="btn btn-sm btn-dark m-1" style="margin-bottom: 0px;"><i class="far fa-money-check-edit" style="margin-right: 10px;"></i>Update</button>
						<button data-toggle="modal" data-target="#findso" id="find" class="btn btn-sm btn-dark m-1" style="margin-bottom: 0px;"><i class="far fa-file-search" style="margin-right: 10px;"></i>Find</button>
						<button id="cancel" class="btn btn-sm btn-danger m-1" data-toggle="modal" data-target="#modalcancel"><i class="far fa-times-circle" style="margin-right: 10px;"></i>Cancel</button>
						<button id="clear" class="btn btn-sm btn-dark m-1" style="margin-bottom: 0px;"><i class="far fa-refresh" style="margin-right: 10px;"></i>Clear</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="col-12 table-responsive hidescroll sssalesorder" style="height: 555px; overflow-y: scroll;">
	<div class="row">
		<div class="col-12 col-md-12 col-lg-12 col-xl-6">
			<div class="modal-content mb-2">
				<div class="modal-header p-2">
					<h5 class="mb-0">DATA DOKUMEN</h5>
				</div>
				<div class="modal-body">
					<div class="row mb-2 mt-2">
						<div class="col-sm-6 input-group">
							<div class="col-sm-6 p-0 input-group-prepend">
								<span class="col-sm-12 input-group-text">Nomor SO</span>
							</div>
							<input class="form-control" type="text" name="nomorso" id="nomorso" maxlength="50" placeholder="Nomor SO" readonly required />
							<input class="form-control" type="hidden" name="nomorso" id="nomor" maxlength="50" placeholder="Nomor SO" readonly required />
							<input class="form-control" type="hidden" name="cabang" id="cabang" maxlength="50" value="<?php echo $this->session->userdata('mycabang') ?>" readonly required />
						</div>
						<div class="col-sm-6 input-group box">
							<div class="col-sm-6 p-0 input-group-prepend">
								<span class="col-sm-12 input-group-text">Tanggal SO</span>
							</div>
							<input class="form-control" type="text" name="tanggalso" id="tanggalso" maxlength="50" value="<?= date('Y-m-d') ?>" readonly required />
							<div class="input-group-text btn-dark">
								<span class="input-group-addon">
									<i class="fa fa-calendar-alt text-white"></i>
								</span>
							</div>
						</div>
					</div>
					<div class="row mb-2">
						<div class="col-sm-6 input-group">
							<div class="col-sm-6 p-0 input-group-prepend">
								<span class="col-sm-12 input-group-text">Nama Pelanggan</span>
							</div>
							<input class="form-control" type="text" name="namapelanggan" id="namapelanggan" maxlength="50" placeholder="Nama Pelanggan" readonly required />
							<input class="form-control" type="hidden" name="statuspelanggan" id="statuspelanggan" maxlength="50" placeholder="Nama Pelanggan" readonly required />
						</div>
						<div class="col-sm-6 input-group">
							<div class="col-sm-6 p-0 input-group-prepend">
								<span class="col-sm-12 input-group-text">Nomor Pelanggan</span>
							</div>
							<input class="form-control" type="text" name="nomorpelanggan" id="nomorpelanggan" maxlength="50" placeholder="Nomor Pelanggan" readonly required />
							<div class="input-group-text btn-dark" data-toggle="modal" data-target="#findpelanggan" id="caripelanggan">
								<span class="input-group-addon">
									<i class="fa fa-search text-white"></i>
								</span>
							</div>
						</div>
					</div>
					<div class="row mb-2">
						<div class="col-sm-6 input-group">
							<div class="col-sm-6 p-0 input-group-prepend">
								<span class="col-sm-12 input-group-text">Limit</span>
							</div>
							<input class="form-control" type="text" name="plafonso" id="plafonso" maxlength="50" placeholder="Limit" readonly required style="text-align: right;" />
						</div>
						<div class="col-sm-6 input-group">
							<div class="col-sm-6 p-0 input-group-prepend">
								<span class="col-sm-12 input-group-text">Sisa</span>
							</div>
							<input class="form-control" type="text" name="sisa" id="sisa" maxlength="50" placeholder="Sisa" readonly required style="text-align: right;" />
						</div>
						<div class="col-sm-6 input-group" style="display: none;">
							<div class="col-sm-6 p-0 input-group-prepend">
								<span class="col-sm-12 input-group-text">Outstanding AR</span>
							</div>
							<input class="form-control" type="text" name="outstandingar" id="outstandingar" maxlength="50" placeholder="Outstanding AR" readonly required style="text-align: right;" />
						</div>
					</div>
					<div class="row mb-2">
						<div class="col-sm-6 input-group">
							<div class="col-sm-6 p-0 input-group-prepend">
								<span class="col-sm-12 input-group-text">Nama Salesman</span>
							</div>
							<input class="form-control" type="text" name="namasalesman" id="namasalesman" maxlength="50" placeholder="Nama Salesman" readonly required />
						</div>
						<div class="col-sm-6 input-group">
							<div class="col-sm-6 p-0 input-group-prepend">
								<span class="col-sm-12 input-group-text">Kode Salesman</span>
							</div>
							<input class="form-control" type="text" name="kodesales" id="kodesales" maxlength="50" placeholder="Kode Salesman" readonly required />
							<div class="input-group-text btn-dark" data-toggle="modal" data-target="#findsalesman" id="carisalesman">
								<span class="input-group-addon">
									<i class="fa fa-search text-white"></i>
								</span>
							</div>
						</div>
					</div>
					<div class="row mb-2">
						<div class="col-sm-6 input-group box">
							<div class="col-sm-6 p-0 input-group-prepend">
								<span class="col-sm-12 input-group-text">Jenis Sales Order</span>
							</div>
							<select class="form-control" style="height: 29px; padding: 0.175rem 0.50rem;" id="jenisso">
								<option value="">Pilih Sales Order</option>
								<option value="1">Customer</option>
								<option value="2">Internal</option>
							</select>
						</div>
						<div class="col-sm-6 input-group box">
							<div class="col-sm-6 p-0 input-group-prepend">
								<span class="col-sm-12 input-group-text">Jenis Penjualan</span>
							</div>
							<select class="form-control" style="height: 29px; padding: 0.175rem 0.50rem;" id="jenisjual">
								<option value="">Pilih Penjualan</option>
								<option value="1">Tunai</option>
								<option value="2">Kredit</option>
								<option value="3">COD</option>
								<option value="4">Tunai 0%</option>
							</select>
						</div>

					</div>
					<div class="row mb-2">
						<div class="col-sm-6 input-group box">
							<div class="col-sm-6 p-0 input-group-prepend">
								<span class="col-sm-12 input-group-text">Lokasi Gudang</span>
							</div>
							<select class="form-control" style="height: 29px; padding: 0.175rem 0.50rem;" id="lokasigdg">
								<option value="">Pilih Gudang</option>
							</select>
						</div>
						<div class="col-sm-6 input-group">
							<div class="col-sm-6 p-0 input-group-prepend">
								<span class="col-sm-12 input-group-text">Proses Gudang</span>

							</div>
							<div class="input-group-text">
								<label class="radio radio-dark mb-0 mr-1 ml-1">
									<input type="radio" name="gudang" id="gudang" value="true"><span>YES</span><span class="checkmark"></span>
								</label>
								<label class="radio radio-dark mb-0 mr-1 ml-1">
									<input type="radio" name="gudang" id="tidak_gudang" value="false"><span>NO</span><span class="checkmark"></span>
								</label>
							</div>
						</div>
					</div>
					<div class="row mb-2">
						<div class="col-sm-6 input-group">
							<div class="col-sm-6 p-0 input-group-prepend">
								<span class="col-sm-12 input-group-text">Nama Racikan</span>
							</div>
							<input class="form-control" type="text" name="namaracikan" id="namaracikan" maxlength="50" placeholder="Nama Racikan" readonly required />
						</div>
						<div class="col-sm-6 input-group">
							<div class="col-sm-6 p-0 input-group-prepend">
								<span class="col-sm-12 input-group-text">Jenis Racikan</span>
							</div>
							<input class="form-control" type="text" name="jenisracikan" id="jenisracikan" maxlength="50" placeholder="Jenis Racikan" readonly required />
							<div class="input-group-text btn-dark" data-toggle="modal" data-target="#findracikan" id="cariracikan">
								<span class="input-group-addon">
									<i class="fa fa-search text-white"></i>
								</span>
							</div>
						</div>
					</div>
					<div class="row mb-2">
						<div class="col-sm-12 input-group">
							<div class="col-sm-3 p-0 input-group-prepend">
								<span class="col-sm-12 input-group-text">Note</span>
							</div>
							<textarea class="form-control" name="note" id="note" rows="3" placeholder="Note"></textarea>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-12 col-md-12 col-lg-12 col-xl-6">
			<div class="modal-content mb-2">
				<div class="modal-header p-2">
					<h5 class="mb-0">DATA DOKUMEN</h5>
				</div>
				<div class="modal-body">
					<div class="row mb-2">
						<div class="col-sm-6 input-group">
							<div class="col-sm-6 p-0 input-group-prepend">
								<span class="col-sm-12 input-group-text">Total</span>
							</div>
							<input class="form-control" type="text" name="total" id="total" maxlength="50" placeholder="0" required readonly style="text-align: right;" />
							<input class="form-control" type="hidden" name="totalsblmdisc2" id="totalsblmdisc2" maxlength="50" placeholder="0" required readonly style="text-align: right;" />
						</div>
					</div>
					<div class="row mb-2">
						<div class="col-sm-6 input-group">
							<div class="col-sm-6 p-0 input-group-prepend">
								<span class="col-sm-12 input-group-text">Disc. Manual</span>
							</div>
							<input class="form-control" type="number" name="discmanual" id="discmanual" maxlength="50" placeholder="0" required style="text-align: right;" />
							<input class="form-control" type="hidden" name="disccount" id="disccount" maxlength="50" placeholder="0" required style="text-align: right;" />
						</div>
						<div class="col-sm-6 input-group">
							<div class="col-sm-6 p-0 input-group-prepend">
								<span class="col-sm-12 input-group-text">Nilai Manual</span>
							</div>
							<input class="form-control" type="text" name="nialimanual" id="nilaimanual" maxlength="50" placeholder="0" required readonly style="text-align: right;" />
						</div>
					</div>
					<div class="row mb-2" style="margin-left: 23%;">
						<div class="col-sm-12 input-group">
							<div class="col-sm-3 p-0 input-group-prepand">
								<span class="col-sm-12 input-group-text">
									<input type="checkbox" name="disccek" id="disccek" class="disccek" />&nbsp;
									Discount Manual
								</span>
							</div>
						</div>
					</div>
					<div class="row mb-2">
						<div class="col-sm-6 input-group">
							<div class="col-sm-6 p-0 input-group-prepend">
								<span class="col-sm-12 input-group-text">Disc. Customer</span>
							</div>
							<input class="form-control" type="number" name="discustomer" id="discustomer" maxlength="50" placeholder="0" required readonly style="text-align: right;" />
						</div>
						<div class="col-sm-6 input-group">
							<div class="col-sm-6 p-0 input-group-prepend">
								<span class="col-sm-12 input-group-text">Nilai Customer</span>
							</div>
							<input class="form-control" type="text" name="nilaicustomer" id="nilaicustomer" maxlength="50" placeholder="0" required readonly style="text-align: right;" />
						</div>
					</div>
					<div class="row mb-2">
						<div class="col-sm-6 input-group">
							<div class="col-sm-6 p-0 input-group-prepend">
								<span class="col-sm-12 input-group-text">Disc. Cash</span>
							</div>
							<input class="form-control" type="text" name="discash" id="discash" maxlength="50" placeholder="0" required readonly style="text-align: right;" />
						</div>
						<div class="col-sm-6 input-group">
							<div class="col-sm-6 p-0 input-group-prepend">
								<span class="col-sm-12 input-group-text">Nilai Cash</span>
							</div>
							<input class="form-control" type="text" name="nilaicash" id="nilaicash" maxlength="50" placeholder="0" required readonly style="text-align: right;" />
						</div>
					</div>
					<div class="row mb-2 mt-2">
						<div class="col-sm-6 input-group">
							<div class="col-sm-6 p-0 input-group-prepend">
								<span class="col-sm-12 input-group-text">DPP</span>
							</div>
							<input class="form-control" type="text" name="dpp" id="dpp" maxlength="50" placeholder="0" readonly required style="text-align: right;" />
						</div>
					</div>
					<div class="row mb-2">
						<div class="col-sm-6 input-group">
							<div class="col-sm-6 p-0 input-group-prepend">
								<span class="col-sm-12 input-group-text">PPN</span>
							</div>
							<input class="form-control" type="hidden" name="ppntunai" id="ppntunai" readonly />
							<input class="form-control" type="text" name="ppn" id="ppn" maxlength="50" placeholder="0" readonly required style="text-align: right;" />
						</div>
					</div>
					<div class="row mb-2">
						<div class="col-sm-6 input-group">
							<div class="col-sm-6 p-0 input-group-prepend">
								<span class="col-sm-12 input-group-text">Grand Total</span>
							</div>
							<input class="form-control" type="text" name="grandtotal" id="grandtotal" maxlength="50" placeholder="0" readonly required style="text-align: right;" />
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-12 col-md-12 col-lg-12 col-xl-12">
		<div class="modal-content mb-2">
			<div class="modal-header p-2">
				<h5 class="mb-0">DATA DETAIL DOKUMEN</h5>
			</div>
			<div class="modal-body barang">
				<div class="row mb-2 mt-2">
					<small style="color: red;">Note: Kalau sudah terlanjur disimpan, dan ada input salah barang, Qty diubah menjadi 0 saja, bukan di delete</small>
				</div>
				<div class="row mb-2">
					<div class="col-sm-6 input-group box">
						<div class="col-sm-6 p-0 input-group-prepend">
							<span class="col-sm-12 input-group-text">Nama barang</span>
						</div>
						<input class="form-control" type="text" name="namabrg" id="namabrg" maxlength="50" placeholder="Nama barang" readonly required />
					</div>
					<div class="col-sm-6 input-group">
						<div class="col-sm-6 p-0 input-group-prepend">
							<span class="col-sm-12 input-group-text">Kode Barang</span>
						</div>
						<input class="form-control" type="text" name="kodebrg" id="kodebrg" maxlength="50" placeholder="Kode Barang" readonly required />
						<input class="form-control" type="hidden" name="kodebrg" id="kode" maxlength="50" placeholder="Kode Barang" readonly required />
						<!-- <div class="input-group-text btn-dark" data-toggle="modal" data-target="#findbarang" id="caribarang"> -->
						<div class="input-group-text btn-dark" data-toggle="modal" id="caribarang">
							<span class="input-group-addon">
								<i class="fa fa-search text-white"></i>
							</span>
						</div>
					</div>
				</div>
				<div class="row mb-2">
					<div class="col-sm-6 input-group box">
						<div class="col-sm-6 p-0 input-group-prepend">
							<span class="col-sm-12 input-group-text">Quantity</span>
						</div>
						<input class="form-control" type="hidden" name="qtyhidden" id="qtyhidden" maxlength="50" value="0" required style="text-align: right;" />
						<input class="form-control" type="text" name="qty" id="qty" maxlength="50" value="0" required style="text-align: right;" />
						<input class="form-control" type="hidden" name="qtybarang" id="qtybarang" maxlength="50" value="0" required style="text-align: right;" />
						<input class="form-control" type="hidden" name="qtyh" id="qtyh" maxlength="50" value="0" required style="text-align: right;" />
						<input class="form-control" type="hidden" name="qtyh" id="qtyfree1" maxlength="50" value="0" required style="text-align: right;" />
					</div>
					<div class="col-sm-6 input-group">
						<div class="col-sm-6 p-0 input-group-prepend">
							<span class="col-sm-12 input-group-text">Harga Jual</span>
						</div>
						<input class="form-control" type="text" name="hargajual" id="hargajual" maxlength="50" value="0" readonly required style="text-align: right;" />
						<div class="input-group-text btn-dark" data-toggle="modal" data-target="#findhargajual" id="cariharga">
							<span class="input-group-addon">
								<i class="fa fa-search text-white"></i>
							</span>
						</div>
					</div>
				</div>
				<div class="row mb-2">
					<div class="col-sm-6 input-group box">
						<div class="col-sm-6 p-0 input-group-prepend">
							<span class="col-sm-12 input-group-text">Disc.</span>
						</div>
						<input class="form-control" type="number" name="disc" id="disc" max="9999" value="0" required readonly style="text-align: right;" />
					</div>
					<div class="col-sm-6 input-group">
						<div class="col-sm-6 p-0 input-group-prepend">
							<span class="col-sm-12 input-group-text">Disc./Item</span>
						</div>
						<input class="form-control" type="text" name="discitem" id="discitem" maxlength="50" value="0" readonly required style="text-align: right;" />
					</div>
				</div>
				<div class="row mb-2">
					<div class="col-sm-6 input-group box">
						<div class="col-sm-6 p-0 input-group-prepend">
							<span class="col-sm-12 input-group-text">Disc. Promo</span>
						</div>
						<input class="form-control" type="number" name="discpaket" id="discpaket" maxlength="50" value="0" required readonly style="text-align: right;" />
					</div>
					<div class="col-sm-6 input-group">
						<div class="col-sm-6 p-0 input-group-prepend">
							<span class="col-sm-12 input-group-text">Disc. Promo/Item</span>
						</div>
						<input type="hidden" name="diskonpro" id="diskonpro">
						<input class="form-control" type="text" name="discpaketitem" id="discpaketitem" maxlength="50" value="0" readonly required style="text-align: right;" />
					</div>
				</div>
				<div class="row mb-2">
					<div class="col-sm-6 input-group box">
						<div class="col-sm-6 p-0 input-group-prepend">
							<span class="col-sm-6 input-group-text">Total Price</span>
						</div>
						<input class="form-control" type="text" name="totalprice" id="totalprice" maxlength="50" value="0" readonly required style="text-align: right;" />
						<input type="hidden" name="minorder" id="minorder">
						<div class="input-group-text btn-dark" id="adddetail">
							<span class="input-group-addon">
								<i class="far fa-plus text-white"></i>
							</span>
						</div>
					</div>
					<div class="col-sm-6 input-group box">
						<button class="btn btn-sm btn-dark form-control text-white" id="viewfree">Bonus Barang</button>
					</div>
				</div>
			</div>
			<div class="modal-body">
				<div class="row my-3">
					<div class="table-responsive">
						<table id="t_detail" class="table table-bordered table-striped table-default mb-0 penomoran" style="width:100%;">
							<thead style="background-color: #68478D; color: #eee;">
								<tr style="line-height: 0.5 cm; ">
									<th style="text-align: center; ">
										No
									</th>
									<th style="text-align: center; ">
										Kode
									</th>
									<th style="text-align: center; ">
										Nama
									</th>
									<th style="text-align: center; ">
										Qty
									</th>
									<th style="text-align: center; ">
										Harga
									</th>
									<th style="text-align: center; ">
										Disc
									</th>
									<th style="text-align: center; ">
										DiscItem
									</th>
									<th style="text-align: center; ">
										DiscPromo
									</th>
									<th style="text-align: center; ">
										DiscPromoItem
									</th>
									<th style="text-align: center; ">
										Subtotal
									</th>
									<th style="text-align: center; display: none;">
										Waktu
									</th>
									<th style="text-align: center; ">
										Action
									</th>
								</tr>
							</thead>
							<tbody class="tbody-scroll scroller" id="tb_detail"></tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="modal-body">
				<div class="row my-3 freebarang">
					<div class="table-responsive">
						<table id="t_free" class="table table-bordered table-striped table-default mb-0" style="width:100%;">
							<thead style="background-color: #68478D; color: #eee;">
								<tr style="line-height: 0.5 cm; ">
									<th style="text-align: center; ">
										Action
									</th>
									<th style="text-align: center; ">
										KodeBarang
									</th>
									<th style="text-align: center; ">
										Nama
									</th>
									<th style="text-align: center; ">
										Qty
									</th>
									<th style="text-align: center; ">
										Harga
									</th>
									<th style="text-align: center; ">
										Disc
									</th>
									<th style="text-align: center; ">
										DiscItem
									</th>
									<th style="text-align: center; ">
										DiscPromo
									</th>
									<th style="text-align: center; ">
										DiscPromoItem
									</th>
									<th style="text-align: center; ">
										Subtotal
									</th>
								</tr>
							</thead>
							<tbody class="tbody-scroll scroller" id="tb_free"></tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="findbarang">
	<div class="modal-dialog modal-lg">
		<div class="modal-content" style="border-radius: 5px;">
			<div class="modal-header" style="padding: 10px; color: #000;">
				<h7 class="modal-title" style="margin-left: 5px;">Data Barang</h7>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="table-responsive">
					<table id="t_barang" class="table table-bordered table-striped dt-responsive display nowrap" style="width:100%">
						<thead class="thead-dark">
							<tr>
								<th width="10">Action</th>
								<th width="150">Kode</th>
								<th width="150">Nama</th>
								<th width="50">Stok</th>
								<th width="150">Harga Jual</th>
							</tr>
						</thead>
						<tbody></tbody>

					</table>

				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="findhargajual">
	<div class="modal-dialog modal-lg">
		<div class="modal-content" style="border-radius: 5px;">
			<div class="modal-header" style="padding: 10px; color: #000;">
				<h7 class="modal-title" style="margin-left: 5px;">Data Tingkatan Harga</h7>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="table-responsive">
					<table id="t_hargajual" class="table table-bordered table-striped dt-responsive display nowrap" style="width:100%">
						<thead class="thead-dark">
							<tr>
								<th width="10">Action</th>
								<th width="150">Harga Jual</th>
							</tr>
						</thead>
						<tbody></tbody>

					</table>

				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="findpelanggan">
	<div class="modal-dialog modal-lg">
		<div class="modal-content" style="border-radius: 5px;">
			<div class="modal-header" style="padding: 10px; color: #000;">
				<h7 class="modal-title" style="margin-left: 5px;">Data Pelanggan</h7>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="table-responsive">
					<table id="t_pelanggan" class="table table-bordered table-striped dt-responsive display nowrap" style="width:100%">
						<thead class="thead-dark">
							<tr>
								<th width="10">Action</th>
								<th width="150">Kode</th>
								<th width="150">Nama Toko</th>
								<th width="150">Nama</th>
								<th width="150">Alamat</th>
								<th width="150">No. HP</th>
								<th width="150">No. Telp</th>
								<th width="150">E-mail</th>
								<th width="150">Status Pelanggan</th>
								<th width="150">Grup</th>
							</tr>
						</thead>
						<tbody></tbody>

					</table>

				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="findsalesman">
	<div class="modal-dialog modal-lg">
		<div class="modal-content" style="border-radius: 5px;">
			<div class="modal-header" style="padding: 10px; color: #000;">
				<h7 class="modal-title" style="margin-left: 5px;">Data Salesman</h7>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="table-responsive">
					<table id="t_salesman" class="table table-bordered table-striped dt-responsive display nowrap" style="width:100%">
						<thead class="thead-dark">
							<tr>
								<th width="10">Action</th>
								<th width="150">Kode Salesman</th>
								<th width="150">Nama Salesman</th>
								<th width="150">Kontak</th>
							</tr>
						</thead>
						<tbody></tbody>

					</table>

				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="findracikan">
	<div class="modal-dialog modal-lg">
		<div class="modal-content" style="border-radius: 5px;">
			<div class="modal-header" style="padding: 10px; color: #000;">
				<h7 class="modal-title" style="margin-left: 5px;">Data Racikan Paket Promo</h7>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="table-responsive">
					<table id="t_racikan" class="table table-bordered table-striped dt-responsive display nowrap" style="width:100%">
						<thead class="thead-dark">
							<tr>
								<th width="10">Action</th>
								<th width="150">Kode</th>
								<th width="150">Nama Racikan</th>
								<th width="150">Min. Order</th>
								<th width="150">Diskon</th>
							</tr>
						</thead>
						<tbody></tbody>

					</table>

				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="findso">
	<div class="modal-dialog modal-lg">
		<div class="modal-content" style="border-radius: 5px;">
			<div class="modal-header" style="padding: 10px; color: #000;">
				<h7 class="modal-title" style="margin-left: 5px;">Data Sales Order</h7>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="table-responsive">
					<table id="t_so" class="table table-bordered table-striped dt-responsive display nowrap" style="width:100%">
						<thead class="thead-dark">
							<tr>
								<th width="10">Action</th>
								<th width="150">Cetak Proforma</th>
								<th width="150">Cetak SO</th>
								<th width="150">Nomor SO</th>
								<th width="150">Tanggal SO</th>
								<th width="150">Pelanggan</th>
								<th width="150">Salesman</th>
								<th width="150">User Input</th>
								<th width="150">Tanggal Input</th>
								<!-- <th width="150">DPP</th> -->
								<th width="150">Total</th>
								<th width="150">Jenis Jual</th>
								<th width="150">Status</th>
							</tr>
						</thead>
						<tbody></tbody>
					</table>

				</div>
			</div>
			<div class="modal-footer p-2">
				<button type="button" class="btn btn-danger mb-0" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="printso">
	<div class="modal-dialog modal-lg">
		<div class="modal-content" style="border-radius: 5px;">
			<div class="modal-header" style="padding: 10px; color: #000;">
				<h7 class="modal-title" style="margin-left: 5px;">Data Sales Order</h7>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="table-responsive">
					<table id="t_so" class="table table-bordered table-striped dt-responsive display nowrap" style="width:100%">
						<thead class="thead-dark">
							<tr>
								<th width="10">Action</th>
								<th width="150">Nomor SO</th>
								<th width="150">Tanggal SO</th>
								<th width="150">Pelanggan</th>
								<th width="150">Salesman</th>
								<th width="150">DPP</th>
								<th width="150">PPN</th>
								<th width="150">Total</th>
							</tr>
						</thead>
						<tbody></tbody>
					</table>

				</div>
			</div>
			<div class="modal-footer p-2">
				<button type="button" class="btn btn-danger mb-0" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="changeqty">
	<div class="modal-dialog modal-md">
		<div class="modal-content" style="border-radius: 5px;">
			<div class="modal-header" style="padding: 10px; color: #000;">
				<h7 class="modal-title" style="margin-left: 5px;">UBAH DETAIL</h7>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="row mb-2">
					<div class="col-sm-6 input-group">
						<div class="col-sm-6 p-0 input-group-prepend">
							<span class="col-sm-12 input-group-text">Kode</span>
						</div>
						<input class="form-control" type="text" name="chgkode" id="chgkode" maxlength="50" placeholder="Kode" readonly required />
					</div>
					<div class="col-sm-6 input-group box">
						<div class="col-sm-6 p-0 input-group-prepend">
							<span class="col-sm-12 input-group-text">Nama</span>
						</div>
						<input class="form-control" type="text" name="chgnama" id="chgnama" maxlength="50" placeholder="Nama" required readonly />
					</div>
				</div>
				<div class="row mb-2">
					<div class="col-sm-6 input-group">
						<div class="col-sm-6 p-0 input-group-prepend">
							<span class="col-sm-6 input-group-text">Qty</span>
						</div>
						<input class="form-control" type="text" name="chgqty" id="chgqty" maxlength="50" placeholder="Qty" required style="text-align: right;" />
					</div>
					<div class="col-sm-6 input-group">
						<div class="col-sm-6 p-0 input-group-prepend">
							<span class="col-sm-6 input-group-text">Harga</span>
						</div>
						<input class="form-control" type="text" name="chgharga" id="chgharga" maxlength="50" placeholder="Harga" readonly required style="text-align: right;" />
						<div class="input-group-text btn-dark" data-toggle="modal" data-target="#findhargajual1" id="cariharga1">
							<span class="input-group-addon">
								<i class="fa fa-search text-white"></i>
							</span>
						</div>
					</div>
				</div>
				<div class="row mb-2">
					<div class="col-sm-6 input-group">
						<div class="col-sm-6 p-0 input-group-prepend">
							<span class="col-sm-6 input-group-text">Diskon</span>
						</div>
						<input class="form-control" type="text" name="chgdisc" id="chgdisc" maxlength="50" placeholder="Diskon" readonly required style="text-align: right;" />
					</div>
					<div class="col-sm-6 input-group">
						<div class="col-sm-6 p-0 input-group-prepend">
							<span class="col-sm-6 input-group-text">Disc. /Item</span>
						</div>
						<input class="form-control" type="text" name="chgdiscitem" id="chgdiscitem" maxlength="50" placeholder="Disc. /Item" readonly required style="text-align: right;" />
					</div>
				</div>
				<div class="row mb-2">
					<div class="col-sm-6 input-group">
						<div class="col-sm-6 p-0 input-group-prepend">
							<span class="col-sm-6 input-group-text">Disc. Promo</span>
						</div>
						<input type="hidden" name="chgdiskonpro" id="chgdiskonpro">
						<input class="form-control" type="text" name="chgdiscpaket" id="chgdiscpaket" maxlength="50" placeholder="Disc. Paket" readonly required style="text-align: right;" />
					</div>
					<div class="col-sm-6 input-group">
						<div class="col-sm-6 p-0 input-group-prepend">
							<span class="col-sm-6 input-group-text">Disc. Promo/Item</span>
						</div>
						<input class="form-control" type="text" name="chgdiscpaketitem" id="chgdiscpaketitem" maxlength="50" placeholder="Disc. Paket/Item" readonly required style="text-align: right;" />
					</div>
				</div>
				<div class="row mb-2">
					<div class="col-sm-6 input-group">
						<div class="col-sm-6 p-0 input-group-prepend">
							<span class="col-sm-6 input-group-text">Subtotal</span>
						</div>
						<input class="form-control" type="text" name="chgsubtotal" id="chgsubtotal" maxlength="50" placeholder="Subtotal" readonly required style="text-align: right;" />
						<input type="hidden" name="chgminorder" id="chgminorder">
						<div class="input-group-text btn-dark" id="changedetail" data-dismiss="modal">
							<span class="input-group-addon">
								<i class="fa fa-random" style="color: #fff;"></i>
							</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="modalcancel">
	<div class="modal-dialog modal-md">
		<div class="modal-content" style="border-radius: 5px;">
			<div class="modal-header" style="padding: 10px;">
				<h5 class="modal-title" style="margin-left: 5px; color: #f44336;"><i class="far fa-info-circle mr-1"></i>CANCEL DOKUMEN</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<center>
					<h5 class="modal-title"></i>Apakah Anda Yakin ?</h5>
				</center>
				<div class="row mb-2">
					<div class="col-sm-12 input-group box">
						<!-- <input class="col-sm-12 form-control" type="hidden" name="cancelnopr" id="cancelnopr" maxlength="50" placeholder="Nomor PR" readonly required/> -->
					</div>
				</div>
				<div class="row mb-2">
					<div class="col-sm-12 input-group box">
						<div class="col-sm-3 p-0 input-group-prepend">
							<span class="col-sm-12 input-group-text">Alasan Batal</span>
						</div>
						<textarea name="keteranganbatal" id="keteranganbatal" class="form-control" placeholder="Alasan Batal ..."></textarea>
					</div>
				</div>
			</div>
			<div class="modal-footer p-2">
				<button id="okcancel" type="button" class="btn btn-dark mb-0" data-dismiss="modal"><i class="far fa-check-circle mr-1"></i>Confirm</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="changefree">
	<div class="modal-dialog modal-md">
		<div class="modal-content" style="border-radius: 5px;">
			<div class="modal-header" style="padding: 10px; color: #000;">
				<h7 class="modal-title" style="margin-left: 5px;">Data Free Barang</h7>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="row mb-2">
					<div class="col-sm-6 input-group">
						<div class="col-sm-6 p-0 input-group-prepend">
							<span class="col-sm-12 input-group-text">Kode</span>
						</div>
						<input class="form-control" type="text" name="chgkode" id="chgkodef" maxlength="50" placeholder="Kode" readonly required />
					</div>
					<div class="col-sm-6 input-group box">
						<div class="col-sm-6 p-0 input-group-prepend">
							<span class="col-sm-12 input-group-text">Nama</span>
						</div>
						<input class="form-control" type="text" name="chgnama" id="chgnamaf" maxlength="50" placeholder="Nama" required readonly />
					</div>
				</div>
				<div class="row mb-2">
					<div class="col-sm-6 input-group">
						<div class="col-sm-6 p-0 input-group-prepend">
							<span class="col-sm-6 input-group-text">Qty</span>
						</div>
						<input class="form-control" type="text" name="chgqty" id="chgqtyf" maxlength="50" placeholder="Qty" required style="text-align: right;" />
						<input class="form-control" type="hidden" name="chgqty" id="qtyfree" maxlength="50" placeholder="Qty" required style="text-align: right;" />
					</div>
					<div class="col-sm-6 input-group">
						<div class="col-sm-6 p-0 input-group-prepend">
							<span class="col-sm-6 input-group-text">Harga</span>
						</div>
						<input class="form-control" type="text" name="chgharga" id="chghargaf" maxlength="50" placeholder="Harga" readonly required style="text-align: right;" />
					</div>
				</div>
				<div class="row mb-2">
					<div class="col-sm-6 input-group">
						<div class="col-sm-6 p-0 input-group-prepend">
							<span class="col-sm-6 input-group-text">Diskon</span>
						</div>
						<input class="form-control" type="text" name="chgdisc" id="chgdiscf" maxlength="50" placeholder="Diskon" readonly required style="text-align: right;" />
					</div>
					<div class="col-sm-6 input-group">
						<div class="col-sm-6 p-0 input-group-prepend">
							<span class="col-sm-6 input-group-text">Disc. /Item</span>
						</div>
						<input class="form-control" type="text" name="chgdiscitem" id="chgdiscitemf" maxlength="50" placeholder="Disc. /Item" readonly required style="text-align: right;" />
					</div>
				</div>
				<div class="row mb-2">
					<div class="col-sm-6 input-group">
						<div class="col-sm-6 p-0 input-group-prepend">
							<span class="col-sm-6 input-group-text">Disc. Promo</span>
						</div>
						<input type="hidden" name="chgdiskonpro" id="chgdiskonpro">
						<input class="form-control" type="text" name="chgdiscpaket" id="chgdiscpaketf" maxlength="50" placeholder="Disc. Paket" readonly required style="text-align: right;" />
					</div>
					<div class="col-sm-6 input-group">
						<div class="col-sm-6 p-0 input-group-prepend">
							<span class="col-sm-6 input-group-text">Disc. Promo/Item</span>
						</div>
						<input class="form-control" type="text" name="chgdiscpaketitem" id="chgdiscpaketitemf" maxlength="50" placeholder="Disc. Paket/Item" readonly required style="text-align: right;" />
					</div>
				</div>
				<div class="row mb-2">
					<div class="col-sm-6 input-group">
						<div class="col-sm-6 p-0 input-group-prepend">
							<span class="col-sm-6 input-group-text">Subtotal</span>
						</div>
						<input class="form-control" type="text" name="chgsubtotal" id="chgsubtotalf" maxlength="50" placeholder="Subtotal" readonly required style="text-align: right;" />
						<input type="hidden" name="chgminorder" id="chgminorder">
						<div class="input-group-text btn-dark" id="addfree" data-dismiss="modal">
							<span class="input-group-addon">
								<i class="fa fa-random" style="color: #fff;"></i>
							</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
