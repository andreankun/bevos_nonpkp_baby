<script type="text/javascript">
	$(document).ready(function() {
		var discCash = 0;
		var discCod = 0;
		var ppn = 0;
		var statuspaket = false;
		var diskonatassatu = false;
		var diskonatasdua = false;
		var diskon1 = 0;
		var diskon2 = 0;
		var Statuspaketinfo = "Tidak";
		var StatusToko = false;
		var statuspkp = false

		var datadiscoutatasdua;

		function ClearDataDetail() {
			$('#kodebrg').val("");
			$('#namabrg').val("");
			$('#qty').val(0);
			$('#qtyhidden').val(0);
			$('#hargajual').val(0);
			$('#disc').val(0);
			$('#discitem').val(0);
			$('#discpaket').val(0);
			$('#discpaketitem').val(0);
			$('#totalprice').val(0);

		}

		$('#tanggalso').datepicker({
			format: "yyyy-mm-dd",
			autoclose: true,
			todayHighlight: true
		});

		function AlamatSama() {
			if ($("#disccek").is(":checked")) {
				console.log($("#disccek").is(":checked"));
				$('#discmanual').prop("disabled", false).val();
				$("#discmanual").keyup(function() {
					$(this).val($(this).val());
					var subtotal = DeFormatRupiah($("#total").val());
					// console.log(subtotal);
					var disc = $(this).val();
					// var nominal = (Math.ceil(Math.round(parseFloat(subtotal) * parseFloat(disc) / 100) / 10) * 10);
					// console.log(nominal);
					$("#disccount").val(FormatRupiah(disc.toString()));
					HitungSummary();
				})
			} else {
				$('#discmanual').prop("disabled", true).val(0);
				$('#nilaimanual').prop("disabled", true).val(0);
			}
		}

		$('#disccek').click(function() {
			AlamatSama();
		})



		function LoadKonfigurasi() {
			$.ajax({
				url: "<?= base_url('sales/SalesOrder/LoadKonfigurasi') ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {},
				success: function(data) {
					// console.log(data);
					for (var i = 0; i < data.length; i++) {
						ppn = data[i].ppn;
						discCash = data[i].disccash;
						discCod = data[i].disccod;
						if (data[i].pkp == "t") {
							statuspkp = true;
						} else {
							statuspkp = false;
						}
					}
				}
			}, false);
		}

		function LoadGudang() {
			$.ajax({
				url: "<?= base_url('sales/SalesOrder/LoadGudang') ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					cabang: $('#cabang').val()
				},
				success: function(data) {
					for (var i = 0; i < data.length; i++) {
						$("#lokasigdg").append('<option value = ' + data[i].kode + ' >' + data[i].nama + '</option>');
					}
				}
			}, false);
		}

		function LoadOutStandingAR(nomorcustomer) {
			$.ajax({
				url: "<?= base_url('sales/SalesOrder/LoadOutStandingAR') ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					nomorcustomer: nomorcustomer
				},
				success: function(data) {
					console.log(data);
					if (data.length > 0) {
						for (var i = 0; i < data.length; i++) {
							if (DeFormatRupiah($('#plafonso').val()) == 0) {
								$("#outstandingar").val(FormatRupiah(data[i].outstandingar.toString()));
								$("#sisa").val(0)
							} else {
								$("#outstandingar").val(FormatRupiah(data[i].outstandingar.toString()));
								$('#sisa').val(FormatRupiah(DeFormatRupiah($('#plafonso').val()) - data[i].outstandingar.trim()));
							}
						}
					} else {
						$("#outstandingar").val(0)
						$('#sisa').val(0);
					}
				}
			}, false);
		};
		// function ClearScreen
		function ClearScreen() {
			LoadKonfigurasi();
			$('#lokasigdg').empty();
			$("#lokasigdg").append('<option value = "" >Pilih Gudang</option>');
			$('#statuspelanggan').val("");
			LoadGudang();
			$('#nomorso').prop("disabled", false).val("");
			$('#tanggalso').prop("disabled", false).val("<?= date('Y-m-d') ?>");
			$('#jenisjual').prop("disabled", false).val("");
			$('#jenisso').prop("disabled", false).val("");
			$('#lokasigdg').prop("disabled", false).val("");
			$('#jenisracikan').prop("disabled", false).val("");
			$('#nomorpelanggan').prop("disabled", false).val("");
			$('#namapelanggan').prop("disabled", false).val("");
			$('#discmanual').prop("disabled", true).val(0);
			$('#statuspelanggan').val("");
			$('#keteranganbatal').val("");
			$('#sisa').prop("disabled", false).val(0);
			$('#plafonso').prop("disabled", false).val(0);
			$('#outstandingar').prop("disabled", false).val(0);
			$('#kodesales').prop("disabled", false).val("");
			$('#namasalesman').prop("disabled", false).val("");
			$('#total').val(0);
			$('#totalsblmdisc2').val(0);


			// $('#gudang').prop('checked', true);
			// $('#tidak_gudang').prop('checked', true);
			$("input[name=gudang][value=true]").prop('checked', true);
			$('#nilaicash').val(0);
			$('#nilaicustomer').val(0);
			$('#discash').val(0);
			$('#discustomer').val(0);
			$('#dpp').val(0);
			$('#ppn').val(0);
			$('#ppntunai').val(FormatRupiah(ppn.toString()));
			$('#grandtotal').val(0);
			$('#disctunai').val(0);

			$('#kodebrg').prop("disabled", false).val("");
			$('#namabrg').prop("disabled", false).val("");
			$('#qty').prop("disabled", false).val(0);
			$('#qtyhidden').prop("disabled", false).val(0);
			$('#hargajual').prop("disabled", false).val(0);
			$('#disc').prop("disabled", false).val(0);
			$('#discitem').prop('disabled', false).val(0);
			$('#discpaket').prop("disabled", false).val(0);
			$('#discpaketitem').prop('disabled', false).val(0);
			$('#totalprice').prop("disabled", false).val(0);

			$('#tb_detail').empty();
			$('#viewfree').hide();
			$('.freebarang').hide();

			$('#cancel').prop("disabled", true);
			$('#update').prop("disabled", true);
			$('#save').prop("disabled", false);
			$('#caripelanggan').show();
			$('#carisalesman').show();
			$('#caribarang').show();
			$('#adddetail').show();
			$('.editdetail').show();
			$('.deldetail').show();
			$('#lokasigdg').val("");
			$('#jenisracikan').val("");
			$('#cariracikan').show();
			$('.barang').show();
		}

		if ($('#grup').val() == 'FINANCE JKT') {
			$('#cancel').hide();
			$('#update').hide();
			$('#save').hide();
			$('#caripelanggan').hide();
			$('#carisalesman').hide();
			$('#caribarang').hide();
			$('#adddetail').hide();
			$('.editdetail').hide();
			$('.deldetail').hide();
			$('#cariracikan').hide();
			$('.barang').hide();

		} else {
			$('#cancel').prop("disabled", true);
			$('#update').prop("disabled", true);
			$('#save').prop("disabled", false);
			$('#caripelanggan').show();
			$('#carisalesman').show();
			$('#caribarang').show();
			$('#adddetail').show();
			$('.editdetail').show();
			$('.deldetail').show();
			$('#lokasigdg').val("");
			$('#jenisracikan').val("");
			$('#cariracikan').show();
			$('.barang').show();
		}

		function setDateFormat(date) {
			dateObj = new Date(date);
			var day = DateObj.getDate();
			var month = dateObj.getMonth() + 1;
			var fullYear = DateObj.getFullYear().toString();
			var setformattedDate = '';
			setformattedDate = fullYear + '-' + getDigitToFormat(month) + '-' + getDigitToFormat(day);
			return setformattedDate;
		}

		function FormatDate(input) {
			var date = new Date(input);
			var yyyy = date.getFullYear();
			var mm = date.getMonth() + 1;
			var dd = date.getDate();
			var hrs = date.getHours();
			var mnt = date.getMinutes();


			if (dd < 10) {
				dd = '0' + dd;
			}
			if (mm < 10) {
				mm = '0' + mm;
			}
			if (hrs < 10) {
				hrs = '0' + hrs;
			}
			if (mnt < 10) {
				mnt = '0' + mnt;
			}
			return yyyy + '-' + mm + '-' + dd;
		};

		function getDigitToFormat(val) {
			if (val < 10) {
				val = '0' + val;
			}
			return val.toString();
		};

		/* Format Rupiah */
		function FormatRupiah(angka, prefix) {
			if (!angka) {
				return '';
			}
			var vangka = angka.toString();
			var number_string = vangka.replace(/[^.\d]/g, '').replace(/[^\w\s]/gi, '').toString(),
				split = number_string.split('.'),
				sisa = split[0].length % 3,
				rupiah = split[0].substr(0, sisa),
				ribuan = split[0].substr(sisa).match(/\d{3}/gi);

			// tambahkan titik jika yang di input sudah menjadi angka ribuan
			if (ribuan) {
				separator = sisa ? ',' : '';
				rupiah += separator + ribuan.join(',');
			}

			rupiah = split[1] != undefined ? rupiah + '.' + split[1] : rupiah;
			return prefix == undefined ? rupiah : (rupiah ? rupiah : '');
		};

		/* DeFormat Rupiah */
		// function DeFormatRupiah(angka) {
		// 	var result = angka.replace(/[^\w\s]/gi, '');

		// 	return result;
		// };
		function DeFormatRupiah(angka) {
			var result = angka.replace(/,/g, '');
			return result;
		};

		$('#disc[max]:not([max=""])').on('input', function(event) {
			var $this = $(this);
			var maxlength = $this.attr('max').length;
			var value = $this.val();
			if (value && value.length >= maxlength) {
				$this.val(value.substr(0, maxlength));
			}
		});

		$('#jenisjual').change(function() {
			var jenisjual = $('#jenisjual').val();
			if (jenisjual == '1') {
				$('#discash').val(discCash);
			} else if (jenisjual == '3') {
				$('#discash').val(discCod);
			} else {
				$('#discash').val(0);
			}
			HitungSummary();
		})

		$('#lokasigdg').change(function() {
			ClearDataDetail();
			$('#tb_detail').empty();
		})

		$('#jenisso').change(function() {
			if ($('#jenisso').val() == '1') {
				$('#jenisjual').val("").prop("disabled", false);
			} else if ($('#jenisso').val() == '2') {
				$('#jenisjual').val("2").prop("disabled", true);
			} else {
				$('#jenisjual').val("").prop("disabled", false);
			}
		})

		function HitungSummary() {
			var table = document.getElementById('t_detail');
			var sublist = 0;
			var subtotal = 0;
			var discustomer = DeFormatRupiah($('#discustomer').val());
			var discash = DeFormatRupiah($('#discash').val());
			var ppntunai = DeFormatRupiah($('#ppntunai').val());
			var discmanual1 = $("#discmanual").val();
			console.log(discmanual1);
			var nilaidiscustomer = 0;
			var nilaidiscash = 0;
			var nilaidiscustomera = 0;
			var nilaidiscasha = 0;
			var dpp = 0;
			var ppn = 0;
			var grandtotal = 0;
			var sublistsblm = 0;
			var subtotalsblm = 0;
			var ppntanpapembulatan = 0;
			var ppnpembulatan = 0;

			if (table.rows.length > 1) {
				for (var r = 1, n = table.rows.length; r < n; r++) {
					for (var c = 0, m = table.rows[r].cells.length; c < m - 1; c++) {
						sublist = (DeFormatRupiah(table.rows[r].cells[9].innerHTML));
						sublistsblm = (Math.round((parseFloat(DeFormatRupiah(table.rows[r].cells[4].innerHTML)) - parseFloat(DeFormatRupiah(table.rows[r].cells[6].innerHTML))) / 10) * 10) * parseFloat(DeFormatRupiah(table.rows[r].cells[3].innerHTML));
						// nilaidiscustomera = Math.round((parseFloat(sublist) * parseFloat(discustomer) / 100) / 10) * 10;
						// nilaidiscasha = Math.round(((parseFloat(sublist) - parseFloat(nilaidiscustomera)) * parseFloat(discash) / 100) / 10) * 10;
						// console.log(nilaidiscasha);
					}
					subtotal += parseFloat(sublist);
					subtotalsblm += parseFloat(sublistsblm);
					// nilaidiscustomer += parseFloat(nilaidiscustomera);
					// nilaidiscash += parseFloat(nilaidiscasha);
				}
			}
			$('#totalsblmdisc2').val(FormatRupiah(subtotalsblm));
			$('#total').val(FormatRupiah(subtotal));
			// nilaidiscustomer = Math.round((parseFloat(subtotal) * parseFloat(discustomer) / 100) / 10) * 10;
			// nilaidiscash = Math.round(((parseFloat(subtotal) - parseFloat(nilaidiscustomer)) * parseFloat(discash) / 100) / 10) * 10;
			// nilaidiscustomer = (Math.round((parseFloat(subtotal) * parseFloat(discustomer) / 100) / 10) * 10);
			// nilaidiscustomer = Math.round((parseFloat(subtotal) * parseFloat(discustomer) / 100) / 10) * 10;
			if ($("#disccek").is(":checked") == true) {
				nilaidiscmanual = Math.round((parseFloat(subtotal) * parseFloat(discmanual1) / 100) / 10) * 10;
				// nilaidiscustomer = 0;
				// nilaidiscasha = 0;
			} else {
				nilaidiscmanual = 0;
			}

			nilaidiscustomera = Math.round((parseFloat(subtotal) * parseFloat(discustomer) / 100) / 10) * 10;
			nilaidiscasha = Math.round(((parseFloat(subtotal) - parseFloat(nilaidiscmanual) - parseFloat(nilaidiscustomera)) * parseFloat(discash) / 100) / 10) * 10;
			// console.log(discustomer);
			// console.log(parseFloat(subtotal) * parseFloat(discustomer) / 100);
			// var nilaidiscustomersebelum = (parseFloat(subtotal) * parseFloat(discustomer) / 100) / 10;
			// nilaidiscash = Math.round(((parseFloat(subtotal) - parseFloat(nilaidiscustomer)) * parseFloat(discash) / 100) / 10) * 10;
			// console.log(nilaidiscash);

			dpp = subtotal - nilaidiscmanual - nilaidiscustomera - nilaidiscasha;

			ppntanpapembulatan = parseFloat(dpp) * (parseFloat(ppntunai) / 100);
			ppnpembulatan = Math.floor(parseFloat(dpp) * (parseFloat(ppntunai) / 100));
			// console.log(ppnpembulatan);
			// console.log(ppntanpapembulatan);
			// console.log(ppntanpapembulatan.toFixed());
			// console.log(parseFloat(ppntanpapembulatan) - parseFloat(ppntanpapembulatan));
			// if ((ppnpembulatan - ppntanpapembulatan) > 0.5) {
			ppn = Math.floor(parseFloat(dpp) * (parseFloat(ppntunai) / 100));
			// } else {
			// 	ppn = Math.floor(parseFloat(dpp) * (parseFloat(ppntunai) / 100));
			// }
			// ppn = ppntanpapembulatan.toFixed();


			// var ppnsebelumround = parseFloat(dpp) * parseFloat(ppntunai) / 100;
			// if ((ppnsebelumround % 0.5) == 0) {
			// 	console.log(ppnsebelumround);
			// 	ppn = Math.floor(parseFloat(dpp) * (parseFloat(ppntunai) / 100));
			// }
			grandtotal = parseFloat(dpp) + parseFloat(ppn);
			$('#nilaicustomer').val(FormatRupiah(nilaidiscustomera.toString()));
			$('#nilaicash').val(FormatRupiah(nilaidiscasha.toString()));
			$('#nilaimanual').val(FormatRupiah(nilaidiscmanual.toString()));
			$('#dpp').val(FormatRupiah(dpp.toString()));
			$('#ppn').val(FormatRupiah(ppn.toString()));
			$('#grandtotal').val(FormatRupiah(grandtotal.toString()));
		};

		function ValidasiSave(datadetail) {
			var nomorpelanggan = $('#nomorpelanggan').val();
			var namapelanggan = $('#namapelanggan').val();
			var plafonso = $('#plafonso').val().replace(",", "").replace(",", "").replace(",", "").replace(",", "").replace(",", "").replace(",", "").replace(",", "").replace(",", "").replace(",", "").replace(",", "");
			var outstandingar = $('#outstandingar').val().replace(",", "").replace(",", "").replace(",", "").replace(",", "").replace(",", "").replace(",", "").replace(",", "").replace(",", "").replace(",", "").replace(",", "");
			var kodesales = $('#kodesales').val();
			var namasalesman = $('#namasalesman').val();
			var jenisjual = $('#jenisjual').val();
			var jenisso = $('#jenisso').val();
			var lokasigdg = $('#lokasigdg').val();
			var jenisracikan = $('#jenisracikan').val();
			var discash = $('#discash').val();
			var discustomer = $('#discustomer').val();
			var total = $('#total').val();
			var dpp = $('#dpp').val();
			var ppn = $('#ppn').val();
			var grandtotal = $('#grandtotal').val().replace(",", "").replace(",", "").replace(",", "").replace(",", "").replace(",", "").replace(",", "").replace(",", "").replace(",", "").replace(",", "").replace(",", "");
			var statuspelanggan = $('#statuspelanggan').val();
			var viewbarang = $('#viewfree').show();
			var result = true;

			if (jenisjual == '' || jenisjual == 0) {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Jenis jual Tidak Boleh Kosong',
					showCloseButton: true,
					width: 350,
				})
				$('#jenisjual').focus();
				result = false;
			} else if (jenisso == '' || jenisso == 0) {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Jenis Sales Order Tidak Boleh Kosong',
					showCloseButton: true,
					width: 350,
				})
				$('#jenisso').focus();
				result = false;
			} else if (lokasigdg == '' || lokasigdg == 0) {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Lokasi Gudang Belum diPilih',
					showCloseButton: true,
					width: 350,
				})
				$('#lokasigdg').focus();
				result = false;
			} else if (nomorpelanggan == '' || nomorpelanggan == 0) {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Nomor Pelanggan Tidak Boleh Kosong',
					showCloseButton: true,
					width: 350,
				})
				$('#nomorpelanggan').focus();
				result = false;
			} else if (namapelanggan == '' || namapelanggan == 0) {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Nama Pelanggan Tidak Boleh Kosong',
					showCloseButton: true,
					width: 350,
				})
				$('#namapelanggan').focus();
				result = false;
			} else if (kodesales == '' || kodesales == 0) {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Kode Salesman Tidak Boleh Kosong',
					showCloseButton: true,
					width: 350,
				})
				$('#kodesales').focus();
				result = false;
			} else if (namasalesman == '' || namasalesman == 0) {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Nama Salesman Tidak Boleh Kosong',
					showCloseButton: true,
					width: 350,
				})
				$('#namasalesman').focus();
				result = false;
			}
			else if (total == '') {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Total Tidak Boleh Kosong',
					showCloseButton: true,
					width: 350,
				})
				$('#total').focus();
				result = false;
			} 
			else if (discash == '') {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Disc Cash Tidak Boleh Kosong',
					showCloseButton: true,
					width: 350,
				})
				$('#discash').focus();
				result = false;
			} else if (discustomer == '') {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Disc Customer Tidak Boleh Kosong',
					showCloseButton: true,
					width: 350,
				})
				$('#discustomer').focus();
				result = false;
			} else if (dpp == '') {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'DPP Tidak Boleh Kosong',
					showCloseButton: true,
					width: 350,
				})
				$('#dpp').focus();
				result = false;
			} else if ($('#viewfree').show() == true) {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Customer Mendapatkan bonus',
					showCloseButton: true,
					width: 350,
				})
				$('#ppn').focus();
				result = false;
			}
			else if (grandtotal == '') {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Total Tidak Boleh Kosong',
					showCloseButton: true,
					width: 350,
				})
				$('#grandtotal').focus();
				result = false;
			} 
			else if (datadetail.length == 0 || datadetail.length == '0') {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Data Detail tidak boleh kosong.',
					showCloseButton: true,
					width: 350,
				});
				$('#namagudang').focus();
				result = false;
			} else if (((parseFloat(outstandingar) + parseFloat(grandtotal)) > parseFloat(plafonso)) && statuspelanggan != 'VVIP' && $('#jenisjual').val() > 1) {
				// if ((parseFloat(outstandingar) + parseFloat(grandtotal)) > parseFloat(plafonso)) {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Nilai Melebihi Plafon SO',
					showCloseButton: true,
					width: 350,
				})
				$('#plafonso').focus();
				result = false;
				// }
			} else {
				result = true;
			}
			return result;
		}

		function CekSaldoStock(kodebarang) {

			$.ajax({
				url: "<?= base_url('masterdata/Customer/DataCustomer') ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					kodebarang: kodebarang
				},
				success: function(data) {
					for (var i = 0; i < data.length; i++) {
						$('#nomorpelanggan').val(data[i].nomor.trim());
						$('#namapelanggan').val(data[i].nama.trim());
						$('#plafonso').val(FormatRupiah(data[i].limit.trim()));
						$('#outstandingar').val(0);
						$('#statuspelanggan').val(data[i].status.trim());
						$('#kodesales').val("");
						$('#namasalesman').val("");
						LoadOutStandingAR(data[i].nomor.trim())
						cekjeniscustomer(data[i].nomor.trim());
					}
				}
			}, false);

		}

		function ValidasiAddFree(kodebarang) {
			var table = document.getElementById('t_detail');
			var kodebarang = $('#chgkodef').val();
			var namabarang = $('#chgnamaf').val();
			var qty = $('#chgqtyf').val();
			var harga = $('#chghargaf').val();
			var diskon = $('#chgdiscf').val();
			var discitem = $('#chgdiscitemf').val();
			var discpromo = $('#chgdiscpaketf').val();
			var discpaketitem = $('#chgdiscpaketitemf').val();
			var subtotal = $('#chgsubtotalf').val();

			var qtyfree = Math.floor(parseFloat($("#qtybarang").val()) / parseFloat($("#qtyh").val()) * parseFloat($('#qtyfree1').val()));
			console.log(qtyfree);
			// if ($('#chgqtyf').val() > $('#qtyfree').val()) {
			// 	Swal.fire({
			// 		title: 'Informasi',
			// 		icon: 'info',
			// 		html: 'Jumlah Barang melebihi stok',
			// 		showCloseButton: true,
			// 		width: 350
			// 	});
			// 	$('#qtyfree').focus();
			// 	return "gagal";
			// } 
			// else 
			// {
			// 	for (var r = 1, n = table.rows.length; r < n; r++) {
			// 		var string = "";
			// 		for (var c = 0, m = table.rows[r].cells.length; c < m; c++) {
			// 			if (c == 1) {
			// 				if (table.rows[r].cells[c].innerHTML == kodebarang) {
			// 					Swal.fire({
			// 						title: 'Informasi',
			// 						icon: 'info',
			// 						html: 'Data ini sudah diinput.',
			// 						showCloseButton: true,
			// 						width: 350,
			// 					});
			// 					return "gagal";
			// 				}
			// 			}
			// 		}
			// 	}
			// }
			return "sukses"
		}

		/* Validasi Data Add Detail */
		function ValidasiAddDetail(kodebarang) {
			var table = document.getElementById('t_detail');

			var kodebarang = $('#kodebrg').val();
			var namabarang = $('#namabrg').val();
			var qty = DeFormatRupiah($('#qty').val());
			var qtyhidden = DeFormatRupiah($('#qtyhidden').val());
			var harga = $('#hargajual').val();
			var disc = $('#disc').val();
			var discitem = $('#discitem').val();
			var discpaket = $('#discpaket').val();
			var discpaketitem = $('#discpaketitem').val();
			var totalprice = DeFormatRupiah($('#totalprice').val());


			// if (CekSaldoStock(kodebarang) == false) {
			// 	Swal.fire({
			// 		title: 'Informasi',
			// 		icon: 'info',
			// 		html: 'Stock Tidak Mencukupi.',
			// 		showCloseButton: true,
			// 		width: 350,
			// 	});
			// 	$('#kodebarang').focus();
			// 	return "gagal";
			// } else 
			if (kodebarang == '' || kodebarang == 0) {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Kode Barang tidak boleh kosong.',
					showCloseButton: true,
					width: 350,
				});
				$('#kodebrg').focus();
				return "gagal";
			} else if (namabarang == '' || namabarang == 0) {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Nama Barang tidak boleh kosong.',
					showCloseButton: true,
					width: 350,
				});
				$('#namabrg').focus();
				return "gagal";
			} else if (qty == '' || qty == 0) {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Qty tidak boleh kosong.',
					showCloseButton: true,
					width: 350,
				});
				$('#qty').focus();
				return "gagal";
			} else if (parseFloat(qty) > parseFloat(qtyhidden)) {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Qty tidak boleh lebih dari Qty yang tersedia.',
					showCloseButton: true,
					width: 350,
				});
				$('#qty').focus();
				return "gagal";
			} else if (harga == '') {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Harga tidak boleh kosong.',
					showCloseButton: true,
					width: 350,
				});
				$('#harga').focus();
				return "gagal";
			} else if (disc == '') {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Disc. tidak boleh kosong.',
					showCloseButton: true,
					width: 350,
				});
				$('#disc').focus();
				return "gagal";
			} else if (discitem == '') {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Disc. Item tidak boleh kosong.',
					showCloseButton: true,
					width: 350,
				});
				$('#discitem').focus();
				return "gagal";
			} else if (discpaket == '') {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Disc. Paket tidak boleh kosong.',
					showCloseButton: true,
					width: 350,
				});
				$('#discpaket').focus();
				return "gagal";
			}
			// else if (parseFloat(totalprice) == '') {
			// 	Swal.fire({
			// 		title: 'Informasi',
			// 		icon: 'info',
			// 		html: 'Total Price tidak boleh kosong.',
			// 		showCloseButton: true,
			// 		width: 350,
			// 	});
			// 	$('#totalprice').focus();
			// 	return "gagal";
			// } 
			else if (discpaketitem == '') {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Disc. Paket Item tidak boleh kosong.',
					showCloseButton: true,
					width: 350,
				});
				$('#discpaketitem').focus();
				return "gagal";
			}
			// else {
			// 	for (var r = 1, n = table.rows.length; r < n; r++) {
			// 		var string = "";
			// 		for (var c = 0, m = table.rows[r].cells.length; c < m; c++) {
			// 			if (c == 1) {
			// 				if (table.rows[r].cells[c].innerHTML == kodebarang) {
			// 					Swal.fire({
			// 						title: 'Informasi',
			// 						icon: 'info',
			// 						html: 'Data ini sudah diinput.',
			// 						showCloseButton: true,
			// 						width: 350,
			// 					});
			// 					return "gagal";
			// 				}
			// 			}
			// 		}
			// 	}
			// }
			return "sukses"
		}

		const Toast = Swal.mixin({
			toast: true,
			position: 'top-end',
			showConfirmButton: false,
			timer: 3000,
			timerProgressBar: true,
			didOpen: (toast) => {
				toast.addEventListener('mouseenter', Swal.stopTimer)
				toast.addEventListener('mouseleave', Swal.resumeTimer)
			}
		});

		document.getElementById("caripelanggan").addEventListener('click', function(event) {
			event.preventDefault();
			$('#t_pelanggan').DataTable({
				"language": {
					"url": "<?= base_url() ?>assets/json/id.json"
				},
				"destroy": true,
				"searching": true,
				"processing": true,
				"serverSide": true,
				"lengthChange": true,
				"pageLength": 5,
				"lengthMenu": [5, 10, 25, 50],
				"ordering": true,
				"order": [
					[2, 'asc']
				],
				"initComplete": function(settings, json) {
					$("#t_pelanggan input[type='search']").focus();
				},
				"ajax": {
					"url": "<?= base_url('masterdata/Customer/CariDataCustomer'); ?>",
					"method": "POST",
					"data": {
						nmtb: "cari_customer",
						field: {
							nomor: "nomor",
							nama_toko: "nama_toko",
							nama: "nama",
							alamat: "alamat",
							nohp: "nohp",
							notelp: "notelp",
							email: "email",
							status: "status",
							namagrup: "namagrup"
						},
						sort: "nomor",
						where: {
							nomor: "nomor",
							nama: "nama",
							nama_toko: "nama_toko"
						},
						value: "aktif = true AND kodecabang = '" + $("#cabang").val() + "'"
					},
				}
			});
		});

		$(document).on('click', ".searchcustomer", function() {
			var nomor = $(this).attr("data-id");
			$.ajax({
				url: "<?= base_url('sales/SalesOrder/DataPelanggan') ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					nomor: nomor
				},
				success: function(data) {
					for (var i = 0; i < data.length; i++) {
						$('#nomorpelanggan').val(data[i].nomor.trim());
						$('#namapelanggan').val(data[i].nama_toko.trim());
						if (data[i].limitbln.trim() != 0) {
							$('#plafonso').val(FormatRupiah(data[i].limitbln.trim()));
						} else {
							$('#plafonso').val(FormatRupiah(data[i].limit.trim()));
						}
						$('#outstandingar').val(0);
						$('#statuspelanggan').val(data[i].status.trim());
						$('#kodesales').val("");
						$('#namasalesman').val("");
						LoadOutStandingAR(data[i].nomor.trim())
						cekjeniscustomer(data[i].nomor.trim());
						cekstatusppn(data[i].statusppn);
					}
				}
			}, false);
		});

		function cekstatusppn(statusppn) {
			LoadKonfigurasi();
			if ((statuspkp == true) && statusppn == "f") {
				ppn = 0;
				$('#ppn').val(0);
				$('#ppntunai').val(0);
			} else {
				$('#ppntunai').val(FormatRupiah(ppn.toString()));
			}
		}

		function cekjeniscustomer(nomorpelanggan) {
			$.ajax({
				url: "<?= base_url('sales/SalesOrder/CariJenisCustomer') ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					nomor: nomorpelanggan
				},
				success: function(data) {
					for (var i = 0; i < data.length; i++) {
						if (data[i].tokosendiri == "t") {
							$('#jenisso').attr('disabled', 'disabled').val(2);
							$('#jenisjual').attr('disabled', 'disabled').val(2);
							$('#jenisracikan').val("");
							$('#cariracikan').hide();
						} else {
							$('#jenisso').attr('disabled', 'disabled').val(1);
							$('#jenisjual').removeAttr('disabled').val("");
							$('#jenisracikan').removeAttr('disabled').val("");
							$('#cariracikan').show();
							if (data[i].tokosendiri == "f") {
								$('#discustomer').val(data[i].diskon);
							}
						}
					}
				}
			}, false);
		}

		$('#jenisracikan').change(function() {
			var statuspaket = false;
			var value = $(this).val();

			$('#tb_detail').empty();
			ClearDataDetail();
		});

		document.getElementById("carisalesman").addEventListener("click", function(event) {
			event.preventDefault();
			$('#t_salesman').DataTable({
				"language": {
					"url": "<?= base_url() ?>assets/json/id.json"
				},
				"destroy": true,
				"searching": true,
				"processing": true,
				"serverSide": true,
				"lengthChange": true,
				"pageLength": 5,
				"lengthMenu": [5, 10, 25, 50],
				"order": [
					[2, 'asc']
				],
				"initComplete": function(settings, json) {
					$('input[type="search"]').delay(10).focus();
				},
				"ajax": {
					"url": "<?= base_url('masterdata/Salesman/CariDataSalesman'); ?>",
					"method": "POST",
					"data": {
						nmtb: "glbm_salesman",
						field: {
							kode: "kode",
							nama: "nama",
							nohp: "nohp"
						},
						sort: "kode",
						where: {
							kode: "kode"
						},
						value: "kode in ( SELECT kodesalesman FROM glbm_customerdetail WHERE nomor = '" + $('#nomorpelanggan').val() + "') AND kodecabang = '" + $("#cabang").val() + "'"
					}
				}
			});
		});

		$(document).on('click', ".searchsalesman", function() {
			var kode = $(this).attr("data-id");
			$.ajax({
				url: "<?= base_url('masterdata/Salesman/DataSalesman') ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					kode: kode
				},
				success: function(data) {
					for (var i = 0; i < data.length; i++) {
						$('#kodesales').val(data[i].kode.trim());
						$('#namasalesman').val(data[i].nama.trim());
					}
				}
			}, false);
		});

		document.getElementById("cariracikan").addEventListener("click", function(event) {
			event.preventDefault();

			$('#t_racikan').DataTable({
				"language": {
					"url": "<?= base_url() ?>assets/json/id.json"
				},
				"destroy": true,
				"searching": true,
				"processing": true,
				"serverSide": true,
				"lengthChange": true,
				"pageLength": 5,
				"lengthMenu": [5, 10, 25, 50],
				"order": [
					[2, 'asc']
				],
				"initComplete": function(settings, json) {
					$('input[type="search"]').delay(10).focus();
				},
				"ajax": {
					"url": "<?= base_url('sales/SalesOrder/CariDataRacikan'); ?>",
					"method": "POST",
					"data": {
						nmtb: "glbm_promo",
						field: {
							kode: "kode",
							nama: "nama",
							minorder: "minorder",
							diskon: "diskon"
						},
						sort: "kode",
						where: {
							kode: "kode",
							nama: "nama"
						},
						value: "aktif = true AND kodecabang = '" + $('#cabang').val() + "'"
					}
				},
				"columnDefs": [{
					"targets": 3,
					"data": "minorder",
					"render": function(data, type, row, meta) {
						return FormatRupiah(row[3]);
					}
				}, {
					"targets": 4,
					"data": "diskon",
					"render": function(data, type, row, meta) {
						return row[4] + "%";
					}
				}]
			});
		});

		$(document).on('click', ".searchracikan", function() {
			var kode = $(this).attr("data-id");
			$.ajax({
				url: "<?= base_url('sales/SalesOrder/DataRacikan') ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					kode: kode
				},
				success: function(data) {
					for (var i = 0; i < data.length; i++) {
						$('#jenisracikan').val(data[i].kode.trim());
						$('#namaracikan').val(data[i].nama.trim());
					}
				}
			}, false);
		});

		document.getElementById("caribarang").addEventListener("click", function(event) {
			event.preventDefault();
			if ($('#jenisso').val() == "") {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Jenis Sales Order Tidak Boleh Kosong',
					showCloseButton: true,
					width: 350,
				})
				$('#caribarang').removeAttr('data-target', '#findbarang')
				$('#jenisso').focus();
			} else {
				$('#caribarang').attr('data-target', '#findbarang');
				var d = new Date(),
					month = '' + (d.getMonth() + 1),
					day = '' + d.getDate(),
					year = d.getFullYear();
				if (month.length < 2)
					month = '0' + month;
				var kodesales = $('#kodesales').val();
				if ($('#jenisracikan').val() == "") {
					$('#t_barang').DataTable({
						"language": {
							"url": "<?= base_url() ?>assets/json/id.json"
						},
						"destroy": true,
						"searching": true,
						"processing": true,
						"serverSide": true,
						"lengthChange": true,
						"pageLength": 5,
						"lengthMenu": [5, 10, 25, 50],
						"order": [
							[1, 'desc']
						],
						"initComplete": function(settings, json) {
							$('input[type="search"]').delay(10).focus();
						},
						"ajax": {
							"url": "<?= base_url('sales/SalesOrder/CariDataBarang'); ?>",
							"method": "POST",
							"data": {
								nmtb: "cari_barangsales",
								field: {
									kodebarang: "kodebarang",
									namabarang: "namabarang",
									stock: "stock",
									hargajual: "hargajual",

								},
								sort: "kodebarang",
								where: {
									kodebarang: "kodebarang",
									namabarang: "namabarang"
								},
								value: "kodesales = '" + kodesales + "' AND kodecabang = '" + $("#cabang").val() + "' AND kodegudang = '" + $("#lokasigdg").val() + "'AND periode = '" + year + month + "' AND stock >= 0",
							}
						},
						"columnDefs": [{
							"targets": 3,
							"data": "stock",
							"render": function(data, type, row, meta) {
								return FormatRupiah(row[3]);
							}
						}, {
							"targets": 4,
							"data": "hargajual",
							"render": function(data, type, row, meta) {
								return FormatRupiah(row[4]);
							}
						}]
					});
				} else {
					$('#t_barang').DataTable({
						"language": {
							"url": "<?= base_url() ?>assets/json/id.json"
						},
						"destroy": true,
						"searching": true,
						"processing": true,
						"serverSide": true,
						"lengthChange": true,
						// "pageLength": 4,
						// "lengthMenu": [4, 8, 12, 24],
						"pageLength": 5,
						"lengthMenu": [5, 10, 25, 50],
						"order": [
							[1, 'desc']
						],
						"initComplete": function(settings, json) {
							$('input[type="search"]').delay(10).focus();
						},
						"ajax": {
							"url": "<?= base_url('sales/SalesOrder/CariDataBarang'); ?>",
							"method": "POST",
							"data": {
								nmtb: "cari_barangsalesracikan",
								field: {
									kodebarang: "kodebarang",
									namabarang: "namabarang",
									stock: "stock",
									hargajual: "hargajual"

								},
								sort: "kodebarang",
								where: {
									kodebarang: "kodebarang",
									namabarang: "namabarang"
								},
								value: "kodesales = '" + kodesales + "' AND kodecabang = '" + $("#cabang").val() + "' AND kodegudang = '" + $("#lokasigdg").val() + "' AND periode = '" + year + month + "' AND kodepromo = '" + $('#jenisracikan').val() + "' AND stock >= 0",
							}
						},
						"columnDefs": [{
							"targets": 3,
							"data": "stock",
							"render": function(data, type, row, meta) {
								return FormatRupiah(row[3]);
							}
						}, {
							"targets": 4,
							"data": "hargajual",
							"render": function(data, type, row, meta) {
								return FormatRupiah(row[4]);
							}
						}]
					});
				}
			}
		});

		$(document).on('click', ".searchbarang", function() {
			var kode = $(this).attr("data-id");
			$.ajax({
				url: "<?= base_url('sales/SalesOrder/DataBarang'); ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					kode: kode,
					jenisracikan: $('#jenisracikan').val(),
					cabang: $('#cabang').val(),
					gudang: $('#lokasigdg').val(),
					sales: $('#kodesales').val()
				},
				success: function(data) {
					console.log(data);
					for (var i = 0; i < data.length; i++) {
						$('#kodebrg').val(data[i].kodebarang.trim());
						$('#namabrg').val(data[i].namabarang.trim());
						$('#hargajual').val(FormatRupiah(data[i].hargajual.trim()));
						$('#qty').val(FormatRupiah(data[i].stock.trim()));
						$('#qtyhidden').val(FormatRupiah(data[i].stock.trim()));
						$("#qtyh").val(data[i].qty.trim());
						$("#qtyfree1").val(data[i].qtyfree.trim());
					}
				}

			}, false);
		});

		document.getElementById("cariharga").addEventListener("click", function(event) {
			event.preventDefault();
			var kodebarang = $("#kodebrg").val();
			var kodecabang = $("#cabang").val();
			// console.log(kodebarang);
			// console.log(kodecabang);
			$('#t_hargajual').DataTable({
				"destroy": true,
				"searching": true,
				"processing": true,
				"serverSide": true,
				"lengthChange": true,
				"pageLength": 5,
				"lengthMenu": [5, 10, 25, 50],
				"order": [],
				"ajax": {
					"url": "<?= base_url('sales/SalesOrder/CariDataHarga'); ?>",
					"method": "POST",
					"data": {
						nmtb: "glbm_barangdetail2",
						field: {
							hargajual: "hargajual",
							// nama: "nama",
							// barcode1: "barcode1",
							// barcode2: "barcode2",
							// kodesatuan: "kodesatuan",
						},
						sort: "nomor",
						where: {
							// nomor: "nomor",
							hargajual: "hargajual",
							// nama: "nama",
							// kodesatuan: "kodesatuan"
						},
						value: "kode = '" + kodebarang + "' AND kodecabang = '" + kodecabang + "'"
					}

				},
				"columnDefs": [{
					"targets": 1,
					"data": "hargajual",
					"render": function(data, type, row, meta) {
						return FormatRupiah(row[1]);
					}
				}]
			});
		});

		$(document).on('click', ".searchharga", function() {
			var nomor = $(this).attr("data-id");
			$.ajax({
				url: "<?= base_url('sales/SalesOrder/DataHarga'); ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					nomor: nomor,
					cabang: $("#cabang").val()
					// jenisracikan: $('#jenisracikan').val(),
					// cabang: $('#cabang').val(),
					// gudang: $('#lokasigdg').val(),
					// sales: $('#kodesales').val()
				},
				success: function(data) {
					console.log(data);
					for (var i = 0; i < data.length; i++) {
						$('#hargajual').val(FormatRupiah(data[i].hargajual.trim()));
						// $('#namabrg').val(data[i].namabarang.trim());
						// $('#hargajual').val(FormatRupiah(data[i].hargajual.trim()));
						// $('#qty').val(FormatRupiah(data[i].stock.trim()));
						// $('#qtyhidden').val(FormatRupiah(data[i].stock.trim()));
					}
				}

			}, false);
		});

		$(document).on('click', ".searchharga", function() {
			var nomor = $(this).attr("data-id");
			$.ajax({
				url: "<?= base_url('sales/SalesOrder/DataHarga'); ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					nomor: nomor,
					cabang: $("#cabang").val()
					// jenisracikan: $('#jenisracikan').val(),
					// cabang: $('#cabang').val(),
					// gudang: $('#lokasigdg').val(),
					// sales: $('#kodesales').val()
				},
				success: function(data) {
					console.log(data);
					for (var i = 0; i < data.length; i++) {
						$('#hargajual').val(FormatRupiah(data[i].hargajual.trim()));
						// $('#namabrg').val(data[i].namabarang.trim());
						// $('#hargajual').val(FormatRupiah(data[i].hargajual.trim()));
						// $('#qty').val(FormatRupiah(data[i].stock.trim()));
						// $('#qtyhidden').val(FormatRupiah(data[i].stock.trim()));
					}
				}

			}, false);
		});

		function GetDataFree() {
			var table = document.getElementById('t_detail');
			var kodebarang = "";
			var qtybarang = 0;
			var qty = 0;
			if (table.rows.length > 1) {
				for (var r = 1, n = table.rows.length; r < n; r++) {
					kodebrg = (table.rows[r].cells[1].innerHTML);
					qty = Math.round((table.rows[r].cells[3].innerHTML)) + Math.round(qty);
				}
				kodebarang = kodebrg;
				console.log(kodebarang);
				qtybarang += parseFloat(qty);
				console.log(qty);
				$("#qtybarang").val(qty);
				$("#kode").val(kodebrg);
				if ($('#qtyh').val() == 0) {
					$('#viewfree').hide();
				} else if (qtybarang < $('#qtyh').val()) {
					$('#viewfree').hide();
				} else {
					$('#viewfree').show();
				}

			}
		}

		// $("#t_free").dataTable({
		// 			"destroy": true,
		// 			"searching": true,
		// 			"processing": true,
		// 			"serverSide": true,
		// 			"lengthChange": true,
		// 			"pageLength": 5,
		// 			"lengthMenu": [5, 10, 25, 50],
		// 			"order": [],
		// 			"ajax": {
		// 				"url": "<?= base_url('sales/SalesOrder/CariDataFree'); ?>",
		// 				"method": "POST",
		// 				"data": {
		// 					nmtb: "cari_barangfree",
		// 					field: {
		// 						barangfree: "barangfree",
		// 						namafree: "namafree",
		// 						qty: "qtyitem",
		// 						harga: "harga",
		// 						disc: "disc",
		// 						discitem: "discitem",
		// 						discpromo: "discpromo",
		// 						discpromoitem: "discpromoitem",
		// 						subtotal: "subtotal"
		// 					},
		// 					sort: "barangfree",
		// 					where: {
		// 						// nomor: "nomor",
		// 						barangfree: "barangfree",
		// 						namafree: "namafree",
		// 						// kodesatuan: "kodesatuan"
		// 					},
		// 					value: "aktif = true AND kodecabang = '" + $("#cabang").val() + "' AND kodebarang = '" + $("#kode").val(kodebrg) + "' AND " + qtybarang + " >= qty"
		// 				}
		// 			}
		// 		});

		document.getElementById('viewfree').addEventListener("click", function(event) {
			event.preventDefault();
			var qtybarang = $("#qtybarang").val();
			var kodebarang = $("#kode").val();
			var d = new Date(),
				month = '' + (d.getMonth() + 1),
				day = '' + d.getDate(),
				year = d.getFullYear();
			if (month.length < 2)
				month = '0' + month;
			console.log(year + month);
			$('.freebarang').show();
			$("#t_free").dataTable({
				"destroy": true,
				"searching": true,
				"processing": true,
				"serverSide": true,
				"lengthChange": true,
				"pageLength": 5,
				"lengthMenu": [5, 10, 25, 50],
				"order": [],
				"ajax": {
					"url": "<?= base_url('sales/SalesOrder/CariDataFree'); ?>",
					"method": "POST",
					"data": {
						nmtb: "cari_barangfree",
						field: {
							barangfree: "barangfree",
							namafree: "namafree",
							qty: "qtyitem",
							harga: "harga",
							disc: "disc",
							discitem: "discitem",
							discpromo: "discpromo",
							discpromoitem: "discpromoitem",
							qtyfree: "qtyfree"
						},
						sort: "barangfree",
						where: {
							// nomor: "nomor",
							barangfree: "barangfree",
							namafree: "namafree",
							// kodesatuan: "kodesatuan"
						},
						value: "aktif = true AND kodecabang = '" + $("#cabang").val() + "' AND kodebarang = '" + kodebarang + "' AND " + qtybarang + " >= qty AND periode = '" + year + month + "'"
					}
				}
			});
		})

		GetDataFree();

		$(document).on('click', '.searchfree', function() {
			_row = $(this);
			$('#chgkodef').val(_row.closest("tr").find("td:eq(1)").text());
			$('#chgnamaf').val(_row.closest("tr").find("td:eq(2)").text());
			$('#chgqtyf').val(Math.floor(parseFloat($("#qtybarang").val()) / parseFloat($("#qtyh").val()) * parseFloat(_row.closest("tr").find("td:eq(9)").text())));
			$('#qtyfree').val(_row.closest("tr").find("td:eq(3)").text());
			$('#chghargaf').val(_row.closest("tr").find("td:eq(4)").text());
			$('#chgdiscf').val(_row.closest("tr").find("td:eq(5)").text());
			$('#chgdiscitemf').val(_row.closest("tr").find("td:eq(6)").text());
			$('#chgdiscpaketf').val(_row.closest("tr").find("td:eq(7)").text());
			$('#chgdiscpaketitemf').val(_row.closest("tr").find("td:eq(8)").text());
			$('#chgsubtotalf').val(0);
		});

		$(document).on('click', '#addfree', function() {
			var kodebarang = $('#chgkodef').val();
			var namabarang = $('#chgnamaf').val();
			var qty = $('#chgqtyf').val();
			var harga = $('#chghargaf').val();
			var disc = $('#chgdiscf').val();
			var discitem = $('#chgdiscitemf').val();
			var discpaket = $('#chgdiscpaketf').val();
			var discpaketitem = $('#chgdiscpaketitemf').val();
			var subtotal = $('#chgsubtotalf').val();

			if (ValidasiAddFree(kodebarang) == 'sukses') {
				myFunction(
					'',
					kodebarang,
					namabarang,
					qty,
					harga,
					disc,
					discitem,
					discpaket,
					discpaketitem,
					subtotal,
					'<div class="text-center"><button data-toggle="modal" data-target="#changeqty" class="editdetail btn btn-sm btn-success mb-0 p-1"><i class="far fa-edit"></i> Edit</button>&nbsp;<button class="deldetail btn btn-danger m-1 p-1"><i class="fa fa-trash mr-1"></i> Delete</button></div>');
				// HitungSummary();


				$('#viewfree').show();
				$('.freebarang').show();
			}
		});

		// $(document).on('click', ".searchfree", function() {
		// 	var kode = $(this).attr("data-id");
		// 	var cabang = $('#cabang').val();
		// 	var qtybarang = $("#qtybarang").val()
		// 	// console.log(qtybarang);
		// 	var kodebarang = $("#kode").val();
		// 	// console.log(kodebarang);
		// 	$.ajax({
		// 		url: "<?= base_url('sales/SalesOrder/DataFreeBarang') ?>",
		// 		method: "POST",
		// 		dataType: "json",
		// 		async: false,
		// 		data: {
		// 			kode: kode,
		// 			cabang: cabang,
		// 			kodebarang: kodebarang
		// 		},
		// 		success: function(data) {
		// 			for (var i = 0; i < data.length; i++) {
		// 				// var kode = data[i].kode;
		// 				var kodebarang = data[i].barangfree.trim();
		// 				var namabarang = data[i].namafree.trim();
		// 				var qty = Math.floor(parseFloat($("#qtybarang").val()) / parseFloat($("#qtyh").val()));
		// 				console.log($("#qtyh").val());
		// 				var harga = data[i].harga.trim();
		// 				var disc = data[i].disc.trim();
		// 				var discitem = data[i].discitem.trim();
		// 				var discpaket = data[i].discpromo.trim();
		// 				var discpaketitem = data[i].discpromoitem.trim();
		// 				var subtotal = data[i].subtotal.trim();
		// 				var no = "";
		// 				var kodestatus = 0;
		// 				if (kodestatus == 0 || kodestatus == '') {
		// 					var action = '<div class="text-center"><button data-toggle="modal" data-target="#changeqty" class="editdetail btn btn-sm btn-success mb-0 p-1"><i class="far fa-edit"></i> Edit</button>&nbsp;<button class="deldetail btn btn-danger m-1 p-1"><i class="fa fa-trash mr-1"></i> Delete</button></div>';

		// 				} else {
		// 					var action = '';
		// 				}

		// 				myFunction(no, kodebarang, namabarang, qty, harga, disc, discitem, discpaket, discpaketitem, subtotal, action);
		// 			}
		// 			$(".freebarang").show();
		// 		}
		// 	}, false);
		// });


		$('#qty').keyup(function() {
			$(this).val(FormatRupiah($(this).val()));
			var qty = $(this).val();
			var table = document.getElementById('t_detail');
			var totaldiskon2 = 0;
			var totaldiskon2item = 0;
			var totalprice = 0;
			var discitem = 0;
			var discpromoitem = 0;
			var totaldiskonatasdua = 0;
			var qtyhitung = DeFormatRupiah(qty);
			var harga = DeFormatRupiah($('#hargajual').val());
			var disc = 0;
			var discitem = 0;
			var discpaket = 0;
			var discpaketitem = 0;
			var hargaafterdisc1 = 0;
			var hargaafterdisc2 = 0;
			var totalprice = 0;

			$.ajax({
				url: "<?= base_url('sales/SalesOrder/CekDiskon'); ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					kodebarang: $('#kodebrg').val(),
					jenisracikan: $('#jenisracikan').val(),
					jenisso: $('#jenisso').val(),
					nomorpelanggan: $('#nomorpelanggan').val(),
					cabang: $('#cabang').val(),
					qty: qty.replace(",", "").replace(",", "").replace(",", "").replace(",", "").replace(",", "").replace(",", ""),
					total: $('#jenisracikan').val().replace(",", "").replace(",", "").replace(",", "").replace(",", "").replace(",", "").replace(",", "")
				},
				success: function(data) {
					// console.log(data.kodebarang);
					// console.log(data.diskon);
					if (DeFormatRupiah(qty) > 0) {
						// discitem = Math.round(parseFloat(harga) * parseFloat(data.diskon) / 100);
						// discpaketitem = Math.round((parseFloat(harga) - parseFloat(discitem)) * parseFloat(discpaket) / 100);
						// totalprice = (Math.round((parseFloat(harga) - parseFloat(discitem) - parseFloat(discpaketitem)) / 10) * 10) * parseFloat(qtyhitung);
						// discitem = Math.ceil((parseFloat(harga) * parseFloat(data.diskon)) / 100);
						// discpaketitem = Math.ceil((parseFloat(harga) - parseFloat(discitem)) * parseFloat(discpaket) / 100);
						// totalprice = (Math.ceil(((parseFloat(harga) - parseFloat(discitem) - parseFloat(discpaketitem)) * parseFloat(qtyhitung)) / 10) * 10);

						// discitem = (Math.ceil((((parseFloat(harga) * parseFloat(qtyhitung)) * parseFloat(data.diskon)) / 100) / 10) * 10);
						// discpaketitem = (Math.ceil((((parseFloat(harga) * parseFloat(qtyhitung)) - parseFloat(discitem)) * parseFloat(discpaket) / 100) / 10) * 10);
						discitem = (Math.round((((parseFloat(harga) * parseFloat(qtyhitung)) * parseFloat(data.diskon)) / 100) / 10) * 10);
						discpaketitem = (Math.round((((parseFloat(harga) * parseFloat(qtyhitung)) - parseFloat(discitem)) * parseFloat(discpaket) / 100) / 10) * 10);
						totalprice = (parseFloat(harga) * parseFloat(qtyhitung)) - parseFloat(discitem) - parseFloat(totalprice)
						// totalprice = ((parseFloat(harga) - parseFloat(discitem) - parseFloat(discpaketitem)) * parseFloat(qtyhitung));
						$('#disc').val(data.diskon.toString());
						$('#discitem').val(FormatRupiah(discitem.toString()));
						$('#discpaket').val(discpaket.toString());
						$('#discpaketitem').val(FormatRupiah(discpaketitem.toString()));
						$('#totalprice').val(FormatRupiah(totalprice.toString()));
					} else {
						$('#disc').val(0);
						$('#discitem').val(0);
						$('#discpaket').val(0);
						$('#discpaketitem').val(0);
						$('#totalprice').val(0);
					}

				}
			}, false);


		})

		$('#chgqty').keyup(function() {
			$(this).val(FormatRupiah($(this).val()));
			var qty = $(this).val();
			var table = document.getElementById('t_detail');
			var totaldiskon2 = 0;
			var totaldiskon2item = 0;
			var totalprice = 0;
			var discitem = 0;
			var discpromoitem = 0;
			var totaldiskonatasdua = 0;
			var qtyhitung = DeFormatRupiah(qty);
			var harga = DeFormatRupiah($('#chgharga').val());
			var disc = 0;
			var discitem = 0;
			var discpaket = 0;
			var discpaketitem = 0;
			var hargaafterdisc1 = 0;
			var hargaafterdisc2 = 0;
			var totalprice = 0;

			$.ajax({
				url: "<?= base_url('sales/SalesOrder/CekDiskon'); ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					kodebarang: $('#chgkode').val(),
					jenisracikan: $('#jenisracikan').val(),
					jenisso: $('#jenisso').val(),
					nomorpelanggan: $('#nomorpelanggan').val(),
					cabang: $('#cabang').val(),
					qty: qty.replace(",", "").replace(",", "").replace(",", "").replace(",", "").replace(",", "").replace(",", ""),
					total: $('#jenisracikan').val().replace(",", "").replace(",", "").replace(",", "").replace(",", "").replace(",", "").replace(",", "")
				},
				success: function(data) {
					// console.log(data);
					// console.log(data.diskon);
					if (DeFormatRupiah(qty) > 0) {
						// discitem = Math.round(parseFloat(harga) * parseFloat(data.diskon) / 100);
						// discpaketitem = Math.round((parseFloat(harga) - parseFloat(discitem)) * parseFloat(discpaket) / 100);
						// // totalprice = (parseFloat(harga) - parseFloat(discitem) - parseFloat(discpaketitem)) * parseFloat(qtyhitung);
						// totalprice = (Math.round((parseFloat(harga) - parseFloat(discitem) - parseFloat(discpaketitem)) / 10) * 10) * parseFloat(qtyhitung);
						// discitem = Math.ceil((parseFloat(harga) * parseFloat(data.diskon)) / 100);
						// discpaketitem = Math.ceil((parseFloat(harga) - parseFloat(discitem)) * parseFloat(discpaket) / 100);
						// totalprice = (Math.ceil(((parseFloat(harga) - parseFloat(discitem) - parseFloat(discpaketitem)) * parseFloat(qtyhitung)) / 10) * 10);

						// discitem = (Math.ceil((((parseFloat(harga) * parseFloat(qtyhitung)) * parseFloat(data.diskon)) / 100) / 10) * 10);
						// discpaketitem = (Math.ceil((((parseFloat(harga) * parseFloat(qtyhitung)) - parseFloat(discitem)) * parseFloat(discpaket) / 100) / 10) * 10);
						discitem = (Math.round((((parseFloat(harga) * parseFloat(qtyhitung)) * parseFloat(data.diskon)) / 100) / 10) * 10);
						discpaketitem = (Math.round((((parseFloat(harga) * parseFloat(qtyhitung)) - parseFloat(discitem)) * parseFloat(discpaket) / 100) / 10) * 10);
						totalprice = (parseFloat(harga) * parseFloat(qtyhitung)) - parseFloat(discitem) - parseFloat(totalprice)
						// totalprice = ((parseFloat(harga) - parseFloat(discitem) - parseFloat(discpaketitem)) * parseFloat(qtyhitung));
						$('#chgdisc').val(FormatRupiah(data.diskon.toString()));
						$('#chgdiscitem').val(FormatRupiah(discitem.toString()));
						$('#chgdiscpaket').val(FormatRupiah(discpaket.toString()));
						$('#chgdiscpaketitem').val(FormatRupiah(discpaketitem.toString()));
						$('#chgsubtotal').val(FormatRupiah(totalprice.toString()));
					} else {
						$('#chgdisc').val(0);
						$('#chgdiscitem').val(0);
						$('#chgdiscpaket').val(0);
						$('#chgdiscpaketitem').val(0);
						$('#chgsubtotal').val(0);
					}

				}
			}, false);

		})

		function RejectSpecialChar(char) {
			return char.toString().replace(/(\r\n|\n|\r)/gm, " ");
		}

		// $("#adddetail").click(function() {
		// 	var table = document.getElementById('t_detail');
		// 	var kodebarang = $('#kodebrg').val();
		// 	var namabarang = $('#namabrg').val();
		// 	var qty = $('#qty').val();
		// 	var harga = $('#hargajual').val();
		// 	var disc = $('#disc').val();
		// 	var discitem = $('#discitem').val();
		// 	var discpaket = $('#discpaket').val();
		// 	var discpaketitem = $('#discpaketitem').val();
		// 	var totalprice = $('#totalprice').val();
		// 	var kodepencarian = kodebarang.replace(/[^a-zA-Z0-9 ]/g, '');

		// 	if (ValidasiAddDetail(kodebarang) == 'sukses') {
		// 		var row = "";
		// 		row =
		// 			'<tr id="' + kodepencarian + '">' +
		// 			'<td nowrap>' + kodebarang + '</td>' +
		// 			'<td nowrap>' + RejectSpecialChar(namabarang) + '</td>' +
		// 			'<td nowrap id="qty' + kodepencarian + '">' + qty + '</td>' +
		// 			'<td nowrap id="harga' + kodepencarian + '">' + harga + '</td>' +
		// 			'<td nowrap id="disc' + kodepencarian + '">' + disc + '</td>' +
		// 			'<td nowrap id="discitem' + kodepencarian + '">' + discitem + '</td>' +
		// 			'<td nowrap id="discpaket' + kodepencarian + '">' + discpaket + '</td>' +
		// 			'<td nowrap id="discpaketitem' + kodepencarian + '">' + discpaketitem + '</td>' +
		// 			'<td nowrap id="totalprice' + kodepencarian + '">' + FormatRupiah(totalprice) + '</td>' +
		// 			'<td nowrap>' +
		// 			'<button data-table="' + kodepencarian + '" data-toggle="modal" data-target="#changeqty" class="editdetail btn btn-sm btn-success mb-0 p-1"><i class="far fa-edit"></i> Edit</button>&nbsp;' +
		// 			'<button data-table="' + kodepencarian + '" class="deldetail btn btn-sm btn-danger mb-0 p-1"><i class="fa fa-trash"></i> Delete</button>' +
		// 			'</td>' +
		// 			'</tr>';
		// 		$('#tb_detail').append(row);
		// 		HitungSummary();
		// 		if ($('#jenisracikan').val() != "") {
		// 			EditListDiskonAtasDua();
		// 		}

		// 		HitungSummary();
		// 		$('#kodebrg').val("");
		// 		$('#namabrg').val("");
		// 		$('#qty').val(0);
		// 		$('#hargajual').val(0);
		// 		$('#disc').val(0);
		// 		$('#discitem').val(0);
		// 		$('#discpaket').val(0);
		// 		$('#discpaketitem').val(0);
		// 		$('#totalprice').val(0);
		// 	}
		// });

		function myFunction(nomor, kodebarang, namabarang, qty, harga, disc, discitem, discpaket, discpaketitem, subtotal, waktu, action) {
			var table = document.getElementById('t_detail').getElementsByTagName('tbody')[0];
			var row = table.insertRow(0);
			var kodepencarian = kodebarang.replace(/[^A-Z0-9]/ig, '');
			console.log(kodepencarian);
			// const d = new Date();
			// let hour = d.getHours();
			// let minutes = d.getMinutes();
			// let seconds = d.getSeconds();
			// // console.log(hour+":"+minutes+":"+seconds);
			// var jam = hour+":"+minutes+":"+seconds;
			row.setAttribute("id", kodepencarian);
			// console.log(row)
			var cell1 = row.insertCell(0);
			var cell2 = row.insertCell(1);
			var cell3 = row.insertCell(2);
			var cell4 = row.insertCell(3);
			var cell5 = row.insertCell(4);
			var cell6 = row.insertCell(5);
			var cell7 = row.insertCell(6);
			var cell8 = row.insertCell(7);
			var cell9 = row.insertCell(8);
			var cell10 = row.insertCell(9);
			var cell11 = row.insertCell(10);
			var cell12 = row.insertCell(11);
			// var cell3 = '<button data-table="' + RejectSpecialChar(kodebarang) + '" class="deldetail btn btn-danger m-1 p-1"><i class="fa fa-trash mr-1"></i> Delete</button>';
			cell1.innerHTML = nomor;
			cell2.innerHTML = kodebarang;
			cell3.innerHTML = namabarang;
			cell4.innerHTML = FormatRupiah(qty);
			cell5.innerHTML = FormatRupiah(harga);
			cell6.innerHTML = disc;
			cell7.innerHTML = FormatRupiah(discitem);
			cell8.innerHTML = discpaket;
			cell9.innerHTML = FormatRupiah(discpaketitem);
			cell10.innerHTML = FormatRupiah(subtotal);
			cell11.innerHTML = waktu;
			cell12.innerHTML = action;
			cell4.setAttribute("id", "qty" + kodepencarian);
			cell5.setAttribute("id", "harga" + kodepencarian);
			cell6.setAttribute("id", "disc" + kodepencarian);
			cell7.setAttribute("id", "discitem" + kodepencarian);
			cell8.setAttribute("id", "discpaket" + kodepencarian);
			cell9.setAttribute("id", "discpaketitem" + kodepencarian);
			cell10.setAttribute("id", "totalprice" + kodepencarian);
			cell11.style.display = 'none';
			// row.innerHTML = kodebarang;

			var tbody = $('#t_detail').find('tbody');
			var newRows = tbody.find('tr').sort(function(a, b) {
				return $('td:eq(10)', a).text().localeCompare($('td:eq(10)', b).text());
			});
			console.log(newRows);
			tbody.append(newRows);
		}

		$('#adddetail').click(function() {
			var kodebarang = $('#kodebrg').val();
			var namabarang = $('#namabrg').val();
			var qty = $('#qty').val();
			var harga = $('#hargajual').val();
			var disc = $('#disc').val();
			var discitem = $('#discitem').val();
			var discpaket = $('#discpaket').val();
			var discpaketitem = $('#discpaketitem').val();
			var totalprice = $('#totalprice').val();
			const d = new Date();
			let hour = d.getHours();
			let minutes = d.getMinutes();
			let seconds = d.getSeconds();
			// console.log(hour+":"+minutes+":"+seconds);
			var waktu = hour + ":" + minutes + ":" + seconds;
			if (ValidasiAddDetail(kodebarang) == 'sukses') {
				myFunction(
					'',
					kodebarang,
					namabarang,
					qty,
					harga,
					disc,
					discitem,
					discpaket,
					discpaketitem,
					totalprice,
					waktu,
					'<div class="text-center"><button data-toggle="modal" data-target="#changeqty" class="editdetail btn btn-sm btn-success mb-0 p-1"><i class="far fa-edit"></i> Edit</button>&nbsp;<button class="deldetail btn btn-danger m-1 p-1"><i class="fa fa-trash mr-1"></i> Delete</button></div>');
				HitungSummary();
				if ($('#jenisracikan').val() != "") {
					EditListDiskonAtasDua();
				}

				HitungSummary();
				GetDataFree();
				$('#kodebrg').val("");
				$('#namabrg').val("");
				$('#qty').val(0);
				$('#hargajual').val(0);
				$('#disc').val(0);
				$('#discitem').val(0);
				$('#discpaket').val(0);
				$('#discpaketitem').val(0);
				$('#totalprice').val(0);
			}
		});

		$('#t_detail').on('click', '.deldetail', function() {
			$(this).closest('tr').remove();
			if ($('#jenisracikan').val() != "") {
				EditListDiskonAtasDua();
			}
			HitungSummary();
		})

		function EditListDiskonAtasDua() {
			var table = document.getElementById('t_detail');
			var hargasetelahdiscsatu = 0;
			var discpromo = 0;
			var discitempromo = 0;
			var subtotal = 0;
			var sublistsblm = 0;
			var totalsub = 0;

			if (table.rows.length > 1) {
				for (var r = 1, n = table.rows.length; r < n; r++) {
					for (var c = 0, m = table.rows[r].cells.length; c < m - 1; c++) {
						// sublistsblm = (Math.round((parseFloat(DeFormatRupiah(table.rows[r].cells[3].innerHTML)) - parseFloat(DeFormatRupiah(table.rows[r].cells[5].innerHTML))) / 10) * 10) * parseFloat(DeFormatRupiah(table.rows[r].cells[2].innerHTML));
						sublistsblm = Math.round(((parseFloat(DeFormatRupiah(table.rows[r].cells[4].innerHTML)) * parseFloat(DeFormatRupiah(table.rows[r].cells[3].innerHTML))) - parseFloat(DeFormatRupiah(table.rows[r].cells[6].innerHTML))) / 10) * 10;
					}
					totalsub += parseFloat(sublistsblm);
				}
			}
			$.ajax({
				url: "<?= base_url('sales/SalesOrder/CekDiskonRacikanPromo'); ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					jenisracikan: $('#jenisracikan').val(),
					cabang: $('#cabang').val(),
					// minorder: $('#total').val().replace(",", "").replace(",", "").replace(",", "").replace(",", "").replace(",", "").replace(",", ""),
					minorder: totalsub.toString(),

				},
				success: function(data) {
					// console.log(data);
					if (data.length > 0) {
						for (var i = 0; i < data.length; i++) {
							discpromo = data[i].diskon;
							// console.log(discpromo);
						}
					} else {
						discpromo = 0;
					}
				}
			}, false);
			// console.log(discpromo);
			table = document.getElementById('t_detail');
			if (table.rows.length > 1) {
				for (var r = 1, n = table.rows.length; r < n; r++) {
					for (var c = 0, m = table.rows[r].cells.length; c < m - 1; c++) {

						hargasetelahdiscsatu = Math.round(((parseFloat(DeFormatRupiah(table.rows[r].cells[4].innerHTML)) * parseFloat(DeFormatRupiah(table.rows[r].cells[3].innerHTML))) - parseFloat(DeFormatRupiah(table.rows[r].cells[6].innerHTML))) / 10) * 10;
						var namabarang = table.rows[r].cells[1].innerHTML.toString();
						console.log(namabarang);


						console.log(parseFloat(DeFormatRupiah(table.rows[r].cells[4].innerHTML)));
						discitempromo = Math.round((parseFloat(hargasetelahdiscsatu) * parseFloat(discpromo) / 100) / 10) * 10;

						subtotal = (parseFloat(hargasetelahdiscsatu) - parseFloat(discitempromo));
						if (parseFloat(DeFormatRupiah(table.rows[r].cells[4].innerHTML)) == "0" && table.rows[r].cells[1].innerHTML.toString() == table.rows[r].cells[1].innerHTML.toString()) {
							$('#discpaket' + table.rows[r].cells[1].innerHTML.toString()).html(0);

							$('#discpaketitem' + table.rows[r].cells[1].innerHTML.toString()).html(0);

							$('#totalprice' + table.rows[r].cells[1].innerHTML.toString()).html(0);
						} else {

							$('#discpaket' + table.rows[r].cells[1].innerHTML.toString().toString().replace('-', '').replace('-', '').replace('-', '').replace('.', '').replace('.', '').replace('.', '').replace('/', '').replace('/', '').replace('/', '')).html(FormatRupiah(discpromo.toString()));

							$('#discpaketitem' + table.rows[r].cells[1].innerHTML.toString().toString().replace('-', '').replace('-', '').replace('-', '').replace('.', '').replace('.', '').replace('.', '').replace('/', '').replace('/', '').replace('/', '')).html(FormatRupiah(discitempromo.toString()));

							$('#totalprice' + table.rows[r].cells[1].innerHTML.toString().toString().replace('-', '').replace('-', '').replace('-', '').replace('.', '').replace('.', '').replace('.', '').replace('/', '').replace('/', '').replace('/', '')).html(FormatRupiah(subtotal.toString()));

						}
					}
				}
			}

		}



		// $(document).on('click', '.deldetail', function() {
		// 	var id = $(this).attr("data-table");
		// 	$('#' + id).remove();
		// 	// HitungSummary();
		// 	if ($('#jenisracikan').val() != "") {
		// 		EditListDiskonAtasDua();
		// 	}
		// 	HitungSummary();
		// });


		$(document).on('click', '.editdetail', function() {
			_row = $(this);
			$('#chgkode').val(_row.closest("tr").find("td:eq(1)").text());
			$('#chgnama').val(_row.closest("tr").find("td:eq(2)").text());
			$('#chgqty').val(_row.closest("tr").find("td:eq(3)").text());
			$('#chgharga').val(_row.closest("tr").find("td:eq(4)").text());
			$('#chgdisc').val(_row.closest("tr").find("td:eq(5)").text());
			$('#chgdiscitem').val(_row.closest("tr").find("td:eq(6)").text());
			$('#chgdiscpaket').val(_row.closest("tr").find("td:eq(7)").text());
			$('#chgdiscpaketitem').val(_row.closest("tr").find("td:eq(8)").text());
			$('#chgsubtotal').val(_row.closest("tr").find("td:eq(9)").text());
		});

		$(document).on('click', '#changedetail', function() {
			// console.log($('#chgkode').val());
			// console.log($('#qtyBRP50' + $('#chgkode').val().toString()));
			$('#qty' + $('#chgkode').val().trim().replace(/[^A-Z0-9]/ig, '')).html(FormatRupiah($('#chgqty').val()));
			$('#disc' + $('#chgkode').val().trim().replace(/[^A-Z0-9]/ig, '')).html(FormatRupiah($('#chgdisc').val()));
			$('#discitem' + $('#chgkode').val().trim().replace(/[^A-Z0-9]/ig, '')).html(FormatRupiah($('#chgdiscitem').val()));
			$('#discpaket' + $('#chgkode').val().trim().replace(/[^A-Z0-9]/ig, '')).html(FormatRupiah($('#chgdiscpaket').val()));
			$('#discpaketitem' + $('#chgkode').val().trim().replace(/[^A-Z0-9]/ig, '')).html(FormatRupiah($('#chgdiscpaketitem').val()));
			$('#totalprice' + $('#chgkode').val().trim().replace(/[^A-Z0-9]/ig, '')).html(FormatRupiah($('#chgsubtotal').val()));
			HitungSummary();
			if ($('#jenisracikan').val() != "") {
				EditListDiskonAtasDua();
			}
			HitungSummary();
			GetDataFree();

			$('#chgkode').val("");
			$('#chgnama').val("");
			$('#chgqty').val(0);
			$('#chgharga').val(0);
			$('#chgdisc').val(0);
			$('#chgdiscitem').val(0);
			$('#chgdiscpaket').val(0);
			$('#chgdiscpaketitem').val(0);
			$('#chgsubtotal').val(0);
		});

		function InsertDataDetail(kodebarang, namabarang, qty, harga, disc, discitem, discpaket, discpaketitem, subtotal, kodestatus) {
			if (discpaket > 0) {
				Statuspaketinfo = "Ya";
			} else {
				Statuspaketinfo = "Tidak";
			}
			var row = "";
			if (kodestatus == 0 || kodestatus == "") {
				row =
					'<tr id="' + kodebarang + '">' +
					'<td nowrap>' + kodebarang + '</td>' +
					'<td nowrap>' + namabarang + '</td>' +
					'<td nowrap id="qty' + kodebarang + '">' + FormatRupiah(qty) + '</td>' +
					'<td nowrap id="harga' + kodebarang + '">' + FormatRupiah(harga) + '</td>' +
					'<td nowrap id="disc' + kodebarang + '">' + disc + '</td>' +
					'<td nowrap id="discitem' + kodebarang + '">' + FormatRupiah(discitem) + '</td>' +
					'<td nowrap id="discpaket' + kodebarang + '">' + discpaket + '</td>' +
					'<td nowrap id="discpaketitem' + kodebarang + '">' + FormatRupiah(discpaketitem) + '</td>' +
					'<td nowrap id="totalprice' + kodebarang + '">' + FormatRupiah(subtotal) + '</td>' +
					'<td nowrap style="display: none;">' + Statuspaketinfo + '</td>' +
					'<td nowrap style="text-align: center; width: 200px;">' +
					'<button data-table="' + kodebarang + '" data-toggle="modal" data-target="#changeqty" class="editdetail btn btn-sm btn-success m-1 p-1"><i class="fas fa-edit"></i> Edit</button>' +
					'<button data-table="' + kodebarang + '" class="deldetail btn btn-sm btn-danger m-1 p-1"><i class="fa fa-trash mr-1"></i> Delete</button>' +
					'</td>' +
					'</tr>';
			} else {
				row =
					'<tr id="' + kodebarang + '">' +
					'<td nowrap>' + kodebarang + '</td>' +
					'<td nowrap>' + namabarang + '</td>' +
					'<td nowrap id="qty' + kodebarang + '">' + FormatRupiah(qty) + '</td>' +
					'<td nowrap id="harga' + kodebarang + '">' + FormatRupiah(harga) + '</td>' +
					'<td nowrap id="disc' + kodebarang + '">' + disc + '</td>' +
					'<td nowrap id="discitem' + kodebarang + '">' + FormatRupiah(discitem) + '</td>' +
					'<td nowrap id="discpaket' + kodebarang + '">' + discpaket + '</td>' +
					'<td nowrap id="discpaketitem' + kodebarang + '">' + FormatRupiah(discpaketitem) + '</td>' +
					'<td nowrap id="totalprice' + kodebarang + '">' + FormatRupiah(subtotal) + '</td>' +
					'<td nowrap style="display: none;">' + Statuspaketinfo + '</td>' +
					'<td nowrap style="text-align: center; width: 200px;"></td>' +
					'</tr>';
			}
			$('#tb_detail').append(row);
		};



		function AmbilDataDetail() {
			var table = document.getElementById('t_detail');
			var arr2 = [];
			for (var r = 1, n = table.rows.length; r < n; r++) {
				var string = "";
				for (var c = 0, m = table.rows[r].cells.length; c < m - 1; c++) {
					if (c == 0) {
						string = "{" + table.rows[0].cells[c].innerHTML + " : '" + table.rows[r].cells[c].innerHTML + "'";
					} else if (c == 1) {
						string = string + ", " + table.rows[0].cells[c].innerHTML + " : '" + table.rows[r].cells[c].innerHTML.replace("'", " ") + "'";
					} else {
						string = string + ", " + table.rows[0].cells[c].innerHTML + " : '" + table.rows[r].cells[c].innerHTML + "'";
					}
				}
				string = string + "}";
				var obj = JSON.stringify(eval('(' + string + ')'));
				var arr = $.parseJSON(obj);
				arr2.push(arr);
			}
			return arr2;
		};

		/* Save */
		document.getElementById('save').addEventListener("click", function(event) {
			event.preventDefault();
			var nomor = $('#nomorso').val();
			var tanggal = $('#tanggalso').val();
			var nopelanggan = $('#nomorpelanggan').val();
			var namapelanggan = $('#namapelanggan').val();
			var plafonso = $('#plafonso').val();
			var outstandingar = $('#outstandingar').val();
			var kodeslm = $('#kodesales').val();
			var namaslm = $('#namasalesman').val();
			var jenisjual = $('#jenisjual').val();
			var jenisso = $('#jenisso').val();
			var jenisracikan = $('#jenisracikan').val();
			var discash = $('#discash').val();
			var discustomer = $('#discustomer').val();
			var discmanual = $('#discmanual').val();
			var nilaicash = $('#nilaicash').val();
			var nilaicustomer = $('#nilaicustomer').val();
			var nilaimanual = $('#nilaimanual').val();
			var total = $('#total').val();
			var dpp = $('#dpp').val();
			var ppn = $('#ppn').val();
			var grandtotal = $('#grandtotal').val();
			var cabang = $('#cabang').val();
			var lokasi = $('#lokasigdg').val();
			var gudang = $('input[name="gudang"]:checked').val();
			var keterangan = $('#note').val();

			var datadetail = AmbilDataDetail();
			if (ValidasiSave(datadetail) == true) {
				$('#save').hide();
				$.ajax({
					url: "<?= base_url("sales/SalesOrder/Save") ?>",
					method: "POST",
					dataType: "json",
					async: true,
					data: {
						nomor: nomor,
						tanggal: tanggal,
						nopelanggan: nopelanggan,
						namapelanggan: namapelanggan,
						plafonso: plafonso,
						outstandingar: outstandingar,
						kodeslm: kodeslm,
						namaslm: namaslm,
						jenisjual: jenisjual,
						jenisso: jenisso,
						lokasi: lokasi,
						jenisracikan: jenisracikan,
						discash: discash,
						discustomer: discustomer,
						discmanual: discmanual,
						nilaicash: nilaicash,
						nilaicustomer: nilaicustomer,
						nilaimanual: nilaimanual,
						total: total,
						dpp: dpp,
						ppn: ppn,
						grandtotal: grandtotal,
						cabang: cabang,
						datadetail: datadetail,
						gudang: gudang,
						keterangan: keterangan
					},
					success: function(data) {
						if (data.nomor != "") {
							$('#save').prop('disabled', true).show();
							Swal.fire({
								icon: 'success',
								title: data.message,
								text: data.nomor
							})
							ClearScreen();
							ClearDataDetail();
							// $('#nomorso').val(data.nomor);
							// SimpanData();
							// Toast.fire({
							// 	icon: 'success',
							// 	title: data.message
							// });
							// Cetak(data.nomor);
						} else {
							$('#save').prop('disabled', false).show();
							// ClearScreen();
							// ClearDataDetail();
							Swal.fire({
								icon: 'error',
								title: data.message,
								// text: data.nomor
							})
							// Toast.fire({
							// 	icon: 'error',
							// 	title: data.message
							// });
						}
					}
				}, false)
			}
		})

		function Cetak(nomor) {
			// console.log(nomor);
			window.open("<?= base_url() ?>cetak_pdf/SalesOrder/Print/" + nomor);
		}

		function SimpanData() {
			$('#nomorso').prop("disabled", true);
			$('#tanggalso').prop("disabled", true);
			$('#jenisjual').prop("disabled", true);
			$('#jenisso').prop("disabled", true);
			$('#lokasigdg').prop("disabled", true);
			$('#jenisracikan').prop("disabled", true);
			$('#nomorpelanggan').prop("disabled", true);
			$('#namapelanggan').prop("disabled", true);
			$('#statuspelanggan').prop("disabled", true);
			$('#plafonso').prop("disabled", true);
			$('#outstandingar').prop("disabled", true);
			$('#kodesales').prop("disabled", true);
			$('#namasalesman').prop("disabled", true);
			$('#total').prop("disabled", true);
			$('#discash').prop("disabled", true);
			$('#discustomer').prop("disabled", true);
			$('#dpp').prop("disabled", true);
			$('#ppn').prop("disabled", true);
			$('#grandtotal').prop("disabled", true);
			$('#kodebrg').prop("disabled", true);
			$('#namabrg').prop("disabled", true);
			$('#qty').prop("disabled", true);
			$('#hargajual').prop("disabled", true);
			$('#disc').prop("disabled", true);
			$('#discitem').prop('disabled', true);
			$('#discpaket').prop("disabled", true);
			$('#discpaketitem').prop('disabled', true);
			$('#totalprice').prop("disabled", true);

			$('#cancel').prop("disabled", false);
			$('#update').prop("disabled", false);
			$('#save').prop("disabled", true);

			$('#caripelanggan').hide();
			$('#carisalesman').hide();
			$('#caribarang').hide();
			$('#adddetail').hide();
			$('.editdetail').hide();
			$('.deldetail').hide();
		}

		function FindData() {
			$('#nomorso').prop("disabled", true);
			$('#tanggalso').prop("disabled", true);

			$('#jenisso').prop("disabled", true);
			$('#lokasigdg').prop("disabled", true);
			$('#jenisracikan').prop("disabled", true);
			$('#nomorpelanggan').prop("disabled", true);
			$('#namapelanggan').prop("disabled", true);
			$('#statuspelanggan').prop("disabled", true);
			$('#plafonso').prop("disabled", true);
			$('#outstandingar').prop("disabled", true);
			$('#kodesales').prop("disabled", true);
			$('#namasalesman').prop("disabled", true);
			$('#total').prop("disabled", true);
			$('#discash').prop("disabled", true);
			$('#discustomer').prop("disabled", true);
			$('#dpp').prop("disabled", true);
			$('#ppn').prop("disabled", true);
			$('#grandtotal').prop("disabled", true);
			$('#kodebrg').prop("disabled", true);
			$('#namabrg').prop("disabled", true);
			$('#qty').prop("disabled", true);
			$('#hargajual').prop("disabled", true);
			$('#disc').prop("disabled", true);
			$('#discitem').prop('disabled', true);
			$('#discpaket').prop("disabled", true);
			$('#discpaketitem').prop('disabled', true);
			$('#totalprice').prop("disabled", true);

			$('#cancel').prop("disabled", true);
			$('#update').prop("disabled", true);
			$('#save').prop("disabled", false);

			$('#caripelanggan').hide();
			$('#carisalesman').hide();
		}

		document.getElementById("find").addEventListener("click", function(event) {
			event.preventDefault();
			$('#tb_detail').empty();
			$('#t_so').DataTable({
				"language": {
					"url": "<?= base_url() ?>assets/json/id.json"
				},
				"destroy": true,
				"searching": true,
				"processing": true,
				"serverSide": true,
				"lengthChange": true,
				"pageLength": 5,
				"lengthMenu": [5, 10, 25, 50],
				"order": [],
				"initComplete": function(settings, json) {
					$('#t_so input[type="search"]').delay(10).focus();
				},
				"ajax": {
					"url": "<?= base_url('sales/SalesOrder/CariDataSalesOrder'); ?>",
					"method": "POST",
					"data": {
						nmtb: "srvt_salesorder",
						field: {
							nomor: "nomor",
							tanggal: "tanggal",
							namapelanggan: "namapelanggan",
							namasalesman: "namasalesman",
							pemakai: "pemakai",
							tglsimpan: "tglsimpan",
							// ppn: "ppn",
							grandtotal: "grandtotal",
							jenisjual: "jenisjual",
							gudang: "gudang",
							lokasi: "lokasi",
						},
						sort: "nomor",
						where: {
							nomor: "nomor",
							namapelanggan: "namapelanggan",
							namasalesman: "namasalesman",
						},
						value: "batal = false AND kodecabang = '" + $('#cabang').val() + "' AND kodestatus < 2"
					},
				},
				"columnDefs": [{
						"targets": 4,
						"render": $.fn.dataTable.render.moment('YYYY-MM-DD HH:mm:ss', 'D MMM YY')
					},
					{
						"targets": 8,
						"render": $.fn.dataTable.render.moment('YYYY-MM-DD HH:mm:ss', 'D MMM YYYY HH:mm')
					},
					{
						"targets": 9,
						"data": "grandtotal",
						"render": function(data, type, row, meta) {
							return FormatRupiah(row[9].toString())
						}
					}, {
						"targets": 10,
						"data": "jenisjual",
						"render": function(data, type, row, meta) {
							return (row[10] == "1") ? "Tunai" : (row[10] == "2") ? "Kredit" : (row[10] == "3") ? "COD" : "Tunai 0%"
						}
					}, {
						"targets": 11,
						"data": "gudang",
						"render": function(data, type, row, meta) {
							return (row[11] == "t") ? "Gudang" : "Admin";
						}
					}
				]
			});
		})

		$(document).on('click', ".cetakproforma", function() {
			var nomor = $(this).attr("data-id");
			Cetak(nomor);
		});

		function Cetak(nomor) {
			// console.log(nomor);
			window.open("<?= base_url() ?>cetak_pdf/SalesOrder/Print/" + nomor);
		}

		$(document).on('click', ".cetaksalesorder", function() {
			var nomor = $(this).attr("data-id");
			CetakSO(nomor);
		});

		function CetakSO(nomor) {
			// console.log(nomor);
			window.open("<?= base_url() ?>cetak_pdf/SO/Print/" + nomor);
		}

		$(document).on('click', ".searchso", function() {
			var nomor = $(this).attr("data-id");
			GetData(nomor);
		});

		function GetData(nomor) {
			var kodestatus = 0;
			$.ajax({
				url: "<?= base_url('sales/SalesOrder/DataSO'); ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					nomor: nomor
				},
				success: function(data) {
					for (var i = 0; i < data.length; i++) {
						$('#nomorso').val(data[i].nomor.trim());
						$('#tanggalso').val(FormatDate(data[i].tanggal.trim()));
						$('#nomorpelanggan').val(data[i].nopelanggan.trim());
						$('#namapelanggan').val(data[i].namapelanggan.trim());
						// $('#plafonso').val(FormatRupiah(data[i].plafonso.toString()));
						$('#note').val(data[i].keterangan.trim());
						$('#kodesales').val(data[i].kodesalesman.trim());
						$('#namasalesman').val(data[i].namasalesman.trim());
						$('#jenisjual').val(data[i].jenisjual.trim());
						$('#jenisso').val(data[i].jenisso.trim());
						$('#lokasigdg').val(data[i].lokasi.trim());
						if (data[i].jenisdisc.trim() == '') {
							$('#jenisracikan').val("");
							$('#namaracikan').val("");
						} else {
							$('#jenisracikan').val(data[i].jenisdisc.trim());
							$('#namaracikan').val(data[i].namaracikan.trim());
						}
						$('#discash').val(FormatRupiah(data[i].disccash.trim()));
						$('#discustomer').val(FormatRupiah(data[i].disccustomer.trim()));
						$('#nilaicash').val(FormatRupiah(data[i].nilaicash.trim()));
						$('#nilaicustomer').val(FormatRupiah(data[i].nilaicustomer.trim()));
						$('#total').val(FormatRupiah(data[i].total.trim()));
						$('#dpp').val(FormatRupiah(data[i].dpp.trim()));
						$('#ppn').val(FormatRupiah(data[i].ppn.trim()));
						$('#grandtotal').val(FormatRupiah(data[i].grandtotal.trim()));
						kodestatus = data[i].kodestatus;
						GetPlatFonCustomer(data[i].nopelanggan.trim());
						LoadOutStandingAR(data[i].nopelanggan.trim());
						if (data[i].gudang.trim() == 't') {
							// $('#gudang').prop('checked', true);
							$("input[name=gudang][value=true]").prop('checked', true);
						} else {
							$('#tidak_gudang').prop('checked', true);
						}
					}
					SimpanData();
					GetDataDetail(nomor, kodestatus);

					if (kodestatus > 0) {
						$('#update').prop('disabled', true);
						$('#cancel').prop('disabled', true);
						$('#jenisjual').prop("disabled", true);
						// SimpanData();
					} else {
						$('#update').prop('disabled', false);
						$('#cancel').prop('disabled', false);
						$('#jenisjual').prop("disabled", false);
						// FindData();
					}
					$('#save').prop('disabled', true);
					$('.barang').hide();
				}
			});
		};

		function GetPlatFonCustomer(nomorpelanggan) {
			$.ajax({
				url: "<?= base_url('masterdata/Customer/DataCustomer'); ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					nomor: nomorpelanggan
				},
				success: function(data) {

					for (var i = 0; i < data.length; i++) {
						// console.log(data[i].limit);
						// console.log(data[i].status);
						$('#plafonso').val(FormatRupiah(data[i].limit.toString()));
						$('#statuspelanggan').val(data[i].status.trim());
					}
				}
			});
		};

		function GetDataDetail(nomor, kodestatus) {
			$.ajax({
				url: "<?= base_url('sales/SalesOrder/DataSODetail'); ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					nomor: nomor
				},
				success: function(data) {
					for (var i = 0; i < data.length; i++) {
						// var kode = data[i].kode;
						var kodebarang = data[i].kodebarang.trim();
						var namabarang = data[i].namabarang.trim();
						var qty = data[i].qty.trim();
						var harga = data[i].harga.trim();
						var disc = data[i].discbarang.trim();
						var discitem = data[i].discbrgitem.trim();
						var discpaket = data[i].discpromo.trim();
						var discpaketitem = data[i].discpaketpromo.trim();
						var subtotal = data[i].total.trim();
						var no = "";
						if (kodestatus == 0 || kodestatus == '') {
							var action = '<div class="text-center"><button data-toggle="modal" data-target="#changeqty" class="editdetail btn btn-sm btn-success mb-0 p-1"><i class="far fa-edit"></i> Edit</button>&nbsp;<button class="deldetail btn btn-danger m-1 p-1"><i class="fa fa-trash mr-1"></i> Delete</button></div>';

						} else {
							var action = '';
						}

						const d = new Date();
						let hour = d.getHours();
						let minutes = d.getMinutes();
						let seconds = d.getSeconds();
						// console.log(hour+":"+minutes+":"+seconds);
						var waktu = hour + ":" + minutes + ":" + seconds;

						myFunction(no, kodebarang, namabarang, qty, harga, disc, discitem, discpaket, discpaketitem, subtotal, waktu, action);
					}
					if ($('#grup').val() == 'FINANCE JKT') {
						$('.editdetail').hide();
						$('.deldetail').hide();
					} else {
						$('.editdetail').show();
						$('.deldetail').show();
					}
				}
			});
		};

		/* Update */
		document.getElementById('update').addEventListener("click", function(event) {
			event.preventDefault();
			var nomor = $('#nomorso').val();
			var discash = $('#discash').val();
			var discustomer = $('#discustomer').val();
			var discmanual = $('#discmanual').val();
			var nilaicash = $('#nilaicash').val();
			var nilaicustomer = $('#nilaicustomer').val();
			var nilaimanual = $('#nilaimanual').val();
			var tanggalso = $('#tanggalso').val();
			var total = $('#total').val();
			var dpp = $('#dpp').val();
			var ppn = $('#ppn').val();
			var grandtotal = $('#grandtotal').val();
			var cabang = $('#cabang').val();
			var lokasi = $('#lokasigdg').val();
			var keterangan = $('#note').val();

			var datadetail = AmbilDataDetail();

			if (ValidasiSave(datadetail) == true) {
				$.ajax({
					url: "<?= base_url("sales/SalesOrder/Update") ?>",
					method: "POST",
					dataType: "json",
					async: true,
					data: {
						nomor: nomor,
						tanggalso: tanggalso,
						discash: discash,
						discustomer: discustomer,
						discmanual: discmanual,
						nilaicash: nilaicash,
						nilaicustomer: nilaicustomer,
						nilaimanual: nilaimanual,
						total: total,
						dpp: dpp,
						ppn: ppn,
						grandtotal: grandtotal,
						cabang: cabang,
						lokasi: lokasi,
						datadetail: datadetail,
						keterangan: keterangan
					},
					success: function(data) {
						// console.log(data)
						if (data.nomor != "") {
							Toast.fire({
								icon: 'success',
								title: data.message
							});
							// ('#nomorso').val(data.nomor);
							SimpanData();
						} else {
							Toast.fire({
								icon: 'error',
								title: data.message
							});
						}
					}
				}, false)
			}
		})

		document.getElementById("okcancel").addEventListener("click", function(event) {
			event.preventDefault();

			var nomor = $('#nomorso').val();
			var keterangan = $('#keteranganbatal').val();
			var cabang = $('#cabang').val();
			var lokasi = $('#lokasigdg').val();
			var datadetail = AmbilDataDetail();

			if (nomor == "") {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Silahkan Pilih No Sales Order yang diinginkan.',
					showCloseButton: true,
					width: 350,
				});
				$('#nomorso').focus();
			} else {
				$.ajax({
					url: "<?= base_url("sales/SalesOrder/Cancel") ?>",
					method: "POST",
					dataType: "json",
					async: true,
					data: {
						nomor: nomor,
						keterangan: keterangan,
						cabang: cabang,
						lokasi: lokasi,
						datadetail: datadetail
					},
					success: function(data) {
						if (data.nomor != "") {
							Toast.fire({
								icon: 'success',
								title: data.message
							});
							location.reload(true);
						} else {
							Toast.fire({
								icon: 'error',
								title: data.message
							});
						}
					}
				}, false)
			}
		})

		document.getElementById("clear").addEventListener("click", function(event) {
			event.preventDefault();
			location.reload(true);
		})

		ClearScreen();

	});
</script>