<script type="text/javascript">
	$(document).ready(function() {
		var discCash = 0;
		var ppn = 0;
		var statuspaket = false;
		var diskonatassatu = false;
		var diskonatasdua = false;
		var diskon1 = 0;
		var diskon2 = 0;

		var datadiscoutatasdua;

		function LoadKonfigurasi() {
			$.ajax({
				url: "<?= base_url('sales/SalesOrder/LoadKonfigurasi') ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {},
				success: function(data) {
					for (var i = 0; i < data.length; i++) {
						ppn = data[i].ppn;
						discCash = data[i].disccash;
					}
				}
			}, false);
		}

		function LoadGudang() {
			$.ajax({
				url: "<?= base_url('sales/SalesOrder/LoadGudang') ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					cabang: $('#cabang').val()
				},
				success: function(data) {
					for (var i = 0; i < data.length; i++) {
						$("#lokasigdg").append('<option value = ' + data[i].kode + ' >' + data[i].nama + '</option>');
					}
				}
			}, false);
		}

		function LoadRacikan() {
			$.ajax({
				url: "<?= base_url('sales/SalesOrder/LoadRacikan') ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					cabang: $('#cabang').val()
				},
				success: function(data) {
					for (var i = 0; i < data.length; i++) {
						$("#jenisracikan").append('<option value = ' + data[i].kode + ' >' + data[i].nama + '</option>');
					}
				}
			}, false);
		};
		// function ClearScreen
		function ClearScreen() {
			LoadKonfigurasi();
			LoadGudang();
			LoadRacikan()
			$('#nomorso').prop("disabled", false).val("");
			$('#tanggalso').prop("disabled", false).val("<?= date('Y-m-d') ?>");
			$('#jenisjual').prop("disabled", false).val("");
			$('#jenisso').prop("disabled", false).val("");
			$('#lokasigdg').prop("disabled", false).val("");
			$('#jenisracikan').prop("disabled", false).val("");
			$('#nomorpelanggan').prop("disabled", false).val("");
			$('#namapelanggan').prop("disabled", false).val("");
			$('#statuspelanggan').val("");
			$('#plafonso').prop("disabled", false).val(0);
			$('#outstandingar').prop("disabled", false).val(0);
			$('#kodesales').prop("disabled", false).val("");
			$('#namasalesman').prop("disabled", false).val("");
			$('#total').val(0);
			$('#discash').val(0);
			$('#discustomer').val(0);
			$('#dpp').val(0);
			$('#ppn').val(0);
			$('#grandtotal').val(0);
			$('#disctunai').val(0);

			$('#kodebrg').prop("disabled", false).val("");
			$('#namabrg').prop("disabled", false).val("");
			$('#qty').prop("disabled", false).val(0);
			$('#hargajual').prop("disabled", false).val(0);
			$('#disc').prop("disabled", false).val(0);
			$('#discitem').prop('disabled', false).val(0);
			$('#discpaket').prop("disabled", false).val(0);
			$('#discpaketitem').prop('disabled', false).val(0);
			$('#totalprice').prop("disabled", false).val(0);

			$('#tb_detail').empty();

			$('#update').prop("disabled", true);
			$('#save').prop("disabled", false);
			$('#caripelanggan').show();
			$('#carisalesman').show();
			$('#caribarang').show();
			$('#adddetail').show();
			$('.editdetail').show();
			$('.deldetail').show();
			$('#lokasigdg').val("");
			$('#jenisracikan').val("");
		}

		function setDateFormat(date) {
			dateObj = new Date(date);
			var day = DateObj.getDate();
			var month = dateObj.getMonth() + 1;
			var fullYear = DateObj.getFullYear().toString();
			var setformattedDate = '';
			setformattedDate = fullYear + '-' + getDigitToFormat(month) + '-' + getDigitToFormat(day);
			return setformattedDate;
		}

		function FormatDate(input) {
			var date = new Date(input);
			var yyyy = date.getFullYear();
			var mm = date.getMonth() + 1;
			var dd = date.getDate();
			var hrs = date.getHours();
			var mnt = date.getMinutes();


			if (dd < 10) {
				dd = '0' + dd;
			}
			if (mm < 10) {
				mm = '0' + mm;
			}
			if (hrs < 10) {
				hrs = '0' + hrs;
			}
			if (mnt < 10) {
				mnt = '0' + mnt;
			}
			return yyyy + '-' + mm + '-' + dd;
		};

		function getDigitToFormat(val) {
			if (val < 10) {
				val = '0' + val;
			}
			return val.toString();
		};

		/* Format Rupiah */
		function FormatRupiah(angka, prefix) {
			if (!angka) {
				return '';
			}
			var vangka = angka.toString();
			var number_string = vangka.replace(/[^.\d]/g, '').replace(/[^\w\s]/gi, '').toString(),
				split = number_string.split('.'),
				sisa = split[0].length % 3,
				rupiah = split[0].substr(0, sisa),
				ribuan = split[0].substr(sisa).match(/\d{3}/gi);

			// tambahkan titik jika yang di input sudah menjadi angka ribuan
			if (ribuan) {
				separator = sisa ? ',' : '';
				rupiah += separator + ribuan.join(',');
			}

			rupiah = split[1] != undefined ? rupiah + '.' + split[1] : rupiah;
			return prefix == undefined ? rupiah : (rupiah ? rupiah : '');
		};

		/* DeFormat Rupiah */
		function DeFormatRupiah(angka) {
			var result = angka.replace(/[^\w\s]/gi, '');

			return result;
		};

		$('#disc[max]:not([max=""])').on('input', function(event) {
			var $this = $(this);
			var maxlength = $this.attr('max').length;
			var value = $this.val();
			if (value && value.length >= maxlength) {
				$this.val(value.substr(0, maxlength));
			}
		});

		$('#jenisjual').change(function() {
			var jenisjual = $('#jenisjual').val();
			if (jenisjual == '1') {
				$('#discash').val(discCash);
				// HitungSummary();
			} else {
				$('#discash').val(0);
				// HitungSummary();
			}
		})

		$('#lokasigdg').change(function() {
			$('#kodebrg').val("");
			$('#namabrg').val("");
			$('#qty').val(0);
			$('#hargajual').val(0);
			$('#disc').val(0);
			$('#discitem').val(0);
			$('#discpaket').val(0);
			$('#discpaketitem').val(0);
			$('#totalprice').val(0);

			$('#tb_detail').empty();
		})


		$('#chgqty').keyup(function() {
			$(this).val(FormatRupiah($(this).val()));
			// (Harga - Disc Barang per Item - Disc Paket per Item) x qty
			var harga = DeFormatRupiah($('#chgharga').val());
			var discitem = DeFormatRupiah($('#chgdiscitem').val());
			var discpaket = DeFormatRupiah($('#chgdiscpaket').val());
			var discpaketitem = (harga - (discitem)) * (discpaket / 100);
			var discpromoitem = $('#chgdiscpaketitem').val(FormatRupiah(discpaketitem));
			var qty = DeFormatRupiah($('#chgqty').val());


			var subtotal = ((harga - discitem - discpaketitem) * qty);
			$('#chgsubtotal').val(FormatRupiah(subtotal));

			var minorder = DeFormatRupiah($('#chgminorder').val());
			if (subtotal >= minorder) {
				$('#chgdiscpaket').val($('#chgdiskonpro').val());
			} else {
				$('#chgdiscpaket').val(0);
			}
		})

		function HitungSummary() {
			var table = document.getElementById('t_detail');
			var sublist = 0;
			var subtotal = 0;
			var discash = $('#discash').val();
			var discustomer = DeFormatRupiah($('#discustomer').val());
			var ppntunai = $('#ppntunai').val();

			for (var r = 1, n = table.rows.length; r < n; r++) {
				for (var c = 0, m = table.rows[r].cells.length; c < m - 1; c++) {
					sublist = parseFloat(DeFormatRupiah(table.rows[r].cells[8].innerHTML));
				}
				subtotal += parseFloat(sublist);
			}

			$('#total').val(FormatRupiah(subtotal));
			var dpp = subtotal - discash - discustomer;
			var ppn = parseFloat(dpp) * (ppntunai / 100);
			var grandtotal = parseFloat(dpp) + parseFloat(Math.round(ppn));
			$('#dpp').val(FormatRupiah(dpp));
			$('#ppn').val(FormatRupiah(Math.round(ppn)));
			$('#grandtotal').val(FormatRupiah(grandtotal));
		};

		function ValidasiSave(datadetail) {
			var nomorpelanggan = $('#nomorpelanggan').val();
			var namapelanggan = $('#namapelanggan').val();
			var plafonso = $('#plafonso').val();
			var outstandingar = $('#outstandingar').val();
			var kodesales = $('#kodesales').val();
			var namasalesman = $('#namasalesman').val();
			var jenisjual = $('#jenisjual').val();
			var jenisso = $('#jenisso').val();
			var lokasigdg = $('#lokasigdg').val();
			var jenisracikan = $('#jenisracikan').val();
			var discash = $('#discash').val();
			var discustomer = $('#discustomer').val();
			var total = $('#total').val();
			var dpp = $('#dpp').val();
			var ppn = $('#ppn').val();
			var grandtotal = $('#grandtotal').val();

			if (jenisjual == '' || jenisjual == 0) {
				Swal.fire({
					title: 'Informasi',
					icon: 'icon',
					html: 'Jenis jual Tidak Boleh Kosong',
					showCloseButton: true,
					width: 350,
				})
				$('#jenisjual').focus();
				var result = false;
			} else if (jenisso == '' || jenisso == 0) {
				Swal.fire({
					title: 'Informasi',
					icon: 'icon',
					html: 'Jenis Sales Order Tidak Boleh Kosong',
					showCloseButton: true,
					width: 350,
				})
				$('#jenisso').focus();
				var result = false;
			} else if (lokasigdg == '' || lokasigdg == 0) {
				Swal.fire({
					title: 'Informasi',
					icon: 'icon',
					html: 'Tanggal Tidak Boleh Kosong',
					showCloseButton: true,
					width: 350,
				})
				$('#lokasigdg').focus();
				var result = false;
			} else if (jenisracikan == '' || jenisracikan == 0) {
				Swal.fire({
					title: 'Informasi',
					icon: 'icon',
					html: 'Jenis Racikan Tidak Boleh Kosong',
					showCloseButton: true,
					width: 350,
				})
				$('#jenisracikan').focus();
				var result = false;
			} else if (nomorpelanggan == '' || nomorpelanggan == 0) {
				Swal.fire({
					title: 'Informasi',
					icon: 'icon',
					html: 'Nomor Pelanggan Tidak Boleh Kosong',
					showCloseButton: true,
					width: 350,
				})
				$('#nomorpelanggan').focus();
				var result = false;
			} else if (namapelanggan == '' || namapelanggan == 0) {
				Swal.fire({
					title: 'Informasi',
					icon: 'icon',
					html: 'Nama Pelanggan Tidak Boleh Kosong',
					showCloseButton: true,
					width: 350,
				})
				$('#namapelanggan').focus();
				var result = false;
			} else if (kodesales == '' || kodesales == 0) {
				Swal.fire({
					title: 'Informasi',
					icon: 'icon',
					html: 'Kode Salesman Tidak Boleh Kosong',
					showCloseButton: true,
					width: 350,
				})
				$('#kodesales').focus();
				var result = false;
			} else if (namasalesman == '' || namasalesman == 0) {
				Swal.fire({
					title: 'Informasi',
					icon: 'icon',
					html: 'Nama Salesman Tidak Boleh Kosong',
					showCloseButton: true,
					width: 350,
				})
				$('#namasalesman').focus();
				var result = false;
			} else if (total == '' || total == 0) {
				Swal.fire({
					title: 'Informasi',
					icon: 'icon',
					html: 'Total Tidak Boleh Kosong',
					showCloseButton: true,
					width: 350,
				})
				$('#total').focus();
				var result = false;
			} else if (discash == '') {
				Swal.fire({
					title: 'Informasi',
					icon: 'icon',
					html: 'Disc Cash Tidak Boleh Kosong',
					showCloseButton: true,
					width: 350,
				})
				$('#discash').focus();
				var result = false;
			} else if (discustomer == '') {
				Swal.fire({
					title: 'Informasi',
					icon: 'icon',
					html: 'Disc Customer Tidak Boleh Kosong',
					showCloseButton: true,
					width: 350,
				})
				$('#discustomer').focus();
				var result = false;
			} else if (dpp == '' || dpp == 0) {
				Swal.fire({
					title: 'Informasi',
					icon: 'icon',
					html: 'Total Tidak Boleh Kosong',
					showCloseButton: true,
					width: 350,
				})
				$('#dpp').focus();
				var result = false;
			} else if (ppn == '' || ppn == 0) {
				Swal.fire({
					title: 'Informasi',
					icon: 'icon',
					html: 'PPN Tidak Boleh Kosong',
					showCloseButton: true,
					width: 350,
				})
				$('#ppn').focus();
				var result = false;
			} else if (plafonso == '') {
				Swal.fire({
					title: 'Informasi',
					icon: 'icon',
					html: 'Plafon SO Tidak Boleh Kosong',
					showCloseButton: true,
					width: 350,
				})
				$('#plafonso').focus();
				var result = false;
			} else if (outstandingar == '') {
				Swal.fire({
					title: 'Informasi',
					icon: 'icon',
					html: 'Outstanding AR Tidak Boleh Kosong',
					showCloseButton: true,
					width: 350,
				})
				$('#outstandingar').focus();
				var result = false;
			} else if (grandtotal == '' || grandtotal == 0) {
				Swal.fire({
					title: 'Informasi',
					icon: 'icon',
					html: 'Total Tidak Boleh Kosong',
					showCloseButton: true,
					width: 350,
				})
				$('#grandtotal').focus();
				var result = false;
			} else if (datadetail.length == 0 || datadetail.length == '0') {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Data Detail tidak boleh kosong.',
					showCloseButton: true,
					width: 350,
				});
				$('#namagudang').focus();
				var result = false;
			} else {
				var result = true;
			}
			return result;
		}

		/* Validasi Data Add Detail */
		function ValidasiAddDetail(kodebarang) {
			var table = document.getElementById('t_detail');

			var kodebarang = $('#kodebrg').val();
			var namabarang = $('#namabrg').val();
			var qty = $('#qty').val();
			var harga = $('#hargajual').val();
			var disc = $('#disc').val();
			var discitem = $('#discitem').val();
			var discpaket = $('#discpaket').val();
			var discpaketitem = $('#discpaketitem').val();
			var totalprice = $('#totalprice').val();

			if (kodebarang == '' || kodebarang == 0) {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Kode Barang tidak boleh kosong.',
					showCloseButton: true,
					width: 350,
				});
				$('#kodebarang').focus();
				return "gagal";
			} else if (namabarang == '' || namabarang == 0) {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Nama Barang tidak boleh kosong.',
					showCloseButton: true,
					width: 350,
				});
				$('#namabarang').focus();
				return "gagal";
			} else if (qty == '' || qty == 0) {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Qty tidak boleh kosong.',
					showCloseButton: true,
					width: 350,
				});
				$('#qty').focus();
				return "gagal";
			} else if (harga == '' || harga == 0) {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Harga tidak boleh kosong.',
					showCloseButton: true,
					width: 350,
				});
				$('#harga').focus();
				return "gagal";
			} else if (disc == '') {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Disc. tidak boleh kosong.',
					showCloseButton: true,
					width: 350,
				});
				$('#disc').focus();
				return "gagal";
			} else if (discitem == '') {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Disc. Item tidak boleh kosong.',
					showCloseButton: true,
					width: 350,
				});
				$('#discitem').focus();
				return "gagal";
			} else if (discpaket == '') {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Disc. Paket tidak boleh kosong.',
					showCloseButton: true,
					width: 350,
				});
				$('#discpaket').focus();
				return "gagal";
			} else if (discpaketitem == '') {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Disc. Paket Item tidak boleh kosong.',
					showCloseButton: true,
					width: 350,
				});
				$('#discpaketitem').focus();
				return "gagal";
			} else {
				for (var r = 1, n = table.rows.length; r < n; r++) {
					var string = "";
					for (var c = 0, m = table.rows[r].cells.length; c < m; c++) {
						if (c == 0) {
							if (table.rows[r].cells[c].innerHTML == kodebarang) {
								Swal.fire({
									title: 'Informasi',
									icon: 'info',
									html: 'Data ini sudah diinput.',
									showCloseButton: true,
									width: 350,
								});
								return "gagal";
							}
						}
					}
				}
			}
			return "sukses"
		}

		const Toast = Swal.mixin({
			toast: true,
			position: 'top-end',
			showConfirmButton: false,
			timer: 3000,
			timerProgressBar: true,
			didOpen: (toast) => {
				toast.addEventListener('mouseenter', Swal.stopTimer);
				toast.addEventListener('mouseleave', Swal.stopTimer);
			}
		});

		document.getElementById("caripelanggan").addEventListener('click', function(event) {
			event.preventDefault();
			$('#t_pelanggan').DataTable({
				"destroy": true,
				"searching": true,
				"processing": true,
				"serverSide": true,
				"lengthChange": false,
				"pageLength": 5,
				"lengthMenu": [5, 25, 50, 100],
				"order": [],
				"ajax": {
					"url": "<?= base_url('masterdata/Customer/CariDataCustomer'); ?>",
					"method": "POST",
					"data": {
						nmtb: "glbm_customer",
						field: {
							nomor: "nomor",
							nama: "nama",
							alamat: "alamat",
							nohp: "nohp",
							notelp: "notelp",
							email: "email",
							status: "status"
						},
						sort: "nomor",
						where: {
							nomor: "nomor"
						},
					},
				}
			});
		});

		$(document).on('click', ".searchcustomer", function() {
			var nomor = $(this).attr("data-id");
			$.ajax({
				url: "<?= base_url('masterdata/Customer/DataCustomer') ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					nomor: nomor
				},
				success: function(data) {
					for (var i = 0; i < data.length; i++) {
						$('#nomorpelanggan').val(data[i].nomor.trim());
						$('#namapelanggan').val(data[i].nama.trim());
						$('#plafonso').val(FormatRupiah(data[i].limit.trim()));
						$('#outstandingar').val(0);
						$('#statuspelanggan').val(data[i].status.trim());
						$('#kodesales').val("");
						$('#namasalesman').val("");
						cekjeniscustomer(data[i].nomor.trim());
					}
				}
			}, false);
		});

		function cekjeniscustomer(nomorpelanggan) {
			$.ajax({
				url: "<?= base_url('sales/SalesOrder/CariJenisCustomer') ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					nomor: nomorpelanggan
				},
				success: function(data) {
					for (var i = 0; i < data.length; i++) {
						if (data[i].tokosendiri == "t") {
							$('#jenisso').attr('disabled', 'disabled').val(2);
							$('#jenisjual').attr('disabled', 'disabled').val(2);
							$('#jenisracikan').attr('disabled', 'disabled');

						} else {
							$('#jenisso').removeAttr('disabled').val("");
							$('#jenisjual').removeAttr('disabled').val("");
							$('#jenisracikan').removeAttr('disabled').val("");
							if (data[i].discountatas == "t") {
								diskonatas = true;
							} else {
								diskonatas = false;
							}
						}
					}
				}
			}, false);
		}



		$('#jenisracikan').change(function() {
			var statuspaket = false;
			var value = $(this).val();

			var table = document.getElementById('t_detail');
			var sublist = 0;
			var subtotal = 0;

			if (table.rows.length > 1) {
				for (var r = 1, n = table.rows.length; r < n; r++) {
					for (var c = 0, m = table.rows[r].cells.length; c < m - 1; c++) {
						sublist = (parseFloat(DeFormatRupiah(table.rows[r].cells[3].innerHTML)) - parseFloat(DeFormatRupiah(table.rows[r].cells[5].innerHTML))) * parseFloat(DeFormatRupiah(table.rows[r].cells[2].innerHTML));
					}
					subtotal += parseFloat(sublist);
				}
			}
			return subtotal;
		});


		document.getElementById("carisalesman").addEventListener("click", function(event) {
			event.preventDefault();

			$('#t_salesman').DataTable({
				"destroy": true,
				"searching": true,
				"processing": true,
				"serverSide": true,
				"lengthChange": false,
				"pageLength": 5,
				"lengthMenu": [5, 25, 50, 100],
				"order": [],
				"ajax": {
					"url": "<?= base_url('masterdata/Salesman/CariDataSalesman'); ?>",
					"method": "POST",
					"data": {
						nmtb: "glbm_salesman",
						field: {
							kode: "kode",
							nama: "nama",
							nohp: "nohp"
						},
						sort: "kode",
						where: {
							kode: "kode"
						},
						value: "kode in ( SELECT kodesalesman FROM glbm_customerdetail WHERE nomor = '" + $('#nomorpelanggan').val() + "')"
					}
				}
			});
		});

		$(document).on('click', ".searchsalesman", function() {
			var kode = $(this).attr("data-id");
			$.ajax({
				url: "<?= base_url('masterdata/Salesman/DataSalesman') ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					kode: kode
				},
				success: function(data) {
					for (var i = 0; i < data.length; i++) {
						$('#kodesales').val(data[i].kode.trim());
						$('#namasalesman').val(data[i].nama.trim());
					}
				}
			}, false);
		});

		document.getElementById("caribarang").addEventListener("click", function(event) {
			event.preventDefault();
			var d = new Date(),
				month = '' + (d.getMonth() + 1),
				day = '' + d.getDate(),
				year = d.getFullYear();
			if (month.length < 2)
				month = '0' + month;
			var kodesales = $('#kodesales').val();
			$('#t_barang').DataTable({
				"destroy": true,
				"searching": true,
				"processing": true,
				"serverSide": true,
				"lengthChange": false,
				"pageLength": 5,
				"lengthMenu": [5, 25, 50, 100],
				"order": [],
				"ajax": {
					"url": "<?= base_url('sales/SalesOrder/CariDataBarang'); ?>",
					"method": "POST",
					"data": {
						nmtb: "cari_barangsales",
						field: {
							kodebarang: "kodebarang",
							namabarang: "namabarang",
							stock: "stock",
							hargajual: "hargajual"

						},
						sort: "kodebarang",
						where: {
							kodebarang: "kodebarang",
							namabarang: "namabarang"
						},
						value: "kodesales = '" + kodesales + "' AND kodecabang = '" + $("#cabang").val() + "' AND periode = '" + year + month + "'",
					}
				}
			});
		});

		$(document).on('click', ".searchbarang", function() {
			var kode = $(this).attr("data-id");
			$.ajax({
				url: "<?= base_url('sales/SalesOrder/DataBarang'); ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					kode: kode,
					jenisracikan: $('#jenisracikan').val(),
				},
				success: function(data) {
					for (var i = 0; i < data.length; i++) {
						$('#kodebrg').val(data[i].kode.trim());
						$('#namabrg').val(data[i].nama.trim());
						$('#hargajual').val(FormatRupiah(data[i].hargajual.trim()));
						// console.log(CekDiskonPromo());
						if ($('#jenisracikan').val() != "" && CekDiskonPromo().length > 0) {
							// console.log("A");
							diskonatasdua = true;
						} else {
							// console.log("b");
							diskonatasdua = false;
						}


					}
				}

			}, false);
			return diskonatasdua;
		});

		function CekDiskonPromoList(kode) {
			$.ajax({
				url: "<?= base_url('sales/SalesOrder/CekDiskonPromo'); ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					kodebarang: kode,
					jenisracikan: $('#jenisracikan').val(),
					cabang: $('#cabang').val(),
				},
				success: function(data) {
					datadiscoutatasdua = data;
				}
			}, false);
			return datadiscoutatasdua;
		}

		function CekDiskonPromo() {
			$.ajax({
				url: "<?= base_url('sales/SalesOrder/CekDiskonPromo'); ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					kodebarang: $('#kodebrg').val(),
					jenisracikan: $('#jenisracikan').val(),
					cabang: $('#cabang').val(),
				},
				success: function(data) {
					datadiscoutatasdua = data;
				}
			}, false);
			return datadiscoutatasdua;
		}

		$('#qty').keyup(function() {
			var qty = FormatRupiah($(this).val());
			var table = document.getElementById('t_detail');
			var totaldiskon2 = 0;
			var totaldiskon2item = 0;
			var totalprice = 0;
			var discitem = 0;
			var discpromoitem = 0;
			var totaldiskonatasdua = 0;
			var totallistdiskonatasdua = 0;
			var qtyhitung = DeFormatRupiah(qty);
			var harga = DeFormatRupiah($('#hargajual').val());
			var disc = DeFormatRupiah($('#disc').val());
			var discitem = Math.round(parseFloat(harga) * parseFloat(disc) / 100);
			var discpaket = 0;
			var discpaketitem = 0;
			var hargaafterdisc1 = parseFloat(harga) - parseFloat(discitem);
			var hargaafterdisc2 = 0;
			var totalprice = 0;
			// console.log(diskonatasdua);
			if (diskonatasdua == false) {
				discpaket = 0;
			} else {
				totaldiskonatasdua = (parseFloat(harga) - parseFloat(discitem)) * parseFloat(qtyhitung)
				totallistdiskonatasdua = TotalListDiskonAtasDua();
				totaldiskon2 = parseFloat(totaldiskonatasdua) + parseFloat(totallistdiskonatasdua)
				if (datadiscoutatasdua.length > 0) {
					for (var i = 0; i < datadiscoutatasdua.length; i++) {
						if (totaldiskon2 >= datadiscoutatasdua[i].minorder) {
							discpaket = datadiscoutatasdua[i].diskon;
							diskon2 = discpaket;
						}
					}
				}
				discpaketitem = Math.round((parseFloat(harga) - parseFloat(discitem)) * parseFloat(discpaket) / 100);
				// console.log(discitem);
				// console.log(hargaafterdisc1);
			}
			totalprice = (parseFloat(harga) - parseFloat(discitem) - parseFloat(discpaketitem)) * parseFloat(qtyhitung);

			$('#disc').val(FormatRupiah(disc.toString()));
			$('#discitem').val(FormatRupiah(discitem.toString()));
			$('#discpaket').val(FormatRupiah(discpaket.toString()));
			$('#discpaketitem').val(FormatRupiah(discpaketitem.toString()));
			$('#totalprice').val(FormatRupiah(totalprice.toString()));
		})



		function TotalListDiskonAtasSatu() {
			var table = document.getElementById('t_detail');
			var sublist = 0;
			var subtotal = 0;

			if (table.rows.length > 1) {
				for (var r = 1, n = table.rows.length; r < n; r++) {
					for (var c = 0, m = table.rows[r].cells.length; c < m - 1; c++) {
						sublist = parseFloat(DeFormatRupiah(table.rows[r].cells[3].innerHTML)) * parseFloat(DeFormatRupiah(table.rows[r].cells[2].innerHTML));
					}
					subtotal += parseFloat(sublist);
				}
			}
			return subtotal;
		}

		function TotalListDiskonAtasDua() {
			var table = document.getElementById('t_detail');
			var sublist = 0;
			var subtotal = 0;

			if (table.rows.length > 1) {
				for (var r = 1, n = table.rows.length; r < n; r++) {
					for (var c = 0, m = table.rows[r].cells.length; c < m - 1; c++) {
						sublist = (parseFloat(DeFormatRupiah(table.rows[r].cells[3].innerHTML)) - parseFloat(DeFormatRupiah(table.rows[r].cells[5].innerHTML))) * parseFloat(DeFormatRupiah(table.rows[r].cells[2].innerHTML));
					}
					subtotal += parseFloat(sublist);
				}
			}
			return subtotal;
		}

		function CekDiskonPelangganKategori() {
			$.ajax({
				url: "<?= base_url('sales/SalesOrder/DataBarang'); ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					kode: kode,
					jenisracikan: $('#jenisracikan').val(),
				},
				success: function(data) {
					for (var i = 0; i < data.length; i++) {
						$('#kodebrg').val(data[i].kode.trim());
						$('#namabrg').val(data[i].nama.trim());
						$('#disc').val(FormatRupiah(data[i].diskon.trim()));
						$('#hargajual').val(FormatRupiah(data[i].hargajual.trim()));
						$('#discitem').val(FormatRupiah(data[i].discitem.trim()));
						$('#minorder').val(FormatRupiah(data[i].minorder.trim()));
						$('#diskonpro').val(FormatRupiah(data[i].diskonpromo.trim()));
					}
				}
			}, false);
		}

		function CekDiskonKhusus() {
			$.ajax({
				url: "<?= base_url('sales/SalesOrder/DataBarang'); ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					kode: kode,
					jenisracikan: $('#jenisracikan').val(),
				},
				success: function(data) {
					for (var i = 0; i < data.length; i++) {
						$('#kodebrg').val(data[i].kode.trim());
						$('#namabrg').val(data[i].nama.trim());
						$('#disc').val(FormatRupiah(data[i].diskon.trim()));
						$('#hargajual').val(FormatRupiah(data[i].hargajual.trim()));
						$('#discitem').val(FormatRupiah(data[i].discitem.trim()));
						$('#minorder').val(FormatRupiah(data[i].minorder.trim()));
						$('#diskonpro').val(FormatRupiah(data[i].diskonpromo.trim()));
					}
				}
			}, false);
		}

		function CekDiskonKelompoAJ() {
			$.ajax({
				url: "<?= base_url('sales/SalesOrder/DataBarang'); ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					kode: kode,
					jenisracikan: $('#jenisracikan').val(),
				},
				success: function(data) {
					for (var i = 0; i < data.length; i++) {
						$('#kodebrg').val(data[i].kode.trim());
						$('#namabrg').val(data[i].nama.trim());
						$('#disc').val(FormatRupiah(data[i].diskon.trim()));
						$('#hargajual').val(FormatRupiah(data[i].hargajual.trim()));
						$('#discitem').val(FormatRupiah(data[i].discitem.trim()));
						$('#minorder').val(FormatRupiah(data[i].minorder.trim()));
						$('#diskonpro').val(FormatRupiah(data[i].diskonpromo.trim()));
					}
				}
			}, false);
		}

		function CekDiskonLevel() {
			$.ajax({
				url: "<?= base_url('sales/SalesOrder/DataBarang'); ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					kode: kode,
					jenisracikan: $('#jenisracikan').val(),
				},
				success: function(data) {
					for (var i = 0; i < data.length; i++) {
						$('#kodebrg').val(data[i].kode.trim());
						$('#namabrg').val(data[i].nama.trim());
						$('#disc').val(FormatRupiah(data[i].diskon.trim()));
						$('#hargajual').val(FormatRupiah(data[i].hargajual.trim()));
						$('#discitem').val(FormatRupiah(data[i].discitem.trim()));
						$('#minorder').val(FormatRupiah(data[i].minorder.trim()));
						$('#diskonpro').val(FormatRupiah(data[i].diskonpromo.trim()));
					}
				}
			}, false);
		}



		$("#adddetail").click(function() {
			var table = document.getElementById('t_detail');
			var kodebarang = $('#kodebrg').val();
			var namabarang = $('#namabrg').val();
			var qty = $('#qty').val();
			var harga = $('#hargajual').val();
			var disc = $('#disc').val();
			var discitem = $('#discitem').val();
			var discpaket = $('#discpaket').val();
			var discpaketitem = $('#discpaketitem').val();
			var totalprice = $('#totalprice').val();

			if (ValidasiAddDetail(kodebarang) == 'sukses') {
				var row = "";
				row =
					'<tr id="' + kodebarang + '">' +
					'<td>' + kodebarang + '</td>' +
					'<td>' + namabarang + '</td>' +
					'<td>' + qty + '</td>' +
					'<td>' + harga + '</td>' +
					'<td id="disc' + kodebarang + '">' + disc + '</td>' +
					'<td id="discitem' + kodebarang + '">' + discitem + '</td>' +
					'<td id="discpaket' + kodebarang + '">' + discpaket + '</td>' +
					'<td id="discpaketitem' + kodebarang + '">' + discpaketitem + '</td>' +
					'<td id="totalprice' + kodebarang + '">' + FormatRupiah(totalprice) + '</td>' +
					'<td>' +
					'<button data-table="' + kodebarang + '" data-toggle="modal" data-target="#changeqty" class="editdetail btn btn-sm btn-success mb-0 p-1"><i class="far fa-edit"></i> Edit</button>&nbsp;' +
					'<button data-table="' + kodebarang + '" class="deldetail btn btn-sm btn-danger mb-0 p-1"><i class="fa fa-trash"></i> Delete</button>' +
					'</td>' +
					'</tr>';
				$('#tb_detail').append(row);

				EditListDiskonAtasDua();


				HitungSummary();
				$('#kodebrg').val("");
				$('#namabrg').val("");
				$('#qty').val(0);
				$('#hargajual').val(0);
				$('#disc').val(0);
				$('#discitem').val(0);
				$('#discpaket').val(0);
				$('#discpaketitem').val(0);
				$('#totalprice').val(0);
			}
		});

		function EditListDiskonAtasDua() {
			var table = document.getElementById('t_detail');
			var hargasetelahdiscsatu = 0;
			var discitempromo = 0;
			var subtotal = 0;


			if (table.rows.length > 2) {
				for (var r = 1, n = table.rows.length; r < n; r++) {
					for (var c = 0, m = table.rows[r].cells.length; c < m - 1; c++) {
						// sublist = (parseFloat(DeFormatRupiah(table.rows[r].cells[3].innerHTML)) - parseFloat(DeFormatRupiah(table.rows[r].cells[5].innerHTML))) * parseFloat(DeFormatRupiah(table.rows[r].cells[2].innerHTML));						
						hargasetelahdiscsatu = (parseFloat(DeFormatRupiah(table.rows[r].cells[3].innerHTML)) - parseFloat(DeFormatRupiah(table.rows[r].cells[5].innerHTML)));
						discitempromo = Math.round(parseFloat(hargasetelahdiscsatu) * parseFloat(diskon2) / 100);
						subtotal = (parseFloat(hargasetelahdiscsatu) - parseFloat(discitempromo)) * parseFloat(DeFormatRupiah(table.rows[r].cells[2].innerHTML));
						$('#discpaket' + table.rows[r].cells[0].innerHTML.toString()).html(FormatRupiah(diskon2.toString()));
						$('#discpaketitem' + table.rows[r].cells[0].innerHTML.toString()).html(FormatRupiah(discitempromo.toString()));
						$('#totalprice' + table.rows[r].cells[0].innerHTML.toString()).html(FormatRupiah(subtotal.toString()));
					}
				}
			}
		}

		function EditListDiskonAtasDua() {
			var table = document.getElementById('t_detail');
			var hargasetelahdiscsatu = 0;
			var discitempromo = 0;
			var subtotal = 0;


			if (table.rows.length > 2) {
				for (var r = 1, n = table.rows.length; r < n; r++) {
					for (var c = 0, m = table.rows[r].cells.length; c < m - 1; c++) {
						// sublist = (parseFloat(DeFormatRupiah(table.rows[r].cells[3].innerHTML)) - parseFloat(DeFormatRupiah(table.rows[r].cells[5].innerHTML))) * parseFloat(DeFormatRupiah(table.rows[r].cells[2].innerHTML));						
						hargasetelahdiscsatu = (parseFloat(DeFormatRupiah(table.rows[r].cells[3].innerHTML)) - parseFloat(DeFormatRupiah(table.rows[r].cells[5].innerHTML)));
						discitempromo = Math.round(parseFloat(hargasetelahdiscsatu) * parseFloat(diskon2) / 100);
						subtotal = (parseFloat(hargasetelahdiscsatu) - parseFloat(discitempromo)) * parseFloat(DeFormatRupiah(table.rows[r].cells[2].innerHTML));
						$('#discpaket' + table.rows[r].cells[0].innerHTML.toString()).html(FormatRupiah(diskon2.toString()));
						$('#discpaketitem' + table.rows[r].cells[0].innerHTML.toString()).html(FormatRupiah(discitempromo.toString()));
						$('#totalprice' + table.rows[r].cells[0].innerHTML.toString()).html(FormatRupiah(subtotal.toString()));
					}
				}
			}
		}

		$(document).on('click', '.deldetail', function() {
			var id = $(this).attr("data-table");
			$('#' + id).remove();
			var table = document.getElementById('t_detail');
			console.log(table.rows.length);
			HitungSummary();
		});

		$(document).on('click', '.editdetail', function() {
			var table = document.getElementById('t_detail');
			var tr = document.getElementById($(this).attr("data-table"));
			var td = tr.getElementsByTagName("td");
			for (var c = 0, m = table.rows[0].cells.length; c < m - 1; c++) {
				var chgth = table.rows[0].cells[c].innerHTML.replace(/ /g, '').replace(/\s/g, '').toLowerCase();
				var valuetd = td[c].innerHTML;
				$('#chg' + chgth).val(valuetd);
			}
		});

		function InsertDataDetail(kodebarang, namabarang, qty, harga, disc, discitem, discpaket, discpaketitem, subtotal) {

			var row = "";
			row =
				'<tr id="' + kodebarang + '">' +
				'<td>' + kodebarang + '</td>' +
				'<td>' + namabarang + '</td>' +
				'<td>' + FormatRupiah(qty) + '</td>' +
				'<td>' + FormatRupiah(harga) + '</td>' +
				'<td id="disc' + kodebarang + '">' + disc + '</td>' +
				'<td id="discitem' + kodebarang + '">' + FormatRupiah(discitem) + '</td>' +
				'<td id="discpaket' + kodebarang + '">' + discpaket + '</td>' +
				'<td id="discpaketitem' + kodebarang + '">' + FormatRupiah(discpaketitem) + '</td>' +
				'<td id="subtotal' + kodebarang + '">' + FormatRupiah(subtotal) + '</td>' +
				'<td style="text-align: center; width: 200px;">' +
				'<button data-table="' + kodebarang + '" data-toggle="modal" data-target="#changeqty" class="editdetail btn btn-sm btn-success m-1 p-1"><i class="fas fa-edit"></i> Edit</button>' +
				'<button data-table="' + kodebarang + '" class="deldetail btn btn-sm btn-danger m-1 p-1"><i class="fa fa-trash mr-1"></i> Delete</button>' +
				'</td>' +
				'</tr>';
			$('#tb_detail').append(row);
		};

		$(document).on('click', '#changedetail', function() {
			// var kode = $('#chgkode').val(); 
			var kodebarang = $('#chgkode').val();
			var namabarang = $('#chgnama').val();
			var qty = $('#chgqty').val();
			var harga = $('#chgharga').val();
			var disc = $('#chgdisc').val();
			var discitem = $('#chgdiscitem').val();
			var discpaket = $('#chgdiscpaket').val();
			var discpaketitem = $('#chgdiscpaketitem').val();
			var subtotal = $('#chgsubtotal').val();
			$('#' + kodebarang).remove();
			InsertDataDetail(kodebarang, namabarang, qty, harga, disc, discitem, discpaket, discpaketitem, subtotal);
			HitungSummary();
		});

		function AmbilDataDetail() {
			var table = document.getElementById('t_detail');
			var arr2 = [];
			for (var r = 1, n = table.rows.length; r < n; r++) {
				var string = "";
				for (var c = 0, m = table.rows[r].cells.length; c < m - 1; c++) {
					if (c == 0) {
						string = "{" + table.rows[0].cells[c].innerHTML + " : '" + table.rows[r].cells[c].innerHTML + "'";
					} else {
						string = string + ", " + table.rows[0].cells[c].innerHTML + " : '" + table.rows[r].cells[c].innerHTML + "'";
					}
				}
				string = string + "}";
				var obj = JSON.stringify(eval('(' + string + ')'));
				var arr = $.parseJSON(obj);
				arr2.push(arr);
			}
			return arr2;
		};

		/* Save */
		document.getElementById('save').addEventListener("click", function(event) {
			event.preventDefault();
			var nomor = $('#nomorso').val();
			var tanggal = $('#tanggalso').val();
			var nopelanggan = $('#nomorpelanggan').val();
			var namapelanggan = $('#namapelanggan').val();
			var plafonso = $('#plafonso').val();
			var outstandingar = $('#outstandingar').val();
			var kodeslm = $('#kodesales').val();
			var namaslm = $('#namasalesman').val();
			var jenisjual = $('#jenisjual').val();
			var jenisso = $('#jenisso').val();
			var lokasi = $('#lokasigdg').val();
			var jenisracikan = $('#jenisracikan').val();
			var discash = $('#discash').val();
			var discustomer = $('#discustomer').val();
			var total = $('#total').val();
			var dpp = $('#dpp').val();
			var ppn = $('#ppn').val();
			var grandtotal = $('#grandtotal').val();

			var datadetail = AmbilDataDetail();

			if (ValidasiSave(datadetail) == true) {
				$.ajax({
					url: "<?= base_url("sales/SalesOrder/Save") ?>",
					method: "POST",
					dataType: "json",
					async: true,
					data: {
						nomor: nomor,
						tanggal: tanggal,
						nopelanggan: nopelanggan,
						namapelanggan: namapelanggan,
						plafonso: plafonso,
						outstandingar: outstandingar,
						kodeslm: kodeslm,
						namaslm: namaslm,
						jenisjual: jenisjual,
						jenisso: jenisso,
						lokasi: lokasi,
						jenisracikan: jenisracikan,
						discash: discash,
						discustomer: discustomer,
						total: total,
						dpp: dpp,
						ppn: ppn,
						grandtotal: grandtotal,
						datadetail: datadetail
					},
					success: function(data) {
						if (data.nomor != "") {
							Toast.fire({
								icon: 'success',
								title: data.message
							});
							$('#nomorso').prop("disabled", true).val(data.nomor);
							$('#tanggalso').prop("disabled", true);
							$('#jenisjual').prop("disabled", true);
							$('#jenisso').prop("disabled", true);
							$('#lokasigdg').prop("disabled", true);
							$('#jenisracikan').prop("disabled", true);
							$('#nomorpelanggan').prop("disabled", true);
							$('#namapelanggan').prop("disabled", true);
							$('#statuspelanggan').prop("disabled", true);
							$('#plafonso').prop("disabled", true);
							$('#outstandingar').prop("disabled", true);
							$('#kodesales').prop("disabled", true);
							$('#namasalesman').prop("disabled", true);
							$('#total').prop("disabled", true);
							$('#discash').prop("disabled", true);
							$('#discustomer').prop("disabled", true);
							$('#dpp').prop("disabled", true);
							$('#ppn').prop("disabled", true);
							$('#grandtotal').prop("disabled", true);
							$('#kodebrg').prop("disabled", true);
							$('#namabrg').prop("disabled", true);
							$('#qty').prop("disabled", true);
							$('#hargajual').prop("disabled", true);
							$('#disc').prop("disabled", true);
							$('#discitem').prop('disabled', true);
							$('#discpaket').prop("disabled", true);
							$('#discpaketitem').prop('disabled', true);
							$('#totalprice').prop("disabled", true);

							$('#update').prop("disabled", true);
							$('#save').prop("disabled", false);

							$('#caripelanggan').hide();
							$('#carisalesman').hide();
							$('#caribarang').hide();
							$('#adddetail').hide();
							$('.editdetail').hide();
							$('.deldetail').hide();
						} else {
							Toast.fire({
								icon: 'error',
								title: data.message
							});
						}
					}
				}, false)
			}
		})

		document.getElementById("find").addEventListener("click", function(event) {
			event.preventDefault();
			$('#tb_detail').empty();
			$('#t_so').DataTable({
				"destroy": true,
				"searching": true,
				"processing": true,
				"serverSide": true,
				"lengthChange": true,
				"pageLength": 5,
				"lengthMenu": [5, 10, 25, 50],
				"order": [],
				"ajax": {
					"url": "<?= base_url('sales/SalesOrder/CariDataSalesOrder'); ?>",
					"method": "POST",
					"data": {
						nmtb: "cari_so",
						field: {
							nomor: "nomor",
							tanggal: "tanggal",
							namapelanggan: "namapelanggan",
							namasalesman: "namasalesman",
							jenisjual: "jenisjual",
							jenisso: "jenisso",
							lokasi: "lokasi",
							jenisdisc: "jenisdisc"
						},
						sort: "nomor",
						where: {
							nomor: "nomor"
						}
					},
				},
				"columnDefs": [{
						"targets": 2,
						"render": $.fn.dataTable.render.moment('YYYY-MM-DD HH:mm:ss', 'D MMM YY')
					},
					{
						"targets": 5,
						"data": "jenisjual",
						"render": function(data, type, row, meta) {
							return (row[5] == '1') ? "<p>Tunai</p>" : "<p>Kredit</p>"
						}
					}, {
						"targets": 6,
						"data": "jenisso",
						"render": function(data, type, row, meta) {
							return (row[6] == '1') ? "<p>Customer</p>" : "<p>Internal</p>"
						}
					}
				]
			});
		})

		$(document).on('click', ".searchso", function() {
			var nomor = $(this).attr("data-id");
			GetData(nomor);
		});

		function GetData(nomor) {
			$.ajax({
				url: "<?= base_url('sales/SalesOrder/DataSO'); ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					nomor: nomor
				},
				success: function(data) {
					for (var i = 0; i < data.length; i++) {
						$('#nomorso').val(data[i].nomor.trim());
						$('#tanggalso').val(FormatDate(data[i].tanggal.trim()));
						$('#nomorpelanggan').val(data[i].nopelanggan.trim());
						$('#namapelanggan').val(data[i].namapelanggan.trim());
						$('#plafonso').val(data[i].plafonso.trim());
						$('#outstandingar').val(data[i].outstandingar.trim());
						$('#kodesales').val(data[i].kodesalesman.trim());
						$('#namasalesman').val(data[i].namasalesman.trim());
						$('#jenisjual').val(data[i].jenisjual.trim());
						$('#jenisso').val(data[i].jenisso.trim());
						$('#lokasigdg').val(data[i].lokasi.trim());
						$('#jenisracikan').val(data[i].jenisdisc.trim());
						$('#discash').val(FormatRupiah(data[i].disccash.trim()));
						$('#discustomer').val(FormatRupiah(data[i].disccustomer.trim()));
						$('#total').val(FormatRupiah(data[i].total.trim()));
						$('#dpp').val(FormatRupiah(data[i].dpp.trim()));
						$('#ppn').val(FormatRupiah(data[i].ppn.trim()));
						$('#grandtotal').val(FormatRupiah(data[i].grandtotal.trim()));
					}
					GetDataDetail(nomor);
					$('#update').prop('disabled', false);
					$('#save').prop('disabled', true);
				}
			});
		};

		function GetDataDetail(nomor) {
			$.ajax({
				url: "<?= base_url('sales/SalesOrder/DataSODetail'); ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					nomor: nomor
				},
				success: function(data) {
					for (var i = 0; i < data.length; i++) {
						// var kode = data[i].kode;
						var kodebarang = data[i].kodebarang.trim();
						var namabarang = data[i].namabarang.trim();
						var qty = data[i].qty.trim();
						var harga = data[i].harga.trim();
						var disc = data[i].discbarang.trim();
						var discitem = data[i].discbrgitem.trim();
						var discpaket = data[i].discpromo.trim();
						var discpaketitem = data[i].discpaketpromo.trim();
						var subtotal = data[i].total.trim();

						InsertDataDetail(kodebarang, namabarang, qty, harga, disc, discitem, discpaket, discpaketitem, subtotal);
					}
				}
			});
		};

		/* Update */
		document.getElementById('update').addEventListener("click", function(event) {
			event.preventDefault();
			var nomor = $('#nomorso').val();
			var tanggal = $('#tanggalso').val();
			var nopelanggan = $('#nomorpelanggan').val();
			var namapelanggan = $('#namapelanggan').val();
			var plafonso = $('#plafonso').val();
			var outstandingar = $('#outstandingar').val();
			var kodeslm = $('#kodesales').val();
			var namaslm = $('#namasalesman').val();
			var jenisjual = $('#jenisjual').val();
			var jenisso = $('#jenisso').val();
			var lokasi = $('#lokasigdg').val();
			var jenisracikan = $('#jenisracikan').val();
			var discash = $('#discash').val();
			var discustomer = $('#discustomer').val();
			var total = $('#total').val();
			var dpp = $('#dpp').val();
			var ppn = $('#ppn').val();
			var grandtotal = $('#grandtotal').val();

			var datadetail = AmbilDataDetail();

			if (ValidasiSave(datadetail) == true) {
				$.ajax({
					url: "<?= base_url("sales/SalesOrder/Update") ?>",
					method: "POST",
					dataType: "json",
					async: true,
					data: {
						nomor: nomor,
						tanggal: tanggal,
						nopelanggan: nopelanggan,
						namapelanggan: namapelanggan,
						plafonso: plafonso,
						outstandingar: outstandingar,
						kodeslm: kodeslm,
						namaslm: namaslm,
						jenisjual: jenisjual,
						jenisso: jenisso,
						lokasi: lokasi,
						jenisracikan: jenisracikan,
						discash: discash,
						discustomer: discustomer,
						total: total,
						dpp: dpp,
						ppn: ppn,
						grandtotal: grandtotal,
						datadetail: datadetail
					},
					success: function(data) {
						if (data.nomor != "") {
							Toast.fire({
								icon: 'success',
								title: data.message
							});
							$('#nomorso').prop("disabled", true).val(data.nomor);
							$('#tanggalso').prop("disabled", true);
							$('#jenisjual').prop("disabled", true);
							$('#jenisso').prop("disabled", true);
							$('#lokasigdg').prop("disabled", true);
							$('#jenisracikan').prop("disabled", true);
							$('#nomorpelanggan').prop("disabled", true);
							$('#namapelanggan').prop("disabled", true);
							$('#statuspelanggan').prop("disabled", true);
							$('#plafonso').prop("disabled", true);
							$('#outstandingar').prop("disabled", true);
							$('#kodesales').prop("disabled", true);
							$('#namasalesman').prop("disabled", true);
							$('#total').prop("disabled", true);
							$('#discash').prop("disabled", true);
							$('#discustomer').prop("disabled", true);
							$('#dpp').prop("disabled", true);
							$('#ppn').prop("disabled", true);
							$('#grandtotal').prop("disabled", true);
							$('#kodebrg').prop("disabled", true);
							$('#namabrg').prop("disabled", true);
							$('#qty').prop("disabled", true);
							$('#hargajual').prop("disabled", true);
							$('#disc').prop("disabled", true);
							$('#discitem').prop('disabled', true);
							$('#discpaket').prop("disabled", true);
							$('#discpaketitem').prop('disabled', true);
							$('#totalprice').prop("disabled", true);

							$('#update').prop("disabled", true);
							$('#save').prop("disabled", false);

							$('#caripelanggan').hide();
							$('#carisalesman').hide();
							$('#caribarang').hide();
							$('#adddetail').hide();
							$('.editdetail').hide();
							$('.deldetail').hide();
						} else {
							Toast.fire({
								icon: 'error',
								title: data.message
							});
						}
					}
				}, false)
			}
		})

		document.getElementById("clear").addEventListener("click", function(event) {
			event.preventDefault();
			ClearScreen();
		})

		ClearScreen();

	});
</script>