<script type="text/javascript">
    $(document).ready(function() {

        function ClearScreen() {
            $('#kodegudang').val('');
            $('#tb_detail').empty();
        }

        /* Declare Toast */
        const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000,
            timerProgressBar: true,
            didOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
            }
        });

        document.getElementById('carigudang').addEventListener("click", function(event) {
            event.preventDefault();
            $('#t_gudang').DataTable({
                "destroy": true,
                "searching": true,
                "processing": true,
                "serverSide": true,
                "lengthChange": true,
                "pageLength": 5,
                "lengthMenu": [5, 10, 25, 50],
                "order": [],
                "ajax": {
                    "url": "<?= base_url('masterdata/Gudang/CariDataGudang'); ?>",
                    "method": "POST",
                    "data": {
                        nmtb: "glbm_gudang",
                        field: {
                            kode: "kode",
                            nama: "nama",
                            alamat: "alamat"
                        },
                        sort: "kode",
                        where: {
                            kode: "kode"
                        },
                        value: "aktif = true AND kodecabang = '" + $('#cabang').val() + "'"
                    },
                }
            });
        });

        $(document).on('click', '.searchgudang', function() {
            var kode = $(this).attr("data-id");
            $.ajax({
                url: "<?= base_url('masterdata/Gudang/DataGudang') ?>",
                method: "POST",
                dataType: "json",
                async: false,
                data: {
                    kode: kode
                },
                success: function(data) {
                    for (var i = 0; i < data.length; i++) {
                        $('#kodegudang').val(data[i].kode.trim());


                    }
                }
            });
        });

        document.getElementById("cari_data").addEventListener("click", function(event) {
            event.preventDefault();
            var kodegudang = $('#kodegudang').val();
            $('#t_detail').DataTable({
                "language": {
                    "url": "<?= base_url() ?>assets/json/id.json"
                },
                "destroy": true,
                "searching": true,
                "serverSide": true,
                "lengthChange": true,
                "pageLength": 5,
                "lengthMenu": [5, 10, 25, 50],
                "order": [
                    [1, 'desc']
                ],
                "ajax": {
                    "url": "<?= base_url('sales/Invoice/CetakAdmin'); ?>",
                    "method": "POST",
                    "data": {
                        nmtb: "cari_reprint",
                        field: {
                            nomor: "nomor",
                            // nomorsj: "nomorsj",
                            tanggal: "tanggal",
                            nomorso: "nomorso",
                            tgljthtempo: "tgljthtempo",
                            namapelanggan: "namapelanggan",
                            namacabang: "namacabang",
                            namagudang: "namagudang"
                        },
                        sort: "nomor",
                        where: {
                            nomor: "nomor",
                            namapelanggan: "namapelanggan"
                        },
                        value: "batal = false AND kodecabang = '" + $('#cabang').val() + "' AND lokasi = '" + kodegudang + "' AND gudang = false"
                    },
                },
                "columnDefs": [{
                        "targets": 2,
                        "render": $.fn.dataTable.render.moment('YYYY-MM-DD HH:mm:ss', 'D MMM YY')
                    },
                    {
                        "targets": 4,
                        "render": $.fn.dataTable.render.moment('YYYY-MM-DD HH:mm:ss', 'D MMM YY')
                    }
                ]
            });
        }, false);

        $(document).on('click', ".cetakadmin", function() {
            var noinvoice = $(this).attr("data-id");
            Reprint(noinvoice);
            GetDataSJ(noinvoice);
        });

        function Reprint(noinvoice) {
            // console.log(noinvoice);
            window.open("<?= base_url() ?>cetak_pdf/Invoice/Print/" + noinvoice);
        }

        function GetDataSJ(noinvoice) {
            $.ajax({
                url: "<?= base_url('sales/Invoice/ReprintSJ'); ?>",
                method: "POST",
                dataType: "json",
                async: true,
                data: {
                    noinvoice: noinvoice
                },
                success: function(data) {
                    console.log(data);
                    for (var i = 0; i < data.length; i++) {
                        window.open('<?= base_url() ?>cetak_pdf/SuratJalan/Print/' + data[i].nomor, '_blank');
                        // console.log(data[i].filepdf);
                    }
                }
            }, false);
        };

        document.getElementById("clear").addEventListener("click", function(event) {
            event.preventDefault();
            ClearScreen();
        })

        ClearScreen();
    });
</script>