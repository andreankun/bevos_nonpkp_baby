<div class="col-xl-12 mb-2">
	<div class="modal-header" style="padding: 5px;">
		<div class="col-xl-12">
			<div class="row">
				<div class="col-md-5">
					<h2>
						<span class="logo-menu"><?= $icons ?></span>
						<span class="text-uppercase"><?= $title ?></span>
					</h2>
				</div>
				<div class="col-md-7">
					<div class="form-group" style="float: right;">
						<button data-toggle="modal" data-target="#findinvbatal" id="invbatal" class="btn btn-sm btn-dark mb-1 mt-1 mr-1 ml-1" style="margin-bottom: 0px;"><i class="far fa-file-search" style="margin-right: 10px;"></i>No Invoice Batal</button>
						<button id="save" class="btn btn-sm btn-dark mb-1 mt-1 mr-1 ml-1" style="margin-bottom: 0px;"><i class="far fa-save" style="margin-right: 10px;"></i>Save</button>
						<!-- <button id="update" class="btn btn-sm btn-dark mb-1 mt-1 mr-1 ml-1" style="margin-bottom: 0px;"><i class="far fa-money-check-edit" style="margin-right: 10px;"></i>Update</button> -->
						<button data-toggle="modal" data-target="#findinvoice" id="find" class="btn btn-sm btn-dark mb-1 mt-1 mr-1 ml-1" style="margin-bottom: 0px;"><i class="far fa-file-search" style="margin-right: 10px;"></i>Find</button>
						<button id="cancel" class="btn btn-sm btn-danger m-1" data-toggle="modal" data-target="#modalcancel"><i class="far fa-times-circle" style="margin-right: 10px;"></i>Cancel</button>
						<button id="clear" class="btn btn-sm btn-dark mb-1 mt-1 mr-1 ml-1" style="margin-bottom: 0px;"><i class="far fa-refresh" style="margin-right: 10px;"></i>Clear</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="col-md-12 table-responsive hidescroll" style="height: 555px; overflow-y: scroll;">
	<div class="row">
		<div class="col-12 col-md-12 col-lg-12 col-xl-6">
			<div class="modal-content mb-2">
				<div class="modal-header p-2">
					<h5 class="mb-0">DATA DOKUMEN</h5>
				</div>
				<div class="modal-body">
					<div class="row mb-2 mt-2">
						<div class="col-sm-6 input-group">
							<div class="col-sm-6 p-0 input-group-prepend">
								<span class="col-sm-12 input-group-text">Nomor Invoice</span>
							</div>
							<input class="form-control" type="text" name="noinvoice" id="noinvoice" maxlength="50" placeholder="Nomor Invoice" readonly required />
							<input class="form-control" type="hidden" name="noinvoice" id="rekeningtf" maxlength="50" placeholder="Nomor Invoice" readonly required />
							<input class="form-control" type="hidden" name="noinvoice" id="nogb" maxlength="50" placeholder="Nomor Invoice" readonly required />
							<div class="input-group-text btn-dark" style="background-color:#ff0000;" id="clearinvbatal">
								<span class="input-group-addon">
									<i class="far fa-window-close text-white"></i>
								</span>
							</div>
							<!-- <input class="form-control" type="hidden" name="namagudang" id="namagudang" maxlength="50" value="<?php echo $this->session->userdata('mygudang') ?>" readonly required /> -->
						</div>
						<div class="col-sm-6 input-group box">
							<div class="col-sm-6 p-0 input-group-prepend">
								<span class="col-sm-12 input-group-text">Tanggal Invoice</span>
							</div>
							<input class="form-control" type="text" name="tanggalinv" id="tanggalinv" maxlength="50" value="<?= date('Y-m-d') ?>" readonly required />
							<input class="form-control" type="hidden" name="cabang" id="cabang" maxlength="50" value="<?php echo $this->session->userdata('mycabang') ?>" readonly required />
							<div class="input-group-text btn-dark">
								<span class="input-group-addon">
									<i class="fa fa-calendar-alt text-white"></i>
								</span>
							</div>
						</div>
					</div>
					<div class="row mb-2 nosj">
						<div class="col-sm-6 input-group">
							<div class="col-sm-6 p-0 input-group-prepend">
								<span class="col-sm-12 input-group-text">Nomor Surat Jalan</span>
							</div>
							<input class="form-control" type="text" name="nosj" id="nosj" maxlength="50" placeholder="Nomor Surat Jalan" readonly required />
						</div>
						<div class="col-sm-6 input-group box">
							<div class="col-sm-6 p-0 input-group-prepend">
								<span class="col-sm-12 input-group-text">Tanggal Surat Jalan</span>
							</div>
							<input class="form-control" type="text" name="tanggalsj" id="tanggalsj" maxlength="50" value="<?= date('Y-m-d') ?>" readonly required />
							<div class="input-group-text btn-dark">
								<span class="input-group-addon">
									<i class="fa fa-calendar-alt text-white"></i>
								</span>
							</div>
						</div>
					</div>
					<div class="row mb-2">
						<div class="col-sm-6 input-group">
							<div class="col-sm-6 p-0 input-group-prepend">
								<span class="col-sm-12 input-group-text">Nomor Packing</span>
							</div>
							<input class="form-control" type="text" name="nopacking" id="nopacking" maxlength="50" placeholder="Nomor Packing" required readonly />
							<div class="input-group-text btn-dark" data-toggle="modal" data-target="#findpacking" id="caripacking">
								<span class="input-group-addon">
									<i class="fa fa-search text-white"></i>
								</span>
							</div>
						</div>
						<div class="col-sm-6 input-group box date" id="tgljatuhtempo">
							<div class="col-sm-6 p-0 input-group-prepend">
								<span class="col-sm-12 input-group-text">Tanggal Jth. Tempo</span>
							</div>
							<input class="form-control" type="text" name="tgljthtempo" id="tgljthtempo" maxlength="50" value="<?= date('Y-m-d') ?>" readonly required />
							<div class="input-group-text btn-dark">
								<span class="input-group-addon">
									<i class="fa fa-calendar-alt text-white"></i>
								</span>
							</div>
						</div>
					</div>
					<div class="row mb-2 mt-2">
						<div class="col-sm-6 input-group">
							<div class="col-sm-6 p-0 input-group-prepend">
								<span class="col-sm-12 input-group-text">Nomor SO</span>
							</div>
							<input class="form-control" type="text" name="nomorso" id="nomorso" maxlength="50" placeholder="Nomor SO" readonly required />
						</div>
						<div class="col-sm-6 input-group box">
							<div class="col-sm-6 p-0 input-group-prepend">
								<span class="col-sm-12 input-group-text">Tanggal SO</span>
							</div>
							<input class="form-control" type="text" name="tanggalso" id="tanggalso" maxlength="50" value="<?= date('Y-m-d') ?>" readonly required />
							<div class="input-group-text btn-dark">
								<span class="input-group-addon">
									<i class="fa fa-calendar-alt text-white"></i>
								</span>
							</div>
						</div>
					</div>
					<div class="row mb-2">
						<div class="col-sm-6 input-group">
							<div class="col-sm-6 p-0 input-group-prepend">
								<span class="col-sm-12 input-group-text">Nomor Pelanggan</span>
							</div>
							<input class="form-control" type="text" name="nomorpelanggan" id="nomorpelanggan" maxlength="50" placeholder="Nomor Pelanggan" readonly required />
							<!-- <div class="input-group-text btn-dark" data-toggle="modal" data-target="#findpelanggan" id="caripelanggan">
								<span class="input-group-addon">
									<i class="fa fa-search text-white"></i>
								</span>
							</div> -->
						</div>
						<div class="col-sm-6 input-group">
							<div class="col-sm-6 p-0 input-group-prepend">
								<span class="col-sm-12 input-group-text">Nama Pelanggan</span>
							</div>
							<input class="form-control" type="text" name="namapelanggan" id="namapelanggan" maxlength="50" placeholder="Nama Pelanggan" readonly required />
						</div>
					</div>
					<div class="row mb-2">
						<div class="col-sm-6 input-group">
							<div class="col-sm-6 p-0 input-group-prepend">
								<span class="col-sm-12 input-group-text">Nomor Faktur Pajak</span>
							</div>
							<input class="form-control" type="text" name="nomorfaktur" id="nomorfaktur" maxlength="50" placeholder="Nomor Faktur" required />
							<!-- <div class="input-group-text btn-dark" data-toggle="modal" data-target="#findfaktur" id="carifaktur">
								<span class="input-group-addon">
									<i class="fa fa-search text-white"></i>
								</span>
							</div> -->
						</div>
						<div class="col-sm-6 input-group box">
							<div class="col-sm-6 p-0 input-group-prepend">
								<span class="col-sm-12 input-group-text">Lokasi Cetak</span>
							</div>
							<select class="form-control" style="height: 29px; padding: 0.175rem 0.50rem;" id="lokasi">
								<option value="">Pilih Gudang</option>
							</select>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-12 col-md-12 col-lg-12 col-xl-12">
			<div class="modal-content mb-2">
				<div class="modal-header p-2">
					<h5 class="mb-0">DATA DETAIL DOKUMEN</h5>
				</div>
				<div class="modal-body">
					<div class="row my-3">
						<div class="table-responsive">
							<table id="t_detail" class="table table-bordered table-striped table-default mb-0" style="width:100%;">
								<thead style="background-color: #68478D; color: #eee;">
									<tr style="line-height: 0.5 cm; ">
										<th style="text-align: center; ">
											Kode
										</th>
										<th style="text-align: center; ">
											Nama
										</th>
										<th style="text-align: center; ">
											Qty
										</th>
										<th style="text-align: center; ">
											Harga
										</th>
										<th style="display: none;">
											Disc
										</th>
										<th style="display: none;">
											DiscItem
										</th>
										<th style="display: none;">
											DiscPaket
										</th>
										<th style="display: none;">
											DiscPaketItem
										</th>
										<th style="text-align: center; ">
											Subtotal
										</th>
										<th style="text-align: center; ">
											Action
										</th>
									</tr>
								</thead>
								<tbody class="tbody-scroll scroller" id="tb_detail"></tbody>
							</table>
						</div>
					</div>
				</div>
				<div class="modal-body">
					<div class="row mb-2">
						<div class="col-sm-6 input-group">
							<div class="col-sm-6 p-0 input-group-prepend">
								<span class="col-sm-12 input-group-text">Total</span>
							</div>
							<input class="form-control" type="text" name="total" id="total" maxlength="50" value="0" required readonly style="text-align: right;" />
						</div>
					</div>
					<div class="row mb-2">
						<div class="col-sm-6 input-group">
							<div class="col-sm-6 p-0 input-group-prepend">
								<span class="col-sm-12 input-group-text">Nilai Cash</span>
							</div>
							<input class="form-control" type="text" name="nilaicash" id="nilaicash" maxlength="50" value="0" required readonly style="text-align: right;" />
						</div>
						<div class="col-sm-6 input-group">
							<div class="col-sm-6 p-0 input-group-prepend">
								<span class="col-sm-12 input-group-text">Disc. Cash</span>
							</div>
							<input class="form-control" type="text" name="discash" id="discash" maxlength="50" value="0" required readonly style="text-align: right;" />
						</div>
					</div>
					<div class="row mb-2">
						<div class="col-sm-6 input-group">
							<div class="col-sm-6 p-0 input-group-prepend">
								<span class="col-sm-12 input-group-text">Nilai Customer</span>
							</div>
							<input class="form-control" type="text" name="nilaicustomer" id="nilaicustomer" maxlength="50" value="0" required readonly style="text-align: right;" />
						</div>
						<div class="col-sm-6 input-group">
							<div class="col-sm-6 p-0 input-group-prepend">
								<span class="col-sm-12 input-group-text">Disc. Customer</span>
							</div>
							<input class="form-control" type="text" name="discustomer" id="discustomer" maxlength="50" value="0" required readonly style="text-align: right;" />
						</div>
					</div>
					<div class="row mb-2 mt-2">
						<div class="col-sm-6 input-group">
							<div class="col-sm-6 p-0 input-group-prepend">
								<span class="col-sm-12 input-group-text">DPP</span>
							</div>
							<input class="form-control" type="text" name="dpp" id="dpp" maxlength="50" value="0" readonly style="text-align: right;" required />
						</div>
					</div>
					<div class="row mb-2">
						<div class="col-sm-6 input-group">
							<div class="col-sm-6 p-0 input-group-prepend">
								<span class="col-sm-12 input-group-text">PPN</span>
							</div>
							<input class="form-control" type="text" name="ppn" id="ppn" maxlength="50" value="0" readonly style="text-align: right;" required />
						</div>
					</div>
					<div class="row mb-2">
						<div class="col-sm-6 input-group">
							<div class="col-sm-6 p-0 input-group-prepend">
								<span class="col-sm-12 input-group-text">Grand Total</span>
							</div>
							<input class="form-control" type="text" name="grandtotal" id="grandtotal" maxlength="50" value="0" readonly style="text-align: right;" required />
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="findbarang">
	<div class="modal-dialog modal-lg">
		<div class="modal-content" style="border-radius: 5px;">
			<div class="modal-header" style="padding: 10px; color: #000;">
				<h7 class="modal-title" style="margin-left: 5px;">Data Barang</h7>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="table-responsive">
					<table id="t_barang" class="table table-bordered table-striped" style="width:100%">
						<thead class="thead-dark">
							<tr>
								<th width="10">Action</th>
								<th width="150">Kode</th>
								<th width="150">Nama</th>
							</tr>
						</thead>
						<tbody></tbody>

					</table>

				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="findsuratjalan">
	<div class="modal-dialog modal-lg">
		<div class="modal-content" style="border-radius: 5px;">
			<div class="modal-header" style="padding: 10px; color: #000;">
				<h7 class="modal-title" style="margin-left: 5px;">Data Surat Jalan</h7>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="table-responsive">
					<table id="t_suratjalan" class="table table-bordered table-striped" style="width:100%">
						<thead class="thead-dark">
							<tr>
								<th width="10">Action</th>
								<th width="150">No. Surat Jalan</th>
								<th width="150">Tgl. Surat Jalan</th>
								<th width="150">No. Pelanggan</th>
								<th width="150">No. Packing</th>
								<th width="150">Lokasi Gudang</th>
							</tr>
						</thead>
						<tbody></tbody>

					</table>

				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="findpelanggan">
	<div class="modal-dialog modal-lg">
		<div class="modal-content" style="border-radius: 5px;">
			<div class="modal-header" style="padding: 10px; color: #000;">
				<h7 class="modal-title" style="margin-left: 5px;">Data Pelanggan</h7>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="table-responsive">
					<table id="t_pelanggan" class="table table-bordered table-striped" style="width:100%">
						<thead class="thead-dark">
							<tr>
								<th width="10">Action</th>
								<th width="150">Kode</th>
								<th width="150">Nama</th>
								<th width="150">Alamat</th>
								<th width="150">No. HP</th>
								<th width="150">No. Telp</th>
								<th width="150">E-mail</th>
								<th width="150">Status Pelanggan</th>
							</tr>
						</thead>
						<tbody></tbody>

					</table>

				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="findinvoice">
	<div class="modal-dialog modal-lg">
		<div class="modal-content" style="border-radius: 5px;">
			<div class="modal-header" style="padding: 10px; color: #000;">
				<h7 class="modal-title" style="margin-left: 5px;">Data Faktur Jual</h7>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="table-responsive">
					<table id="t_invoice" class="table table-bordered table-striped nowrap" style="width:100%">
						<thead class="thead-dark">
							<tr>
								<th width="10">Action</th>
								<th width="150">Nomor Invoice</th>
								<th width="150">Tanggal Invoice</th>
								<th width="150">Nomor SO</th>
								<th width="150">Tgl Jth. Tempo</th>
								<th width="150">Nama Pelanggan</th>
							</tr>
						</thead>
						<tbody></tbody>
					</table>

				</div>
			</div>
			<div class="modal-footer p-2">
				<button type="button" class="btn btn-danger mb-0" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="findreprintinvoice">
	<div class="modal-dialog modal-lg">
		<div class="modal-content" style="border-radius: 5px;">
			<div class="modal-header" style="padding: 10px; color: #000;">
				<h7 class="modal-title" style="margin-left: 5px;">Data Faktur Jual</h7>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="table-responsive">
					<table id="t_reprintinvoice" class="table table-bordered table-striped nowrap" style="width:100%">
						<thead class="thead-dark">
							<tr>
								<!-- <th width="10">Action</th> -->
								<th width="150">Re-Print</th>
								<th width="150">Nomor Invoice</th>
								<th width="150">Tanggal Invoice</th>
								<th width="150">Nomor SO</th>
								<th width="150">Tgl Jth. Tempo</th>
								<th width="150">Nama Pelanggan</th>
							</tr>
						</thead>
						<tbody></tbody>
					</table>

				</div>
			</div>
			<div class="modal-footer p-2">
				<button type="button" class="btn btn-danger mb-0" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="findinvbatal">
	<div class="modal-dialog modal-lg">
		<div class="modal-content" style="border-radius: 5px;">
			<div class="modal-header" style="padding: 10px; color: #000;">
				<h7 class="modal-title" style="margin-left: 5px;">Data Faktur Jual Batal</h7>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="table-responsive">
					<table id="t_invoicebatal" class="table table-bordered table-striped nowrap" style="width:100%">
						<thead class="thead-dark">
							<tr>
								<th width="10">Action</th>
								<th width="150">Nomor Invoice</th>
								<th width="150">No Surat Jalan</th>
								<th width="150">Tanggal</th>
								<th width="150">Ket. Batal</th>
							</tr>
						</thead>
						<tbody></tbody>
					</table>

				</div>
			</div>
			<div class="modal-footer p-2">
				<button type="button" class="btn btn-danger mb-0" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>


<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="printinvoice">
	<div class="modal-dialog modal-lg">
		<div class="modal-content" style="border-radius: 5px;">
			<div class="modal-header" style="padding: 10px; color: #000;">
				<h7 class="modal-title" style="margin-left: 5px;">Data Faktur Jual</h7>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="table-responsive">
					<table id="t_invoice" class="table table-bordered table-striped" style="width:100%">
						<thead class="thead-dark">
							<tr>
								<th width="10">Action</th>
								<th width="150">Nomor Invoice</th>
								<th width="150">Tanggal Invoice</th>
								<th width="150">Nomor Surat Jalan</th>
								<th width="150">Tgl Jth. Tempo</th>
								<th width="150">Nama Pelanggan</th>
							</tr>
						</thead>
						<tbody></tbody>
					</table>

				</div>
			</div>
			<div class="modal-footer p-2">
				<button type="button" class="btn btn-danger mb-0" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="findpacking">
	<div class="modal-dialog modal-lg">
		<div class="modal-content" style="border-radius: 5px;">
			<div class="modal-header" style="padding: 10px; color: #000;">
				<h7 class="modal-title" style="margin-left: 5px;">Data Packing Barang</h7>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="table-responsive">
					<table id="t_packing" class="table table-bordered table-striped dt-responsive display nowrap" style="width:100%">
						<thead class="thead-dark">
							<tr>
								<th width="10">Action</th>
								<th width="150">Nomor Packing</th>
								<th width="150">Tanggal Packing</th>
								<th width="150">Nomor SO</th>
								<th width="150">Tanggal SO</th>
								<th width="150">Nama Toko</th>
								<th width="150">Status Surat Jalan</th>
							</tr>
						</thead>
						<tbody></tbody>

					</table>

				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>


<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="changeqty">
	<div class="modal-dialog modal-md">
		<div class="modal-content" style="border-radius: 5px;">
			<div class="modal-header" style="padding: 10px; color: #000;">
				<h7 class="modal-title" style="margin-left: 5px;">UBAH DETAIL</h7>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="row mb-2">
					<div class="col-sm-6 input-group">
						<div class="col-sm-6 p-0 input-group-prepend">
							<span class="col-sm-12 input-group-text">Kode</span>
						</div>
						<input class="form-control" type="text" name="chgkode" id="chgkode" maxlength="50" placeholder="Kode" readonly required />
					</div>
					<div class="col-sm-6 input-group box">
						<div class="col-sm-6 p-0 input-group-prepend">
							<span class="col-sm-12 input-group-text">Nama</span>
						</div>
						<input class="form-control" type="text" name="chgnama" id="chgnama" maxlength="50" placeholder="Nama" required readonly />
					</div>
				</div>
				<div class="row mb-2">
					<div class="col-sm-6 input-group">
						<div class="col-sm-6 p-0 input-group-prepend">
							<span class="col-sm-6 input-group-text">Qty</span>
						</div>
						<input class="form-control" type="text" name="chgqty" id="chgqty" maxlength="50" placeholder="Qty" required readonly />
					</div>
					<div class="col-sm-6 input-group">
						<div class="col-sm-6 p-0 input-group-prepend">
							<span class="col-sm-6 input-group-text">Harga</span>
						</div>
						<input class="form-control" type="text" name="chgharga" id="chgharga" maxlength="50" placeholder="Harga" required readonly />
					</div>
				</div>
				<div class="row mb-2">
					<div class="col-sm-6 input-group">
						<div class="col-sm-6 p-0 input-group-prepend">
							<span class="col-sm-6 input-group-text">Diskon</span>
						</div>
						<input class="form-control" type="text" name="chgdisc" id="chgdisc" maxlength="50" placeholder="Diskon" required />
					</div>
					<div class="col-sm-6 input-group">
						<div class="col-sm-6 p-0 input-group-prepend">
							<span class="col-sm-6 input-group-text">Disc. /Item</span>
						</div>
						<input class="form-control" type="text" name="chgdiscitem" id="chgdiscitem" maxlength="50" placeholder="Disc. /Item" required readonly />
					</div>
				</div>
				<div class="row mb-2">
					<div class="col-sm-6 input-group">
						<div class="col-sm-6 p-0 input-group-prepend">
							<span class="col-sm-6 input-group-text">Disc. Paket</span>
						</div>
						<input class="form-control" type="text" name="chgdiscpaket" id="chgdiscpaket" maxlength="50" placeholder="Disc. Paket" required />
					</div>
					<div class="col-sm-6 input-group">
						<div class="col-sm-6 p-0 input-group-prepend">
							<span class="col-sm-6 input-group-text">Disc. Paket/Item</span>
						</div>
						<input class="form-control" type="text" name="chgdiscpaketitem" id="chgdiscpaketitem" maxlength="50" placeholder="Disc. Paket/Item" required readonly />
					</div>
				</div>
				<div class="row mb-2">
					<div class="col-sm-6 input-group">
						<div class="col-sm-6 p-0 input-group-prepend">
							<span class="col-sm-6 input-group-text">Subtotal</span>
						</div>
						<input class="form-control" type="text" name="chgsubtotal" id="chgsubtotal" maxlength="50" placeholder="Subtotal" required readonly />
						<div class="input-group-text btn-dark" id="changedetail" data-dismiss="modal">
							<span class="input-group-addon">
								<i class="fa fa-random" style="color: #fff;"></i>
							</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="modalcancel">
	<div class="modal-dialog modal-md">
		<div class="modal-content" style="border-radius: 5px;">
			<div class="modal-header" style="padding: 10px;">
				<h5 class="modal-title" style="margin-left: 5px; color: #f44336;"><i class="far fa-info-circle mr-1"></i>CANCEL DOKUMEN</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<center>
					<h5 class="modal-title"></i>Apakah Anda Yakin ?</h5>
				</center>
				<div class="row mb-2">
					<div class="col-sm-12 input-group box">
						<!-- <input class="col-sm-12 form-control" type="hidden" name="cancelnopr" id="cancelnopr" maxlength="50" placeholder="Nomor PR" readonly required/> -->
					</div>
				</div>
				<div class="row mb-2">
					<div class="col-sm-12 input-group box">
						<div class="col-sm-3 p-0 input-group-prepend">
							<span class="col-sm-12 input-group-text">Alasan Batal</span>
						</div>
						<textarea name="keteranganbatal" id="keteranganbatal" class="form-control" placeholder="Alasan Batal ..."></textarea>
					</div>
				</div>
			</div>
			<div class="modal-footer p-2">
				<button id="okcancel" type="button" class="btn btn-dark mb-0" data-dismiss="modal"><i class="far fa-check-circle mr-1"></i>Confirm</button>
			</div>
		</div>
	</div>
</div>