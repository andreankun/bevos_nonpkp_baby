<script type="text/javascript">
	$(document).ready(function() {
		var topcustomer = 0;

		$('#tgljatuhtempo').datepicker({
			format: "yyyy-mm-dd",
			autoclose: true,
			todayHighlight: true
		});

		$('#tanggalinv').datepicker({
			format: "yyyy-mm-dd",
			autoclose: true,
			todayHighlight: true
		});

		function LoadKonfigurasi() {
			$.ajax({
				url: "<?= base_url('sales/Invoice/LoadKonfigurasi') ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {},
				success: function(data) {
					for (var i = 0; i < data.length; i++) {
						ppn = data[i].ppn;
						discCash = data[i].disccash;
					}
				}
			}, false);
		}

		function LoadGudang() {
			$.ajax({
				url: "<?= base_url('sales/Invoice/LoadGudang') ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					cabang: $('#cabang').val()
				},
				success: function(data) {
					for (var i = 0; i < data.length; i++) {
						$("#lokasi").append('<option value = ' + data[i].kode + ' >' + data[i].nama + '</option>');
					}
				}
			}, false);
		}

		function LoadRacikan() {
			$.ajax({
				url: "<?= base_url('sales/Invoice/LoadRacikan') ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					cabang: $('#cabang').val()
				},
				success: function(data) {
					for (var i = 0; i < data.length; i++) {
						$("#jenisracikan").append('<option value = ' + data[i].kode + ' >' + data[i].nama + '</option>');
					}
				}
			}, false);
		};

		function ClearScreen() {
			LoadKonfigurasi();
			$('#lokasi').empty();
			$("#lokasi").append('<option value = "" >Pilih Gudang</option>');
			$('#jenisracikan').empty();
			$("#jenisracikan").append('<option value = "" >Pilih Racikan</option>');
			topcustomer = 0;
			LoadGudang();
			LoadRacikan();
			$('#lokasi').prop('disabled', false);
			$('#lokasi').val("");
			$('#salesman').val("");
			$('#jenisjual').val("");
			$('#tgljatuhtempo').prop('disabled', false);
			$('#keteranganbatal').val("");
			$('#noinvoice').val("");
			$('#tanggalinv').val("<?= date('Y-m-d') ?>");
			$('#nosj').val("");
			$('.nosj').hide();
			$('#tanggalsj').val("<?= date('Y-m-d') ?>");
			$('#nopacking').val("");
			$('#tgljthtempo').val("<?= date('Y-m-d') ?>");
			$('#nomorso').val("");
			$('#tanggalso').val("<?= date('Y-m-d') ?>");
			$('#nomorpelanggan').val("");
			$('#namapelanggan').val("");
			$('#nomorfaktur').val("").prop('disabled', false);
			// $('#lokasi').prop("disabled", false).val("");
			$('#discash').val(0);
			$('#discustomer').val(0);
			$('#dismanual').val(0);
			$('#nilaicash').val(0);
			$('#nilaicustomer').val(0);
			$('#nilaidiscmanual').val(0);
			$('#total').val(0);
			$('#dpp').val(0);
			$('#ppn').val(0);
			$('#grandtotal').val(0);
			$('#kodebrg').val("");
			$('#namabrg').val("");
			$('#qty').val(0);
			$('#hargajual').val(0);
			$('#disc').val(0);
			$('#discitem').val(0);
			$('#discpaket').val(0);
			$('#discpaketitem').val(0);
			$('#totalprice').val(0);

			$('#tb_detail').empty();

			$('#save').prop('disabled', false);
			$('#invbatal').prop('disabled', false);


			$('#cancel').prop('disabled', true);
			// $('#update').prop('disabled', true);

			$('#caripacking').show();
			$('#caribarang').show();
			$('#adddetail').show();
			$('.editdetail').show();
			$('.deldetail').show();
			$('#clearinvbatal').hide();
			$('.nogb').hide();

			if ($('#grup').val() == 'HO1' || $('#grup').val() == 'HO2' || $('#grup').val() == 'SUPERADMIN') {
				$('#accreprint').prop('disabled', false);
				$('#invbatal').prop('disabled', false);
				$('#accreprint').show();
				$('#invbatal').show();

			} else {
				$('#accreprint').prop('disabled', true);
				$('#accreprint').hide();
				$('#invbatal').prop('disabled', false);
				$('#invbatal').hide();
			}

			// if ($('#grup').val() == 'ADM JKT') {
			// 	$('#find').show();
			// 	$('#clear').show();
			// 	$('#save').show();
			// 	$('#cancel').hide();
			// 	$('#caripacking').hide();
			// 	$('#update').hide();
			// 	$('#invbatal').hide();

			// } else {
			// 	$('#find').show();
			// 	$('#clear').show();
			// 	$('#save').show();
			// 	$('#cancel').show();
			// 	$('#caripacking').show();
			// 	$('#update').show();
			// 	$('#invbatal').show();
			// }

		}
		document.getElementById("clearinvbatal").addEventListener("click", function(event) {
			event.preventDefault();
			$('#clearinvbatal').hide();
			$('#invbatal').prop('disabled', false);
			$('#noinvoice').val("");
			$('#tanggalinv').val("<?= date('Y-m-d') ?>");

		});

		function ValidasiSave(datadetail) {
			// var nopacking = $('#nopacking').val();
			var tanggalinv = $('#tanggalinv').val();
			var tgljthtempo = $('#tgljthtempo').val();
			var nomorso = $('#nomorso').val();
			var tanggalso = $('#tanggalso').val();
			var nopelanggan = $('#nomorpelanggan').val();
			var namapelanggan = $('#namapelanggan').val();
			var lokasi = $('#lokasi').val();
			var nofaktur = $('#nomorfaktur').val();
			var discash = $('#discash').val();
			var discustomer = $('#discustomer').val();
			var total = $('#total').val();
			var dpp = $('#dpp').val();
			var ppn = $('#ppn').val();
			var grandtotal = $('#grandtotal').val();

			if (tanggalinv == '' || tanggalinv == 0) {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Tanggal Invoice tidak boleh kosong.',
					showCloseButton: true,
					width: 350,
				});
				$('#tanggalinv').focus();
				var result = false;
			} else if (tgljthtempo == '' || tgljthtempo == 0) {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Tanggal Jatuh Tempo tidak boleh kosong.',
					showCloseButton: true,
					width: 350,
				});
				$('#tgljthtempo').focus();
				var result = false;
			} else if (nomorso == '' || nomorso == 0) {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Nomor Sales Order tidak boleh kosong.',
					showCloseButton: true,
					width: 350,
				});
				$('#nomorso').focus();
				var result = false;
			} else if (tanggalso == '' || tanggalso == 0) {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Tanggal Sales Order tidak boleh kosong.',
					showCloseButton: true,
					width: 350,
				});
				$('#tanggalso').focus();
				var result = false;
			} else if (nopelanggan == '' || nopelanggan == 0) {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Nomor Pelanggan tidak boleh kosong.',
					showCloseButton: true,
					width: 350,
				});
				$('#nomorpelanggan').focus();
				var result = false;
			} else if (namapelanggan == '' || namapelanggan == 0) {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Nama Pelanggan tidak boleh kosong.',
					showCloseButton: true,
					width: 350,
				});
				$('#namapelanggan').focus();
				var result = false;
			} else if (lokasi == '' || lokasi == 0) {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Lokasi tidak boleh kosong.',
					showCloseButton: true,
					width: 350,
				});
				$('#lokasi').focus();
				var result = false;
				// } else if (nofaktur == '' || nofaktur == 0) {
				//     Swal.fire({
				//         title: 'Informasi',
				//         icon: 'info',
				//         html: 'Nomor Faktur Pajak tidak boleh kosong.',
				//         showCloseButton: true,
				//         width: 350,
				//     });
				//     $('#nomorfaktur').focus();
				//     var result = false;
			} else if (discash == '') {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Diskon Cash tidak boleh kosong.',
					showCloseButton: true,
					width: 350,
				});
				$('#discash').focus();
				var result = false;
			} else if (discustomer == '') {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Diskon Customer tidak boleh kosong.',
					showCloseButton: true,
					width: 350,
				});
				$('#discustomer').focus();
				var result = false;
			} else if (total == '') {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Total tidak boleh kosong.',
					showCloseButton: true,
					width: 350,
				});
				$('#total').focus();
				var result = false;
			} else if (dpp == '') {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'DPP tidak boleh kosong.',
					showCloseButton: true,
					width: 350,
				});
				$('#dpp').focus();
				var result = false;
			} else if (ppn == '') {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'PPN tidak boleh kosong.',
					showCloseButton: true,
					width: 350,
				});
				$('#ppn').focus();
				var result = false;
			} else if (grandtotal == '') {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Grand Total tidak boleh kosong.',
					showCloseButton: true,
					width: 350,
				});
				$('#grandtotal').focus();
				var result = false;
			} else if (datadetail.length == 0 || datadetail.length == '0') {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Data Detail tidak boleh kosong.',
					showCloseButton: true,
					width: 350,
				});
				$('#kodebrg').focus();
				var result = false;
			} else {
				var result = true;
			}
			return result;
		}

		/* Validasi Data Add Detail */
		function ValidasiAddDetail(kodebarang) {
			var table = document.getElementById('t_detail');

			var kodebarang = $('#kodebrg').val();
			var namabarang = $('#namabrg').val();
			var qty = $('#qty').val();
			var harga = $('#hargajual').val();
			var disc = $('#disc').val();
			var discitem = $('#discitem').val();
			var discpaket = $('#discpaket').val();
			var discpaketitem = $('#discpaketitem').val();
			var totalprice = $('#totalprice').val();

			if (kodebarang == '' || kodebarang == 0) {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Kode Barang tidak boleh kosong.',
					showCloseButton: true,
					width: 350,
				});
				$('#kodebarang').focus();
				return "gagal";
			} else if (namabarang == '' || namabarang == 0) {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Nama Barang tidak boleh kosong.',
					showCloseButton: true,
					width: 350,
				});
				$('#namabarang').focus();
				return "gagal";
			} else if (qty == '' || qty == 0) {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Qty tidak boleh kosong.',
					showCloseButton: true,
					width: 350,
				});
				$('#qty').focus();
				return "gagal";
			} else if (harga == '' || harga == 0) {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Harga tidak boleh kosong.',
					showCloseButton: true,
					width: 350,
				});
				$('#harga').focus();
				return "gagal";
			} else if (disc == '') {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Disc. tidak boleh kosong.',
					showCloseButton: true,
					width: 350,
				});
				$('#disc').focus();
				return "gagal";
			} else if (discitem == '') {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Disc. Item tidak boleh kosong.',
					showCloseButton: true,
					width: 350,
				});
				$('#discitem').focus();
				return "gagal";
			} else if (discpaket == '') {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Disc. Paket tidak boleh kosong.',
					showCloseButton: true,
					width: 350,
				});
				$('#discpaket').focus();
				return "gagal";
			} else if (discpaketitem == '') {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Disc. Paket Item tidak boleh kosong.',
					showCloseButton: true,
					width: 350,
				});
				$('#discpaketitem').focus();
				return "gagal";
			} else if (totalprice == '' || totalprice == 0) {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Disc. Paket Item tidak boleh kosong.',
					showCloseButton: true,
					width: 350,
				});
				$('#totalprice').focus();
				return "gagal";
			} else {
				for (var r = 1, n = table.rows.length; r < n; r++) {
					var string = "";
					for (var c = 0, m = table.rows[r].cells.length; c < m; c++) {
						if (c == 0) {
							if (table.rows[r].cells[c].innerHTML == kodebarang) {
								Swal.fire({
									title: 'Informasi',
									icon: 'info',
									html: 'Data ini sudah diinput.',
									showCloseButton: true,
									width: 350,
								});
								return "gagal";
							}
						}
					}
				}
			}
			return "sukses"
		}

		/*Format Date*/
		function FormatDate(input) {
			var date = new Date(input);
			var yyyy = date.getFullYear();
			var mm = date.getMonth() + 1;
			var dd = date.getDate();
			var hrs = date.getHours();
			var mnt = date.getMinutes();


			if (dd < 10) {
				dd = '0' + dd;
			}
			if (mm < 10) {
				mm = '0' + mm;
			}
			if (hrs < 10) {
				hrs = '0' + hrs;
			}
			if (mnt < 10) {
				mnt = '0' + mnt;
			}
			return yyyy + '-' + mm + '-' + dd;
		};

		/* Declare Toast */
		const Toast = Swal.mixin({
			toast: true,
			position: 'top-end',
			showConfirmButton: false,
			timer: 3000,
			timerProgressBar: true,
			didOpen: (toast) => {
				toast.addEventListener('mouseenter', Swal.stopTimer)
				toast.addEventListener('mouseleave', Swal.resumeTimer)
			}
		});

		/* Format Rupiah */
		function FormatRupiah(angka, prefix) {
			if (!angka) {
				return '';
			}
			var vangka = angka.toString();
			var number_string = vangka.replace(/[^.\d]/g, '').replace(/[^\w\s]/gi, '').toString(),
				split = number_string.split('.'),
				sisa = split[0].length % 3,
				rupiah = split[0].substr(0, sisa),
				ribuan = split[0].substr(sisa).match(/\d{3}/gi);

			// tambahkan titik jika yang di input sudah menjadi angka ribuan
			if (ribuan) {
				separator = sisa ? ',' : '';
				rupiah += separator + ribuan.join(',');
			}

			rupiah = split[1] != undefined ? rupiah + '.' + split[1] : rupiah;
			return prefix == undefined ? rupiah : (rupiah ? rupiah : '');
		};

		/* DeFormat Rupiah */
		function DeFormatRupiah(angka) {
			var result = angka.replace(/[^\w\s]/gi, '');

			return result;
		};

		$('#disc[max]:not([max=""])').on('input', function(event) {
			var $this = $(this);
			var maxlength = $this.attr('max').length;
			var value = $this.val();
			if (value && value.length >= maxlength) {
				$this.val(value.substr(0, maxlength));
			}
		});

		$('#discpaket[max]:not([max=""])').on('input', function(event) {
			var $this = $(this);
			var maxlength = $this.attr('max').length;
			var value = $this.val();
			if (value && value.length >= maxlength) {
				$this.val(value.substr(0, maxlength));
			}
		});

		$('#disc').keyup(function() {
			// (Harga - Disc Barang per Item - Disc Paket per Item) x qty   
			var disc = $(this).val();
			var harga = DeFormatRupiah($('#hargajual').val());
			var discitem = DeFormatRupiah($('#discitem').val());
			var discpaketitem = DeFormatRupiah($('#discpaketitem').val());
			var qty = DeFormatRupiah($('#qty').val());

			var discperitem = (parseFloat(harga) * (disc / 100));
			$('#discitem').val(FormatRupiah(Math.round(discperitem)));
			var subtotal = ((harga - discperitem - discpaketitem) * qty);
			$('#totalprice').val(FormatRupiah(subtotal));
		})

		$('#discpaket').keyup(function() {
			// (Harga - Disc Barang per Item - Disc Paket per Item) x qty
			var discpaket = $(this).val();
			var disc = $('#disc').val();
			var harga = DeFormatRupiah($('#hargajual').val());
			var discitem = DeFormatRupiah($('#discitem').val());
			var discpaketitem = DeFormatRupiah($('#discpaketitem').val());
			var qty = DeFormatRupiah($('#qty').val());

			var discperitem = (parseFloat(harga) * (disc / 100));
			$('#discitem').val(FormatRupiah(Math.round(discperitem)));
			var discpaketperitem = (parseFloat(harga) - (discperitem)) * (discpaket / 100);
			$('#discpaketitem').val(FormatRupiah(Math.round(discpaketperitem)));
			var subtotal = ((harga - discperitem - discpaketperitem) * qty);
			$('#totalprice').val(FormatRupiah(subtotal));
		})

		$('#chgdisc').keyup(function() {
			// (Harga - Disc Barang per Item - Disc Paket per Item) x qty   
			var disc = $(this).val();
			var harga = DeFormatRupiah($('#chgharga').val());
			var discitem = DeFormatRupiah($('#chgdiscitem').val());
			var discpaketitem = DeFormatRupiah($('#chgdiscpaketitem').val());
			var qty = DeFormatRupiah($('#chgqty').val());

			var discperitem = (parseFloat(harga) * (disc / 100));
			$('#chgdiscitem').val(FormatRupiah(Math.round(discperitem)));
			var subtotal = ((harga - discperitem - discpaketitem) * qty);
			$('#chgsubtotal').val(FormatRupiah(subtotal));
		})

		$('#chgdiscpaket').keyup(function() {
			// (Harga - Disc Barang per Item - Disc Paket per Item) x qty
			var discpaket = $(this).val();
			var disc = $('#chgdisc').val();
			var harga = DeFormatRupiah($('#chgharga').val());
			var discitem = DeFormatRupiah($('#chgdiscitem').val());
			var discpaketitem = DeFormatRupiah($('#chgdiscpaketitem').val());
			var qty = DeFormatRupiah($('#chgqty').val());

			var discperitem = (parseFloat(harga) * (disc / 100));
			$('#chgdiscitem').val(FormatRupiah(Math.round(discperitem)));
			var discpaketperitem = (parseFloat(harga) - (discperitem)) * (discpaket / 100);
			$('#chgdiscpaketitem').val(FormatRupiah(Math.round(discpaketperitem)));
			var subtotal = ((harga - discperitem - discpaketperitem) * qty);
			$('#chgsubtotal').val(FormatRupiah(subtotal));
		})

		function HitungSummary() {
			var table = document.getElementById('t_detail');
			var sublist = 0;
			var subtotal = 0;
			var discash = DeFormatRupiah($('#discash').val());
			var discustomer = DeFormatRupiah($('#discustomer').val());
			var ppntunai = DeFormatRupiah($('#ppntunai').val());

			for (var r = 1, n = table.rows.length; r < n; r++) {
				for (var c = 0, m = table.rows[r].cells.length; c < m - 1; c++) {
					sublist = parseFloat(DeFormatRupiah(table.rows[r].cells[8].innerHTML));
				}
				subtotal += parseFloat(sublist);
			}

			$('#total').val(FormatRupiah(subtotal));
			var dpp = subtotal - discash - discustomer;
			var ppn = parseFloat(dpp) * (ppntunai / 100);
			var grandtotal = parseFloat(dpp) + parseFloat(Math.floor(ppn));
			$('#dpp').val(FormatRupiah(dpp));
			$('#ppn').val(FormatRupiah(Math.floor(ppn)));
			$('#grandtotal').val(FormatRupiah(grandtotal));
		};


		/*Cari No Invoice Batal*/
		document.getElementById("invbatal").addEventListener("click", function(event) {
			event.preventDefault();
			var kodecabang = "<?= $this->session->userdata('mycabang') ?>";
			$('#t_invoicebatal').DataTable({
				"language": {
					"url": "<?= base_url() ?>assets/json/id.json"
				},
				"destroy": true,
				"searching": true,
				"processing": true,
				"serverSide": true,
				"lengthChange": true,
				"pageLength": 5,
				"lengthMenu": [5, 10, 25, 50],
				"order": [
					[1, 'desc']
				],
				"ajax": {
					"url": "<?= base_url('sales/Invoice/CariDataInvBatal'); ?>",
					"method": "POST",
					"data": {
						nmtb: "hist_cancel",
						field: {
							noglobal: "noglobal",
							noinvoice: "noinvoice",
							nosuratjalan: "nosuratjalan",
							tanggal: "tanggal",
							keteranganbatal: "keteranganbatal"
						},
						sort: "noinvoice",
						where: {
							noinvoice: "noinvoice",
							nosuratjalan: "nosuratjalan",
							keteranganbatal: "keteranganbatal"
						},
						// value: "statuspakai = false AND jenisdokumen = 'Invoice Penjualan' AND kodecabang = '" + $('#cabang').val() + "'"
						value: "statuspakai = false AND jenisdokumen = 'Invoice Penjualan' AND kodecabang = '" + kodecabang + "'"
					},
				},
				"columnDefs": [{
					"targets": 3,
					"render": $.fn.dataTable.render.moment('YYYY-MM-DD HH:mm:ss', 'D MMM YY'),
				}]
			});
		})

		$(document).on('click', ".searchinvbatal", function() {
			var nomor = $(this).attr("data-id");
			$.ajax({
				url: "<?= base_url('sales/Invoice/DataInvBatal'); ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					nomor: nomor
				},
				success: function(data) {
					// console.log(data);
					for (var i = 0; i < data.length; i++) {
						// $('#noinvoice').val(data[i].noinvoice.trim());
						$('#nogb').val(data[i].noglobal.trim());
						$('#tanggalinv').val(FormatDate(data[i].tanggal.trim()));
						$('#clearinvbatal').show();
						$('#invbatal').prop('disabled', true);
						$('.nogb').show();
					}
				}
			});
		});




		/*Cari Data Packing Barang*/
		document.getElementById("caripacking").addEventListener("click", function(event) {
			event.preventDefault();
			$('#tb_detail').empty();
			$('#t_packing').DataTable({
				"language": {
					"url": "<?= base_url() ?>assets/json/id.json"
				},
				"destroy": true,
				"searching": true,
				"processing": true,
				"serverSide": true,
				"lengthChange": true,
				"pageLength": 5,
				"lengthMenu": [5, 10, 25, 50],
				"order": [
					[1, 'desc']
				],
				"ajax": {
					"url": "<?= base_url('sales/Invoice/CariDataPacking'); ?>",
					"method": "POST",
					"data": {
						nmtb: "cari_packing",
						field: {
							nomor: "nomor",
							tanggal: "tanggal",
							nomorso: "nomorso",
							tglso: "tglso",
							namapelanggan: "namapelanggan",
							status: "status"
						},
						sort: "nomor",
						where: {
							nomor: "nomor",
							nomorso: "nomorso",
							namapelanggan: "namapelanggan"
						},
						value: "status = false AND kodecabang = '" + $('#cabang').val() + "' AND batal = false"
					},
				},
				"columnDefs": [{
					"targets": 2,
					"render": $.fn.dataTable.render.moment('YYYY-MM-DD HH:mm:ss', 'D MMM YY'),
				}, {
					"targets": 4,
					"render": $.fn.dataTable.render.moment('YYYY-MM-DD HH:mm:ss', 'D MMM YY'),
				}, {
					"targets": 6,
					"data": "status",
					"render": function(data, type, row, meta) {
						return (row[6] == 't') ? '<p>Sudah Cetak Surat Jalan</p>' : '<p>Belum Cetak Surat Jalan</p>';
					},
				}]
			});
		})

		$(document).on('click', ".searchpacking", function() {
			var nomor = $(this).attr("data-id");
			GetDataPacking(nomor);
		});

		function GetDataPacking(nomor) {
			$.ajax({
				url: "<?= base_url('sales/Invoice/DataPacking'); ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					nomor: nomor
				},
				success: function(data) {

					for (var i = 0; i < data.length; i++) {
						$('#nopacking').val(data[i].nopacking.trim());
						$('#tanggalpacking').val(FormatDate(data[i].tglpacking.trim()));
						$('#nomorso').val(data[i].nomorso.trim());
						$('#tanggalso').val(FormatDate(data[i].tglso.trim()));
						$('#nomorpelanggan').val(data[i].nopelanggan.trim());
						$('#namapelanggan').val(data[i].namapelanggan.trim());
						$('#salesman').val(data[i].namasalesman.trim());
						if (data[i].jenisjual == '1' || data[i].jenisjual == '4') {
							$('#jenisjual').val('TUNAI');
						} else if (data[i].jenisjual == '2') {
							$('#jenisjual').val('KREDIT');
						} else {
							$('#jenisjual').val('COD');
						}
						$('#discash').val(data[i].disccash.trim());
						$('#nilaicash').val(FormatRupiah(data[i].nilaicash.trim()));
						$('#discustomer').val(data[i].disccustomer.trim());
						$('#nilaicustomer').val(FormatRupiah(data[i].nilaicustomer.trim()));
						$('#total').val(FormatRupiah(data[i].total.trim()));
						$('#lokasi').val(data[i].lokasi.trim());
						$('#dpp').val(FormatRupiah(data[i].dpp.trim()));
						$('#ppn').val(FormatRupiah(data[i].ppn.trim()));
						$('#grandtotal').val(FormatRupiah(data[i].grandtotal.trim()));
						$('#rekeningtf').val(data[i].kodeprefix.trim());
						$('#koderekening').val(data[i].norek.trim());
						$('#norekening').val(data[i].norekening.trim());
						$('#namapemilik').val(data[i].namapemilik.trim());
						// $('#lokasi').val(data[i].lokasi.trim());
						topcustomer = Number(data[i].top.trim());
						date = new Date($('#tanggalinv').val());
						jenisjual = data[i].jenisjual.trim();
						// console.log(date);
						if (jenisjual == '1' || jenisjual == '3' || jenisjual == '4') {
							date.setDate(date.getDate());
						} else {
							date.setDate(date.getDate() + Number(data[i].top.trim()));
						}
						$('#tgljthtempo').val($.datepicker.formatDate('yy-mm-dd', date));
						// console.log(data);
						GetDataPckDetail(nomor);
					}
				}
			});
		};

		function myFunction(nomor, kodebarang, namabarang, qty, harga, disc, discitem, discpaket, discpaketitem, subtotal, action) {
			var table = document.getElementById('t_detail').getElementsByTagName('tbody')[0];
			var row = table.insertRow(0);
			var kodepencarian = kodebarang.replace(/[^a-zA-Z0-9 ]/g, '');
			row.setAttribute("id", kodepencarian);
			// console.log(row)
			var cell1 = row.insertCell(0);
			var cell2 = row.insertCell(1);
			var cell3 = row.insertCell(2);
			var cell4 = row.insertCell(3);
			var cell5 = row.insertCell(4);
			var cell6 = row.insertCell(5);
			var cell7 = row.insertCell(6);
			var cell8 = row.insertCell(7);
			var cell9 = row.insertCell(8);
			var cell10 = row.insertCell(9);
			var cell11 = row.insertCell(10);
			// var cell3 = '<button data-table="' + RejectSpecialChar(kodebarang) + '" class="deldetail btn btn-danger m-1 p-1"><i class="fa fa-trash mr-1"></i> Delete</button>';
			cell1.innerHTML = nomor;
			cell2.innerHTML = kodebarang;
			cell3.innerHTML = namabarang;
			cell4.innerHTML = FormatRupiah(qty);
			cell5.innerHTML = FormatRupiah(harga);
			cell6.innerHTML = disc;
			cell7.innerHTML = FormatRupiah(discitem);
			cell8.innerHTML = discpaket;
			cell9.innerHTML = FormatRupiah(discpaketitem);
			cell10.innerHTML = FormatRupiah(subtotal);
			cell11.innerHTML = action;
			// cell4.setAttribute("id", "qty" + kodepencarian);
			// cell5.setAttribute("id", "harga" + kodepencarian);
			// cell6.setAttribute("id", "disc" + kodepencarian);
			// cell7.setAttribute("id", "discitem" + kodepencarian);
			// cell8.setAttribute("id", "discpaket" + kodepencarian);
			// cell9.setAttribute("id", "discpaketitem" + kodepencarian);
			// cell10.setAttribute("id", "totalprice" + kodepencarian);
			// cell1.style.display = 'none';
			// row.innerHTML = kodebarang;

			var tbody = $('#t_detail').find('tbody');
			var newRows = tbody.find('tr').sort(function(a, b) {
				return $('td:eq(2)', a).text().localeCompare($('td:eq(2)', b).text());
			});
			console.log(newRows);
			tbody.append(newRows);
		}

		function GetDataPckDetail(nomor) {
			$.ajax({
				url: "<?= base_url('sales/Invoice/DataPckDetail'); ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					nomor: nomor
				},
				success: function(data) {
					console.log(data);
					for (var i = 0; i < data.length; i++) {
						var kodebarang = data[i].kodebarang.trim();
						var namabarang = data[i].namabarang.trim();
						var qty = data[i].qty.trim();
						var harga = data[i].harga.trim();
						var disc = data[i].discbarang.trim();
						var discitem = data[i].discbrgitem.trim();
						var discpaket = data[i].discpromo.trim();
						var discpaketitem = data[i].discpaketpromo.trim();
						var subtotal = data[i].subtotal.trim();
						var nomor = "";
						var action = "-";
						myFunction(nomor, kodebarang, namabarang, qty, harga, disc, discitem, discpaket, discpaketitem, subtotal, action);
					}
				}
			}, false);
		};

		/*Cari Data Barang*/
		// document.getElementById("caribarang").addEventListener("click", function(event) {
		//     event.preventDefault();

		//     var nomorso = $('#nomorso').val();
		//     $('#t_barang').DataTable({
		//         "destroy": true,
		//         "searching": true,
		//         "processing": true,
		//         "serverSide": true,
		//         "lengthChange": false,
		//         "pageLength": 5,
		//         "lengthMenu": [5, 25, 50, 100],
		//         "order": [],
		//         "ajax": {
		//             "url": "<?= base_url('sales/Invoice/CariDataPackingDetail'); ?>",
		//             "method": "POST",
		//             "data": {
		//                 nmtb: "srvt_sodetail",
		//                 field: {
		//                     kodebarang: "kodebarang",
		//                     namabarang: "namabarang"
		//                 },
		//                 sort: "kodebarang",
		//                 where: {
		//                     kodebarang: "kodebarang"
		//                 },
		//                 value: "nomor = '" + nomorso + "' "
		//             },
		//         }
		//     });
		// });

		$("#adddetail").click(function() {
			var kodebarang = $('#kodebrg').val();
			var namabarang = $('#namabrg').val();
			var qty = $('#qty').val();
			var harga = $('#hargajual').val();
			var disc = $('#disc').val();
			var discitem = $('#discitem').val();
			var discpaket = $('#discpaket').val();
			var discpaketitem = $('#discpaketitem').val();
			var totalprice = $('#totalprice').val();

			if (ValidasiAddDetail(kodebarang) == 'sukses') {
				var row = "";
				row =
					'<tr id="' + kodebarang + '">' +
					'<td>' + kodebarang + '</td>' +
					'<td>' + namabarang + '</td>' +
					'<td>' + qty + '</td>' +
					'<td>' + harga + '</td>' +
					'<td style="display: none;">' + disc + '</td>' +
					'<td style="display: none;">' + discitem + '</td>' +
					'<td style="display: none;">' + discpaket + '</td>' +
					'<td style="display: none;">' + discpaketitem + '</td>' +
					'<td>' + FormatRupiah(totalprice) + '</td>' +
					'<td>' +
					'<button data-table="' + kodebarang + '" data-toggle="modal" data-target="#changeqty" class="editdetail btn btn-sm btn-success mb-0 p-1"><i class="far fa-edit"></i> Edit</button>&nbsp;' +
					'<button data-table="' + kodebarang + '" class="deldetail btn btn-sm btn-danger mb-0 p-1"><i class="fa fa-trash"></i> Delete</button>' +
					'</td>' +
					'</tr>';
				$('#tb_detail').append(row);
				HitungSummary();
				$('#kodebrg').val("");
				$('#namabrg').val("");
				$('#qty').val(0);
				$('#hargajual').val(0);
				$('#disc').val(0);
				$('#discitem').val(0);
				$('#discpaket').val(0);
				$('#discpaketitem').val(0);
				$('#totalprice').val(0);
			}
		});

		$(document).on('click', '.deldetail', function() {
			var id = $(this).attr("data-table");
			$('#' + id).remove();
			HitungSummary();
		});

		$(document).on('click', '.editdetail', function() {
			var table = document.getElementById('t_detail');
			var tr = document.getElementById($(this).attr("data-table"));
			var td = tr.getElementsByTagName("td");
			for (var c = 0, m = table.rows[0].cells.length; c < m - 1; c++) {
				var chgth = table.rows[0].cells[c].innerHTML.replace(/ /g, '').replace(/\s/g, '').toLowerCase();
				var valuetd = td[c].innerHTML;
				$('#chg' + chgth).val(valuetd);
			}
		});

		$(document).on('click', '#changedetail', function() {
			// var kode = $('#chgkode').val(); 
			var kodebarang = $('#chgkode').val();
			var namabarang = $('#chgnama').val();
			var qty = $('#chgqty').val();
			var harga = $('#chgharga').val();
			var disc = $('#chgdisc').val();
			var discitem = $('#chgdiscitem').val();
			var discpaket = $('#chgdiscpaket').val();
			var discpaketitem = $('#chgdiscpaketitem').val();
			var subtotal = $('#chgsubtotal').val();
			$('#' + kodebarang).remove();
			InsertDataDetail(kodebarang, namabarang, qty, harga, disc, discitem, discpaket, discpaketitem, subtotal);
			HitungSummary();
		});

		function InsertDataDetail(kodebarang, namabarang, qty, harga, disc, discitem, discpaket, discpaketitem, subtotal) {

			var row = "";
			row =
				'<tr id="' + kodebarang + '">' +
				'<td nowrap>' + kodebarang + '</td>' +
				'<td nowrap>' + namabarang + '</td>' +
				'<td nowrap>' + FormatRupiah(qty) + '</td>' +
				'<td nowrap>' + FormatRupiah(harga) + '</td>' +
				'<td nowrap>' + disc + '</td>' +
				'<td nowrap>' + FormatRupiah(discitem) + '</td>' +
				'<td nowrap>' + discpaket + '</td>' +
				'<td nowrap>' + FormatRupiah(discpaketitem) + '</td>' +
				'<td nowrap >' + FormatRupiah(subtotal) + '</td>' +
				'<td nowrap style="text-align: center; width: 200px;">' +
				// '<button data-table="' + kodebarang + '" data-toggle="modal" data-target="#changeqty" class="editdetail btn btn-sm btn-success m-1 p-1"><i class="fas fa-edit"></i> Edit</button>' +
				// '<button data-table="' + kodebarang + '" class="deldetail btn btn-sm btn-danger m-1 p-1"><i class="fa fa-trash mr-1"></i> Delete</button>' +
				'</td>' +
				'</tr>';
			$('#tb_detail').append(row);
		};

		$(document).on('click', ".searchpackingdetail", function() {
			var nomor = $('#nopacking').val();
			var kodebarang = $(this).attr("data-id");
			GetDataPackingDetail(nomor, kodebarang);
		});

		function GetDataPackingDetail(nomor, kodebarang) {
			$.ajax({
				url: "<?= base_url('sales/Invoice/DataPackingDetail'); ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					nomor: nomor,
					kodebarang: kodebarang
				},
				success: function(data) {
					console.log(data);
					for (var i = 0; i < data.length; i++) {
						$('#kodebrg').val(data[i].kodebarang.trim());
						$('#namabrg').val(data[i].namabarang.trim());
						$('#qty').val(data[i].qtyso.trim());
						$('#hargajual').val(FormatRupiah(data[i].harga.trim()));
					}
				}
			}, false);
		};

		function AmbilDataDetail() {
			var table = document.getElementById('t_detail');
			var arr2 = [];
			for (var r = 1, n = table.rows.length; r < n; r++) {
				var string = "";
				for (var c = 0, m = table.rows[r].cells.length; c < m - 1; c++) {
					if (c == 0) {
						string = "{" + table.rows[0].cells[c].innerHTML + " : '" + table.rows[r].cells[c].innerHTML + "'";
					} else {
						string = string + ", " + table.rows[0].cells[c].innerHTML + " : '" + table.rows[r].cells[c].innerHTML + "'";
					}
				}
				string = string + "}";
				var obj = JSON.stringify(eval('(' + string + ')'));
				var arr = $.parseJSON(obj);
				arr2.push(arr);
			}
			return arr2;
		};

		document.getElementById("save").addEventListener("click", function(event) {
			// $('#save').hide();
			event.preventDefault();
			var noinvoice = $('#noinvoice').val();
			var nomorgb = $('#nogb').val();
			var tanggalinv = $('#tanggalinv').val();
			var nopacking = $('#nopacking').val();
			var tgljthtempo = $('#tgljthtempo').val();
			var nomorso = $('#nomorso').val();
			var tanggalso = $('#tanggalso').val();
			var nopelanggan = $('#nomorpelanggan').val();
			var namapelanggan = $('#namapelanggan').val();
			var lokasi = $('#lokasi').val();
			var nofaktur = $('#nomorfaktur').val();
			var discash = $('#discash').val();
			var discustomer = $('#discustomer').val();
			var nilaicash = $('#nilaicash').val();
			var nilaicustomer = $('#nilaicustomer').val();
			var total = $('#total').val();
			var dpp = $('#dpp').val();
			var ppn = $('#ppn').val();
			var grandtotal = $('#grandtotal').val();
			var rekeningtf = $('#rekeningtf').val();

			var datadetail = AmbilDataDetail();

			if (ValidasiSave(datadetail) == true) {
				// $('#save').hide();
				$.ajax({
					url: "<?= base_url("sales/Invoice/Save") ?>",
					method: "POST",
					dataType: "json",
					async: true,
					data: {
						noinvoice: noinvoice,
						nomorgb: nomorgb,
						tanggalinv: tanggalinv,
						nopacking: nopacking,
						tgljthtempo: tgljthtempo,
						nomorso: nomorso,
						tanggalso: tanggalso,
						nopelanggan: nopelanggan,
						namapelanggan: namapelanggan,
						nofaktur: nofaktur,
						lokasi: lokasi,
						discash: discash,
						discustomer: discustomer,
						nilaicash: nilaicash,
						nilaicustomer: nilaicustomer,
						total: total,
						dpp: dpp,
						ppn: ppn,
						grandtotal: grandtotal,
						datadetail: datadetail,
						rekeningtf: rekeningtf
					},
					success: function(data) {
						// $('#save').show();
						if (data.nomor != "" && data.nomorsj != "" && data.lokasi != "") {
							// Toast.fire({
							//     icon: 'success',
							//     title: data.message
							// });
							$('#noinvoice').val(data.nomor);
							$('#nosj').val(data.nomorsj);
							$('nomorgb').val(data.nomorgb);
							$('#tanggalinv').prop("disabled", true);
							$('#nopacking').prop("disabled", true);
							$('#tgljthtempo').prop("disabled", true);
							$('#nomorso').prop("disabled", true);
							$('#tanggalso').prop("disabled", true);
							$('#nomorpelanggan').prop("disabled", true);
							$('#namapelanggan').prop("disabled", true);
							$('#nomorfaktur').prop("disabled", true);
							$('#lokasi').prop("disabled", true);
							$('#discash').prop("disabled", true);
							$('#discustomer').prop("disabled", true);
							$('#total').prop("disabled", true);
							$('#dpp').prop("disabled", true);
							$('#ppn').prop("disabled", true);
							$('#grandtotal').prop("disabled", true);

							// $('#update').prop("disabled", true);
							$('#save').prop("disabled", true).show();
							$('#invbatal').prop("disabled", true);
							$('.nogb').show();

							$('#caripacking').hide();
							$('#caribarang').hide();
							$('#adddetail').hide();
							$('.editdetail').hide();
							$('.deldetail').hide();
							Swal.fire({
								icon: 'success',
								title: data.message,
								text: data.nomor
							})

							$('.nosj').show();
							if (data.lokasi == $('#lokasi').val()) {
								CetakInvoice(data.nomorgb);
								CetakSuratJalan(data.nomorsj, data.nomorgb);
							}
						} else {
							// Toast.fire({
							//     icon: 'error',
							//     title: data.message
							// });
							$('#save').prop("disabled", false).show();
							Swal.fire({
								icon: 'error',
								title: data.message,
								// text: data.nomor
							})
						}
					}
				}, false)
			}
		})

		function CetakInvoice(nomorgb) {
			// console.log(nomor);
			window.open("<?= base_url() ?>cetak_pdf/Invoice/Print/" + nomorgb);
		}

		function CetakSuratJalan(nomorsj, nomorgb) {
			// console.log(nomor);
			window.open("<?= base_url() ?>cetak_pdf/SuratJalan/Print/" + nomorsj + ":" + nomorgb);
		}

		document.getElementById("find").addEventListener("click", function(event) {
			event.preventDefault();
			$('#tb_detail').empty();
			$('#t_invoice').DataTable({
				"language": {
					"url": "<?= base_url() ?>assets/json/id.json"
				},
				"destroy": true,
				"searching": true,
				"processing": true,
				"serverSide": true,
				"lengthChange": true,
				"pageLength": 5,
				"lengthMenu": [5, 10, 25, 50],
				"order": [],
				"ajax": {
					"url": "<?= base_url('sales/Invoice/CariDataInvoice'); ?>",
					"method": "POST",
					"data": {
						nmtb: "cari_invoicegb",
						field: {
							nomorgb: "nomorgb",
							nomor: "nomor",
							tanggal: "tanggal",
							nomorso: "nomorso",
							nomorsj: "nomorsj",
							tgljthtempo: "tgljthtempo",
							namapelanggan: "namapelanggan",
							grandtotal: "grandtotal",
							pemakaiso: "pemakaiso",
							tglsimpanso: "tglsimpanso"
						},
						sort: "nomorsj",
						where: {
							nomor: "nomor",
							nomorso: "nomorso",
							namapelanggan: "namapelanggan"
						},
						value: "batal = false AND kodecabang = '" + $('#cabang').val() + "'"
					},
				},
				"columnDefs": [{
						"targets": 3,
						"render": $.fn.dataTable.render.moment('YYYY-MM-DD HH:mm:ss', 'D MMM YY')
					},
					{
						"targets": 6,
						"render": $.fn.dataTable.render.moment('YYYY-MM-DD HH:mm:ss', 'D MMM YY')
					}, {
						"targets": 10,
						"render": $.fn.dataTable.render.moment('YYYY-MM-DD HH:mm:ss', 'D MMM YYYY HH:mm')
					}, {
						"targets": 8,
						"render": $.fn.dataTable.render.number(',', '.')
					}
				]
			});
		})

		$(document).on('click', ".searchinvoice", function() {
			var nomor = $(this).attr("data-id");
			// var kodecabang = "<?= $this->session->userdata('mycabang') ?>";
			GetData(nomor);
			$('#caripacking').hide();
		});

		function GetData(nomor) {
			$.ajax({
				url: "<?= base_url('sales/Invoice/DataInvoice'); ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					nomor: nomor,
					// kodecabang: kodecabang
				},
				success: function(data) {
					for (var i = 0; i < data.length; i++) {
						$('#noinvoice').val(data[i].nomor.trim());
						$('#nogb').val(data[i].nomorgb.trim());
						$('#tanggalinv').val(FormatDate(data[i].tanggal.trim()));
						$('#nopacking').val(data[i].nopacking.trim());
						$('#tgljthtempo').val(FormatDate(data[i].tgljthtempo.trim()));
						$('#nomorso').val(data[i].nomorso.trim());
						$('#tanggalso').val(FormatDate(data[i].tanggalso.trim()));
						$('#nomorpelanggan').val(data[i].nopelanggan.trim());
						$('#namapelanggan').val(data[i].namapelanggan.trim());
						$('#salesman').val(data[i].namasalesman.trim());
						$('#koderekening').val(data[i].norek.trim());
						$('#norekening').val(data[i].norekening.trim());
						$('#namapemilik').val(data[i].namapemilik.trim());
						if (data[i].jenisjual.trim() == '1' || data[i].jenisjual.trim() == '4') {
							$('#jenisjual').val('TUNAI');
						} else if (data[i].jenisjual.trim() == '2') {
							$('#jenisjual').val('KREDIT');
						} else {
							$('#jenisjual').val('COD');
						}
						$('#nomorfaktur').val(data[i].nofakturpajak.trim());
						$('#lokasi').val(data[i].lokasi.trim());
						$('#discash').val(FormatRupiah(data[i].disccash.trim()));
						$('#discustomer').val(FormatRupiah(data[i].disccustomer.trim()));
						$('#dismanual').val(FormatRupiah(data[i].discmanual.trim()));
						$('#nilaicash').val(FormatRupiah(data[i].nilaicash.trim()));
						$('#nilaicustomer').val(FormatRupiah(data[i].nilaicustomer.trim()));
						$('#nilaidiscmanual').val(FormatRupiah(data[i].nilaimanual.trim()));
						$('#total').val(FormatRupiah(data[i].total.trim()));
						$('#dpp').val(FormatRupiah(data[i].dpp.trim()));
						$('#ppn').val(FormatRupiah(data[i].ppn.trim()));
						$('#grandtotal').val(FormatRupiah(data[i].grandtotal.trim()));
					}
					GetDataDetail(nomor);
					$('#cancel').prop('disabled', false);
					// $('#update').prop('disabled', false);
					$('#save').prop('disabled', true);
					$('#invbatal').prop('disabled', true);
					$('#tgljatuhtempo').prop('disabled', true);
				}
			});
		};

		function GetDataDetail(nomor) {
			$.ajax({
				url: "<?= base_url('sales/Invoice/DataInvoiceDetail'); ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					nomor: nomor,
					// kodecabang: kodecabang
				},
				success: function(data) {
					for (var i = 0; i < data.length; i++) {
						// var kode = data[i].kode;
						var kodebarang = data[i].kodebarang.trim();
						var namabarang = data[i].namabarang.trim();
						var qty = data[i].qty.trim();
						var harga = data[i].harga.trim();
						var disc = data[i].discbarang.trim();
						var discitem = data[i].discbrgitem.trim();
						var discpaket = data[i].discpaket.trim();
						var discpaketitem = data[i].discpaketitem.trim();
						var subtotal = data[i].totalprice.trim();

						var nomor = "";
						var action = "-";
						myFunction(nomor, kodebarang, namabarang, qty, harga, disc, discitem, discpaket, discpaketitem, subtotal, action);
					}
				}
			});
		};

		// $(document).on('click', ".reprintok", function() {
		//     var nomor = $(this).attr("data-id");
		//     var datadetail = AmbilDataDetail();

		//     if (ValidasiSave(datadetail) == true) {
		//         $.ajax({
		//             url: "<?= base_url("sales/Invoice/ReprintOk") ?>",
		//             method: "POST",
		//             dataType: "json",
		//             async: true,
		//             data: {
		//                 nomor: nomor,
		//                 datadetail: datadetail
		//             },
		//             success: function(data) {
		//                 if (data.nomor != "" && data.nomorsj != "" && data.lokasi != "") {
		//                     Toast.fire({
		//                         icon: 'success',
		//                         title: data.message
		//                     });
		//                     $('#noinvoice').val(data.nomor);
		//                     $('#nosj').val(data.nomorsj);
		//                     $('#tanggalinv').prop("disabled", true);
		//                     $('#nopacking').prop("disabled", true);
		//                     $('#tgljthtempo').prop("disabled", true);
		//                     $('#nomorso').prop("disabled", true);
		//                     $('#tanggalso').prop("disabled", true);
		//                     $('#nomorpelanggan').prop("disabled", true);
		//                     $('#namapelanggan').prop("disabled", true);
		//                     $('#nomorfaktur').prop("disabled", true);
		//                     $('#lokasi').prop("disabled", true);
		//                     $('#discash').prop("disabled", true);
		//                     $('#discustomer').prop("disabled", true);
		//                     $('#total').prop("disabled", true);
		//                     $('#dpp').prop("disabled", true);
		//                     $('#ppn').prop("disabled", true);
		//                     $('#grandtotal').prop("disabled", true);

		//                     // $('#update').prop("disabled", true);
		//                     $('#save').prop("disabled", true);
		//                     $('#invbatal').prop("disabled", true);


		//                     $('#caripacking').hide();
		//                     $('#caribarang').hide();
		//                     $('#adddetail').hide();
		//                     $('.editdetail').hide();
		//                     $('.deldetail').hide();

		//                     $('.nosj').show();
		//                     if (data.lokasi == $('#lokasi').val()) {
		//                         CetakInvoice(data.nomor);
		//                         CetakSuratJalan(data.nomorsj);
		//                     }
		//                 } else {
		//                     Toast.fire({
		//                         icon: 'error',
		//                         title: data.message
		//                     });
		//                 }
		//             }
		//         }, false)
		//     }
		// });

		document.getElementById('okcancel').addEventListener("click", function(event) {
			event.preventDefault();
			var nomor = $('#noinvoice').val();
			var nomorgb = $('#nogb').val();
			var nomorso = $('#nomorso').val();
			var nopacking = $('#nopacking').val();
			var keterangan = $('#keteranganbatal').val();
			var tanggalinv = $('#tanggalinv').val();
			var datadetail = AmbilDataDetail();
			if (nomor == "") {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Pilih Dulu Nomor Yang Ingin dibatalkan',
					showCloseButton: true,
					width: 350,
				});
			} else if (keterangan == "") {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Keterangan Batal Tidak Boleh Kosong',
					showCloseButton: true,
					width: 350,
				});
			} else {
				$.ajax({
					url: "<?= base_url("sales/Invoice/Cancel") ?>",
					method: "POST",
					dataType: "json",
					async: true,
					data: {
						nomor: nomor,
						nomorso: nomorso,
						nopacking: nopacking,
						keterangan: keterangan,
						tanggalinv: tanggalinv,
						nomorgb: nomorgb,
						datadetail: datadetail
					},
					success: function(data) {
						console.log(data);
						if (data.nomor != "") {
							Toast.fire({
								icon: 'success',
								title: data.message
							});
							location.reload(true);
						} else {
							Toast.fire({
								icon: 'error',
								title: data.message
							});
						}
					}
				}, false)
			}
		})

		document.getElementById("clear").addEventListener("click", function(event) {
			event.preventDefault();
			location.reload(true);
		})

		ClearScreen();
	})
</script>