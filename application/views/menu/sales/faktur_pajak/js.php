<script type="text/javascript">
    $(document).ready(function() {

        function LoadNomorFakturPajak() {
            $.ajax({
                url: "<?= base_url('masterdata/RegistrasiNomorFakturPajak/LoadNomorFakturPajak') ?>",
                method: "POST",
                dataType: "json",
                async: false,
                data: {
                    // cabang: $('#cabang').val()
                },
                success: function(data) {
                    for (var i = 0; i < data.length; i++) {
                        $("#nofakturpajak").val(data[i].nomor);
                    }
                }
            }, false);
        }

        function RefreshLayar() {
            location.reload(true);
        };

        function ClearScreen() {
            $('#nomor').val('');
            $('#nofakturpajak').empty();
            LoadNomorFakturPajak();
            $('#tanggal').val('<?= date('Y-m-d') ?>');
            $('#noinvoice').val('');
            $('#tglppn').val('<?= date('Y-m-d') ?>');
            $('#namapelanggan').val('');
            $('#nopelanggan').val('');
            $('#dpp').val('');
            $('#ppn').val('');
            $('#grandtotal').val('');
            $('#keterangan1').val('').prop('disabled', false);
            $('#keterangan2').val('').prop('disabled', false);
            $('#penanggungjawab').val('').prop('disabled', false);
            $('#jabatan').val('').prop('disabled', false);

            $('#cancel').prop('disabled', true);
            $('#save').prop('disabled', false).show();

            $('#cariinvoice').show();
        }

        function ValidasiSave() {
            var penanggungjawab = $('#penanggungjawab').val();
            var jabatan = $('#jabatan').val();
            var nofaktur = $('#noinvoice').val();

            if (nofaktur == '' || nofaktur == 0) {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Nomor Faktur tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#noinvoice').focus();
                var result = false;
            } else if (penanggungjawab == '' || penanggungjawab == 0) {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Penanggung Jawab tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#penanggungjawab').focus();
                var result = false;
            } else if (jabatan == '' || jabatan == 0) {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Jabatan tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#jabatan').focus();
                var result = false;
            } else {
                var result = true;
            }
            return result;
        }

        /* Declare Toast */
        const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000,
            timerProgressBar: true,
            didOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
            }
        });

        $('#tglppn').datepicker({
            format: "yyyy-mm-dd",
            autoclose: true,
            todayHighlight: true
        });

        function FormatRupiah(angka, prefix) {
            if (!angka) {
                return '';
            }
            var vangka = angka.toString();
            var number_string = vangka.replace(/[^.\d]/g, '').replace(/[^\w\s]/gi, '').toString(),
                split = number_string.split('.'),
                sisa = split[0].length % 3,
                rupiah = split[0].substr(0, sisa),
                ribuan = split[0].substr(sisa).match(/\d{3}/gi);

            // tambahkan titik jika yang di input sudah menjadi angka ribuan
            if (ribuan) {
                separator = sisa ? ',' : '';
                rupiah += separator + ribuan.join(',');
            }

            rupiah = split[1] != undefined ? rupiah + '.' + split[1] : rupiah;
            return prefix == undefined ? rupiah : (rupiah ? rupiah : '');
        };

        function FormatDate(input) {
            var date = new Date(input);
            var yyyy = date.getFullYear();
            var mm = date.getMonth() + 1;
            var dd = date.getDate();
            var hrs = date.getHours();
            var mnt = date.getMinutes();


            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }
            if (hrs < 10) {
                hrs = '0' + hrs;
            }
            if (mnt < 10) {
                mnt = '0' + mnt;
            }
            return yyyy + '-' + mm + '-' + dd;
        };

        function getDigitToFormat(val) {
            if (val < 10) {
                val = '0' + val;
            }
            return val.toString();
        };

        document.getElementById("cariinvoice").addEventListener("click", function(event) {
            event.preventDefault();
            $('#tb_detail').empty();
            $('#t_invoice').DataTable({
                "language": {
                    "url": "<?= base_url() ?>assets/json/id.json"
                },
                "destroy": true,
                "searching": true,
                "processing": true,
                "serverSide": true,
                "lengthChange": true,
                "pageLength": 5,
                "lengthMenu": [5, 10, 25, 50],
                "order": [],
                "ajax": {
                    "url": "<?= base_url('sales/Invoice/CariDataInvoice'); ?>",
                    "method": "POST",
                    "data": {
                        nmtb: "srvt_invoice",
                        field: {
                            nomor: "nomor",
                            tanggal: "tanggal",
                            nomorso: "nomorso",
                            tgljthtempo: "tgljthtempo",
                            namapelanggan: "namapelanggan"
                        },
                        sort: "nomor",
                        where: {
                            nomor: "nomor",
                            namapelanggan: "namapelanggan"
                        },
                        // value: "batal = false AND kodecabang = '" + $('#cabang').val() + "' AND nofakturpajak = '' "
                        value: "batal = false AND nofakturpajak = '' "
                    },
                },
                "columnDefs": [{
                        "targets": 2,
                        "render": $.fn.dataTable.render.moment('YYYY-MM-DD HH:mm:ss', 'D MMM YY')
                    },
                    {
                        "targets": 4,
                        "render": $.fn.dataTable.render.moment('YYYY-MM-DD HH:mm:ss', 'D MMM YY')
                    }
                ]
            });
        })

        $(document).on('click', ".searchinvoice", function() {
            var nomor = $(this).attr("data-id");
            GetDataInvoice(nomor);
        });

        function GetDataInvoice(nomor) {
            $.ajax({
                url: "<?= base_url('sales/Invoice/DataInvoice'); ?>",
                method: "POST",
                dataType: "json",
                async: false,
                data: {
                    nomor: nomor
                },
                success: function(data) {
                    for (var i = 0; i < data.length; i++) {
                        $('#noinvoice').val(data[i].nomor.trim());
                        $('#nopelanggan').val(data[i].nopelanggan.trim());
                        $('#namapelanggan').val(data[i].namapelanggan.trim());
                        $('#dpp').val(FormatRupiah(data[i].dpp.trim()));
                        $('#ppn').val(FormatRupiah(data[i].ppn.trim()));
                        $('#grandtotal').val(FormatRupiah(data[i].grandtotal.trim()));
                    }

                }
            });
        };

        document.getElementById("find").addEventListener("click", function(event) {
            event.preventDefault();
            $('#tb_detail').empty();
            $('#t_fp').DataTable({
                "language": {
                    "url": "<?= base_url() ?>assets/json/id.json"
                },
                "destroy": true,
                "searching": true,
                "processing": true,
                "serverSide": true,
                "lengthChange": true,
                "pageLength": 5,
                "lengthMenu": [5, 10, 25, 50],
                "order": [],
                "ajax": {
                    "url": "<?= base_url('sales/FakturPajak/CariDataFakturPajak'); ?>",
                    "method": "POST",
                    "data": {
                        nmtb: "cari_fakturpajak",
                        field: {
                            nomor: "nomor",
                            tanggal: "tanggal",
                            nofaktur: "nofaktur",
                            nama_toko: "nama_toko",
                            penanggungjawab: "penanggungjawab",
                            jabatan: "jabatan"
                        },
                        sort: "nomor",
                        where: {
                            nomor: "nomor",
                            nofaktur: "nofaktur",
                            nama_toko: "nama_toko",
                            penanggungjawab: "penanggungjawab",
                            jabatan: "jabatan"
                        },
                        value: "batal = false"
                    },
                },
                "columnDefs": [{
                    "targets": 2,
                    "render": $.fn.dataTable.render.moment('YYYY-MM-DD HH:mm:ss', 'D MMM YY')
                }]
            });
        })

        $(document).on('click', ".searchfakturpajak", function() {
            var nomor = $(this).attr("data-id");
            GetData(nomor);
        });

        function GetData(nomor) {
            $.ajax({
                url: "<?= base_url('sales/FakturPajak/DataFakturPajak'); ?>",
                method: "POST",
                dataType: "json",
                async: false,
                data: {
                    nomor: nomor
                },
                success: function(data) {
                    for (var i = 0; i < data.length; i++) {
                        $('#nomor').val(data[i].nomor.trim());
                        $('#tanggal').val(FormatDate(data[i].tanggal.trim()));
                        $('#nofakturpajak').val(data[i].nofakturpajak.trim());
                        $('#noinvoice').val(data[i].nofaktur.trim());
                        $('#nopelanggan').val(data[i].nopelanggan.trim());
                        $('#namapelanggan').val(data[i].nama_toko.trim());
                        $('#keterangan1').val(data[i].keterangan.trim());
                        $('#keterangan2').val(data[i].keterangan2.trim());
                        $('#penanggungjawab').val(data[i].penanggungjawab.trim());
                        $('#jabatan').val(data[i].jabatan.trim());
                        $('#dpp').val(FormatRupiah(data[i].dpp.trim()));
                        $('#ppn').val(FormatRupiah(data[i].ppn.trim()));
                        $('#grandtotal').val(FormatRupiah(data[i].grandtotal.trim()));
                    }
                    $('#save').prop('disabled', true);
                    $('#cancel').prop('disabled', false);
                    $('#keterangan1').prop('disabled', true);
                    $('#keterangan2').prop('disabled', true);
                    $('#penanggungjawab').prop('disabled', true);
                    $('#jabatan').prop('disabled', true);
                    $('#cariinvoice').hide();
                }
            });
        };

        document.getElementById("save").addEventListener("click", function(event) {
            event.preventDefault();
            var nomor = $('#nomor').val();
            var nofakturpajak = $('#nofakturpajak').val();
            var tanggal = $('#tanggal').val();
            var noinvoice = $('#noinvoice').val();
            var tglppn = $('#tglppn').val();
            var namapelanggan = $('#namapelanggan').val();
            var nopelanggan = $('#nopelanggan').val();
            var dpp = $('#dpp').val();
            var ppn = $('#ppn').val();
            var grandtotal = $('#grandtotal').val();
            var keterangan1 = $('#keterangan1').val();
            var keterangan2 = $('#keterangan2').val();
            var penanggungjawab = $('#penanggungjawab').val();
            var jabatan = $('#jabatan').val();

            // if(ValidasiSave() == true){
            $('#save').hide();
            $.ajax({
                url: "<?= base_url("sales/FakturPajak/Save") ?>",
                method: "POST",
                dataType: "json",
                async: true,
                data: {
                    nomor: nomor,
                    nofakturpajak: nofakturpajak,
                    tanggal: tanggal,
                    noinvoice: noinvoice,
                    tglppn: tglppn,
                    nopelanggan: nopelanggan,
                    dpp: dpp,
                    ppn: ppn,
                    grandtotal: grandtotal,
                    keterangan1: keterangan1,
                    keterangan2: keterangan2,
                    penanggungjawab: penanggungjawab,
                    jabatan: jabatan
                },
                success: function(data) {
                    if (data.nomor != "") {
                        Toast.fire({
                            icon: 'success',
                            title: data.message
                        });
                        $('#nomor').val(data.nomor);
                        $('#save').prop('disabled', true).hide();
                    } else {
                        Toast.fire({
                            icon: 'error',
                            title: data.message
                        });
                        $('#save').prop('disabled', false).hide();
                    }
                }
            }, false)
            // }
        })

        document.getElementById('okcancel').addEventListener("click", function(event) {
            event.preventDefault();
            var nomor = $('#nomor').val();
            var keterangan = $('#keteranganbatal').val();
            if (nomor == "") {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Pilih Dulu Nomor Yang Ingin dibatalkan',
                    showCloseButton: true,
                    width: 350,
                });
            } else if (keterangan == "") {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Keterangan Batal Tidak Boleh Kosong',
                    showCloseButton: true,
                    width: 350,
                });
            } else {
                $.ajax({
                    url: "<?= base_url("sales/FakturPajak/Cancel") ?>",
                    method: "POST",
                    dataType: "json",
                    async: true,
                    data: {
                        nomor: nomor,
                        keterangan: keterangan
                    },
                    success: function(data) {
                        if (data.nomor != "") {
                            Toast.fire({
                                icon: 'success',
                                title: data.message
                            });
                            // RefreshLayar();
                        } else {
                            Toast.fire({
                                icon: 'error',
                                title: data.message
                            });
                        }
                    }
                }, false)
            }
        })

        document.getElementById("clear").addEventListener("click", function(event) {
            event.preventDefault();
            ClearScreen();
            RefreshLayar();
        })

        ClearScreen();
    })
</script>