<script type="text/javascript">
    $(document).ready(function() {

        // function ClearScreen() {
        //     $('#jenisdokumen').val("");
        //     $('.faktur').hide();
        //     $('.suratjalan').hide();
        // }

        // document.getElementById("cari_data").addEventListener("click", function(event) {
        //     if ($('#jenisdokumen').val() == '1') {
        //         $('.faktur').show();
        //         $('.suratjalan').hide();
        //     } else if ($('#jenisdokumen').val() == '2') {
        //         $('.faktur').hide();
        //         $('.suratjalan').show();
        //     } else {
        //         $('.faktur').hide();
        //         $('.suratjalan').hide();
        //     }
        // })

        /* Declare Toast */
        const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000,
            timerProgressBar: true,
            didOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
            }
        });

        var values = "";
        if ($('#grup').val() == 'GUDANG JKT' && $('#gudang').val() == 'JD') {
            values = "batal = false AND kodecabang = '" + $('#cabang').val() + "' AND reqreprint = false"
        } else if ($('#grup').val() == 'GUDANG JKT' && $('#gudang').val() == 'SD') {
            values = "batal = false AND kodecabang = '" + $('#cabang').val() + "' AND reqreprint = false"
        } else if ($('#grup').val() == 'GUDANG JKT') {
            values = "batal = false AND kodecabang = '" + $('#cabang').val() + "' AND lokasi = '" + $('#gudang').val() + "' AND reqreprint = false"
        } else {
            values = "batal = false AND kodecabang = '" + $('#cabang').val() + "' AND reqreprint = false"
        }
        var faktur = $('#t_faktur').DataTable({
            "language": {
                "url": "<?= base_url() ?>assets/json/id.json"
            },
            "destroy": true,
            "searching": true,
            "serverSide": true,
            "lengthChange": true,
            "pageLength": 5,
            "lengthMenu": [5, 10, 25, 50],
            "order": [],
            "ajax": {
                "url": "<?= base_url('sales/Invoice/CariDataReqReprintInvoice'); ?>",
                "method": "POST",
                "data": {
                    nmtb: "cari_reprint",
                    field: {
                        nomorgb: "nomorgb",
                        nomor: "nomor",
                        tanggal: "tanggal",
                        nomorsj: "nomorsj",
                        nomorso: "nomorso",
                        tgljthtempo: "tgljthtempo",
                        namapelanggan: "namapelanggan",
                        namacabang: "namacabang",
                        namagudang: "namagudang"
                    },
                    sort: "nomorgb",
                    where: {
                        nomorgb: "nomorgb",
                        nomor: "nomor",
                        nomorsj: "nomorsj",
                        nomorso: "nomorso",
                        namapelanggan: "namapelanggan"
                    },
                    value: values
                },
            },
            "columnDefs": [{
                    "targets": 3,
                    "render": $.fn.dataTable.render.moment('YYYY-MM-DD HH:mm:ss', 'D MMM YY')
                },
                {
                    "targets": 6,
                    "render": $.fn.dataTable.render.moment('YYYY-MM-DD HH:mm:ss', 'D MMM YY')
                }
            ]
        });

        // var suratjalan = $('#t_suratjalan').DataTable({
        //     "language": {
        //         "url": "<?= base_url() ?>assets/json/id.json"
        //     },
        //     "destroy": true,
        //     "searching": true,
        //     "serverSide": true,
        //     "lengthChange": true,
        //     "pageLength": 5,
        //     "lengthMenu": [5, 10, 25, 50],
        //     "order": [],
        //     "ajax": {
        //         "url": "<?= base_url('sales/Invoice/CariDataReqReprintSuratJalan'); ?>",
        //         "method": "POST",
        //         "data": {
        //             nmtb: "cari_reprint",
        //             field: {
        //                 nomorsj: "nomorsj",
        //                 tanggalsj: "tanggalsj",
        //                 nomor: "nomor",
        //                 namapelanggan: "namapelanggan",
        //                 namacabang: "namacabang",
        //                 namagudang: "namagudang"
        //             },
        //             sort: "nomorsj",
        //             where: {
        //                 nomorsj: "nomorsj",
        //                 namapelanggan: "namapelanggan"
        //             },
        //             value: "batal = false AND kodecabang = '" + $('#cabang').val() + "' AND reqreprintsj = false"
        //         },
        //     },
        //     "columnDefs": [{
        //             "targets": 2,
        //             "render": $.fn.dataTable.render.moment('YYYY-MM-DD HH:mm:ss', 'D MMM YY')
        //         }
        //     ]
        // });

        $(document).on('click', ".reqreprintinvoice", function() {
            var nomor = $(this).attr("data-id");
            ReqReprintInvoice(nomor);
        });

        // $(document).on('click', ".reqreprintsuratjalan", function() {
        //     var nomor = $(this).attr("data-id");
        //     ReqReprintSuratJalan(nomor);
        // });

        function ReqReprintInvoice(nomor) {
            $.ajax({
                url: "<?= base_url("sales/Invoice/ReqReprintInvoice") ?>",
                method: "POST",
                dataType: "json",
                async: true,
                data: {
                    nomor: nomor
                },
                success: function(data) {
                    if (data.nomor != "") {
                        Toast.fire({
                            icon: 'success',
                            title: data.message
                        });
                    } else {
                        Toast.fire({
                            icon: 'error',
                            title: data.message
                        });
                    }
                }
            }, false)
        }

        // function ReqReprintSuratJalan(nomor) {
        //     $.ajax({
        //         url: "<?= base_url("sales/Invoice/ReqReprintSuratJalan") ?>",
        //         method: "POST",
        //         dataType: "json",
        //         async: true,
        //         data: {
        //             nomor: nomor
        //         },
        //         success: function(data) {
        //             if (data.nomor != "") {
        //                 Toast.fire({
        //                     icon: 'success',
        //                     title: data.message
        //                 });
        //             } else {
        //                 Toast.fire({
        //                     icon: 'error',
        //                     title: data.message
        //                 });
        //             }
        //         }
        //     }, false)
        // }

        setInterval(function() {
            faktur.ajax.reload(null, false);
            // suratjalan.ajax.reload(null, false); // user paging is not reset on reload
        }, 3000);

        // document.getElementById("clear").addEventListener("click", function(event) {
        //     ClearScreen();
        // })

        // ClearScreen();
    });
</script>
