<script type="text/javascript">
    $(document).ready(function() {

        function ClearScreen() {
            document.getElementById('save').disabled = false;
            document.getElementById('update').disabled = true;

            $('#qtybesar').prop("disabled", false);
            $('#qtykecil').prop("disabled", false);
            $('#kadar').prop("disabled", false);

            $('#kodefg').val("");
            $('#namafg').val("");
            $('#fgbesar').val(0);
            $('#fgkecil').val(0);
            $('#fgkonversi').val(0);

            $('#jenismaterial').val("");
            $('#kodematerial').val("");
            $('#namamaterial').val("");
            $('#qtybesar').val(0);
            $('#uombesar').val("");
            $('#qtykecil').val(0);
            $('#uomkecil').val("");
            $('#konversi').val(0);
            $('#kadar').val(0);

            $('#tb_detail').empty();

        };

        function ValidasiSave(datadetail) {
            if($('#namafg').val() == ''){
                $.alert({
                    title: '<i class="fa fa-info-circle" style="color: #e60029; margin-right: 10px;" aria-hidden="true"></i><span style="font-family: bahnschrift; color: #e60029;">Info ...</span>',
                    content: 'Nama FG Tidak Boleh Kosong',
                    buttons: {
                        formSubmit: {
                            text: 'OK',
                            btnClass: 'btn-red'
                        }
                    }
                });   
                var result = false;
            } else if($('#fgbesar').val() == ''){
                $.alert({
                    title: '<i class="fa fa-info-circle" style="color: #e60029; margin-right: 10px;" aria-hidden="true"></i><span style="font-family: bahnschrift; color: #e60029;">Info ...</span>',
                    content: 'QTY FG Besar Tidak Boleh Kosong',
                    buttons: {
                        formSubmit: {
                            text: 'OK',
                            btnClass: 'btn-red'
                        }
                    }
                });   
                var result = false;
            } else if($('#fgkecil').val() == ''){
                $.alert({
                    title: '<i class="fa fa-info-circle" style="color: #e60029; margin-right: 10px;" aria-hidden="true"></i><span style="font-family: bahnschrift; color: #e60029;">Info ...</span>',
                    content: 'QTY FG kecil Tidak Boleh Kosong',
                    buttons: {
                        formSubmit: {
                            text: 'OK',
                            btnClass: 'btn-red'
                        }
                    }
                });   
                var result = false;
            } else if (datadetail.length == 0) {
                $.alert({
                    title: '<i class="fa fa-info-circle" style="color: #e60029; margin-right: 10px;" aria-hidden="true"></i><span style="font-family: bahnschrift; color: #e60029;">Info ...</span>',
                    content: 'Detail Tidak Boleh Kosong',
                    buttons: {
                        formSubmit: {
                            text: 'OK',
                            btnClass: 'btn-red'
                        }
                    }
                });   
                var result = false;
            } else {
                var result = true;
            }
            return result;
        };

        document.getElementById("carifg").addEventListener("click", function(event){
			event.preventDefault();
			$('#t_fg').DataTable({
				"destroy": true,
				"searching": true,
				"processing": true,
				"serverSide": true,
				"lengthChange": false,
				"pageLength": 5,
				"lengthMenu": [5, 25, 50, 100],
				"order": [],
				"ajax": {
					"url": "<?php echo base_url('masterdata/Bom/CariDataFG'); ?>",
					"method": "POST",
					"data": {
						nmtb: "glbm_finishgoods",
						field:{
							kode: "kode",
							nama: "nama",
						},
						sort: "kode",
						where: {
							kode: "kode"
						},
                        // value: ""
					},
				}
			});
		});

        $(document).on('click', ".searchfg", function() {
            var kode = $(this).attr("data-id");
            GetDataFG(kode);
        });

		function GetDataFG(kode) {
            $.ajax({
                url: "<?php echo base_url('masterdata/Bom/DataFG'); ?>",
                method: "POST",
                dataType: "json",
                async: false,
                data: {
                    kode : kode
                },
                success: function(data) {
                    for (var i = 0; i < data.length; i++) {
                        $('#kodefg').val(data[i].kode.trim());
                        $('#namafg').val(data[i].nama.trim());
                        $('#uomfgbesar').val(data[i].uombesar.trim());
                        $('#uomfgkecil').val(data[i].uomkecil.trim());
						$('#fgkonversi').val(data[i].konversi.trim());
                    }
                }
            }, false);
        };

        document.getElementById("carimaterial").addEventListener("click", function(event) {
            event.preventDefault();
            var jenismaterial = $('#jenismaterial').val();
            $('#title-jenis-material').html("");
            if (jenismaterial == 'RM') {
                $('#title-jenis-material').append("Data Raw Material");
                $('#t_carimaterial').DataTable({
                    "destroy": true,
                    "searching": true,
                    "processing": true,
                    "serverSide": true,
                    "lengthChange": true,
                    "order": [],
                    "ajax": {
                        "url": "<?php echo base_url('masterdata/Bom/CariDataMaterial'); ?>",
                        "method": "POST",
                        "data": {
                            nmtb: "glbm_rawmaterial",
                            field: {
                                kode: "kode",
                                nama: "nama",
                                jenis: "jenis",
                                uombesar: "uombesar",
                                uomkecil: "uomkecil",
                                konversi: "konversi",
                            },
                            sort: "kode",
                            where: {
                                kode: "kode"
                            }
                        },
                    }
                });
            } else if (jenismaterial == 'BP') {
                $('#title-jenis-material').append("Data Bahan Pendukung");
                $('#t_carimaterial').DataTable({
                    "destroy": true,
                    "searching": true,
                    "processing": true,
                    "serverSide": true,
                    "lengthChange": true,
                    "order": [],
                    "ajax": {
                        "url": "<?php echo base_url('masterdata/Bom/CariDataMaterial'); ?>",
                        "method": "POST",
                        "data": {
                            nmtb: "glbm_bahanpendukung",
                            field: {
                                kode: "kode",
                                nama: "nama",
                                jenis: "jenis",
                                uombesar: "uombesar",
                                uomkecil: "uomkecil",
                                berat: "berat",
                            },
                            sort: "kode",
                            where: {
                                kode: "kode"
                            }
                        },
                    }
                });
            } else {
                $('#tb_carimaterial').empty();
                $.alert({
                    title: '<i class="fa fa-info-circle" style="color: #e60029; margin-right: 10px;" aria-hidden="true"></i><span style="font-family: bahnschrift; color: #e60029;">Info ...</span>',
                    content: 'Pilih Jenis Material Terlebih Dahulu',
                    buttons: {
                        formSubmit: {
                            text: 'OK',
                            btnClass: 'btn-red'
                        }
                    }
                });
                return false;
            }
            
        });

        $(document).on('click', ".searchmaterial", function() {
            var jenismaterial = $('#jenismaterial').val();
            var kode = $(this).attr("data-id");
            $.ajax({
                url: "<?php echo base_url('masterdata/Bom/DataMaterial'); ?>",
                method: "POST",
                dataType: "json",
                async: false,
                data: {
                    kode : kode,
                    jenismaterial : jenismaterial
                },
                success: function(data) {
                    for (var i = 0; i < data.length; i++) {
                        $('#kodematerial').val(data[i].kode.trim());
                        $('#namamaterial').val(data[i].nama.trim());
                        $('#uombesar').val(data[i].uombesar.trim());
                        $('#uomkecil').val(data[i].uomkecil.trim());
						$('#konversi').val(data[i].konversi.trim());
						$('#typematerial').val(data[i].jenis);

                        if (jenismaterial == 'BP' && data[i].jenis == 1) {
                            $('#qtybesar').val(1);
                            $('#qtykecil').val(0);
                            $('#qtybesar').prop("disabled", false);
                            $('#qtykecil').prop("disabled", true);
                            $('#kadar').prop("disabled", true);
                        } else if (jenismaterial == 'BP' && data[i].jenis == 2) {
                            $('#qtybesar').prop("disabled", true);
                            $('#qtykecil').prop("disabled", false);
                            $('#kadar').prop("disabled", true);
                            $('#qtybesar').val(0);
                            $('#qtykecil').val(1);
                        } else if (jenismaterial == 'RM') {
                            $('#qtybesar').prop("disabled", false);
                            $('#qtykecil').prop("disabled", true);
                            $('#kadar').prop("disabled", false);
                            $('#qtybesar').val(1);
                            $('#qtykecil').val(data[i].konversi.trim());
                        }
                    }
                }
            }, false);
        });

        $("#fgbesar").keyup(function(){
            var qtybesar = this.value;
            var qtykecil = $('#fgkecil').val();
            var konversi = $('#fgkonversi').val();
            var subtotal = parseFloat(konversi) * parseFloat(qtybesar);
            $('#fgkecil').val(subtotal);
        });

        $("#qtybesar").keyup(function(){
            var jenismaterial = $('#jenismaterial').val();
            if (jenismaterial == 'RM') {
                var qtybesar = this.value;
                var qtykecil = $('#qtykecil').val();
                var konversi = $('#konversi').val();
                var subtotal = parseFloat(konversi) * parseFloat(qtybesar);
                $('#qtykecil').val(subtotal);
            }
        });

        $("#chgqtybesar").keyup(function(){
            var jenismaterial = $('#chgjenis').val();
            if (jenismaterial == 'RM') {
                var qtybesar = this.value;
                var qtykecil = $('#chgqtykecil').val();
                var konversi = $('#chgkonversi').val();
                var subtotal = parseFloat(konversi) * parseFloat(qtybesar);
                $('#chgqtykecil').val(subtotal);
            }
        });

        $("#adddetail").click(function() {
            var type = $('#typematerial').val();
            var jenis = $('#jenismaterial').val();
            var kode = $('#kodematerial').val();
            var nama = $('#namamaterial').val();
            var qtybesar = $('#qtybesar').val();
            var qtykecil = $('#qtykecil').val();
            var konversi = $('#konversi').val();
            var kadar = $('#kadar').val();
            var row = "";
            row = 
                '<tr id="'+ kode +'">' +
                    '<td>'+kode+'</td>' +
                    '<td>'+nama+'</td>' +
                    '<td style="display:none;">'+type+'</td>' +
                    '<td>'+jenis+'</td>' +
                    '<td>'+qtybesar+'</td>' +
                    '<td>'+qtykecil+'</td>' +
                    '<td style="display:none;">'+konversi+'</td>' +
                    '<td>'+kadar+'</td>' +
                    '<td style="text-align: center; width: 200px;">'+
                        '<button data-table="'+kode+'" data-toggle="modal" data-target="#changeqty" class="editdetail btn btn-success mb-0 mr-1 ml-1"><i class="fas fa-edit"></i> Edit</button>' +
                        '<button data-table="'+kode+'" class="deldetail btn btn-danger mb-0"><i class="fa fa-trash mr-1"></i> Delete</button>' +
                    '</td>'+
                '</tr>';
            $('#tb_detail').append(row);
            $('#typematerial').val(0);
            $('#kodematerial').val("");
            $('#namamaterial').val("");
            $('#qtybesar').val(0);
            $('#qtykecil').val(0);
            $('#konversi').val(0);
            $('#kadar').val(0);
        });

        $(document).on('click','.editdetail',function() {
			var table = document.getElementById('t_detail');
			var tr = document.getElementById($(this).attr("data-table"));
			var td = tr.getElementsByTagName("td");
			for (var c = 0, m = table.rows[0].cells.length; c < m-1; c++) {
				var chgth = table.rows[0].cells[c].innerHTML.replace(/ /g,'').replace(/\s/g,'').toLowerCase();
				var valuetd = td[c].innerHTML;

				$('#chg'+chgth).val(valuetd);
			}
            if ($('#chgjenis').val() == 'BP' && $('#chgtype').val() == 1) {
                $('#chgqtybesar').prop("disabled", false);
                $('#chgqtykecil').prop("disabled", true);
                $('#chgkadar').prop("disabled", true);
            } else if ($('#chgjenis').val() == 'BP' && $('#chgtype').val() == 2) {
                $('#chgqtybesar').prop("disabled", true);
                $('#chgqtykecil').prop("disabled", false);
                $('#chgkadar').prop("disabled", true);
            } else if ($('#chgjenis').val() == 'RM') {
                $('#chgqtybesar').prop("disabled", false);
                $('#chgqtykecil').prop("disabled", true);
                $('#chgkadar').prop("disabled", false);
            }
		});

        $(document).on('click','#changedetail',function() {
            var type = $('#chgtype').val();
            var jenis = $('#chgjenis').val();
            var kode = $('#chgkode').val();
            var nama = $('#chgnama').val();
            var qtybesar = $('#chgqtybesar').val();
            var qtykecil = $('#chgqtykecil').val();
            var konversi = $('#chgkonversi').val();
            var kadar = $('#chgkadar').val();
            $('#'+kode).remove();
			InsertDataDetail(kode, nama, type, jenis, qtybesar, qtykecil, konversi, kadar);
        });

        function InsertDataDetail(kode, nama, type, jenis, qtybesar, qtykecil, konversi, kadar) {
			
			row = 
                '<tr id="'+ kode +'">' +
                    '<td>'+kode+'</td>' +
                    '<td>'+nama+'</td>' +
                    '<td style="display:none;">'+type+'</td>' +
                    '<td>'+jenis+'</td>' +
                    '<td>'+qtybesar+'</td>' +
                    '<td>'+qtykecil+'</td>' +
                    '<td style="display:none;">'+konversi+'</td>' +
                    '<td>'+kadar+'</td>' +
                    '<td style="text-align: center; width: 200px;">'+
                        '<button data-table="'+kode+'" data-toggle="modal" data-target="#changeqty" class="editdetail btn btn-success mb-0 mr-1 ml-1"><i class="fas fa-edit"></i> Edit</button>' +
                        '<button data-table="'+kode+'" class="deldetail btn btn-danger mb-0"><i class="fa fa-trash mr-1"></i> Delete</button>' +
                    '</td>'+
                '</tr>';
            $('#tb_detail').append(row);
		};

        $(document).on('click','.deldetail',function() {
            var id = $(this).attr("data-table");
            $('#'+id).remove();
        });

        document.getElementById("find").addEventListener("click", function(event){
			event.preventDefault();
			$('#t_find').DataTable({
				"destroy": true,
				"searching": true,
				"processing": true,
				"serverSide": true,
				"lengthChange": false,
				"pageLength": 5,
				"lengthMenu": [5, 25, 50, 100],
				"order": [],
				"ajax": {
					"url": "<?php echo base_url('masterdata/Bom/CariData'); ?>",
					"method": "POST",
					"data": {
						nmtb: "glbm_bom",
						field:{
							kodefg: "kodefg",
							namafg: "namafg",
						},
						sort: "kodefg",
						where: {
							kodefg: "kodefg"
						},
                        
					},
				}
			});
		});

        $(document).on('click', ".searchfind", function() {
            var kode = $(this).attr("data-id");
            GetData(kode);
            document.getElementById('save').disabled = true;
            document.getElementById('update').disabled = false;
        });

		function GetData(kode) {
            $.ajax({
                url: "<?php echo base_url('masterdata/Bom/DataFind'); ?>",
                method: "POST",
                dataType: "json",
                async: false,
                data: {
                    kode : kode
                },
                success: function(data) {
                    for (var i = 0; i < data.length; i++) {
                        $('#kodefg').val(data[i].kodefg.trim());
                        $('#namafg').val(data[i].namafg.trim());
                        $('#fgbesar').val(data[i].qtybesar.trim());
                        $('#fgkecil').val(data[i].qtykecil.trim());
                        $('#fgkonversi').val(data[i].konversi.trim());
                    }
                    GetDataDetail(kode);
                }
            });
        };

        function GetDataDetail(kode) {
            $.ajax({
                url: "<?php echo base_url('masterdata/Bom/DataFindDetail'); ?>",
                method: "POST",
                dataType: "json",
                async: false,
                data: {
                    kode : kode
                },
                success: function(data) {
                    for (var i = 0; i < data.length; i++) {
                        var kode = data[i].kodematerial.trim();   
                        var nama = data[i].namamaterial.trim();
                        var type = data[i].typem.trim();
                        var jenis = data[i].jenismaterial.trim();
                        var qtybesar = data[i].qtybesar;
                        var qtykecil = data[i].qtykecil;
                        var konversi = data[i].konversi;
                        var kadar = data[i].kadar;

                        InsertDataDetail(kode, nama, type, jenis, qtybesar, qtykecil, konversi, kadar);
                    }
                }
            });
        };












        function AmbilDataDetail() {
			var table = document.getElementById('t_detail');
			var arr2 =[];
			for (var r = 1, n = table.rows.length; r < n; r++) {
				var string ="";
				for (var c = 0, m = table.rows[r].cells.length; c < m-1; c++) {
					if (c==0) {
						string = "{"+table.rows[0].cells[c].innerHTML+" : '"+table.rows[r].cells[c].innerHTML+"'";
					}
					else{
						string = string +", "+table.rows[0].cells[c].innerHTML+" : '"+table.rows[r].cells[c].innerHTML+"'";           
					}
				}
				string = string+"}";
				var obj = JSON.stringify(eval('(' + string + ')'));
				var arr = $.parseJSON(obj);
				arr2.push(arr);
			}
			return arr2;
		};

        document.getElementById("save").addEventListener("click", function(event) {
            event.preventDefault();

            var kode = $('#kodefg').val();
            var nama = $('#namafg').val();
            var qtybesar = $('#fgbesar').val();
            var qtykecil = $('#fgkecil').val();

            var datadetail = AmbilDataDetail();

            
            if (ValidasiSave(datadetail) == true){
                $.ajax({
                    url: "<?php echo base_url('masterdata/Bom/Save'); ?>",
                    method: "POST",
                    dataType: "json",
                    async: true,
                    data: {
                        kode : kode,
                        nama : nama,
                        qtybesar : qtybesar,
                        qtykecil : qtykecil,
                        datadetail : datadetail,
                    },
                    success: function(data) {
                        if (data.kode != "") {
                            $.alert({
                                title: '<i class="fa fa-info-circle" style="color: #e60029; margin-right: 10px;" aria-hidden="true"></i><span style="font-family: bahnschrift; color: #e60029;">Info ...</span>',
                                content: data.message,
                                buttons: {
                                    formSubmit: {
                                        text: 'OK',
                                        btnClass: 'btn-red'
                                    }
                                }
                            });
                            ClearScreen();
                        } else {
                            $.alert({
                                title: '<i class="fa fa-info-circle" style="color: #e60029; margin-right: 10px;" aria-hidden="true"></i><span style="font-family: bahnschrift; color: #e60029;">Info ...</span>',
                                content: data.message,
                                buttons: {
                                    formSubmit: {
                                        text: 'OK',
                                        btnClass: 'btn-red'
                                    }
                                }
                            });
                        }
                    }
                }, false);
            }
        });

        document.getElementById("update").addEventListener("click", function(event) {
            event.preventDefault();

            var kode = $('#kodefg').val();
            var nama = $('#namafg').val();
            var qtybesar = $('#fgbesar').val();
            var qtykecil = $('#fgkecil').val();

            var datadetail = AmbilDataDetail();

            
            if (ValidasiSave(datadetail) == true){
                $.ajax({
                    url: "<?php echo base_url('masterdata/Bom/Update'); ?>",
                    method: "POST",
                    dataType: "json",
                    async: true,
                    data: {
                        kode : kode,
                        nama : nama,
                        qtybesar : qtybesar,
                        qtykecil : qtykecil,
                        datadetail : datadetail,
                    },
                    success: function(data) {
                        if (data.kode != "") {
                            $.alert({
                                title: '<i class="fa fa-info-circle" style="color: #e60029; margin-right: 10px;" aria-hidden="true"></i><span style="font-family: bahnschrift; color: #e60029;">Info ...</span>',
                                content: data.message,
                                buttons: {
                                    formSubmit: {
                                        text: 'OK',
                                        btnClass: 'btn-red'
                                    }
                                }
                            });
                            ClearScreen();
                        } else {
                            $.alert({
                                title: '<i class="fa fa-info-circle" style="color: #e60029; margin-right: 10px;" aria-hidden="true"></i><span style="font-family: bahnschrift; color: #e60029;">Info ...</span>',
                                content: data.message,
                                buttons: {
                                    formSubmit: {
                                        text: 'OK',
                                        btnClass: 'btn-red'
                                    }
                                }
                            });
                        }
                    }
                }, false);
            }
        });

        document.getElementById("clear").addEventListener("click", function(event) {
            event.preventDefault();
            ClearScreen();
        });

        ClearScreen();

    });
</script>