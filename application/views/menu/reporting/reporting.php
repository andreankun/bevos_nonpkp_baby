<style>
    .logo-menu{
        background: #11101D;
        width: 80px;
        height: 80px;
        border-radius: 8px;
        padding: 5px 10px;
        color: #FFF;
        margin-right: 5px;
    }
    .modal-content{
        border-color: #EEE;
    }
    .hidescroll {
        scrollbar-width: none !important;
    }
    .hidescroll::-webkit-scrollbar {
        display: none !important;
    }
    .table-responsive {
        scrollbar-width: thin;
        scrollbar-color: #47404F #DEDEDE;
    }
    .table-responsive::-webkit-scrollbar {
        width: 10px;
        height: 10px;
    }

    .table-responsive::-webkit-scrollbar-track {
        background-color: #DEDEDE;
        border-radius: 100px;
    }

    .table-responsive::-webkit-scrollbar-thumb {
        border-radius: 100px;
        background-color: #47404F;
    }

</style>
<div class="col-md-12 mb-2">
    <div class="modal-header" style="padding: 5px;">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-3">
                    <h2>
                        <span class="logo-menu"><?php echo $icons ?></span>
                        <span><?php echo $title ?></span>
                    </h2>
                </div>
                <div class="col-md-9">
                    
                </div>
            </div>
        </div>
    </div>
</div>