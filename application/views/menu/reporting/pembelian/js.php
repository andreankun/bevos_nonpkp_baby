<script type="text/javascript">
	$(document).ready(function() {

		$('#jenis_laporan').change(function() {
			var jenislaporan = this.value;
			if (jenislaporan == '1') {
				$('.gudang').show();
			} else if (jenislaporan == '2') {
				$('.gudang').hide();
			} else if (jenislaporan == '3') {
				$('.gudang').hide();
			}
		})

		$('#tglawal').datepicker({
			format: "dd-mm-yyyy",
			autoclose: true,
			todayHightlight: true,
			startDate: '-100y'
		});

		$('#tglakhir').datepicker({
			format: "dd-mm-yyyy",
			autoclose: true,
			todayHightlight: true,
			startDate: '-100y'
		});

		/* Cari Data Cabang*/
		document.getElementById("carikodegudang").addEventListener("click", function(event) {
			event.preventDefault();
			$('#t_gudang').DataTable({
				"destroy": true,
				"searching": true,
				"processing": true,
				"serverSide": true,
				"lengthChange": true,
				"pageLength": 5,
				"lengthMenu": [5, 10, 25, 50],
				"order": [],
				"ajax": {
					"url": "<?= base_url('masterdata/Gudang/CariDataGudang'); ?>",
					"method": "POST",
					"data": {
						nmtb: "glbm_gudang",
						field: {
							kode: "kode",
							nama: "nama",
							// alamat: "alamat"
						},
						sort: "kode",
						where: {
							kode: "kode",
							nama: "nama",
							// alamat: "alamat"
						},
						value: "aktif = true AND kodecabang = '"+$("#kodecabang").val()+"'"
					},
				}
			});
		});

		/*Get Data Cabang*/
		$(document).on('click', ".searchgudang", function() {
			var kode = $(this).attr("data-id");
			$.ajax({
				url: "<?= base_url('masterdata/Gudang/DataGudang'); ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					kode: kode
				},
				success: function(data) {
					for (var i = 0; i < data.length; i++) {
						$('#kodegudang').val(data[i].kode.trim());
					}
				}
			}, false);
		});

		/* Cari Data Cabang*/
		document.getElementById("caricabang").addEventListener("click", function(event) {
			event.preventDefault();
			$('#t_gudang').DataTable({
				"destroy": true,
				"searching": true,
				"processing": true,
				"serverSide": true,
				"lengthChange": true,
				"pageLength": 5,
				"lengthMenu": [5, 10, 25, 50],
				"order": [],
				"ajax": {
					"url": "<?= base_url('masterdata/Cabang/CariDataCabang'); ?>",
					"method": "POST",
					"data": {
						nmtb: "glbm_cabang",
						field: {
							kode: "kode",
							nama: "nama",
							// alamat: "alamat"
						},
						sort: "kode",
						where: {
							kode: "kode",
							nama: "nama",
							// alamat: "alamat"
						},
						value: "aktif = true"
					},
				}
			});
		});

		/*Get Data Cabang*/
		$(document).on('click', ".searchcabang", function() {
			var kode = $(this).attr("data-id");
			$.ajax({
				url: "<?= base_url('masterdata/Cabang/DataCabang'); ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					kode: kode
				},
				success: function(data) {
					for (var i = 0; i < data.length; i++) {
						$('#kodecabang').val(data[i].kode.trim());
					}
				}
			}, false);
		});

		document.getElementById("print").addEventListener("click", function(event) {
			event.preventDefault();
			var tglawal = $('#tglawal').val();
			var tglakhir = $('#tglakhir').val();
			var gudang = $('#kodegudang').val();
			var cabang = $('#kodecabang').val();
			var jenislaporan = $('#jenis_laporan').val();

			if (jenislaporan == '1') {
				window.open(
					"<?php echo base_url('cetak_pdf/LaporanPembelian/PrintPembelianPdf/') ?>" + tglawal + ":" + tglakhir + ":" + gudang
				);
			} else if (jenislaporan == '2') {
				window.open(
					"<?php echo base_url('cetak_pdf/LaporanPembelian/PembelianAllgudang/') ?>" + tglawal + ":" + tglakhir + ":" + cabang
				)
			} else if (jenislaporan == '3') {
				window.open(
					"<?php echo base_url('cetak_pdf/LaporanPembelian/LaporanBarangImport/') ?>" + tglawal + ":" + tglakhir + ":" + cabang
				)
			}
		});

		document.getElementById("export").addEventListener("click", function(event) {
			event.preventDefault();
			var tglawal = $('#tglawal').val();
			var tglakhir = $('#tglakhir').val();
			var gudang = $('#kodegudang').val();
			var cabang = $('#kodecabang').val();
			var jenislaporan = $('#jenis_laporan').val();
			if (jenislaporan == '1') {
				window.open(
					"<?php echo base_url('cetak_pdf/LaporanPembelian/PrintPembelianExcel/') ?>" + tglawal + ":" + tglakhir + ":" + gudang
				);
			} else if (jenislaporan == '2'){
				window.open(
					"<?php echo base_url('cetak_pdf/LaporanPembelian/PrintPembelianAllGudangExcel/') ?>" + tglawal + ":" + tglakhir + ":" + cabang
				);
			}
		});

		function RefreshLayar() {
			location.reload(true);
		};


		document.getElementById("clear").addEventListener("click", function(event) {
			event.preventDefault();
			RefreshLayar();
		});
	});
</script>
