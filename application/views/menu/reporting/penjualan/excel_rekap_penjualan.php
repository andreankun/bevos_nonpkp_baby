<?php

header("Content-type: application/vnd-ms-excel");

header("Content-Disposition: attachment; filename=Laporan Rekap Penjualan.xls");

header("Pragma: no-cache");

header("Expires: 0");

function FormatRupiah($angka)
{
	$hasil_rupiah = number_format($angka, 0, ',', '.');
	return $hasil_rupiah;
}
?>
<h4 style="text-align: center;">LAPORAN REKAP PENJUALAN</h4>
CABANG : <?php echo $cabang ?>
<br>
PERIODE : <?php echo $tglawal ?> S/D <?php echo $tglakhir ?>
<br>
<table border="1" width="100%">

	<thead>
		<tr>
			<th>NO</th>
			<th>PEMBAYARAN</th>
			<th>INVOICE</th>
			<th>NAMA PELANGGAN</th>
			<th>NAMA TOKO</th>
			<th>NAMA SALES</th>
			<th>TANGGAL</th>
			<th>SUBTOTAL</th>
			<th>Total Diskon</th>
			<th>DPP</th>
			<th>PPN</th>
			<th>Total</th>
		</tr>
	</thead>

	<tbody>
		<?php
		  $total = 0;
		  $totaldiskon1 = 0;
		  $totaldpp = 0;
		  $totalppn = 0;
		  $grandtotal = 0;
		  $no = 1;
		  $diskon = 0;
		  $totaldiskon = 0;
		foreach ($reportlist as $val) : ?>
			<tr>
				<td><?= $no++ ?></td>
				<td><?php
					if ($val->jenisjual == 1) {
						echo "TUNAI";
					} else if ($val->jenisjual == 2) {
						echo "KREDIT";
					} else if ($val->jenisjual == 3) {
						echo "COD";
					} else {
						echo "TUNAI 0%";
					}
					?></td>
				<td><?= $val->nomor ?></td>
				<td><?= $val->nama ?></td>
				<td><?= $val->nama_toko ?></td>
				<td><?= $val->namasalesman ?></td>
				<td><?= date('d/m/Y', strtotime($val->tanggal)) ?></td>
				<!-- <td><?= FormatRupiah(intval($val->total)) ?></td>
				<td><?= FormatRupiah(intval($val->nilaicustomer + $val->nilaicash)) ?></td>
				<td><?= FormatRupiah(intval($val->dpp)) ?></td>
				<td><?= FormatRupiah(intval($val->ppn)) ?></td>
				<td><?= FormatRupiah(intval($val->grandtotal)) ?></td> -->
				<td><?= intval($val->total) ?></td>
				<td><?= intval($val->nilaicustomer + $val->nilaicash) ?></td>
				<td><?= intval($val->dpp) ?></td>
				<td><?= intval($val->ppn) ?></td>
				<td><?= intval($val->grandtotal) ?></td>
			</tr>
			<?php $total = $total + $val->total ?>
			<?php $totaldiskon1 = $totaldiskon1 + ($val->nilaicustomer + $val->nilaicash) ?>
			<?php $totaldpp = $totaldpp + $val->dpp ?>
			<?php $totalppn = $totalppn + $val->ppn ?>
			<?php $grandtotal = $grandtotal + $val->grandtotal ?>
		<?php endforeach; ?>
		<tr>
			<td colspan="7" style="text-align: right;">total : </td>
			<td style="text-align: right;"><?= $total ?></td>
			<td style="text-align: right;"><?= $totaldiskon1 ?></td>
			<td style="text-align: right;"><?= $totaldpp ?></td>
			<td style="text-align: right;"><?= $totalppn ?></td>
			<td style="text-align: right;"><?= $grandtotal ?></td>
		</tr>
	</tbody>
</table>
