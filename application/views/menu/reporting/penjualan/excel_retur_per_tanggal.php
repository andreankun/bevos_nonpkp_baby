<?php

header("Content-type: application/vnd-ms-excel");

header("Content-Disposition: attachment; filename=Laporan Retur Per Tanggal.xls");

header("Pragma: no-cache");

header("Expires: 0");

function FormatRupiah($angka)
{
    $hasil_rupiah = number_format($angka, 0, '.', ',');
    return $hasil_rupiah;
}
?>
<h4 style="text-align: center;">LAPORAN RETUR PER TANGGAL</h4>
CABANG : <?php echo $row->nama ?>
<br>
PERIODE : <?php echo $tglawal ?> S/D <?php echo $tglakhir ?>
<br>
<table border="1" width="100%">

    <thead>
        <tr>
            <th>NO. RETUR</th>
            <th>CUSTOMER#</th>
            <th>NAMA CUSTOMER#</th>
            <th>KODE#</th>
            <th>NAMA BARANG</th>
            <th>QTY</th>
            <th>HARGA</th>
            <th>DISC</th>
            <th>JUMLAH</th>
        </tr>
    </thead>

    <tbody>

        <?php
        foreach ($reportlist as $value) :
        ?>
            <tr>
                <td colspan="8" style="font-size: 14px; font-weight: bold;">Tanggal Retur : <?= date('d-m-Y', strtotime($value->tanggal)) ?></td>
            </tr>
            <?php $tanggal = $value->tanggal; ?>
            <?php
            $total = 0;
            $grandtotal = 0;
            foreach ($report as $val) : ?>
                <?php if ($val->tanggal == $tanggal) : ?>
                    <tr>
                        <td style="text-align: left;"><?= $val->nomor ?></td>
                        <td style="text-align: left;"><?= $val->nopelanggan ?></td>
                        <td style="text-align: left;"><?= $val->nama_toko ?></td>
                        <td style="text-align: left;"><?= $val->kodebarang ?></td>
                        <td style="text-align: left;"><?= $val->namabarang ?></td>
                        <td style="text-align: right;"><?= $val->qty ?></td>
                        <td style="text-align: right;"><?= FormatRupiah($val->harga) ?></td>
                        <td style="text-align: right;"><?= $val->discbarang ?>%</td>
                        <td style="text-align: right;"><?= FormatRupiah($val->total) ?></td>
                    </tr>
                    <?php $total = $total + $val->total; ?>
                <?php endif; ?>
                <?php $grandtotal = $grandtotal + $val->total; ?>
            <?php endforeach; ?>
            <tr style="float: right; text-align: right; font-weight: bold;">
                <td colspan="7">Total : </td>
                <td colspan="1"><?= FormatRupiah($total) ?></td>
            </tr>
        <?php endforeach; ?>
        <tr style="float: right; text-align: right; font-weight: bold;">
            <td colspan="7">Grand Total : </td>
            <td colspan="1"><?= FormatRupiah($grandtotal) ?></td>
        </tr>
    </tbody>
</table>