<?php

header("Content-type: application/vnd-ms-excel");

header("Content-Disposition: attachment; filename=Laporan Komisi Sales.xls");

header("Pragma: no-cache");

header("Expires: 0");

function FormatRupiah($angka)
{
    $hasil_rupiah = number_format($angka, 0, '.', ',');
    return $hasil_rupiah;
}
?>
<h4 style="text-align: center;">LAPORAN KOMISI</h4>
CABANG : <?php echo $row->nama ?>
<br>
PERIODE : <?php echo $tglawal ?> S/D <?php echo $tglakhir ?>
<br>
NAMA SALES : <?php echo $reportrow->namasalesman ?>
<br>
<table border="1" width="100%">

    <thead>
        <tr>
            <th>No.</th>
            <th>Tanggal</th>
            <th>Pelanggan</th>
            <th>Toko</th>
            <th>No. Proforma</th>
            <th>No. Faktur</th>
            <th>Subtotal</th>
            <th>Diskon</th>
            <th>Total</th>
            <th>Komisi</th>
        </tr>
    </thead>

    <tbody>
        <?php
        $no = 1;
        $htotal = 0;
        $htotal_diskon = 0;
        $hsubtotal = 0;
        $hkomisi = 0;
        foreach ($report as $vals) :
        ?>
            <tr>
                <td style="text-align: center;"><?= $no++; ?></td>
                <td style="text-align: center;"><?= date('d-m-Y', strtotime($vals->tanggal)) ?></td>
                <td style="text-align: left;"><?= $vals->namapelanggan ?></td>
                <td style="text-align: left;"><?= $vals->nama_toko ?></td>
                <td style="text-align: left;"><?= $vals->nomorso ?></td>
                <td style="text-align: left;"><?= $vals->nomor ?></td>
                <td style="text-align: right;"><?= FormatRupiah($vals->total) ?></td>
                <?php if ($vals->totaldiskon == 0) : ?>
                    <td style="text-align: right;"><?= '' ?></td>
                <?php else : ?>
                    <td style="text-align: right;"><?= FormatRupiah($vals->totaldiskon) ?></td>
                <?php endif; ?>
                <td style="text-align: right;"><?= FormatRupiah($vals->subtotal) ?></td>
                <td style="text-align: right;"><?= FormatRupiah(round($vals->subtotal * ($vals->komisi / 100))) ?></td>
                <?php $htotal = $htotal + $vals->total; ?>
                <?php $htotal_diskon = $htotal_diskon + $vals->totaldiskon; ?>
                <?php $hsubtotal = $hsubtotal + $vals->subtotal; ?>
                <?php $hkomisi = $hkomisi + round($vals->subtotal * ($vals->komisi / 100)); ?>
            </tr>
        <?php endforeach; ?>
        <tr style="float: right; text-align: right; font-weight: bold; text-transform: uppercase;">
            <td colspan="6">Grand Total : </td>
            <td colspan="1"><?= FormatRupiah($htotal) ?></td>
            <td colspan="1"><?= FormatRupiah($htotal_diskon) ?></td>
            <td colspan="1"><?= FormatRupiah($hsubtotal) ?></td>
            <td colspan="1"><?= FormatRupiah($hkomisi) ?></td>
        </tr>
    </tbody>
</table>