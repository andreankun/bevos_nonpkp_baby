<?php

header("Content-type: application/vnd-ms-excel");

header("Content-Disposition: attachment; filename=Laporan Penjualan Terlaris.xls");

header("Pragma: no-cache");

header("Expires: 0");

function FormatRupiah($angka)
{
	$hasil_rupiah = number_format($angka, 0, '.', ',');
	return $hasil_rupiah;
}
?>
<h4 style="text-align: center;">LAPORAN PENJUALAN TERLARIS</h4>
CABANG : <?php echo $cabang->nama ?>
<br>
KATERGORI : <?php echo $merkawal->nama ?> S/D <?php echo $merkakhir->nama ?>
<br>
PERIODE : <?php echo $tglawal ?> S/D <?php echo $tglakhir ?>
<br>
<table border="1" width="100%">

	<thead>
		<tr>
			<th>NO.</th>
			<th>KODE</th>
			<th>NAMA BARANG</th>
			<th>KATEGORI</th>
			<th>QTY</th>
		</tr>
	</thead>

	<tbody>
		<?php
		$no = 1;
		foreach ($report as $value) : ?>
			<tr>
				<td style="text-align: left;"><?= $no++ ?></td>
				<td style="text-align: left;"><?= $value->kodebarang ?></td>
				<td style="text-align: left;"><?= $value->namabarang ?></td>
				<td style="text-align: left;"><?= $value->namamerk ?></td>
				<td style="text-align: right;"><?= $value->qtybarang ?></td>
			</tr>
		<?php endforeach; ?>
	</tbody>
</table>
