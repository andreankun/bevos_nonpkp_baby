<script type="text/javascript">
    $(document).ready(function() {
        $('#tglawal').datepicker({
            format: "yyyy-mm-dd",
            autoclose: true,
            todayHighlight: true
        });

        $('#tglakhir').datepicker({
            format: "yyyy-mm-dd",
            autoclose: true,
            todayHighlight: true
        });
        $('#jenis_laporan').change(function() {
            var jenisdokumen = this.value;
            if (jenisdokumen == '1') {
                $('.pelanggan').hide();
                $('.barang').hide();
                $('.salesman').hide();
                $('.gdg').hide();
                $('.cbg').show();
                $('.tgl').show();
                $('#preview').hide();
                $('#print').show();
                $('.merk').hide();
            } else if (jenisdokumen == '2') {
                $('.pelanggan').show();
                $('.barang').hide();
                $('.salesman').hide();
                $('.gdg').hide();
                $('.cbg').show();
                $('.tgl').show();
                $('#preview').hide();
                $('#print').show();
                $('.merk').hide();
            } else if (jenisdokumen == '3') {
                $('.pelanggan').hide();
                $('.barang').hide();
                $('.salesman').hide();
                $('.gdg').hide();
                $('.cbg').show();
                $('.tgl').show();
                $('#preview').show();
                $('#print').show();
                $('.merk').hide();
            } else if (jenisdokumen == '4') {
                $('.pelanggan').hide();
                $('.barang').hide();
                $('.salesman').hide();
                $('.gdg').hide();
                $('.cbg').show();
                $('.tgl').show();
                $('#preview').show();
                $('#print').show();
                $('.merk').hide();
            } else if (jenisdokumen == '5') {
                $('.pelanggan').show();
                $('.barang').show();
                $('.salesman').hide();
                $('.gdg').hide();
                $('.cbg').show();
                $('.tgl').show();
                $('#preview').hide();
                $('#print').show();
                $('.merk').hide();
            } else if (jenisdokumen == '6') {
                $('.pelanggan').hide();
                $('.barang').hide();
                $('.salesman').hide();
                $('.gdg').hide();
                $('.cbg').show();
                $('.tgl').show();
                $('#preview').hide();
                $('#print').show();
                $('.merk').hide();
            } else if (jenisdokumen == '7') {
                $('.pelanggan').hide();
                $('.barang').hide();
                $('.salesman').show();
                $('.gdg').hide();
                $('.cbg').show();
                $('.tgl').show();
                $('#preview').hide();
                $('#print').show();
                $('.merk').hide();
            } else if (jenisdokumen == '8') {
                $('.pelanggan').show();
                $('.barang').hide();
                $('.salesman').hide();
                $('.gdg').hide();
                $('.cbg').show();
                $('.tgl').show();
                $('#preview').hide();
                $('#print').show();
                $('.merk').hide();
            } else if (jenisdokumen == '9') {
                $('.pelanggan').hide();
                $('.barang').hide();
                $('.salesman').hide();
                $('.gdg').hide();
                $('.cbg').show();
                $('.tgl').show();
                $('#preview').show();
                $('#print').show();
                $('.merk').hide();
            } else if (jenisdokumen == '10') {
                $('.pelanggan').show();
                $('.barang').hide();
                $('.salesman').hide();
                $('.gdg').hide();
                $('.cbg').show();
                $('.tgl').show();
                $('#preview').hide();
                $('#print').show();
                $('.merk').hide();
            } else if (jenisdokumen == '11') {
                $('.pelanggan').hide();
                $('.barang').hide();
                $('.salesman').hide();
                $('.gdg').show();
                $('.cbg').show();
                $('.tgl').show();
                $('#preview').hide();
                $('#print').show();
                $('.merk').hide();
            } else if (jenisdokumen == '12') {
                $('.pelanggan').hide();
                $('.barang').hide();
                $('.salesman').hide();
                $('.gdg').hide();
                $('.cbg').show();
                $('.tgl').show();
                $('#preview').hide();
                $('#print').show();
                $('.merk').hide();
            } else if (jenisdokumen == '13') {
                $('.pelanggan').hide();
                $('.barang').hide();
                $('.salesman').hide();
                $('.gdg').hide();
                $('.cbg').show();
                $('.tgl').show();
                $('#preview').hide();
                $('#print').show();
                $('.merk').hide();
            } else if (jenisdokumen == '14') {
                $('.pelanggan').hide();
                $('.barang').hide();
                $('.salesman').hide();
                $('.gdg').hide();
                $('.cbg').show();
                $('.tgl').show();
                $('#preview').hide();
                $('#print').show();
                $('.merk').hide();
            } else if (jenisdokumen == '15') {
                $('.pelanggan').hide();
                $('.barang').hide();
                $('.salesman').hide();
                $('.gdg').hide();
                $('.cbg').show();
                $('.tgl').show();
                $('#preview').hide();
                $('#print').show();
                $('.merk').hide();
            } else if (jenisdokumen == '16') {
                $('.pelanggan').hide();
                $('.barang').hide();
                $('.salesman').show();
                $('.gdg').hide();
                $('.cbg').show();
                $('.tgl').show();
                $('#preview').hide();
                $('#print').show();
                $('.merk').hide();
            } else if (jenisdokumen == '18') {
                $('.pelanggan').hide();
                $('.barang').hide();
                $('.salesman').hide();
                $('.gdg').hide();
                $('.cbg').show();
                $('.tgl').show();
                $('#preview').show();
                $('#print').hide();
                $('.merk').hide();
            } else if (jenisdokumen == '19') {
                $('.pelanggan').hide();
                $('.barang').hide();
                $('.salesman').hide();
                $('.gdg').hide();
                $('.cbg').show();
                $('.tgl').show();
                $('#preview').show();
                $('#print').show();
                $('.merk').hide();
            } else if (jenisdokumen == '20') {
                $('.pelanggan').hide();
                $('.barang').hide();
                $('.salesman').hide();
                $('.gdg').hide();
                $('.cbg').show();
                $('.tgl').show();
                $('#preview').show();
                $('#print').hide();
                $('.merk').hide();
            } else if (jenisdokumen == '21') {
                $('.pelanggan').hide();
                $('.barang').hide();
                $('.salesman').hide();
                $('.gdg').hide();
                $('.cbg').show();
                $('.tgl').show();
                $('#preview').show();
                $('#print').hide();
                $('.merk').show();
            } else if (jenisdokumen == '22') {
                $('.pelanggan').hide();
                $('.barang').hide();
                $('.salesman').hide();
                $('.gdg').hide();
                $('.cbg').hide();
                $('.tgl').show();
                $('#preview').show();
                $('#print').show();
                $('.merk').hide();
            } else if (jenisdokumen == '23') {
                $('.pelanggan').hide();
                $('.barang').hide();
                $('.salesman').show();
                $('.gdg').hide();
                $('.cbg').show();
                $('.tgl').show();
                $('#preview').hide();
                $('.merk').hide();
            } else if (jenisdokumen == '24') {
                $('.pelanggan').hide();
                $('.barang').hide();
                $('.salesman').show();
                $('.gdg').hide();
                $('.cbg').show();
                $('.tgl').show();
                $('#preview').hide();
                $('.merk').hide();
            } else if (jenisdokumen == '25') {
                $('.pelanggan').hide();
                $('.barang').hide();
                $('.salesman').hide();
                $('.gdg').hide();
                $('.cbg').show();
                $('#kodecabang').val("");
                $('#carikodecabang').show();
                // $('.outstandingso').show();
                $('.tgl').hide();
                $('#preview').show();
                $('#print').show();
                $('.merk').hide();
            } else if (jenisdokumen == '26') {
                $('.pelanggan').hide();
                $('.barang').hide();
                $('.salesman').hide();
                $('.gdg').hide();
                $('.cbg').hide();
                $('.tgl').show();
                $('#preview').show();
                $('#print').show();
                $('.merk').hide();
            } else if (jenisdokumen == '27') {
                $('.pelanggan').hide();
                $('.barang').hide();
                $('.salesman').show();
                $('.gdg').hide();
                $('.cbg').show();
                $('.tgl').show();
                $('#preview').hide();
                $('.merk').hide();
            } else {
                $('.pelanggan').hide();
                $('.barang').hide();
                $('.salesman').hide();
                $('.gdg').hide();
                $('.cbg').hide();
                $('.tgl').hide();
                $('#preview').hide();
                $('.merk').hide();
            }
        })

        $('#merkawal').select2({
            placeholder: "Pilih Barang"
        })

        $('#merkakhir').select2({
            placeholder: "Pilih Barang"
        })

        $("#kodegudang").select2({
            placeholder: "Pilih Gudang"
        });

        // $("#nopelanggan").select2({
        //     placeholder: "Pilih Pelanggan"
        // });

        $("#kodebarang").select2({
            placeholder: "Pilih Barang"
        });

        $("#kodesalesman").select2({
            placeholder: "Pilih Salesman"
        });

        // function LoadPelanggan() {
        //     $.ajax({
        //         url: "<?= base_url('masterdata/Customer/DataPelanggan') ?>",
        //         method: "POST",
        //         dataType: "json",
        //         async: false,
        //         data: {
        //             cabang: $('#kodecabang').val()
        //         },
        //         success: function(data) {
        //             for (var i = 0; i < data.length; i++) {
        //                 $("#nopelanggan").append('<option value = ' + data[i].id + ' > Nama Pelanggan : ' + data[i].nama + ' ~ Nama Toko : ' + data[i].text + '</option>');
        //             }
        //         }
        //     }, false);
        // }

        function LoadGudang() {
            $.ajax({
                url: "<?= base_url('masterdata/Gudang/DataGdg') ?>",
                method: "POST",
                dataType: "json",
                async: false,
                data: {
                    cabang: $('#kodecabang').val()
                },
                success: function(data) {
                    for (var i = 0; i < data.length; i++) {
                        $("#kodegudang").append('<option value = ' + data[i].id + ' >' + data[i].text + '</option>');
                    }
                }
            }, false);
        }

        function LoadBarang() {
            $.ajax({
                url: "<?= base_url('masterdata/Barang/DataBrg') ?>",
                method: "POST",
                dataType: "json",
                async: false,
                data: {},
                success: function(data) {
                    for (var i = 0; i < data.length; i++) {
                        $("#kodebarang").append('<option value = ' + data[i].id + ' >' + data[i].text + '</option>');
                    }
                }
            }, false);
        }

        function LoadSalesman() {
            var cabang = $('#kodecabang').val();
            $.ajax({
                url: "<?= base_url('masterdata/Salesman/DataSlm') ?>",
                method: "POST",
                dataType: "json",
                async: false,
                data: {
                    cabang: cabang
                },
                success: function(data) {
                    for (var i = 0; i < data.length; i++) {
                        $("#kodesalesman").append('<option value = ' + data[i].id + ' >' + data[i].text + '</option>');
                    }
                }
            }, false);
        }

        function LoadMerkAwal() {
            $.ajax({
                url: "<?= base_url('masterdata/Merk/DataMerkSelect') ?>",
                method: "POST",
                dataType: "json",
                async: false,
                data: {},
                success: function(data) {
                    for (var i = 0; i < data.length; i++) {
                        $("#merkawal").append('<option value = ' + data[i].id + ' >' + data[i].nama + '</option>');
                    }
                }
            }, false);
        }

        function LoadMerkAkhir() {
            $.ajax({
                url: "<?= base_url('masterdata/Merk/DataMerkSelectAkhir') ?>",
                method: "POST",
                dataType: "json",
                async: false,
                data: {},
                success: function(data) {
                    for (var i = 0; i < data.length; i++) {
                        $("#merkakhir").append('<option value = ' + data[i].id + ' >' + data[i].nama + '</option>');
                    }
                }
            }, false);
        }

        function LoadData() {
            $('#kodecabang').val("<?= $this->session->userdata('mycabang'); ?>");
            $('.pelanggan').hide();
            $('.barang').hide();
            $('.salesman').hide();
            $('.cbg').hide();
            $('.tgl').hide();
            $('.gdg').hide();
            $('.merk').hide();
            $("#kodegudang").empty();
            LoadGudang();
            $("#kodegudang").val("");
            $("#nopelanggan").empty();
            // LoadPelanggan();
            $("#nopelanggan").val("");
            $("#kodebarang").empty();
            LoadBarang();
            $("#kodebarang").val("");
            $("#kodesalesman").empty();
            LoadSalesman();
            $("#kodesalesman").val("");
            $('#carikodecabang').show();
            $('#preview').hide();
            LoadMerkAkhir();
            LoadMerkAwal();
        }

        /* Cari Data Cabang*/
        document.getElementById("carikodecabang").addEventListener("click", function(event) {
            event.preventDefault();
            $('#t_cabang').DataTable({
                "destroy": true,
                "searching": true,
                "processing": true,
                "serverSide": true,
                "lengthChange": true,
                "pageLength": 5,
                "lengthMenu": [5, 10, 25, 50],
                "order": [],
                "ajax": {
                    "url": "<?= base_url('masterdata/Gudang/CariDataCabang'); ?>",
                    "method": "POST",
                    "data": {
                        nmtb: "glbm_cabang",
                        field: {
                            kode: "kode",
                            nama: "nama",
                            alamat: "alamat"
                        },
                        sort: "kode",
                        where: {
                            kode: "kode",
                            nama: "nama",
                            alamat: "alamat"
                        },
                        value: "aktif = true"
                    },
                }
            });
        });

        /*Get Data Cabang*/
        $(document).on('click', ".searchcabang", function() {
            var kodecabang = $(this).attr("data-id");;
            $.ajax({
                url: "<?= base_url('masterdata/Gudang/DataCabang'); ?>",
                method: "POST",
                dataType: "json",
                async: false,
                data: {
                    kodecabang: kodecabang
                },
                success: function(data) {
                    for (var i = 0; i < data.length; i++) {
                        $('#kodecabang').val(data[i].kodecabang.trim());
                    }
                }
            }, false);
        });

         /* Cari Data Cabang*/
         document.getElementById("caripelanggan").addEventListener("click", function(event) {
            event.preventDefault();
            $('#t_pelanggan').DataTable({
                "destroy": true,
                "searching": true,
                "processing": true,
                "serverSide": true,
                "lengthChange": true,
                "pageLength": 5,
                "lengthMenu": [5, 10, 25, 50],
                "order": [],
                "ajax": {
                    "url": "<?= base_url('masterdata/Customer/CariDataCustomer'); ?>",
                    "method": "POST",
                    "data": {
                        nmtb: "glbm_customer",
                        field: {
                            nomor: "nomor",
                            nama: "nama",
                            nama_toko: "nama_toko"
                        },
                        sort: "nomor",
                        where: {
                            nomor: "nomor",
                            nama: "nama",
                            nama_toko: "nama_toko"
                        },
                        value: "aktif = true AND kodecabang = '"+$('#kodecabang').val()+"'"
                    },
                }
            });
        });

        /*Get Data Cabang*/
        $(document).on('click', ".searchcustomer", function() {
            var nomor = $(this).attr("data-id");;
            $.ajax({
                url: "<?= base_url('masterdata/Customer/DataCustomer'); ?>",
                method: "POST",
                dataType: "json",
                async: false,
                data: {
                    nomor: nomor
                },
                success: function(data) {
                    for (var i = 0; i < data.length; i++) {
                        $('#nopelanggan').val(data[i].nomor.trim());
                        $('#namapelanggan').val(data[i].nama.trim());
                    }
                }
            }, false);
        });

        document.getElementById("print").addEventListener("click", function(event) {
            event.preventDefault();
            var tglawal = $('#tglawal').val();
            var tglakhir = $('#tglakhir').val();
            var cabang = $('#kodecabang').val();
            var gudang = $('#kodegudang').val();
            var barang = $('#kodebarang').val();
            var salesman = $('#kodesalesman').val();
            var nopelanggan = $('#nopelanggan').val();
            var username = $('#username').val();
            var merkawal = $('#merkawal').val();
            var merkakhir = $('#merkakhir').val();
            var jenislaporan = $('#jenis_laporan').val();

            if (jenislaporan == '1') {
                window.open(
                    "<?php echo base_url('cetak_pdf/Laporan/ReturPerTanggal/') ?>" + tglawal + ":" + tglakhir + ":" + cabang
                );
            } else if (jenislaporan == '2') {
                window.open(
                    "<?php echo base_url('cetak_pdf/Laporan/PenjualanPerPelanggan/') ?>" + tglawal + ":" + tglakhir + ":" + nopelanggan + ":" + cabang
                );
            } else if (jenislaporan == '3') {
                window.open(
                    "<?php echo base_url('cetak_pdf/Laporan/SalesOrder/') ?>" + tglawal + ":" + tglakhir + ":" + cabang
                );
            } else if (jenislaporan == '4') {
                window.open(
                    "<?php echo base_url('cetak_pdf/Laporan/PenjualanSemuaPelanggan/') ?>" + tglawal + ":" + tglakhir + ":" + cabang
                );
            } else if (jenislaporan == "5") {
                window.open(
                    "<?= base_url('cetak_pdf/Laporan/PenjualanPerPelangganPerBarang/'); ?>" + tglawal + ":" + tglakhir + ":" + nopelanggan + ":" + cabang + ":" + barang
                )
            } else if (jenislaporan == '6') {
                window.open(
                    "<?php echo base_url('cetak_pdf/Laporan/PenjualanSemuaSalesman/') ?>" + tglawal + ":" + tglakhir + ":" + cabang
                );
            } else if (jenislaporan == '7') {
                window.open(
                    "<?php echo base_url('cetak_pdf/Laporan/PenjualanPerSalesman/') ?>" + tglawal + ":" + tglakhir + ":" + cabang + ":" + salesman
                );
            } else if (jenislaporan == '8') {
                window.open(
                    "<?php echo base_url('cetak_pdf/Laporan/RekapPenjualanPerPelanggan/') ?>" + tglawal + ":" + tglakhir + ":" + nopelanggan + ":" + cabang
                );
            } else if (jenislaporan == '9') {
                window.open(
                    "<?php echo base_url('cetak_pdf/Laporan/SalesOrderDetail/') ?>" + tglawal + ":" + tglakhir + ":" + cabang
                );
            } else if (jenislaporan == '10') {
                window.open(
                    "<?php echo base_url('cetak_pdf/Laporan/PenjualanDetilPerPelanggan/') ?>" + tglawal + ":" + tglakhir + ":" + cabang + ":" + nopelanggan
                );
            } else if (jenislaporan == '11') {
                window.open(
                    "<?php echo base_url('cetak_pdf/Laporan/RekapPenjualanPerGudang/') ?>" + tglawal + ":" + tglakhir + ":" + gudang
                );
            } else if (jenislaporan == '12') {
                window.open(
                    "<?php echo base_url('cetak_pdf/Laporan/ReturDetail/') ?>" + tglawal + ":" + tglakhir + ":" + cabang
                );
            } else if (jenislaporan == '13') {
                window.open(
                    "<?php echo base_url('cetak_pdf/Laporan/RekapPenjualanPelanggan/') ?>" + tglawal + ":" + tglakhir + ":" + cabang
                );
            } else if (jenislaporan == '14') {
                window.open(
                    "<?php echo base_url('cetak_pdf/Laporan/RekapRetur/') ?>" + tglawal + ":" + tglakhir + ":" + cabang
                );
            } else if (jenislaporan == '15') {
                window.open(
                    "<?php echo base_url('cetak_pdf/Laporan/PenjualanPromo/') ?>" + tglawal + ":" + tglakhir + ":" + cabang
                );
            } else if (jenislaporan == '16') {
                window.open(
                    "<?php echo base_url('cetak_pdf/Laporan/KomisiSalesNett/') ?>" + tglawal + ":" + tglakhir + ":" + cabang + ":" + salesman
                );
            } else if (jenislaporan == '17') {
                window.open(
                    "<?php echo base_url('cetak_pdf/Laporan/KomisiSalesPerPelanggan/') ?>" + tglawal + ":" + tglakhir + ":" + cabang + ":" + salesman
                );
            } else if (jenislaporan == '19') {
                window.open(
                    "<?php echo base_url('cetak_pdf/Laporan/LaporanRekapPenjualan/') ?>" + tglawal + ":" + tglakhir + ":" + cabang
                );
            } else if (jenislaporan == '22') {
                window.open(
                    "<?php echo base_url('cetak_pdf/Laporan/LaporanRekapPenjualanAllCabang/') ?>" + tglawal + ":" + tglakhir
                );
            } else if (jenislaporan == '23') {
                window.open(
                    "<?php echo base_url('cetak_pdf/Laporan/KomisiSalesProyeksi/') ?>" + tglawal + ":" + tglakhir + ":" + cabang + ":" + salesman
                );
            } else if (jenislaporan == '24') {
                window.open(
                    "<?php echo base_url('cetak_pdf/Laporan/KomisiSalesHangus/') ?>" + tglawal + ":" + tglakhir + ":" + cabang + ":" + salesman
                );
            } else if (jenislaporan == '25') {
                window.open(
                    "<?php echo base_url('cetak_pdf/Laporan/OutstandingSalesOrder/') ?>" + cabang
                );
            } else if (jenislaporan == '26') {
                window.open(
                    "<?php echo base_url('cetak_pdf/Laporan/LaporanPenjualanDetailAllCabang/') ?>" + tglawal + ":" + tglakhir
                );
            } else if (jenislaporan == '27') {
                window.open(
                    "<?php echo base_url('cetak_pdf/Laporan/KomisiSales/') ?>" + tglawal + ":" + tglakhir + ":" + cabang + ":" + salesman
                );
            }

            // RefreshLayar();
        });
        document.getElementById("preview").addEventListener("click", function(event) {
            event.preventDefault();
            var tglawal = $('#tglawal').val();
            var tglakhir = $('#tglakhir').val();
            var cabang = $('#kodecabang').val();
            var gudang = $('#kodegudang').val();
            var barang = $('#kodebarang').val();
            var salesman = $('#kodesalesman').val();
            var nopelanggan = $('#nopelanggan').val();
            var username = $('#username').val();
            var merkawal = $('#merkawal').val();
            var merkakhir = $('#merkakhir').val();
            var jenislaporan = $('#jenis_laporan').val();

            if (jenislaporan == '1') {
                window.open(
                    "<?php echo base_url('cetak_pdf/Laporan/ReturPerTanggal/') ?>" + tglawal + ":" + tglakhir + ":" + cabang
                );
            } else if (jenislaporan == '2') {
                window.open(
                    "<?php echo base_url('cetak_pdf/Laporan/PenjualanPerPelanggan/') ?>" + tglawal + ":" + tglakhir + ":" + nopelanggan + ":" + cabang
                );
            } else if (jenislaporan == '3') {
                window.open(
                    "<?php echo base_url('cetak_pdf/Laporan/PreviewSalesOrder/') ?>" + tglawal + ":" + tglakhir + ":" + cabang
                );
            } else if (jenislaporan == '4') {
                window.open(
                    "<?php echo base_url('cetak_pdf/Laporan/PreviewPenjualanSemuaPelanggan/') ?>" + tglawal + ":" + tglakhir + ":" + cabang
                );
            } else if (jenislaporan == "5") {
                window.open(
                    "<?= base_url('cetak_pdf/Laporan/PenjualanPerPelangganPerBarang/'); ?>" + tglawal + ":" + tglakhir + ":" + nopelanggan + ":" + cabang + ":" + barang
                )
            } else if (jenislaporan == '6') {
                window.open(
                    "<?php echo base_url('cetak_pdf/Laporan/PenjualanSemuaSalesman/') ?>" + tglawal + ":" + tglakhir + ":" + cabang
                );
            } else if (jenislaporan == '7') {
                window.open(
                    "<?php echo base_url('cetak_pdf/Laporan/PenjualanPerSalesman/') ?>" + tglawal + ":" + tglakhir + ":" + cabang + ":" + salesman
                );
            } else if (jenislaporan == '8') {
                window.open(
                    "<?php echo base_url('cetak_pdf/Laporan/RekapPenjualanPerPelanggan/') ?>" + tglawal + ":" + tglakhir + ":" + nopelanggan + ":" + cabang
                );
            } else if (jenislaporan == '9') {
                window.open(
                    "<?php echo base_url('cetak_pdf/Laporan/PreviewSalesOrderDetail/') ?>" + tglawal + ":" + tglakhir + ":" + cabang
                );
            } else if (jenislaporan == '10') {
                window.open(
                    "<?php echo base_url('cetak_pdf/Laporan/PenjualanDetilPerPelanggan/') ?>" + tglawal + ":" + tglakhir + ":" + cabang + ":" + nopelanggan
                );
            } else if (jenislaporan == '11') {
                window.open(
                    "<?php echo base_url('cetak_pdf/Laporan/RekapPenjualanPerGudang/') ?>" + tglawal + ":" + tglakhir + ":" + gudang
                );
            } else if (jenislaporan == '12') {
                window.open(
                    "<?php echo base_url('cetak_pdf/Laporan/ReturDetail/') ?>" + tglawal + ":" + tglakhir + ":" + cabang
                );
            } else if (jenislaporan == '13') {
                window.open(
                    "<?php echo base_url('cetak_pdf/Laporan/RekapPenjualanPelanggan/') ?>" + tglawal + ":" + tglakhir + ":" + cabang
                );
            } else if (jenislaporan == '14') {
                window.open(
                    "<?php echo base_url('cetak_pdf/Laporan/RekapRetur/') ?>" + tglawal + ":" + tglakhir + ":" + cabang
                );
            } else if (jenislaporan == '15') {
                window.open(
                    "<?php echo base_url('cetak_pdf/Laporan/PenjualanPromo/') ?>" + tglawal + ":" + tglakhir + ":" + cabang
                );
            } else if (jenislaporan == '16') {
                window.open(
                    "<?php echo base_url('cetak_pdf/Laporan/KomisiSalesNett/') ?>" + tglawal + ":" + tglakhir + ":" + cabang + ":" + salesman
                );
            } else if (jenislaporan == '17') {
                window.open(
                    "<?php echo base_url('cetak_pdf/Laporan/KomisiSalesPerPelanggan/') ?>" + tglawal + ":" + tglakhir + ":" + cabang + ":" + salesman
                );
            } else if (jenislaporan == '18') {
                window.open(
                    "<?php echo base_url('cetak_pdf/Laporan/PreviewLaporanPenjualanDetail/') ?>" + tglawal + ":" + tglakhir + ":" + cabang
                );
            } else if (jenislaporan == '19') {
                window.open(
                    "<?php echo base_url('cetak_pdf/Laporan/PreviewLaporanRekapPenjualan/') ?>" + tglawal + ":" + tglakhir + ":" + cabang
                );
            } else if (jenislaporan == '20') {
                window.open(
                    "<?php echo base_url('cetak_pdf/Laporan/PreviewLaporanPenjualanPertanggal/') ?>" + tglawal + ":" + tglakhir + ":" + cabang
                );
            } else if (jenislaporan == '21') {
                window.open(
                    "<?php echo base_url('cetak_pdf/Laporan/PreviewLaporanPenjualanPerkategori/') ?>" + tglawal + ":" + tglakhir + ":" + cabang + ":" + merkawal + ":" + merkakhir
                );
            } else if (jenislaporan == '22') {
                window.open(
                    "<?php echo base_url('cetak_pdf/Laporan/PreviewLaporanRekapPenjualanAllCabang/') ?>" + tglawal + ":" + tglakhir
                );
            } else if (jenislaporan == '25') {
                window.open(
                    "<?php echo base_url('cetak_pdf/Laporan/PreviewOutstandingSalesOrder/') ?>" + cabang
                );
            } else if (jenislaporan == '26') {
                window.open(
                    "<?php echo base_url('cetak_pdf/Laporan/PreviewLaporanPenjualanDetailAllCabang/') ?>" + tglawal + ":" + tglakhir
                );
            }

            // RefreshLayar();
        });

        document.getElementById("export").addEventListener("click", function(event) {
            event.preventDefault();
            var tglawal = $('#tglawal').val();
            var tglakhir = $('#tglakhir').val();
            var cabang = $('#kodecabang').val();
            var gudang = $('#kodegudang').val();
            var barang = $('#kodebarang').val();
            var salesman = $('#kodesalesman').val();
            var nopelanggan = $('#nopelanggan').val();
            var username = $('#username').val();
            var jenislaporan = $('#jenis_laporan').val();
            var merkawal = $('#merkawal').val();
            var merkakhir = $('#merkakhir').val();

            if (jenislaporan == '1') {
                window.open(
                    "<?= base_url('cetak_pdf/Laporan/ExportReturPerTanggal/'); ?>" + tglawal + ":" + tglakhir + ":" + cabang
                )
            } else if (jenislaporan == '2') {
                window.open(
                    "<?= base_url('cetak_pdf/Laporan/ExportPenjualanPerPelanggan/'); ?>" + tglawal + ":" + tglakhir + ":" + nopelanggan + ":" + cabang
                )
            } else if (jenislaporan == '3') {
                window.open(
                    "<?= base_url('cetak_pdf/Laporan/ExportSalesOrder/'); ?>" + tglawal + ":" + tglakhir + ":" + cabang
                )
            } else if (jenislaporan == '4') {
                window.open(
                    "<?= base_url('cetak_pdf/Laporan/ExportPenjualanSemuaPelanggan/'); ?>" + tglawal + ":" + tglakhir + ":" + cabang
                )
            } else if (jenislaporan == '5') {
                window.open(
                    "<?= base_url('cetak_pdf/Laporan/ExportPenjualanPerPelangganPerBarang/'); ?>" + tglawal + ":" + tglakhir + ":" + nopelanggan + ":" + cabang + ":" + barang
                )
            } else if (jenislaporan == '6') {
                window.open(
                    "<?= base_url('cetak_pdf/Laporan/ExportPenjualanSemuaSalesman/'); ?>" + tglawal + ":" + tglakhir + ":" + cabang
                )
            } else if (jenislaporan == '7') {
                window.open(
                    "<?= base_url('cetak_pdf/Laporan/ExportPenjualanPerSalesman/'); ?>" + tglawal + ":" + tglakhir + ":" + cabang + ":" + salesman
                )
            } else if (jenislaporan == '8') {
                window.open(
                    "<?php echo base_url('cetak_pdf/Laporan/ExportRekapPenjualanPerPelanggan/') ?>" + tglawal + ":" + tglakhir + ":" + nopelanggan + ":" + cabang
                );
            } else if (jenislaporan == '9') {
                window.open(
                    "<?= base_url('cetak_pdf/Laporan/ExportSalesOrderDetail/'); ?>" + tglawal + ":" + tglakhir + ":" + cabang
                )
            } else if (jenislaporan == '10') {
                window.open(
                    "<?= base_url('cetak_pdf/Laporan/ExportPenjualanDetilPerPelanggan/'); ?>" + tglawal + ":" + tglakhir + ":" + cabang + ":" + nopelanggan
                )
            } else if (jenislaporan == '11') {
                window.open(
                    "<?= base_url('cetak_pdf/Laporan/ExportRekapPenjualanPerGudang/'); ?>" + tglawal + ":" + tglakhir + ":" + gudang
                )
            } else if (jenislaporan == '12') {
                window.open(
                    "<?= base_url('cetak_pdf/Laporan/ExportReturDetail/'); ?>" + tglawal + ":" + tglakhir + ":" + cabang
                )
            } else if (jenislaporan == '13') {
                window.open(
                    "<?= base_url('cetak_pdf/Laporan/ExportRekapPenjualanPelanggan/'); ?>" + tglawal + ":" + tglakhir + ":" + cabang
                )
            } else if (jenislaporan == '14') {
                window.open(
                    "<?= base_url('cetak_pdf/Laporan/ExportRekapRetur/'); ?>" + tglawal + ":" + tglakhir + ":" + cabang
                )
            } else if (jenislaporan == '15') {
                window.open(
                    "<?= base_url('cetak_pdf/Laporan/ExportPenjualanPromo/'); ?>" + tglawal + ":" + tglakhir + ":" + cabang
                )
            } else if (jenislaporan == '16') {
                window.open(
                    "<?php echo base_url('cetak_pdf/Laporan/ExportKomisiSalesNett/') ?>" + tglawal + ":" + tglakhir + ":" + cabang + ":" + salesman
                );
            } else if (jenislaporan == '17') {
                window.open(
                    "<?php echo base_url('cetak_pdf/Laporan/ExportKomisiSalesPerPelanggan/') ?>" + tglawal + ":" + tglakhir + ":" + cabang + ":" + salesman
                );
            } else if (jenislaporan == '18') {
                window.open(
                    "<?= base_url('cetak_pdf/Laporan/ExportPenjualanDetail/'); ?>" + tglawal + ":" + tglakhir + ":" + cabang
                )
            } else if (jenislaporan == '19') {
                window.open(
                    "<?= base_url('cetak_pdf/Laporan/ExportRekapPenjualan/'); ?>" + tglawal + ":" + tglakhir + ":" + cabang
                )
            } else if (jenislaporan == '22') {
                window.open(
                    "<?php echo base_url('cetak_pdf/Laporan/ExportRekapPenjualanAllCabang/') ?>" + tglawal + ":" + tglakhir
                );
            } else if (jenislaporan == '23') {
                window.open(
                    "<?php echo base_url('cetak_pdf/Laporan/ExportKomisiSalesProyeksi/') ?>" + tglawal + ":" + tglakhir + ":" + cabang + ":" + salesman
                );
            } else if (jenislaporan == '24') {
                window.open(
                    "<?php echo base_url('cetak_pdf/Laporan/ExportKomisiSalesHangus/') ?>" + tglawal + ":" + tglakhir + ":" + cabang + ":" + salesman
                );
            } else if (jenislaporan == '25') {
                window.open(
                    "<?php echo base_url('cetak_pdf/Laporan/ExportOutstandingSalesOrder/') ?>" + cabang
                );
            } else if (jenislaporan == '26') {
                window.open(
                    "<?php echo base_url('cetak_pdf/Laporan/ExportLaporanPenjualanDetailAllCabang/') ?>" + tglawal + ":" + tglakhir
                );
            } else if (jenislaporan == '21') {
                window.open(
                    "<?php echo base_url('cetak_pdf/Laporan/ExportPenjualanPerkategori/') ?>" + tglawal + ":" + tglakhir + ":" + cabang + ":" + merkawal + ":" + merkakhir
                );
            }else if (jenislaporan == '27') {
                window.open(
                    "<?php echo base_url('cetak_pdf/Laporan/ExportKomisiSales/') ?>" + tglawal + ":" + tglakhir + ":" + cabang + ":" + salesman
                );
            }

            // RefreshLayar();
        })

        function RefreshLayar() {
            location.reload(true);
        };

        document.getElementById("clear").addEventListener("click", function(event) {
            event.preventDefault();
            RefreshLayar();
            LoadData();
        });

        LoadData();
    });
</script>