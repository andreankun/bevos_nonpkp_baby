<?php

header("Content-type: application/vnd-ms-excel");

header("Content-Disposition: attachment; filename=Laporan Komisi Sales Hangus.xls");

header("Pragma: no-cache");

header("Expires: 0");

function FormatRupiah($angka)
{
    $hasil_rupiah = number_format($angka, 0, '.', ',');
    return $hasil_rupiah;
}
?>
<h4 style="text-align: center;">LAPORAN KOMISI SALES HANGUS</h4>
CABANG : <?php echo $row->nama ?>
<br>
PERIODE : <?php echo $tglawal ?> S/D <?php echo $tglakhir ?>
<br>
<?php if (!empty($reportrow)) : ?>
    NAMA SALES : <?php echo $reportrow->namasalesman ?>
<?php else : ?>
    NAMA SALES : -
<?php endif; ?>
<br>
<table border="1" width="100%">

    <thead>
        <tr>
            <th>Tanggal</th>
            <th>Customer</th>
            <th>No. Faktur</th>
            <th>Kode</th>
            <th>Nama Produk</th>
            <th>Qty</th>
            <th>Harga</th>
            <th>Diskon</th>
            <th>Total</th>
            <th>Komisi</th>
            <th>Persen</th>
        </tr>
    </thead>

    <tbody>
        <?php if (!empty($detail)) : ?>
            <?php
            $grandtotal = 0;
            $grandtotalkomisi = 0;
            foreach ($detail as $val) :
                $nilaicash = 0;
                $nilaicustomer = 0;
                $promo = 0;
                $barangitem = 0;
                $totaldiskon = 0;
                $total = 0;
                $barangitem = $barangitem + $val->discbrgitem;
                $promo = $promo + $val->discpaketitem;
                $nilaicustomer = $nilaicustomer + $val->nilaicustomer;
                $nilaicash = $nilaicash + $val->nilaicash;
                // $totaldiskon = $totaldiskon + $barangitem + $promo + $nilaicustomer + $nilaicash;
                $totaldiskon = $totaldiskon + $barangitem + $promo;
                $total = $total  + (($val->qty * $val->harga) - $totaldiskon);
            ?>
                <tr>
                    <td><?= date('d-m-Y', strtotime($val->tanggal)) ?></td>
                    <td><?= $val->namapelanggan ?></td>
                    <td><?= $val->nomor ?></td>
                    <td><?= $val->kodebarang ?></td>
                    <td><?= $val->namabarang ?></td>
                    <td style="text-align: right;"><?= FormatRupiah($val->qty) ?></td>
                    <td style="text-align: right;"><?= FormatRupiah($val->harga) ?></td>
                    <td style="text-align: right;"><?= FormatRupiah($totaldiskon) ?></td>
                    <td style="text-align: right;"><?= FormatRupiah($total) ?></td>
                    <td style="text-align: right;"><?= FormatRupiah($total * ($val->komisi / 100)) ?></td>
                    <td style="text-align: center;"><?= $val->komisi ?></td>
                </tr>
            <?php
                $grandtotal = $grandtotal + $total;
                $grandtotalkomisi = $grandtotalkomisi + ($total * ($val->komisi / 100));
            endforeach;
            ?>
            <tr>
                <td colspan="8" style="text-align: right; font-weight: bold;">Grand Total :</td>
                <td style="text-align: right;"><?= FormatRupiah($grandtotal) ?></td>
                <td style="text-align: right;"><?= FormatRupiah($grandtotalkomisi) ?></td>
            </tr>
        <?php else : ?>
            <tr>
                <td colspan="11"></td>
            </tr>
        <?php endif; ?>
    </tbody>
</table>