<?php

header("Content-type: application/vnd-ms-excel");

header("Content-Disposition: attachment; filename=Laporan Rekap Penjualan Per Pelanggan .xls");

header("Pragma: no-cache");

header("Expires: 0");
?>
<h4 style="text-align: center;">LAPORAN REKAP PENJUALAN PER PELANGGAN</h4>
CABANG : <?php echo $rekappenjualanperpelangganrow->namacabang ?>
<br>
PERIODE : <?php echo $tglawal ?> S/D <?php echo $tglakhir ?>
<br>
PELANGGAN : <?php echo $rekappenjualanperpelangganrow->namapelanggan ?>
<br>
NAMA TOKO : <?php echo $rekappenjualanperpelangganrow->nama_toko ?>
<br>
<table border="1" width="100%">

    <thead>
        <tr>
            <th>NO.</th>
            <th>PEMBAYARAN</th>
            <th>INVOICE</th>
            <th>NAMA PELANGGAN</th>
            <th>NAMA TOKO</th>
            <th>TANGGAL</th>
            <th>SUBTOTAL</th>
            <th>DISKON</th>
            <th>DPP</th>
            <th>PPN</th>
            <th>TOTAL</th>
        </tr>
    </thead>

    <tbody>
        <?php $no = 1;
        $gsubtotal = 0;
        $gdiskon = 0;
        $gdpp = 0;
        $gppn = 0;
        $ggrandtotal = 0;
        $totals_diskon = 0;
        $subtotals = 0;
        foreach ($rekappenjualanperpelanggan as $value) :
            $subtotals = $value->subtotal;
            $totals_diskon = $value->nilaicustomer + $value->nilaicash;
        ?>
            <tr>
                <td style="text-align: center;"><?= $no++; ?></td>

                <td style="text-align: center;">
                    <?php if ($value->jenisjual == '1' || $value->jenisjual == '3' || $value->jenisjual == '4') : ?>
                        TUNAI
                    <?php elseif ($value->jenisjual == '2') : ?>
                        KREDIT
                    <?php endif; ?>
                </td>
                <td style="text-align: left;"><?= $value->nomor ?></td>
                <td style="text-align: left;"><?= $value->namapelanggan ?></td>
                <td style="text-align: left;"><?= $value->nama_toko ?></td>
                <td style="text-align: right;"><?= date('d/m/Y', strtotime($value->tanggal)) ?></td>
                <td style="text-align: right;"><?= $subtotals ?></td>
                <td style="text-align: right;"><?= $totals_diskon ?></td>
                <td style="text-align: right;"><?= $value->dpp ?></td>
                <?php $hasilppn = 0;
                $grandtotal = 0;
                $hasilppn = floor($value->dpp * ($ppn->ppn / 100));
                $grandtotal = $value->dpp + $hasilppn; ?>
                <td style="text-align: right;"><?= $hasilppn ?></td>
                <td style="text-align: right;"><?= $grandtotal ?></td>
            </tr>
            <?php $gsubtotal = $gsubtotal + $subtotals; ?>
            <?php $gdiskon = $gdiskon + $totals_diskon; ?>
            <?php $gdpp = $gdpp + $value->dpp; ?>
            <?php $gppn = $gppn + $hasilppn; ?>
            <?php $ggrandtotal = $ggrandtotal + $grandtotal; ?>
        <?php endforeach; ?>
        <tr style="float: right; text-align: right; font-weight: bold;">
            <td colspan="6">Grand Total : </td>
            <td colspan="1"><?= $gsubtotal ?></td>
            <td colspan="1"><?= $gdiskon ?></td>
            <td colspan="1"><?= $gdpp ?></td>
            <td colspan="1"><?= $gppn ?></td>
            <td colspan="1"><?= $ggrandtotal ?></td>
        </tr>
    </tbody>
</table>