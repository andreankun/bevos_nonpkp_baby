<?php

header("Content-type: application/vnd-ms-excel");

header("Content-Disposition: attachment; filename=Laporan Penjualan Semua Salesman.xls");

header("Pragma: no-cache");

header("Expires: 0");

function FormatRupiah($angka)
{
    $hasil_rupiah = number_format($angka, 0, '.', ',');
    return $hasil_rupiah;
}
?>
<h4 style="text-align: center;">LAPORAN PENJUALAN SEMUA SALESMAN</h4>
CABANG : <?php echo $row->nama ?>
<br>
PERIODE : <?php echo $tglawal ?> S/D <?php echo $tglakhir ?>
<br>
<table border="1" width="100%">

    <thead>
        <tr>
            <th>NO.</th>
            <th>FAKTUR</th>
            <th>TANGGAL</th>
            <th>SALES ORDER</th>
            <th>PELANGGAN</th>
            <th>TOTAL PENJUALAN</th>
            <th>PEMBAYARAN</th>
            <th>SALDO</th>
        </tr>
    </thead>

    <tbody>
        <?php
        foreach ($reportlist as $value) :
        ?>
            <tr>
                <td colspan="8" style="font-size: 14px; font-weight: bold;"><?= $value->namasalesman ?></td>
            </tr>
            <?php $kodesalesman = $value->kodesalesman; ?>
            <?php
            $no = 1;
            $total = 0;
            $grandtotal = 0;
            foreach ($report as $val) : ?>
                <?php if ($val->kodesalesman == $kodesalesman) : ?>
                    <tr>
                        <td style="text-align: center;"><?= $no++; ?></td>
                        <td style="text-align: left;"><?= $val->nomor ?></td>
                        <td style="text-align: right;"><?= date('d-m-Y', strtotime($val->tanggal)) ?></td>
                        <td style="text-align: left;"><?= $val->nomorso ?></td>
                        <td style="text-align: left;"><?= $val->namapelanggan ?></td>
                        <td style="text-align: right;"><?= FormatRupiah($val->grandtotal) ?></td>
                        <td></td>
                        <td style="text-align: right;"><?= FormatRupiah($val->grandtotal) ?></td>
                    </tr>
                    <?php $total = $total + $val->grandtotal; ?>
                <?php endif; ?>
                <?php $grandtotal = $grandtotal + $val->grandtotal; ?>
            <?php endforeach; ?>
            <tr style="float: right; text-align: right; font-weight: bold;">
                <td colspan="5">Total : </td>
                <td colspan="1"><?= FormatRupiah($total) ?></td>
                <td colspan="1"></td>
                <td colspan="1"><?= FormatRupiah($total) ?></td>
            </tr>
        <?php endforeach; ?>
        <tr style="float: right; text-align: right; font-weight: bold;">
            <td colspan="5">Grand Total : </td>
            <td colspan="1"><?= FormatRupiah($grandtotal) ?></td>
            <td colspan="1"></td>
            <td colspan="1"><?= FormatRupiah($grandtotal) ?></td>
        </tr>
    </tbody>
</table>