<?php

header("Content-type: application/vnd-ms-excel");

header("Content-Disposition: attachment; filename=Laporan Rekap Penjualan Semua Pelanggan.xls");

header("Pragma: no-cache");

header("Expires: 0");

function FormatRupiah($angka)
{
    $hasil_rupiah = number_format($angka, 0, ',', '.');
    return $hasil_rupiah;
}
?>
<h4 style="text-align: center;">LAPORAN REKAP PENJUALAN SEMUA PELANGGAN</h4>
CABANG : <?php echo $row->nama ?>
<br>
PERIODE : <?php echo $tglawal ?> S/D <?php echo $tglakhir ?>
<br>
<table width="100%">

    <thead>
        <tr>
            <th style="border: 1px solid #333; border-width:thin;">NO.</th>
            <th style="border: 1px solid #333; border-width:thin;">INVOICE</th>
            <th style="border: 1px solid #333; border-width:thin;">NAMA PELANGGAN</th>
            <th style="border: 1px solid #333; border-width:thin;">NAMA TOKO</th>
            <th style="border: 1px solid #333; border-width:thin;">TANGGAL</th>
            <th style="border: 1px solid #333; border-width:thin;">SUBTOTAL</th>
            <th style="border: 1px solid #333; border-width:thin;">DISKON</th>
            <th style="border: 1px solid #333; border-width:thin;">DPP</th>
            <th style="border: 1px solid #333; border-width:thin;">PPN</th>
            <th style="border: 1px solid #333; border-width:thin;">TOTAL</th>
        </tr>
    </thead>

    <tbody>
        <?php
        $no = 1;
        $subtotal = 0;
        $grandtotal = 0;
        $total_diskon = 0;
        $dpp = 0;
        $hasilppn = 0;
        $hsubtotal = 0;
        $htotal_diskon = 0;
        $hdpp = 0;
        $hhasilppn = 0;
        $hgrandtotal = 0;
        foreach ($report as $vals) :
            $subtotal = $vals->subtotal - $vals->discbrgitem;
            $total_diskon = $vals->discpaketitem + $vals->nilaicash + $vals->nilaicustomer;
            $dpp = $vals->dpp;
            // print_r($ppn->ppn);
            // die();
            $hasilppn = floor($vals->dpp * ($ppn->ppn / 100));
            $grandtotal = $vals->dpp + $hasilppn;
        ?>
            <tr>
                <td style="text-align: center; border: 1px solid #dbdbdb;border-width: thin;"><?= $no++; ?></td>
                <td style="text-align: left; border: 1px solid #dbdbdb;border-width: thin;"><?= $vals->nomor ?></td>
                <td style="text-align: left; border: 1px solid #dbdbdb;border-width: thin;"><?= $vals->namapelanggan ?></td>
                <td style="text-align: left; border: 1px solid #dbdbdb;border-width: thin;"><?= $vals->nama_toko ?></td>
                <td style="text-align: right; border: 1px solid #dbdbdb;border-width: thin;"><?= date('d/m/Y', strtotime($vals->tanggal)) ?></td>
                <td style="text-align: right; border: 1px solid #dbdbdb;border-width: thin;"><?= $subtotal ?></td>
                <td style="text-align: right; border: 1px solid #dbdbdb;border-width: thin;"><?= $total_diskon ?></td>
                <td style="text-align: right; border: 1px solid #dbdbdb;border-width: thin;"><?= $dpp?></td>
                <td style="text-align: right; border: 1px solid #dbdbdb;border-width: thin;"><?= $hasilppn?></td>
                <td style="text-align: right; border: 1px solid #dbdbdb;border-width: thin;"><?= $grandtotal?></td>
                <?php $hsubtotal = $hsubtotal + $subtotal; ?>
                <?php $htotal_diskon = $htotal_diskon + $total_diskon; ?>
                <?php $hdpp = $hdpp + $dpp; ?>
                <?php $hhasilppn = $hhasilppn + $hasilppn; ?>
                <?php $hgrandtotal = $hgrandtotal + $grandtotal; ?>
            </tr>
        <?php endforeach; ?>
        <tr style="float: right; text-align: right; font-weight: bold; text-transform: uppercase;">
            <td style="border: 1px solid #dbdbdb;border-width: thin;" colspan="5">Grand Total : </td>
            <td style="border: 1px solid #dbdbdb;border-width: thin;" colspan="1"><?= $hsubtotal ?></td>
            <td style="border: 1px solid #dbdbdb;border-width: thin;" colspan="1"><?= $htotal_diskon?></td>
            <td style="border: 1px solid #dbdbdb;border-width: thin;" colspan="1"><?= $hdpp ?></td>
            <td style="border: 1px solid #dbdbdb;border-width: thin;" colspan="1"><?= $hhasilppn?></td>
            <td style="border: 1px solid #dbdbdb;border-width: thin;" colspan="1"><?= $hgrandtotal?></td>
        </tr>
    </tbody>
</table>