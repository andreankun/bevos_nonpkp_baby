<?php

header("Content-type: application/vnd-ms-excel");

header("Content-Disposition: attachment; filename=Laporan Outstanding Sales Order.xls");

header("Pragma: no-cache");

header("Expires: 0");

function FormatRupiah($angka)
{
    $hasil_rupiah = number_format($angka, 0, '.', ',');
    return $hasil_rupiah;
}
?>

<style>
    table thead th,
    table tbody td {
        border: 1px solid #3c3c3c;

    }
</style>
<h4 style="text-align: center;">LAPORAN OUTSTANDING SALES ORDER</h4>
<?php if (!empty($row)) : ?>
    CABANG : <?php echo $row->nama ?>
<?php else :  ?>
    CABANG : SEMUA CABANG
<?php endif; ?>
<br>

<table border="1" width="100%">

    <thead>
        <tr>
            <th style="text-align: center;">NO.</th>
            <th style="text-align: center;">SALES ORDER</th>
            <th style="text-align: center;">TANGGAL</th>
            <th style="text-align: center;">NAMA TOKO</th>
            <th style="text-align: center;">DPP</th>
            <th style="text-align: center;">PPN</th>
            <th style="text-align: center;">TOTAL</th>
            <th style="text-align: center;">CABANG</th>
        </tr>
    </thead>

    <tbody>
        <?php
        $no = 1;
        $grandtotal = 0;
        foreach ($report as $val) : ?>
            <tr>
                <td style="text-align: center;"><?= $no++; ?></td>
                <td style="text-align: center;"><?= $val->nomor ?></td>
                <td style="text-align: center;"><?= date('d-m-Y', strtotime($val->tanggal)) ?></td>
                <td style="text-align: left;"><?= $val->namapelanggan ?></td>
                <td style="text-align: right;"><?= FormatRupiah($val->dpp) ?></td>
                <td style="text-align: right;"><?= FormatRupiah($val->ppn) ?></td>
                <td style="text-align: right;"><?= FormatRupiah($val->grandtotal) ?></td>
                <td style="text-align: right;"><?= $val->namacabang ?></td>
                <!-- <td style="text-align: right;"><-?= $val->discbarang ?>%</td>
                    <td style="text-align: right;"><-?= FormatRupiah($val->total) ?></td> -->
                <?php $grandtotal += $val->grandtotal++; ?>
            </tr>
        <?php endforeach; ?>
        <tr style="float: right; text-align: right; font-weight: bold;">
            <td colspan="6">Grand Total : </td>
            <td colspan="1"><?= FormatRupiah($grandtotal) ?></td>
            <td colspan="1"></td>
        </tr>
    </tbody>

</table>