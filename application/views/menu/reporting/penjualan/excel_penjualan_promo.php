<?php

header("Content-type: application/vnd-ms-excel");

header("Content-Disposition: attachment; filename=Laporan Penjualan Promo.xls");

header("Pragma: no-cache");

header("Expires: 0");


function FormatRupiah($angka)
{
    $hasil_rupiah = number_format($angka, 0, '.', ',');
    return $hasil_rupiah;
}
?>
<h4 style="text-align: center;">LAPORAN PENJUALAN PROMO</h4>
<!-- CABANG : <-?php echo $row->nama ?> -->
<br>
PERIODE : <?php echo $tglawal ?> S/D <?php echo $tglakhir ?>
<br>

<table border="1" width="100%">

    <thead>
        <tr>
            <th style="text-align:center; text-transform:uppercase;">No.</th>
            <th style="text-align:center; text-transform:uppercase;">Kode</th>
            <th style="text-align:center; text-transform:uppercase;" colspan="3">Nama Produk</th>
            <th style="text-align:center; text-transform:uppercase;">Qty</th>
            <th style="text-align:center; text-transform:uppercase;">Satuan</th>
            <th style="text-align:center; text-transform:uppercase;">Harga</th>
            <th style="text-align:center; text-transform:uppercase;">Diskon</th>
            <th style="text-align:center; text-transform:uppercase;">Total</th>
        </tr>
    </thead>

    <tbody>
        <?php foreach ($report as $val) : ?>
            <?php $nomor = $val->nomor; ?>
            <?php foreach ($reportrow as $vals) : ?>
                <?php if ($vals->nomor == $nomor) : ?>
                    <tr>
                        <td colspan="2">No.Faktur : <?= $vals->nomor; ?></td>
                        <td colspan="2">Tanggal : <?= date('d-m-Y', strtotime($vals->tanggal)) ?></td>
                        <td colspan="2">Sales : <?= $vals->namasalesman ?></td>
                        <td colspan="4">Promo : <?= $vals->namapromo ?></td>
                    </tr>
                <?php endif; ?>
            <?php endforeach; ?>
            <?php
            $no = 1;
            $total = 0;
            $subtotal = 0;
            $subtotals = 0;
            $diskonlevel = 0;
            $diskonpromo = 0;
            $totaldiskonpromo = 0;
            $diskoncash = 0;
            $diskoncustomer = 0;
            $potongan = 0;
            $dpp = 0;
            $grandtotal = 0;
            $ppn = 0;
            foreach ($detail as $key => $value) : ?>
                <?php if ($value->nomor == $nomor) :
                    $total = $value->harga * $value->qty;
                    $diskonlevel = $value->discbrgitem;
                    $subtotal = $total - $diskonlevel;
                    $diskonpromo = $value->qty * $value->discpaketitem;
                    $diskoncash = $value->nilaicash;
                    $diskoncustomer = $value->nilaicustomer;
                ?>
                    <tr>
                        <td style="text-align: left;"><?= $no++; ?></td>
                        <td style="text-align: left;"><?= $value->kodebarang; ?></td>
                        <td style="text-align: left;" colspan="3"><?= $value->namabarang; ?></td>
                        <td style="text-align: right;"><?= $value->qty; ?></td>
                        <td style="text-align: left;"><?= $value->namasatuan; ?></td>
                        <td style="text-align: right;"><?= FormatRupiah($value->harga); ?></td>
                        <td style="text-align: right;"><?= FormatRupiah($diskonlevel); ?></td>
                        <td style="text-align: right;"><?= FormatRupiah($subtotal); ?></td>
                    </tr>
                    <?php
                    $subtotals = $subtotals + $subtotal;
                    $totaldiskonpromo = $totaldiskonpromo + $diskonpromo;
                    $potongan = round(($totaldiskonpromo + $diskoncash + $diskoncustomer) / 10) * 10;
                    $dpp = $subtotals - $potongan;
                    $ppn = ($dpp * $konfigurasi->ppn) / 100;
                    $grandtotal = $dpp + $ppn;
                    ?>
                <?php endif; ?>
            <?php endforeach; ?>
            <tr>
                <td colspan="2">Subtotal : <?= FormatRupiah($subtotals); ?></td>
                <td colspan="2">Potongan : <?= FormatRupiah($potongan); ?></td>
                <td colspan="2">DPP : <?= FormatRupiah($dpp); ?></td>
                <td colspan="2">PPN : <?= FormatRupiah($ppn); ?></td>
                <td colspan="2">Total : <?= FormatRupiah($grandtotal); ?></td>
            </tr>
            <tr>
                <td colspan="10"><br></td>
            </tr>
        <?php endforeach; ?>
    </tbody>

</table>