<?php

header("Content-type: application/vnd-ms-excel");

header("Content-Disposition: attachment; filename=Laporan Rekap Penjualan Pelanggan.xls");

header("Pragma: no-cache");

header("Expires: 0");

function FormatRupiah($angka)
{
    $hasil_rupiah = number_format($angka, 0, '.', ',');
    return $hasil_rupiah;
}
?>

<style>
    table thead th,
    table tbody td {
        border: 1px solid #3c3c3c;

    }
</style>
<h4 style="text-align: center;">LAPORAN REKAP PENJUALAN PELANGGAN</h4>
CABANG : <?php echo $cabang->nama ?>
<br>
PERIODE : <?php echo $tglawal ?> S/D <?php echo $tglakhir ?>
<br>

<table border="1" width="100%">

    <thead>
        <tr>
            <th style="text-align: center; text-transform: uppercase;">No.</th>
            <th style="text-align: center; text-transform: uppercase;">Nama Pelanggan</th>
            <th style="text-align: center; text-transform: uppercase;">Nama Toko</th>
            <th style="text-align: center; text-transform: uppercase;">Subtotal</th>
            <th style="text-align: center; text-transform: uppercase;">Diskon</th>
            <th style="text-align: center; text-transform: uppercase;">DPP</th>
            <th style="text-align: center; text-transform: uppercase;">PPN</th>
            <th style="text-align: center; text-transform: uppercase;">Total</th>
        </tr>
    </thead>

    <tbody>
        <?php
        $no = 1;
        $subtotal = 0;
        $grandtotal = 0;
        $total_diskon = 0;
        $dpp = 0;
        $hasilppn = 0;
        $hsubtotal = 0;
        $htotal_diskon = 0;
        $hdpp = 0;
        $hhasilppn = 0;
        $hgrandtotal = 0;
        foreach ($report as $val) :
            $subtotal = $val->subtotal - $val->discbrgitem - $val->discpaketitem;
            $total_diskon = $val->nilaicash + $val->nilaicustomer;
            $dpp = $val->dpp;
            $hasilppn = floor($val->dpp * ($ppn->ppn / 100));
            $grandtotal = $val->dpp + $hasilppn;
        ?>
            <tr>
                <td style="text-align: center;"><?= $no++; ?></td>
                <td style="text-align: left;"><?= $val->namapelanggan ?></td>
                <td style="text-align: left;"><?= $val->nama_toko ?></td>
                <td style="text-align: right;"><?= $subtotal ?></td>
                <td style="text-align: right;"><?= $total_diskon ?></td>
                <td style="text-align: right;"><?= $dpp ?></td>
                <td style="text-align: right;"><?= $hasilppn ?></td>
                <td style="text-align: right;"><?= $grandtotal ?></td>
                <?php $hsubtotal = $hsubtotal + $subtotal; ?>
                <?php $htotal_diskon = $htotal_diskon + $total_diskon; ?>
                <?php $hdpp = $hdpp + $dpp; ?>
                <?php $hhasilppn = $hhasilppn + $hasilppn; ?>
                <?php $hgrandtotal = $hgrandtotal + $grandtotal; ?>
            </tr>
        <?php endforeach; ?>
        <tr style="float: right; text-align: right; font-weight: bold; text-transform: uppercase;">
            <td colspan="3">Grand Total : </td>
            <td colspan="1"><?= $hsubtotal ?></td>
            <td colspan="1"><?= $htotal_diskon ?></td>
            <td colspan="1"><?= $hdpp ?></td>
            <td colspan="1"><?= $hhasilppn ?></td>
            <td colspan="1"><?= $hgrandtotal ?></td>
        </tr>
    </tbody>

</table>