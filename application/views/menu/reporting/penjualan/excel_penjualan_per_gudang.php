<?php

header("Content-type: application/vnd-ms-excel");

header("Content-Disposition: attachment; filename=Laporan Rekap Penjualan Per Gudang.xls");

header("Pragma: no-cache");

header("Expires: 0");

function FormatRupiah($angka)
{
    $hasil_rupiah = number_format($angka, 0, '.', ',');
    return $hasil_rupiah;
}
?>

<style>
    table thead th,
    table tbody td {
        border: 1px solid #3c3c3c;

    }
</style>
<h4 style="text-align: center;">LAPORAN REKAP PENJUALAN PER GUDANG</h4>
PERIODE : <?php echo $tglawal ?> S/D <?php echo $tglakhir ?>
<br>
GUDANG : <?php echo $row->nama ?>
<br>
DICETAK : <?php echo $this->session->userdata('myusername') ?>
<br>

<table border="1" width="100%">

    <thead>
        <tr>
            <th style="text-align: center;">NO.</th>
            <th style="text-align: center;">INVOICE</th>
            <th style="text-align: center;">SALES ORDER</th>
            <th style="text-align: center;">NAMA PELANGGAN</th>
            <th style="text-align: center;">NAMA TOKO</th>
            <th style="text-align: center;">DITERIMA</th>
            <th style="text-align: center;">DISERAHKAN</th>
            <th style="text-align: center;">ADMIN</th>
            <th style="text-align: center;">KETERANGAN</th>
        </tr>
    </thead>

    <tbody>
        <?php
        $no = 1;
        foreach ($excel as $val) :
        ?>
            <tr>
                <td style="text-align: center;"><?= $no++; ?></td>
                <td style="text-align: left;"><?= $val->nomor ?></td>
                <td style="text-align: left;"><?= $val->nomorso ?></td>
                <td style="text-align: left;"><?= $val->namapelanggan ?></td>
                <td style="text-align: left;"><?= $val->nama_toko ?></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
        <?php endforeach; ?>
    </tbody>

</table>