<?php

header("Content-type: application/vnd-ms-excel");

header("Content-Disposition: attachment; filename=Laporan Penjualan Perkategori cabang.xls");

header("Pragma: no-cache");

header("Expires: 0");

function FormatRupiah($angka)
{
	$hasil_rupiah = number_format($angka, 0, '.', ',');
	return $hasil_rupiah;
}
?>
<h4 style="text-align: center;">LAPORAN PENJUALAN PERKATEGORI SEMUA CABANG</h4>
CABANG : SEMUA CABANG
<br>
PERIODE : <?php echo $tglawal ?> S/D <?php echo $tglakhir ?>
<br>
<table border="1" width="100%">
<thead>
				<tr>
					<th>Tanggal</th>
					<th>Nomor Faktur</th>
					<th>Kode Cabang</th>
					<th>Kode Barang</th>
					<th>Nama Barang</th>
					<th>Qty</th>
					<th>Harga</th>
					<th>Diskon</th>
					<th>Jumlah</th>
				</tr>
			</thead>
			<tbody>
				<?php
				foreach ($reportrow as $val) : ?>

					<?php $merk = $val->kodemerk ?>
					<?php $namamerk = $val->namamerk ?>
					<tr>
						<td colspan="8" style="font-weight: bold; font-size: 12px">Kategori : <?= $namamerk ?></td>
					</tr>
					<?php
					$totalgrand = 0;
					foreach ($reportlist as $vals) : ?>
						<?php if ($vals->kodemerk == $merk) : ?>
							<?php $total = $vals->qty * $vals->harga ?>
							<?php $totaldiskon = $vals->discpaket + $vals->discbarang ?>
							<?php $diskon = $vals->harga * $vals->qty * $totaldiskon / 100 ?>
							<?php $totalharga = $total - $diskon ?>
							<tr>
								<td style="text-align: left;"><?= date('d-m-Y', strtotime($vals->tanggal)) ?></td>
								<td style="text-align: left;"><?= $vals->nomor ?></td>
								<td style="text-align: left;"><?= $vals->namacabang ?></td>
								<td style="text-align: left;"><?= $vals->kodebarang ?></td>
								<td style="text-align: left;"><?= $vals->namabarang ?></td>
								<td style="text-align: right;"><?= FormatRupiah($vals->qty) ?></td>
								<td style="text-align: right;"><?= FormatRupiah($vals->harga) ?></td>
								<td style="text-align: right;"><?= FormatRupiah($totaldiskon) ?>%</td>
								<!-- <td style="text-align: right;"><?= FormatRupiah($vals->dpp) ?></td> -->
								<!-- <td style="text-align: right;"><?= FormatRupiah($vals->ppn) ?></td> -->
								<td style="text-align: right;"><?= FormatRupiah($totalharga) ?></td>
							</tr>
							<?php $totalgrand = $totalgrand + $totalharga ?>
							<!-- <?php $totaldiskon1 = $totaldiskon1 + $diskon ?> -->
							<!-- <?php $totaldpp = $totaldpp + $vals->dpp ?> -->
							<!-- <?php $totalppn = $totalppn + $vals->ppn ?> -->
							<!-- <?php $grandtotal = $grandtotal + $totalgrand ?> -->
						<?php endif ?>

					<?php endforeach; ?>
					<tr>
						<td colspan="9">
							<hr>
						</td>
					</tr>
					<tr>
						<td colspan="6" style="text-align: right;">total : </td>
						<td style="text-align: right;"><?= FormatRupiah($totalgrand) ?></td>
						<!-- <td style="text-align: right;"><?= FormatRupiah($totaldiskon1) ?></td> -->
						<!-- <td style="text-align: right;"><?= FormatRupiah($totaldpp) ?></td> -->
						<!-- <td style="text-align: right;"><?= FormatRupiah($totalppn) ?></td> -->
					</tr>
				<?php endforeach; ?>
				<tr>
					<td colspan="6" style="text-align: right;">Grandtotal : </td>
					<td style="text-align: right;"><?= FormatRupiah($grandtotal) ?></td>
				</tr>
			</tbody>
</table>
