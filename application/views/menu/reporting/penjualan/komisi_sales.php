<?php

header("Content-type: application/vnd-ms-excel");

header("Content-Disposition: attachment; filename=Laporan Komisi Sales.xls");

header("Pragma: no-cache");

header("Expires: 0");

function FormatRupiah($angka)
{
    $hasil_rupiah = number_format($angka, 0, '.', ',');
    return $hasil_rupiah;
}
?>
<h4 style="text-align: center;">LAPORAN KOMISI</h4>
CABANG : <?php echo $row->nama ?>
<br>
PERIODE : <?php echo date('F Y',strtotime($tglawal)) ?>
<br>
NAMA SALES : <?php if($reportrow->namasalesman == ""){
    echo "-";
} else {
    echo $reportrow->namasalesman;
} ?>
<br>
<table border="1" width="100%">

    <thead>
        <tr>
            <th>Tanggal</th>
            <th>Pelanggan</th>
            <th>Toko</th>
            <th>No. Proforma</th>
            <th>No. Faktur</th>
            <th>Subtotal</th>
            <th>Diskon</th>
            <th>Total</th>
            <th>Komisi 1%</th>
        </tr>
    </thead>

    <tbody>
       <?php if($report != ""): ?>
        <?php
        // $htotal = 0;
        // $htotal_diskon = 0;
        // $hsubtotal = 0;
        // $hkomisi = 0;
        foreach ($report as $vals) :
        ?>
            <tr>
                <!-- <td style="text-align: center;"><-?= $no++; ?></td> -->
                <td style="text-align: center;"><?= date('d/m/Y', strtotime($vals->tanggal)) ?></td>
                <!-- <td style="text-align: center;"><-?= date('d/m/Y', strtotime($vals->tglbayar)) ?></td> -->
                <td style="text-align: left;"><?= $vals->namapelanggan ?></td>
                <td style="text-align: left;"><?= $vals->nama_toko ?></td>
                <td style="text-align: left;"><?= $vals->nomorso ?></td>
                <td style="text-align: left;"><?= $vals->nomor ?></td>
                <td style="text-align: right;"><?= $vals->subtotal - $vals->discbrgitem ?></td>
                <!-- <-?php if ($vals->totaldiskon == 0) : ?>
                    <td style="text-align: right;"><?= '' ?></td>
                <-?php else : ?> -->
                <td style="text-align: right;"><?= $vals->discpaketitem + $vals->nilaicustomer + $vals->nilaicash ?></td>
                <!-- <-?php endif; ?> -->
                <td style="text-align: right;"><?= $vals->dpp ?></td>
                <td style="text-align: right;"><?= round($vals->dpp * (1 / 100)) ?></td>
            </tr>
        <?php endforeach; ?>
        <!-- <tr style="float: right; text-align: right; font-weight: bold; text-transform: uppercase;">
            <td colspan="6">Grand Total : </td>
            <td colspan="1"><?= FormatRupiah($htotal) ?></td>
            <td colspan="1"><?= FormatRupiah($htotal_diskon) ?></td>
            <td colspan="1"><?= FormatRupiah($hsubtotal) ?></td>
            <td colspan="1"><?= FormatRupiah($hkomisi) ?></td>
        </tr> -->
        <?php else : ?>
            <tr>
                <td colspan="9">Data Tidak Ada</td>
            </tr>
        <?php endif; ?>
    </tbody>
</table>