<?php

header("Content-type: application/vnd-ms-excel");

header("Content-Disposition: attachment; filename=Laporan Komisi Sales - NETT.xls");

header("Pragma: no-cache");

header("Expires: 0");

function FormatRupiah($angka)
{
    $hasil_rupiah = number_format($angka, 0, ',', '.');
    return $hasil_rupiah;
}
?>
<h4 style="text-align: center;">LAPORAN KOMISI SALES - NETT</h4>
CABANG : <?php echo $row->nama ?>
<br>
PERIODE : <?php echo $tglawal ?> S/D <?php echo $tglakhir ?>
<br>
<?php if (!empty($reportrow)) : ?>
    NAMA SALES : <?php echo $reportrow->namasalesman ?>
<?php else : ?>
    NAMA SALES : -
<?php endif; ?>
<br>
<table border="1" width="100%">

    <thead>
        <tr>
            <th>Tanggal</th>
            <th>Customer</th>
            <th>No. Faktur</th>
            <th>Kode</th>
            <th>Nama Produk</th>
            <th>Qty</th>
            <th>Harga</th>
            <th>Diskon</th>
            <th>Total</th>
            <th>Tanggal Lunas</th>
            <th>Komisi</th>
            <th>Persen</th>
        </tr>
    </thead>

    <tbody>
        <?php
        $grandtotal = 0;
        $grandtotalkomisi = 0;
        foreach ($detail as $val) :
            $nilaicash = 0;
                $nilaicustomer = 0;
                $promo = 0;
                $barangitem = 0;
                $totaldiskon = 0;
                $total = 0;
                $cash = 0;
                $hasil = 0;
                $barangitem = $barangitem + $val->discbrgitem;
                $promo = $promo + $val->discpaketitem;
                $nilaicustomer = $nilaicustomer + $val->nilaicustomer;
                $nilaicash = $nilaicash + $val->nilaicash;
                // $totaldiskon = $totaldiskon + $barangitem + $promo + $nilaicustomer + $nilaicash;
                $totaldiskon = $totaldiskon + $barangitem + $promo;
                $total = $total  + (($val->qty * $val->harga) - $totaldiskon);
                $cash = $cash + round(round($total * ($val->disccash / 100) / 10) * 10);
                $hasil = $hasil + ($total - $cash);
        ?>
            <tr>
                <td><?= date('d-m-Y', strtotime($val->tanggal)) ?></td>
                <td><?= $val->namapelanggan ?></td>
                <td><?= $val->nomor ?></td>
                <td><?= $val->kodebarang ?></td>
                <td><?= $val->namabarang ?></td>
                <td style="text-align: right;"><?= $val->qty ?></td>
                <td style="text-align: right;"><?= $val->harga?></td>
                <td style="text-align: right;"><?= $totaldiskon + $cash ?></td>
                <td style="text-align: right;"><?= $hasil ?></td>
                <td style="text-align: center;"><?= date('d-m-Y', strtotime($val->tglbayar)) ?></td>
                <td style="text-align: right;"><?= $hasil * ($val->komisi / 100) ?></td>
                <td style="text-align: center;"><?= $val->komisi ?></td>
            </tr>
        <?php
            $grandtotal = $grandtotal + $hasil;
            $grandtotalkomisi = $grandtotalkomisi + ($hasil * ($val->komisi / 100));
        endforeach;
        ?>
        <tr>
            <td colspan="8" style="text-align: right; font-weight: bold;">Grand Total :</td>
            <td style="text-align: right;"><?= $grandtotal ?></td>
            <td></td>
            <td style="text-align: right;"><?= $grandtotalkomisi ?></td>
        </tr>
    </tbody>
</table>