<?php

header("Content-type: application/vnd-ms-excel");

header("Content-Disposition: attachment; filename=Laporan Sales Order Detail.xls");

header("Pragma: no-cache");

header("Expires: 0");


function FormatRupiah($angka)
{
    $hasil_rupiah = number_format($angka, 0, '.', ',');
    return $hasil_rupiah;
}
?>
<h4 style="text-align: center;">LAPORAN SALES ORDER DETAIL</h4>
<!-- CABANG : <-?php echo $row->nama ?> -->
<br>
PERIODE : <?php echo $tglawal ?> S/D <?php echo $tglakhir ?>
<br>

<table border="1" width="100%">

    <thead>
        <tr>
            <th>NO.</th>
            <th>KODE</th>
            <th>NAMA PRODUK</th>
            <th>QTY</th>
            <th>SATUAN</th>
            <th>HARGA</th>
            <th>DISKON</th>
            <th>TOTAL</th>
        </tr>
    </thead>

    <tbody>
        <?php foreach ($report as $val) : ?>
            <?php $nomor = $val->nomor; ?>
            <?php foreach ($reportrow as $vals) : ?>
                <?php if ($vals->nomor == $nomor) : ?>
                    <tr>
                        <td colspan="3" style="font-weight: bold; border: none;">No. Order : <?= $vals->nomor ?></td>
                        <?php if ($vals->jenisjual == '1' || $vals->jenisjual == '3' || $vals->jenisjual == '4') : ?>
                            <td colspan="3" style="font-weight: bold; border: none;">TUNAI/KREDIT : TUNAI</td>
                        <?php else : ?>
                            <td colspan="3" style="font-weight: bold; border: none;">TUNAI/KREDIT : KREDIT</td>
                        <?php endif; ?>
                        <td colspan="2" style="font-weight: bold; border: none;">Pelanggan : <?= $vals->namapelanggan ?></td>
                    </tr>
                    <tr>
                        <td colspan="3" style="font-weight: bold; border: none;">Tanggal : <?= date('d-m-Y', strtotime($vals->tanggal)) ?></td>
                        <td colspan="3" style="font-weight: bold; border: none;">Sales : <?= $vals->namasalesman ?></td>
                        <td colspan="2" style="font-weight: bold; border: none;">Toko : <?= $vals->nama_toko ?></td>
                    </tr>
                    <tr>
                        <td colspan="3" style="font-weight: bold; border: none;">Gudang : <?= $vals->namagudang ?></td>
                        <td colspan="3" style="font-weight: bold; border: none;">User : <?= $vals->pemakai ?></td>
                        <td colspan="2" style="font-weight: bold; border: none;">Alamat : <?= $vals->pengiriman ?></td>
                    </tr>
                <?php endif; ?>
            <?php endforeach; ?>
            <?php
            $no = 1;
            $total = 0;
            $diskonlevel = 0;
            $subtotal = 0;
            $subtotals = 0;
            $diskonpromo = 0;
            $totaldiskonpromo = 0;
            $diskoncash = 0;
            $diskoncustomer = 0;
            $potongan = 0;
            $dpp = 0;
            $ppn = 0;
            $grandtotal = 0;
            foreach ($detail as $row) :
                $total = $row->harga * $row->qty;
                $diskonlevel = $row->discbrgitem;
                $diskonpromo = $row->discpaketpromo;
                $subtotal = $total - ($diskonlevel + $diskonpromo);
                // $diskonpromo = $row->qty * $row->discpaketpromo;
                $diskoncash = $row->nilaicash;
                $diskoncustomer = $row->nilaicustomer;
            ?>
                <?php if ($row->nomor == $nomor) :
                ?>
                    <tr>
                        <td style="border: none;"><?= $no++; ?></td>
                        <td style="border: none;"><?= $row->kodebarang; ?></td>
                        <td style="border: none;"><?= $row->namabarang; ?></td>
                        <td style="border: none; text-align:right;"><?= $row->qty; ?></td>
                        <td style="border: none; text-align:left;"><?= $row->namasatuan; ?></td>
                        <td style="border: none; text-align:right;"><?= FormatRupiah($row->harga); ?></td>
                        <td style="border: none; text-align:right;"><?= FormatRupiah($diskonlevel + $diskonpromo); ?></td>
                        <td style="border: none; text-align:right;"><?= FormatRupiah($total - ($diskonlevel + $diskonpromo)); ?></td>
                    </tr>
                    <?php
                    $subtotals = $subtotals + ($total - ($diskonlevel + $diskonpromo));
                    $totaldiskonpromo = $totaldiskonpromo + $diskonpromo;
                    $potongan = round(round(($diskoncash + $diskoncustomer) / 10) * 10);
                    $dpp = $subtotals - $potongan;
                    if ($row->statusppn == 't' || $row->statusppn == 'true') {
                        $ppn = floor(($dpp * $konfigurasi->ppn) / 100);
                    } else {
                        $ppn = 0;
                    }
                    $grandtotal = $dpp + $ppn;
                    ?>
                <?php endif; ?>
            <?php endforeach; ?>
            <tr>
                <td colspan="8"></td>
            </tr>
            <tr>
                <td colspan="7" style="text-align: right; border: none;">Subtotal : </td>
                <td colspan="1" style="text-align: right; border: none;"><?= FormatRupiah($subtotals); ?></td>
            </tr>
            <tr>
                <td colspan="7" style="text-align: right; border: none;">Potongan : </td>
                <td colspan="1" style="text-align: right; border: none;"><?= FormatRupiah($potongan); ?></td>
            </tr>
            <tr>
                <td colspan="7" style="text-align: right; border: none;">DPP : </td>
                <td colspan="1" style="text-align: right; border: none;"><?= FormatRupiah($dpp); ?></td>
            </tr>
            <tr>
                <td colspan="7" style="text-align: right; border: none;">PPN : </td>
                <td colspan="1" style="text-align: right; border: none;"><?= FormatRupiah($ppn); ?></td>
            </tr>
            <tr>
                <td colspan="7" style="text-align: right; border: none;">Total : </td>
                <td colspan="1" style="text-align: right; border: none;"><?= FormatRupiah($grandtotal); ?></td>
            </tr>
            <tr>
                <td colspan="8"></td>
            </tr>
        <?php endforeach; ?>
    </tbody>

</table>