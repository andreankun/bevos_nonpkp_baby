<?php

header("Content-type: application/vnd-ms-excel");

header("Content-Disposition: attachment; filename=Laporan Retur Detail.xls");

header("Pragma: no-cache");

header("Expires: 0");


function FormatRupiah($angka)
{
    $hasil_rupiah = number_format($angka, 0, '.', ',');
    return $hasil_rupiah;
}
?>
<h4 style="text-align: center;">LAPORAN RETUR DETAIL</h4>
PERIODE : <?php echo $tglawal ?> S/D <?php echo $tglakhir ?>
<br>
CABANG : <?php echo $row->nama ?>
<br>

<table border="1" width="100%">

    <thead>
        <tr>
            <th>NO.</th>
            <th>KODE</th>
            <th>NAMA PRODUK</th>
            <th>QTY</th>
            <th>SATUAN</th>
            <th>HARGA</th>
            <th>DISKON</th>
            <th>TOTAL</th>
        </tr>
    </thead>

    <tbody>
        <?php foreach ($report as $val) : ?>
            <?php $nomor = $val->nomor; ?>
            <?php foreach ($reportrow as $vals) : ?>
                <?php if ($vals->nomor == $nomor) : ?>
                    <tr>
                        <td colspan="3" style="font-weight: bold; border: none;">No. Retur : <?= $vals->nomor ?></td>
                        <td colspan="3" style="font-weight: bold; border: none;">No. Faktur : <?= $vals->noinvoice ?></td>
                        <td colspan="2" style="font-weight: bold; border: none;">Pelanggan : <?= $vals->namapelanggan ?></td>
                    </tr>
                    <tr>
                        <td colspan="3" style="font-weight: bold; border: none;">Tgl. Jual : <?= date('d-m-Y', strtotime($vals->tanggal)) ?></td>
                        <td colspan="3" style="font-weight: bold; border: none;">Faktur Pajak : <?= $vals->nofakturpajak ?></td>
                        <td colspan="2" style="font-weight: bold; border: none;">Toko : <?= $vals->nama_toko ?></td>
                    </tr>
                    <tr>
                        <td colspan="3" style="font-weight: bold; border: none;">Gudang : <?= $vals->namagudang ?></td>
                        <td colspan="3" style="font-weight: bold; border: none;">User : <?= $vals->pemakai ?></td>
                        <td colspan="2" style="font-weight: bold; border: none;">Alamat : <?= $vals->pengiriman ?></td>
                    </tr>
                <?php endif; ?>
            <?php endforeach; ?>
            <?php
            $no = 1;
            $total = 0;
            $diskonlevel = 0;
            $subtotal = 0;
            $subtotals = 0;
            $diskonpromo = 0;
            $totaldiskonpromo = 0;
            $diskoncash = 0;
            $diskoncustomer = 0;
            $potongan = 0;
            $dpp = 0;
            $ppn = 0;
            $grandtotal = 0;
            foreach ($detail as $row) :
                $total = $row->harga * $row->qty;
                $diskonlevel = $row->discbrgitem;
                $subtotal = $total - $diskonlevel;
                $diskonpromo = $row->qty * $row->discpaket;
                $diskoncash = $row->nilaicash;
                $diskoncustomer = $row->nilaicustomer;
            ?>
                <?php if ($row->nomor == $nomor) :
                ?>
                    <tr>
                        <td style="border: none;"><?= $no++; ?></td>
                        <td style="border: none;"><?= $row->kodebarang; ?></td>
                        <td style="border: none;"><?= $row->namabarang; ?></td>
                        <td style="text-align:right; border: none;"><?= $row->qty; ?></td>
                        <td style="text-align:left; border: none;"><?= $row->namasatuan; ?></td>
                        <td style="text-align:right; border: none;"><?= FormatRupiah($row->harga); ?></td>
                        <td style="text-align:right; border: none;"><?= FormatRupiah($diskonlevel); ?></td>
                        <td style="text-align:right; border: none;"><?= FormatRupiah($subtotal); ?></td>
                    </tr>
                    <?php
                    $subtotals = $subtotals + $subtotal;
                    $totaldiskonpromo = $totaldiskonpromo + $diskonpromo;
                    $potongan = round(($totaldiskonpromo + $diskoncash + $diskoncustomer) / 10) * 10;
                    $dpp = $subtotals - $potongan;
                    $ppn = $row->ppn;
                    // $ppn = ($dpp * $konfigurasi->ppn) / 100;
                    $grandtotal = $dpp + $ppn;
                    ?>
                <?php endif; ?>
            <?php endforeach; ?>
            <tr>
                <td colspan="7" style="text-align: right;">Subtotal : </td>
                <td colspan="1" style="text-align: right;"><?= FormatRupiah($subtotals); ?></td>
            </tr>
            <tr>
                <td colspan="7" style="text-align: right;">Potongan : </td>
                <td colspan="1" style="text-align: right;"><?= FormatRupiah($potongan); ?></td>
            </tr>
            <tr>
                <td colspan="7" style="text-align: right;">DPP : </td>
                <td colspan="1" style="text-align: right;"><?= FormatRupiah($dpp); ?></td>
            </tr>
            <tr>
                <td colspan="7" style="text-align: right;">PPN : </td>
                <td colspan="1" style="text-align: right;"><?= FormatRupiah($ppn); ?></td>
            </tr>
            <tr>
                <td colspan="7" style="text-align: right;">Total : </td>
                <td colspan="1" style="text-align: right;"><?= FormatRupiah($grandtotal); ?></td>
            </tr>
            <tr>
                <td colspan="8"></td>
            </tr>
        <?php endforeach; ?>
    </tbody>

</table>