<?php

header("Content-type: application/vnd-ms-excel");

header("Content-Disposition: attachment; filename=Laporan Penjualan Detail.xls");

header("Pragma: no-cache");

header("Expires: 0");


function FormatRupiah($angka)
{
	$hasil_rupiah = number_format($angka, 0, '.', ',');
	return $hasil_rupiah;
}
?>
<h4 style="text-align: center;">LAPORAN PENJUALAN DETAIL ALL CABANG</h4>
<!-- CABANG : <-?php echo $row->nama ?> -->
<br>
PERIODE : <?php echo $tglawal ?> S/D <?php echo $tglakhir ?>
<br>

<table border="1" width="100%">

	<thead>
		<tr>
			<th>No.</th>
			<th>Kode</th>
			<th>Nama Produk</th>
			<th>Qty</th>
			<th>Satuan</th>
			<th>Harga</th>
			<th>Diskon Level</th>
			<th>Diskon Promo</th>
			<th>Diskon Customer</th>
			<th>Diskon Cash</th>
			<th>Total</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($reportlist as $val) : ?>
			<?php $nomor = $val->nomor; ?>
			<?php foreach ($reportrow as $vals) : ?>
				<?php if ($vals->nomor == $nomor) : ?>
					<?php $ppn = $vals->ppn ?>
					<?php $dpp = $vals->dpp ?>
					<?php $diskoncash = $vals->nilaicash ?>
					<?php $diskoncustomer = $vals->nilaicustomer ?>
					<tr>
						<td colspan="3" style="font-weight: bold;">No. Faktur : <?= $vals->nomor ?></td>
						<?php if ($vals->jenisjual == '1' || $vals->jenisjual == '3' || $vals->jenisjual == '4') : ?>
							<td colspan="3" style="font-weight: bold;">TUNAI/KREDIT : TUNAI</td>
						<?php else : ?>
							<td colspan="3" style="font-weight: bold;">TUNAI/KREDIT : KREDIT</td>
						<?php endif; ?>
						<td colspan="2" style="font-weight: bold;">Pelanggan : <?= $vals->namapelanggan ?></td>
					</tr>
					<tr>
						<td colspan="3" style="font-weight: bold;">Tanggal : <?= date('d-m-Y', strtotime($vals->tanggal)) ?></td>
						<td colspan="3" style="font-weight: bold;">Sales : <?= $vals->nama ?></td>
						<td colspan="2" style="font-weight: bold;">Toko : <?= $vals->namatoko ?></td>
					</tr>
					<tr>
						<td colspan="3" style="font-weight: bold;">Gudang : <?= $vals->kodegudang ?></td>
						<td colspan="3" style="font-weight: bold;">User : <?= $vals->pemakai ?></td>
						<td colspan="2" style="font-weight: bold;">Alamat : <?= $vals->alamat ?></td>
					</tr>
				<?php endif; ?>
			<?php endforeach; ?>
			<?php
			$no = 1;
			$no = 1;
			$total = 0;
			$diskonlevel = 0;
			$subtotal = 0;
			$subtotals = 0;
			$diskonpromo = 0;
			$totaldiskonpromo = 0;
			// $potongan = 0;
			$grandtotal = 0;
			foreach ($report as $value) :
				$total = $value->harga * $value->qty;
				$diskonlevel = $value->discbrgitem;
				$diskonpromo1 = $value->discpaketitem;
				$subtotal = $total - $diskonlevel - $diskonpromo1;
				$discash = $value->disccash;
				$discustomer = $value->disccustomer;
			?>
				<?php if ($value->nomor == $nomor) : ?>
					<?php
					$diskonpromo = $diskonpromo + $value->discpaketitem;
					$subtotals = $subtotals + $subtotal;
					$totaldiskon = $diskoncash + $diskoncustomer;
					$grandtotal = $dpp + $ppn;
					?>
					<tr>
						<td><?= $no++ ?></td>
						<td><?= $value->kodebarang ?></td>
						<td><?= $value->namabarang ?></td>
						<td style="text-align: right;"><?= $value->qty ?></td>
						<td style="text-align: right;"><?= $value->kodesatuan ?></td>
						<td style="text-align: right;"><?= FormatRupiah($value->harga) ?></td>
						<td style="text-align: right;"><?= FormatRupiah($diskonlevel) ?></td>
						<td style="text-align: right;"><?= FormatRupiah($diskonpromo1) ?></td>
						<td style="text-align: right;"><?= FormatRupiah($discustomer) ?> %</td>
						<td style="text-align: right;"><?= FormatRupiah($discash) ?> %</td>
						<td style="text-align: right;"><?= FormatRupiah($subtotal) ?></td>
					</tr>

				<?php endif; ?>
			<?php endforeach; ?>
			<tr>
				<td colspan="10" style="text-align: right;">Subtotal : </td>
				<td colspan="1" style="text-align: right;"><?= FormatRupiah($subtotals); ?></td>
			</tr>
			<tr>
				<td colspan="10" style="text-align: right;">Potongan : </td>
				<td colspan="1" style="text-align: right;"><?= FormatRupiah($totaldiskon); ?></td>
			</tr>
			<tr>
				<td colspan="10" style="text-align: right;">DPP : </td>
				<td colspan="1" style="text-align: right;"><?= FormatRupiah($dpp); ?></td>
			</tr>
			<tr>
				<td colspan="10" style="text-align: right;">PPN : </td>
				<td colspan="1" style="text-align: right;"><?= FormatRupiah($ppn); ?></td>
			</tr>
			<tr>
				<td colspan="10" style="text-align: right;">Total : </td>
				<td colspan="1" style="text-align: right;"><?= FormatRupiah($grandtotal); ?></td>
			</tr>
		<?php endforeach; ?>
	</tbody>

</table>