<div class="col-12 col-xl-12 mb-2">
    <div class="modal-header" style="padding: 5px;">
        <div class="col-12 col-md-12">
            <div class="row">
                <div class="col-5 col-md-5">
                    <h2>
                        <span class="logo-menu"><?php echo $icons ?></span>
                        <span class="text-uppercase"><?php echo $title ?></span>
                    </h2>
                </div>
                <div class="col-7 col-md-7">
                    <div class="form-group" style="float: right;">
                        <button id="preview" style="background: #983264; color: #fff;" class="btn btn-sm mb-1 mt-1 mr-1 ml-1"><i class="fa fa-eye" style="margin-right: 10px;"></i>Preview PDF</button>
                        <button id="print" style="background: salmon; color: #fff;" class="btn btn-sm mb-1 mt-1 mr-1 ml-1"><i class="far fa-file-pdf" style="margin-right: 10px;"></i>Download PDF</button>
                        <button id="export" class="btn btn-sm btn-success mb-1 mt-1 mr-1 ml-1"><i class="far fa-file-excel" style="margin-right: 10px;"></i>Download Excel</button>
                        <button id="clear" class="btn btn-sm btn-dark mb-1 mt-1 mr-1 ml-1"><i class="far fa-refresh" style="margin-right: 10px;"></i>Clear</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-12 table-responsive hidescroll" style="height: 555px; overflow-y: scroll;">
    <div class="row">
        <div class="col-12 col-md-12 col-lg-12 col-xl-6">
            <div class="modal-content mb-2">
                <div class="modal-body">
                    <div class="row mb-2 mt-2">
                        <div class="col-sm-12 input-group box">
                            <div class="col-sm-3 p-0 input-group-prepend">
                                <span class="col-sm-12 input-group-text">Laporan Dokumen</span>
                            </div>
                            <select class="form-control" style="height: 29px; padding: 0.175rem 0.50rem;" id="jenis_laporan">
                                <option value="">Pilih Laporan Dokumen</option>
                                <option value="1">Laporan Retur Penjualan Per Tanggal</option>
                                <option value="2">Laporan Penjualan Per Pelanggan</option>
                                <option value="3">Laporan Sales Order</option>
                                <option value="4">Laporan Rekap Penjualan Semua Pelanggan</option>
                                <option value="5">Laporan Penjualan Per Pelanggan & Per Barang</option>
                                <option value="6">Laporan Penjualan Semua Salesman</option>
                                <option value="7">Laporan Penjualan Per Salesman</option>
                                <option value="8">Laporan Rekap Penjualan Per Pelanggan</option>
                                <option value="9">Laporan Detail Sales Order</option>
                                <option value="10">Laporan Penjualan Detil Per Pelanggan</option>
                                <option value="11">Laporan Rekap Penjualan Per Gudang</option>
                                <option value="12">Laporan Detail Retur</option>
                                <option value="13">Laporan Rekap Penjualan Pelanggan</option>
                                <option value="14">Laporan Rekap Retur</option>
                                <option value="15">Laporan Penjualan Promo</option>
                                <option value="16">Laporan Komisi Sales - NETT</option>
                                <option value="18">Laporan Penjualan Detil</option>
                                <option value="19">Laporan Rekap Penjualan</option>
                                <option value="20">Laporan Penjualan Pertanggal</option>
                                <option value="21">Laporan Penjualan Perkategori</option>
                                <option value="22">Laporan Rekap Penjualan All Cabang</option>
                                <option value="23">Laporan Komisi Sales Proyeksi</option>
                                <option value="24">Laporan Komisi Sales Hangus</option>
                                <option value="25">Laporan Outstanding Sales Order</option>
                                <option value="26">Laporan Penjualan Detail All Cabang</option>
                                <option value="27">Laporan Komisi Sales</option>
                                <!-- <option value="17">Laporan Komisi Sales Per Pelanggan</option> -->
                            </select>
                        </div>
                    </div>
                    <div class="row mb-2 cbg">
                        <div class="col-sm-6 input-group">
                            <div class="col-sm-6 p-0 input-group-prepend">
                                <span class="col-sm-12 input-group-text">Kode Cabang</span>
                            </div>
                            <input class="form-control" type="text" name="kodecabang" id="kodecabang" maxlength="2" placeholder="Kode Cabang" required readonly />
                            <div class="input-group-text btn-dark" data-toggle="modal" data-target="#findcabang" id="carikodecabang">
                                <span class="input-group-addon">
                                    <i class="fa fa-search text-white"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-2 gdg">
                        <div class="col-sm-12 input-group">
                            <div class="col-sm-3 p-0 input-group-prepend">
                                <span class="col-sm-12 input-group-text">Gudang</span>
                            </div>
                            <select class="form-control" name="kodegudang" id="kodegudang" style="height: 29px; padding: 0.175rem 0.50rem;"></select>
                        </div>
                    </div>
                    <div class="row mb-2 pelanggan">
                        <div class="col-sm-6 input-group">
                            <div class="col-sm-6 p-0 input-group-prepend">
                                <span class="col-sm-12 input-group-text">Nomor Pelanggan</span>
                            </div>
                            <input class="form-control" type="text" name="nopelanggan" id="nopelanggan" maxlength="2" placeholder="Nomor Pelanggan" required readonly />
                        </div>
                        <div class="col-sm-6 input-group">
                            <div class="col-sm-6 p-0 input-group-prepend">
                                <span class="col-sm-12 input-group-text">Nama Pelanggan</span>
                            </div>
                            <input class="form-control" type="text" name="namapelanggan" id="namapelanggan" maxlength="2" placeholder="Nama Pelanggan" required readonly />
                            <div class="input-group-text btn-dark" data-toggle="modal" data-target="#findpelanggan" id="caripelanggan">
                                <span class="input-group-addon">
                                    <i class="fa fa-search text-white"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-2 barang">
                        <div class="col-sm-12 input-group">
                            <div class="col-sm-3 p-0 input-group-prepend">
                                <span class="col-sm-12 input-group-text">Kode Barang</span>
                            </div>
                            <select class="form-control" name="kodebarang" id="kodebarang" style="height: 29px; padding: 0.175rem 0.50rem;"></select>
                        </div>
                    </div>
                    <div class="row mb-2 salesman">
                        <div class="col-sm-12 input-group">
                            <div class="col-sm-3 p-0 input-group-prepend">
                                <span class="col-sm-12 input-group-text">Kode Salesman</span>
                            </div>
                            <select class="form-control" name="kodesalesman" id="kodesalesman" style="height: 29px; padding: 0.175rem 0.50rem;"></select>
                        </div>
                    </div>
                    <div class="row mb-2 merk">
                        <div class="col-sm-6 input-group">
                            <div class="col-sm-6 p-0 input-group-prepend">
                                <span class="col-sm-12 input-group-text">Kode Merk</span>
                            </div>
                            <select class="form-control" name="kodesalesman" id="merkawal" style="height: 29px; padding: 0.175rem 0.50rem;"></select>
                        </div>
						<div class="col-sm-6 input-group">
                            <div class="col-sm-6 p-0 input-group-prepend">
                                <span class="col-sm-12 input-group-text">Sampai</span>
                            </div>
                            <select class="form-control" name="kodesalesman" id="merkakhir" style="height: 29px; padding: 0.175rem 0.50rem;"></select>
                        </div>
                    </div>
                    <div class="row mb-2 tgl">
                        <div class="col-sm-6 input-group">
                            <div class="col-sm-6 p-0 input-group-prepend">
                                <span class="col-sm-12 input-group-text">Tanggal Awal</span>
                            </div>
                            <input class="form-control" id="tglawal" value="<?php echo date('Y-05-01'); ?>" padding="5px" type="text" required />
                        </div>
                        <div class="col-sm-6 input-group">
                            <div class="col-sm-6 p-0 input-group-prepend">
                                <span class="col-sm-12 input-group-text">Tanggal Akhir</span>
                            </div>
                            <input class="form-control" id="tglakhir" value="<?php echo date('Y-m-d'); ?>" padding="5px" type="text" required />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="findcabang">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" style="border-radius: 5px;">
            <div class="modal-header" style="padding: 10px; color: #000;">
                <h7 class="modal-title" style="margin-left: 5px;">Data Cabang</h7>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table id="t_cabang" class="table table-bordered table-striped dt-responsive display nowrap" style="width:100%">
                        <thead class="thead-dark">
                            <tr>
                                <th width="10">Action</th>
                                <th width="150">Kode Cabang</th>
                                <th width="150">Nama Cabang</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="findpelanggan">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" style="border-radius: 5px;">
            <div class="modal-header" style="padding: 10px; color: #000;">
                <h7 class="modal-title" style="margin-left: 5px;">Data Pelanggan</h7>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table id="t_pelanggan" class="table table-bordered table-striped dt-responsive display nowrap" style="width:100%">
                        <thead class="thead-dark">
                            <tr>
                                <th width="10">Action</th>
                                <th width="150">Kode Pelanggan</th>
                                <th width="150">Nama Pelanggan</th>
                                <th width="150">Nama Toko</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>