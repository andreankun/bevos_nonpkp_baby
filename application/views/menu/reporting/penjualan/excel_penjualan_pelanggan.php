<?php

header("Content-type: application/vnd-ms-excel");

header("Content-Disposition: attachment; filename=Laporan.xls");

header("Pragma: no-cache");

header("Expires: 0");

function FormatRupiah($angka)
{
    $hasil_rupiah = number_format($angka, 0, '.', ',');
    return $hasil_rupiah;
}
?>
<h4 style="text-align: center;">LAPORAN PENJUALAN PER PELANGGAN</h4>
CABANG : <?php echo $penjualanperpelangganrow->namacabang ?>
<br>
PELANGGAN : <?php echo $penjualanperpelangganrow->namapelanggan ?>
<br>
NAMA TOKO : <?php echo $penjualanperpelangganrow->nama_toko ?>
<br>
PERIODE : <?php echo $tglawal ?> S/D <?php echo $tglakhir ?>
<br>

<table border="1" width="100%">
    <thead>
        <tr>
            <th style="text-align: center;">TANGGAL</th>
            <th style="text-align: center;">NO. FAKTUR</th>
            <th style="text-align: center;">NAMA BARANG</th>
            <th style="text-align: center;">QTY</th>
            <th style="text-align: center;">HARGA</th>
            <th style="text-align: center;">DISKON (%)</th>
            <th style="text-align: center;">JUMLAH</th>
        </tr>
    </thead>
    <tbody>
        <?php $i = 1;
        foreach ($penjualanperpelanggan as $val) { ?>
            <tr>
                <td style="text-align: right;"><?php echo date('d/m/Y', strtotime($val->tanggal)); ?></td>
                <td style="text-align: left;"><?php echo $val->nomor; ?></td>
                <td style="text-align: left;"><?php echo $val->namabarang; ?></td>
                <td style="text-align: right;"><?php echo $val->qty; ?></td>
                <td style="text-align: right;"><?php echo $val->harga; ?></td>
                <td style="text-align: right;"><?php echo $val->discbarang; ?></td>
                <td style="text-align: right;"><?php echo $val->total; ?></td>
            </tr>
        <?php $i++;
        } ?>
    </tbody>
</table>