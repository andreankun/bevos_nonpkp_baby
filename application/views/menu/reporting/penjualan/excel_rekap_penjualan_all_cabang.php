<?php

header("Content-type: application/vnd-ms-excel");

header("Content-Disposition: attachment; filename=Laporan Rekap Penjualan All Cabang.xls");

header("Pragma: no-cache");

header("Expires: 0");

function FormatRupiah($angka)
{
    $hasil_rupiah = number_format($angka, 0, '.', ',');
    return $hasil_rupiah;
}
?>
<h4 style="text-align: center;">LAPORAN REKAP PENJUALAN ALL CABANG</h4>
<!-- CABANG : <?php echo $cabang ?>
<br> -->
PERIODE : <?php echo $tglawal ?> S/D <?php echo $tglakhir ?>
<br>
<table border="1" width="100%">
    <thead>
        <tr>
            <th>No</th>
            <th>Invoice</th>
            <th>Pelanggan</th>
            <th>Tanggal</th>
            <th>Kode Cabang</th>
            <th>Kode Salesman</th>
            <th>Subtotal</th>
            <th>Diskon</th>
            <th>DPP</th>
            <th>PPN</th>
            <th>Total</th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($reportrow as $val) : ?>

            <?php $jenisjual = $val->jenisjual ?>
            <tr>
                <td colspan="9" style="font-weight: bold; font-size: 12px">Methode Pembayaran : <?php if ($jenisjual == 1) {
                                                                                                    echo "TUNAI";
                                                                                                } else if ($jenisjual == 2) {
                                                                                                    echo "KREDIT";
                                                                                                } else if ($jenisjual == 3) {
                                                                                                    echo "COD";
                                                                                                } else {
                                                                                                    echo "TUNAI 0%";
                                                                                                } ?></td>
            </tr>
            <?php
            $total = 0;
            $totaldiskon1 = 0;
            $totaldpp = 0;
            $totalppn = 0;
            $grandtotal = 0;
            $no = 1;
            $diskon = 0;
            $totaldiskon = 0;
            foreach ($reportlist as $vals) : ?>
                <?php if ($vals->jenisjual == $jenisjual) : ?>
                    <tr>
                        <td style="text-align: left; font-size: 11px; font-weight: bold;"><?= $no++ ?></td>
                        <td style="text-align: left; font-size: 11px; font-weight: bold;" cellpadding="2"><?= $vals->nomor ?></td>
                        <td style="text-align: left; font-size: 11px; font-weight: bold;"><?= $vals->nama ?></td>
                        <td style="text-align: left; font-size: 11px; font-weight: bold;"><?= date('d-m-Y', strtotime($vals->tanggal)) ?></td>
                        <td style="text-align: left; font-size: 11px; font-weight: bold;"><?= $vals->namacabang ?></td>
                        <td style="text-align: left; font-size: 11px; font-weight: bold;"><?= $vals->kodesalesman ?></td>
                        <td style="text-align: right; font-size: 11px; font-weight: bold;"><?= FormatRupiah($vals->total) ?></td>
                        <td style="text-align: right; font-size: 11px; font-weight: bold;"><?= FormatRupiah($vals->nilaicustomer + $vals->nilaicash) ?></td>
                        <td style="text-align: right; font-size: 11px; font-weight: bold;"><?= FormatRupiah($vals->dpp) ?></td>
                        <td style="text-align: right; font-size: 11px; font-weight: bold;"><?= FormatRupiah($vals->ppn) ?></td>
                        <td style="text-align: right; font-size: 11px; font-weight: bold;s"><?= FormatRupiah($vals->grandtotal) ?></td>
                    </tr>
                <?php endif ?>
                <?php $total = $total + $vals->total ?>
                <?php $totaldiskon1 = $totaldiskon1 + ($vals->nilaicustomer + $vals->nilaicash) ?>
                <?php $totaldpp = $totaldpp + $vals->dpp ?>
                <?php $totalppn = $totalppn + $vals->ppn ?>
                <?php $grandtotal = $grandtotal + $vals->grandtotal ?>
            <?php endforeach; ?>
            <tr>
                <td colspan="9">
                    <hr>
                </td>
            </tr>
            <tr>
                <td colspan="4" style="text-align: right;">total : </td>
                <td style="text-align: right;"><?= FormatRupiah($total) ?></td>
                <td style="text-align: right;"><?= FormatRupiah($totaldiskon1) ?></td>
                <td style="text-align: right;"><?= FormatRupiah($totaldpp) ?></td>
                <td style="text-align: right;"><?= FormatRupiah($totalppn) ?></td>
                <td style="text-align: right;"><?= FormatRupiah($grandtotal) ?></td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
</table>