<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Invoice extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('caridataaktif_model');
		$this->load->library('form_validation');
		$this->load->library('session');
	}

	public function Print($nomor = "")
	{
		// $paper = 'A4';
		$paper = array(0, 0, 612, 396);
		$data['inv'] = $this->db->query("SELECT nomor, nomorso FROM srvt_invoice WHERE nomor = '" . $nomor . "'")->row();
		$data['konfigurasi'] = $this->db->query("SELECT* FROM stpm_konfigurasi")->row();

		$data['vinv'] = $this->db->query("SELECT
		inv.nomor,
		inv.tgljthtempo,
		inv.tanggal,
		so.kodesalesman,
		inv.lokasi,
		c.nama as namapelanggan,
		c.nama_toko,
		c.pengiriman,
		c.alamat,
		c.notelp,
		c.nohp,
		pos.kota,
		so.jenisdisc,
		so.jenisjual,
		pro.nama as namapromo,
		a.nama as namaaccount,
		a.norekening,
		al.nama as namaaccountlainlain,
		inv.total,
		inv.dpp,
		inv.ppn,
		inv.grandtotal,
		inv.reprint,
		hist.revisike
		FROM
		srvt_invoice inv
		LEFT JOIN srvt_salesorder so ON so.nomor = inv.nomorso
		LEFT JOIN hist_cancel hist ON hist.noinvoice = inv.nomor
		LEFT JOIN glbm_promo pro ON pro.kode = so.jenisdisc
		LEFT JOIN glbm_gudang g ON g.kode = inv.lokasi
		LEFT JOIN glbm_customer c ON c.nomor = inv.nopelanggan
		LEFT JOIN glbm_account a ON a.nomor = c.rekeningtf
		LEFT JOIN glbm_kodepos pos ON pos.kodepos = c.kodepos
		LEFT JOIN glbm_accountlainlain al ON al.nomor = a.nomor
		WHERE inv.nomor = '" . $nomor . "'
		ORDER BY hist.revisike DESC
		LIMIT 1")->row();

		$data['vinvd'] = $this->db->query("SELECT
			inv.nomor,
			so.nilaicash,
			so.nilaicustomer,
			invd.namabarang,
			s.nama as namasatuan,
			inv.disccash,
			invd.qty,
			invd.harga,
			invd.discpaket,
			invd.discpaketitem,
			invd.discbarang,
			invd.discbrgitem,
			invd.total
		FROM
			srvt_invoice inv
			LEFT JOIN srvt_salesorder so ON so.nomor = inv.nomorso
			LEFT JOIN srvt_invoicedetail invd ON invd.nomor = inv.nomor
			LEFT JOIN glbm_barang b ON b.kode = invd.kodebarang
		LEFT JOIN glbm_satuan s ON s.kode = b.kodesatuan
		WHERE inv.nomor = '" . $nomor . "'
		ORDER BY invd.qty DESC")->result();

		$orientation = 'portrait';
		$filename = 'Invoice - ' . $data['inv']->nomor;

		$this->load->library('pdf');
		$this->pdf->load_view('menu/cetakan_pdf/invoice', $data, $filename, $paper, $orientation);

		$this->pdf->setPaper($paper, 'portrait');
		$this->pdf->rander();
		$this->pdf->stream("welcome.pdf", array("Attachment" => 0));

		exit(0);
	}
}
