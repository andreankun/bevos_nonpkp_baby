<?php

header("Content-type: application/vnd-ms-excel");

header("Content-Disposition: attachment; filename=Laporan Sales Order.xls");

header("Pragma: no-cache");

header("Expires: 0");

function FormatRupiah($angka)
{
    $hasil_rupiah = number_format($angka, 0, '.', ',');
    return $hasil_rupiah;
}
?>

<style>
    table thead th,
    table tbody td {
        border: 1px solid #3c3c3c;

    }
</style>
<h4 style="text-align: center;">LAPORAN SALES ORDER</h4>
CABANG : <?php echo $row->nama ?>
<br>
PERIODE : <?php echo $tglawal ?> S/D <?php echo $tglakhir ?>
<br>

<table border="1" width="100%">

    <thead>
        <tr>
            <th style="text-align: center;">NO.</th>
            <th style="text-align: center;">SALES ORDER</th>
            <th style="text-align: center;">TANGGAL</th>
            <th style="text-align: center;">NAMA PELANGGAN</th>
            <th style="text-align: center;">NAMA TOKO</th>
            <th style="text-align: center;">USER</th>
            <th style="text-align: center;">TOTAL</th>
        </tr>
    </thead>

    <tbody>
        <?php if (!empty($excel)) : ?>
            <?php $no = 1;
            foreach ($excel as $val) : ?>

                <tr>
                    <td style="text-align:right;"><?php echo $no++; ?></td>
                    <td style="text-align:left;"><?php echo $val->nomor; ?></td>
                    <td style="text-align: right;"><?php echo date('d/m/Y', strtotime($val->tanggal)); ?></td>
                    <td style="text-align:left;"><?php echo $val->nama; ?></td>
                    <td style="text-align:left;"><?php echo $val->nama_toko; ?></td>
                    <td style="text-align:left;"><?php echo $val->pemakai; ?></td>
                    <td style="text-align:right;"><?php echo FormatRupiah($val->grandtotal); ?></td>
                </tr>

            <?php endforeach; ?>
        <?php else :  ?>
            <tr>
                <td colspan="7" style="font-weight: bold; text-align: center;">Data tidak tersedia.</td>
            </tr>
        <?php endif; ?>

    </tbody>

</table>