<?php

header("Content-type: application/vnd-ms-excel");

header("Content-Disposition: attachment; filename=Laporan Penjualan PerPelanggan & PerBarang.xls");

header("Pragma: no-cache");

header("Expires: 0");

function FormatRupiah($angka)
{
    $hasil_rupiah = number_format($angka, 0, '.', ',');
    return $hasil_rupiah;
}
?>

<style>
    table thead th,
    table tbody td {
        border: 1px solid #3c3c3c;

    }
</style>
<h4 style="text-align: center;">LAPORAN PENJUALAN PERPELANGGAN & PERBARANG</h4>
CABANG : <?php echo $cabang->nama ?>
<br>
PERIODE : <?php echo $tglawal ?> S/D <?php echo $tglakhir ?>
<br>

<table border="1" width="100%">

    <thead>
        <tr>
            <th style="text-align: center; text-transform: uppercase;">Tanggal</th>
            <th style="text-align: center; text-transform: uppercase;">No.Faktur</th>
            <th style="text-align: center; text-transform: uppercase;">Nama Barang</th>
            <th style="text-align: center; text-transform: uppercase;">Qty</th>
            <th style="text-align: center; text-transform: uppercase;">Harga</th>
            <th style="text-align: center; text-transform: uppercase;">Disc</th>
            <th style="text-align: center; text-transform: uppercase;">Jumlah</th>
        </tr>
    </thead>

    <tbody>
        <?php if (!empty($penjualanperpelangganperbarang)) : ?>
            <?php
            foreach ($penjualanperpelangganperbarang as $value) :
            ?>
                <tr>
                    <td style="text-align: left;"><?= date('d/m/Y', strtotime($value->tanggal)) ?></td>
                    <td style="text-align: left;"><?= $value->nomor ?></td>
                    <td style="text-align: left;"><?= $value->namabarang ?></td>
                    <td style="text-align: right;"><?= FormatRupiah($value->qty) ?></td>
                    <td style="text-align: right;"><?= FormatRupiah($value->harga) ?></td>
                    <td style="text-align: right;"><?= $value->discbarang ?>%</td>
                    <td style="text-align: right;"><?= FormatRupiah($value->total) ?></td>
                </tr>
            <?php endforeach; ?>
        <?php else : ?>
            <tr>
                <td style="text-align: center;" colspan="7">Data tidak tersedia.</td>
            </tr>
        <?php endif; ?>

    </tbody>

</table>