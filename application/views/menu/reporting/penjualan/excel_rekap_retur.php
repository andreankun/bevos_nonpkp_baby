<?php

header("Content-type: application/vnd-ms-excel");

header("Content-Disposition: attachment; filename=Laporan Rekap Retur.xls");

header("Pragma: no-cache");

header("Expires: 0");

function FormatRupiah($angka)
{
    $hasil_rupiah = number_format($angka, 0, '.', ',');
    return $hasil_rupiah;
}
?>
<h4 style="text-align: center;">LAPORAN REKAP RETUR</h4>
CABANG : <?php echo $row->nama ?>
<br>
PERIODE : <?php echo $tglawal ?> S/D <?php echo $tglakhir ?>
<br>
<table border="1" width="100%">

    <thead>
        <tr>
            <th>NO.</th>
            <th>NO. RETUR</th>
            <th>TANGGAL</th>
            <th>NAMA PELANGGAN</th>
            <th>NAMA TOKO</th>
            <th>SUBTOTAL</th>
            <th>DISKON</th>
            <th>DPP</th>
            <th>PPN</th>
            <th>TOTAL</th>
        </tr>
    </thead>

    <tbody>
        <?php
        $no = 1;
        $subtotal = 0;
        $grandtotal = 0;
        $total_diskon = 0;
        $dpp = 0;
        $hasilppn = 0;
        $hsubtotal = 0;
        $htotal_diskon = 0;
        $hdpp = 0;
        $hhasilppn = 0;
        $hgrandtotal = 0;
        foreach ($report as $vals) :
            $subtotal = $vals->subtotal - $vals->discbrgitem;
            $total_diskon = $vals->discpaketitem + $vals->nilaicash + $vals->nilaicustomer;
            $dpp = $vals->dpp;
            // print_r($ppn->ppn);
            // die();
            // $hasilppn = floor($vals->dpp * ($ppn->ppn / 100));
            $hasilppn = $vals->ppn;
            $grandtotal = $vals->dpp + $hasilppn;
        ?>
            <tr>
                <td style="text-align: center;"><?= $no++; ?></td>
                <td style="text-align: left;"><?= $vals->nomor ?></td>
                <td style="text-align: right;"><?= date('d/m/Y', strtotime($vals->tanggal)) ?></td>
                <td style="text-align: left;"><?= $vals->namapelanggan ?></td>
                <td style="text-align: left;"><?= $vals->nama_toko ?></td>
                <td style="text-align: right;"><?= FormatRupiah($subtotal) ?></td>
                <td style="text-align: right;"><?= FormatRupiah($total_diskon) ?></td>
                <td style="text-align: right;"><?= FormatRupiah($dpp) ?></td>
                <td style="text-align: right;"><?= FormatRupiah($hasilppn) ?></td>
                <td style="text-align: right;"><?= FormatRupiah($grandtotal) ?></td>
                <?php $hsubtotal = $hsubtotal + $subtotal; ?>
                <?php $htotal_diskon = $htotal_diskon + $total_diskon; ?>
                <?php $hdpp = $hdpp + $dpp; ?>
                <?php $hhasilppn = $hhasilppn + $hasilppn; ?>
                <?php $hgrandtotal = $hgrandtotal + $grandtotal; ?>
            </tr>
        <?php endforeach; ?>
        <tr style="float: right; text-align: right; font-weight: bold; text-transform: uppercase;">
            <td colspan="5">Grand Total : </td>
            <td colspan="1"><?= FormatRupiah($hsubtotal) ?></td>
            <td colspan="1"><?= FormatRupiah($htotal_diskon) ?></td>
            <td colspan="1"><?= FormatRupiah($hdpp) ?></td>
            <td colspan="1"><?= FormatRupiah($hhasilppn) ?></td>
            <td colspan="1"><?= FormatRupiah($hgrandtotal) ?></td>
        </tr>
    </tbody>
</table>