<?php

header("Content-type: application/vnd-ms-excel");

header("Content-Disposition: attachment; filename=Laporan Penjualan Per Salesman.xls");

header("Pragma: no-cache");

header("Expires: 0");

function FormatRupiah($angka)
{
    $hasil_rupiah = number_format($angka, 0, '.', ',');
    return $hasil_rupiah;
}
?>
<h4 style="text-align: center;">LAPORAN PENJUALAN PER SALESMAN</h4>
CABANG : <?php echo $row->nama ?>
<br>
PERIODE : <?php echo $tglawal ?> S/D <?php echo $tglakhir ?>
<br>
<?php if (!empty($reportrow)) : ?>
    NAMA SALES : <?php echo $reportrow->namasalesman ?>
<?php else : ?>
    NAMA SALES : -
<?php endif; ?>

<br>
<table border="1" width="100%">

    <thead>
        <tr>
            <th>NO.</th>
            <th>FAKTUR</th>
            <th>TANGGAL</th>
            <th>SALES ORDER</th>
            <th>PELANGGAN</th>
            <th>TOTAL PENJUALAN</th>
            <th>PEMBAYARAN</th>
            <th>SALDO</th>
        </tr>
    </thead>

    <tbody>
        <?php if (!empty($report)) : ?>
            <?php
            $no = 1;
            $grandtotal = 0;
            foreach ($report as $val) : ?>
                <tr>
                    <td style="text-align: center;"><?= $no++; ?></td>
                    <td style="text-align: left;"><?= $val->nomor ?></td>
                    <td style="text-align: left;"><?= date('d-m-Y', strtotime($val->tanggal)) ?></td>
                    <td style="text-align: left;"><?= $val->nomorso ?></td>
                    <td style="text-align: left;"><?= $val->namapelanggan ?></td>
                    <td style="text-align: right;"><?= FormatRupiah($val->grandtotal) ?></td>
                    <td></td>
                    <td style="text-align: right;"><?= FormatRupiah($val->grandtotal) ?></td>
                </tr>
                <?php $grandtotal = $grandtotal + $val->grandtotal; ?>
            <?php endforeach; ?>
            <tr style="float: right; text-align: right; font-weight: bold;">
                <td colspan="5">Grand Total : </td>
                <td colspan="1"><?= FormatRupiah($grandtotal) ?></td>
                <td colspan="1"></td>
                <td colspan="1"><?= FormatRupiah($grandtotal) ?></td>
            </tr>
        <?php else : ?>
            <tr>
                <td colspan="8">
                    Data tidak tersedia.
                </td>
            </tr>
        <?php endif; ?>
    </tbody>
</table>