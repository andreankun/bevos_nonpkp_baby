<style>
    .logo-menu{
        background: #11101D;
        width: 80px;
        height: 80px;
        border-radius: 8px;
        padding: 5px 10px;
        color: #FFF;
        margin-right: 5px;
    }
    .modal-content{
        border-color: #EEE;
    }
    .hidescroll {
        scrollbar-width: none !important;
    }
    .hidescroll::-webkit-scrollbar {
        display: none !important;
    }
    .table-responsive {
        scrollbar-width: thin;
        scrollbar-color: #47404F #DEDEDE;
    }
    .table-responsive::-webkit-scrollbar {
        width: 10px;
        height: 10px;
    }

    .table-responsive::-webkit-scrollbar-track {
        background-color: #DEDEDE;
        border-radius: 100px;
    }

    .table-responsive::-webkit-scrollbar-thumb {
        border-radius: 100px;
        background-color: #47404F;
    }
</style>
<div class="col-md-12 mb-2">
    <div class="modal-header" style="padding: 5px;">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-3">
                    <h2>
                        <span class="logo-menu"><?php echo $icons ?></span>
                        <span><?php echo $title ?></span>
                    </h2>
                </div>
                <div class="col-md-9">
                    <div class="form-group" style="float: right;">
                        <button id="print" class="btn btn-sm btn-dark mb-1 mt-1 mr-1 ml-1"><i class="far fa-print" style="margin-right: 10px;"></i>Print</button>
                        <button id="clear" class="btn btn-sm btn-dark mb-1 mt-1 mr-1 ml-1"><i class="far fa-refresh" style="margin-right: 10px;"></i>Clear</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-md-12 table-responsive hidescroll" style="height: 555px; overflow-y: scroll;">
    <div class="row">
        <div class="col-md-6">
            <div class="modal-content mb-2">
                <div class="modal-body">
                    <div class="row mb-2 mt-2">
                        <div class="col-sm-6 input-group box">
                            <div class="col-sm-6 p-0 input-group-prepend">
                                <span class="col-sm-12 input-group-text">Laporan Dokumen</span>
                            </div>
							<select class="form-control" style="height: 29px; padding: 0.175rem 0.50rem;" id="jenis_laporan">
								<option value="">Pilih Laporan</option>
								<option value="1">Sales Order</option>
								<option value="2">Faktur</option>
							</select>
                        </div>
                    </div>
                    <div class="row mb-2">
                        <div class="col-sm-6 input-group">
                            <div class="col-sm-6 p-0 input-group-prepend">
                                <span class="col-sm-12 input-group-text">Tanggal Awal</span>
                            </div>
                            <input class="form-control" id="tglawal" value = "<?php echo date('Y-m-d'); ?>" padding="5px" type="date" required/>
                        </div>
						<div class="col-sm-6 input-group">
							<div class="col-sm-6 p-0 input-group-prepend">
								<span class="col-sm-12 input-group-text">Tanggal Akhir</span>
							</div>
							<input class="form-control" id="tglakhir" value = "<?php echo date('Y-m-d'); ?>" padding="5px" type="date" required />
						</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
