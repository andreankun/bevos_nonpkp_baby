<?php

header("Content-type: application/vnd-ms-excel");

header("Content-Disposition: attachment; filename=Laporan Master Customer.xls");

header("Pragma: no-cache");

header("Expires: 0");

function FormatRupiah($angka)
{
	$hasil_rupiah = number_format($angka, 0, '.', ',');
	return $hasil_rupiah;
}
?>
<h4 style="text-align: center;">LAPORAN MASTER CUSTOMER</h4>
<br>

<table border="1" width="100%">

	<thead>
		<tr>
			<th>No</th>
			<th>Kode</th>
			<th>Nama Pelanggan</th>
			<th>Alamat</th>
			<th>Cabang</th>
		</tr>
	</thead>
	<tbody>
		<?php
		$no = 1;
		foreach ($report as $val) : ?>
			<tr>
				<td><?= $no++ ?></td>
				<td><?= $val->nomor ?></td>
				<td><?= $val->nama ?></td>
				<td><?= $val->alamat ?></td>
				<td><?= $val->namacabang ?></td>
			</tr>
		<?php endforeach; ?>
	</tbody>

</table>
