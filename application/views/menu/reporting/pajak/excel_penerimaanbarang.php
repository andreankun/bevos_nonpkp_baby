<?php

header("Content-type: application/vnd-ms-excel");

header("Content-Disposition: attachment; filename=Laporan Penerimaan Barang.xls");

header("Pragma: no-cache");

header("Expires: 0");

function FormatRupiah($angka)
{
    $hasil_rupiah = number_format($angka, 0, ',', '.');
    return $hasil_rupiah;
}
function ClearChr($param)
{
    return $result = str_replace(array('\'', '"', ',', ';', '<', '>', '_', '-', ' ', '.', '!', '?', '/', '=', '+', ']', '[', '{', '}', '@', '#', '$', '%', '^', '&', "*", '(', ')'), '', $param);
}
?>
<h4 style="text-align: center;">LAPORAN PENERIMAAN bARANG</h4>
GUDANG : <?php echo $kodegudang->nama ?>
<br>
PERIODE : <?php echo $tglawal ?> S/D <?php echo $tglakhir ?>
<br>
<table border="1" width="100%">

    <thead>
        <tr>
            <th style="text-transform: uppercase;">No</th>
            <th style="text-transform: uppercase;">NO.LPB</th>
            <th style="text-transform: uppercase;">Tanggal</th>
            <th style="text-transform: uppercase;">Kode Barang</th>
            <th style="text-transform: uppercase;">Nama Barang</th>
            <th style="text-transform: uppercase;">Qty</th>
            <th style="text-transform: uppercase;">Satuan</th>
            <th style="text-transform: uppercase;">Supplier</th>
            <th style="text-transform: uppercase;">Kode Gudang</th>
            <th style="text-transform: uppercase;">No Supplier</th>
        </tr>
    </thead>

    <tbody>
        <?php
        $no = 0;
        foreach ($report as $vals) :
        ?>
            <tr>
                <td style="text-align: left; border: 1px solid #dbdbdb;border-width: thin;"><?= $no++; ?></td>
                <td style="text-align: left; border: 1px solid #dbdbdb;border-width: thin;"><?= $vals->nomorpo ?></td>
                <td style="text-align: left; border: 1px solid #dbdbdb;border-width: thin;"><?= date('Y/m/d', strtotime($vals->tanggal)) ?></td>
                <td style="text-align: left; border: 1px solid #dbdbdb;border-width: thin;"><?= $vals->kodebarang; ?></td>
                <td style="text-align: left; border: 1px solid #dbdbdb;border-width: thin;"><?= $vals->namabarang ?></td>
                <td style="text-align: right; border: 1px solid #dbdbdb;border-width: thin;"><?= $vals->qty ?></td>
                <td style="text-align: left; border: 1px solid #dbdbdb;border-width: thin;"><?= $vals->kodesatuan ?></td>
                <td style="text-align: left; border: 1px solid #dbdbdb;border-width: thin;"><?= $vals->namasupplier ?></td>
                <td style="text-align: left; border: 1px solid #dbdbdb;border-width: thin;"><?= $vals->kodegudang ?></td>
                <td style="text-align: left; border: 1px solid #dbdbdb;border-width: thin;"><?= $vals->nosupplier ?></td>
               
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>