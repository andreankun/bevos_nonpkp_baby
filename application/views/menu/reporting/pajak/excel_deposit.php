<?php

header("Content-type: application/vnd-ms-excel");

header("Content-Disposition: attachment; filename=Deposit.xls");

header("Pragma: no-cache");

header("Expires: 0");


function FormatRupiah($angka)
{
    $hasil_rupiah = number_format($angka, 0, '.', ',');
    return $hasil_rupiah;
}
?>
<!-- <h4 style="text-align: center;">LAPORAN SALES ORDER DETAIL</h4> -->
<!-- CABANG : <-?php echo $row->nama ?> -->
<!-- <br>
PERIODE : <?php echo $tglawal ?> S/D <?php echo $tglakhir ?>
<br> -->

<table border="1" width="100%">

    <thead>
        <tr>
            <th>KODE PAJAK</th>
            <th>KODE CUSTOMER</th>
            <th>AKUN</th>
            <th>TANGGAL</th>
            <th>KETERANGAN GL</th>
            <th>KREDIT</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $i = 1;
        foreach ($detail as $value) :
            $tanggal = date('m-d', strtotime($value->tanggal));
            $nomor = 'D' . $kodecabang . '-' . $tanggal . '-';
        ?>
            <tr>
                <td><?= $nomor . sprintf('%04d', $i++) ?></td>
                <td><?= $kodecabang . $value->nocustomer ?></td>
                <!-- <td><-?= $value->kodeaccount ?></td> -->
                <td>103.999</td>
                <td><?= date('Y-m-d', strtotime($value->tanggal)) ?></td>
                <td>DEPOSIT - <?= $value->namapelanggan ?></td>
                <td><?= $value->nilaialokasi ?></td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>