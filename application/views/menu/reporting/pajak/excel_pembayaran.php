<?php

header("Content-type: application/vnd-ms-excel");

header("Content-Disposition: attachment; filename=Laporan Pembayaran Piutang.xls");

header("Pragma: no-cache");

header("Expires: 0");

function FormatRupiah($angka)
{
	$hasil_rupiah = number_format($angka, 0, '.', ',');
	return $hasil_rupiah;
}
?>
<h4 style="text-align: center;">LAPORAN PEMBAYARAN PIUTANG</h4>
<!-- CABANG : <-?php echo $cabang ?> -->
<br>

<table border="1" width="100%">

	<thead>
		<tr>
			<th>No Bukti Bayar</th>
			<th>Kode</th>
			<th>Kode Pelanggan</th>
			<th>Akun Kas</th>
			<th>Tanggal Bayar</th>
			<th>Tanggal Cek Giro</th>
			<th>Keterangan</th>
			<th>No Faktur</th>
			<th>Keterangan Detail</th>
			<th>Nilai Faktur</th>
			<th>Customer</th>
		</tr>
	</thead>
	<tbody>
		<?php
		$no = 1;
		foreach ($report as $val) : ?>
			<tr>
				<td><?= $val->nomor ?></td>
				<td></td>
				<td><?= $val->nocustomer ?></td>
				<td><?= $val->kodeaccount ?></td>
				<td><?= date('Y/m/d', strtotime($val->tanggal)) ?></td>
				<td></td>
				<td></td>
				<td><?= $val->noreferensi ?></td>
				<td></td>
				<td><?= $val->nilaipiutang ?></td>
				<td><?= $val->namacustomer ?></td>
			</tr>
		<?php endforeach; ?>
	</tbody>

</table>
