<script type="text/javascript">
	$(document).ready(function() {

		$('#jenis_laporan').change(function() {
			var jenisdokumen = this.value;
			if (jenisdokumen == '1') {
				$('.tanggal').hide();
				$('#preview').hide();
				$('.cabang').hide();
			} else if (jenisdokumen == '2') {
				$('.tanggal').show();
				$('#preview').hide();
				$('.cabang').hide();
			} else if (jenisdokumen == '3') {
				$('.tanggal').show();
				$('.cabang').hide();
				$('#preview').hide();
			} else if (jenisdokumen == '4') {
				$('.pelanggan').hide();
				$('.barang').hide();
				$('.salesman').hide();
				$('.cabang').show();
				$('.gudang').show();
				$('.tanggal').show();
				$('#preview').hide();
			} else if (jenisdokumen == '5') {
				$('.tanggal').hide();
			} else if (jenisdokumen == '6') {
				$('.pelanggan').hide();
				$('.barang').hide();
				$('.salesman').hide();
				$('.cabang').show();
				$('.gudang').hide();
				$('.tanggal').show();
				$('#preview').hide();
			} else if (jenisdokumen == '7') {
				$('.pelanggan').hide();
				$('.barang').hide();
				$('.salesman').hide();
				$('.cabang').show();
				$('.gudang').hide();
				$('.tanggal').show();
				$('#print').hide();
				$('#preview').hide();
			} else if (jenisdokumen == '8') {
				$('.pelanggan').hide();
				$('#print').hide();
				$('.barang').hide();
				$('.salesman').hide();
				$('.cabang').show();
				$('.gudang').show();
				$('.tanggal').show();
				$('#preview').hide();
				$('#carikodecabang').show();
			} else if (jenisdokumen == '9') {
				$('.pelanggan').hide();
				$('#print').hide();
				$('.barang').hide();
				$('.salesman').hide();
				$('.cabang').hide();
				$('.gudang').show();
				$('.tanggal').show();
				$('#preview').hide();
				$('#carikodecabang').show();
			}else{
				$('.pelanggan').hide();
				// $('#print').hide();
				$('.barang').hide();
				$('.salesman').hide();
				$('.cabang').hide();
				$('.gudang').hide();
				$('.tanggal').hide();
			}
		})

		$('#tglawal').datepicker({
			format: "yyyy-mm-dd",
			autoclose: true,
			todayHightlight: true,
			startDate: '-100y'
		});

		$('#tglakhir').datepicker({
			format: "yyyy-mm-dd",
			autoclose: true,
			todayHightlight: true,
			startDate: '-100y'
		});

		/* Cari Data Cabang*/
		document.getElementById("carikodecabang").addEventListener("click", function(event) {
			event.preventDefault();
			$('#t_cabang').DataTable({
				"destroy": true,
				"searching": true,
				"processing": true,
				"serverSide": true,
				"lengthChange": true,
				"pageLength": 5,
				"lengthMenu": [5, 10, 25, 50],
				"order": [],
				"ajax": {
					"url": "<?= base_url('masterdata/Cabang/CariDataCabang'); ?>",
					"method": "POST",
					"data": {
						nmtb: "glbm_cabang",
						field: {
							kode: "kode",
							nama: "nama",
							// alamat: "alamat"
						},
						sort: "kode",
						where: {
							kode: "kode",
							nama: "nama",
							// alamat: "alamat"
						},
						value: "aktif = true"
					},
				}
			});
		});

		/*Get Data Cabang*/
		$(document).on('click', ".searchcabang", function() {
			var kode = $(this).attr("data-id");
			$.ajax({
				url: "<?= base_url('masterdata/Cabang/DataCabang'); ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					kode: kode
				},
				success: function(data) {
					for (var i = 0; i < data.length; i++) {
						$('#kodecabang').val(data[i].kode.trim());
					}
				}
			}, false);
		});

		 /* Cari Data Cabang*/
		 document.getElementById("carikodegudang").addEventListener("click", function(event) {
            event.preventDefault();
            $('#t_gudang').DataTable({
                "destroy": true,
                "searching": true,
                "processing": true,
                "serverSide": true,
                "lengthChange": true,
                "pageLength": 5,
                "lengthMenu": [5, 10, 25, 50],
                "order": [],
                "ajax": {
                    "url": "<?= base_url('masterdata/Gudang/CariDataGudang'); ?>",
                    "method": "POST",
                    "data": {
                        nmtb: "glbm_gudang",
                        field: {
                            kode: "kode",
                            nama: "nama",
                            // alamat: "alamat"
                        },
                        sort: "kode",
                        where: {
                            kode: "kode",
                            nama: "nama",
                            // alamat: "alamat"
                        },
                        value: "aktif = true"
                    },
                }
            });
        });

		 /*Get Data Cabang*/
		 $(document).on('click', ".searchgudang", function() {
            var kode = $(this).attr("data-id");;
            $.ajax({
                url: "<?= base_url('masterdata/Gudang/DataGudang'); ?>",
                method: "POST",
                dataType: "json",
                async: false,
                data: {
                    kode: kode
                },
                success: function(data) {
                    for (var i = 0; i < data.length; i++) {
                        $('#kodegudang').val(data[i].kode.trim());
                    }
                }
            }, false);
        });

		document.getElementById('export').addEventListener("click", function(event) {
			event.preventDefault()
			var tglawal = $('#tglawal').val();
			var tglakhir = $('#tglakhir').val();
			var jenislaporan = $('#jenis_laporan').val();
			var cabang = $('#kodecabang').val();
			var gudang = $('#kodegudang').val();
			if (jenislaporan == '1') {
				window.open(
					"<?php echo base_url('cetak_pdf/LaporanPajak/ExportLaporanCustomer/') ?>" + cabang
				);
			} else if (jenislaporan == '2') {
				window.open(
					"<?php echo base_url('cetak_pdf/LaporanPajak/ExportInvoicePajak/') ?>" + tglawal + ":" + tglakhir + ":" + cabang
				);
			} else if (jenislaporan == '3') {
				window.open(
					"<?php echo base_url('cetak_pdf/LaporanPajak/ExportPembayaranPiutang/') ?>" + tglawal + ":" + tglakhir + ":" + cabang
				);
			} else if (jenislaporan == '4') {
				window.open(
					"<?php echo base_url('cetak_pdf/LaporanPajak/ExportPenjualan/') ?>" + tglawal + ":" + tglakhir + ":" + cabang
				);
			} else if (jenislaporan == '5') {
				window.open(
					"<?php echo base_url('cetak_pdf/LaporanPajak/ExportMasterBarang/') ?>" + cabang
				);
			} else if (jenislaporan == '6') {
				window.open(
					"<?= base_url('cetak_pdf/LaporanPajak/ExportRekapPPnPenjualan/'); ?>" + tglawal + ":" + tglakhir
				)
			} else if (jenislaporan == '7') {
				window.open(
					"<?= base_url('cetak_pdf/LaporanPajak/ExportEfaktur/'); ?>" + tglawal + ":" + tglakhir
				)
			} else if (jenislaporan == '8') {
				window.open(
					"<?= base_url('cetak_pdf/LaporanPajak/ExportDeposit/'); ?>" + tglawal + ":" + tglakhir + ":" + cabang
				)
			}else if (jenislaporan == '9') {
				window.open(
					"<?= base_url('cetak_pdf/LaporanPajak/ExportPenerimaan/'); ?>" + tglawal + ":" + tglakhir + ":" + gudang
				)
			}
		});

		document.getElementById("preview").addEventListener("click", function(event) {
			event.preventDefault();
			var tglawal = $('#tglawal').val();
			var tglakhir = $('#tglakhir').val();
			var jenislaporan = $('#jenis_laporan').val();
			var cabang = $('#kodecabang').val();
			if (jenislaporan == '1') {
				window.open(
					"<?php echo base_url('cetak_pdf/LaporanPajak/PreviewLaporanCustomer/') ?>" + cabang
				);
			} else if (jenislaporan == '2') {
				window.open(
					"<?php echo base_url('cetak_pdf/LaporanPajak/PreviewInvoicePajak/') ?>" + tglawal + ":" + tglakhir + ":" + cabang
				);
			} else if (jenislaporan == '3') {
				window.open(
					"<?php echo base_url('cetak_pdf/LaporanPajak/PreviewPembayaranPiutang/') ?>" + tglawal + ":" + tglakhir + ":" + cabang
				);
			} else if (jenislaporan == '5') {
				window.open(
					"<?php echo base_url('cetak_pdf/LaporanPajak/PreviewLaporanMasterBarang/') ?>" + cabang
				);
			}
		});

		function RefreshLayar() {
			location.reload(true);
		};


		document.getElementById("clear").addEventListener("click", function(event) {
			event.preventDefault();
			RefreshLayar();
		});
	});
</script>
