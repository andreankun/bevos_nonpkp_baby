<?php

header("Content-type: application/vnd-ms-excel");

header("Content-Disposition: attachment; filename=Export Penjualan.xls");

header("Pragma: no-cache");

header("Expires: 0");

function FormatRupiah($angka)
{
    $hasil_rupiah = number_format($angka, 0, '.', ',');
    return $hasil_rupiah;
}
function ClearChr($param)
{
    return $result = str_replace(array('\'', '"', ',', ';', '<', '>', '_', '-', ' ', '.', '!', '?', '/', '=', '+', ']', '[', '{', '}', '@', '#', '$', '%', '^', '&', "*", '(', ')'), '', $param);
}
?>
<table border="1" width="100%">

    <thead>
        <tr>
            <th>No Faktur</th>
            <th>Tanggal</th>
            <th>Kode Pelanggan</th>
            <th>Kena Pajak</th>
            <th>No PPN</th>
            <th>Nama Termin</th>
            <th>Diskon Faktur</th>
            <th>Keterangan</th>
            <th>Kode Barang</th>
            <th>Keterangan Detail</th>
            <th>Jumlah</th>
            <th>Harga</th>
            <th>Diskon</th>
            <th>Kode Gudang</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($report as $row) : ?>
            <tr>
                <td><?= $row->nomor ?></td>
                <td><?= date('d/m/Y', strtotime($row->tanggal)) ?></td>
                <td><?= $row->nopelanggan ?></td>
                <td>1</td>
                <td style="vnd.ms-excel.numberformat:@"><?= $row->nofakturpajak ?></td>
                <td><?= $row->termin ?></td>
                <td><?= intval($row->nilaicustomer + $row->nilaicash) ?></td>
                <td></td>
                <td><?= $row->kodebarang ?></td>
                <td></td>
                <td><?= intval($row->qty) ?></td>
                <td><?= intval($row->harga) ?></td>
                <td><?= intval($row->discbrgitem + $row->discpaketitem) ?></td>
                <td><?= $row->lokasi ?></td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
