<?php

header("Content-type: application/vnd-ms-excel");

header("Content-Disposition: attachment; filename=Laporan Rekap PPn Penjualan.xls");

header("Pragma: no-cache");

header("Expires: 0");

function FormatRupiah($angka)
{
    $hasil_rupiah = number_format($angka, 0, '.', ',');
    return $hasil_rupiah;
}
function ClearChr($param)
{
    return $result = str_replace(array('\'', '"', ',', ';', '<', '>', '_', '-', ' ', '.', '!', '?', '/', '=', '+', ']', '[', '{', '}', '@', '#', '$', '%', '^', '&', "*", '(', ')'), '', $param);
}
?>
<h4 style="text-align: center;">LAPORAN REKAP PPN PENJUALAN</h4>
PERIODE : <?php echo $tglawal ?> S/D <?php echo $tglakhir ?>
<br>
<table border="1" width="100%">

    <thead>
        <tr>
            <th style="text-transform: uppercase;">No.</th>
            <th style="text-transform: uppercase;">Invoice</th>
            <th style="text-transform: uppercase;">Seri Faktur Pajak</th>
            <th style="text-transform: uppercase;">Nomor Pelanggan</th>
            <th style="text-transform: uppercase;">Pelanggan</th>
            <th style="text-transform: uppercase;">Nama Toko</th>
            <th style="text-transform: uppercase;">Tanggal</th>
            <th style="text-transform: uppercase;">Subtotal</th>
            <th style="text-transform: uppercase;">Diskon</th>
            <th style="text-transform: uppercase;">DPP</th>
            <th style="text-transform: uppercase;">PPN</th>
            <th style="text-transform: uppercase;">Total</th>
        </tr>
    </thead>

    <tbody>
        <?php
        $no = 1;
        $subtotal = 0;
        $grandtotal = 0;
        $total_diskon = 0;
        $dpp = 0;
        $hasilppn = 0;
        $hsubtotal = 0;
        $htotal_diskon = 0;
        $hdpp = 0;
        $hhasilppn = 0;
        $hgrandtotal = 0;
        foreach ($report as $vals) :
            // $subtotal = $vals->subtotal - $vals->discbrgitem;
            $subtotal = $vals->subtotal;
            $total_diskon = $vals->discbrgitem + $vals->discpaketitem + $vals->nilaicash + $vals->nilaicustomer;
            $dpp = $vals->dpp;
            // print_r($ppn->ppn);
            // die();
            // $hasilppn = floor($vals->dpp * ($ppn->ppn / 100));
            $grandtotal = $vals->dpp + $vals->ppn;
        ?>
            <tr>
                <td style="text-align: center; border: 1px solid #dbdbdb;border-width: thin;"><?= $no++; ?></td>
                <td style="text-align: left; border: 1px solid #dbdbdb;border-width: thin;"><?= $vals->nomor ?></td>
                <?php if (!empty($vals->nofakturpajak)) : ?>
                    <td style="text-align: left; border: 1px solid #dbdbdb;border-width: thin; vnd.ms-excel.numberformat:@"><?= ClearChr($vals->nofakturpajak) ?></td>
                <?php else : ?>
                    <td style="text-align: left; border: 1px solid #dbdbdb;border-width: thin;"></td>
                <?php endif; ?>
                <td style="text-align: left; border: 1px solid #dbdbdb;border-width: thin;"><?= $vals->kodecabang . $vals->nopelanggan ?></td>
                <td style="text-align: left; border: 1px solid #dbdbdb;border-width: thin;"><?= $vals->namapelanggan ?></td>
                <td style="text-align: left; border: 1px solid #dbdbdb;border-width: thin;"><?= $vals->nama_toko ?></td>
                <td style="text-align: center; border: 1px solid #dbdbdb;border-width: thin;"><?= date('d-m-Y', strtotime($vals->tanggal)) ?></td>
                <td style="text-align: right; border: 1px solid #dbdbdb;border-width: thin;"><?= $subtotal ?></td>
                <?php if ($total_diskon == 0) : ?>
                    <td style="text-align: right; border: 1px solid #dbdbdb;border-width: thin;"><?= 0 ?></td>
                <?php else : ?>
                    <td style="text-align: right; border: 1px solid #dbdbdb;border-width: thin;"><?= $total_diskon ?></td>
                <?php endif; ?>
                <td style="text-align: right; border: 1px solid #dbdbdb;border-width: thin;"><?= $dpp ?></td>
                <td style="text-align: right; border: 1px solid #dbdbdb;border-width: thin;"><?= floor($vals->dpp * ($ppn->ppn / 100)) ?></td>
                <td style="text-align: right; border: 1px solid #dbdbdb;border-width: thin;"><?= $grandtotal ?></td>
                <?php $hsubtotal = $hsubtotal + $subtotal; ?>
                <?php $htotal_diskon = $htotal_diskon + $total_diskon; ?>
                <?php $hdpp = $hdpp + $dpp; ?>
                <?php $hhasilppn = $hhasilppn + $hasilppn; ?>
                <?php $hgrandtotal = $hgrandtotal + $grandtotal; ?>
            </tr>
        <?php endforeach; ?>
        <!-- <tr style="float: right; text-align: right; border: 1px solid #dbdbdb;border-width: thin; font-weight: bold; text-transform: uppercase;">
            <td colspan="7">Grand Total : </td>
            <td colspan="1"><?= FormatRupiah($hsubtotal) ?></td>
            <td colspan="1"><?= FormatRupiah($htotal_diskon) ?></td>
            <td colspan="1"><?= FormatRupiah($hdpp) ?></td>
            <td colspan="1"><?= FormatRupiah($hhasilppn) ?></td>
            <td colspan="1"><?= FormatRupiah($hgrandtotal) ?></td>
        </tr> -->
    </tbody>
</table>