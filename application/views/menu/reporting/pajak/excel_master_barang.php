<?php

header("Content-type: application/vnd-ms-excel");

header("Content-Disposition: attachment; filename=Export Master Barang.xls");

header("Pragma: no-cache");

header("Expires: 0");

function FormatRupiah($angka)
{
    $hasil_rupiah = number_format($angka, 0, '.', ',');
    return $hasil_rupiah;
}
function ClearChr($param)
{
    return $result = str_replace(array('\'', '"', ',', ';', '<', '>', '_', '-', ' ', '.', '!', '?', '/', '=', '+', ']', '[', '{', '}', '@', '#', '$', '%', '^', '&', "*", '(', ')'), '', $param);
}
?>
<h4 style="text-align: center;">MASTER BARANG</h4>
<table border="1" width="100%">

    <thead>
        <tr>
            <th style="text-transform: uppercase;">KODE</th>
            <th style="text-transform: uppercase;">BARANG</th>
            <th style="text-transform: uppercase;">KODE</th>
            <th style="text-transform: uppercase;">GUDANG</th>
            <th style="text-transform: uppercase;">KODE</th>
            <th style="text-transform: uppercase;">KATEGORI</th>
            <th style="text-transform: uppercase;">KODE</th>
            <th style="text-transform: uppercase;">SATUAN</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($report as $row) : ?>
            <tr>
                <td style="text-align: left;"><?= $row->kodebarang ?></td>
                <td style="text-align: left;"><?= $row->namabarang ?></td>
                <td style="text-align: left;"><?= $row->kodegudang ?></td>
                <td style="text-align: left;"><?= $row->namagudang ?></td>
                <td style="text-align: left;"><?= $row->kodekategori ?></td>
                <td style="text-align: left;"><?= $row->namakategori ?></td>
                <td style="text-align: left;"><?= $row->kodesatuan ?></td>
                <td style="text-align: left;"><?= $row->namasatuan ?></td>
            </tr>
        <?php endforeach; ?>
    </tbody>
    </tbody>
</table>