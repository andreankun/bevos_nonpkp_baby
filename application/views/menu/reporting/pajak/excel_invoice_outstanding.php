<?php

header("Content-type: application/vnd-ms-excel");

header("Content-Disposition: attachment; filename=Laporan Outstanding Invoice Pajak.xls");

header("Pragma: no-cache");

header("Expires: 0");

function FormatRupiah($angka)
{
	$hasil_rupiah = number_format($angka, 0, '.', ',');
	return $hasil_rupiah;
}
?>
<h4 style="text-align: center;">LAPORAN OUTSTANDING INVOICE PAJAK</h4>
CABANG : <?php echo $kodecabang ?>
<br>
PERIODE : <?php echo $tglawal ?> S/D <?php echo $tglakhir ?>
<br>

<table border="1" width="100%">

	<thead>
		<tr>
			<th>Kode Pelanggan</th>
			<th>Nama Pelanggan</th>
			<th>No Faktur</th>
			<th>Tanggal Faktur</th>
			<th>Jatuh Tempo</th>
			<th>Piutang</th>
			<th>Bayar</th>
			<th>Sisa Piutang</th>
		</tr>
	</thead>
	<tbody>
		<?php
		$no = 1;
		$jumlah = 0;
		foreach ($report as $val) : ?>
		<?php $jumlah = $val->nilaipiutang - $val->nilaialokasi - $val->nilaipenerimaan; ?>
			<tr>
				<td><?= $val->nopelanggan ?></td>
				<td><?= $val->namacustomer ?></td>
				<td><?= $val->nomor ?></td>
				<td><?= date('Y/m/d', strtotime($val->tanggal)) ?></td>
				<td><?= date('Y/m/d', strtotime($val->tanggal)) ?></td>
				<td><?= $val->nilaipiutang ?></td>
				<td><?= $val->nilaialokasi + $val->nilaipenerimaan ?></td>
				<td><?= $jumlah ?></td>
			</tr>
		<?php endforeach; ?>
	</tbody>

</table>
