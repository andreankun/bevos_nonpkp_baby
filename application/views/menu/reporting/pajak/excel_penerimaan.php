<?php

header("Content-type: application/vnd-ms-excel");

header("Content-Disposition: attachment; filename=Laporan Penerimaan.xls");

header("Pragma: no-cache");

header("Expires: 0");

function FormatRupiah($angka)
{
    $hasil_rupiah = number_format($angka, 0, ',', '.');
    return $hasil_rupiah;
}
function ClearChr($param)
{
    return $result = str_replace(array('\'', '"', ',', ';', '<', '>', '_', '-', ' ', '.', '!', '?', '/', '=', '+', ']', '[', '{', '}', '@', '#', '$', '%', '^', '&', "*", '(', ')'), '', $param);
}
?>
<table border="1" width="100%">

    <thead>
        <tr>
            <th style="text-transform: uppercase;">No Bukti Bayar</th>
            <th style="text-transform: uppercase;">Kode</th>
            <th style="text-transform: uppercase;">Kode Pelanggan</th>
            <th style="text-transform: uppercase;">Akun Kas</th>
            <th style="text-transform: uppercase;">Tanggal Bayar</th>
            <th style="text-transform: uppercase;">Tanggal Cek/Giro</th>
            <th style="text-transform: uppercase;">Keterangan</th>
            <th style="text-transform: uppercase;">No Faktur</th>
            <th style="text-transform: uppercase;">Jumlah Bayar</th>
            <th style="text-transform: uppercase;">Jumlah Potongan</th>
            <th style="text-transform: uppercase;">Akun Potongan</th>
            <th style="text-transform: uppercase;">No Retur</th>
            <th style="text-transform: uppercase;">Jumlah Retur</th>
            <th style="text-transform: uppercase;">Jumlah Potongan Retur</th>
            <th style="text-transform: uppercase;">Akun Potongan Retur</th>
            <th style="text-transform: uppercase;">Keterangan Detail</th>
            <th style="text-transform: uppercase;">Nilai Faktur</th>
            <th style="text-transform: uppercase;">Customer</th>
        </tr>
    </thead>

    <tbody>
        <?php
        foreach ($report as $vals) :
        ?>
            <tr>
                <td style="text-align: center; border: 1px solid #dbdbdb;border-width: thin;"><?= $vals->nomorpenerimaan; ?></td>
                <td style="text-align: left; border: 1px solid #dbdbdb;border-width: thin;"></td>
                <td style="text-align: left; border: 1px solid #dbdbdb;border-width: thin;"><?= $vals->kodecabang . $vals->nocustomer ?></td>
                <?php if (($vals->kodeaccount == '102001' || $vals->kodeaccount == '102002' || $vals->kodeaccount == '102003' || $vals->kodeaccount == '102004' || $vals->kodeaccount == '102005' || $vals->kodeaccount == '102006' || $vals->kodeaccount == '102007' || $vals->kodeaccount == '102012' || $vals->kodeaccount == '102013' || $vals->kodeaccount == '102014') && ($vals->accountalokasi == '103999' && $vals->nilaialokasi > '0')) : ?>
                    <td style="text-align: center; border: 1px solid #dbdbdb;border-width: thin; vnd.ms-excel.numberformat:@"><?= FormatRupiah($vals->accountalokasi); ?></td>
                <?php else : ?>
                    <td style="text-align: center; border: 1px solid #dbdbdb;border-width: thin; vnd.ms-excel.numberformat:@"><?= FormatRupiah($vals->kodeaccount); ?></td>
                <?php endif; ?>
                <td style="text-align: center; border: 1px solid #dbdbdb;border-width: thin;"><?= date('d/m/Y', strtotime($vals->tanggal)) ?></td>
                <td style="text-align: left; border: 1px solid #dbdbdb;border-width: thin;"></td>
                <td style="text-align: left; border: 1px solid #dbdbdb;border-width: thin;"></td>
                <td style="text-align: left; border: 1px solid #dbdbdb;border-width: thin;"><?= $vals->nomor ?></td>
                <?php if (($vals->kodeaccount == '102001' || $vals->kodeaccount == '102002' || $vals->kodeaccount == '102003' || $vals->kodeaccount == '102004' || $vals->kodeaccount == '102005' || $vals->kodeaccount == '102006' || $vals->kodeaccount == '102007' || $vals->kodeaccount == '102012' || $vals->kodeaccount == '102013' || $vals->kodeaccount == '102014') && ($vals->accountalokasi == '103999' && $vals->nilaialokasi > '0')) : ?>
                    <td style="text-align: left; border: 1px solid #dbdbdb;border-width: thin;"><?= $vals->nilaialokasi ?></td>
                    <?php if ($vals->nilaipembulatan > '0' || $vals->nilaipembulatan < '0') : ?>
                        <td style="text-align: right; border: 1px solid #dbdbdb;border-width: thin;"><?= $vals->nilaipembulatan ?></td>
                        <td style="text-align: right; border: 1px solid #dbdbdb;border-width: thin; vnd.ms-excel.numberformat:@">831.000</td>
                    <?php else : ?>
                        <td style="text-align: right; border: 1px solid #dbdbdb;border-width: thin;"></td>
                        <td style="text-align: right; border: 1px solid #dbdbdb;border-width: thin; vnd.ms-excel.numberformat:@"></td>
                    <?php endif; ?>
                <?php else : ?>
                    <td style="text-align: left; border: 1px solid #dbdbdb;border-width: thin;"><?= $vals->nilaipenerimaan ?></td>
                    <?php if ($vals->nilaipembulatan > '0' || $vals->nilaipembulatan < '0') : ?>
                        <td style="text-align: right; border: 1px solid #dbdbdb;border-width: thin;"><?= $vals->nilaipembulatan ?></td>
                        <td style="text-align: right; border: 1px solid #dbdbdb;border-width: thin; vnd.ms-excel.numberformat:@">831.000</td>
                    <?php elseif ($vals->nilaialokasi > '0' || $vals->nilaialokasi < '0') : ?>
                        <td style="text-align: right; border: 1px solid #dbdbdb;border-width: thin;"><?= $vals->nilaialokasi ?></td>
                        <td style="text-align: right; border: 1px solid #dbdbdb;border-width: thin;">103.999</td>
                    <?php else : ?>
                        <td style="text-align: right; border: 1px solid #dbdbdb;border-width: thin;"></td>
                        <td style="text-align: right; border: 1px solid #dbdbdb;border-width: thin; vnd.ms-excel.numberformat:@"></td>
                    <?php endif; ?>
                <?php endif; ?>
                <td style="text-align: right; border: 1px solid #dbdbdb;border-width: thin;"></td>
                <td style="text-align: right; border: 1px solid #dbdbdb;border-width: thin;"></td>
                <td style="text-align: right; border: 1px solid #dbdbdb;border-width: thin;"></td>
                <td style="text-align: right; border: 1px solid #dbdbdb;border-width: thin;"></td>
                <td style="text-align: right; border: 1px solid #dbdbdb;border-width: thin;"></td>
                <td style="text-align: left; border: 1px solid #dbdbdb;border-width: thin;"><?= $vals->nilaipiutang ?></td>
                <td style="text-align: right; border: 1px solid #dbdbdb;border-width: thin;"><?= $vals->nama ?></td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>