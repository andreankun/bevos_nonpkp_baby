<?php

header("Content-type: application/vnd-ms-excel");

header("Content-Disposition: attachment; filename=e-Faktur.xls");

header("Pragma: no-cache");

header("Expires: 0");

function FormatRupiah($angka)
{
    $hasil_rupiah = number_format($angka, 0, '.', ',');
    return $hasil_rupiah;
}

function ClearChr($param)
{
    return $result = str_replace(array('\'', '"', ',', ';', '<', '>', '_', '-', ' ', '.', '!', '?', '/', '=', '+', ']', '[', '{', '}', '@', '#', '$', '%', '^', '&', "*", '(', ')'), '', $param);
}
?>

<style>
    table {
        border: 1px solid #dbdbdb;
    }
</style>

<table>
    <tr>
        <td style="background-color: yellow;">FK</td>
        <td style="background-color: yellow;">KD_JENIS_TRANSAKSI</td>
        <td style="background-color: yellow;">FG_PENGGANTI</td>
        <td style="background-color: yellow;">NOMOR_FAKTUR</td>
        <td style="background-color: yellow;">MASA_PAJAK</td>
        <td style="background-color: yellow;">TAHUN_PAJAK</td>
        <td style="background-color: yellow;">TANGGAL_FAKTUR</td>
        <td style="background-color: yellow;">NPWP</td>
        <td style="background-color: yellow;">NAMA</td>
        <td style="background-color: yellow;">ALAMAT_LENGKAP</td>
        <td style="background-color: yellow;">JUMLAH_DPP</td>
        <td style="background-color: yellow;">JUMLAH_PPN</td>
        <td style="background-color: yellow;">JUMLAH_PPNBM</td>
        <td style="background-color: yellow;">ID_KETERANGAN_TAMBAHAN</td>
        <td style="background-color: yellow;">FG_UANG_MUKA</td>
        <td style="background-color: yellow;">UANG_MUKA_DPP</td>
        <td style="background-color: yellow;">UANG_MUKA_PPN</td>
        <td style="background-color: yellow;">UANG_MUKA_PPNBM</td>
        <td style="background-color: yellow;">REFERENSI</td>
        <td style="background-color: yellow;">NOMOR_DOKUMEN_PENDUKUNG</td>
    </tr>
    <tr>
        <td style="background-color: yellow;">LT</td>
        <td style="background-color: yellow;">NPWP</td>
        <td style="background-color: yellow;">NAMA</td>
        <td style="background-color: yellow;">JALAN</td>
        <td style="background-color: yellow;">BLOK</td>
        <td style="background-color: yellow;">NOMOR</td>
        <td style="background-color: yellow;">RT</td>
        <td style="background-color: yellow;">RW</td>
        <td style="background-color: yellow;">KECAMATAN</td>
        <td style="background-color: yellow;">KELURAHAN</td>
        <td style="background-color: yellow;">KABUPATEN</td>
        <td style="background-color: yellow;">PROPINSI</td>
        <td style="background-color: yellow;">KODE_POS</td>
        <td style="background-color: yellow;">NOMOR_TELEPON</td>
        <td style="background-color: yellow;"></td>
        <td style="background-color: yellow;"></td>
        <td style="background-color: yellow;"></td>
        <td style="background-color: yellow;"></td>
        <td style="background-color: yellow;"></td>
        <td style="background-color: yellow;"></td>
    </tr>
    <tr>
        <td style="background-color: yellow;">OF</td>
        <td style="background-color: yellow;">KODE_OBJEK</td>
        <td style="background-color: yellow;">NAMA</td>
        <td style="background-color: yellow;">HARGA_SATUAN</td>
        <td style="background-color: yellow;">JUMLAH_BARANG</td>
        <td style="background-color: yellow;">HARGA_TOTAL</td>
        <td style="background-color: yellow;">DISKON</td>
        <td style="background-color: yellow;">DPP</td>
        <td style="background-color: yellow;">PPN</td>
        <td style="background-color: yellow;">TARIF_PPNBM</td>
        <td style="background-color: yellow;">PPNBM</td>
        <td style="background-color: yellow;"></td>
        <td style="background-color: yellow;"></td>
        <td style="background-color: yellow;"></td>
        <td style="background-color: yellow;"></td>
        <td style="background-color: yellow;"></td>
        <td style="background-color: yellow;"></td>
        <td style="background-color: yellow;"></td>
        <td style="background-color: yellow;"></td>
        <td style="background-color: yellow;"></td>
    </tr>
    <tr>
        <!-- <td style="background-color: yellow;" colspan="20"></td> -->
        <td colspan="20"></td>
    </tr>
    <?php foreach ($report as $row) :
        $nomor = $row->nomor; ?>
        <?php foreach ($reportrow as $list) : ?>
            <?php if ($list->nomor == $nomor) : ?>
                <tr>
                    <td style="border: 1px solid #dbdbdb;border-width: thin; width: 30;">FK</td>
                    <?php if ($list->statusppn == 't') : ?>
                        <td style="border: 1px solid #dbdbdb;border-width: thin; width: 150; vnd.ms-excel.numberformat:@">01</td>
                    <?php else : ?>
                        <td style="border: 1px solid #dbdbdb;border-width: thin; width: 150; vnd.ms-excel.numberformat:@">07</td>
                    <?php endif; ?>
                    <td style="border: 1px solid #dbdbdb;border-width: thin; width: 150;">0</td>
                    <?php if ($list->nofakturpajak != '' || $list->nofakturpajak != 0) : ?>
                        <td style="border: 1px solid #dbdbdb;border-width: thin; width: 500; vnd.ms-excel.numberformat:@"><?= ClearChr($list->nofakturpajak); ?></td>
                    <?php else : ?>
                        <td style="border: 1px solid #dbdbdb;border-width: thin; width: 150;">0</td>
                    <?php endif; ?>
                    <td style="border: 1px solid #dbdbdb;border-width: thin; width: 150; text-align: left; vnd.ms-excel.numberformat:@"><?= date('m', strtotime($list->tanggal)) ?></td>
                    <td style="border: 1px solid #dbdbdb;border-width: thin; width: 150; text-align: right;"><?= date('Y') ?></td>
                    <td style="border: 1px solid #dbdbdb;border-width: thin; width: 500;"><?= date('d/m/Y', strtotime($list->tanggal)) ?></td>
                    <?php if ($list->npwp == '' || $list->npwp == 0) : ?>
                        <td style="border: 1px solid #dbdbdb;border-width: thin; width: 500;vnd.ms-excel.numberformat:@">000000000000000</td>
                        <td style="border: 1px solid #dbdbdb;border-width: thin; width: 500;vnd.ms-excel.numberformat:@"><?= $list->ktp . '#NIK#NAMA#' . $list->namapelanggan; ?></td>
                    <?php else : ?>
                        <td style="border: 1px solid #dbdbdb;border-width: thin; width: 500;vnd.ms-excel.numberformat:@"><?= ClearChr(strval($list->npwp)); ?></td>
                        <td style="border: 1px solid #dbdbdb;border-width: thin; width: 500;"><?= $list->namapelanggan; ?></td>
                    <?php endif; ?>
                    <td style="border: 1px solid #dbdbdb;border-width: thin; width: 1000;"><?= $list->alamat; ?></td>
                    <td style="border: 1px solid #dbdbdb;border-width: thin; width: 500; text-align: right;"><?= $list->dpp; ?></td>
                    <!-- <td style="border: 1px solid #dbdbdb;border-width: thin; width: 500; text-align: right;"><-?= number_format($list->ppn, 1, ".", ""); ?></td> -->
                    <td style="border: 1px solid #dbdbdb;border-width: thin; width: 500; text-align: right;"><?= intval(floor($list->dpp * ($konfigurasi->ppn / 100))); ?></td>
                    <td style="border: 1px solid #dbdbdb;border-width: thin; width: 150;">0</td>
                    <?php if ($list->statusppn == 't') : ?>
                        <td style="border: 1px solid #dbdbdb;border-width: thin; width: 150; vnd.ms-excel.numberformat:@">0</td>
                    <?php else : ?>
                        <td style="border: 1px solid #dbdbdb;border-width: thin; width: 150; vnd.ms-excel.numberformat:@">18</td>
                    <?php endif; ?>
                    <td style="border: 1px solid #dbdbdb;border-width: thin; width: 150;">0</td>
                    <td style="border: 1px solid #dbdbdb;border-width: thin; width: 150;">0</td>
                    <td style="border: 1px solid #dbdbdb;border-width: thin; width: 150;">0</td>
                    <td style="border: 1px solid #dbdbdb;border-width: thin; width: 150;">0</td>
                    <td style="border: 1px solid #dbdbdb;border-width: thin; width: 150;"><?= $list->nomor ?></td>
                    <td style="border: 1px solid #dbdbdb;border-width: thin; width: 150;"></td>
                </tr>
            <?php endif; ?>
        <?php endforeach; ?>
        <?php foreach ($detail as $value) :
            $discustomer = 0;
            $discash = 0;
            $disc = 0;
            // $total = 0;
            $hasil = 0;
            $subtotal = 0;
        ?>
            <?php if ($value->nomor == $nomor) :
                $subtotal = $subtotal + ($value->qty * $value->harga);
                // $discustomer = (round(($value->total * $value->disccustomer / 100) / 10) * 10);
                $discustomer = round(round($value->total * ($value->disccustomer / 100) / 10) * 10);
                // $discustomer = $discustomer + (ceil(round($value->total * $value->disccustomer / 100) / 10) * 10);
                // $discash = (round((($value->total - $discustomer) * $value->disccash / 100) / 10) * 10);
                $discash = round(round(($value->total - $discustomer) * ($value->disccash / 100) / 10) * 10);
                // $discash = $discash + (ceil(round(($value->total - $discustomer) * $value->disccash / 100) / 10) * 10);
                // $disc = round(($disc + $discustomer + $discash) / 10) * 10;
                // $disc = $disc + $discustomer + $discash;
                // $total = $total + ($value->qty * $value->harga);
                $hasil = $hasil + (((($subtotal - $value->discbrgitem) - $value->discpaketitem) - ($discustomer + $discash)));
            ?>
                <tr>
                    <td style="border: 1px solid #dbdbdb;border-width: thin; width: 30;">OF</td>
                    <td style="border: 1px solid #dbdbdb;border-width: thin; width: 150;"></td>
                    <td style="border: 1px solid #dbdbdb;border-width: thin; width: 1000;"><?= $value->namabarang; ?></td>
                    <td style="border: 1px solid #dbdbdb;border-width: thin; width: 500;"><?= $value->harga; ?></td>
                    <td style="border: 1px solid #dbdbdb;border-width: thin; width: 500;"><?= $value->qty; ?></td>
                    <td style="border: 1px solid #dbdbdb;border-width: thin; width: 500;"><?= $subtotal; ?></td>
                    <td style="border: 1px solid #dbdbdb;border-width: thin; width: 500;"><?= $value->discbrgitem + $value->discpaketitem + $discustomer + $discash; ?></td>
                    <td style="border: 1px solid #dbdbdb;border-width: thin; width: 500;"><?= $hasil; ?></td>
                    <!-- <-php if ($value->statusppn == 't' || $value->statusppn == 'true') : ?> -->
                        <td style="border: 1px solid #dbdbdb;border-width: thin; width: 500; text-align: right;"><?= doubleval($hasil * ($konfigurasi->ppn / 100)); ?></td>
                    <!-- <-?php else : ?>
                        <td style="border: 1px solid #dbdbdb;border-width: thin; width: 500; text-align: right;">0</td>
                    <-?php endif; ?> -->
                    <td style="border: 1px solid #dbdbdb;border-width: thin; width: 150;">0</td>
                    <td style="border: 1px solid #dbdbdb;border-width: thin; width: 150;">0</td>
                    <td style="border: 1px solid #dbdbdb;border-width: thin; width: 150;"></td>
                    <td style="border: 1px solid #dbdbdb;border-width: thin; width: 150;"></td>
                    <td style="border: 1px solid #dbdbdb;border-width: thin; width: 300;"></td>
                    <td style="border: 1px solid #dbdbdb;border-width: thin; width: 150;"></td>
                    <td style="border: 1px solid #dbdbdb;border-width: thin; width: 150;"></td>
                    <td style="border: 1px solid #dbdbdb;border-width: thin; width: 150;"></td>
                    <td style="border: 1px solid #dbdbdb;border-width: thin; width: 150;"></td>
                    <td style="border: 1px solid #dbdbdb;border-width: thin; width: 150;"></td>
                    <td style="border: 1px solid #dbdbdb;border-width: thin; width: 300;"></td>
                </tr>
            <?php endif; ?>
        <?php endforeach; ?>
    <?php endforeach; ?>
</table>