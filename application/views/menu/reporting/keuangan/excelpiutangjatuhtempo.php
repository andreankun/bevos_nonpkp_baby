<?php

header("Content-type: application/vnd-ms-excel");

header("Content-Disposition: attachment; filename=LAPORAN PIUTANG JATUH TEMPO.xls");

header("Pragma: no-cache");

header("Expires: 0");

function FormatRupiah($angka)
{
	$hasil_rupiah = number_format($angka, 0, '.', ',');
	return $hasil_rupiah;
}
?>
<h4 style="text-align: center;">LAPORAN PIUTANG JATUH TEMPO</h4>
CABANG : <?php echo $cabang ?>
<br>
PERIODE : <?php echo $tglawal ?> S/D <?php echo $tglakhir ?>
<br>

<table border="1" width="100%">

	<thead>
		<tr>
			<th>No</th>
			<th>No.Faktur</th>
			<th>Tanggal</th>
			<th>Jatuh Tempo</th>
			<th>Piutang</th>
			<th>Terbayar</th>
			<th>Saldo</th>
		</tr>
	</thead>
	<tbody>
		<?php
		$totalsemua = 0;
		$totalterbayarasemua = 0;
		$totalsaldosemua = 0;
		$totalpiutang = 0;
		$totalterbayar = 0;
		$totalsaldo = 0;
		foreach ($row as $val) : ?>
			<?php $totalsemua = $totalsemua + $totalpiutang ?>
			<?php $totalterbayarasemua = $totalterbayarasemua + $totalterbayar ?>
			<?php $totalsaldosemua = $totalsaldosemua + $totalsaldo ?>
			<?php $nama = $val->nama_toko ?>
			<tr>
				<td style="font-weight: bold; font-size: 11px;" colspan="7"><?= $val->nama_toko ?></td>
			</tr>
			<?php
			$no = 1;
			$saldo = 0;
			$totalpiutang = 0;
			$totalterbayar = 0;
			$totalsaldo = 0;
			foreach ($report as $vals) : ?>
				<?php if ($vals->nama_toko == $nama) : ?>
					<?php $saldo = $vals->piutang - $vals->terbayar ?>
					<?php $totalpiutang = $totalpiutang + $vals->piutang; ?>
					<?php $totalsaldo = $totalsaldo + $saldo ?>
					<?php $totalterbayar = $totalterbayar + $vals->terbayar ?>
					<tr>
						<td style="font-weight: bold; font-size: 11px; "><?= $no++ ?></td>
						<td style="font-weight: bold; font-size: 11px; "><?= $vals->nomor ?></td>
						<td style="font-weight: bold; font-size: 11px; "><?= date('d-m-Y', strtotime($vals->tanggal)) ?></td>
						<td style="font-weight: bold; font-size: 11px; "><?= date('d-m-Y', strtotime($vals->tgljthtempo)) ?></td>
						<td style="font-weight: bold; font-size: 11px; text-align: right;"><?= FormatRupiah($vals->piutang) ?></td>
						<td style="font-weight: bold; font-size: 11px; text-align: right;"><?= FormatRupiah($vals->terbayar) ?></td>
						<td style="font-weight: bold; font-size: 11px; text-align: right;"><?= FormatRupiah($saldo) ?></td>
					</tr>
				<?php endif; ?>
			<?php endforeach; ?>
			<tr>
				<td colspan="7">
					<hr>
				</td>
			</tr>
			<tr>
				<td colspan="5" style="font-weight: bold; font-size: 11px; text-align: right;">Total :</td>
				<td colspan="1" style="font-weight: bold; font-size: 11px;"><?= FormatRupiah($totalpiutang) ?></td>
				<td colspan="1" style="font-weight: bold; font-size: 11px; text-align: right;"><?= FormatRupiah($totalsaldo) ?></td>
			</tr>
		<?php endforeach; ?>
		<tr>

			<td colspan="4" style="font-weight: bold; font-size: 11px; text-align: right;">Grandtotal :</td>
			<td colspan="1" style="font-weight: bold; font-size: 11px; text-align: right;"><?= FormatRupiah($totalsemua) ?></td>
			<td colspan="1" style="font-weight: bold; font-size: 11px; text-align: right;"><?= FormatRupiah($totalterbayarasemua) ?></td>
			<td colspan="1" style="font-weight: bold; font-size: 11px; text-align: right;"><?= FormatRupiah($totalsaldosemua) ?></td>
		</tr>
	</tbody>
</table>

</table>