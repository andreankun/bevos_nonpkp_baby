<div class="col-md-12 mb-2">
	<div class="modal-header" style="padding: 5px;">
		<div class="col-md-12">
			<div class="row">
				<div class="col-md-3">
					<h2>
						<span class="logo-menu"><?php echo $icons ?></span>
						<span><?php echo $title ?></span>
					</h2>
				</div>
				<div class="col-md-9">
					<div class="form-group" style="float: right;">
						<button id="preview" class="btn btn-sm btn-danger mb-1 mt-1 mr-1 ml-1"><i class="far fa-print" style="margin-right: 10px;"></i>Preview</button>
						<button id="print" class="btn btn-sm btn-dark mb-1 mt-1 mr-1 ml-1"><i class="far fa-print" style="margin-right: 10px;"></i>Print</button>
						<button id="export" class="btn btn-sm btn-success mb-1 mt-1 mr-1 ml-1"><i class="far fa-file-export" style="margin-right: 10px;"></i>Export</button>
						<button id="clear" class="btn btn-sm btn-dark mb-1 mt-1 mr-1 ml-1"><i class="far fa-refresh" style="margin-right: 10px;"></i>Clear</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="col-md-12 table-responsive hidescroll" style="height: 555px; overflow-y: scroll;">
	<div class="row">
		<div class="col-md-6">
			<div class="modal-content mb-2">
				<div class="modal-body">
					<div class="row mb-2 mt-2">
						<div class="col-sm-12 input-group box">
							<div class="col-sm-3 p-0 input-group-prepend">
								<span class="col-sm-12 input-group-text">Laporan Dokumen</span>
							</div>
							<select class="form-control" style="height: 29px; padding: 0.175rem 0.50rem;" id="jenis_laporan">
								<option value="">Pilih Laporan</option>
								<option value="1">Laporan Piutang Jatuh Tempo</option>
								<option value="2">Laporan Deposit Pelanggan</option>
								<option value="3">Laporan Pelanggan Tidak Aktif</option>
								<option value="4">Laporan Invoice Outstanding Per Periode</option>
								<option value="5">Laporan Piutang Dengan Jenis Pembayaran</option>
								<option value="6">Laporan Kartu Piutang Usaha</option>
								<option value="7">Laporan Piutang Per Pelanggan</option>
								<option value="8">Laporan Daftar Piutang</option>
								<option value="9">Laporan Rekap Pembayaran</option>
								<option value="10">Laporan Rincian Pembayaran Per Pelanggan</option>
								<option value="11">Laporan Rincian Pembayaran</option>
							</select>
						</div>
					</div>
					<div class="row mb-2">
						<div class="col-sm-6 input-group">
							<div class="col-sm-6 p-0 input-group-prepend">
								<span class="col-sm-12 input-group-text">Kode Cabang</span>
							</div>
							<input class="form-control" type="text" name="kodecabang" id="kodecabang" maxlength="2" placeholder="Kode Cabang" required readonly />
							<div class="input-group-text btn-dark" data-toggle="modal" data-target="#findcabang" id="carikodecabang">
								<span class="input-group-addon">
									<i class="fa fa-search text-white"></i>
								</span>
							</div>
						</div>
					</div>
					<div class="row mb-2 pelanggan">
						<!-- <div class="col-sm-12 input-group">
							<div class="col-sm-3 p-0 input-group-prepend">
								<span class="col-sm-12 input-group-text">Nomor Pelanggan</span>
							</div>
							<select class="form-control" name="nopelanggan" id="nopelanggan" style="height: 29px; padding: 0.175rem 0.50rem;"></select>
						</div> -->
						<div class="col-sm-6 input-group">
							<div class="col-sm-6 p-0 input-group-prepend">
								<span class="col-sm-12 input-group-text">Nomor Pelanggan</span>
							</div>
							<input class="form-control" type="text" name="nopelanggan" id="nopelanggan" maxlength="50" placeholder="Nomor Pelanggan" required readonly />
						</div>

						<div class="col-sm-6 input-group">
							<div class="col-sm-6 p-0 input-group-prepend">
								<span class="col-sm-12 input-group-text">Nama Pelanggan</span>
							</div>
							<input class="form-control" type="text" name="namapelanggan" id="namapelanggan" maxlength="50" placeholder="Nama Pelanggan" required readonly />
							<div class="input-group-text btn-dark" data-toggle="modal" data-target="#findpelanggan" id="caricustomer">
								<span class="input-group-addon">
									<i class="fa fa-search text-white"></i>
								</span>
							</div>
						</div>
					</div>

					<div class="row mb-2">
						<div class="col-sm-6 input-group">
							<div class="col-sm-6 p-0 input-group-prepend">
								<span class="col-sm-12 input-group-text">Tanggal Awal</span>
							</div>
							<input class="form-control" id="tglawal" value="<?php echo date('Y-08-01'); ?>" padding="5px" type="date" required />
						</div>
						<div class="col-sm-6 input-group">
							<div class="col-sm-6 p-0 input-group-prepend">
								<span class="col-sm-12 input-group-text">Tanggal Akhir</span>
							</div>
							<input class="form-control" id="tglakhir" value="<?php echo date('Y-m-d'); ?>" padding="5px" type="date" required />
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="findcabang">
	<div class="modal-dialog modal-lg">
		<div class="modal-content" style="border-radius: 5px;">
			<div class="modal-header" style="padding: 10px; color: #000;">
				<h7 class="modal-title" style="margin-left: 5px;">Data Cabang</h7>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="table-responsive">
					<table id="t_cabang" class="table table-bordered table-striped dt-responsive display nowrap" style="width:100%">
						<thead class="thead-dark">
							<tr>
								<th width="10">Action</th>
								<th width="150">Kode Cabang</th>
								<th width="150">Nama Cabang</th>
							</tr>
						</thead>
						<tbody></tbody>
					</table>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="findmerkawal">
	<div class="modal-dialog modal-lg">
		<div class="modal-content" style="border-radius: 5px;">
			<div class="modal-header" style="padding: 10px; color: #000;">
				<h7 class="modal-title" style="margin-left: 5px;">Data Merk</h7>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="table-responsive">
					<table id="t_merkawal" class="table table-bordered table-striped dt-responsive display nowrap" style="width:100%">
						<thead class="thead-dark">
							<tr>
								<th width="10">Action</th>
								<th width="150">Kode Merk</th>
								<th width="150">Nama Merk</th>
							</tr>
						</thead>
						<tbody></tbody>
					</table>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="findpelanggan">
	<div class="modal-dialog modal-lg">
		<div class="modal-content" style="border-radius: 5px;">
			<div class="modal-header" style="padding: 10px; color: #000;">
				<h7 class="modal-title" style="margin-left: 5px;">Data Pelanggan</h7>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="table-responsive">
					<table id="t_pelanggan" class="table table-bordered table-striped dt-responsive display nowrap" style="width:100%;">
						<thead class="thead-dark">
							<tr>
								<th width="10">Action</th>
								<th width="150">Nomor</th>
								<th width="150">Nama Toko</th>
								<th width="150">Nama</th>
								
							</tr>
						</thead>
						<tbody></tbody>
					</table>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="findmerkakhir">
	<div class="modal-dialog modal-lg">
		<div class="modal-content" style="border-radius: 5px;">
			<div class="modal-header" style="padding: 10px; color: #000;">
				<h7 class="modal-title" style="margin-left: 5px;">Data Merk</h7>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="table-responsive">
					<table id="t_merkakhir" class="table table-bordered table-striped dt-responsive display nowrap" style="width:100%">
						<thead class="thead-dark">
							<tr>
								<th width="10">Action</th>
								<th width="150">Kode Merk</th>
								<th width="150">Nama Merk</th>
							</tr>
						</thead>
						<tbody></tbody>
					</table>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>