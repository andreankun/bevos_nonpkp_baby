<?php

header("Content-type: application/vnd-ms-excel");

header("Content-Disposition: attachment; filename=Laporan Rekap Pembayaran.xls");

header("Pragma: no-cache");

header("Expires: 0");

function FormatRupiah($angka)
{
	$hasil_rupiah = number_format($angka, 0, '.', ',');
	return $hasil_rupiah;
}
?>
<h4 style="text-align: center;">LAPORAN REKAP PEMBAYARAN</h4>
CABANG : <?php echo $cabang ?>
<br>
PERIODE : <?php echo $tglawal ?> S/D <?php echo $tglakhir ?>
<br>

<table border="1" width="100%">

	<thead>
		<tr>
			<th>No</th>
			<th>PEMBAYARAN</th>
			<th>NO.FAKTUR</th>
			<th>TANGGAL</th>
			<th>PELANGGAN</th>
			<th>TOKO</th>
			<th>INFO PEMBAYARAN</th>
			<th>JUMLAH</th>
		</tr>
	</thead>

	<tbody>
		<?php 
		$no = 1;
		foreach($report as $val) : ?>
		<tr>
			<td><?= $no++ ?></td>
			<td><?php if($val->jenispenerimaan == "TUF02"){
				echo "Tunai";
			} else {
				echo "Kredit";
			} ?></td>
			<td><?= $val->noreferensi ?></td>
			<td><?= date('d-m-Y', strtotime($val->tanggal)) ?></td>
			<td><?= $val->nama ?></td>
			<td><?= $val->namatoko ?></td>
			<td><?= $val->keterangan ?></td>
			<th><?= FormatRupiah($val->jumlah) ?></th>
		</tr>
		<?php endforeach; ?>
	</tbody>

</table>
