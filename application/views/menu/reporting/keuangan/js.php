<script type="text/javascript">
	$(document).ready(function() {

		$('#jenis_laporan').change(function() {
			var jenisdokumen = this.value;
			if (jenisdokumen == '1') {
				$('.pelanggan').hide();
				$('.barang').hide();
				$('.merkawal').hide();
				$('.merkakhir').hide();
			} else if (jenisdokumen == '2') {
				$('.pelanggan').hide();
				$('.barang').hide();
				$('.merkawal').hide();
				$('.merkakhir').hide();
			} else if (jenisdokumen == '3') {
				$('.pelanggan').hide();
				$('.barang').hide();
				$('.merkawal').hide();
				$('.merkakhir').hide();
			} else if (jenisdokumen == '4') {
				$('.pelanggan').hide();
				$('.barang').hide();
				$('.merkawal').hide();
				$('.merkakhir').hide();
			} else if (jenisdokumen == '5') {
				$('.pelanggan').show();
				$('.barang').show();
				$('.merkawal').hide();
				$('.merkakhir').hide();
			} else if (jenisdokumen == '6') {
				$('.pelanggan').hide();
				$('.barang').hide();
				$('.merkawal').hide();
				$('.merkakhir').hide();
			} else if (jenisdokumen == '7') {
				$('.pelanggan').show();
				$('.barang').hide();
				$('.merkawal').hide();
				$('.merkakhir').hide();
			} else if (jenisdokumen == '8') {
				$('.pelanggan').hide();
				$('.barang').hide();
				$('.merkawal').hide();
				$('.merkakhir').hide();
			} else if (jenisdokumen == '9') {
				$('.pelanggan').hide();
				$('.barang').hide();
				$('.merkawal').hide();
				$('.merkakhir').hide();
			} else if (jenisdokumen == '10') {
				$('.pelanggan').show();
				$('.barang').hide();
				$('.merkawal').hide();
				$('.merkakhir').hide();
			} else if (jenisdokumen == '11') {
				$('.pelanggan').hide();
				$('.barang').hide();
				$('.merkawal').hide();
				$('.merkakhir').hide();
			} else {
				$('.pelanggan').hide();
				$('.barang').hide();
				$('.merkawal').hide();
				$('.merkakhir').hide();
			}
		})

		function LoadPelanggan() {
			$.ajax({
				url: "<?= base_url('masterdata/Customer/DataPelanggan') ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					cabang: $('#kodecabang').val()
				},
				success: function(data) {
					for (var i = 0; i < data.length; i++) {
						$("#nopelanggan").append('<option value = ' + data[i].id + ' > Nama Pelanggan : ' + data[i].nama + ' ~ Nama Toko : ' + data[i].text + '</option>');
					}
				}
			}, false);
		}

		document.getElementById("caricustomer").addEventListener("click", function(event) {
            event.preventDefault();
            $('#tb_detail').empty();
            $('#t_pelanggan').DataTable({
                "destroy": true,
                "searching": true,
                "processing": true,
                "serverSide": true,
                "lengthChange": true,
                "pageLength": 5,
                "lengthMenu": [5, 10, 25, 50],
                "order": [],
                "autoWidth": false,
                "ajax": {
                    "url": "<?= base_url('masterdata/Customer/CariDataCustomer'); ?>",
                    "method": "POST",
                    "data": {
                        nmtb: "cari_customer",
                        field: {
                            nomor: "nomor",
                            nama_toko: "nama_toko",
                            nama: "nama",
                        },
                        sort: "nomor",
                        where: {
                            nomor: "nomor",
                            nama: "nama",
                            nama_toko: "nama_toko"
                        },
                        value: "kodecabang = '" + $('#kodecabang').val() + "'"
                    },
                }
            });
        });

		$(document).on('click', ".searchcustomer", function() {
            var nomor = $(this).attr("data-id");;
            $.ajax({
                url: "<?= base_url('masterdata/Customer/DataCustomer'); ?>",
                method: "POST",
                dataType: "json",
                async: false,
                data: {
                    nomor: nomor
                },
                success: function(data) {
                    for (var i = 0; i < data.length; i++) {
                        $('#nopelanggan').val(data[i].nomor.trim());
                        $('#namapelanggan').val(data[i].nama.trim());
                    }
                }
            }, false);
        });

		function LoadBarang() {
			$.ajax({
				url: "<?= base_url('masterdata/Barang/DataBrg') ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {},
				success: function(data) {
					for (var i = 0; i < data.length; i++) {
						$("#kodebarang").append('<option value = ' + data[i].id + ' >' + data[i].text + '</option>');
					}
				}
			}, false);
		}

		function LoadData() {
			$('.pelanggan').hide();
			$('.barang').hide();
			$('.merkawal').hide();
			$('.merkakhir').hide();
			$('#kodecabang').val("<?= $this->session->userdata('mycabang'); ?>");
			$("#nopelanggan").empty();
			LoadPelanggan();
			$("#nopelanggan").val("");
			$("#kodebarang").empty();
			LoadBarang();
			$("#kodebarang").val("");
			$('#carikodecabang').show();
		}

		// $("#nopelanggan").select2({
		// 	placeholder: "Pilih Pelanggan"
		// });

		$("#kodebarang").select2({
			placeholder: "Pilih Barang"
		});

		/* Cari Data Cabang*/
		document.getElementById("carikodecabang").addEventListener("click", function(event) {
			event.preventDefault();
			$('#t_cabang').DataTable({
				"destroy": true,
				"searching": true,
				"processing": true,
				"serverSide": true,
				"lengthChange": true,
				"pageLength": 5,
				"lengthMenu": [5, 10, 25, 50],
				"order": [],
				"ajax": {
					"url": "<?= base_url('masterdata/Gudang/CariDataCabang'); ?>",
					"method": "POST",
					"data": {
						nmtb: "glbm_cabang",
						field: {
							kode: "kode",
							nama: "nama",
							alamat: "alamat"
						},
						sort: "kode",
						where: {
							kode: "kode",
							nama: "nama",
							alamat: "alamat"
						},
						value: "aktif = true"
					},
				}
			});
		});

		/*Get Data Cabang*/
		$(document).on('click', ".searchcabang", function() {
			var kodecabang = $(this).attr("data-id");;
			$.ajax({
				url: "<?= base_url('masterdata/Gudang/DataCabang'); ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					kodecabang: kodecabang
				},
				success: function(data) {
					for (var i = 0; i < data.length; i++) {
						$('#kodecabang').val(data[i].kodecabang.trim());
					}
				}
			}, false);
		});

		document.getElementById("print").addEventListener("click", function(event) {
			event.preventDefault();
			var tglawal = $('#tglawal').val();
			var tglakhir = $('#tglakhir').val();
			var cabang = $('#kodecabang').val();
			var barang = $('#kodebarang').val();
			var username = $('#username').val();
			var merkawal = $('#merkawal').val();
			var merkakhir = $('#merkakhir').val();
			var nopelanggan = $('#nopelanggan').val();
			var jenislaporan = $('#jenis_laporan').val();

			if (jenislaporan == '1') {
				window.open(
					"<?php echo base_url('cetak_pdf/LaporanPiutang/LaporanPiutangJatuhTempo/') ?>" + tglawal + ":" + tglakhir + ":" + cabang
				);
			} else if (jenislaporan == '2') {
				window.open(
					"<?php echo base_url('cetak_pdf/LaporanPiutang/LaporanDepositPelanggan/') ?>" + cabang + ":" + tglawal + ":" + tglakhir
				);
			} else if (jenislaporan == '3') {
				window.open(
					"<?php echo base_url('cetak_pdf/LaporanPiutang/LaporanPelangganTidakAktif/') ?>" + cabang
				);
			} else if (jenislaporan == '4') {
				window.open(
					"<?php echo base_url('cetak_pdf/LaporanPiutang/LaporanInvoiceOutstanding/') ?>" + tglawal + ":" + tglakhir + ":" + cabang
				)
			} else if (jenislaporan == '5') {
				// console.log(jenislaporan);
				window.open(
					"<?php echo base_url('cetak_pdf/LaporanPiutang/LaporanJenisPembayaran/') ?>" + tglawal + ":" + tglakhir + ":" + cabang
				)
			} else if (jenislaporan == '6') {
				window.open(
					"<?php echo base_url('cetak_pdf/LaporanPiutang/LaporanKartuPiutangUsaha/') ?>" + tglawal + ":" + tglakhir + ":" + cabang
				)
			} else if (jenislaporan == '7') {
				window.open(
					"<?php echo base_url('cetak_pdf/LaporanPiutang/LaporanPiutangPerPelanggan/') ?>" + tglawal + ":" + tglakhir + ":" + cabang + ":" + nopelanggan
				)
			} else if (jenislaporan == '8') {
				window.open(
					"<?php echo base_url('cetak_pdf/LaporanPiutang/LaporanDaftarPiutang/') ?>" + tglawal + ":" + tglakhir + ":" + cabang
				)
			} else if (jenislaporan == '9') {
				window.open(
					"<?php echo base_url('cetak_pdf/LaporanPiutang/LaporanRekapPembayaran/') ?>" + tglawal + ":" + tglakhir + ":" + cabang
				)
			} else if (jenislaporan == '10') {
				window.open(
					"<?php echo base_url('cetak_pdf/LaporanPiutang/LaporanRincianPembayaranPerPelanggan/') ?>" + tglawal + ":" + tglakhir + ":" + cabang + ":" + nopelanggan
				)
			} else if (jenislaporan == '11') {
				window.open(
					"<?php echo base_url('cetak_pdf/LaporanPiutang/LaporanRincianPembayaran/') ?>" + tglawal + ":" + tglakhir + ":" + cabang
				)
			}

			// RefreshLayar();
		});

		document.getElementById("export").addEventListener("click", function(event) {
			event.preventDefault();
			var tglawal = $('#tglawal').val();
			var tglakhir = $('#tglakhir').val();
			var username = $('#username').val();
			var nopelanggan = $('#nopelanggan').val();
			var cabang = $('#kodecabang').val();
			var jenislaporan = $('#jenis_laporan').val();

			if (jenislaporan == '1') {
				window.open(
					"<?php echo base_url('cetak_pdf/LaporanPiutang/ExportPiutangJatuhTempo/') ?>" + tglawal + ":" + tglakhir + ":" + cabang
				);
			} else if (jenislaporan == '2') {
				window.open(
					"<?= base_url('cetak_pdf/LaporanPiutang/ExportDepositPelanggan/'); ?>" + cabang + ":" + tglawal + ":" + tglakhir
				)
			} else if (jenislaporan == '3') {
				window.open(
					"<?= base_url('cetak_pdf/Laporan/ExportSalesOrder/'); ?>" + tglawal + ":" + tglakhir + ":" + cabang
				)
			} else if (jenislaporan == '4') {
				window.open(
					"<?= base_url('cetak_pdf/LaporanPiutang/ExportOustandingInvoice/'); ?>" + tglawal + ":" + tglakhir + ":" + cabang
				)
			} else if (jenislaporan == '6') {
				window.open(
					"<?= base_url('cetak_pdf/LaporanPiutang/ExportKartuPiutangUsaha/'); ?>" + tglawal + ":" + tglakhir + ":" + cabang
				)
			} else if (jenislaporan == '7') {
				window.open(
					"<?= base_url('cetak_pdf/LaporanPiutang/ExportPiutangPerPelanggan/'); ?>" + tglawal + ":" + tglakhir + ":" + cabang + ":" + nopelanggan
				)
			} else if (jenislaporan == '8') {
				window.open(
					"<?= base_url('cetak_pdf/LaporanPiutang/ExportDaftarPiutang/'); ?>" + tglawal + ":" + tglakhir + ":" + cabang
				)
			} else if (jenislaporan == '9') {
				window.open(
					"<?= base_url('cetak_pdf/LaporanPiutang/ExportRekapPembayaran/'); ?>" + tglawal + ":" + tglakhir + ":" + cabang
				)
			} else if (jenislaporan == '10') {
				window.open(
					"<?php echo base_url('cetak_pdf/LaporanPiutang/ExportRincianPembayaranPerPelanggan/') ?>" + tglawal + ":" + tglakhir + ":" + cabang + ":" + nopelanggan
				)
			} else if (jenislaporan == '11') {
				window.open(
					"<?php echo base_url('cetak_pdf/LaporanPiutang/ExportRincianPembayaran/') ?>" + tglawal + ":" + tglakhir + ":" + cabang
				)
			}

			// RefreshLayar();
		})

		document.getElementById("preview").addEventListener("click", function(event) {
			event.preventDefault();
			var tglawal = $('#tglawal').val();
			var tglakhir = $('#tglakhir').val();
			var cabang = $('#kodecabang').val();
			var barang = $('#kodebarang').val();
			var username = $('#username').val();
			var merkawal = $('#merkawal').val();
			var merkakhir = $('#merkakhir').val();
			var jenislaporan = $('#jenis_laporan').val();

			if (jenislaporan == '1') {
				window.open(
					"<?php echo base_url('cetak_pdf/LaporanPiutang/PreviewLaporanPiutangJatuhTempo/') ?>" + tglawal + ":" + tglakhir + ":" + cabang
				);
			} else if (jenislaporan == '2') {
				window.open(
					"<?= base_url('cetak_pdf/LaporanPiutang/PreviewLaporanDepositPelanggan/'); ?>" + cabang + ":" + tglawal + ":" + tglakhir
				)
			} else if (jenislaporan == '3') {
				window.open(
					"<?= base_url('cetak_pdf/Laporan/ExportSalesOrder/'); ?>" + tglawal + ":" + tglakhir + ":" + cabang
				)
			} else if (jenislaporan == '4') {
				window.open(
					"<?= base_url('cetak_pdf/LaporanPiutang/PreviewLaporanInvoiceOutstanding/'); ?>" + tglawal + ":" + tglakhir + ":" + cabang
				)
			} else if (jenislaporan == '4') {
				window.open(
					"<?= base_url('cetak_pdf/Laporan/ExportPenjualanSemuaPelanggan/'); ?>" + tglawal + ":" + tglakhir + ":" + cabang
				)
			} else if (jenislaporan == '6') {
				window.open(
					"<?= base_url('cetak_pdf/LaporanPiutang/ExportRekapPenjualan/'); ?>" + tglawal + ":" + tglakhir + ":" + cabang
				)
			}

			RefreshLayar();
		})

		function RefreshLayar() {
			location.reload(true);
		};

		document.getElementById("clear").addEventListener("click", function(event) {
			event.preventDefault();
			RefreshLayar();
			LoadData();
		});

		LoadData();
	});
</script>