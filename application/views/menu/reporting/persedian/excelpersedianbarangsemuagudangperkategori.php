<?php

header("Content-type: application/vnd-ms-excel");

header("Content-Disposition: attachment; filename=Laporan Persedian Barang Semua Gudang Perkategori.xls");

header("Pragma: no-cache");

header("Expires: 0");

function FormatRupiah($angka)
{
	$hasil_rupiah = number_format($angka, 0, '.', ',');
	return $hasil_rupiah;
}
?>
<h4 style="text-align: center;">LAPORAN PERSEDIAN BARANG SEMUA GUDANG PERKATEGORI</h4>
GUDANG : SEMUA GUDANG	
<br>
KATEGORI : <?php echo $merkawal->nama ?> S/D <?php echo $merkakhir->nama ?>
<br>
PERIODE : <?php echo $tglawal ?> S/D <?php echo $tglakhir ?>
<br>

<table border="1" width="100%">

	<thead>
		<tr>
			<th>Kategori</th>
			<th>Barcode</th>
			<th>Nama Produk</th>
			<th>Qty</th>
			<th>Satuan</th>
			<th>Komisi</th>
			<th>Harga Jual</th>
			<th>Total</th>
		</tr>
	</thead>

	<tbody>
		<?php 
		$subqty = 0;
		$subtotal = 0;
		foreach($report as $val) : ?>
			<?php $total = $val->stok * $val->hargajual ?>
			<?php $subtotal = $subtotal + $total ?>
			<?php $subqty = $subqty + $val->stok ?>
			<tr>
				<td><?= $val->namamerk ?></td>
				<td><?= $val->kodebarang ?></td>
				<td><?= $val->namabarang ?></td>
				<td><?= $val->stok ?></td>
				<td><?= $val->satuan ?></td>
				<td><?= $val->komisi ?></td>
				<td><?= $val->hargajual ?></td>
				<td><?= $total ?></td>
			</tr>
		<?php endforeach; ?>
		<tr>
			<td colspan="8">
				<hr>
			</td>
		</tr>
		<tr>
			<td colspan="6" style="text-align: right;">Grandtotal: </td>
			<td><?= $subqty ?></td>
			<td><?= $subtotal ?></td>
		</tr>
	</tbody>

</table>
