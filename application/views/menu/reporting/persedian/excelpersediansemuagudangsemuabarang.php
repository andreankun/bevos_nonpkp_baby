<?php

header("Content-type: application/vnd-ms-excel");

header("Content-Disposition: attachment; filename=Laporan Persedian Barang Semua Gudang Semua kategori.xls");

header("Pragma: no-cache");

header("Expires: 0");

function FormatRupiah($angka)
{
	$hasil_rupiah = number_format($angka, 0, '.', ',');
	return $hasil_rupiah;
}
?>
<h4 style="text-align: center;">LAPORAN PERSEDIAN BARANG SEMUA GUDANG SEMUA KATEGORI</h4>
CABANG : <?= $cabang->nama ?>	
<br>
PERIODE : <?php echo $tglawal ?> S/D <?php echo $tglakhir ?>
<br>

<table border="1" width="100%">

	<thead>
		<tr>
			<th>Kategori</th>
			<th>Barcode</th>
			<th>Nama Produk</th>
			<th>Qty</th>
			<th>Satuan</th>
			<th>Komisi</th>
			<th>Harga Jual</th>
			<th>Total</th>
		</tr>
	</thead>

	<tbody>
		<?php 
		$subqty = 0;
		$subtotal = 0;
		foreach($report as $val) : ?>
			<?php $total = $val->stok * $val->hargajual ?>
			<?php $subtotal = $subtotal + $total ?>
			<?php $subqty = $subqty + $val->stok ?>
			<tr>
				<td style="text-align: left;"><?= $val->namamerk ?></td>
				<td style="text-align: left;"><?= $val->kodebarang ?></td>
				<td style="text-align: left;"><?= $val->namabarang ?></td>
				<td style="text-align: left;"><?= $val->stok ?></td>
				<td style="text-align: left;"><?= $val->satuan ?></td>
				<td style="text-align: left;"><?= $val->komisi ?></td>
				<td style="text-align: left;"><?= $val->hargajual ?></td>
				<td><?= $total ?></td>
			</tr>
		<?php endforeach; ?>
		<tr>
			<td colspan="8">
				<hr>
			</td>
		</tr>
		<tr>
			<td colspan="6" style="text-align: right;">Grandtotal: </td>
			<td><?= $subqty ?></td>
			<td><?= $subtotal ?></td>
		</tr>
	</tbody>

</table>
