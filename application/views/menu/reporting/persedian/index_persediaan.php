<div class="col-md-12 mb-2">
    <div class="modal-header" style="padding: 5px;">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-3">
                    <h2>
                        <span class="logo-menu"><?php echo $icons ?></span>
                        <span><?php echo $title ?></span>
                    </h2>
                </div>
                <div class="col-md-9">
                    <div class="form-group" style="float: right;">
						<button id="preview" class="btn btn-sm btn-danger mb-1 mt-1 mr-1 ml-1"><i class="far fa-file-pdf" style="margin-right: 10px;"></i>Preview</button>
                        <button id="print" class="btn btn-sm btn-dark mb-1 mt-1 mr-1 ml-1"><i class="far fa-print" style="margin-right: 10px;"></i>Print</button>
						<button id="export" class="btn btn-sm btn-success mb-1 mt-1 mr-1 ml-1"><i class="far fa-file-export" style="margin-right: 10px;"></i>Export</button>
                        <button id="clear" class="btn btn-sm btn-dark mb-1 mt-1 mr-1 ml-1"><i class="far fa-refresh" style="margin-right: 10px;"></i>Clear</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-md-12 table-responsive hidescroll" style="height: 555px; overflow-y: scroll;">
    <div class="row">
        <div class="col-md-6">
            <div class="modal-content mb-2">
                <div class="modal-body">
                    <div class="row mb-2 mt-2">
                        <div class="col-sm-12 input-group box">
                            <div class="col-sm-3 p-0 input-group-prepend">
                                <span class="col-sm-12 input-group-text">Laporan Dokumen</span>
                            </div>
                            <select class="form-control" style="height: 29px; padding: 0.175rem 0.50rem;" id="jenis_laporan">
                                <option value="">Pilih Laporan</option>
                                <option value="1">Laporan Persediaan All Gudang Per-Kategori</option>
                                <option value="2">Laporan Persediaan All Gudang All Kategori</option>
                                <option value="3">Laporan Persediaan Per Gudang All Kategori</option>
                                <option value="4">Laporan Barang Keluar Pergudang</option>
                                <option value="5">Laporan Barang Keluar Semua Gudang</option>
                                <option value="6">Kartu Stok Perbarang</option>
                                <option value="7">Kartu Stok Semua Gudang</option>
                                <option value="8">Kartu Stok Per Gudang</option>
                                <option value="9">Kartu Stok Perbarang Pajak</option>
                                <option value="10">Kartu Stok Semua Gudang Pajak</option>
                                <!-- <option value="2">Faktur</option> -->
                            </select>
                        </div>
                    </div>
                    <div class="row mb-2">
                        <div class="col-sm-6 input-group gudang">
                            <div class="col-sm-6 p-0 input-group-prepend">
                                <span class="col-sm-12 input-group-text">Kode Gudang</span>
                            </div>
                            <input class="form-control" type="text" name="kodegudang" id="kodegudang" maxlength="2" placeholder="Kode Gudang" value="<?php echo $this->session->userdata('mygudang'); ?>" required readonly />
                            <input class="form-control" type="hidden" name="kodecabang" id="kodecabang" maxlength="2" placeholder="Kode Gudang" value="<?php echo $this->session->userData('mycabang') ?>" required readonly />
                            <!-- <input class="form-control" type="hidden" name="kodecabang" id="gudang" maxlength="2" placeholder="Kode Gudang" value="" required readonly /> -->
                            <div class="input-group-text btn-dark" data-toggle="modal" data-target="#findgudang" id="carikodegudang">
                                <span class="input-group-addon">
                                    <i class="fa fa-search text-white"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-2">
                        <div class="col-sm-6 input-group cabang">
                            <div class="col-sm-6 p-0 input-group-prepend">
                                <span class="col-sm-12 input-group-text">Kode Cabang</span>
                            </div>
                            <input class="form-control" type="text" name="kodegudang" id="kodecabang" maxlength="2" placeholder="Kode Cabang" value="<?php echo $this->session->userdata('mycabang'); ?>" required readonly />
                            <!-- <input class="form-control" type="hidden" name="kodecabang" id="gudang" maxlength="2" placeholder="Kode Gudang" value="" required readonly /> -->
                            <div class="input-group-text btn-dark" data-toggle="modal" data-target="#findcabang" id="caricabang">
                                <span class="input-group-addon">
                                    <i class="fa fa-search text-white"></i>
                                </span>
                            </div>
                        </div>
                    </div>
					<div class="row mb-2 merk barang">
                        <div class="col-sm-6 input-group">
                            <div class="col-sm-6 p-0 input-group-prepend">
                                <span class="col-sm-12 input-group-text">Kode Barang</span>
                            </div>
							<input class="form-control" type="text" name="kodebarang" id="kodebarang" maxlength="2" placeholder="Kode Barang" required readonly />
                            <!-- <input class="form-control" type="hidden" name="kodecabang" id="gudang" maxlength="2" placeholder="Kode Gudang" value="" required readonly /> -->
                            <div class="input-group-text btn-dark" data-toggle="modal" data-target="#findbarang" id="carikodebarang">
                                <span class="input-group-addon">
                                    <i class="fa fa-search text-white"></i>
                                </span>
                            </div>
                        </div>
						<div class="col-sm-6 input-group">
                            <div class="col-sm-6 p-0 input-group-prepend">
                                <span class="col-sm-12 input-group-text">Nama Barang</span>
                            </div>
                            <input class="form-control" type="text" name="kodebarang" id="namabarang" maxlength="2" placeholder="Nama Barang" required readonly />
                        </div>
                    </div>
                    </div>
					<div class="row mb-2 merk">
                        <div class="col-sm-6 input-group">
                            <div class="col-sm-6 p-0 input-group-prepend">
                                <span class="col-sm-12 input-group-text">Kode Merk</span>
                            </div>
                            <select class="form-control" name="kodesalesman" id="merkawal" style="height: 29px; padding: 0.175rem 0.50rem;"></select>
                        </div>
						<div class="col-sm-6 input-group">
                            <div class="col-sm-6 p-0 input-group-prepend">
                                <span class="col-sm-12 input-group-text">Sampai</span>
                            </div>
                            <select class="form-control" name="kodesalesman" id="merkakhir" style="height: 29px; padding: 0.175rem 0.50rem;"></select>
                        </div>
                    </div>
                    <div class="row mb-2">
                        <div class="col-sm-6 input-group">
                            <div class="col-sm-6 p-0 input-group-prepend">
                                <span class="col-sm-12 input-group-text">Tanggal Awal</span>
                            </div>
                            <input class="form-control" id="tglawal" value="<?php echo date('01-08-Y'); ?>" padding="5px" type="text" required readonly />
                        </div>
                        <div class="col-sm-6 input-group">
                            <div class="col-sm-6 p-0 input-group-prepend">
                                <span class="col-sm-12 input-group-text">Tanggal Akhir</span>
                            </div>
                            <input class="form-control" id="tglakhir" value="<?php echo date('d-m-Y'); ?>" padding="5px" type="text" required readonly/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="findgudang">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" style="border-radius: 5px;">
            <div class="modal-header" style="padding: 10px; color: #000;">
                <h7 class="modal-title" style="margin-left: 5px;">Data Gudang</h7>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table id="t_gudang" class="table table-bordered table-striped dt-responsive display nowrap" style="width:100%">
                        <thead class="thead-dark">
                            <tr>
                                <th width="10">Action</th>
                                <th width="150">Kode Gudang</th>
                                <th width="150">Nama Gudang</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="findcabang">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" style="border-radius: 5px;">
            <div class="modal-header" style="padding: 10px; color: #000;">
                <h7 class="modal-title" style="margin-left: 5px;">Data Cabang</h7>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table id="t_cabang" class="table table-bordered table-striped dt-responsive display nowrap" style="width:100%">
                        <thead class="thead-dark">
                            <tr>
                                <th width="10">Action</th>
                                <th width="150">Kode Cabang</th>
                                <th width="150">Nama Cabang</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="findbarang">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" style="border-radius: 5px;">
            <div class="modal-header" style="padding: 10px; color: #000;">
                <h7 class="modal-title" style="margin-left: 5px;">Data Barang</h7>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table id="t_barang" class="table table-bordered table-striped dt-responsive display nowrap" style="width:100%">
                        <thead class="thead-dark">
                            <tr>
                                <th width="10">Action</th>
                                <th width="150">Kode Barang</th>
                                <th width="150">Nama Barang</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="findmerkawal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" style="border-radius: 5px;">
            <div class="modal-header" style="padding: 10px; color: #000;">
                <h7 class="modal-title" style="margin-left: 5px;">Data Merk</h7>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table id="t_merkawal" class="table table-bordered table-striped dt-responsive display nowrap" style="width:100%">
                        <thead class="thead-dark">
                            <tr>
                                <th width="10">Action</th>
                                <th width="150">Kode Merk</th>
                                <th width="150">Nama Merk</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="findmerkakhir">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" style="border-radius: 5px;">
            <div class="modal-header" style="padding: 10px; color: #000;">
                <h7 class="modal-title" style="margin-left: 5px;">Data Merk</h7>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table id="t_merkakhir" class="table table-bordered table-striped dt-responsive display nowrap" style="width:100%">
                        <thead class="thead-dark">
                            <tr>
                                <th width="10">Action</th>
                                <th width="150">Kode Merk</th>
                                <th width="150">Nama Merk</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
