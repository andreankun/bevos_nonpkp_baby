<?php

header("Content-type: application/vnd-ms-excel");

header("Content-Disposition: attachment; filename=Laporan Kartu Stok Perbarang.xls");

header("Pragma: no-cache");

header("Expires: 0");

function FormatRupiah($angka)
{
	$hasil_rupiah = number_format($angka, 0, '.', ',');
	return $hasil_rupiah;
}
?>
<h4 style="text-align: center;">LAPORAN KARTU STOK PERBARANG</h4>
GUDANG : <?php echo $gudang->nama ?>
<br>
BARANG : <?php echo $barang ?>
<br>
PERIODE : <?php echo $tglawal ?> S/D <?php echo $tglakhir ?>

<table border="1" width="100%">

<thead>
				<tr>
					<th>Tanggal</th>
					<th>Keterangan</th>
					<th>Nomor Transaksi</th>
					<th>Sales</th>
					<th>Qty Masuk</th>
					<th>Qty Keluar</th>
					<th>Mutasi Saldo</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td colspan="6">Saldo Awal</td>
					<td style="text-align:right"><?= $kartustok->qtyawal ?></td>
				</tr>
				<?php 
				$saldoawal = 0;
				$mutasisaldo = 0;
				$totalmutasi = 0;
				$qtykeluar = 0;
				$qtymasuk = 0;
				foreach($report as $val): ?>
				<?php $saldoawal = $kartustok->qtyawal ?>
				<?php $qtykeluar = $qtykeluar + $val->qtykeluar ?>
				<?php $qtymasuk = $qtymasuk + $val->qtymasuk ?>
				<?php $totalmutasi = $saldoawal + $qtymasuk - $qtykeluar ?>
					<tr>
						<td><?= date('d-m-Y', strtotime($val->tanggal)) ?></td>
						<td style="text-align:left;"><?= $val->subketerangan .' '. $val->keterangan ?></td>
						<td style="text-align:left"><?= $val->nomor ?></td>
						<td style="text-align:left"><?= $val->namasalesman ?></td>
						<td style="text-align:right"><?= $val->qtymasuk ?></td>
						<td style="text-align:right"><?= $val->qtykeluar ?></td>
						<td style="text-align:right"><?= $totalmutasi ?></td>
					</tr>
				<?php endforeach; ?>
			</tbody>

</table>
