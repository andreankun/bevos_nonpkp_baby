<?php

header("Content-type: application/vnd-ms-excel");

header("Content-Disposition: attachment; filename=Laporan Kartu Stok Merk Semua Gudang Pajak.xls");

header("Pragma: no-cache");

header("Expires: 0");

function FormatRupiah($angka)
{
	$hasil_rupiah = number_format($angka, 0, '.', ',');
	return $hasil_rupiah;
}
?>
<h4 style="text-align: center;">LAPORAN KARTU STOK MERK Pajak</h4>
CABANG: <?php echo $cabang->nama ?>
<br>
MERK : <?php echo $merkawal->nama ?> S/D <?php echo $merkakhir->nama ?>
<br>
PERIODE : <?php echo $tglawal ?> S/D <?php echo $tglakhir ?>

<table border="1" width="100%">

	<thead>
		<tr>
			<th>NO</th>
			<th>KATEGORI</th>
			<th>KODE</th>
			<th>NAMA</th>
			<th>SALDO AWAL</th>
			<th>Qty MASUK</th>
			<th>QTY KELUAR</th>
			<th>SALDO AKHIR</th>
		</tr>
	</thead>
	<tbody>
		<?php 
		$no = 1;
		$saldo_akhir = 0;
		foreach($kartustok as $val): ?>
		<?php $saldo_akhir = $val->qtyawal + $val->qtymasuk - $val->qtykeluar ?>
			<tr>
				<td><?= $no++ ?></td>
				<td><?= $val->namamerk ?></td>
				<td><?= $val->kodebarang ?></td>
				<td><?= $val->namabarang ?></td>
				<td><?= $val->qtyawal ?></td>
				<td><?= $val->qtymasuk ?></td>
				<td><?= $val->qtykeluar ?></td>
				<td><?= $saldo_akhir ?></td>
			</tr>
		<?php endforeach; ?>
	</tbody>

</table>
