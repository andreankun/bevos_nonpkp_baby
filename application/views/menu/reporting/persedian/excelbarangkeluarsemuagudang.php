<?php

header("Content-type: application/vnd-ms-excel");

header("Content-Disposition: attachment; filename=Laporan Barang Keluar Semua Gudang.xls");

header("Pragma: no-cache");

header("Expires: 0");

function FormatRupiah($angka)
{
	$hasil_rupiah = number_format($angka, 0, '.', ',');
	return $hasil_rupiah;
}
?>
<h4 style="text-align: center;">LAPORAN BARANG KELUAR SEMUA GUDANG</h4>
CABANG : <?php echo $cabang->nama ?>	
<br>
PERIODE : <?php echo $tglawal ?> S/D <?php echo $tglakhir ?>
<br>

<table border="1" width="100%">

<thead>
		<tr>
			<th>No.BK</th>
			<th>Tanggal</th>
			<th>Kode Barang</th>
			<th>Nama Barang</th>
			<th>Qty</th>
			<th>Satuan</th>
			<th>Harga</th>
			<th>Total</th>
			<th>Keterangan</th>
		</tr>
	</thead>

	<tbody>
		<?php if (!empty($persediaan)) : ?>
			<?php $no = 1;
			$total = 0;
			foreach ($persediaan as $value) : ?>
				<?php $nomor = $value->nomor ?>
				<?php foreach ($report as $val) : ?>
					<?php if ($val->nomor == $nomor) : ?>
						<?php $total = $val->qty * $val->hargajual ?>
						<tr>
							<td style="text-align: left;"><?= $val->nomor ?></td>
							<td style="text-align: left;"><?= date('d-m-Y', strtotime($val->tanggal)) ?></td>
							<td style="text-align: left;"><?= $val->kodebarang ?></td>
							<td style="text-align: left;"><?= $val->namabarang ?></td>
							<td style="text-align: right;"><?= $val->qty ?></td>
							<td style="text-align: right;"><?= $val->kodesatuan ?></td>
							<td style="text-align: right;"><?= $val->hargajual ?></td>
							<td style="text-align: right;"><?= $total ?></td>
							<td style="text-align: right;"><?= $val->keterangan ?></td>
						</tr>
					<?php endif; ?>
				<?php endforeach; ?>
			<?php endforeach; ?>
		<?php else :  ?>
			<tr>
				<td colspan="6" style="font-weight: bold; text-align: center;">Data tidak tersedia.</td>
			</tr>
		<?php endif; ?>

	</tbody>

</table>
