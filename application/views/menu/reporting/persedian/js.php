<script type="text/javascript">
	$(document).ready(function() {

		$('#jenis_laporan').change(function() {
			var jenisdokumen = this.value;
			if (jenisdokumen == '1') {
				$('.gudang').hide();
				$('.barang').hide();
				$('.cabang').show();
				$('#print').hide();
				$('#preview').show();
			} else if (jenisdokumen == '2') {
				$('.gudang').hide();
				$('.barang').hide();
				$('.cabang').show();
				$('#print').hide();
				$('#preview').show();
			} else if (jenisdokumen == '3') {
				$('.gudang').show();
				$('.barang').hide();
				$('.cabang').hide();
				$('#print').hide();
				$('#preview').show();
			} else if (jenisdokumen == '4') {
				$('.gudang').show();
				$('.merk').hide();
				$('#print').show();
				$('.barang').hide();
				$('.barang').show();
				$('#preview').hide();
			} else if (jenisdokumen == '5') {
				$('.gudang').hide();
				$('.merk').hide();
				$('#print').show();
				$('.barang').hide();
				$('.cabang').show();
				$('#preview').hide();
			} else if (jenisdokumen == '6') {
				$('.gudang').show();
				$('.merk').hide();
				$('.barang').show();
				$('.cabang').hide();
				$('#print').show();
				$('#preview').hide();
			} else if (jenisdokumen == '7') {
				$('.gudang').hide();
				$('.merk').show();
				$('.barang').hide();
				$('#print').hide();
				$('#preview').show();
			}  else if (jenisdokumen == '8') {
				$('.gudang').show();
				$('.merk').show();
				$('.barang').hide();
				$('.barang').hide();
				$('.cabang').hide();
				$('#preview').show();
			} else if (jenisdokumen == '9') {
				$('.gudang').show();
				$('.merk').hide();
				$('.barang').show();
				$('.cabang').hide();
				$('#print').show();
				$('#preview').hide();
			}else if (jenisdokumen == '10') {
				$('.gudang').hide();
				$('.merk').show();
				$('.barang').hide();
				$('.cabang').show();
				$('#print').hide();
				$('#preview').show();
			} 
		})

		$('#merkawal').select2({
			placeholder: "Pilih Barang"
		})

		$('#merkakhir').select2({
			placeholder: "Pilih Barang"
		})

		$('#tglawal').datepicker({
			format: "dd-mm-yyyy",
			autoclose: true,
			todayHightlight: true,
			startDate: '-100y'
		});

		$('#tglakhir').datepicker({
			format: "dd-mm-yyyy",
			autoclose: true,
			todayHightlight: true,
			startDate: '-100y'
		});

		/* Cari Data Cabang*/
		document.getElementById("carikodegudang").addEventListener("click", function(event) {
			event.preventDefault();
			$('#t_gudang').DataTable({
				"destroy": true,
				"searching": true,
				"processing": true,
				"serverSide": true,
				"lengthChange": true,
				"pageLength": 5,
				"lengthMenu": [5, 10, 25, 50],
				"order": [],
				"ajax": {
					"url": "<?= base_url('masterdata/Gudang/CariDataGudang'); ?>",
					"method": "POST",
					"data": {
						nmtb: "glbm_gudang",
						field: {
							kode: "kode",
							nama: "nama",
							// alamat: "alamat"
						},
						sort: "kode",
						where: {
							kode: "kode",
							nama: "nama",
							// alamat: "alamat"
						},
						value: "aktif = true"
					},
				}
			});
		});

		function LoadMerkAwal() {
            $.ajax({
                url: "<?= base_url('masterdata/Merk/DataMerkSelect') ?>",
                method: "POST",
                dataType: "json",
                async: false,
                data: {},
                success: function(data) {
                    for (var i = 0; i < data.length; i++) {
                        $("#merkawal").append('<option value = ' + data[i].id + ' >' + data[i].nama + '</option>');
                    }
                }
            }, false);
        }

		function LoadMerkAkhir() {
            $.ajax({
                url: "<?= base_url('masterdata/Merk/DataMerkSelectAkhir') ?>",
                method: "POST",
                dataType: "json",
                async: false,
                data: {},
                success: function(data) {
                    for (var i = 0; i < data.length; i++) {
                        $("#merkakhir").append('<option value = ' + data[i].id + ' >' + data[i].nama + '</option>');
                    }
                }
            }, false);
        }


		/*Get Data Cabang*/
		$(document).on('click', ".searchgudang", function() {
			var kode = $(this).attr("data-id");
			$.ajax({
				url: "<?= base_url('masterdata/Gudang/DataGudang'); ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					kode: kode
				},
				success: function(data) {
					for (var i = 0; i < data.length; i++) {
						$('#kodegudang').val(data[i].kode.trim());
						$('#kodecabang').val(data[i].kodecabang.trim());
					}
				}
			}, false);
		});

		/* Cari Data Cabang*/
		document.getElementById("caricabang").addEventListener("click", function(event) {
			event.preventDefault();
			$('#t_cabang').DataTable({
				"destroy": true,
				"searching": true,
				"processing": true,
				"serverSide": true,
				"lengthChange": true,
				"pageLength": 5,
				"lengthMenu": [5, 10, 25, 50],
				"order": [],
				"ajax": {
					"url": "<?= base_url('masterdata/Cabang/CariDataCabang'); ?>",
					"method": "POST",
					"data": {
						nmtb: "glbm_cabang",
						field: {
							kode: "kode",
							nama: "nama",
							// alamat: "alamat"
						},
						sort: "kode",
						where: {
							kode: "kode",
							nama: "nama",
							// alamat: "alamat"
						},
						value: "aktif = true"
					},
				}
			});
		});

		/*Get Data Cabang*/
		$(document).on('click', ".searchcabang", function() {
			var kode = $(this).attr("data-id");
			$.ajax({
				url: "<?= base_url('masterdata/Cabang/DataCabang'); ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					kode: kode
				},
				success: function(data) {
					for (var i = 0; i < data.length; i++) {
						$('#kodecabang').val(data[i].kode.trim());
					}
				}
			}, false);
		});

		/* Cari Data Cabang*/
		document.getElementById("carikodebarang").addEventListener("click", function(event) {
			event.preventDefault();
			$('#t_barang').DataTable({
				"destroy": true,
				"searching": true,
				"processing": true,
				"serverSide": true,
				"lengthChange": true,
				"pageLength": 5,
				"lengthMenu": [5, 10, 25, 50],
				"order": [],
				"ajax": {
					"url": "<?= base_url('masterdata/Barang/CariDataBarang'); ?>",
					"method": "POST",
					"data": {
						nmtb: "glbm_barang",
						field: {
							kode: "kode",
							nama: "nama",
							// alamat: "alamat"
						},
						sort: "kode",
						where: {
							kode: "kode",
							nama: "nama",
							// alamat: "alamat"
						},
					},
				}
			});
		});

		/*Get Data Cabang*/
		$(document).on('click', ".searchbarang", function() {
			var kode = $(this).attr("data-id");
			$.ajax({
				url: "<?= base_url('masterdata/Barang/DataBarang'); ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					kode: kode
				},
				success: function(data) {
					for (var i = 0; i < data.length; i++) {
						$('#kodebarang').val(data[i].kode.trim());
						$('#namabarang').val(data[i].nama.trim());
					}
				}
			}, false);
		});

		function LoadBarang() {
			$.ajax({
				url: "<?= base_url('masterdata/Barang/DataBrg') ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {},
				success: function(data) {
					for (var i = 0; i < data.length; i++) {
						$("#kodebarang").append('<option value = ' + data[i].id + ' >' + data[i].text + '</option>');
					}
				}
			}, false);
		}

		
		document.getElementById("print").addEventListener("click", function(event) {
			event.preventDefault();
			var tglawal = $('#tglawal').val();
			var tglakhir = $('#tglakhir').val();
			var gudang = $('#kodegudang').val();
			var merkawal = $('#merkawal').val();
			var merkakhir = $('#merkakhir').val();
			var jenislaporan = $('#jenis_laporan').val();
			var cabang = $('#kodecabang').val();
			var barang = $('#kodebarang').val();
			if (jenislaporan == '1') {
				window.open(
					"<?php echo base_url('cetak_pdf/LaporanPersediaan/PersediaanBarangAllgudangPerkategori/') ?>" + tglawal + ":" + tglakhir + ":" + merkawal + ":" + merkakhir + ":" + cabang
				);
			} else if (jenislaporan == '2') {
				window.open(
					"<?php echo base_url('cetak_pdf/LaporanPersediaan/PersedianBarangSemuaGudangSemuaKategori/') ?>" + tglawal + ":" + tglakhir + ":" + merkawal + ":" + merkakhir + ":" + cabang
				);
			} else if (jenislaporan == '3') {
				window.open(
					"<?php echo base_url('cetak_pdf/LaporanPersediaan/PersediaanBarangPergudangAllKategori/') ?>" + tglawal + ":" + tglakhir + ":" + merkawal + ":" + merkakhir + ":" + gudang + ":" + cabang
				);
			} else if (jenislaporan == '4') {
				window.open(
					"<?php echo base_url('cetak_pdf/LaporanPersediaan/LaporanBarangKeluar/') ?>" + tglawal + ":" + tglakhir + ":" + gudang
				);
			} else if (jenislaporan == '5') {
				window.open(
					"<?php echo base_url('cetak_pdf/LaporanPersediaan/LaporanBarangKeluarSemuaGudang/') ?>" + tglawal + ":" + tglakhir + ":" + cabang
				);
			} else if (jenislaporan == '6') {
				window.open(
					"<?php echo base_url('cetak_pdf/LaporanPersediaan/CetakKartuStokPerbarang/') ?>" + tglawal + ":" + tglakhir + ":" + barang + ":" + gudang
				);
			} else if (jenislaporan == '7') {
				window.open(
					"<?php echo base_url('cetak_pdf/LaporanPersediaan/CetakKartuStokSemuaBarang/') ?>" + tglawal + ":" + tglakhir + ":" + merkawal + ":" + merkakhir + ":" + cabang
				);
			} else if (jenislaporan == '9') {
				window.open(
					"<?php echo base_url('cetak_pdf/LaporanPersediaan/CetakKartuStokPerbarangPajak/') ?>" + tglawal + ":" + tglakhir + ":" + barang + ":" + gudang
				);
			} else if (jenislaporan == '10') {
				window.open(
					"<?php echo base_url('cetak_pdf/LaporanPersediaan/CetakKartuStokSemuaBarangPajak/') ?>" + tglawal + ":" + tglakhir + ":" + merkawal + ":" + merkakhir + ":" + cabang
				);
			}
		});

		document.getElementById('export').addEventListener("click", function(event) {
			event.preventDefault()
			var tglawal = $('#tglawal').val();
			var tglakhir = $('#tglakhir').val();
			var gudang = $('#kodegudang').val();
			var merkawal = $('#merkawal').val();
			var merkakhir = $('#merkakhir').val();
			var jenislaporan = $('#jenis_laporan').val();
			var cabang = $('#kodecabang').val();
			var barang = $('#kodebarang').val();
			if (jenislaporan == '1') {
				window.open(
					"<?php echo base_url('cetak_pdf/LaporanPersediaan/ExportPersedianSemuaGudangPerkategori/') ?>" + tglawal + ":" + tglakhir + ":" + merkawal + ":" + merkakhir + ":" + cabang
				);
			} else if (jenislaporan == '2') {
				window.open(
					"<?php echo base_url('cetak_pdf/LaporanPersediaan/ExportPersedianSemuaGudangSemuaBarang/') ?>" + tglawal + ":" + tglakhir + ":" + merkawal + ":" + merkakhir + ":" + cabang
				);
			} else if (jenislaporan == '3') {
				window.open(
					"<?php echo base_url('cetak_pdf/LaporanPersediaan/ExportPersedianPerGudangPerkategori/') ?>" + tglawal + ":" + tglakhir + ":" + merkawal + ":" + merkakhir + ":" + gudang + ":" + cabang
				);
			} else if (jenislaporan == '4') {
				window.open(
					"<?php echo base_url('cetak_pdf/LaporanPersediaan/ExportBarangKeluarPergudang/') ?>" + tglawal + ":" + tglakhir + ":" + gudang
				);
			} else if (jenislaporan == '5') {
				window.open(
					"<?php echo base_url('cetak_pdf/LaporanPersediaan/ExportBarangKeluarSemuaGudang/') ?>" + tglawal + ":" + tglakhir + ":" + cabang
				);
			} else if (jenislaporan == '6') {
				window.open(
					"<?php echo base_url('cetak_pdf/LaporanPersediaan/ExportLaporanKartuStokPerbarang/') ?>" + tglawal + ":" + tglakhir + ":" + barang + ":" + gudang
				);
			} else if (jenislaporan == '7') {
					window.open(
						"<?php echo base_url('cetak_pdf/LaporanPersediaan/ExportLaporanKartuStokMerk/') ?>" + tglawal + ":" + tglakhir + ":" + merkawal + ":" + merkakhir + ":" + cabang
					);
				
			} else if (jenislaporan == '8') {
					window.open(
						"<?php echo base_url('cetak_pdf/LaporanPersediaan/ExportLaporanKartuStokMerkPegudang/') ?>" + tglawal + ":" + tglakhir + ":" + merkawal + ":" + merkakhir + ":" + gudang + ":" + cabang
					);
				
			} else if (jenislaporan == '9') {
					window.open(
						"<?php echo base_url('cetak_pdf/LaporanPersediaan/ExportLaporanKartuStokPerbarangPajak/') ?>" + tglawal + ":" + tglakhir + ":" + merkawal + ":" + merkakhir + ":" + gudang
					);
				
			} else if (jenislaporan == '10') {
					window.open(
						"<?php echo base_url('cetak_pdf/LaporanPersediaan/ExportLaporanKartuStokMerkPajak/') ?>" + tglawal + ":" + tglakhir + ":" + merkawal + ":" + merkakhir + ":" + cabang
					);
				
			}
		});

		document.getElementById("preview").addEventListener("click", function(event) {
			event.preventDefault();
			var tglawal = $('#tglawal').val();
			var tglakhir = $('#tglakhir').val();
			var gudang = $('#kodegudang').val();
			var merkawal = $('#merkawal').val();
			var merkakhir = $('#merkakhir').val();
			var jenislaporan = $('#jenis_laporan').val();
			var cabang = $('#kodecabang').val();
			var barang = $('#kodebarang').val();
			if (jenislaporan == '1') {
				window.open(
					"<?php echo base_url('cetak_pdf/LaporanPersediaan/PreviewPersediaanBarangAllgudangPerkategori/') ?>" + tglawal + ":" + tglakhir + ":" + merkawal + ":" + merkakhir + ":" + cabang
				);
			} else if (jenislaporan == '2') {
				window.open(
					"<?php echo base_url('cetak_pdf/LaporanPersediaan/PreviewPersedianBarangSemuaGudangSemuaKategori/') ?>" + tglawal + ":" + tglakhir + ":" + merkawal + ":" + merkakhir + ":" + cabang
				);
			} else if (jenislaporan == '3') {
				window.open(
					"<?php echo base_url('cetak_pdf/LaporanPersediaan/PreviewPersediaanBarangPergudangAllKategori/') ?>" + tglawal + ":" + tglakhir + ":" + merkawal + ":" + merkakhir + ":" + gudang + ":" + cabang
				);
			}  else if (jenislaporan == '7') {
				window.open(
					"<?php echo base_url('cetak_pdf/LaporanPersediaan/PreviewCetakKartuStokSemuaBarang/') ?>" + tglawal + ":" + tglakhir + ":" + merkawal + ":" + merkakhir + ":" + cabang
				);
			} else if (jenislaporan == '10') {
				window.open(
					"<?php echo base_url('cetak_pdf/LaporanPersediaan/PreviewCetakKartuStokSemuaBarangPajak/') ?>" + tglawal + ":" + tglakhir + ":" + merkawal + ":" + merkakhir + ":" + cabang
				);
			}
		});

		function RefreshLayar() {
			location.reload(true);
		};


		document.getElementById("clear").addEventListener("click", function(event) {
			event.preventDefault();
			RefreshLayar();
		});

		LoadBarang();
		LoadMerkAwal();
		LoadMerkAkhir();
	});
</script>
