<div class="col-12 col-xl-12 mb-2">
    <div class="modal-header" style="padding: 5px;">
        <div class="col-12 col-md-12">
            <div class="row">
                <div class="col-5 col-md-5">
                    <h2>
                        <span class="logo-menu"><?php echo $icons ?></span>
                        <span class="text-uppercase"><?php echo $title ?></span>
                    </h2>
                </div>
                <div class="col-7 col-md-7">
                    <div class="form-group" style="float: right;">
                        <button id="save" class="btn btn-sm btn-dark m-1" style="margin-bottom: 0px;"><i class="far fa-save" style="margin-right: 10px;"></i>Save</button>
                        <button data-toggle="modal" data-target="#findpengeluaran" id="find" class="btn btn-sm btn-dark m-1" style="margin-bottom: 0px;"><i class="far fa-file-search" style="margin-right: 10px;"></i>Find</button>
                        <button id="cancel" class="btn btn-sm btn-danger m-1" data-toggle="modal" data-target="#modalcancel" disabled><i class="far fa-times-circle" style="margin-right: 10px;"></i>Cancel</button>
                        <button id="clear" class="btn btn-sm btn-dark m-1" style="margin-bottom: 0px;"><i class="far fa-refresh" style="margin-right: 10px;"></i>Clear</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-md-12 table-responsive hidescroll" style="height: 555px; overflow-y: scroll;">
    <div class="row">
        <div class="col-12 col-md-12 col-lg-12 col-xl-6">
            <div class="modal-content mb-2">
                <div class="modal-body">
                    <div class="row mb-2 mt-2">
                        <div class="col-sm-6 input-group">
                            <div class="col-sm-6 p-0 input-group-prepend">
                                <span class="col-sm-12 input-group-text">Nomor</span>
                            </div>
                            <input class="form-control" type="text" name="nomor" id="nomor" maxlength="50" placeholder="Nomor" readonly required />
                            <input class="form-control" type="hidden" name="cabang" id="cabang" maxlength="50" value="<?php echo $this->session->userdata('mycabang') ?>" readonly required />
                        </div>
                        <div class="col-sm-6 input-group date" id="tgl">
                            <div class="col-sm-6 p-0 input-group-prepend">
                                <span class="col-sm-12 input-group-text">Tanggal</span>
                            </div>
                            <input class="form-control" id="tanggal" value="<?php echo date('Y-m-d'); ?>" padding="5px" type="text" maxlength="50" readonly required />
                            <div class="input-group-text btn-dark">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar text-white"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-2 mt-2">
                        <div class="col-sm-12 input-group">
                            <div class="col-sm-3 p-0 input-group-prepend">
                                <span class="col-sm-12 input-group-text">Jenis Pengeluaran</span>
                            </div>
                            <input class="col-sm-3 form-control" type="text" name="kodepengeluaran" id="kodepengeluaran" maxlength="50" placeholder="Kode Pengeluaran" readonly required />
                            <input class="form-control" type="text" name="namapengeluaran" id="namapengeluaran" maxlength="50" placeholder="Nama Pengeluaran" readonly required />
                            <div class="input-group-text btn-dark" data-toggle="modal" data-target="#findjenispengeluaran" id="jenispengeluaran">
                                <span class="input-group-addon">
                                    <i class="fa fa-search text-white"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-2 mt-2">
                        <div class="col-sm-6 input-group">
                            <div class="col-sm-6 p-0 input-group-prepend">
                                <span class="col-sm-12 input-group-text">Nomor Permohonan</span>
                            </div>
                            <input class="form-control" type="text" name="nopermohonan" id="nopermohonan" maxlength="50" placeholder="Nomor" readonly required />
                            <div class="input-group-text btn-dark" data-toggle="modal" data-target="#findpermohonan" id="caripermohonan">
                                <span class="input-group-addon">
                                    <i class="fa fa-search text-white"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-2">
                        <div class="col-sm-12 input-group">
                            <div class="col-sm-3 p-0 input-group-prepend">
                                <span class="col-sm-12 input-group-text">Departemen</span>
                            </div>
                            <input class="col-sm-3 form-control" type="text" name="kodedepartement" id="kodedepartement" maxlength="50" placeholder="Kode" readonly required />
                            <input class="form-control" type="text" name="namadepartement" id="namadepartement" maxlength="50" placeholder="Nama" readonly required />
                        </div>
                    </div>
                    <div class="row mb-2">
                        <div class="col-sm-12 input-group">
                            <div class="col-sm-3 p-0 input-group-prepend">
                                <span class="col-sm-12 input-group-text">Keterangan</span>
                            </div>
                            <textarea name="keterangan" id="keterangan" class="form-control" placeholder="Keterangan" readonly></textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-12 col-lg-12 col-xl-12">
            <div class="modal-content mb-2">
                <div class="modal-body">
                    <div class="row mb-2">
                        <div class="table-responsive">
                            <table id="t_detail" class="table table-bordered table-striped table-default" style="width:100%">
                                <thead class="thead-dark">
                                    <tr style="line-height: 0.5 cm; ">
                                        <th style="text-align: center; ">
                                            DocInvoice
                                        </th>
                                        <th style="text-align: center; ">
                                            Jenis
                                        </th>
                                        <th style="text-align: center; ">
                                            Pengeluaran
                                        </th>
                                        <th style="text-align: center; ">
                                            NoReferensi
                                        </th>
                                        <th style="text-align: center; ">
                                            Referensi
                                        </th>
                                        <th style="text-align: center; ">
                                            Account
                                        </th>
                                        <th style="text-align: center; ">
                                            NamaAccount
                                        </th>
                                        <th style="text-align: center; ">
                                            NilaiPengeluaran
                                        </th>
                                        <th style="text-align: center; ">
                                            AccountAlokasi
                                        </th>
                                        <th style="text-align: center; ">
                                            NilaiAlokasi
                                        </th>
                                        <th style="text-align: center; ">
                                            Memo
                                        </th>
                                        <th style="text-align: center; ">
                                            Action
                                        </th>
                                    </tr>
                                </thead>
                                <tbody class="tbody-scroll scroller" id="tb_detail">

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="modal-footer p-1">
                    <!-- <div class="row mb-2 mt-2">
                        <div class="col-sm-12 input-group">
                            <div class="col-sm-6 p-0 input-group-prepend">
                                <span class="col-sm-12 input-group-text">Total</span>
                            </div>
                            <input class="form-control" type="text" name="total" id="total" maxlength="50" placeholder="0" readonly required/>
                        </div>
                    </div> -->
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="findjenispengeluaran">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" style="border-radius: 5px;">
            <div class="modal-header" style="padding: 10px; color: #000000;">
                <h7 class="modal-title" style="margin-left: 5px;">Data Jenis Pengeluaran</h7>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table id="t_jenispengeluaran" class="table table-bordered table-striped" style="width:100%">
                        <thead class="thead-dark">
                            <tr style="line-height: 0.5 cm; ">
                                <th style="text-align: center; ">
                                    <span style="font-weight: bold; color: #eee;">Action</span>
                                </th>
                                <th style="text-align: center; ">
                                    <span style="font-weight: bold; color: #eee;">Kode</span>
                                </th>
                                <th style="text-align: center; ">
                                    <span style="font-weight: bold; color: #eee;">Nama</span>
                                </th>
                            </tr>
                        </thead>
                        <tbody></tbody>

                    </table>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="findpermohonan">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" style="border-radius: 5px;">
            <div class="modal-header" style="padding: 10px; color: #000000;">
                <h7 class="modal-title" style="margin-left: 5px;">Data Permohonan</h7>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table id="t_permohonan" class="table table-bordered table-striped" style="width:100%">
                        <thead class="thead-dark">
                            <tr style="line-height: 0.5 cm; ">
                                <th style="text-align: center; ">
                                    <span style="font-weight: bold; color: #eee;">Action</span>
                                </th>
                                <th style="text-align: center; ">
                                    <span style="font-weight: bold; color: #eee;">Nomor</span>
                                </th>
                                <th style="text-align: center; ">
                                    <span style="font-weight: bold; color: #eee;">Tanggal</span>
                                </th>
                                <th style="text-align: center; ">
                                    <span style="font-weight: bold; color: #eee;">Jenis</span>
                                </th>
                                <th style="text-align: center; ">
                                    <span style="font-weight: bold; color: #eee;">Keterangan</span>
                                </th>
                            </tr>
                        </thead>
                        <tbody></tbody>

                    </table>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="findpengeluaran">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" style="border-radius: 5px;">
            <div class="modal-header" style="padding: 10px; color: #000000;">
                <h7 class="modal-title" style="margin-left: 5px;">Data Pengeluaran</h7>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table id="t_pengeluaran" class="table table-bordered table-striped" style="width:100%">
                        <thead class="thead-dark">
                            <tr style="line-height: 0.5 cm; ">
                                <th style="text-align: center; ">
                                    <span style="font-weight: bold; color: #eee;">Action</span>
                                </th>
                                <th style="text-align: center; ">
                                    <span style="font-weight: bold; color: #eee;">Nomor</span>
                                </th>
                                <th style="text-align: center; ">
                                    <span style="font-weight: bold; color: #eee;">Tanggal</span>
                                </th>
                                <th style="text-align: center; ">
                                    <span style="font-weight: bold; color: #eee;">Jenis Pengeluaran</span>
                                </th>
                                <th style="text-align: center; ">
                                    <span style="font-weight: bold; color: #eee;">Nomor Departemen</span>
                                </th>
                                <th style="text-align: center; ">
                                    <span style="font-weight: bold; color: #eee;">Nama Departemen</span>
                                </th>
                            </tr>
                        </thead>
                        <tbody></tbody>

                    </table>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="modalcancel">
    <div class="modal-dialog modal-md">
        <div class="modal-content" style="border-radius: 5px;">
            <div class="modal-header" style="padding: 10px;">
                <h5 class="modal-title" style="margin-left: 5px; color: #f44336;"><i class="far fa-info-circle mr-1"></i>CANCEL DOKUMEN</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <center>
                    <h5 class="modal-title"></i>Apakah Anda Yakin ?</h5>
                </center>
                <div class="row mb-2">
                    <div class="col-sm-12 input-group box">
                        <!-- <input class="col-sm-12 form-control" type="hidden" name="cancelnopr" id="cancelnopr" maxlength="50" placeholder="Nomor PR" readonly required/> -->
                    </div>
                </div>
                <div class="row mb-2">
                    <div class="col-sm-12 input-group box">
                        <div class="col-sm-3 p-0 input-group-prepend">
                            <span class="col-sm-12 input-group-text">Alasan Batal</span>
                        </div>
                        <textarea name="keteranganbatal" id="keteranganbatal" class="form-control" placeholder="Alasan Batal ..."></textarea>
                    </div>
                </div>
            </div>
            <div class="modal-footer p-2">
                <button id="okcancel" type="button" class="btn btn-dark mb-0" data-dismiss="modal"><i class="far fa-check-circle mr-1"></i>Confirm</button>
            </div>
        </div>
    </div>
</div>