<script type="text/javascript">
    $(document).ready(function() {

        function clearScreen() {
            $('#notif_fire').val("fireon");

            $('#save').prop('disabled', false);

            $('#nomor').val('');
            $('#tanggal').val($.datepicker.formatDate('yy-mm-dd', new Date()));
            $('#kodepengeluaran').val('');
            $('#namapengeluaran').val('');
            $('#kodedepartement').val('');
            $('#namadepartement').val('');
            $('#nopermohonan').val('');
            $('#nomoracc').val('');
            $('#namaacc').val('');
            $('#nilaipengeluaran').val('');
            $('#keterangan').val('');
            $('#keteranganbatal').val('');

            $('#jenispengeluaran').show();
            $('#caripermohonan').show()

            $('#tb_detail').empty();
        };

        function FormatRupiah(angka, prefix) {
            if (!angka) {
                return '';
            }
            var vangka = angka.toString();
            var number_string = vangka.replace(/[^.\d]/g, '').replace(/[^\w\s]/gi, '').toString(),
                split = number_string.split('.'),
                sisa = split[0].length % 3,
                rupiah = split[0].substr(0, sisa),
                ribuan = split[0].substr(sisa).match(/\d{3}/gi);

            // tambahkan titik jika yang di input sudah menjadi angka ribuan
            if (ribuan) {
                separator = sisa ? ',' : '';
                rupiah += separator + ribuan.join(',');
            }

            rupiah = split[1] != undefined ? rupiah + '.' + split[1] : rupiah;
            return prefix == undefined ? rupiah : (rupiah ? rupiah : '');
        };

        function DeFormatRupiah(angka) {
            var result = angka.replace(/[^\w\s]/gi, '');

            return result;
        };

        document.getElementById("jenispengeluaran").addEventListener("click", function(event) {
            event.preventDefault();
            $('#t_jenispengeluaran').DataTable({
                "destroy": true,
                "searching": true,
                "processing": true,
                "serverSide": true,
                "lengthChange": true,
                "order": [],
                "ajax": {
                    "url": "<?php echo base_url('finance/Pengeluaranuang/CariJenisPengeluaran'); ?>",
                    "method": "POST",
                    "data": {
                        nmtb: "stpm_otorisasikasir",
                        field: {
                            kode: "kode",
                            nama: "nama",
                            jenis: "jenis",
                        },
                        sort: "kode",
                        where: {
                            kode: "kode"
                        },
                        value: "aktif = true AND jenis = '1'"
                    },
                }
            });
        });

        $(document).on('click', ".searchjenis", function() {
            var kode = $(this).attr("data-id");
            DataJenisPengeluaran(kode);

        });

        function DataJenisPengeluaran(kode) {
            $.ajax({
                url: "<?php echo base_url('finance/Pengeluaranuang/DataJenisPengeluaran'); ?>",
                method: "POST",
                dataType: "json",
                async: false,
                data: {
                    kode: kode,
                },
                success: function(data) {
                    for (var i = 0; i < data.length; i++) {
                        $('#kodepengeluaran').val(data[i].kode.trim());
                        $('#namapengeluaran').val(data[i].nama.trim());

                        if (data[i].kode.trim() == 'KUF99') {
                            $('#grupdepartement').show();
                        } else {
                            $('#grupdepartement').hide();
                        }
                    }

                }
            });
        }

        document.getElementById("caripermohonan").addEventListener("click", function(event) {
            event.preventDefault();
            var kodejenis = $('#kodepengeluaran').val();
            $('#tb_detail').empty();
            $('#t_permohonan').DataTable({
                "destroy": true,
                "searching": true,
                "processing": true,
                "serverSide": true,
                "lengthChange": true,
                "order": [],
                "ajax": {
                    "url": "<?php echo base_url('finance/Pengeluaranuang/CariPermohonan'); ?>",
                    "method": "POST",
                    "data": {
                        nmtb: "trnt_permohonanuang",
                        field: {
                            nomor: "nomor",
                            tanggal: "tanggal",
                            jenispengeluaran: "jenispengeluaran",
                            keterangan: "keterangan",
                        },
                        sort: "nomor",
                        where: {
                            nomor: "nomor"
                        },
                        value: "jenispengeluaran = '" + kodejenis + "' AND batal = false AND statuspengeluaran = false"
                    },
                }
            });
        });

        $(document).on('click', ".searchpermohonan", function() {
            var nomor = $(this).attr("data-id");
            $.ajax({
                url: "<?php echo base_url('finance/Pengeluaranuang/DataPermohonan'); ?>",
                method: "POST",
                dataType: "json",
                async: false,
                data: {
                    nomor: nomor,
                },
                success: function(data) {
                    for (var i = 0; i < data.length; i++) {
                        $('#nopermohonan').val(data[i].nomor.trim());
                        $('#kodedepartement').val(data[i].kodedepartement.trim());
                        $('#namadepartement').val(data[i].namadepartement.trim());
                        $('#keterangan').val(data[i].keterangan.trim());
                    }
                    GetDataPermohonanDetail(nomor);
                }
            });
        });

        function GetDataPermohonanDetail(nomor) {
            $.ajax({
                url: "<?php echo base_url('finance/Pengeluaranuang/DataPermohonanDetail'); ?>",
                method: "POST",
                dataType: "json",
                async: false,
                data: {
                    nomor: nomor
                },
                success: function(data) {
                    for (var i = 0; i < data.length; i++) {
                        var noinvoice = data[i].noreferensi.trim();
                        var kodepengeluaran = data[i].jenispengeluaran.trim();
                        var namapengeluaran = data[i].namapengeluaran.trim();
                        var noreferensi = data[i].kodesupplier.trim();
                        var namareferensi = data[i].namasupplier.trim();
                        var nomoracc = data[i].kodeaccount.trim();
                        var namaacc = data[i].namaaccount.trim();
                        var nilaipengeluaran = data[i].nilaipermohonan.trim();
                        var accountalokasi = data[i].accountalokasi.trim();
                        var nilaialokasi = data[i].nilaialokasi.trim();
                        var memo = data[i].memo.trim();

                        InsertDetailPermohonanUang(noinvoice, kodepengeluaran, namapengeluaran, noreferensi, namareferensi, nomoracc, namaacc, nilaipengeluaran, accountalokasi, nilaialokasi, memo);
                    }
                }
            });
        };

        function InsertDetailPermohonanUang(noinvoice, kodepengeluaran, namapengeluaran, noreferensi, namareferensi, nomoracc, namaacc, nilaipengeluaran, accountalokasi, nilaialokasi, memo) {
            var row = "";
            row =
                '<tr id="' + noinvoice + '">' +
                '<td>' + noinvoice + '</td>' +
                '<td>' + kodepengeluaran + '</td>' +
                '<td>' + namapengeluaran + '</td>' +
                '<td>' + noreferensi + '</td>' +
                '<td>' + namareferensi + '</td>' +
                '<td>' + nomoracc + '</td>' +
                '<td>' + namaacc + '</td>' +
                '<td>' + FormatRupiah(nilaipengeluaran) + '</td>' +
                '<td>' + accountalokasi + '</td>' +
                '<td>' + FormatRupiah(nilaialokasi) + '</td>' +
                '<td>' + memo + '</td>' +
                '<td></td>' +
                '</tr>';
            $('#tb_detail').append(row);
        };

        document.getElementById("find").addEventListener("click", function(event) {
            event.preventDefault();
            $('#tb_detail').empty();
            $('#t_pengeluaran').DataTable({
                "destroy": true,
                "searching": true,
                "processing": true,
                "serverSide": true,
                "lengthChange": true,
                "order": [],
                "ajax": {
                    "url": "<?php echo base_url('finance/Pengeluaranuang/CariPengeluaran'); ?>",
                    "method": "POST",
                    "data": {
                        nmtb: "trnt_pengeluaranuang",
                        field: {
                            nomor: "nomor",
                            tanggal: "tanggal",
                            jenispengeluaran: "jenispengeluaran",
                            kodedepartement: "kodedepartement",
                            namadepartement: "namadepartement",
                        },
                        sort: "nomor",
                        where: {
                            nomor: "nomor"
                        },
                        value: "batal = false"
                    },
                },
                "columnDefs": [{
                    "targets": 2,
                    "render": $.fn.dataTable.render.moment('YYYY-MM-DD HH:mm:ss', 'D-MMM-YY')
                }, ]
            });
        });

        $(document).on('click', ".searchok", function() {
            var nomor = $(this).attr("data-id");
            $.ajax({
                url: "<?php echo base_url('finance/Pengeluaranuang/DataPengeluaran'); ?>",
                method: "POST",
                dataType: "json",
                async: false,
                data: {
                    nomor: nomor,
                },
                success: function(data) {
                    for (var i = 0; i < data.length; i++) {
                        $('#nomor').val(data[i].nomor.trim());
                        $('#kodepengeluaran').val(data[i].jenispengeluaran.trim());
                        DataJenisPengeluaran(data[i].jenispengeluaran.trim());
                        $('#nopermohonan').val(data[i].nopermohonan.trim());
                        $('#kodedepartement').val(data[i].kodedepartement.trim());
                        $('#namadepartement').val(data[i].namadepartement.trim());
                        $('#keterangan').val(data[i].keterangan.trim());
                    }
                    GetDataPengeluaranDetail(nomor);
                    $('#jenispengeluaran').hide();
                    $('#caripermohonan').hide()
                    $('#cancel').prop('disabled', false);
                    $('#save').prop('disabled', true);
                    $('#keteranganbatal').val('');
                }
            });
        });



        function GetDataPengeluaranDetail(nomor) {
            $.ajax({
                url: "<?php echo base_url('finance/Pengeluaranuang/DataPengeluaranDetail'); ?>",
                method: "POST",
                dataType: "json",
                async: false,
                data: {
                    nomor: nomor,
                },
                success: function(data) {
                    for (var i = 0; i < data.length; i++) {
                        var noinvoice = data[i].noreferensi.trim();
                        var kodepengeluaran = data[i].jenispengeluaran.trim();
                        var namapengeluaran = data[i].namapengeluaran.trim();
                        var noreferensi = data[i].kodesupplier.trim();
                        var namareferensi = data[i].namasupplier.trim();
                        var nomoracc = data[i].kodeaccount.trim();
                        var namaacc = data[i].namaaccount.trim();
                        var nilaipengeluaran = data[i].nilaipengeluaran.trim();
                        var accountalokasi = data[i].accountalokasi.trim();
                        var nilaialokasi = data[i].nilaialokasi.trim();
                        var memo = data[i].memo.trim();

                        InsertDetailPermohonanUang(noinvoice, kodepengeluaran, namapengeluaran, noreferensi, namareferensi, nomoracc, namaacc, nilaipengeluaran, accountalokasi, nilaialokasi, memo)
                    }
                }
            });
        };


        function AmbilDataTableDetail() {
            var table = document.getElementById('t_detail');
            var arr2 = [];
            for (var r = 1, n = table.rows.length; r < n; r++) {
                var string = "";
                for (var c = 0, m = table.rows[r].cells.length; c < m - 1; c++) {
                    if (c == 0) {
                        string = "{" + table.rows[0].cells[c].innerHTML + " : '" + table.rows[r].cells[c].innerHTML + "'";
                    } else {
                        string = string + ", " + table.rows[0].cells[c].innerHTML + " : '" + table.rows[r].cells[c].innerHTML + "'";
                    }
                }
                string = string + "}";
                var obj = JSON.stringify(eval('(' + string + ')'));
                var arr = $.parseJSON(obj);
                arr2.push(arr);
            }
            return arr2;
        };

        function validasiSave() {
            var datadetail = AmbilDataTableDetail();
            if ($('#kodepengeluaran').val() == '') {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Jenis Pengeluaran tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#kodepengeluaran').focus();
                var result = false;
            } else if ($('#keterangan').val() == '') {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Keterangan tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#keterangan').focus();
                var result = false;
            } else if ($('#kodepengeluaran').val() == 'KUF99' && $('#kodedepartement').val() == '') {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Kode Department boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                var result = false;
            } else if (datadetail.length == 0) {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Data Detail tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                var result = false;
            } else {
                var result = true;
            }
            return result;
        };

        document.getElementById("save").addEventListener("click", function(event) {
            event.preventDefault();

            var tanggal = $('#tanggal').val();
            var kodepengeluaran = $('#kodepengeluaran').val();
            var namapengeluaran = $('#namapengeluaran').val();
            var kodedepartement = $('#kodedepartement').val();
            var namadepartement = $('#namadepartement').val();
            var nopermohonan = $('#nopermohonan').val();
            var keterangan = $('#keterangan').val();
            var cabang = $('#cabang').val();
            var datadetail = AmbilDataTableDetail();

            if (validasiSave() == true) {
                $.ajax({
                    url: "<?php echo base_url('finance/Pengeluaranuang/Save'); ?>",
                    method: "POST",
                    dataType: "json",
                    async: false,
                    data: {
                        tanggal: tanggal,
                        kodepengeluaran: kodepengeluaran,
                        namapengeluaran: namapengeluaran,
                        kodedepartement: kodedepartement,
                        namadepartement: namadepartement,
                        nopermohonan: nopermohonan,
                        keterangan: keterangan,
                        cabang: cabang,
                        datadetail: datadetail,
                    },
                    success: function(data) {
                        console.log(data);
                        if (data.nomor != "") {
                            Toast.fire({
                                icon: 'success',
                                title: data.message
                            });
                            $('#save').prop('disabled', true);

                            $('#nomor').val(data.nomor);
                            $('#tanggal').prop('disabled', true);
                            $('#kodepengeluaran').prop('disabled', true);
                            $('#namapengeluaran').prop('disabled', true);
                            $('#kodedepartement').prop('disabled', true);
                            $('#namadepartement').prop('disabled', true);
                            $('#nopermohonan').prop('disabled', true);
                            $('#nomoracc').prop('disabled', true);
                            $('#namaacc').prop('disabled', true);
                            $('#nilaipengeluaran').prop('disabled', true);
                            $('#keterangan').prop('disabled', true);

                            $('#jenispengeluaran').hide();
                            $('#caripermohonan').hide();
                            // clearScreen();
                        } else {
                            Toast.fire({
                                icon: 'error',
                                title: data.message
                            });
                        }
                    }
                }, false);
            }
        });

        document.getElementById("okcancel").addEventListener("click", function(event) {
            event.preventDefault();

            var nomor = $('#nomor').val();
            var kodepengeluaran = $('#kodepengeluaran').val();
            var keterangan = $('#keteranganbatal').val();
            var nopermohonan = $('#nopermohonan').val();
            var kodecabang = $('#cabang').val();
            var datadetail = AmbilDataTableDetail();

            if (keterangan == '') {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Keterangan batal harus diisi.',
                    showCloseButton: true,
                    width: 350,
                });
                return false;
            } else {
                $.ajax({
                    url: "<?php echo base_url('finance/Pengeluaranuang/Cancel'); ?>",
                    method: "POST",
                    dataType: "json",
                    async: true,
                    data: {
                        nomor: nomor,
                        kodepengeluaran: kodepengeluaran,
                        keterangan: keterangan,
                        nopermohonan: nopermohonan,
                        kodecabang: kodecabang,
                        datadetail: datadetail,
                    },
                    success: function(data) {
                        if (data.nomor != "") {
                            Toast.fire({
                                icon: 'success',
                                title: data.message
                            });
                            RefreshLayar();

                        } else {
                            Toast.fire({
                                icon: 'error',
                                title: data.message
                            });
                        }
                    }
                });
            }

        });
        /* Declare Toast */
        const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000,
            timerProgressBar: true,
            didOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
            }
        });


        document.getElementById("clear").addEventListener("click", function(event) {
            event.preventDefault();
            RefreshLayar();
        });

        function RefreshLayar() {
            location.reload(true);
        };

        clearScreen();
    });
</script>