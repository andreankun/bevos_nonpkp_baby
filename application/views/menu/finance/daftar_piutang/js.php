<script type="text/javascript">
    $(document).ready(function() {

		function FormatDate(input) {
			var date = new Date(input);
			var yyyy = date.getFullYear();
			var mm = date.getMonth() + 1;
			var dd = date.getDate();
			var hrs = date.getHours();
			var mnt = date.getMinutes();


			if (dd < 10) {
				dd = '0' + dd;
			}
			if (mm < 10) {
				mm = '0' + mm;
			}
			if (hrs < 10) {
				hrs = '0' + hrs;
			}
			if (mnt < 10) {
				mnt = '0' + mnt;
			}
			return yyyy + '-' + mm + '-' + dd;
		};

		function getDigitToFormat(val) {
			if (val < 10) {
				val = '0' + val;
			}
			return val.toString();
		};

		/* Format Rupiah */
		function FormatRupiah(angka, prefix) {
			if (!angka) {
				return '';
			}
			var vangka = angka.toString();
			var number_string = vangka.replace(/[^.\d]/g, '').replace(/[^\w\s]/gi, '').toString(),
				split = number_string.split('.'),
				sisa = split[0].length % 3,
				rupiah = split[0].substr(0, sisa),
				ribuan = split[0].substr(sisa).match(/\d{3}/gi);

			// tambahkan titik jika yang di input sudah menjadi angka ribuan
			if (ribuan) {
				separator = sisa ? ',' : '';
				rupiah += separator + ribuan.join(',');
			}

			rupiah = split[1] != undefined ? rupiah + '.' + split[1] : rupiah;
			return prefix == undefined ? rupiah : (rupiah ? rupiah : '');
		};


        document.getElementById("caripelanggan").addEventListener('click', function(event) {
			event.preventDefault();
			$("#tb_detail").empty();
			$("#totalpiutang").val(0);
			$('#t_pelanggan').DataTable({
				"language": {
					"url": "<?= base_url() ?>assets/json/id.json"
				},
				"destroy": true,
				"searching": true,
				"processing": true,
				"serverSide": true,
				"lengthChange": true,
				"pageLength": 5,
				"lengthMenu": [5, 10, 25, 50],
				"ordering": true,
				"order": [
					[2, 'asc']
				],
				"initComplete": function(settings, json) {
					$("#t_pelanggan input[type='search']").focus();
				},
				"ajax": {
					"url": "<?= base_url('finance/Piutang/CariDataCustomer'); ?>",
					"method": "POST",
					"data": {
						nmtb: "cari_customer",
						field: {
							nomor: "nomor",
							nama_toko: "nama_toko",
							nama: "nama",
						},
						sort: "nomor",
						where: {
							nomor: "nomor",
							nama: "nama",
							nama_toko: "nama_toko"
						},
						value: "aktif = true AND kodecabang = '" + $("#kodecabang").val() + "'"
					},
				}
			});
		});

         /*Get Data Customer*/
         $(document).on('click', ".searchpiutang", function() {
            var nomor = $(this).attr("data-id");;
            $.ajax({
                url: "<?= base_url('masterdata/Customer/DataCustomer'); ?>",
                method: "POST",
                dataType: "json",
                async: false,
                data: {
                    nomor: nomor
                },
                success: function(data) {
                    for (var i = 0; i < data.length; i++) {
                        $('#nopelanggan').val(data[i].nomor.trim());
                        $('#namapelanggan').val(data[i].nama.trim());
                    }
                   
                }
            }, false);
        });

		/*Get Data Customer*/
		$(document).on('click', ".searchpiutang", function() {
            var nopelanggan = $(this).attr("data-id");
            $.ajax({
                url: "<?= base_url('finance/Piutang/GetDataPiutang'); ?>",
                method: "POST",
                dataType: "json",
                async: false,
                data: {
                    nopelanggan: nopelanggan
                },
                success: function(data) {
                    for (var i = 0; i < data.length; i++) {
                        $('#totalpiutang').val(FormatRupiah(data[i].total.trim()));
                        // $('#namapelanggan').val(data[i].nama.trim());
                    }
					GetDataDetail(nopelanggan);
                   
                }
            }, false);
        });

		function GetDataDetail(nopelanggan) {
            $.ajax({
                url: "<?= base_url('finance/Piutang/GetDataDaftarPiutang'); ?>",
                method: "POST",
                dataType: "json",
                async: false,
                data: {
                    nopelanggan: nopelanggan
                },
                success: function(data) {
                    for (var i = 0; i < data.length; i++) {
                        // var kode = data[i].kode;
                        var nomorgb = data[i].nomorgb;
                        var noinvoice = data[i].nomor;
                        var tglinvoice = data[i].tanggal.trim();
                        var jthtempo = data[i].tgljthtempo.trim();
                        var jumlah = data[i].nilaipiutang.trim();
                        var bayar = data[i].terbayar.trim();
                        var sisa = data[i].sisa.trim();

                        InsertDataDetail(noinvoice, tglinvoice, jthtempo, jumlah, bayar, sisa, nomorgb);
                    }
                }
            });
        };

		function InsertDataDetail(noinvoice, tglinvoice, jthtempo, jumlah, bayar, sisa, nomorgb) {
            var row = "";
            row =
                '<tr id="' + noinvoice + '">' +
                '<td>' + '<button class="btn btn-sm btn-danger cetakpdf" data-id='+nomorgb+'><i class="far fa-download"></i></button>' + '</td>' +
                '<td>' + noinvoice + '</td>' +
                '<td>' + FormatDate(tglinvoice) + '</td>' +
                '<td>' + FormatDate(jthtempo) + '</td>' +
                '<td>' + FormatRupiah(jumlah) + '</td>' +
                '<td>' + FormatRupiah(bayar) + '</td>' +
                '<td>' + FormatRupiah(sisa) + '</td>' +
               
                '</tr>';
            $('#tb_detail').append(row);
        };

		$(document).on('click', ".cetakpdf", function() {
            var nomorgb = $(this).attr("data-id");
            CetakInvoice(nomorgb);
        });

		function CetakInvoice(nomorgb) {
            // console.log(nomor);
            window.open("<?= base_url() ?>cetak_pdf/Reprint_Invoice/Print/" + nomorgb);
        }


    });
</script>
