<div class="col-md-12 mb-2">
    <div class="modal-header" style="padding: 5px;">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-3">
                    <h2>
                        <span class="logo-menu"><?php echo $icons ?></span>
                        <span><?php echo $title ?></span>
                    </h2>
                </div>
                <!-- <div class="col-md-9">
                    <div class="form-group" style="float: right;">
                        <button id="preview" class="btn btn-sm btn-danger mb-1 mt-1 mr-1 ml-1"><i class="far fa-file-pdf" style="margin-right: 10px;"></i>Preview</button>
                        <button id="print" class="btn btn-sm btn-dark mb-1 mt-1 mr-1 ml-1"><i class="far fa-print" style="margin-right: 10px;"></i>Print</button>
                        <button id="export" class="btn btn-sm btn-success mb-1 mt-1 mr-1 ml-1"><i class="far fa-file-export" style="margin-right: 10px;"></i>Export</button>
                        <button id="clear" class="btn btn-sm btn-dark mb-1 mt-1 mr-1 ml-1"><i class="far fa-refresh" style="margin-right: 10px;"></i>Clear</button>
                    </div>
                </div> -->
            </div>
        </div>
    </div>
</div>
<div class="col-md-12 table-responsive hidescroll" style="height: 555px; overflow-y: scroll;">
    <div class="row">
        <div class="col-md-6">
            <div class="modal-content mb-2">
                <div class="modal-body">

                    <div class="row mb-2">
                        <div class="col-sm-6 input-group gudang">
                            <div class="col-sm-6 p-0 input-group-prepend">
                                <span class="col-sm-12 input-group-text">Kode Gudang</span>
                            </div>
                            <input class="form-control" type="text" name="kodegudang" id="nopelanggan" maxlength="2" placeholder="Kode Gudang" value="<?php echo $this->session->userdata('mygudang'); ?>" required readonly />
                            <input class="form-control" type="hidden" name="kodecabang" id="kodecabang" maxlength="2" placeholder="Kode Gudang" value="<?php echo $this->session->userData('mycabang') ?>" required readonly />
                            <!-- <input class="form-control" type="hidden" name="kodecabang" id="gudang" maxlength="2" placeholder="Kode Gudang" value="" required readonly /> -->
                            <div class="input-group-text btn-dark" data-toggle="modal" data-target="#findpelanggan" id="caripelanggan">
                                <span class="input-group-addon">
                                    <i class="fa fa-search text-white"></i>
                                </span>
                            </div>
                        </div>

                        <div class="col-sm-6 input-group gudang">
                            <div class="col-sm-6 p-0 input-group-prepend">
                                <span class="col-sm-12 input-group-text">Kode Gudang</span>
                            </div>
                            <input class="form-control" type="text" name="kodegudang" id="namapelanggan" maxlength="2" placeholder="Kode Gudang" required readonly />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-12 col-lg-12 col-xl-12">
            <div class="modal-content mb-2">
                <div class="modal-header p-2">
                    <h5 class="mb-0">DATA PIUTANG CUSTOMER</h5>
                </div>
                <div class="modal-body">
                    <div class="row my-3">
                        <div class="table-responsive">
                            <table id="t_detail" class="table table-bordered table-striped dt-responsive display nowrap" style="width:100%; text-align: center;">
                                <thead style="background-color: #68478D; color: #eee;">
                                    <tr>
                                        <th width="100">Download</th>
                                        <th width="100">No.Invoice</th>
                                        <th width="30">Tgl. Invoice</th>
                                        <th width="30">Jatuh Tempo</th>
                                        <th width="30">Jumlah</th>
                                        <th width="50">Bayar</th>
                                        <th width="50">Sisa</th>
                                    </tr>
                                </thead>
                                <tbody id="tb_detail"></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="row mb-2">
                <div class="col-sm-6 input-group gudang">
                    <input class="form-control right" type="text" name="kodegudang" id="totalpiutang" maxlength="2" placeholder="Kode Gudang" required readonly />
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="findpelanggan">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" style="border-radius: 5px;">
            <div class="modal-header" style="padding: 10px; color: #000;">
                <h7 class="modal-title" style="margin-left: 5px;">Data Pelanggan</h7>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table id="t_pelanggan" class="table table-bordered table-striped dt-responsive display nowrap" style="width:100%">
                        <thead class="thead-dark">
                            <tr>
                                <th width="10">Action</th>
                                <th width="150">Kode Pelanggan</th>
                                <th width="150">Nama Pelanggan</th>
                                <th width="150">Nama Toko</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
