<script type="text/javascript">
    $(document).ready(function() {

        function clearScreen() {
            $('#save').prop('disabled', false);
            $('#cancel').prop('disabled', true);
            // ClearTransaksi();
            $('#nomor').val("");
            $('#tanggal').val($.datepicker.formatDate('yy-mm-dd', new Date()));
            $('#kodepengeluaran').val("");
            $('#namapengeluaran').val("");
            $('#kodedepartement').val("");
            $('#namadepartement').val("");
            $('#noinvoice').val("");
            $('#noreferensi').val("");
            $('#namareferensi').val("");
            $('#keterangan').val("").prop('disabled', false);
            $('#nomoracc').val("");
            $('#namaacc').val("");
            $('#nilaipengeluaran').val(0).prop('disabled', false);
            $('#memo').val("").prop('disabled', false);

            $('#tb_detail').empty();

            $('#jenispengeluaran').show();
            $('#cariinvoice').show();
            $('#cariaccount').show();
            $('#adddetail').show();
            $('.deldetail').show();
            $('.editdetail').show();

            $('#grupdepartement').hide();
            // $('#grupalokasi').hide();

            UnableAction();
        };

        function FormatRupiah(angka, prefix) {
            if (!angka) {
                return '';
            }
            var vangka = angka.toString();
            var number_string = vangka.replace(/[^.\d]/g, '').replace(/[^\w\s]/gi, '').toString(),
                split = number_string.split('.'),
                sisa = split[0].length % 3,
                rupiah = split[0].substr(0, sisa),
                ribuan = split[0].substr(sisa).match(/\d{3}/gi);

            // tambahkan titik jika yang di input sudah menjadi angka ribuan
            if (ribuan) {
                separator = sisa ? ',' : '';
                rupiah += separator + ribuan.join(',');
            }

            rupiah = split[1] != undefined ? rupiah + '.' + split[1] : rupiah;
            return prefix == undefined ? rupiah : (rupiah ? rupiah : '');
        };

        function DeFormatRupiah(angka) {
            var result = angka.replace(/[^\w\s]/gi, '');

            return result;
        };

        function UnableAction() {
            $('#keterangan').prop("disabled", false);
            $('#memo').prop("disabled", false);
            $('#nilaipengeluaran').prop("disabled", false);

            $('#jenispengeluaran').show();
            $('#cariinvoice').show();
            $('#cariaccount').show();
            $('#adddetail').show();
        };

        function DisableAction() {
            document.getElementById('cancel').disabled = false;
            $('#keterangan').prop("disabled", true);
            $('#memo').prop("disabled", true);
            $('#nilaipengeluaran').prop("disabled", true);

            $('#jenispengeluaran').hide();
            $('#cariinvoice').hide();
            $('#cariaccount').hide();
            $('#adddetail').hide();
        };

        function AddView() {
            var table = document.getElementById('t_detail');
            if (table.rows.length > 1) {
                $('#jenispengeluaran').hide();
                $('#cariaccount').hide();
                $('#caridepartement').hide();
            } else {
                $('#jenispengeluaran').show();
                $('#cariaccount').show();
                $('#caridepartement').show();


            }
        }


        function AmbilDataTableDetail() {
            var table = document.getElementById('t_detail');
            var arr2 = [];
            for (var r = 1, n = table.rows.length; r < n; r++) {
                var string = "";
                for (var c = 0, m = table.rows[r].cells.length; c < m - 1; c++) {
                    if (c == 0) {
                        string = "{" + table.rows[0].cells[c].innerHTML + " : '" + table.rows[r].cells[c].innerHTML + "'";
                    } else {
                        string = string + ", " + table.rows[0].cells[c].innerHTML + " : '" + table.rows[r].cells[c].innerHTML + "'";
                    }
                }
                string = string + "}";
                var obj = JSON.stringify(eval('(' + string + ')'));
                var arr = $.parseJSON(obj);
                arr2.push(arr);
            }
            return arr2;
        };

        document.getElementById("jenispengeluaran").addEventListener("click", function(event) {
            event.preventDefault();
            $('#t_jenispengeluaran').DataTable({
                "destroy": true,
                "searching": true,
                "processing": true,
                "serverSide": true,
                "lengthChange": true,
                "order": [],
                "ajax": {
                    "url": "<?php echo base_url('finance/Permohonanuang/CariJenisPengeluaran'); ?>",
                    "method": "POST",
                    "data": {
                        nmtb: "stpm_otorisasikasir",
                        field: {
                            kode: "kode",
                            nama: "nama",
                            jenis: "jenis",
                        },
                        sort: "kode",
                        where: {
                            kode: "kode"
                        },
                        value: "aktif = true AND jenis = '1'"
                    },
                }
            });
        });

        $(document).on('click', ".searchjenis", function() {
            var kode = $(this).attr("data-id");
            $.ajax({
                url: "<?php echo base_url('finance/Permohonanuang/DataJenisPengeluaran'); ?>",
                method: "POST",
                dataType: "json",
                async: false,
                data: {
                    kode: kode,
                },
                success: function(data) {
                    for (var i = 0; i < data.length; i++) {
                        $('#kodepengeluaran').val(data[i].kode.trim());
                        $('#namapengeluaran').val(data[i].nama.trim());

                        if (data[i].kode.trim() == 'KUF99') {
                            $('#grupdepartement').show();
                        } else {
                            $('#grupdepartement').hide();
                        }
                    }

                }
            });
        });

        document.getElementById("caridepartement").addEventListener("click", function(event) {
            event.preventDefault();
            $('#t_departemen').DataTable({
                "destroy": true,
                "searching": true,
                "processing": true,
                "serverSide": true,
                "lengthChange": true,
                "order": [],
                "ajax": {
                    "url": "<?php echo base_url('finance/Permohonanuang/CariDepartement'); ?>",
                    "method": "POST",
                    "data": {
                        nmtb: "glbm_departement",
                        field: {
                            kode: "kode",
                            nama: "nama"
                        },
                        sort: "kode",
                        where: {
                            kode: "kode"
                        },
                        value: "aktif = true"
                    },
                }
            });
        });

        $(document).on('click', ".searchdepartement", function() {
            var kode = $(this).attr("data-id");
            $.ajax({
                url: "<?php echo base_url('finance/Permohonanuang/DataDepartement'); ?>",
                method: "POST",
                dataType: "json",
                async: false,
                data: {
                    kode: kode,
                },
                success: function(data) {
                    for (var i = 0; i < data.length; i++) {
                        $('#kodedepartement').val(data[i].kode.trim());
                        $('#namadepartement').val(data[i].nama.trim());
                    }
                }
            });
        });


        function validasiPencarian() {
            if ($('#kodepengeluaran').val() == '') {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Jenis Pengeluaran tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#kodepengeluaran').focus();
                var result = false;
            } else if ($('#kodepengeluaran').val() != 'KUF01' && $('#kodedepartement').val() == '') {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Silakan pilih department terlebih dahulu.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#kodedepartement').focus();
                var result = false;
            } else {
                var result = true;
            }
            return result;
        };

        document.getElementById("cariinvoice").addEventListener("click", function(event) {
            event.preventDefault();

            $('#cariinvoice').attr('data-toggle', '');
            if (validasiPencarian() == true) {
                $('#cariinvoice').attr('data-toggle', 'modal');
                if ($('#kodepengeluaran').val() == 'KUF01') {
                    $('#cariinvoice').attr('data-target', '#findDokument');
                    CariDokumenHutang();
                } else {
                    $('#cariinvoice').attr('data-target', '#findaccountlain');
                    CariDokumenLainLain();
                }
            }

        });

        function CariDokumenHutang() {
            $('#t_hutang').DataTable({
                "destroy": true,
                "searching": true,
                "processing": true,
                "serverSide": true,
                "lengthChange": true,
                "order": [],
                "ajax": {
                    "url": "<?php echo base_url('finance/Permohonanuang/CariDataHutang'); ?>",
                    "method": "POST",
                    "data": {
                        nmtb: "srvt_hutang",
                        field: {
                            nomor: "nomor",
                            nomorsupplier: "nomorsupplier",
                            nilaihutang: "nilaihutang",
                            nilaipermohonan: "nilaipermohonan",
                            nilaipembayaran: "nilaipembayaran",
                        },
                        sort: "nomor",
                        where: {
                            nomor: "nomor"
                        },
                        value: "jenistransaksi = 'KUF01' AND nilaihutang > (nilaipermohonan + nilaipembayaran)"
                    },
                }
            });
        };

        $(document).on('click', ".searchhutang", function() {
            var nomor = $(this).attr("data-id");
            $.ajax({
                url: "<?php echo base_url('finance/Permohonanuang/DataHutang'); ?>",
                method: "POST",
                dataType: "json",
                async: false,
                data: {
                    nomor: nomor,
                },
                success: function(data) {
                    for (var i = 0; i < data.length; i++) {
                        $('#noinvoice').val(data[i].nomor.trim());
                        $('#noreferensi').val(data[i].nomorsupplier.trim());
                        $('#namareferensi').val(data[i].namasupplier.trim());
                        $('#nilaihutang').val(FormatRupiah(data[i].nilaihutang.trim()));
                        $('#nilaipengeluaran').val(FormatRupiah(data[i].nilaihutang.trim()));
                    }
                }
            });
        });

        function CariDokumenLainLain() {
            $('#t_accountlain').DataTable({
                "destroy": true,
                "searching": true,
                "processing": true,
                "serverSide": true,
                "lengthChange": true,
                "order": [],
                "ajax": {
                    "url": "<?php echo base_url('finance/Permohonanuang/CariDokumenLainLain'); ?>",
                    "method": "POST",
                    "data": {
                        nmtb: "glbm_accountlainlain",
                        field: {
                            nomor: "nomor",
                            nama: "nama",
                        },
                        sort: "nomor",
                        where: {
                            nomor: "nomor"
                        }
                    },
                }
            });
        }

        $(document).on('click', ".searchdoclain", function() {
            var nomor = $(this).attr("data-id");
            $.ajax({
                url: "<?php echo base_url('finance/Permohonanuang/DataAccountLain'); ?>",
                method: "POST",
                dataType: "json",
                async: false,
                data: {
                    nomor: nomor,
                },
                success: function(data) {
                    for (var i = 0; i < data.length; i++) {
                        $('#noinvoice').val(data[i].nomor.trim());
                        $('#noreferensi').val(data[i].nomor.trim());
                        $('#namareferensi').val(data[i].nama.trim());
                    }
                }
            });
        });

        document.getElementById("cariaccount").addEventListener("click", function(event) {
            event.preventDefault();
            $('#t_account').DataTable({
                "destroy": true,
                "searching": true,
                "processing": true,
                "serverSide": true,
                "lengthChange": true,
                "order": [],
                "ajax": {
                    "url": "<?php echo base_url('finance/Permohonanuang/CariAccount'); ?>",
                    "method": "POST",
                    "data": {
                        nmtb: "glbm_account",
                        field: {
                            nomor: "nomor",
                            nama: "nama",
                            norekening: "norekening",
                        },
                        sort: "nomor",
                        where: {
                            nomor: "nomor",
                            nama: "nama",
                            norekening: "norekening",
                        },
                        // value: "aktif = true AND kode_cabang = '" + $('#cabang').val() + "'"
                    },
                }
            });
        });

        $(document).on('click', ".searchaccount", function() {
            var nomor = $(this).attr("data-id");
            $.ajax({
                url: "<?php echo base_url('finance/Permohonanuang/DataAccount'); ?>",
                method: "POST",
                dataType: "json",
                async: false,
                data: {
                    nomor: nomor,
                },
                success: function(data) {
                    for (var i = 0; i < data.length; i++) {
                        $('#nomoracc').val(data[i].nomor.trim());
                        $('#namaacc').val(data[i].nama.trim());
                    }
                }
            });
        });

        function validasiAddDetail(kode) {
            var intRegex = /^\d+$/;

            var table = document.getElementById('t_detail');
            if ($('#kodepengeluaran').val() == '') {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Silakan pilih jenis pengeluaran terlebih dahulu.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#kodepengeluaran').focus();
                return 'false';
            } else if ($('#noinvoice').val() == '') {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Document Invoice tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#noinvoice').focus();
                return 'false';
            } else if ($('#nomoracc').val() == '') {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Account Kasir tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#nomoracc').focus();
                return 'false';
            } else if ($('#nilaipengeluaran').val() == '' || $('#nilaipengeluaran').val() == 0) {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Nilai Pengeluaran tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#nilaipengeluaran').focus();
                return 'false';
            } else if (!intRegex.test(DeFormatRupiah($('#nilaipengeluaran').val()))) {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Nilai Pengeluaran tidak boleh mengandung huruf.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#nilaipengeluaran').focus();
                return 'false';
            } else if ($('#kodepengeluaran').val() == 'KUF01' && Number(DeFormatRupiah($('#nilaipengeluaran').val())) > Number(DeFormatRupiah($('#nilaihutang').val()))) {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Nilai Pengeluaran tidak boleh lebih besar dari Nilai Hutang.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#nilaipengeluaran').focus();
                return 'false';
            } else {
                for (var r = 1, n = table.rows.length; r < n; r++) {
                    var string = "";
                    for (var c = 0, m = table.rows[r].cells.length; c < m; c++) {
                        if (c == 0) {
                            if (table.rows[r].cells[c].innerHTML == kode) {
                                Swal.fire({
                                    title: 'Informasi',
                                    icon: 'info',
                                    html: 'Data ini sudah diinput.',
                                    showCloseButton: true,
                                    width: 350,
                                });
                                return 'false';
                            }
                        }
                    }
                }
            }
            return 'true';
        };

        document.getElementById("adddetail").addEventListener("click", function(event) {
            event.preventDefault();
            var kodepengeluaran = $('#kodepengeluaran').val();
            var namapengeluaran = $('#namapengeluaran').val();
            var noinvoice = $('#noinvoice').val();
            var noreferensi = $('#noreferensi').val();
            var namareferensi = $('#namareferensi').val();
            var nomoracc = $('#nomoracc').val();
            var namaacc = $('#namaacc').val();
            var nilaipengeluaran = $('#nilaipengeluaran').val();
            var noalokasi = '';
            var nilaialokasi = 0;
            var keterangan = $('#keterangan').val();
            var memo = $('#memo').val();

            if (validasiAddDetail(noinvoice) == 'true') {
                var row = "";
                row =
                    '<tr id="' + noinvoice.toString().replace(".", "").replace(".", "").replace(".", "").replace(",", "").replace(",", "").replace(",", "") + '">' +
                    '<td>' + noinvoice + '</td>' +
                    '<td>' + kodepengeluaran + '</td>' +
                    '<td>' + namapengeluaran + '</td>' +
                    '<td>' + noreferensi + '</td>' +
                    '<td>' + namareferensi + '</td>' +
                    '<td>' + nomoracc + '</td>' +
                    '<td>' + namaacc + '</td>' +
                    '<td>' + FormatRupiah(nilaipengeluaran) + '</td>' +
                    '<td>' + noalokasi + '</td>' +
                    '<td>0</td>' +
                    '<td>' + memo + '</td>' +
                    '<td style="text-align: center;">' +
                    '<button data-table="' + noinvoice.toString().replace(".", "").replace(".", "").replace(".", "").replace(",", "").replace(",", "").replace(",", "") + '" data-toggle="modal" data-target="#changedetail" class="editdetail btn btn-success m-1"><i class="fas fa-edit mr-1"></i> Edit</button>' +
                    '<button data-table="' + noinvoice.toString().replace(".", "").replace(".", "").replace(".", "").replace(",", "").replace(",", "").replace(",", "") + '" class="deldetail btn btn-danger m-1"><i class="fa fa-trash mr-1"></i> Delete</button>' +
                    '</td>' +
                    '</tr>';
                $('#tb_detail').append(row);
                AddView();
                // HitungSummary();

                $('#noinvoice').val('');
                $('#noreferensi').val('');
                $('#nilaihutang').val(0);
                $('#namareferensi').val('');
                $('#nilaipengeluaran').val(0);
                $('#memo').val('');
                // ClearTransaksi();
            }
        });

        $(document).on('click', '.deldetail', function() {
            var id = $(this).attr("data-table");
            // $('#th' + id.replace(".", "").replace(".", "").replace(".", "").replace(",", "").replace(",", "").replace(",", "")).remove();
            $('#' + id.toString()).remove();
            AddView();
        });

        $(document).on('click', '.editdetail', function() {
            _row = $(this);
            $('#chgdocinvoice').val(_row.closest("tr").find("td:eq(0)").text());
            $('#chgjenis').val(_row.closest("tr").find("td:eq(1)").text());
            $('#chgpengeluaran').val(_row.closest("tr").find("td:eq(2)").text());
            $('#chgnoreferensi').val(_row.closest("tr").find("td:eq(3)").text());
            $('#chgreferensi').val(_row.closest("tr").find("td:eq(4)").text());
            $('#chgaccount').val(_row.closest("tr").find("td:eq(5)").text());
            $('#chgnamaaccount').val(_row.closest("tr").find("td:eq(6)").text());
            $('#chgnilaipengeluaran').val(_row.closest("tr").find("td:eq(7)").text());
            $('#chgaccountalokasi').val(_row.closest("tr").find("td:eq(8)").text());
            $('#chgnilaialokasi').val(_row.closest("tr").find("td:eq(9)").text());
            $('#chgmemo').val(_row.closest("tr").find("td:eq(10)").text());
            if (_row.closest("tr").find("td:eq(1)").text() == "KUF99") {
                $('#chgnilaialokasi').prop("disabled", true);
            } else {
                $('#chgnilaialokasi').prop("disabled", false);
            }

        });

        $(document).on('click', '#changedetail', function() {

            var kodepengeluaran = $('#chgjenis').val();
            var namapengeluaran = $('#chgpengeluaran').val();
            var noinvoice = $('#chgdocinvoice').val();
            var noreferensi = $('#chgnoreferensi').val();
            var namareferensi = $('#chgreferensi').val();
            var nomoracc = $('#chgaccount').val();
            var namaacc = $('#chgnamaaccount').val();
            var nilaipengeluaran = $('#chgnilaipengeluaran').val();
            var noalokasi = $('#chgaccountalokasi').val();
            var namaalokasi = $('#chgnamaalokasi').val();
            var nilaialokasi = $('#chgnilaialokasi').val();
            var memo = $('#chgmemo').val();

            $('#' + noinvoice.toString().replace(".", "").replace(".", "").replace(".", "").replace(",", "").replace(",", "").replace(",", "")).remove();
            InsertDetailPenerimaan(noinvoice, kodepengeluaran, namapengeluaran, noreferensi, namareferensi, nomoracc, namaacc, nilaipengeluaran, noalokasi, nilaialokasi, memo);
        });

        $('#nilaipengeluaran').keyup(function() {
            var harga = FormatRupiah(this.value, '');
            $('#nilaipengeluaran').val(harga);
        });

        $('#chgnilaipengeluaran').keyup(function() {
            var harga = FormatRupiah(this.value, '');
            $('#chgnilaipengeluaran').val(harga);
        });

        function InsertDetailPenerimaan(noinvoice, kodepengeluaran, namapengeluaran, noreferensi, namareferensi, nomoracc, namaacc, nilaipengeluaran, noalokasi, nilaialokasi, memo) {

            var row = "";
            row =
                '<tr id="' + noinvoice.toString().replace(".", "").replace(".", "").replace(".", "").replace(",", "").replace(",", "").replace(",", "") + '">' +
                '<td>' + noinvoice + '</td>' +
                '<td>' + kodepengeluaran + '</td>' +
                '<td>' + namapengeluaran + '</td>' +
                '<td>' + noreferensi + '</td>' +
                '<td>' + namareferensi + '</td>' +
                '<td>' + nomoracc + '</td>' +
                '<td>' + namaacc + '</td>' +
                '<td>' + FormatRupiah(nilaipengeluaran) + '</td>' +
                '<td>' + noalokasi + '</td>' +
                '<td>' + FormatRupiah(nilaialokasi) + '</td>' +
                '<td>' + memo + '</td>' +
                '<td style="text-align: center;">' +
                '<button data-table="' + noinvoice.toString().replace(".", "").replace(".", "").replace(".", "").replace(",", "").replace(",", "").replace(",", "") + '" data-toggle="modal" data-target="#changedetail" class="editdetail btn btn-success m-1"><i class="fas fa-edit mr-1"></i> Edit</button>' +
                '<button data-table="' + noinvoice.toString().replace(".", "").replace(".", "").replace(".", "").replace(",", "").replace(",", "").replace(",", "") + '" class="deldetail btn btn-danger m-1"><i class="fa fa-trash mr-1"></i> Delete</button>' +
                '</td>' +
                '</tr>';
            $('#tb_detail').append(row);
            // HitungSummary();
        };



        document.getElementById("find").addEventListener("click", function(event) {
            $('#keteranganbatal').val("");
            event.preventDefault();
            $('#tb_detail').empty();
            $('#t_permohonanuang').DataTable({
                "destroy": true,
                "searching": true,
                "processing": true,
                "serverSide": true,
                "lengthChange": true,
                "order": [],
                "ajax": {
                    "url": "<?php echo base_url('finance/Permohonanuang/CariPermohonanUang'); ?>",
                    "method": "POST",
                    "data": {
                        nmtb: "trnt_permohonanuang",
                        field: {
                            nomor: "nomor",
                            tanggal: "tanggal",
                            jenispengeluaran: "jenispengeluaran",
                            kodedepartement: "kodedepartement",
                            namadepartement: "namadepartement",
                        },
                        sort: "nomor",
                        where: {
                            nomor: "nomor"
                        },
                        value: "batal = false AND statuspengeluaran = false"
                    },
                },
                "columnDefs": [{
                    "targets": 2,
                    "render": $.fn.dataTable.render.moment('YYYY-MM-DD HH:mm:ss', 'D-MMM-YY')
                }, ]
            });
        });

        $(document).on('click', ".searchpermohonan", function() {
            var nomor = $(this).attr("data-id");;
            GetDataPermohonanUang(nomor);
            DisableAction();
        });

        function GetDataPermohonanUang(nomor) {
            $.ajax({
                url: "<?php echo base_url('finance/Permohonanuang/DataPermohonanUang'); ?>",
                method: "POST",
                dataType: "json",
                async: false,
                data: {
                    nomor: nomor
                },
                success: function(data) {
                    for (var i = 0; i < data.length; i++) {
                        $('#nomor').val(data[i].nomor.trim());
                        $('#tanggal').val($.datepicker.formatDate('yy-mm-dd', new Date(data[i].tanggal.trim())));
                        $('#kodedepartement').val(data[i].kodedepartement.trim());
                        $('#namadepartement').val(data[i].namadepartement.trim());
                        $('#kodepengeluaran').val(data[i].jenispengeluaran.trim());
                        $('#namapengeluaran').val(data[i].namapengeluaran.trim());
                        $('#keterangan').val(data[i].keterangan.trim());
                        GetDataPermohonanUangDetail(nomor, data[i].jenispengeluaran.trim(), data[i].namapengeluaran.trim());
                    }
                    document.getElementById('save').disabled = true;
                }
            });
        };

        function GetDataPermohonanUangDetail(nomor, kodepengeluaran, namapengeluaran) {
            $.ajax({
                url: "<?php echo base_url('finance/Permohonanuang/DataPermohonanUangDetail'); ?>",
                method: "POST",
                dataType: "json",
                async: false,
                data: {
                    nomor: nomor
                },
                success: function(data) {
                    for (var i = 0; i < data.length; i++) {
                        var noinvoice = data[i].noreferensi.trim();
                        var noreferensi = data[i].kodesupplier.trim();
                        var namareferensi = data[i].namasupplier.trim();
                        var nomoracc = data[i].kodeaccount.trim();
                        var namaacc = data[i].namaaccount.trim();
                        var nilaipengeluaran = FormatRupiah(data[i].nilaipermohonan.trim());
                        var accountalokasi = data[i].accountalokasi.trim();
                        var nilaialokasi = FormatRupiah(data[i].nilaialokasi.trim());
                        var memo = data[i].memo.trim();

                        InsertDetailPermohonanUang(noinvoice, kodepengeluaran, namapengeluaran, noreferensi, namareferensi, nomoracc, namaacc, nilaipengeluaran, accountalokasi, nilaialokasi, memo);
                    }
                }
            });
        };

        function InsertDetailPermohonanUang(noinvoice, kodepengeluaran, namapengeluaran, noreferensi, namareferensi, nomoracc, namaacc, nilaipengeluaran, accountalokasi, nilaialokasi, memo) {
            var row = "";
            row =
                '<tr id="' + noinvoice + '">' +
                '<td>' + noinvoice + '</td>' +
                '<td>' + kodepengeluaran + '</td>' +
                '<td>' + namapengeluaran + '</td>' +
                '<td>' + noreferensi + '</td>' +
                '<td>' + namareferensi + '</td>' +
                '<td>' + nomoracc + '</td>' +
                '<td>' + namaacc + '</td>' +
                '<td>' + nilaipengeluaran + '</td>' +
                '<td>' + accountalokasi + '</td>' +
                '<td>' + nilaialokasi + '</td>' +
                '<td>' + memo + '</td>' +
                '<td></td>' +
                '</tr>';
            $('#tb_detail').append(row);
        };







        function validasiSave() {
            var datadetail = AmbilDataTableDetail();
            if ($('#kodepengeluaran').val() == '') {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Kode Pengeluaran tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#kodepengeluaran').focus();
                var result = false;
            } else if ($('#keterangan').val() == '') {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Keterangan tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#keterangan').focus();
                var result = false;
            } else if ($('#kodepengeluaran').val() == 'KUF99' && $('#kodedepartement').val() == '') {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Kode Department boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                var result = false;
            } else if (datadetail.length == 0) {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Data Detail tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                var result = false;
            } else {
                var result = true;
            }
            return result;
        };

        document.getElementById("save").addEventListener("click", function(event) {
            event.preventDefault();

            var tanggal = $('#tanggal').val();
            var kodepengeluaran = $('#kodepengeluaran').val();
            var namapengeluaran = $('#namapengeluaran').val();
            var kodedepartement = $('#kodedepartement').val();
            var namadepartement = $('#namadepartement').val();
            var keterangan = $('#keterangan').val();
            var kodecabang = $('#cabang').val();
            var datadetail = AmbilDataTableDetail();

            if (validasiSave() == true) {
                $.ajax({
                    url: "<?php echo base_url('finance/Permohonanuang/Save'); ?>",
                    method: "POST",
                    dataType: "json",
                    async: true,
                    data: {
                        tanggal: tanggal,
                        kodedepartement: kodedepartement,
                        namadepartement: namadepartement,
                        keterangan: keterangan,
                        kodepengeluaran: kodepengeluaran,
                        namapengeluaran: namapengeluaran,
                        kodecabang: kodecabang,
                        datadetail: datadetail,
                    },
                    success: function(data) {
                        if (data.nomor != "") {
                            Toast.fire({
                                icon: 'success',
                                title: data.message
                            });
                            $('#save').prop('disabled', true);
                            $('#cancel').prop('disabled', true);
                            // ClearTransaksi();
                            $('#nomor').val(data.nomor);
                            $('#tanggal').prop('disabled', true);
                            $('#kodepengeluaran').prop('disabled', true);
                            $('#namapengeluaran').prop('disabled', true);
                            $('#kodedepartement').prop('disabled', true);
                            $('#namadepartement').prop('disabled', true);
                            $('#noinvoice').prop('disabled', true);
                            $('#noreferensi').prop('disabled', true);
                            $('#namareferensi').prop('disabled', true);
                            $('#keterangan').prop('disabled', true);
                            $('#nomoracc').prop('disabled', true);
                            $('#namaacc').prop('disabled', true);
                            $('#nilaipengeluaran').prop('disabled', true);
                            $('#memo').prop('disabled', true);

                            $('#jenispengeluaran').hide();
                            $('#cariinvoice').hide();
                            $('#cariaccount').hide();
                            $('#adddetail').hide();
                            $('.deldetail').hide();
                            $('.editdetail').hide();
                            // clearScreen();
                        } else {
                            Toast.fire({
                                icon: 'error',
                                title: data.message
                            });
                        }
                    }
                }, false);
            }
        });

        document.getElementById("okcancel").addEventListener("click", function(event) {
            event.preventDefault();


            var nomor = $('#nomor').val();
            var kodepengeluaran = $('#kodepengeluaran').val();
            var keterangan = $('#keteranganbatal').val();
            var datadetail = AmbilDataTableDetail();

            if (keterangan == '') {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Keterangan batal harus diisi.',
                    showCloseButton: true,
                    width: 350,
                });
                return false;
            } else {
                $.ajax({
                    url: "<?php echo base_url('finance/permohonanuang/Cancel'); ?>",
                    method: "POST",
                    dataType: "json",
                    async: true,
                    data: {
                        nomor: nomor,
                        kodepengeluaran: kodepengeluaran,
                        keterangan: keterangan,
                        datadetail: datadetail,
                    },
                    success: function(data) {
                        if (data.nomor != "") {
                            Toast.fire({
                                icon: 'success',
                                title: data.message
                            });
                            RefreshLayar();

                        } else {
                            Toast.fire({
                                icon: 'error',
                                title: data.message
                            });
                        }
                    }
                });
            }

        });

        document.getElementById("clear").addEventListener("click", function(event) {
            event.preventDefault();
            clearScreen();
        });

        const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000,
            timerProgressBar: true,
            didOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
            }
        });
        clearScreen();

        function RefreshLayar() {
            location.reload(true);
            clearScreen();
        };
    });
</script>