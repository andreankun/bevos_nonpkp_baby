<div class="col-12 col-xl-12 mb-2">
    <div class="modal-header" style="padding: 5px;">
        <div class="col-12 col-md-12">
            <div class="row">
                <div class="col-5 col-md-5">
                    <h2>
                        <span class="logo-menu"><?php echo $icons ?></span>
                        <span class="text-uppercase"><?php echo $title ?></span>
                    </h2>
                </div>
                <div class="col-7 col-md-7">
                    <div class="form-group" style="float: right;">
                        <button id="save" class="btn btn-sm btn-dark m-1" style="margin-bottom: 0px;"><i class="far fa-save" style="margin-right: 10px;"></i>Save</button>
                        <button id="update" class="btn btn-sm btn-dark mb-1 mt-1 mr-1 ml-1" style="margin-bottom: 0px;"><i class="far fa-money-check-edit" style="margin-right: 10px;"></i>Update</button>
                        <button id="find" data-toggle="modal" data-target="#findPenerimaanUang" class="btn btn-sm btn-dark m-1" style="margin-bottom: 0px;"><i class="far fa-file-search" style="margin-right: 10px;"></i>Find</button>
                        <button id="cancel" class="btn btn-sm btn-danger m-1" data-toggle="modal" data-target="#modalcancel"><i class="far fa-times-circle" style="margin-right: 10px;"></i>Cancel</button>
                        <button id="clear" class="btn btn-sm btn-dark m-1" style="margin-bottom: 0px;"><i class="far fa-refresh" style="margin-right: 10px;"></i>Clear</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-md-12 table-responsive hidescroll" style="height: 555px; overflow-y: scroll;">
    <div class="row">
        <div class="col-12 col-md-12 col-lg-12 col-xl-6">
            <div class="modal-content mb-2">
                <div class="modal-body">
                    <div class="row mb-2 mt-2">
                        <div class="col-sm-6 input-group">
                            <div class="col-sm-6 p-0 input-group-prepend">
                                <span class="col-sm-12 input-group-text">Nomor</span>
                            </div>
                            <input class="form-control" type="text" name="nomor" id="nomor" maxlength="50" placeholder="Nomor" readonly required />
                            <input class="form-control" type="hidden" name="cabang" id="cabang" maxlength="50" value="<?php echo $this->session->userdata('mycabang') ?>" readonly required />
                        </div>
                        <div class="col-sm-6 input-group date" id="tgl">
                            <div class="col-sm-6 p-0 input-group-prepend">
                                <span class="col-sm-12 input-group-text">Tanggal</span>
                            </div>
                            <input class="form-control" id="tanggal" value="<?php echo date('Y-m-d'); ?>" padding="5px" type="text" maxlength="50" required />
                            <div class="input-group-text btn-dark">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar text-white"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-2">
                        <div class="col-sm-12 input-group">
                            <div class="col-sm-3 p-0 input-group-prepend">
                                <span class="col-sm-12 input-group-text">Jenis Penerimaan</span>
                            </div>
                            <input class="col-sm-3 form-control" type="text" name="kodepenerimaan" id="kodepenerimaan" maxlength="50" placeholder="Kode Penerimaan" readonly required />
                            <input class="form-control" type="text" name="namapenerimaan" id="namapenerimaan" maxlength="50" placeholder="Nama Penerimaan" readonly required />
                            <div class="input-group-text btn-dark" data-toggle="modal" data-target="#findjenispenerimaan" id="carijenispenerimaan">
                                <span class="input-group-addon">
                                    <i class="fa fa-search text-white"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-2" id="grupdepartement">
                        <div class="col-sm-12 input-group">
                            <div class="col-sm-3 p-0 input-group-prepend">
                                <span class="col-sm-12 input-group-text">Departemen</span>
                            </div>
                            <input class="col-sm-3 form-control" type="text" name="kodedepartement" id="kodedepartement" maxlength="50" placeholder="Kode" readonly required />
                            <input class="form-control" type="text" name="namadepartement" id="namadepartement" maxlength="50" placeholder="Nama" readonly required />
                            <div class="input-group-text btn-dark" data-toggle="modal" data-target="#finddepartement" id="caridepartement">
                                <span class="input-group-addon">
                                    <i class="fa fa-search text-white"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-2">
                        <div class="col-sm-12 input-group">
                            <div class="col-sm-3 p-0 input-group-prepend">
                                <span class="col-sm-12 input-group-text judulpencarian">Document Invoice</span>
                            </div>
                            <input class="form-control" type="text" name="nomordocument" id="nomordocument" maxlength="50" placeholder="No Document" readonly required />
                            <div class="input-group-text btn-dark" id="caridokumen">
                                <span class="input-group-addon">
                                    <i class="fa fa-search text-white"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-2">
                        <div class="col-sm-12 input-group">
                            <div class="col-sm-3 p-0 input-group-prepend">
                                <span class="col-sm-12 input-group-text">Referensi</span>
                            </div>
                            <input class="col-sm-3 form-control" type="text" name="noreferensi" id="noreferensi" maxlength="50" placeholder="Nomor Customer" readonly required />
                            <input class="form-control" type="text" name="namareferensi" id="namareferensi" maxlength="50" placeholder="Nama Customer" readonly required />
                        </div>
                    </div>
                    <div class="row mb-2">
                        <div class="col-sm-6 input-group">
                            <div class="col-sm-6 p-0 input-group-prepend">
                                <span class="col-sm-12 input-group-text">Telah Diterima</span>
                            </div>
                            <input class="form-control" type="text" name="diterima" id="diterima" maxlength="50" placeholder="0" readonly required />
                        </div>
                        <div class="col-sm-6 input-group">
                            <div class="col-sm-6 p-0 input-group-prepend">
                                <span class="col-sm-12 input-group-text">Total Transaksi</span>
                            </div>
                            <input class="form-control" type="text" name="totaltransaksi" id="totaltransaksi" maxlength="50" placeholder="0" readonly required />
                        </div>
                    </div>
                    <div class="row mb-2">
                        <div class="col-sm-6 input-group">
                            <div class="col-sm-6 p-0 input-group-prepend">
                                <span class="col-sm-12 input-group-text">Sisa</span>
                            </div>
                            <input class="form-control" type="text" name="sisa" id="sisa" maxlength="50" placeholder="0" readonly required />
                        </div>
                    </div>
                    <div class="row mb-2">
                        <div class="col-sm-12 input-group">
                            <div class="col-sm-3 p-0 input-group-prepend">
                                <span class="col-sm-12 input-group-text">Keterangan</span>
                            </div>
                            <textarea name="keterangan" id="keterangan" class="form-control" placeholder="Keterangan"></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <!-- <div class="modal-content mb-2">
                <div class="modal-body">
                    
                </div>
            </div> -->
        </div>
        <div class="col-12 col-md-12 col-lg-12 col-xl-6">
            <div class="modal-content mb-2">
                <div class="modal-body">
                    <div class="row mb-2 mt-2">
                        <div class="col-sm-12 input-group">
                            <div class="col-sm-3 p-0 input-group-prepend">
                                <span class="col-sm-12 input-group-text">Account Kasir</span>
                            </div>
                            <input class="col-sm-3 form-control" type="text" name="nomoracc" id="nomoracc" maxlength="50" placeholder="Nomor" readonly required />
                            <input class="form-control" type="text" name="namaacc" id="namaacc" maxlength="50" placeholder="Nama" readonly required />
                            <div class="input-group-text btn-dark" data-toggle="modal" data-target="#findAccount" id="cariaccount">
                                <span class="input-group-addon">
                                    <i class="fa fa-search text-white"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-2">
                        <div class="col-sm-12 input-group">
                            <div class="col-sm-3 p-0 input-group-prepend">
                                <span class="col-sm-12 input-group-text">Ket. Account</span>
                            </div>
                            <textarea name="ketacc" id="ketacc" class="form-control" placeholder="Ket. Account" readonly></textarea>
                        </div>
                    </div>
                    <div class="row mb-2">
                        <div class="col-sm-12 input-group">
                            <div class="col-sm-3 p-0 input-group-prepend">
                                <span class="col-sm-12 input-group-text">Memo</span>
                            </div>
                            <textarea name="memo" id="memo" class="form-control" placeholder="Memo"></textarea>
                        </div>
                    </div>
                    <div class="row mb-2">
                        <div class="col-sm-12 input-group">
                            <div class="col-sm-3 p-0 input-group-prepend">
                                <span class="col-sm-12 input-group-text">Nilai Penerimaan</span>
                            </div>
                            <input class="form-control" type="text" name="nilaipenerimaan" id="nilaipenerimaan" maxlength="50" placeholder="0" required />
                            <div class="input-group-text btn-dark" id="adddetail">
                                <span class="input-group-addon">
                                    <i class="fa fa-plus mr-1 text-white"></i>
                                    <span class="text-white">Add</span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-content mb-2" id="grupalokasi">
                <div class="modal-body">
                    <div class="row mb-2 mb-2">
                        <div class="col-sm-12 input-group">
                            <div class="col-sm-3 p-0 input-group-prepend">
                                <span class="col-sm-12 input-group-text">Deposit Customer</span>
                            </div>
                            <div class="input-group-text">
                                <label class="radio radio-dark mb-0 mr-1 ml-1">
                                    <input type="radio" name="deposit" id="deposit" value="true"><span>Potong Deposit</span><span class="checkmark"></span>
                                </label>
                                <label class="radio radio-dark mb-0 mr-1 ml-1">
                                    <input type="radio" name="deposit" id="tidak_deposit" value="false"><span>Penambahan Deposit</span><span class="checkmark"></span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-2 mt-2">
                        <div class="col-sm-12 input-group">
                            <div class="col-sm-3 p-0 input-group-prepend">
                                <span class="col-sm-12 input-group-text">Account Alokasi</span>
                            </div>
                            <input class="col-sm-3 form-control" type="text" name="noalokasi" id="noalokasi" maxlength="50" placeholder="Nomor Alokasi" readonly required />
                            <input class="form-control" type="text" name="namaalokasi" id="namaalokasi" maxlength="50" placeholder="Nama Alokasi" readonly required />
                            <!-- <div class="input-group-text btn-dark" data-toggle="modal" data-target="#findaccountlain" id="cariaccalokasi">
                                <span class="input-group-addon">
                                    <i class="fa fa-search text-white"></i>
                                </span>
                            </div> -->
                        </div>
                    </div>
                    <div class="row mb-2">
                        <div class="col-sm-12 input-group">
                            <div class="col-sm-3 p-0 input-group-prepend">
                                <span class="col-sm-12 input-group-text">Nilai Alokasi</span>
                            </div>
                            <input class="form-control" type="text" name="nilaialokasi" id="nilaialokasi" maxlength="50" placeholder="0" required />
                        </div>
                    </div>
                    <div class="row mb-2 mb-2">
                        <div class="col-sm-12 input-group">
                            <div class="col-sm-3 p-0 input-group-prepend">
                                <span class="col-sm-12 input-group-text">Pembulatan</span>
                            </div>
                            <div class="input-group-text">
                                <label class="radio radio-dark mb-0 mr-1 ml-1">
                                    <input type="radio" name="pembulatan" id="pembulatan" value="true"><span>Ya</span><span class="checkmark"></span>
                                </label>
                                <label class="radio radio-dark mb-0 mr-1 ml-1">
                                    <input type="radio" name="pembulatan" id="tidak_pembulatan" value="false"><span>Tidak</span><span class="checkmark"></span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-2">
                        <div class="col-sm-12 input-group">
                            <div class="col-sm-3 p-0 input-group-prepend">
                                <span class="col-sm-12 input-group-text">Nilai Pembulatan</span>
                            </div>
                            <input class="form-control" type="text" name="nilaipembulatan" id="nilaipembulatan" maxlength="50" placeholder="0" required />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-12 col-lg-12 col-xl-12">
            <div class="modal-content mb-2">
                <div class="modal-body">
                    <div class="row mb-2">
                        <div class="table-responsive">
                            <table id="t_detail" class="table table-bordered table-striped table-default" style="width:100%">
                                <thead class="thead-dark">
                                    <tr style="line-height: 0.5 cm;">
                                        <th style="text-align: center;" id="judulth">
                                            DocInvoice
                                        </th>
                                        <th style="text-align: center; display: none; ">
                                            Jenis
                                        </th>
                                        <th style="text-align: center; display: none; ">
                                            Penerimaan
                                        </th>
                                        <th style="text-align: center; ">
                                            NoReferensi
                                        </th>
                                        <th style="text-align: center; ">
                                            Referensi
                                        </th>
                                        <th style="text-align: center; ">
                                            Account
                                        </th>
                                        <th style="text-align: center; ">
                                            NamaAccount
                                        </th>
                                        <th style="text-align: center; ">
                                            TotalTransaksi
                                        </th>
                                        <th style="text-align: center; ">
                                            NilaiPenerimaan
                                        </th>
                                        <th style="text-align: center; ">
                                            AccountAlokasi
                                        </th>
                                        <th style="text-align: center; ">
                                            NilaiAlokasi
                                        </th>
                                        <th style="text-align: center; ">
                                            Memo
                                        </th>
                                        <th style="text-align: center; ">
                                            NilaiPembulatan
                                        </th>
                                        <th style="text-align: center; display:none;">
                                            Deposit
                                        </th>
                                        <th style="text-align: center; ">
                                            Action
                                        </th>
                                    </tr>
                                </thead>
                                <tbody class="tbody-scroll scroller" id="tb_detail">

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="modal-footer p-2">
                    <div class="col-sm-4 input-group">
                        <div class="col-sm-6 p-0 input-group-prepend">
                            <span class="col-sm-12 input-group-text">Total Penerimaan</span>
                        </div>
                        <input class="form-control" type="text" name="totalpenerimaan" id="totalpenerimaan" maxlength="50" placeholder="0" readonly required />
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="findjenispenerimaan">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" style="border-radius: 5px;">
            <div class="modal-header" style="padding: 10px; color: #000000;">
                <h7 class="modal-title" style="margin-left: 5px;">Data Jenis Penerimaan</h7>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table id="t_jenispenerimaan" class="table table-bordered table-striped dt-responsive display nowrap" style="width:100%">
                        <thead class="thead-dark">
                            <tr style="line-height: 0.5 cm; ">
                                <th style="text-align: center;">
                                    <span style="font-weight: bold; color: #eee;">Action</span>
                                </th>
                                <th style="text-align: center; ">
                                    <span style="font-weight: bold; color: #eee;">Kode</span>
                                </th>
                                <th style="text-align: center; ">
                                    <span style="font-weight: bold; color: #eee;">Nama</span>
                                </th>
                            </tr>
                        </thead>
                        <tbody></tbody>

                    </table>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="finddepartement">
    <div class="modal-dialog modal-md">
        <div class="modal-content" style="border-radius: 5px;">
            <div class="modal-header" style="padding: 10px; color: #000000;">
                <h7 class="modal-title" style="margin-left: 5px;">Data Departement</h7>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table id="t_departement" class="table table-bordered table-striped dt-responsive display nowrap" style="width:100%">
                        <thead class="thead-dark">
                            <tr style="line-height: 0.5 cm; ">
                                <th style="text-align: center; ">
                                    <span style="font-weight: bold; color: #eee;">Action</span>
                                </th>
                                <th style="text-align: center; ">
                                    <span style="font-weight: bold; color: #eee;">Kode</span>
                                </th>
                                <th style="text-align: center; ">
                                    <span style="font-weight: bold; color: #eee;">Nama</span>
                                </th>
                            </tr>
                        </thead>
                        <tbody></tbody>

                    </table>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="findDataCustomer">
    <div class="modal-dialog modal-lg" id="modalDocument">
        <div class="modal-content" style="border-radius: 5px;">
            <div class="modal-header" style="padding: 10px; color: #000000;">
                <h7 class="modal-title" style="margin-left: 5px;">Data Customer</h7>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table id="t_deposit" class="table table-bordered table-striped dt-responsive display nowrap" style="width:100%">
                        <thead class="thead-dark">
                            <tr style="line-height: 0.5 cm; ">
                                <th style="text-align: center; ">
                                    <span style="font-weight: bold; color: #eee;">Action</span>
                                </th>

                                <th style="text-align: center; ">
                                    <span style="font-weight: bold; color: #eee;">Nomor</span>
                                </th>
                                <th style="text-align: center; ">
                                    <span style="font-weight: bold; color: #eee;">Nama</span>
                                </th>
                                <th style="text-align: center; ">
                                    <span style="font-weight: bold; color: #eee;">Nama Toko</span>
                                </th>
                                <th style="text-align: center; ">
                                    <span style="font-weight: bold; color: #eee;">Total Deposit</span>
                                    <!-- </th>
                                <th style="text-align: center; ">
                                    <span style="font-weight: bold; color: #eee;">No. HP</span>
                                </th>
                                <th style="text-align: center; ">
                                    <span style="font-weight: bold; color: #eee;">No. Telp</span>
                                </th> -->
                            </tr>
                        </thead>
                        <tbody></tbody>

                    </table>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="findDocument">
    <div class="modal-dialog modal-lg" id="modalDocument">
        <div class="modal-content" style="border-radius: 5px;">
            <div class="modal-header" style="padding: 10px; color: #000000;">
                <h7 class="modal-title" style="margin-left: 5px;">Data Dokumen</h7>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table id="t_document" class="table table-bordered table-striped dt-responsive display nowrap" style="width:100%">
                        <thead class="thead-dark">
                            <tr style="line-height: 0.5 cm; ">
                                <th style="text-align: center; ">
                                    <span style="font-weight: bold; color: #eee;">Action</span>
                                </th>
                                <!-- <th style="text-align: center; ">
                                    <span style="font-weight: bold; color: #eee;">Nomor RO</span>
                                </th>
                                <th style="text-align: center; ">
                                    <span style="font-weight: bold; color: #eee;">Nomor PO</span>
                                </th> -->
                                <th style="text-align: center; ">
                                    <span style="font-weight: bold; color: #eee;">Nomor SO</span>
                                </th>
                                <th style="text-align: center; ">
                                    <span style="font-weight: bold; color: #eee;">Tanggal SO</span>
                                </th>
                                <th style="text-align: center; ">
                                    <span style="font-weight: bold; color: #eee;">Nama Toko</span>
                                </th>
                                <!-- <th style="text-align: center; ">
                                    <span style="font-weight: bold; color: #eee;">Nama Customer</span>
                                </th> -->
                                <!-- <th style="text-align: center; ">
                                    <span style="font-weight: bold; color: #eee;">Alamat Kirim</span>
                                </th> -->
                                <!-- <th style="text-align: center; ">
                                    <span style="font-weight: bold; color: #eee;">Catatan</span>
                                </th> -->
                                <th style="text-align: center; ">
                                    <span style="font-weight: bold; color: #eee;">Nilai Uang Muka</span>
                                </th>
                                <th style="text-align: center; ">
                                    <span style="font-weight: bold; color: #eee;">Grand Total</span>
                                </th>
                            </tr>
                        </thead>
                        <tbody></tbody>

                    </table>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="findPiutang">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" style="border-radius: 5px;">
            <div class="modal-header" style="padding: 10px; color: #000000;">
                <h7 class="modal-title" style="margin-left: 5px;">Data Dokumen</h7>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table id="t_piutang" class="table table-bordered table-striped dt-responsive display nowrap" style="width:100% dt-responsive display nowrap" style="width:100%">
                        <thead class="thead-dark">
                            <tr style="line-height: 0.5 cm; ">
                                <th style="text-align: center; ">
                                    <span style="font-weight: bold; color: #eee;">Action</span>
                                </th>
                                <th style="text-align: center; ">
                                    <span style="font-weight: bold; color: #eee;">Nomor Faktur</span>
                                </th>
                                <th style="text-align: center; ">
                                    <span style="font-weight: bold; color: #eee;">Tanggal</span>
                                </th>
                                <th style="text-align: center; ">
                                    <span style="font-weight: bold; color: #eee;">Tanggal Jatuh Tempo</span>
                                </th>
                                <th style="text-align: center; ">
                                    <span style="font-weight: bold; color: #eee;">Nomor Surat Jalan</span>
                                </th>
                                <th style="text-align: center; ">
                                    <span style="font-weight: bold; color: #eee;">Nama Toko</span>
                                </th>
                                <th style="text-align: center; ">
                                    <span style="font-weight: bold; color: #eee;">Nama Customer</span>
                                </th>
                                <th style="text-align: center; ">
                                    <span style="font-weight: bold; color: #eee;">Nilai Piutang</span>
                                </th>
                                <th style="text-align: center; ">
                                    <span style="font-weight: bold; color: #eee;">Nilai Penerimaan</span>
                                </th>
                                <!-- <th style="text-align: center; ">
                                    <span style="font-weight: bold; color: #eee;">Uang Muka</span>
                                </th> -->
                                <!-- <th style="text-align: center; ">
                                    <span style="font-weight: bold; color: #eee;">Transaksi</span>
                                </th> -->
                            </tr>
                        </thead>
                        <tbody></tbody>

                    </table>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="findongkir">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" style="border-radius: 5px;">
            <div class="modal-header" style="padding: 10px; color: #000000;">
                <h7 class="modal-title" style="margin-left: 5px;">Data Dokumen</h7>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table id="t_ongkir" class="table table-bordered table-striped dt-responsive display nowrap" style="width:100% dt-responsive display nowrap" style="width:100%">
                        <thead class="thead-dark">
                            <tr style="line-height: 0.5 cm; ">
                                <th style="text-align: center; ">
                                    <span style="font-weight: bold; color: #eee;">Action</span>
                                </th>
                                <th style="text-align: center; ">
                                    <span style="font-weight: bold; color: #eee;">Nomor Faktur</span>
                                </th>
                                <th style="text-align: center; ">
                                    <span style="font-weight: bold; color: #eee;">Tanggal</span>
                                </th>
                                <th style="text-align: center; ">
                                    <span style="font-weight: bold; color: #eee;">Tanggal Jatuh Tempo</span>
                                </th>
                                <th style="text-align: center; ">
                                    <span style="font-weight: bold; color: #eee;">Nomor Surat Jalan</span>
                                </th>
                                <th style="text-align: center; ">
                                    <span style="font-weight: bold; color: #eee;">Nama Toko</span>
                                </th>
                                <th style="text-align: center; ">
                                    <span style="font-weight: bold; color: #eee;">Nama Customer</span>
                                </th>
                                <th style="text-align: center; ">
                                    <span style="font-weight: bold; color: #eee;">Nilai Piutang</span>
                                </th>
                                <th style="text-align: center; ">
                                    <span style="font-weight: bold; color: #eee;">Nilai Penerimaan</span>
                                </th>
                                <!-- <th style="text-align: center; ">
                                    <span style="font-weight: bold; color: #eee;">Uang Muka</span>
                                </th> -->
                                <!-- <th style="text-align: center; ">
                                    <span style="font-weight: bold; color: #eee;">Transaksi</span>
                                </th> -->
                            </tr>
                        </thead>
                        <tbody></tbody>

                    </table>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="findAccount">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" style="border-radius: 5px;">
            <div class="modal-header" style="padding: 10px; color: #000000;">
                <h7 class="modal-title" style="margin-left: 5px;">Data Account</h7>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table id="t_account" class="table table-bordered table-striped dt-responsive display nowrap" style="width:100%">
                        <thead class="thead-dark">
                            <tr style="line-height: 0.5 cm; ">
                                <th style="text-align: center; ">
                                    <span style="font-weight: bold; color: #eee;">Action</span>
                                </th>
                                <th style="text-align: center; ">
                                    <span style="font-weight: bold; color: #eee;">Nomor</span>
                                </th>
                                <th style="text-align: center; ">
                                    <span style="font-weight: bold; color: #eee;">Nama</span>
                                </th>
                                <th style="text-align: center; ">
                                    <span style="font-weight: bold; color: #eee;">No. Rekening</span>
                                </th>
                                <th style="text-align: center; ">
                                    <span style="font-weight: bold; color: #eee;">Keterangan</span>
                                </th>
                            </tr>
                        </thead>
                        <tbody></tbody>

                    </table>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="findaccountlain">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" style="border-radius: 5px;">
            <div class="modal-header" style="padding: 10px; color: #000000;">
                <h7 class="modal-title" style="margin-left: 5px;">Data Account Lain</h7>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table id="t_accountlain" class="table table-bordered table-striped dt-responsive display nowrap" style="width:100%">
                        <thead class="thead-dark">
                            <tr style="line-height: 0.5 cm; ">
                                <th style="text-align: center; ">
                                    <span style="font-weight: bold; color: #eee;">Action</span>
                                </th>
                                <th style="text-align: center; ">
                                    <span style="font-weight: bold; color: #eee;">Nomor</span>
                                </th>
                                <th style="text-align: center; ">
                                    <span style="font-weight: bold; color: #eee;">Nama</span>
                                </th>
                            </tr>
                        </thead>
                        <tbody></tbody>

                    </table>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="findPenerimaanUang">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" style="border-radius: 5px;">
            <div class="modal-header" style="padding: 10px; color: #000000;">
                <h7 class="modal-title" style="margin-left: 5px;">Data Penerimaan Uang</h7>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table id="t_penerimaanuang" class="table table-bordered table-striped dt-responsive display nowrap" style="width:100% dt-responsive display nowrap" style="width:100%">
                        <thead class="thead-dark">
                            <tr style="line-height: 0.5 cm; ">
                                <th style="text-align: center; ">
                                    <span style="font-weight: bold; color: #eee;">Action</span>
                                </th>
                                <th style="text-align: center; ">
                                    <span style="font-weight: bold; color: #eee;">Nomor</span>
                                </th>
                                <th style="text-align: center; ">
                                    <span style="font-weight: bold; color: #eee;">Tanggal</span>
                                </th>
                                <th style="text-align: center; ">
                                    <span style="font-weight: bold; color: #eee;">Nama Toko</span>
                                </th>
                                <th style="text-align: center; ">
                                    <span style="font-weight: bold; color: #eee;">Nilai Penerimaan</span>
                                </th>
                                <th style="text-align: center; ">
                                    <span style="font-weight: bold; color: #eee;">Jenis Penerimaan</span>
                                </th>
                                <th style="text-align: center; ">
                                    <span style="font-weight: bold; color: #eee;">User Input</span>
                                </th>
                                <th style="text-align: center; ">
                                    <span style="font-weight: bold; color: #eee;">Tanggal Input</span>
                                </th>
                            </tr>
                        </thead>
                        <tbody></tbody>

                    </table>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="changepenerimaan">
    <div class="modal-dialog modal-md">
        <div class="modal-content" style="border-radius: 5px;">
            <div class="modal-header" style="padding: 10px; color: #000;">
                <h7 class="modal-title" style="margin-left: 5px;"> UBAH DETAIL</h7>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row mb-2">
                    <div class="col-sm-6 input-group">
                        <div class="col-sm-6 p-0 input-group-prepend">
                            <span class="col-sm-12 input-group-text judulpencarian">Doc Invoice</span>
                        </div>
                        <input class="form-control" type="text" name="chgdocinvoice" id="chgdocinvoice" maxlength="50" readonly required />
                    </div>
                </div>
                <div class="row mb-2">
                    <div class="col-sm-6 input-group">
                        <div class="col-sm-6 p-0 input-group-prepend">
                            <span class="col-sm-12 input-group-text">Jenis</span>
                        </div>
                        <input class="form-control" type="text" name="chgjenis" id="chgjenis" maxlength="50" value="0" required readonly />
                    </div>
                    <div class="col-sm-6 input-group">
                        <div class="col-sm-6 p-0 input-group-prepend">
                            <span class="col-sm-12 input-group-text">Penerimaan</span>
                        </div>
                        <input class="form-control" type="text" name="chgpenerimaan" id="chgpenerimaan" maxlength="50" value="0" required readonly />
                    </div>

                </div>
                <div class="row mb-2">
                    <div class="col-sm-6 input-group">
                        <div class="col-sm-6 p-0 input-group-prepend">
                            <span class="col-sm-12 input-group-text">No Customer</span>
                        </div>
                        <input class="form-control" type="text" name="chgnoreferensi" id="chgnoreferensi" maxlength="50" required readonly />
                    </div>
                    <div class="col-sm-6 input-group">
                        <div class="col-sm-6 p-0 input-group-prepend">
                            <span class="col-sm-12 input-group-text">Nama Customer</span>
                        </div>
                        <input class="form-control" type="text" name="chgreferensi" id="chgreferensi" maxlength="50" required readonly />
                    </div>
                </div>
                <div class="row mb-2">
                    <div class="col-sm-6 input-group">
                        <div class="col-sm-6 p-0 input-group-prepend">
                            <span class="col-sm-12 input-group-text">Account</span>
                        </div>
                        <input class="form-control" type="text" name="chgaccount" id="chgaccount" maxlength="50" value="0" required readonly />
                    </div>
                    <div class="col-sm-6 input-group">
                        <div class="col-sm-6 p-0 input-group-prepend">
                            <span class="col-sm-12 input-group-text">Nama Account</span>
                        </div>
                        <input class="form-control" type="text" name="chgnamaaccount" id="chgnamaaccount" maxlength="50" value="0" required readonly />
                    </div>
                </div>
                <div class="row mb-2">

                    <div class="col-sm-6 input-group">
                        <div class="col-sm-6 p-0 input-group-prepend">
                            <span class="col-sm-12 input-group-text">Total Penerimaan</span>
                        </div>
                        <input class="form-control" type="text" name="chgtotalpenerimaan" id="chgtotalpenerimaan" maxlength="50" value="0" required readonly />
                    </div>
                    <div class="col-sm-6 input-group">
                        <div class="col-sm-6 p-0 input-group-prepend">
                            <span class="col-sm-12 input-group-text">Nilai Penerimaan</span>
                        </div>
                        <input class="form-control" type="text" name="chgnilaipenerimaan" id="chgnilaipenerimaan" maxlength="50" value="0" required />
                    </div>
                </div>
                <div class="row mb-2">
                    <div class="col-sm-6 input-group">
                        <div class="col-sm-6 p-0 input-group-prepend">
                            <span class="col-sm-12 input-group-text">Acc. Alokasi</span>
                        </div>
                        <input class="form-control" type="text" name="chgaccountalokasi" id="chgaccountalokasi" maxlength="50" required readonly />
                    </div>
                    <div class="col-sm-6 input-group">
                        <div class="col-sm-6 p-0 input-group-prepend">
                            <span class="col-sm-12 input-group-text">Nilai Alokasi</span>
                        </div>
                        <input class="form-control" type="text" name="chgnilaialokasi" id="chgnilaialokasi" maxlength="50" value="0" required />
                    </div>
                </div>
                <div class="row mb-2">
                    <div class="col-sm-12 input-group">
                        <div class="col-sm-3 p-0 input-group-prepend">
                            <span class="col-sm-12 input-group-text">Memo</span>
                        </div>
                        <textarea name="chgmemo" id="chgmemo" class="form-control" placeholder="Memo"></textarea>
                    </div>
                </div>
                <div class="row mb-2">
                    <div class="col-sm-6 input-group">
                        <div class="col-sm-6 p-0 input-group-prepend">
                            <span class="col-sm-12 input-group-text">Nilai Pembulatan</span>
                        </div>
                        <input class="form-control" type="text" name="chgnilaipembulatan" id="chgnilaipembulatan" maxlength="50" value="0" required />
                    </div>
                </div>
            </div>
            <div class="modal-footer p-2">
                <button type="button" class="btn btn-dark mb-0" data-dismiss="modal" id="changedetail"><i class="fa fa-random mr-1"></i>Edit</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="modalcancel">
    <div class="modal-dialog modal-md">
        <div class="modal-content" style="border-radius: 5px;">
            <div class="modal-header" style="padding: 10px;">
                <h5 class="modal-title" style="margin-left: 5px; color: #f44336;"><i class="far fa-info-circle mr-1"></i>CANCEL DOKUMEN</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <center>
                    <h5 class="modal-title"></i>Apakah Anda Yakin ?</h5>
                </center>
                <div class="row mb-2">
                    <div class="col-sm-12 input-group box">
                        <!-- <input class="col-sm-12 form-control" type="hidden" name="cancelnopr" id="cancelnopr" maxlength="50" placeholder="Nomor PR" readonly required/> -->
                    </div>
                </div>
                <div class="row mb-2">
                    <div class="col-sm-12 input-group box">
                        <div class="col-sm-3 p-0 input-group-prepend">
                            <span class="col-sm-12 input-group-text">Alasan Batal</span>
                        </div>
                        <textarea name="keteranganbatal" id="keteranganbatal" class="form-control" placeholder="Alasan Batal ..."></textarea>
                    </div>
                </div>
            </div>
            <div class="modal-footer p-2">
                <button id="okcancel" type="button" class="btn btn-dark mb-0" data-dismiss="modal"><i class="far fa-check-circle mr-1"></i>Confirm</button>
            </div>
        </div>
    </div>
</div>
