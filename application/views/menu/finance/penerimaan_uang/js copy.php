<script type="text/javascript">
	$(document).ready(function() {
		var pembulatannilai = "false";
		var depositnilai = "false";
		$('#tgl').datepicker({
			format: "yyyy-mm-dd",
			autoclose: true,
			todayHighlight: true
		});
		var sisadeposit = 0;
		var saldodeposit = 0;
		var chgsaldodeposit = 0;

		function clearScreen() {

			ClearTransaksi();
			$('.judulpencarian').text('Document Invoice');
			$('#save').prop('disabled', false);
			$('#cancel').prop('disabled', true);

			$('#nomor').val("");
			$('#tanggal').val($.datepicker.formatDate('yy-mm-dd', new Date()));
			$('#kodepenerimaan').val("");
			$('#namapenerimaan').val("");
			$('#kodedepartement').val("");
			$('#namadepartement').val("");
			$('#nomoracc').val("");
			$('#namaacc').val("");
			$('#nilaipenerimaan').val(0).prop('disabled', false);
			$('#noalokasi').val("");
			$('#namaalokasi').val("");
			$('#nilaialokasi').val(0).prop('disabled', true);
			$('#nilaipembulatan').val(0).prop('disabled', true);
			$('#keterangan').val("").prop('disabled', false);
			$('#memo').prop('disabled', false).val("");
			$('#totalpenerimaan').val(0);
			$('#keteranganbatal').val("");

			$('#tb_detail').empty();

			$('#carijenispenerimaan').show();
			$('#caridokumen').show();
			$('#cariaccount').show();
			$('#adddetail').show();
			$('.deldetail').show();
			$('.editdetail').show();
			$('#cariaccalokasi').show();

			$('#grupdepartement').hide();
			$('#grupalokasi').hide();

			// if (data[i].aktif.trim() == 't') {
			// 	$('#aktif').prop('checked', true);
			// } else {
			// 	$('#tidak_aktif').prop('checked', true);
			// }

			$('#tidak_deposit').prop('checked', true);
			$('#tidak_pembulatan').prop('checked', true);

			UnableAction();
			DataCoaDepositCabang();
		};

		function ClearTransaksi() {

			$('#nomordocument').val("");
			$('#noreferensi').val("");
			$('#namareferensi').val("");
			$('#diterima').val(0);
			$('#totaltransaksi').val(0);
			$('#sisa').val(0);
			$('#nilaipembulatan').val(0);


			if ($('#kodepenerimaan').val() != "TUF99") {
				$('#nomoracc').val("");
				$('#namaacc').val("");
			}
			$('#nilaipenerimaan').val(0);
			$('#memo').val("");
		};


		function UnableAction() {
			$('#keterangan').prop("disabled", false);
			$('#memo').prop("disabled", false);
			$('#nilaipenerimaan').prop("disabled", false);

			$('#carijenispenerimaan').show();
			$('#caridokumen').show();
			$('#cariaccount').show();
			$('#adddetail').show();
		};

		function DisableAction() {
			document.getElementById('cancel').disabled = false;
			$('#keterangan').prop("disabled", true);
			$('#memo').prop("disabled", true);
			$('#nilaipenerimaan').prop("disabled", true);

			$('#carijenispenerimaan').hide();
			$('#caridokumen').hide();
			$('#cariaccount').hide();
			$('#adddetail').hide();
		};

		function FormatRupiah(angka, prefix) {
			// if (!angka) {
			// 	return '';
			// }
			// var vangka = angka.toString();
			// var number_string = vangka.replace(/[^.\d]/g, '').replace(/[^\w\s]/gi, '').toString(),
			// 	split = number_string.split('.'),
			// 	sisa = split[0].length % 3,
			// 	rupiah = split[0].substr(0, sisa),
			// 	ribuan = split[0].substr(sisa).match(/\d{3}/gi);

			// // tambahkan titik jika yang di input sudah menjadi angka ribuan
			// if (ribuan) {
			// 	separator = sisa ? ',' : '';
			// 	rupiah += separator + ribuan.join(',');
			// }

			// rupiah = split[1] != undefined ? rupiah + '.' + split[1] : rupiah;
			// return prefix == undefined ? rupiah : (rupiah ? rupiah : '');
			if (!angka) {
				return '';
			}
			let v = angka.toString();
			if (v.substring(0, 1) == "0") {
				v = "";
			} else {
				if (angka != 0 || angka != "0") {

					const neg = v.toString().startsWith('-');

					v = v.toString().replace(/[-\D]/g, '');
					v = v.toString().replace(/(\d{1,2})$/, '$1');
					v = v.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');

					v = v != '' ? '' + v : '';
					if (neg) v = '-'.concat(v); // prepend the dash


				}
			}
			return v;
		};

		function DeFormatRupiah(angka) {
			var result = angka.replace(/,/g, '');
			return result;
		};


		function HitungSummary() {
			var table = document.getElementById('t_detail');
			var sublist = 0;
			var nilaipenerimaan = 0;
			var subtotal = 0;
			for (var r = 1, n = table.rows.length; r < n; r++) {
				for (var c = 0, m = table.rows[r].cells.length; c < m - 1; c++) {
					nilaipenerimaan = parseFloat(DeFormatRupiah(table.rows[r].cells[7].innerHTML));
					nilaialokasi = parseFloat(DeFormatRupiah(table.rows[r].cells[9].innerHTML));
					nilaipembulatan = parseFloat(DeFormatRupiah(table.rows[r].cells[11].innerHTML));
					sublist = nilaipenerimaan + nilaialokasi;
				}
				subtotal += parseFloat(sublist);
			}
			$('#totalpenerimaan').val(FormatRupiah(subtotal));
		};

		function AmbilDataTableDetail() {
			var table = document.getElementById('t_detail');
			var arr2 = [];
			for (var r = 1, n = table.rows.length; r < n; r++) {
				var string = "";
				for (var c = 0, m = table.rows[r].cells.length; c < m - 1; c++) {
					if (c == 0) {
						string = "{" + table.rows[0].cells[c].innerHTML + " : '" + table.rows[r].cells[c].innerHTML + "'";
					} else {
						string = string + ", " + table.rows[0].cells[c].innerHTML + " : '" + table.rows[r].cells[c].innerHTML + "'";
					}
				}
				string = string + "}";
				var obj = JSON.stringify(eval('(' + string + ')'));
				var arr = $.parseJSON(obj);
				arr2.push(arr);
			}
			return arr2;
		};

		/* Declare Toast */
		const Toast = Swal.mixin({
			toast: true,
			position: 'top-end',
			showConfirmButton: false,
			timer: 3000,
			timerProgressBar: true,
			didOpen: (toast) => {
				toast.addEventListener('mouseenter', Swal.stopTimer)
				toast.addEventListener('mouseleave', Swal.resumeTimer)
			}
		});

		function validasiSave() {
			var datadetail = AmbilDataTableDetail();
			if ($('#kodepenerimaan').val() == '') {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Kode Penerimaan tidak boleh kosong.',
					showCloseButton: true,
					width: 350,
				});
				$('#kodepenerimaan').focus();
				var result = false;
			} else if ($('#keterangan').val() == '') {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Keterangan tidak boleh kosong.',
					showCloseButton: true,
					width: 350,
				});
				$('#keterangan').focus();
				var result = false;
			} else if ($('#kodepenerimaan').val() == 'TUF99' && $('#kodedepartement').val() == '') {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Kode Departement boleh kosong.',
					showCloseButton: true,
					width: 350,
				});
				$('#kodedepartement').focus();
				var result = false;
			} else if (datadetail.length == 0) {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Data Detail tidak boleh kosong.',
					showCloseButton: true,
					width: 350,
				});
				var result = false;
			} else {
				var result = true;
			}
			return result;
		};

		function validasiAddDetail(kode) {
			var intRegex = /^\d+$/;

			var table = document.getElementById('t_detail');
			if ($('#kodepenerimaan').val() == '') {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Kode Penerimaan tidak boleh kosong.',
					showCloseButton: true,
					width: 350,
				});
				$('#kodepenerimaan').focus();
				return 'false';
			} else if ($('#nomordocument').val() == '') {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Nomor Dokumen tidak boleh kosong.',
					showCloseButton: true,
					width: 350,
				});
				$('#nomordocument').focus();
				return 'false';
			} else if ($('#nomoracc').val() == '') {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Nomor Account tidak boleh kosong.',
					showCloseButton: true,
					width: 350,
				});
				$('#nomoracc').focus();
				return 'false';
			} else if (($('#nilaipenerimaan').val() == '' || $('#nilaipenerimaan').val() == 0) && ($('#nilaialokasi').val() == '' || $('#nilaialokasi').val() == 0)) {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Nilai Penerimaan tidak boleh kosong.',
					showCloseButton: true,
					width: 350,
				});
				$('#nilaipenerimaan').focus();
				return 'false';
			} else if (!intRegex.test(DeFormatRupiah($('#nilaipenerimaan').val()))) {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Nilai Penerimaan tidak boleh mengandung huruf.',
					showCloseButton: true,
					width: 350,
				});
				$('#nilaipenerimaan').focus();
				return 'false';
			} else if ($('#kodepenerimaan').val() == 'TUF02' && (Number(DeFormatRupiah($('#nilaipenerimaan').val())) + Number(DeFormatRupiah($('#nilaialokasi').val())) + Number(DeFormatRupiah($('#nilaipembulatan').val()))) > Number(DeFormatRupiah($('#sisa').val()))) {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Nilai Penerimaan tidak boleh lebih besar dari sisa.',
					showCloseButton: true,
					width: 350,
				});
				$('#nilaipenerimaan').focus();
				return 'false';
			} else if ($('#kodepenerimaan').val() == 'TUF03' && (Number(DeFormatRupiah($('#nilaipenerimaan').val())) + Number(DeFormatRupiah($('#nilaialokasi').val())) + Number(DeFormatRupiah($('#nilaipembulatan').val()))) > Number(DeFormatRupiah($('#sisa').val()))) {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Nilai Penerimaan tidak boleh lebih besar dari sisa.',
					showCloseButton: true,
					width: 350,
				});
				$('#nilaipenerimaan').focus();
				return 'false';
			}
			// else if (parseFloat($('#nilaialokasi').val().replace(",", "").replace(",", "").replace(",", "").replace(",", "")) > (parseFloat(saldodeposit))) {
			// 	Swal.fire({
			// 		title: 'Informasi',
			// 		icon: 'info',
			// 		html: 'Nilai Alokasi Tidak Boleh Lebih Besar Dari Saldo Deposit ' + FormatRupiah(saldodeposit.toString()),
			// 		showCloseButton: true,
			// 		width: 350,
			// 	});
			// 	$('#nilaialokasi').focus();
			// 	return 'false';

			// }
			else {
				for (var r = 1, n = table.rows.length; r < n; r++) {
					var string = "";
					for (var c = 0, m = table.rows[r].cells.length; c < m; c++) {
						if (c == 0) {
							if (table.rows[r].cells[c].innerHTML == kode) {
								Swal.fire({
									title: 'Informasi',
									icon: 'info',
									html: 'Data ini sudah diinput.',
									showCloseButton: true,
									width: 350,
								});
								return 'false';
							}
						}
					}
				}
			}
			return 'true';
		};

		document.getElementById("carijenispenerimaan").addEventListener("click", function(event) {
			event.preventDefault();

			$('#t_jenispenerimaan').DataTable({
				"language": {
					"url": "<?= base_url() ?>assets/json/id.json"
				},
				"destroy": true,
				"searching": true,
				"processing": true,
				"serverSide": true,
				"lengthChange": true,
				"pageLength": 5,
				"lengthMenu": [5, 10, 25, 50],
				"order": [],
				"ajax": {
					"url": "<?php echo base_url('finance/Penerimaanuang/CariJenisPenerimaan'); ?>",
					"method": "POST",
					"data": {
						nmtb: "stpm_otorisasikasir",
						field: {
							kode: "kode",
							nama: "nama",
							jenis: "jenis",
						},
						// sort: "kode
						where: {
							kode: "kode"
						},
						value: "jenis = '0' AND aktif = true "
					},
				}
			});

		});

		$(document).on('click', ".searchjenispenerimaan", function() {
			var kode = $(this).attr("data-id");
			$('#kodedepartement').val("");
			$('#namadepartement').val("");

			$.ajax({
				url: "<?php echo base_url('finance/Penerimaanuang/DataJenisPenerimaan'); ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					kode: kode,
				},
				success: function(data) {
					for (var i = 0; i < data.length; i++) {
						$('#kodepenerimaan').val(data[i].kode.trim());
						$('#namapenerimaan').val(data[i].nama.trim());

						if (data[i].kode.trim() == 'TUF01') {
							$('#grupalokasi').hide();
							$('#grupdepartement').hide();
							$('.judulpencarian').text('Nomor Customer');
						} else if (data[i].kode.trim() == 'TUF02') {
							$('#grupalokasi').show();
							$('#grupdepartement').hide();
							$('.judulpencarian').text('Document SO');
						} else if (data[i].kode.trim() == 'TUF03') {
							$('#grupalokasi').show();
							$('#grupdepartement').hide();
							$('.judulpencarian').text('Document Invoice');
						} else if (data[i].kode.trim() == 'TUF99') {
							$('#grupdepartement').show();
							$('#grupalokasi').hide();
							$('.judulpencarian').text('Nomor Account');
						}
						SetValueRadioButton()
					}

					ClearTransaksi();
				}
			}, false);
		});

		document.getElementById("caridepartement").addEventListener("click", function(event) {
			event.preventDefault();
			$('#t_departement').DataTable({
				"language": {
					"url": "<?= base_url() ?>assets/json/id.json"
				},
				"destroy": true,
				"searching": true,
				"processing": true,
				"serverSide": true,
				"lengthChange": true,
				"pageLength": 5,
				"lengthMenu": [5, 10, 25, 50],
				"order": [],
				"ajax": {
					"url": "<?php echo base_url('finance/Penerimaanuang/CariDepartement'); ?>",
					"method": "POST",
					"data": {
						nmtb: "glbm_departement",
						field: {
							kode: "kode",
							nama: "nama",
						},
						sort: "kode",
						where: {
							kode: "kode"
						},
						value: "aktif = true"
					},
				}
			});
		});

		$(document).on('click', ".searchdepartement", function() {
			var kode = $(this).attr("data-id");
			$.ajax({
				url: "<?php echo base_url('finance/Penerimaanuang/DataDepartement'); ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					kode: kode,
				},
				success: function(data) {
					for (var i = 0; i < data.length; i++) {
						$('#kodedepartement').val(data[i].kode.trim());
						$('#namadepartement').val(data[i].nama.trim());
					}
				}
			});
		});

		function validasiPencarian() {
			if ($('#kodepenerimaan').val() == '') {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Jenis Penerimaan tidak boleh kosong.',
					showCloseButton: true,
					width: 350,
				});
				var result = false;
			} else if ($('#kodepenerimaan').val() == 'TUF99' && $('#kodedepartement').val() == '') {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Silakan Pilih Department terlebih dahulul.',
					showCloseButton: true,
					width: 350,
				});
				var result = false;
			} else {
				var result = true;
			}
			return result;
		};

		document.getElementById("caridokumen").addEventListener("click", function(event) {
			event.preventDefault();

			$('#caridokumen').attr('data-toggle', '');
			if (validasiPencarian() == true) {
				$('#caridokumen').attr('data-toggle', 'modal');
				if ($('#kodepenerimaan').val() == 'TUF01') {
					$('#caridokumen').attr('data-target', '#findDataCustomer');
					CariDokumenDeposit();
				} else if ($('#kodepenerimaan').val() == 'TUF02') {
					$('#caridokumen').attr('data-target', '#findDocument');
					CariDokumenSO();
				} else if ($('#kodepenerimaan').val() == 'TUF03') {
					$('#caridokumen').attr('data-target', '#findPiutang');
					CariDokumenPiutang();
				} else if ($('#kodepenerimaan').val() == 'TUF99') {
					$('#caridokumen').attr('data-target', '#findaccountlain');
					CariDokumenLainLain();
				}
			}

		});

		function CariDokumenDeposit() {
			$('#t_deposit').DataTable({
				"language": {
					"url": "<?= base_url() ?>assets/json/id.json"
				},
				"destroy": true,
				"searching": true,
				"processing": true,
				"serverSide": true,
				"lengthChange": true,
				"pageLength": 5,
				"lengthMenu": [5, 10, 25, 50],
				"order": [],
				"ajax": {
					"url": "<?php echo base_url('finance/Penerimaanuang/CariDataCustomer'); ?>",
					"method": "POST",
					"data": {
						nmtb: "cari_depositcustomer",
						field: {
							nomor: "nomor",
							nama: "nama",
							nama_toko: "nama_toko",
							total_deposit: "total_deposit"
						},
						sort: "nomor",
						where: {
							nomor: "nomor",
							nama_toko: "nama_toko"
						},
						// value: "aktif = true "
					},
				},
				"columnDefs": [{
					"targets": 4,
					"data": "total_deposit",
					"render": function(data, type, row, meta) {
						return FormatRupiah(row[4].toString())
					}
				}, ]
			});
		};
		$(document).on('click', ".searchcustomer", function() {
			var nomor = $(this).attr("data-id");
			$.ajax({
				url: "<?php echo base_url('finance/Penerimaanuang/DataCustomer'); ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					nomor: nomor,
				},
				success: function(data) {
					for (var i = 0; i < data.length; i++) {
						$('#nomordocument').val(data[i].nomor.trim());
						$('#noreferensi').val(data[i].nomor.trim());
						$('#namareferensi').val(data[i].nama_toko.trim());
						$('#diterima').val(0);
						$('#totaltransaksi').val(0);
						$('#sisa').val(0);
						DataRekening(data[i].rekeningtf.trim());


					}
				}
			}, false);
		});

		function CariDokumenSO() {
			$('#t_document').DataTable({
				"language": {
					"url": "<?= base_url() ?>assets/json/id.json"
				},
				"destroy": true,
				"searching": true,
				"processing": true,
				"serverSide": true,
				"lengthChange": true,
				"pageLength": 5,
				"lengthMenu": [5, 10, 25, 50],
				"order": [],
				"ajax": {
					"url": "<?php echo base_url('finance/Penerimaanuang/CariDataSO'); ?>",
					"method": "POST",
					"data": {
						nmtb: "srvt_salesorder",
						field: {
							nomor: "nomor",
							tanggal: "tanggal",
							namapelanggan: "namapelanggan",
							nilaiuangmuka: "nilaiuangmuka",
							grandtotal: "grandtotal",
						},
						sort: "nomor",
						where: {
							nomor: "nomor",
							namapelanggan: "namapelanggan",
						},
						value: "batal = false AND grandtotal > nilaiuangmuka"
					},
				},
				"columnDefs": [{
					"targets": 2,
					"render": $.fn.dataTable.render.moment('YYYY-MM-DD HH:mm:ss', 'D MMM YY')
				}, {
					"targets": 4,
					"data": "nilaiuangmuka",
					"render": function(data, type, row, meta) {
						return FormatRupiah(row[4]);
					}
				}, {
					"targets": 5,
					"data": "grandtotal",
					"render": function(data, type, row, meta) {
						return FormatRupiah(row[5]);
					}
				}]
			});
		};

		$(document).on('click', ".searchso", function() {
			var nomor = $(this).attr("data-id");
			$.ajax({
				url: "<?php echo base_url('finance/Penerimaanuang/DataSO'); ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					nomor: nomor,
				},
				success: function(data) {
					for (var i = 0; i < data.length; i++) {
						$('#nomordocument').val(data[i].nomor.trim());
						$('#noreferensi').val(data[i].nopelanggan.trim());
						$('#namareferensi').val(data[i].namapelanggan.trim());
						$('#diterima').val(FormatRupiah(data[i].nilaiuangmuka.trim()));
						$('#totaltransaksi').val(FormatRupiah(data[i].grandtotal.trim()));
						$('#sisa').val(FormatRupiah(Number(data[i].grandtotal.trim()) - Number(data[i].nilaiuangmuka.trim())));
						CariRekeningCustomer(data[i].nopelanggan.trim());

					}
				}
			}, false);
		});

		function CariDokumenPiutang() {
			$('#t_piutang').DataTable({
				"language": {
					"url": "<?= base_url() ?>assets/json/id.json"
				},
				"destroy": true,
				"searching": true,
				"processing": true,
				"serverSide": true,
				"lengthChange": true,
				"pageLength": 5,
				"lengthMenu": [5, 10, 25, 50],
				"order": [],
				"ajax": {
					"url": "<?php echo base_url('finance/Penerimaanuang/CariDataPiutang'); ?>",
					"method": "POST",
					"data": {
						nmtb: "cari_piutang",
						field: {
							nomor: "nomor",
							tanggal: "tanggal",
							tgljthtempo: "tgljthtempo",
							nomorsj: "nomorsj",
							nama_toko: "nama_toko",
							namapelanggan: "namapelanggan",
							nilaipiutang: "nilaipiutang",
							nilaipenerimaan: "nilaipenerimaan",
							nilaiuangmuka: "nilaiuangmuka",
							// transaksi: "transaksi"
						},
						sort: "nomor",
						where: {
							nomor: "nomor"
						},
						value: "nilaipiutang > (nilaipenerimaan + nilaiuangmuka) AND kodecabang = '" + $('#cabang').val() + "'"
					},
				},
				"columnDefs": [{
						"targets": 2,
						"render": $.fn.dataTable.render.moment('YYYY-MM-DD HH:mm:ss', 'D MMM YY'),
					}, {
						"targets": 3,
						"render": $.fn.dataTable.render.moment('YYYY-MM-DD HH:mm:ss', 'D MMM YY'),
					}, {
						"targets": 7,
						"data": "nilaipiutang",
						"render": function(data, type, row, meta) {
							return FormatRupiah(row[7]);
						}
					},
					{
						"targets": 8,
						"data": "nilaipenerimaan",
						"render": function(data, type, row, meta) {
							return FormatRupiah(row[8]);
						}
					},
					{
						"targets": 9,
						"data": "nilaiuangmuka",
						"render": function(data, type, row, meta) {
							return FormatRupiah(row[9]);
						}
					}
				]
			});
		};

		$(document).on('click', ".searchpiutang", function() {
			var nomor = $(this).attr("data-id");
			$.ajax({
				url: "<?php echo base_url('finance/Penerimaanuang/DataPiutang'); ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					nomor: nomor,
				},
				success: function(data) {
					for (var i = 0; i < data.length; i++) {
						diterima = 0 + Number(data[i].nilaipenerimaan.trim());
						total = Number(data[i].nilaipiutang.trim()) + Number(data[i].nilaipenerimaan.trim());
						$('#nomordocument').val(data[i].nomor.trim());
						$('#noreferensi').val(data[i].nopelanggan.trim());
						$('#namareferensi').val(data[i].namapelanggan.trim());
						$('#diterima').val(FormatRupiah(diterima));
						$('#totaltransaksi').val(FormatRupiah(Number(data[i].nilaipiutang.trim()) + Number(data[i].nilaipenerimaan.trim())));

						$('#sisa').val(FormatRupiah(total - Number(diterima)));

						// console.log($('input[name="aktif"]:checked').val());

						CariRekeningCustomer(data[i].nopelanggan.trim());
					}
				}
			});
		});

		function CariDokumenLainLain() {
			$('#t_accountlain').DataTable({
				"language": {
					"url": "<?= base_url() ?>assets/json/id.json"
				},
				"destroy": true,
				"searching": true,
				"processing": true,
				"serverSide": true,
				"lengthChange": true,
				"pageLength": 5,
				"lengthMenu": [5, 10, 25, 50],
				"order": [],
				"ajax": {
					"url": "<?php echo base_url('finance/Penerimaanuang/CariDokumenLainLain'); ?>",
					"method": "POST",
					"data": {
						nmtb: "glbm_accountlainlain",
						field: {
							nomor: "nomor",
							nama: "nama",
							// jenis: "jenis",
						},
						sort: "nomor",
						where: {
							nomor: "nomor"
						}
					},
				}
			});
		}

		$(document).on('click', ".searchdoclain", function() {
			var nomor = $(this).attr("data-id");
			$.ajax({
				url: "<?php echo base_url('finance/Penerimaanuang/DataAccountLain'); ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					nomor: nomor,
				},
				success: function(data) {
					for (var i = 0; i < data.length; i++) {
						$('#nomordocument').val(data[i].nomor.trim());
						$('#noreferensi').val(data[i].nomor.trim());
						$('#namareferensi').val(data[i].nama.trim());
					}
				}
			});
		});

		document.getElementById("cariaccount").addEventListener("click", function(event) {
			event.preventDefault();
			$('#t_account').DataTable({
				"language": {
					"url": "<?= base_url() ?>assets/json/id.json"
				},
				"destroy": true,
				"searching": true,
				"processing": true,
				"serverSide": true,
				"lengthChange": true,
				"pageLength": 5,
				"lengthMenu": [5, 10, 25, 50],
				"order": [],
				"ajax": {
					"url": "<?php echo base_url('finance/Penerimaanuang/CariAccount'); ?>",
					"method": "POST",
					"data": {
						nmtb: "glbm_account",
						field: {
							nomor: "nomor",
							nama: "nama",
							norekening: "norekening",
						},
						sort: "nomor",
						where: {
							nomor: "nomor"
						},
						// value: "kode_cabang = '" + $('#cabang').val() + "'"
					},
				}
			});
		});

		$(document).on('click', ".searchaccount", function() {
			var noaccount = $(this).attr("data-id");
			DataRekening(noaccount);

		});

		function DataRekening(noaccount) {
			$.ajax({
				url: "<?php echo base_url('finance/Penerimaanuang/DataAccount'); ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					noaccount: noaccount,
				},
				success: function(data) {
					for (var i = 0; i < data.length; i++) {
						$('#nomoracc').val(data[i].nomor.trim());
						$('#namaacc').val(data[i].nama.trim());
					}
				}
			});
		}

		function SetValueRadioButton() {
			$('#tidak_deposit').prop('checked', true);
			// $('#noalokasi').val("");
			// $('#namaalokasi').val("");
			switch ($('#kodepenerimaan').val()) {
				case "TUF01":
					$('input[name=deposit]').attr("disabled", true);
					$('#noalokasi').val("");
					$('#namaalokasi').val("");
					break;
				case "TUF99":
					$('input[name=deposit]').attr("disabled", true);
					$('#noalokasi').val("");
					$('#namaalokasi').val("");
					break;
				default:
					$('input[name=deposit]').attr("disabled", false);
					break;
			}
		}

		// var aktif = $('input[name="aktif"]:checked').val();
		$('input[type=radio][name=deposit]').change(function() {
			sisadeposit = 0;
			saldodeposit = 0;
			$('#tidak_pembulatan').prop('checked', true);
			HitungNilaipembulatan(false);
			if ($('#nomordocument').val() == "") {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Nomor Dokumen Belum di Pilih.',
					showCloseButton: true,
					width: 350,
				});
				$('#nomordocument').focus();
				$('#tidak_deposit').prop('checked', true);
				$('#cariaccalokasi').show();
				$('#noalokasi').val("");
				$('#namaalokasi').val("");
				$('#nilaialokasi').val(0);
			} else {
				if (this.value == "false") {
					if ($('#kodepenerimaan').val() != "TUF99") {
						DataCoaDepositCabang();
						CariRekeningCustomer($('#noreferensi').val());
						$('#cariaccalokasi').show();
						// $('#noalokasi').val("");
						// $('#namaalokasi').val("");
						$('#nilaialokasi').val(0);
						$('#nilaialokasi').prop('disabled', true);
					}
				} else {
					$('#cariaccalokasi').hide();
					DataCoaDepositCabang();
					SisaDeposit($('#noreferensi').val());
					SisaDepositYangTerpakai();
					$('#nilaialokasi').val(FormatRupiah(saldodeposit.toString()));
					$('#nilaialokasi').prop('disabled', false);


				}
			}
		});

		$('input[type=radio][name=pembulatan]').change(function() {
			pembulatannilai = this.value;
			HitungNilaipembulatan(this.value)
		});


		function HiddenAfterAdd() {
			var table = document.getElementById('t_detail');
			sisadeposit = 0;
			saldodeposit = 0;
			$('#nilaipenerimaan').val('');
			$('#noalokasi').val('');
			$('#namaalokasi').val('');
			$('#nilaialokasi').val(0);
			$('#memo').val("");
			$('#cariaccount').show();
			$('#tidak_deposit').prop('checked', true);
			if (table.rows.length > 1) {
				$('input[name=deposit]').attr("disabled", false);
				$('#carijenispenerimaan').hide();
			} else {
				$('#carijenispenerimaan').show();
			}
		}

		function SisaDeposit(nomor) {
			$.ajax({
				url: "<?php echo base_url('finance/Penerimaanuang/SisaDeposit'); ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					nomor: nomor,
				},
				success: function(data) {
					if (data.length > 0) {
						for (var i = 0; i < data.length; i++) {
							sisadeposit = data[i].sisadeposit
						}
					} else {
						sisadeposit = 0;
					}
				}
			}, false);
		}

		function SisaDepositYangTerpakai() {
			var table = document.getElementById('t_detail');
			var subtotal = 0;
			var sublist = 0;
			saldodeposit = 0
			if (table.rows.length > 1) {
				for (var r = 1, n = table.rows.length; r < n; r++) {
					for (var c = 0, m = table.rows[r].cells.length; c < m - 1; c++) {
						// console.log(table.rows[r].cells[2].innerHTML);
						// console.log(table.rows[r].cells[11].innerHTML);
						if (table.rows[r].cells[3].innerHTML == $('#noreferensi').val() && table.rows[r].cells[11].innerHTML == "YA") {
							// console.log("B");
							sublist = parseFloat(table.rows[r].cells[9].innerHTML.replace(",", "").replace(",", "").replace(",", "").replace(",", "").replace(",", "").replace(",", ""));
						}
					}
					subtotal += parseFloat(sublist);
				}
			}
			// console.log(sublist)
			saldodeposit = parseFloat(sisadeposit) - parseFloat(subtotal);
		}

		function CariRekeningCustomer(nomor) {
			$.ajax({
				url: "<?php echo base_url('finance/Penerimaanuang/DataCustomer'); ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					nomor: nomor,
				},
				success: function(data) {
					for (var i = 0; i < data.length; i++) {
						DataRekening(data[i].rekeningtf.trim());
					}
				}
			}, false);
		}

		function DataKasirAccount(nomor) {
			$.ajax({
				url: "<?php echo base_url('finance/Penerimaanuang/DataKasirAccount'); ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					nomor: nomor,
				},
				success: function(data) {
					for (var i = 0; i < data.length; i++) {
						DataRekening(data[i].rekeningtf.trim());
					}
				}
			}, false);
		}

		function DataCoaDepositCabang() {
			$.ajax({
				url: "<?php echo base_url('finance/Penerimaanuang/DataCoaDepositCabang'); ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					kodecabang: $('#cabang').val(),
				},
				success: function(data) {

					for (var i = 0; i < data.length; i++) {
						DataAlokasiAccount(data[i].coadeposit.trim());
					}
				}
			}, false);
		}


		// document.getElementById("cariaccalokasi").addEventListener("click", function(event) {
		// 	event.preventDefault();
		// 	$('#t_accountlain').DataTable({
		// 		"destroy": true,
		// 		"searching": true,
		// 		"processing": true,
		// 		"serverSide": true,
		// 		"lengthChange": true,
		// 		"order": [],
		// 		"ajax": {
		// 			"url": "<?php echo base_url('finance/Penerimaanuang/CariAccount'); ?>",
		// 			"method": "POST",
		// 			"data": {
		// 				nmtb: "glbm_coa",
		// 				field: {
		// 					noaccount: "noaccount",
		// 					nama: "nama",
		// 					jenis: "jenis",
		// 				},
		// 				sort: "noaccount",
		// 				where: {
		// 					noaccount: "noaccount"
		// 				}
		// 			},
		// 		}
		// 	});
		// });

		$(document).on('click', ".searchaccount", function() {
			var nomor = $(this).attr("data-id");
			DataAlokasiAccount(nomor);
		});

		function DataAlokasiAccount(nomor) {
			$.ajax({
				url: "<?php echo base_url('finance/Penerimaanuang/DataAccountLain'); ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					nomor: nomor,
				},
				success: function(data) {
					if (data.length > 0) {
						for (var i = 0; i < data.length; i++) {
							$('#noalokasi').val(data[i].nomor.trim());
							$('#namaalokasi').val(data[i].nama.trim());
						}
					} else {
						$('#noalokasi').val("");
						$('#namaalokasi').val("");
					}
				}
			});
		}

		document.getElementById("adddetail").addEventListener("click", function(event) {
			event.preventDefault();
			var kodepenerimaan = $('#kodepenerimaan').val();
			var namapenerimaan = $('#namapenerimaan').val();
			var nomordocument = $('#nomordocument').val();
			var noreferensi = $('#noreferensi').val();
			var namareferensi = $('#namareferensi').val();
			var diterima = $('#diterima').val();
			var totaltransaksi = $('#totaltransaksi').val();
			var sisa = $('#sisa').val();
			var nomoracc = $('#nomoracc').val();
			var namaacc = $('#namaacc').val();
			var nilaipenerimaan = $('#nilaipenerimaan').val();
			var noalokasi = $('#noalokasi').val();
			var namaalokasi = $('#namaalokasi').val();
			var nilaialokasi = $('#nilaialokasi').val();
			var nilaipembulatan = $('#nilaipembulatan').val();
			var keterangan = $('#keterangan').val();
			var memo = $('#memo').val();
			var deposit = "TIDAK";
			var kodepencarian = nomordocument.replace(".", "").replace(".", "").replace(".", "").replace("#", "").replace("#", "").replace("#", "").replace(",", "").replace(",", "").replace(",", "");


			if (validasiAddDetail(nomordocument) == 'true') {
				var row = "";
				row =
					'<tr id="' + kodepencarian + '">' +
					'<td>' + nomordocument + '</td>' +
					'<td>' + kodepenerimaan + '</td>' +
					'<td>' + namapenerimaan + '</td>' +
					'<td>' + noreferensi + '</td>' +
					'<td>' + namareferensi + '</td>' +
					'<td>' + nomoracc + '</td>' +
					'<td>' + namaacc + '</td>' +
					'<td id="nilaipenerimaan' + kodepencarian + '"> ' + FormatRupiah(nilaipenerimaan) + '</td>' +
					'<td>' + noalokasi + '</td>' +
					'<td id="nilaialokasi' + kodepencarian + '">' + FormatRupiah(nilaialokasi) + '</td>' +
					'<td id="memo' + kodepencarian + '">' + memo + '</td>' +
					'<td id="nilaipembulatan' + kodepencarian + '">' + FormatRupiah(nilaipembulatan) + '</td>' +
					'<td  style="display:none;">' + deposit + '</td>' +
					'<td style="text-align: center;">' +
					// '<button data-table="' + kodepencarian + '" data-toggle="modal" data-target="#changepenerimaan" class="editdetail btn btn-success m-1"><i class="fas fa-edit mr-1"></i> Edit</button>' +
					'<button data-table="' + kodepencarian + '" class="deldetail btn btn-danger m-1"><i class="fa fa-trash mr-1"></i> Delete</button>' +
					'</td>' +
					'</tr>';
				$('#tb_detail').append(row);
				HiddenAfterAdd();
				HitungSummary();

				ClearTransaksi();
			}
		});

		function HitungNilaipembulatan(status) {
			if (status == "true") {
				var nilaipembulatan = 0;
				var sisa = DeFormatRupiah($('#sisa').val());
				var nilaialokasi = DeFormatRupiah($('#nilaialokasi').val());
				var nilaipenerimaan = DeFormatRupiah($('#nilaipenerimaan').val());
				console.log(sisa);
				console.log(nilaialokasi);
				console.log(nilaipenerimaan);
				nilaipembulatan = parseFloat(sisa) - parseFloat(nilaipenerimaan) - parseFloat(nilaialokasi);
				$('#nilaipembulatan').val(FormatRupiah(nilaipembulatan.toString()));
			} else {
				$('#nilaipembulatan').val(0);
			}

		}

		$('#nilaipenerimaan').keyup(function() {
			var harga = FormatRupiah(this.value, '');
			$('#nilaipenerimaan').val(harga);
			HitungNilaipembulatan(pembulatannilai);
		})


		$('#nilaialokasi').keyup(function() {
			var harga = FormatRupiah(this.value, '');
			$('#nilaialokasi').val(harga);
			HitungNilaipembulatan(pembulatannilai);
		});
		$('#nilaipembulatan').keyup(function() {
			var harga = FormatRupiah(this.value, '');
			$('#nilaipembulatan').val(harga);
		});



		$('#chgnilaipenerimaan').keyup(function() {
			var harga = FormatRupiah(this.value, '');
			$('#chgnilaipenerimaan').val(harga);
		});

		$('#chgnilaialokasi').keyup(function() {
			var harga = FormatRupiah(this.value, '');
			$('#chgnilaialokasi').val(harga);
		});

		$(document).on('click', '.editdetail', function() {
			sisadeposit = 0;
			chgsaldodeposit = 0;
			_row = $(this);
			$('#chgdocinvoice').val(_row.closest("tr").find("td:eq(0)").text());
			$('#chgjenis').val(_row.closest("tr").find("td:eq(1)").text());
			$('#chgpenerimaan').val(_row.closest("tr").find("td:eq(2)").text());
			$('#chgnoreferensi').val(_row.closest("tr").find("td:eq(3)").text());
			$('#chgreferensi').val(_row.closest("tr").find("td:eq(4)").text());
			$('#chgaccount').val(_row.closest("tr").find("td:eq(5)").text());
			$('#chgnamaaccount').val(_row.closest("tr").find("td:eq(6)").text());
			$('#chgnilaipenerimaan').val(_row.closest("tr").find("td:eq(7)").text());
			$('#chgaccountalokasi').val(_row.closest("tr").find("td:eq(8)").text());
			$('#chgnilaialokasi').val(_row.closest("tr").find("td:eq(9)").text());
			$('#chgmemo').val(_row.closest("tr").find("td:eq(10)").text());
			$('#chgnilaipembulatan').val(_row.closest("tr").find("td:eq(11)").text());
			var totalpengeluaran = parseFloat(_row.closest("tr").find("td:eq(7)").text().replace(",", "").replace(",", "").replace(",", "").replace(",", "").replace(",", "")) + parseFloat(_row.closest("tr").find("td:eq(9)").text().replace(",", "").replace(",", "").replace(",", "").replace(",", "").replace(",", "")) + parseFloat(_row.closest("tr").find("td:eq(11)").text().replace(",", "").replace(",", "").replace(",", "").replace(",", "").replace(",", ""));
			$('#chgtotalpenerimaan').val(FormatRupiah(totalpengeluaran.toString()));

			if ($('#chgaccountalokasi').val() == '') {
				$('#chgnilaialokasi').prop("disabled", true);
			} else {
				$('#chgnilaialokasi').prop("disabled", false);
			}
		});

		$(document).on('click', '#changedetail', function() {
			SisaDeposit($('#chgnoreferensi').val());
			ChgSisaDepositYangTerpakai();
			var nilaipenerimaan = parseFloat($('#chgnilaipenerimaan').val().replace(",", "").replace(",", "").replace(",", "").replace(",", "")) + parseFloat($('#chgnilaialokasi').val().replace(",", "").replace(",", "").replace(",", "").replace(",", "")) + parseFloat($('#chgnilaipembulatan').val().replace(",", "").replace(",", "").replace(",", "").replace(",", ""));
			if ((nilaipenerimaan > parseFloat($('#chgtotalpenerimaan').val().replace(",", "").replace(",", "").replace(",", "").replace(",", ""))) && ($('#chgjenis').val() != "TUF01") && ($('#chgjenis').val() != "TUF99")) {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Total Tidak Boleh Lebih Besar Dari Total Penerimaan',
					showCloseButton: true,
					width: 350,
				});
				$('#chgnilaipenerimaan').focus();
			}
			// else if ((parseFloat($('#chgnilaialokasi').val().replace(",", "").replace(",", "").replace(",", "").replace(",", "")) > (parseFloat(chgsaldodeposit))) && ($('#chgjenis').val() != "TUF01") && ($('#chgjenis').val() != "TUF99")) {
			// 	Swal.fire({
			// 		title: 'Informasi',
			// 		icon: 'info',
			// 		html: 'Nilai Alokasi Tidak Boleh Lebih Besar Dari Saldo Deposit ' + FormatRupiah(saldodeposit.toString()),
			// 		showCloseButton: true,
			// 		width: 350,
			// 	});
			// 	$('#chgnilaialokasi').focus();
			// }
			else {
				var nomordocument = $('#chgdocinvoice').val().replace(".", "").replace(".", "").replace(".", "").replace("#", "").replace("#", "").replace("#", "").replace(",", "").replace(",", "").replace(",", "");
				$('#nilaipenerimaan' + nomordocument.trim()).html(FormatRupiah($('#chgnilaipenerimaan').val()));
				$('#nilaialokasi' + nomordocument.trim()).html(FormatRupiah($('#chgnilaialokasi').val()));
				$('#nilaipembulatan' + nomordocument.trim()).html(FormatRupiah($('#chgnilaipembulatan').val()));
				$('#memo' + nomordocument.trim()).html($('#chgmemo').val());
				HitungSummary();
			}
		});


		function ChgSisaDepositYangTerpakai() {
			var table = document.getElementById('t_detail');
			var subtotal = 0;
			var sublist = 0;
			chgsaldodeposit = 0
			if (table.rows.length > 1) {
				for (var r = 1, n = table.rows.length; r < n; r++) {
					for (var c = 0, m = table.rows[r].cells.length; c < m - 1; c++) {
						if (table.rows[r].cells[3].innerHTML == $('#noreferensi').val() && table.rows[r].cells[12].innerHTML == "YA" && table.rows[r].cells[0].innerHTML != $('#chgdocinvoice').val()) {
							sublist = parseFloat(table.rows[r].cells[9].innerHTML.replace(",", "").replace(",", "").replace(",", "").replace(",", "").replace(",", "").replace(",", ""));
						}
					}
					subtotal += parseFloat(sublist);
				}
			}

			chgsaldodeposit = parseFloat(sisadeposit) - parseFloat(subtotal);
		}


		// function InsertDetailPenerimaan(nomordocument, kodepenerimaan, namapenerimaan, noreferensi, namareferensi, nomoracc, namaacc, nilaipenerimaan, noalokasi, nilaialokasi, memo) {

		// 	var row = "";
		// 	row =
		// 		'<tr id="' + nomordocument + '">' +
		// 		'<td>' + nomordocument + '</td>' +
		// 		'<td>' + kodepenerimaan + '</td>' +
		// 		'<td>' + namapenerimaan + '</td>' +
		// 		'<td>' + noreferensi + '</td>' +
		// 		'<td>' + namareferensi + '</td>' +
		// 		'<td>' + nomoracc + '</td>' +
		// 		'<td>' + namaacc + '</td>' +
		// 		'<td>' + FormatRupiah(nilaipenerimaan) + '</td>' +
		// 		'<td>' + noalokasi + '</td>' +
		// 		'<td>' + FormatRupiah(nilaialokasi) + '</td>' +
		// 		'<td>' + memo + '</td>' +
		// 		'<td style="display:none;">' + deposit + '</td>' +
		// 		'<td style="text-align: center;">' +
		// 		'<button data-table="' + nomordocument + '" data-toggle="modal" data-target="#changepenerimaan" class="editdetail btn btn-success mb-0"><i class="fas fa-edit mr-1"></i> Edit</button>' +
		// 		'<button data-table="' + nomordocument + '" class="deldetail btn btn-danger m-1"><i class="fa fa-trash mr-1"></i> Delete</button>' +
		// 		'</td>' +
		// 		'</tr>';
		// 	$('#tb_detail').append(row);
		// 	HitungSummary();
		// };

		$(document).on('click', '.deldetail', function() {
			var id = $(this).attr("data-table");
			$('#' + id).remove();
			HitungSummary();
		});

		document.getElementById("find").addEventListener("click", function(event) {
			event.preventDefault();
			$('#tb_detail').empty();
			$('#t_penerimaanuang').DataTable({
				"language": {
					"url": "<?= base_url() ?>assets/json/id.json"
				},
				"destroy": true,
				"searching": true,
				"processing": true,
				"serverSide": true,
				"lengthChange": true,
				"pageLength": 5,
				"lengthMenu": [5, 10, 25, 50],
				"order": [],
				"ajax": {
					"url": "<?php echo base_url('finance/Penerimaanuang/CariDataPenerimaanUang'); ?>",
					"method": "POST",
					"data": {
						nmtb: "cari_peneriamaanuang",
						field: {
							nomor: "nomor",
							tanggal: "tanggal",
							jenispenerimaan: "jenispenerimaan",
							pemakai: "pemakai",
							tglsimpan: "tglsimpan",
							namacustomer: "namacustomer",
							nilaipenerimaan: "nilaipenerimaan"
						},
						sort: "tanggal",
						where: {
							nomor: "nomor",
							jenispenerimaan: "jenispenerimaan",
							pemakai: "pemakai",
							namacustomer: "namacustomer"
						},
						value: "batal = false"
					},
				},
				"columnDefs": [{
					"targets": 2,
					"render": $.fn.dataTable.render.moment('YYYY-MM-DD HH:mm:ss', 'D MMM YY')
				}, {
					"targets": 3,
					"data": "jenispenerimaan",
					"render": function(data, type, row, meta) {
						return (row[3] == 'TUF01') ? "Deposit Pelanggan" : (row[3] == 'TUF02') ? "Uang Muka Pelanggan" : (row[3] == 'TUF03') ? "Pelunasan AR" : "Penerimaan Lain - Lain"
					}
				}, {
					"targets": 5,
					"render": $.fn.dataTable.render.moment('YYYY-MM-DD HH:mm:ss', 'D MMM YY')
				}, {
					"targets": 7,
					"data": "nilaipenerimaan",
					"render": function(data, type, row, meta) {
						return FormatRupiah(row[7]);
					}
				}]
			});
		});

		$(document).on('click', ".searchpenerimaanuang", function() {
			$('#keteranganbatal').val("");
			var nomor = $(this).attr("data-id");;
			$('#keteranganbatal').val("");
			GetPenerimaanUang(nomor);
			DisableAction();
		});

		function GetPenerimaanUang(nomor) {
			$.ajax({
				url: "<?php echo base_url('finance/Penerimaanuang/DataPenerimaanUang'); ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					nomor: nomor
				},
				success: function(data) {
					for (var i = 0; i < data.length; i++) {
						$('#nomor').val(data[i].nomor.trim());
						$('#tanggal').val($.datepicker.formatDate('yy-mm-dd', new Date(data[i].tanggal.trim())));
						$('#kodepenerimaan').val(data[i].jenispenerimaan.trim());
						$('#namapenerimaan').val(data[i].namapenerimaan.trim());
						$('#kodedepartement').val(data[i].kodedepartement.trim());
						$('#namadepartement').val(data[i].namadepartement.trim());
						$('#keterangan').val(data[i].keterangan.trim());
						var kodepenerimaan = data[i].jenispenerimaan.trim();
						var namapenerimaan = data[i].namapenerimaan.trim();
						GetPenerimaanUangDetail(nomor, kodepenerimaan, namapenerimaan);
					}
					document.getElementById('save').disabled = true;
				}
			});
		};

		function GetPenerimaanUangDetail(nomor, kodepenerimaan, namapenerimaan) {
			$.ajax({
				url: "<?php echo base_url('finance/Penerimaanuang/DataPenerimaanUangDetail'); ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					nomor: nomor
				},
				success: function(data) {
					for (var i = 0; i < data.length; i++) {
						var nomordocument = data[i].noreferensi.trim();
						var nocustomer = data[i].nocustomer.trim();
						var namacustomer = data[i].namacustomer.trim();
						var nomoracc = data[i].kodeaccount.trim();
						var namaacc = data[i].namaaccount.trim();
						var nilaipenerimaan = FormatRupiah(data[i].nilaipenerimaan.trim());
						var accountalokasi = data[i].accountalokasi.trim();
						var nilaialokasi = FormatRupiah(data[i].nilaialokasi.trim());
						var nilaipembulatan = FormatRupiah(data[i].nilaipembulatan.trim());
						var memo = data[i].memo.trim();
						var deposit = "TIDAK";
						if (data[i].deposit == "t") {
							deposit = "YA"
						}

						InsertDetailPenerimaanUang(nomordocument, kodepenerimaan, namapenerimaan, nocustomer, namacustomer, nomoracc, namaacc, nilaipenerimaan, accountalokasi, nilaialokasi, memo, deposit, nilaipembulatan);
					}
				}
			});
		};

		function InsertDetailPenerimaanUang(nomordocument, kodepenerimaan, namapenerimaan, nocustomer, namacustomer, nomoracc, namaacc, nilaipenerimaan, accountalokasi, nilaialokasi, memo, deposit, nilaipembulatan) {
			var row = "";
			row =
				'<tr id="' + nomordocument + '">' +
				'<td>' + nomordocument + '</td>' +
				'<td>' + kodepenerimaan + '</td>' +
				'<td>' + namapenerimaan + '</td>' +
				'<td>' + nocustomer + '</td>' +
				'<td>' + namacustomer + '</td>' +
				'<td>' + nomoracc + '</td>' +
				'<td>' + namaacc + '</td>' +
				'<td>' + nilaipenerimaan + '</td>' +
				'<td>' + accountalokasi + '</td>' +
				'<td>' + nilaialokasi + '</td>' +
				'<td>' + memo + '</td>' +
				'<td>' + nilaipembulatan + '</td>' +
				'<td style="display:none;">' + deposit + '</td>' +
				'<td></td>';
			'</tr>';
			$('#tb_detail').append(row);
			HitungSummary();
		};

		document.getElementById("save").addEventListener("click", function(event) {
			event.preventDefault();

			var tanggal = $('#tanggal').val();
			var kodepenerimaan = $('#kodepenerimaan').val();
			var namapenerimaan = $('#namapenerimaan').val();
			var kodedepartement = $('#kodedepartement').val();
			var namadepartement = $('#namadepartement').val();
			var kodecabang = $('#cabang').val();

			var keterangan = $('#keterangan').val();
			var grandtotal = $('#totalpenerimaan').val();
			var datadetail = AmbilDataTableDetail();

			if (validasiSave() == true) {
				$.ajax({
					url: "<?php echo base_url('finance/Penerimaanuang/Save'); ?>",
					method: "POST",
					dataType: "json",
					async: false,
					data: {
						tanggal: tanggal,
						kodedepartement: kodedepartement,
						namadepartement: namadepartement,
						kodepenerimaan: kodepenerimaan,
						namapenerimaan: namapenerimaan,
						keterangan: keterangan,
						kodecabang: kodecabang,
						datadetail: datadetail,
					},
					success: function(data) {
						if (data.nomor != "") {
							Toast.fire({
								icon: 'success',
								title: data.message
							});

							$('#save').prop('disabled', true);
							$('#cancel').prop('disabled', true);

							$('#nomor').val(data.nomor);
							$('#nilaipenerimaan').prop('disabled', true);
							$('#nilaialokasi').prop('disabled', true);
							$('#keterangan').prop('disabled', true);
							$('#memo').prop('disabled', true);

							$('#carijenispenerimaan').hide();
							$('#caridokumen').hide();
							$('#cariaccount').hide();
							$('#adddetail').hide();
							$('.deldetail').hide();
							$('.editdetail').hide();
							// clearScreen();
						} else {
							Toast.fire({
								icon: 'error',
								title: data.message
							});
						}
					}
				}, false);
			}
		});

		document.getElementById("okcancel").addEventListener("click", function(event) {
			event.preventDefault();

			var nomor = $('#nomor').val();
			var tanggal = $('#tanggal').val();
			var kodepenerimaan = $('#kodepenerimaan').val();
			var keterangan = $('#keteranganbatal').val();
			var datadetail = AmbilDataTableDetail();

			if (keterangan == '') {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Keterangan batal harus diisi.',
					showCloseButton: true,
					width: 350,
				});
				return false;
			} else {
				$.ajax({
					url: "<?php echo base_url('finance/Penerimaanuang/Cancel'); ?>",
					method: "POST",
					dataType: "json",
					async: true,
					data: {
						nomor: nomor,
						tanggal: tanggal,
						kodepenerimaan: kodepenerimaan,
						keterangan: keterangan,
						datadetail: datadetail,
					},
					success: function(data) {
						if (data.nomor != "") {
							Toast.fire({
								icon: 'success',
								title: data.message
							});
							RefreshLayar();

						} else {
							Toast.fire({
								icon: 'error',
								title: data.message
							});
						}
					}
				});
			}

		});

		function RefreshLayar() {
			location.reload(true);
		};


		document.getElementById("clear").addEventListener("click", function(event) {
			event.preventDefault();
			RefreshLayar();
		});

		clearScreen();
	});
</script>