<script type="text/javascript">
	$(document).ready(function() {
		/* Function ClearScreen */
		function ClearScreen() {
			$('#update').prop("disabled", true);
			$('#save').prop("disabled", false);

			$('#kodeslm').prop("disabled", false).val("");
			$('#namaslm').prop("disabled", false).val("");
			$('#nohp').prop("disabled", false).val("");
			$('#kodespv').prop("readonly", true).val("");
			$('#kodecabang').prop("readonly", true).val("<?= $this->session->userdata('mycabang') ?>");
			$('#namaspv').prop("readonly", true).val("");
			$('#kode').prop("readonly", true).val("");
			$('#nama').prop("readonly", true).val("");
			$('#aktif').prop("disabled", false);

			$('.aktif').hide();
			$('#tb_detail').empty();
			$('#carikodespv').show();
			$('#caribarang').show();
		};

		/* Validasi Kosong */
		function ValidasiSave() {
			// var kode = $('#kodeslm').val();
			var nama = $('#namaslm').val();
			var nohp = $('#nohp').val();
			var kodespv = $('#kodespv').val();
			var namaspv = $('#namaspv').val();
			var kodecabang = $('#kodecabang').val();

			// if (kode == '' || kode == 0) {
			// 	Swal.fire({
			// 		title: 'Informasi',
			// 		icon: 'info',
			// 		html: 'Kode tidak boleh kosong.',
			// 		showCloseButton: true,
			// 		width: 350,
			// 	});
			// 	$('#kodeslm').focus();
			// 	var result = false;
			// } else 
			if (nama == '' || nama == 0) {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Nama tidak boleh kosong.',
					showCloseButton: true,
					width: 350,
				});
				$('#namaslm').focus();
				var result = false;
			} else if (kodecabang == '' || kodecabang == 0) {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Kode Cabang tidak boleh kosong',
					showCloseButton: true,
					width: 350,
				});
				$('#kodecabang').focus();
				var result = false;
			} else if (nohp == '' || nohp == 0) {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'No. HP tidak boleh kosong.',
					showCloseButton: true,
					width: 350,
				});
				$('#nohp').focus();
				var result = false;
			} else if (kodespv == '' || kodespv == 0) {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Kode Supervisor tidak boleh kosong.',
					showCloseButton: true,
					width: 350,
				});
				$('#kodespv').focus();
				var result = false;
			} else if (namaspv == '' || namaspv == 0) {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Nama Supervisor tidak boleh kosong.',
					showCloseButton: true,
					width: 350,
				});
				$('#namaspv').focus();
				var result = false;
			} else {
				var result = true;
			}

			return result;
		}

		/* Maxlength Nomor HP */
		$('#nohp[max]:not([max=""])').on('input', function(event) {
			var $this = $(this);
			var maxlength = $this.attr('max').length;
			var value = $this.val();
			if (value && value.length >= maxlength) {
				$this.val(value.substr(0, maxlength));
			}
		})

		/* Format Rupiah */
		function FormatRupiah(angka, prefix) {
			if (!angka) {
				return '';
			}
			var vangka = angka.toString();
			var number_string = vangka.replace(/[^.\d]/g, '').replace(/[^\w\s]/gi, '').toString(),
				split = number_string.split('.'),
				sisa = split[0].length % 3,
				rupiah = split[0].substr(0, sisa),
				ribuan = split[0].substr(sisa).match(/\d{3}/gi);

			// tambahkan titik jika yang di input sudah menjadi angka ribuan
			if (ribuan) {
				separator = sisa ? ',' : '';
				rupiah += separator + ribuan.join(',');
			}

			rupiah = split[1] != undefined ? rupiah + '.' + split[1] : rupiah;
			return prefix == undefined ? rupiah : (rupiah ? rupiah : '');
		};

		/* Declare Toast */
		const Toast = Swal.mixin({
			toast: true,
			position: 'top-end',
			showConfirmButton: false,
			timer: 3000,
			timerProgressBar: true,
			didOpen: (toast) => {
				toast.addEventListener('mouseenter', Swal.stopTimer)
				toast.addEventListener('mouseleave', Swal.resumeTimer)
			}
		});

		/* Cari Data Supervisor*/
		document.getElementById("carikodespv").addEventListener("click", function(event) {
			event.preventDefault();
			var kodecabang = $('#kodecabang').val();
			console.log(kodecabang);
			$('#t_kodespv').DataTable({
				"destroy": true,
				"searching": true,
				"processing": true,
				"serverSide": true,
				"lengthChange": true,
				"pageLength": 5,
				"lengthMenu": [5, 10, 25, 50],
				"order": [],
				"ajax": {
					"url": "<?= base_url('masterdata/Supervisor/CariDataSupervisor'); ?>",
					"method": "POST",
					"data": {
						nmtb: "glbm_supervisor",
						field: {
							kode: "kode",
							nama: "nama",
							nohp: "nohp"
						},
						sort: "kode",
						where: {
							kode: "kode",
							nama: "nama",
							nohp: "nohp"
						},
						value: "aktif = true AND kodecabang = '" + kodecabang + "'"
					},
				}
			});
		});

		/*Get Data Supervisor*/
		$(document).on('click', ".searchsupervisor", function() {
			var kode = $(this).attr("data-id");;
			$.ajax({
				url: "<?= base_url('masterdata/Supervisor/DataSupervisor'); ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					kode: kode
				},
				success: function(data) {
					for (var i = 0; i < data.length; i++) {
						$('#kodespv').val(data[i].kode.trim());
						$('#namaspv').val(data[i].nama.trim());
					}
				}
			}, false);
		});

		/* Cari Data Barang */
		document.getElementById('caricabang').addEventListener('click', function(event) {
			event.preventDefault();
			$('#t_cabang').DataTable({
				"destroy": true,
				"searching": true,
				"processing": true,
				"serverSide": true,
				"lengthChange": true,
				"pageLength": 5,
				"lengthMenu": [5, 10, 25, 50],
				"order": [],
				"ajax": {
					"url": "<?= base_url('masterdata/Cabang/CariDataCabang'); ?>",
					"method": "POST",
					"data": {
						nmtb: "glbm_cabang",
						field: {
							kode: "kode",
							nama: "nama",
							alamat: "alamat",
						},
						sort: "kode",
						where: {
							kode: "kode",
							nama: "nama",
						},
						value: "aktif = true"
					},
				}
			});
		});

		/* Get Data Barang */
		$(document).on('click', ".searchcabang", function() {
			var kode = $(this).attr("data-id");
			$.ajax({
				url: "<?= base_url('masterdata/Cabang/DataCabang'); ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					kode: kode
				},
				success: function(data) {
					for (var i = 0; i < data.length; i++) {
						$('#kodecabang').val(data[i].kode.trim());
					}
				}
			}, false);
		});
		
		/* Save */
		document.getElementById('save').addEventListener("click", function(event) {
			event.preventDefault();
			var kode = $('#kodeslm').val();
			var nama = $('#namaslm').val();
			var nohp = $('#nohp').val();
			var kodespv = $('#kodespv').val();
			var namaspv = $('#namaspv').val();
			var kodecabang = $('#kodecabang').val();

			if (ValidasiSave() == true) {
				$.ajax({
					url: "<?= base_url("masterdata/Salesman/Save") ?>",
					method: "POST",
					dataType: "json",
					async: true,
					data: {
						kode: kode,
						nama: nama,
						nohp: nohp,
						kodespv: kodespv,
						namaspv: namaspv,
						kodecabang: kodecabang,
					},
					success: function(data) {
						if (data.kode != "") {
							Toast.fire({
								icon: 'success',
								title: data.message
							});
							$('#kodeslm').val(data.kode);
							$('#namaslm').prop('disabled', true);
							$('#nohp').prop('disabled', true);
							$('#kodespv').prop("readonly", true);
							$('#namaspv').prop("readonly", true);
							$('#kode').prop('disabeld', true);
							$('#nama').prop('disabled', true);
							$('#kodecabang').prop('disabled', true);

							$('#carikodespv').hide();
							$('#update').prop("disabled", true);
							$('#save').prop("disabled", true);
						} else {
							Toast.fire({
								icon: 'error',
								title: data.message
							});
						}
					}
				}, false)
			}

		})

		/* Cari Data Salesman*/
		document.getElementById("find").addEventListener("click", function(event) {
			event.preventDefault();
			$('#t_salesman').DataTable({
				"destroy": true,
				"searching": true,
				"processing": true,
				"serverSide": true,
				"lengthChange": true,
				"pageLength": 5,
				"lengthMenu": [5, 10, 25, 50],
				"order": [],
				"ajax": {
					"url": "<?= base_url('masterdata/Salesman/CariDataSalesman'); ?>",
					"method": "POST",
					"data": {
						nmtb: "glbm_salesman",
						field: {
							kode: "kode",
							nama: "nama",
							nohp: "nohp",
							kodespv: "kodespv",
							aktif: "aktif"
						},
						sort: "kode",
						where: {
							kode: "kode",
							nama: "nama",
							nohp: "nohp",
							kodespv: "kodespv",
						},
						value: "kodecabang = '" + $('#cabang').val() + "'"
					},
				},
				"columnDefs": [{
					"targets": 5,
					"data": "aktif",
					"render": function(data, type, row, meta) {
						return (row[5] == 't') ? 'Aktif' : 'Tidak Aktif';
					}
				}],
			});
		});

		/*Get Data Salesman*/
		$(document).on('click', ".searchsalesman", function() {
			var kode = $(this).attr("data-id");;
			$.ajax({
				url: "<?= base_url('masterdata/Salesman/DataSalesman'); ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					kode: kode
				},
				success: function(data) {
					for (var i = 0; i < data.length; i++) {
						$('#kodeslm').val(data[i].kode.trim());
						$('#namaslm').val(data[i].nama.trim());
						$('#nohp').val(data[i].nohp.trim());
						$('#kodespv').val(data[i].kodespv.trim());
						$('#namaspv').val(data[i].namaspv.trim());
						$('#kodecabang').val(data[i].kodecabang.trim());
						if (data[i].aktif.trim() == 't') {
							$('#aktif').prop('checked', true);
						} else {
							$('#tidak_aktif').prop('checked', true);
						}
					}
					$('#save').prop('disabled', true);
					$('#update').prop('disabled', false);
					$('#kodeslm').prop('disabled', true);
					$('.aktif').show();
				}
			}, false);
		});



		/* Update */
		document.getElementById('update').addEventListener("click", function(event) {
			event.preventDefault();
			var kode = $('#kodeslm').val();
			var nama = $('#namaslm').val();
			var nohp = $('#nohp').val();
			var kodespv = $('#kodespv').val();
			var namaspv = $('#namaspv').val();
			var kodecabang = $('#kodecabang').val();
			var aktif = $('input[name="aktif"]:checked').val();

			if (ValidasiSave() == true) {
				$.ajax({
					url: "<?= base_url("masterdata/Salesman/Update") ?>",
					method: "POST",
					dataType: "json",
					async: true,
					data: {
						kode: kode,
						nama: nama,
						nohp: nohp,
						kodespv: kodespv,
						namaspv: namaspv,
						kodecabang: kodecabang,
						aktif: aktif
					},
					success: function(data) {
						if (data.kode != "") {
							Toast.fire({
								icon: 'success',
								title: data.message
							});
							$('#kodeslm').val(data.kode);
							$('#namaslm').prop('disabled', true);
							$('#nohp').prop('disabled', true);
							$('#kodespv').prop("readonly", true);
							$('#namaspv').prop("readonly", true);
							$('#kode').prop('disabeld', true);
							$('#nama').prop('disabled', true);
							$('#kodecabang').prop('disabled', true);
							$('#aktif').prop("readonly", true);

							$('#carikodespv').hide();
							$('#update').prop("disabled", true);
							$('#save').prop("disabled", true);
						} else {
							Toast.fire({
								icon: 'error',
								title: data.message
							});
						}
					}
				}, false)
			}
		});

		/* Clear */
		document.getElementById("clear").addEventListener("click", function(event) {
			event.preventDefault();
			ClearScreen();
		});

		ClearScreen();

	});
</script>
