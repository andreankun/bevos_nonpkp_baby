<script type="text/javascript">
    $(document).ready(function() {

        function ClearScreen() {
            document.getElementById('save').disabled = false;
            document.getElementById('update').disabled = true;

            $('#kode').val("");
            $('#nama').val("");
            $('#jenis_material').val("");
            $('#alamat').val("");
            $('#alamat2').val("");
            $('#kota').val("");
            $('#kontak').val("");
            $('#npwp').val("");
            $('#alamatnpwp').val("");
            $('#namapic').val("");
            $('#jabatanpic').val("");
            $('#kontakpic').val("");

        };

        function ValidasiSave() {
            console.log($('#nama').val());
            if($('#nama').val() == ''){
                $.alert({
                    title: '<i class="fa fa-info-circle" style="color: #e60029; margin-right: 10px;" aria-hidden="true"></i><span style="font-family: bahnschrift; color: #e60029;">Info ...</span>',
                    content: 'Nama Supplier Tidak Boleh Kosong',
                    buttons: {
                        formSubmit: {
                            text: 'OK',
                            btnClass: 'btn-red'
                        }
                    }
                });   
                var result = false;
            } else if($('#alamat').val() == ''){
                $.alert({
                    title: '<i class="fa fa-info-circle" style="color: #e60029; margin-right: 10px;" aria-hidden="true"></i><span style="font-family: bahnschrift; color: #e60029;">Info ...</span>',
                    content: 'Alamat Supplier Tidak Boleh Kosong',
                    buttons: {
                        formSubmit: {
                            text: 'OK',
                            btnClass: 'btn-red'
                        }
                    }
                });   
                var result = false;
            } else if($('#kota').val() == ''){
                $.alert({
                    title: '<i class="fa fa-info-circle" style="color: #e60029; margin-right: 10px;" aria-hidden="true"></i><span style="font-family: bahnschrift; color: #e60029;">Info ...</span>',
                    content: 'Nama Kota Tidak Boleh Kosong',
                    buttons: {
                        formSubmit: {
                            text: 'OK',
                            btnClass: 'btn-red'
                        }
                    }
                });   
                var result = false;
            } else if($('#kontak').val() == ''){
                $.alert({
                    title: '<i class="fa fa-info-circle" style="color: #e60029; margin-right: 10px;" aria-hidden="true"></i><span style="font-family: bahnschrift; color: #e60029;">Info ...</span>',
                    content: 'No Kontak Customer Tidak Boleh Kosong',
                    buttons: {
                        formSubmit: {
                            text: 'OK',
                            btnClass: 'btn-red'
                        }
                    }
                });   
                var result = false;
            } else if($('#namapic').val() == ''){
                $.alert({
                    title: '<i class="fa fa-info-circle" style="color: #e60029; margin-right: 10px;" aria-hidden="true"></i><span style="font-family: bahnschrift; color: #e60029;">Info ...</span>',
                    content: 'Nama PIC  Tidak Boleh Kosong',
                    buttons: {
                        formSubmit: {
                            text: 'OK',
                            btnClass: 'btn-red'
                        }
                    }
                });   
                var result = false;
            } else {
                var result = true;
            }
            return result;
        };

        document.getElementById("find").addEventListener("click", function(event) {
            event.preventDefault();
            $('#t_supplier').DataTable({
                "destroy": true,
                "searching": true,
                "processing": true,
                "serverSide": true,
                "lengthChange": true,
                "order": [],
                "ajax": {
                    "url": "<?php echo base_url('masterdata/Supplier/CariDataSupplier'); ?>",
                    "method": "POST",
                    "data": {
                        nmtb: "glbm_supplier",
                        field: {
                            kode: "kode",
                            nama: "nama",
                            alamat1: "alamat1",
                            kontak: "nohp",
                            namapic: "pic",
                            kontakpic: "nohppic",
                        },
                        sort: "kode",
                        where: {
                            kode: "kode"
                        }
                    },
                }
            });
        });

        $(document).on('click', ".searchsupplier", function() {
            var kode = $(this).attr("data-id");;
            $.ajax({
                url: "<?php echo base_url('masterdata/Supplier/DataSupplier'); ?>",
                method: "POST",
                dataType: "json",
                async: true,
                data: {
                    kode : kode
                },
                success: function(data) {
                    for (var i = 0; i < data.length; i++) {
                        $('#kode').val(data[i].kode.trim());
                        $('#nama').val(data[i].nama.trim());
                        $('#jenis_material').val(data[i].jenissupply.trim());
                        $('#alamat').val(data[i].alamat1.trim());
                        $('#alamat2').val(data[i].alamat2.trim());
                        $('#kota').val(data[i].kota.trim());
                        $('#kontak').val(data[i].nohp.trim());
                        $('#npwp').val(data[i].npwp.trim());
                        $('#alamatnpwp').val(data[i].alamatnpwp.trim());
                        $('#namapic').val(data[i].pic.trim());
                        $('#jabatanpic').val(data[i].jabatanpic.trim());
                        $('#kontakpic').val(data[i].nohppic.trim());

                        
                    }
                    document.getElementById('save').disabled = true;
                    document.getElementById('update').disabled = false;
                }
            }, false);
        });

        document.getElementById("save").addEventListener("click", function(event) {
            event.preventDefault();

            var kode = $('#kode').val();
            var nama = $('#nama').val();
            var jenis_material = $('#jenis_material').val();
            var alamat1 = $('#alamat').val();
            var alamat2 = $('#alamat2').val();
            var kota = $('#kota').val();
            var kontak = $('#kontak').val();
            var npwp = $('#npwp').val();
            var alamatnpwp = $('#alamatnpwp').val();
            var namapic = $('#namapic').val();
            var jabatanpic = $('#jabatanpic').val();
            var kontakpic = $('#kontakpic').val();
            var top = $('#top').val();

            
            if (ValidasiSave() == true){
                $.ajax({
                    url: "<?php echo base_url('masterdata/Supplier/Save'); ?>",
                    method: "POST",
                    dataType: "json",
                    async: true,
                    data: {
                        kode : kode,
                        nama : nama,
                        jenis_material : jenis_material,
                        alamat1 : alamat1,
                        alamat2 : alamat2,
                        kota : kota,
                        kontak : kontak,
                        npwp : npwp,
                        alamatnpwp : alamatnpwp,
                        namapic : namapic,
                        jabatanpic : jabatanpic,
                        kontakpic : kontakpic,
                        top : top,
                    },
                    success: function(data) {
                        if (data.nocustomer != "") {
                            $.alert({
                                title: '<i class="fa fa-info-circle" style="color: #e60029; margin-right: 10px;" aria-hidden="true"></i><span style="font-family: bahnschrift; color: #e60029;">Info ...</span>',
                                content: data.message,
                                buttons: {
                                    formSubmit: {
                                        text: 'OK',
                                        btnClass: 'btn-red'
                                    }
                                }
                            });
                            ClearScreen();
                        } else {
                            $.alert({
                                title: '<i class="fa fa-info-circle" style="color: #e60029; margin-right: 10px;" aria-hidden="true"></i><span style="font-family: bahnschrift; color: #e60029;">Info ...</span>',
                                content: data.message,
                                buttons: {
                                    formSubmit: {
                                        text: 'OK',
                                        btnClass: 'btn-red'
                                    }
                                }
                            });
                        }
                    }
                }, false);
            }
        });

        document.getElementById("update").addEventListener("click", function(event) {
            event.preventDefault();

            var kode = $('#kode').val();
            var nama = $('#nama').val();
            var jenis_material = $('#jenis_material').val();
            var alamat1 = $('#alamat').val();
            var alamat2 = $('#alamat2').val();
            var kota = $('#kota').val();
            var kontak = $('#kontak').val();
            var npwp = $('#npwp').val();
            var alamatnpwp = $('#alamatnpwp').val();
            var namapic = $('#namapic').val();
            var jabatanpic = $('#jabatanpic').val();
            var kontakpic = $('#kontakpic').val();
            var top = $('#top').val();
            
            if (ValidasiSave() == true){
                $.ajax({
                    url: "<?php echo base_url('masterdata/Supplier/Update'); ?>",
                    method: "POST",
                    dataType: "json",
                    async: true,
                    data: {
                        kode : kode,
                        nama : nama,
                        jenis_material : jenis_material,
                        alamat1 : alamat1,
                        alamat2 : alamat2,
                        kota : kota,
                        kontak : kontak,
                        npwp : npwp,
                        alamatnpwp : alamatnpwp,
                        namapic : namapic,
                        jabatanpic : jabatanpic,
                        kontakpic : kontakpic,
                        top : top,
                    },
                    success: function(data) {
                        if (data.nocustomer != "") {
                            $.alert({
                                title: '<i class="fa fa-info-circle" style="color: #e60029; margin-right: 10px;" aria-hidden="true"></i><span style="font-family: bahnschrift; color: #e60029;">Info ...</span>',
                                content: data.message,
                                buttons: {
                                    formSubmit: {
                                        text: 'OK',
                                        btnClass: 'btn-red'
                                    }
                                }
                            });
                            ClearScreen();
                        } else {
                            $.alert({
                                title: '<i class="fa fa-info-circle" style="color: #e60029; margin-right: 10px;" aria-hidden="true"></i><span style="font-family: bahnschrift; color: #e60029;">Info ...</span>',
                                content: data.message,
                                buttons: {
                                    formSubmit: {
                                        text: 'OK',
                                        btnClass: 'btn-red'
                                    }
                                }
                            });
                        }
                    }
                }, false);
            }
        });

        document.getElementById("clear").addEventListener("click", function(event) {
            event.preventDefault();
            ClearScreen();
        });

        ClearScreen();

    });
</script>