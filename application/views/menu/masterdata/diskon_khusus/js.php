<script>
	$(document).ready(function() {
		function ClearScreen() {
			// $('#kode').prop('disabled', false).val("");
			$('#nomorpelanggan').prop('disabled', false).val("");
			$('#namapelanggan').prop('disabled', false).val("");
			$('#kodebarang').prop('disabled', false).val("");
			$('#namabarang').prop('disabled', false).val("");
			$('#diskon').prop('disabled', false).val("");
			$('#kodemerk').prop('disabled', false).val("");
			$('#aktif').prop('disabled', false);

			$('#update').prop('disabled', true);
			$('#save').prop('disabled', false);
			$('#tb_detail').empty();

			$('.aktif').hide();
		}

		// /* Maxlength Nomor HP */
		// $('#diskon[max]:not([max=""])').on('input', function(event) {
		//     var $this = $(this);
		//     var maxlength = $this.attr('max').length;
		//     var value = $this.val();
		//     if (value && value.length >= maxlength) {
		//         $this.val(value.substr(0, maxlength));
		//     }
		// })

		function ValidasiSave(datadetail) {
			// var kode = $('#kode').val();
			var nomorpelanggan = $('#nomorpelanggan').val();
			var namapelanggan = $('#namapelanggan').val();
			// var kodebarang = $('#kodebarang').val();
			// var namabarang = $('#namabarang').val();
			var diskon = $('#diskon').val();

			// if (kode == '' || kode == 0) {
			//     Swal.fire({
			//         title: 'Informasi',
			//         icon: 'info',
			//         html: 'Kode tidak boleh kosong',
			//         showCloseButton: true,
			//         width: 350
			//     });
			//     $('#kode').focus();
			//     var result = false;
			// } else 
			if (nomorpelanggan == '' || nomorpelanggan == 0) {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Nomor Pelanggan tidak boleh kosong',
					showCloseButton: true,
					width: 350
				});
				$('#nomorpelanggan').focus();
				var result = false;
			} else if (namapelanggan == '' || namapelanggan == 0) {
				Swal.fire({
					title: 'informasi',
					icon: 'info',
					html: 'Nama Pelanggan tidak boleh kosong',
					showCloseButton: true,
					width: 350
				});
				$('#namapelanggan').focus();
				var result = false;
			} else if (datadetail.length == 0 || datadetail.length == '0') {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Data Detail tidak boleh kosong.',
					showCloseButton: true,
					width: 350,
				});
				$('#namagudang').focus();
				result = false;
			}
			// else if (kodebarang == '' || kodebarang == 0) {
			//     Swal.fire({
			//         title: 'Informasi',
			//         icon: 'info',
			//         html: 'Kode Barang tidak boleh kosong',
			//         showCloseButton: true,
			//         width: 350
			//     });
			//     $('#kodebarang').focus();
			//     var result = false;
			// } else if (namabarang == '' || namabarang == 0) {
			//     Swal.fire({
			//         title: 'Informasi',
			//         icon: 'info',
			//         html: 'Nama Barang tidak boleh kosong',
			//         showCloseButton: true,
			//         width: 350
			//     });
			//     $('#namabarang').focus();
			//     var result = false;
			// } 
			else if (diskon == '' || diskon == 0) {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Diskon Tidak boleh kosong',
					showCloseButton: true,
					width: 350
				});
				$('#diskon').focus();
				var result = false;
			} else {
				var result = true;
			}

			return result;
		}

		/* Validasi Data Add Detail */
		function ValidasiAddDetail(kodebarang) {
			var table = document.getElementById('t_detail');
			var kodebarang = $('#kodebarang').val();
			var namabarang = $('#namabarang').val();

			if (kodebarang == '' || kodebarang == 0) {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Kode Barang tidak boleh kosong.',
					showCloseButton: true,
					width: 350,
				});
				$('#kodebarang').focus();
				return "gagal";
			} else if (namabarang == '' || namabarang == 0) {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Nama Barang tidak boleh kosong.',
					showCloseButton: true,
					width: 350,
				});
				$('#namabarang').focus();
				return "gagal";
			} else {
				for (var r = 1, n = table.rows.length; r < n; r++) {
					var string = "";
					for (var c = 0, m = table.rows[r].cells.length; c < m; c++) {
						if (c == 0) {
							if (table.rows[r].cells[c].innerHTML == kodebarang) {
								Swal.fire({
									title: 'Informasi',
									icon: 'info',
									html: 'Data ini sudah diinput.',
									showCloseButton: true,
									width: 350,
								});
								return "gagal";
							}
						}
					}
				}
			}
			return "sukses"
		}

		/* Declare Toast */
		const Toast = Swal.mixin({
			toast: true,
			position: 'top-end',
			showConfirmButton: false,
			timer: 3000,
			timerProgressBar: true,
			didOpen: (toast) => {
				toast.addEventListener('mouseenter', Swal.stopTimer)
				toast.addEventListener('mouseleave', Swal.resumeTimer)
			}
		});

		$('#diskon').mask('99.99%', {
			reverse: true
		});

		$("#adddetail").click(function() {

			var kodebarang = $('#kodebarang').val();
			var namabarang = $('#namabarang').val();
			if (ValidasiAddDetail(kodebarang) == "sukses") {
				var row = "";
				row =
					'<tr id="' + kodebarang + '">' +
					'<td nowrap>' + kodebarang + '</td>' +
					'<td nowrap>' + namabarang + '</td>' +
					'<td nowrap style="text-align: center; width: 200px;">' +
					// '<button data-table="' + kodebarang + '" data-toggle="modal" data-target="#changediskon" class="editdetail btn btn-success m-1 p-1"><i class="fas fa-edit"></i> Edit</button>' +
					'<button data-table="' + kodebarang + '" class="deldetail btn btn-danger m-1 p-1"><i class="fa fa-trash mr-1"></i> Delete</button>' +
					'</td>' +
					'</tr>';
				$('#tb_detail').append(row);
				$('#kodebarang').val("");
				$('#namabarang').val("");
			}
		});

		$(document).on('click', '.deldetail', function() {
			var id = $(this).attr("data-table");
			$('#' + id).remove();
		});

		function AmbilDataDetail() {
			var table = document.getElementById('t_detail');
			var arr2 = [];
			for (var r = 1, n = table.rows.length; r < n; r++) {
				var string = "";
				for (var c = 0, m = table.rows[r].cells.length; c < m - 1; c++) {
					if (c == 0) {
						string = "{" + table.rows[0].cells[c].innerHTML + " : '" + table.rows[r].cells[c].innerHTML + "'";
					} else {
						string = string + ", " + table.rows[0].cells[c].innerHTML + " : '" + table.rows[r].cells[c].innerHTML + "'";
					}
				}
				string = string + "}";
				var obj = JSON.stringify(eval('(' + string + ')'));
				var arr = $.parseJSON(obj);
				arr2.push(arr);
			}
			return arr2;
		};

		function GetDataDetail(nomorpelanggan) {
			$.ajax({
				url: "<?= base_url('masterdata/DiskonKhusus/DataDiskonKhususDetail'); ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					nomorpelanggan: nomorpelanggan
				},
				success: function(data) {
					for (var i = 0; i < data.length; i++) {
						// var kode = data[i].kode;
						var kodebarang = data[i].kodebarang;
						var namabarang = data[i].namabarang.trim();

						InsertDataDetail(kodebarang, namabarang);
					}
				}
			});
		};

		function InsertDataDetail(kodebarang, namabarang) {

			var row = "";
			row =
				'<tr id="' + kodebarang + '">' +
				'<td nowrap>' + kodebarang + '</td>' +
				'<td nowrap>' + namabarang + '</td>' +
				'<td nowrap style="text-align: center; width: 200px;">' +
				// '<button data-table="' + kodebarang + '" data-toggle="modal" data-target="#changediskon" class="editdetail btn btn-success m-1 p-1"><i class="fas fa-edit"></i> Edit</button>' +
				'<button data-table="' + kodebarang + '" class="deldetail btn btn-danger m-1 p-1"><i class="fa fa-trash mr-1"></i> Delete</button>' +
				'</td>' +
				'</tr>';
			$('#tb_detail').append(row);
		};

		document.getElementById("caripelanggan").addEventListener("click", function(event) {
			event.preventDefault();
			$('#t_pelanggan').DataTable({
				"destroy": true,
				"searching": true,
				"processing": true,
				"serverSide": true,
				"lengthChange": true,
				"pageLength": 5,
				"lengthMenu": [5, 10, 25, 50],
				"order": [],
				"ajax": {
					"url": "<?= base_url('masterdata/Customer/CariDataCustomer'); ?>",
					"method": "POST",
					"data": {
						nmtb: "glbm_customer",
						field: {
							nomor: "nomor",
							nama: "nama",
							nama_toko: "nama_toko",
						},
						sort: "nomor",
						where: {
							nomor: "nomor",
							nama: "nama",
							nama_toko: "nama_toko",
						},
						value: "aktif = true"
					},
				}
			});
		});

		$(document).on('click', ".searchcustomer", function() {
			var nomor = $(this).attr("data-id");
			$.ajax({
				url: "<?= base_url('masterdata/Customer/DataCustomer') ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					nomor: nomor
				},
				success: function(data) {
					for (var i = 0; i < data.length; i++) {
						$('#nomorpelanggan').val(data[i].nomor.trim());
						$('#namapelanggan').val(data[i].nama_toko.trim());
					}
				}
			})
		});

		document.getElementById("caribarang").addEventListener("click", function(event) {
			event.preventDefault();
			var kodemerk = $('#kodemerk').val();
			var value = "";
			if (kodemerk == "") {
				value = "aktif = true";
			} else {
				value = "aktif = true AND kodemerk = '" + kodemerk + "'";
			}
			$('#t_barang').DataTable({
				"destroy": true,
				"searching": true,
				"processing": true,
				"serverSide": true,
				"lengthChange": true,
				"pageLength": 5,
				"lengthMenu": [5, 10, 25, 50],
				"order": [],
				"ajax": {
					"url": "<?= base_url('masterdata/Barang/CariDataBarang'); ?>",
					"method": "POST",
					"data": {
						nmtb: "glbm_barang",
						field: {
							kode: "kode",
							nama: "nama",
							kodesatuan: "kodesatuan",
							kodemerk: "kodemerk",
						},
						sort: "kode",
						where: {
							kode: "kode",
							nama: "nama",
							kodesatuan: "kodesatuan",
							kodemerk: "kodemerk",
						},
						value: value
					},
				}
			});
		});

		$(document).on('click', ".searchbarang", function() {
			var kode = $(this).attr("data-id");
			$.ajax({
				url: "<?= base_url('masterdata/Barang/DataBarang') ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					kode: kode
				},
				success: function(data) {
					for (var i = 0; i < data.length; i++) {
						$('#kodebarang').val(data[i].kode.trim());
						$('#namabarang').val(data[i].nama.trim());
					}
				}
			}, false);
		});

		/* Cari Data Satuan*/
		document.getElementById("carimerk").addEventListener("click", function(event) {
			event.preventDefault();
			$('#t_merk').DataTable({
				"destroy": true,
				"searching": true,
				"processing": true,
				"serverSide": true,
				"lengthChange": true,
				"pageLength": 5,
				"lengthMenu": [5, 10, 25, 50],
				"order": [],
				"ajax": {
					"url": "<?= base_url('masterdata/Merk/CariDataMerk'); ?>",
					"method": "POST",
					"data": {
						nmtb: "glbm_merk",
						field: {
							kode: "kode",
							nama: "nama",
							// qtysatuan: "qtysatuan",
							// konversi: "konversi",
						},
						sort: "kode",
						where: {
							kode: "kode",
							nama: "nama",
						},
						value: "aktif = true"
					},
				}
			});
		});

		/*Get Data Satuan */
		$(document).on('click', ".searchmerk", function() {
			var kode = $(this).attr("data-id");;
			$.ajax({
				url: "<?= base_url('masterdata/Merk/DataMerk'); ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					kode: kode
				},
				success: function(data) {
					for (var i = 0; i < data.length; i++) {
						$('#kodemerk').val(data[i].kode.trim());
						// $('#namamerk').val(data[i].nama.trim());
					}
				}
			}, false);
		});

		document.getElementById("find").addEventListener("click", function(event) {
			event.preventDefault();
			$('#tb_detail').empty();
			$('#t_diskon').DataTable({
				"destroy": true,
				"searching": true,
				"processing": true,
				"serverSide": true,
				"lengthChange": true,
				"pageLength": 5,
				"lengthMenu": [5, 10, 25, 50],
				"order": [],
				"ajax": {
					"url": "<?= base_url('masterdata/DiskonKhusus/CariDataDiskonKhusus'); ?>",
					"method": "POST",
					"data": {
						nmtb: "glbm_diskonkhusus",
						field: {
							// kode: "kode",
							nomorpelanggan: "nomorpelanggan",
							namapelanggan: "namapelanggan",
							aktif: "aktif"
						},
						sort: "nomorpelanggan",
						where: {
							nomorpelanggan: "nomorpelanggan",
							namapelanggan: "namapelanggan",
						}
					},
				},
				"columnDefs": [{
					"targets": 3,
					"data": "aktif",
					"render": function(data, type, row, meta) {
						return (row[3] == 't') ? 'Aktif' : 'Tidak Aktif';
					}
				}],
			});
		});

		$(document).on('click', ".searchdiskon", function() {
			var nomorpelanggan = $(this).attr("data-id");
			$.ajax({
				url: "<?= base_url('masterdata/DiskonKhusus/DataDiskonKhusus') ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					nomorpelanggan: nomorpelanggan
				},
				success: function(data) {
					for (var i = 0; i < data.length; i++) {
						// $('#kode').val(data[i].kode.trim());
						$('#nomorpelanggan').val(data[i].nomorpelanggan.trim());
						$('#namapelanggan').val(data[i].namapelanggan.trim());
						// $('#kodebarang').val(data[i].kodebarang.trim());
						// $('#namabarang').val(data[i].namabarang.trim());
						$('#diskon').val(data[i].diskon.trim() + '%');
						if (data[i].aktif.trim() == 't') {
							$('#aktif').prop('checked', true);
						} else {
							$('#tidak_aktif').prop('checked', true);
						}
					}
					GetDataDetail(nomorpelanggan)
					$('#save').prop('disabled', true);
					$('#update').prop('disabled', false);
					$('#kode').prop('disabled', true);
					$('.aktif').show();
				}
			});
		});

		document.getElementById("save").addEventListener("click", function(event) {
			event.preventDefault();
			// var kode = $('#kode').val();
			var nomorpelanggan = $('#nomorpelanggan').val();
			var namapelanggan = $('#namapelanggan').val();
			var kodebarang = $('#kodebarang').val();
			var namabarang = $('#namabarang').val();
			var diskon = $('#diskon').val();
			var datadetail = AmbilDataDetail();

			if (ValidasiSave(datadetail) == true) {
				$.ajax({
					url: "<?= base_url("masterdata/DiskonKhusus/Save") ?>",
					method: "POST",
					dataType: "json",
					async: true,
					data: {
						// kode: kode,
						nomorpelanggan: nomorpelanggan,
						namapelanggan: namapelanggan,
						kodebarang: kodebarang,
						namabarang: namabarang,
						diskon: diskon,
						datadetail: datadetail
					},
					success: function(data) {
						if (data.nomorpelanggan != "") {
							Toast.fire({
								icon: 'success',
								title: data.message
							});
							// $('#kode').prop('disabled', true);
							$('#nomorpelanggan').prop('disabled', true);
							$('#namapelanggan').prop('disabled', true);
							$('#kodebarang').prop('disabled', true);
							$('#namabarang').prop('disabled', true);
							$('#diskon').prop('disabled', true);
							$('#aktif').prop('disabled', true);

							$('#update').prop("disabled", true);
							$('#save').prop("disabled", true);
						} else {
							Toast.fire({
								icon: 'error',
								title: data.message
							});
						}
					}
				}, false)
			}
		})


		document.getElementById('update').addEventListener("click", function(event) {
			event.preventDefault();
			// var kode = $('#kode').val();
			var nomorpelanggan = $('#nomorpelanggan').val();
			var namapelanggan = $('#namapelanggan').val();
			var kodebarang = $('#kodebarang').val();
			var namabarang = $('#namabarang').val();
			var diskon = $('#diskon').val();
			var aktif = $('input[name="aktif"]:checked').val();
			var datadetail = AmbilDataDetail();

			if (ValidasiSave(datadetail) == true) {
				$.ajax({
					url: "<?= base_url("masterdata/DiskonKhusus/Update") ?>",
					method: "POST",
					dataType: "json",
					async: true,
					data: {
						// kode: kode,
						nomorpelanggan: nomorpelanggan,
						namapelanggan: namapelanggan,
						kodebarang: kodebarang,
						namabarang: namabarang,
						diskon: diskon,
						aktif: aktif,
						datadetail: datadetail
					},
					success: function(data) {
						if (data.nomorpelanggan != "") {
							Toast.fire({
								icon: 'success',
								title: data.message
							});
							// $('#kode').prop('disabled', true);
							$('#nomorpelanggan').prop('disabled', true);
							$('#namapelanggan').prop('disabled', true);
							$('#kodebarang').prop('disabled', true);
							$('#namabarang').prop('disabled', true);
							$('#diskon').prop('disabled', true);
							$('#aktif').prop('disabled', true);

							$('#update').prop("disabled", true);
							$('#save').prop("disabled", true);
						} else {
							Toast.fire({
								icon: 'error',
								title: data.message
							});
						}
					}
				}), false;
			}
		})

		document.getElementById("clear").addEventListener("click", function(event) {
			event.preventDefault();
			ClearScreen();
		});
		ClearScreen();
	});
</script>
