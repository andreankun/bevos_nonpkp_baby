<script type="text/javascript">
    $(document).ready(function() {

        /* Function ClearScreen */
        function ClearScreen() {
            $('#update').prop("disabled", true);
            $('#save').prop("disabled", false);
            $('#aktif').prop("disabled", false);

            $('#nomor').prop('disabled', false).val("");
            $('#nama').prop('disabled', false).val("");
            $('#alamat').prop('disabled', false).val("");
            $('#alamatpengiriman').prop('disabled', false).val("");
            $('#nohp').prop('disabled', false).val(0);
            $('#notlp').prop('disabled', false).val(0);
            $('#namatoko').prop('disabled', false).val("");
            $('#kdcabang').val("<?= $this->session->userdata('mycabang') ?>");

            // $('input[name="statuspkp"]:checked').prop('checked', true);
            $('input[name="statusppn"]:checked').prop('checked', false);
            $('input[type="radio"]').prop('disabled', false);

            $('#npwp').prop('disabled', true).val("");
            $('#ktp').prop('disabled', false).val("");
            $('#email').prop('disabled', false).val("-");
            $('#status').prop('disabled', false).val("Regular");
            $('#limit').prop('disabled', false).val(0);

            $('#top').prop('readonly', true).val("");
            $('#kode').prop('readonly', true).val("");
            $('#kodepos').prop('readonly', true).val("");
            $('#gruppelanggan').prop('readonly', true).val("");
            $('#nocoa').prop('readonly', true).val("");
            $('#kodeslm').prop('readonly', true).val("");
            $('#namaslm').prop('readonly', true).val("");
            $('#alamatcek').prop('checked', false);
            $('#alamatcek').prop('disabled', false);
            $('#tb_detail').empty();
            $('#rekening').val("");
            $('#namarekening').val("");
            $("#yes_ppn").prop('checked', true);
            $("#no").prop('checked', true);

            $('#npwp').mask('00.000.000.0-000.000', {
                placeholder: "00.000.000.0-000.000"
            });

			$('#carirekening').show();

            $('.aktif').hide();
        };

        $('#status, #limit').change(function() {
            var status = $('#status').val();

            if (status == 'VVIP') {
                $('#limit').prop('disabled', true);
                $('#limit').val(0);
            } else if (status == 'Regular') {
                $('#limit').prop('disabled', false);
                // $('#limit').val(0);
            } else {
                $('#limit').prop('disabled', false);
                $('#limit').val("");
            }
        })

        function AlamatSama() {
            if ($("#alamatcek").is(":checked")) {
                $('#alamatpengiriman').val($('#alamat').val());
                $('#alamatpengiriman').prop("disabled", true);
            } else {
                $('#alamatpengiriman').prop("disabled", false).val("");
            }
        }

        $('#alamatcek').click(function() {
            AlamatSama();
        })

        $('input[name="statuspkp"], #npwp, #ktp').change(function() {
            var statupkp = $('input[name="statuspkp"]:checked').val();
            if (statupkp == 'true') {
                $('#ktp').prop('disabled', true);
                $('#npwp').prop('disabled', false);
            } else if (statupkp == 'false') {
                $('#ktp').prop('disabled', false);
                $('#npwp').prop('disabled', true);
            }
        })

        /* Maxlength Nomor HP */
        $('#nohp[max]:not([max=""])').on('input', function(event) {
            var $this = $(this);
            var maxlength = $this.attr('max').length;
            var value = $this.val();
            if (value && value.length >= maxlength) {
                $this.val(value.substr(0, maxlength));
            }
        })

        /* Maxlength Nomor Telepon */
        $('#notlp[max]:not([max=""])').on('input', function(event) {
            var $this = $(this);
            var maxlength = $this.attr('max').length;
            var value = $this.val();
            if (value && value.length >= maxlength) {
                $this.val(value.substr(0, maxlength));
            }
        })

        /* Maxlength Nomor KTP */
        $('#ktp[max]:not([max=""])').on('input', function(event) {
            var $this = $(this);
            var maxlength = $this.attr('max').length;
            var value = $this.val();
            if (value && value.length >= maxlength) {
                $this.val(value.substr(0, maxlength));
            }
        })

        /* Format Rupiah */
        function FormatRupiah(angka, prefix) {
            if (!angka) {
                return '';
            }
            var vangka = angka.toString();
            var number_string = vangka.replace(/[^.\d]/g, '').replace(/[^\w\s]/gi, '').toString(),
                split = number_string.split('.'),
                sisa = split[0].length % 3,
                rupiah = split[0].substr(0, sisa),
                ribuan = split[0].substr(sisa).match(/\d{3}/gi);

            // tambahkan titik jika yang di input sudah menjadi angka ribuan
            if (ribuan) {
                separator = sisa ? ',' : '';
                rupiah += separator + ribuan.join(',');
            }

            rupiah = split[1] != undefined ? rupiah + '.' + split[1] : rupiah;
            return prefix == undefined ? rupiah : (rupiah ? rupiah : '');
        };

        $('#limit').keyup(function() {
            $(this).val(FormatRupiah($(this).val()));
        })

        /* Validasi Kosong */
        function ValidasiSave(datadetail) {
            var nama = $('#nama').val();
            var kodepos = $('#kodepos').val();
            var alamat = $('#alamat').val();
            var alamatpengiriman = $('#alamatpengiriman').val();
            var nohp = $('#nohp').val();
            var notlp = $('#notlp').val();
            var npwp = $('#npwp').val();
            var ktp = $('#ktp').val();
            var email = $('#email').val();
            var top = $('#top').val();
            var status = $('#status').val();
            var limit = $('#limit').val();
            var gruppelanggan = $('#gruppelanggan').val();
            var nocoa = $('#nocoa').val();
            // var salesman = $('#kodeslm').val();

            if (nama == '' || nama == 0) {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Nama tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#nama').focus();
                var result = false;
            }
            // else if (kodepos == '' || kodepos == 0) {
            //     Swal.fire({
            //         title: 'Informasi',
            //         icon: 'info',
            //         html: 'Kode POS tidak boleh kosong.',
            //         showCloseButton: true,
            //         width: 350,
            //     });
            //     $('#kodepos').focus();
            //     var result = false;
            // } 
            else if (alamat == '' || alamat == 0) {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Alamat tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#alamat').focus();
                var result = false;
            } else if (alamatpengiriman == '' || alamatpengiriman == 0) {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Alamat Pengiriman tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#alamatpengiriman').focus();
                var result = false;
            }
            // else if (nohp == '' || nohp == 0) {
            //     Swal.fire({
            //         title: 'Informasi',
            //         icon: 'info',
            //         html: 'No. HP tidak boleh kosong.',
            //         showCloseButton: true,
            //         width: 350,
            //     });
            //     $('#nohp').focus();
            //     var result = false;
            // } 
            // else if (notlp == '' || notlp == 0) {
            //     Swal.fire({
            //         title: 'Informasi',
            //         icon: 'info',
            //         html: 'No. Telp tidak boleh kosong.',
            //         showCloseButton: true,
            //         width: 350,
            //     });
            //     $('#notlp').focus();
            //     var result = false;
            // } 
            // else if (email == '' || email == 0) {
            //     Swal.fire({
            //         title: 'Informasi',
            //         icon: 'info',
            //         html: 'E-mail tidak boleh kosong.',
            //         showCloseButton: true,
            //         width: 350,
            //     });
            //     $('#email').focus();
            //     var result = false;
            // } 
            else if (top == '' || top == 0) {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'TOP tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#top').focus();
                var result = false;
            } else if (gruppelanggan == '' || gruppelanggan == 0) {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Grup Pelanggan tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#gruppelanggan').focus();
                var result = false;
            } else if (nocoa == '' || nocoa == 0) {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'No Account tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#nocoa').focus();
                var result = false;
            }
            // else if (status == 'Regular') {
            //     Swal.fire({
            //         title: 'Informasi',
            //         icon: 'info',
            //         html: 'Limit tidak boleh kosong atau 0.',
            //         showCloseButton: true,
            //         width: 350,
            //     });
            //     $('#limit').focus();
            //     var result = false;
            // } 
            // else if (datadetail.length == 0 || datadetail.length == '0') {
            //     Swal.fire({
            //         title: 'Informasi',
            //         icon: 'info',
            //         html: 'Data Detail tidak boleh kosong.',
            //         showCloseButton: true,
            //         width: 350,
            //     });
            //     var result = false;
            // }
            else {
                var result = true;
            }

            return result;
        }

        function ValidasiSaveDetail(kodesalesman) {
            var table = document.getElementById('t_detail');
            var kodesalesman = $('#kodeslm').val();
            var nama = $('#namaslm').val();
            if (kodesalesman == "" || kodesalesman == 0) {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Kode Salesman tidak boleh kosong',
                    showCloseButton: true,
                    width: 350
                });
                $('#kodeslm').focus();
                return "gagal"
            } else if (nama == "" || nama == 0) {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Nama Salesman tidak boleh kosong',
                    showCloseButton: true,
                    width: 350
                });
                $('#namaslm').focus();
                return "gagal"
            } else {
                for (var r = 1, n = table.rows.length; r < n; r++) {
                    var string = "";
                    for (var c = 0, m = table.rows[r].cells.length; c < m; c++) {
                        if (c == 0) {
                            if (table.rows[r].cells[c].innerHTML == kodesalesman) {
                                Swal.fire({
                                    title: 'Information',
                                    icon: 'info',
                                    html: 'Data ini sudah diinput',
                                    showCloseButton: true,
                                    width: 350,
                                });
                                return "gagal";
                            }
                        }
                    }
                }
            }
            return "sukses"
        }

        /* Declare Toast */
        const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000,
            timerProgressBar: true,
            didOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
            }
        });

        function InsertDataDetail(kodesalesman, namasalesman) {
            var row = "";
            row =
                '<tr id="' + kodesalesman + '">' +
                '<td>' + kodesalesman + '</td>' +
                '<td>' + namasalesman + '</td>' +
                '<td style="text-align: center; width: 200px;">' +
                // '<button data-table="' + kodesalesman + '" data-toggle="modal" data-target="#changeqty" class="editdetail btn btn-sm btn-success m-1 p-1"><i class="fas fa-edit"></i> Edit</button>' +
                '<button data-table="' + kodesalesman + '" class="deldetail btn btn-sm btn-danger m-1 p-1"><i class="fa fa-trash mr-1"></i> Delete</button>' +
                '</td>' +
                '</tr>';
            $('#tb_detail').append(row);
        };

        $("#adddetail").click(function() {
            var kodesalesman = $('#kodeslm').val();
            var namasalesman = $('#namaslm').val();
            if (ValidasiSaveDetail(kodesalesman) == "sukses") {
                var row = "";
                row =
                    '<tr id="' + kodesalesman + '">' +
                    '<td>' + kodesalesman + '</td>' +
                    '<td>' + namasalesman + '</td>' +
                    '<td style="text-align: center; width: 200px;">' +
                    // '<button data-table="' + kodesalesman + '" data-toggle="modal" data-target="#changeqty" class="editdetail btn btn-sm btn-success m-1 p-1"><i class="fas fa-edit"></i> Edit</button>' +
                    '<button data-table="' + kodesalesman + '" class="deldetail btn btn-sm btn-danger m-1 p-1"><i class="fa fa-trash mr-1"></i> Delete</button>' +
                    '</td>' +
                    '</tr>';
                $('#tb_detail').append(row);
                $('#kodeslm').val("");
                $('#namaslm').val("");
            }
        });

        $(document).on('click', '.editdetail', function() {
            var table = document.getElementById('t_detail');
            var tr = document.getElementById($(this).attr("data-table"));
            var td = tr.getElementsByTagName("td");
            for (var c = 0, m = table.rows[0].cells.length; c < m - 1; c++) {
                var chgth = table.rows[0].cells[c].innerHTML.replace(/ /g, '').replace(/\s/g, '').toLowerCase();
                var valuetd = td[c].innerHTML;
                $('#chg' + chgth).val(valuetd);
            }
        });

        $(document).on('click', '#changedetail', function() {
            var kodesalesman = $('#chgkode').val();
            var namasalesman = $('#chgnama').val();
            $('#' + kodesalesman).remove();
            InsertDataDetail(kodesalesman, namasalesman);
        })

        $(document).on('click', '.deldetail', function() {
            var id = $(this).attr("data-table");
            $('#' + id).remove();
        });

        function AmbilDataDetail() {
            var table = document.getElementById('t_detail');
            var arr2 = [];
            for (var r = 1, n = table.rows.length; r < n; r++) {
                var string = "";
                for (var c = 0, m = table.rows[r].cells.length; c < m - 1; c++) {
                    if (c == 0) {
                        string = "{" + table.rows[0].cells[c].innerHTML + " : '" + table.rows[r].cells[c].innerHTML + "'";
                    } else {
                        string = string + ", " + table.rows[0].cells[c].innerHTML + " : '" + table.rows[r].cells[c].innerHTML + "'";
                    }
                }
                string = string + "}";
                var obj = JSON.stringify(eval('(' + string + ')'));
                var arr = $.parseJSON(obj);
                arr2.push(arr);
            }
            return arr2;
        };

        /* Cari Data Grup Customer*/
        document.getElementById("carigruppelanggan").addEventListener("click", function(event) {
            event.preventDefault();
            $('#t_gruppelanggan').DataTable({
                "destroy": true,
                "searching": true,
                "processing": true,
                "serverSide": true,
                "lengthChange": true,
                "pageLength": 5,
                "lengthMenu": [5, 10, 25, 50],
                "order": [],
                "ajax": {
                    "url": "<?= base_url('masterdata/GrupCustomer/CariDataGrupCustomer'); ?>",
                    "method": "POST",
                    "data": {
                        nmtb: "glbm_grupcustomer",
                        field: {
                            kode: "kode",
                            nama: "nama"
                        },
                        sort: "kode",
                        where: {
                            kode: "kode",
                            nama: "nama"
                        },
                        value: "aktif = true"
                    },
                }
            });
        });

        /*Get Data Grup Customer*/
        $(document).on('click', ".searchgrupcustomer", function() {
            var kode = $(this).attr("data-id");;
            $.ajax({
                url: "<?= base_url('masterdata/GrupCustomer/DataGrupCustomer'); ?>",
                method: "POST",
                dataType: "json",
                async: false,
                data: {
                    kode: kode
                },
                success: function(data) {
                    for (var i = 0; i < data.length; i++) {
                        $('#gruppelanggan').val(data[i].kode.trim());
                    }
                }
            }, false);
        });

        /* Cari Data Barang */
        document.getElementById('caricabang').addEventListener('click', function(event) {
            event.preventDefault();
            $('#t_cabang').DataTable({
                "destroy": true,
                "searching": true,
                "processing": true,
                "serverSide": true,
                "lengthChange": true,
                "pageLength": 5,
                "lengthMenu": [5, 10, 25, 50],
                "order": [],
                "ajax": {
                    "url": "<?= base_url('masterdata/Cabang/CariDataCabang'); ?>",
                    "method": "POST",
                    "data": {
                        nmtb: "glbm_cabang",
                        field: {
                            kode: "kode",
                            nama: "nama",
                            alamat: "alamat",
                        },
                        sort: "kode",
                        where: {
                            kode: "kode",
                            nama: "nama",
                        },
                        value: "aktif = true"
                    },
                }
            });
        });

        /* Get Data Barang */
        $(document).on('click', ".searchcabang", function() {
            var kode = $(this).attr("data-id");
            $.ajax({
                url: "<?= base_url('masterdata/Cabang/DataCabang'); ?>",
                method: "POST",
                dataType: "json",
                async: false,
                data: {
                    kode: kode
                },
                success: function(data) {
                    for (var i = 0; i < data.length; i++) {
                        $('#kodecabang').val(data[i].kode.trim());
                    }
                }
            }, false);
        });

        /* Cari Data KodePOS*/
        document.getElementById("carikodepos").addEventListener("click", function(event) {
            event.preventDefault();
            $('#t_kodepos').DataTable({
                "destroy": true,
                "searching": true,
                "processing": true,
                "serverSide": true,
                "lengthChange": true,
                "pageLength": 5,
                "lengthMenu": [5, 10, 25, 50],
                "order": [],
                "ajax": {
                    "url": "<?= base_url('masterdata/Kodepos/CariDataKodePos'); ?>",
                    "method": "POST",
                    "data": {
                        nmtb: "glbm_kodepos",
                        field: {
                            kode: "kode",
                            kodepos: "kodepos",
                            // provinsi: "provinsi",
                            kota: "kota",
                            kecamatan: "kecamatan",
                            kelurahan: "kelurahan",
                        },
                        sort: "kode",
                        where: {
                            kode: "kode",
                            kota: "kota",
                            kecamatan: "kecamatan",
                            kelurahan: "kelurahan",
                        },
                        value: "aktif = true"
                    },
                }
            });
        });

        /*Get Data KodePOS*/
        $(document).on('click', ".searchkodepos", function() {
            var kode = $(this).attr("data-id");;
            $.ajax({
                url: "<?= base_url('masterdata/Kodepos/DataKodePos'); ?>",
                method: "POST",
                dataType: "json",
                async: false,
                data: {
                    kode: kode
                },
                success: function(data) {
                    for (var i = 0; i < data.length; i++) {
                        $('#kode').val(data[i].kode.trim());
                        $('#kodepos').val(data[i].kodepos.trim());
                    }
                }
            }, false);
        });



        /* Cari Data Top*/
        document.getElementById("caritop").addEventListener("click", function(event) {
            event.preventDefault();
            $('#t_top').DataTable({
                "destroy": true,
                "searching": true,
                "processing": true,
                "serverSide": true,
                "lengthChange": true,
                "pageLength": 5,
                "lengthMenu": [5, 10, 25, 50],
                "order": [],
                "ajax": {
                    "url": "<?= base_url('masterdata/Top/CariDataTop'); ?>",
                    "method": "POST",
                    "data": {
                        nmtb: "glbm_top",
                        field: {
                            kode: "kode",
                            keterangan: "keterangan",
                            jumlah: "jumlah",
                            kota: "kota"
                        },
                        sort: "kode",
                        where: {
                            kode: "kode",
                            keterangan: "keterangan",
                            kota: "kota"
                        },
                        value: "aktif = true"
                    },
                }
            });
        });

        /*Get Data Top*/
        $(document).on('click', ".searchtop", function() {
            var kode = $(this).attr("data-id");;
            $.ajax({
                url: "<?= base_url('masterdata/Top/DataTop'); ?>",
                method: "POST",
                dataType: "json",
                async: false,
                data: {
                    kode: kode
                },
                success: function(data) {
                    for (var i = 0; i < data.length; i++) {
                        $('#top').val(data[i].kode.trim());
                    }
                }
            }, false);
        });

        /* Cari Data Rekening Transfer*/
        document.getElementById("carirekening").addEventListener("click", function(event) {
            event.preventDefault();
            $('#t_rekening').DataTable({
                "destroy": true,
                "searching": true,
                "processing": true,
                "serverSide": true,
                "lengthChange": true,
                "pageLength": 5,
                "lengthMenu": [5, 10, 25, 50],
                "order": [],
                "ajax": {
                    "url": "<?= base_url('masterdata/Rekening_transfer/CariDataRekening'); ?>",
                    "method": "POST",
                    "data": {
                        nmtb: "cari_account",
                        field: {
                            nomor: "nomor",
                            nama: "nama",
                            norekening: "norekening",
                            namacabang: "namacabang",

                        },
                        sort: "nomor",
                        where: {
                            nomor: "nomor",
                            nama: "nama",
                            norekening: "norekening"
                        },
                        value: "aktif = true"
                        // value: "aktif = true AND kode_cabang = '" + $('#cabang').val() + "'"
                    },
                }
            });
        });

        /*Get Data Rekening Transfer*/
        $(document).on('click', ".searchrekening", function() {
            var norek = $(this).attr("data-id");;
            dataRekeningDetail(norek);
        });

        function dataRekeningDetail(norek) {
            $.ajax({
                url: "<?= base_url('masterdata/Rekening_transfer/DataRekening'); ?>",
                method: "POST",
                dataType: "json",
                async: false,
                data: {
                    norek: norek
                },
                success: function(data) {
                    console.log(data);
                    for (var i = 0; i < data.length; i++) {
                        $('#nocoa').val(data[i].nomor.trim());
                        $('#rekening').val(data[i].norekening.trim());
                        $('#namarekening').val(data[i].nama.trim());
                        $('#namabisnis').val(data[i].namabisnis.trim());

                    }
                }
            }, false);
        }

        /*Cari Data Salesman*/
        document.getElementById("carisalesman").addEventListener("click", function(event) {
            event.preventDefault();
            $('#t_salesman').DataTable({
                "destroy": true,
                "searching": true,
                "processing": true,
                "serverSide": true,
                "lengthChange": true,
                "pageLength": 5,
                "lengthMenu": [5, 10, 25, 50],
                "order": [],
                "ajax": {
                    "url": "<?= base_url('masterdata/Salesman/CariDataSalesman'); ?>",
                    "method": "POST",
                    "data": {
                        nmtb: "glbm_salesman",
                        field: {
                            kode: "kode",
                            nama: "nama"
                        },
                        sort: "kode",
                        where: {
                            kode: "kode",
                            nama: "nama"
                        },
                        value: "aktif = true AND kodecabang = '" + $('#cabang').val() + "'"
                    }
                }
            });
        });

        /*get data salesman */
        $(document).on('click', ".searchsalesman", function() {
            var kode = $(this).attr('data-id');
            $.ajax({
                url: "<?= base_url('masterdata/Salesman/DataSalesman') ?>",
                method: "POST",
                dataType: "json",
                async: false,
                data: {
                    kode: kode
                },
                success: function(data) {
                    for (var i = 0; i < data.length; i++) {
                        $('#kodeslm').val(data[i].kode.trim());
                        $('#namaslm').val(data[i].nama.trim());
                    }
                }
            });
        });

        /* Save */
        document.getElementById('save').addEventListener("click", function(event) {
            event.preventDefault();
            var nomor = $('#nomor').val();
            var nama = $('#nama').val();
            var kodepos = $('#kodepos').val();
            var alamat = $('#alamat').val();
            var alamatpengiriman = $('#alamatpengiriman').val();
            var nohp = $('#nohp').val();
            var notlp = $('#notlp').val();
            var namatoko = $('#namatoko').val();
            var statuspkp = $('input[name="statuspkp"]:checked').val();
            var npwp = $('#npwp').val();
            var statusppn = $('input[name="statusppn"]:checked').val();
            var ktp = $('#ktp').val();
            var email = $('#email').val();
            var rekening = $('#rekening').val();
            var top = $('#top').val();
            var status = $('#status').val();
            var limit = $('#limit').val();
            var gruppelanggan = $('#gruppelanggan').val();
            var nocoa = $('#nocoa').val();
            var cabang = $('#kdcabang').val();
            var datadetail = AmbilDataDetail();

            if (ValidasiSave(datadetail) == true) {
                $.ajax({
                    url: "<?= base_url("masterdata/Customer/Save") ?>",
                    method: "POST",
                    dataType: "json",
                    async: true,
                    data: {
                        nomor: nomor,
                        nama: nama,
                        kodepos: kodepos,
                        alamat: alamat,
                        alamatpengiriman: alamatpengiriman,
                        nohp: nohp,
                        notlp: notlp,
                        statuspkp: statuspkp,
                        statusppn: statusppn,
                        npwp: npwp,
                        ktp: ktp,
                        namatoko: namatoko,
                        email: email,
                        rekening: rekening,
                        top: top,
                        status: status,
                        limit: limit,
                        gruppelanggan: gruppelanggan,
                        nocoa: nocoa,
                        cabang: cabang,
                        datadetail: datadetail
                    },
                    success: function(data) {
                        console.table(data);
                        if (data.nomor != "") {
                            Toast.fire({
                                icon: 'success',
                                title: data.message
                            });
                            $('#nomor').val(data.nomor);
                            $('#nama').prop('disabled', true);
                            $('#alamat').prop('disabled', true);
                            $('#alamatpengiriman').prop('disabled', true);
                            $('#nohp').prop('disabled', true);
                            $('#notlp').prop('disabled', true);
                            $('#namatoko').prop('disabled', true);
                            $('input[name="statuspkp"]').prop('disabled', true);
                            $('#npwp').prop('disabled', true);
                            $('#ktp').prop('disabled', true);
                            $('#email').prop('disabled', true);
                            $('#status').prop('disabled', true);
                            $('#limit').prop('disabled', true);
                            $('#alamatcek').prop('disabled', true);

                            $('#update').prop("disabled", true);
                            $('#save').prop("disabled", true);
                            ClearScreen();
                        } else {
                            Toast.fire({
                                icon: 'error',
                                title: data.message
                            });
                        }
                    }
                }, false)
            }

        })

        /* Cari Data Customer*/
        document.getElementById("find").addEventListener("click", function(event) {
            event.preventDefault();
            $('#tb_detail').empty();
            $('#t_pelanggan').DataTable({
                "destroy": true,
                "searching": true,
                "processing": true,
                "serverSide": true,
                "lengthChange": true,
                "pageLength": 5,
                "lengthMenu": [5, 10, 25, 50],
                "order": [],
                "autoWidth": false,
                "ajax": {
                    "url": "<?= base_url('masterdata/Customer/CariDataCustomer'); ?>",
                    "method": "POST",
                    "data": {
                        nmtb: "cari_customer",
                        field: {
                            nomor: "nomor",
                            aktif: "aktif",
                            nama_toko: "nama_toko",
                            nama: "nama",
                            limit: "limit",
                            status: "status",
                            namagrup: "namagrup"
                        },
                        sort: "nomor",
                        where: {
                            nomor: "nomor",
                            nama: "nama",
                            nama_toko: "nama_toko"
                        },
                        value: "kodecabang = '" + $('#cabang').val() + "'"
                    },
                },
                "columnDefs": [{
                        "targets": 2,
                        "data": "aktif",
                        "render": function(data, type, row, meta) {
                            return (row[2] == 't') ? 'Aktif' : 'Tidak Aktif';
                        }
                    },
                    {
                        "targets": 5,
                        "data": "limit",
                        "render": function(data, type, row, meta) {
                            return (row[5] == 0) ? 0 : FormatRupiah(row[5].toString());
                        }
                    }
                ],
            });
        });

        /*Get Data Customer*/
        $(document).on('click', ".searchcustomer", function() {
            var nomor = $(this).attr("data-id");;
            $.ajax({
                url: "<?= base_url('masterdata/Customer/DataCustomer'); ?>",
                method: "POST",
                dataType: "json",
                async: false,
                data: {
                    nomor: nomor
                },
                success: function(data) {
                    for (var i = 0; i < data.length; i++) {
                        $('#nomor').val(data[i].nomor.trim());
                        $('#nama').val(data[i].nama.trim());
                        $('#kode').val(data[i].kodepos.trim());
                        $('#kodepos').val(data[i].kodepos.trim());
                        $('#alamat').val(data[i].alamat.trim());
                        $('#alamatpengiriman').val(data[i].pengiriman.trim());
                        $('#nohp').val(data[i].nohp.trim());
                        $('#notlp').val(data[i].notelp.trim());
                        $('#namatoko').val(data[i].nama_toko.trim());
                        $('#npwp').val(data[i].npwp.trim());
                        $('#ktp').val(data[i].ktp.trim());
                        $('#email').val(data[i].email.trim());
                        $('#top').val(data[i].top.trim());
                        $('#limit').val(FormatRupiah(data[i].limit.trim()));
                        $('#gruppelanggan').val(data[i].gruppelanggan.trim());
                        $('#nocoa').val(data[i].rekeningtf.trim());
                        dataRekeningDetail(data[i].rekeningtf.trim())
                        // $('#kodeslm').val(data[i].kodesalesman.trim());
                        if (data[i].status == 'Regular') {
                            $('#status').val(data[i].status.trim());
                            $('#limit').prop('disabled', false);
                        } else {
                            $('#status').val(data[i].status.trim());
                            $('#limit').prop('disabled', true);
                        }
                        if (data[i].statuspkp.trim() == 't') {
                            $('#yes').prop('checked', true);
                            $('#ktp').prop('disabled', true);
                        } else {
                            $('#no').prop('checked', true);
                            $('#npwp').prop('disabled', true);
                        }
                        if ($('#alamat').val() == $('#alamatpengiriman').val()) {
                            $('#alamatpengiriman').prop('disabled', true);
                            $('#alamatcek').prop('checked', true);
                        } else {
                            $('#alamatpengiriman').prop('disabled', false);
                            $('#alamatcek').prop('checked', false);
                        }
                        if (data[i].aktif.trim() == 't') {
                            $('#aktif').prop('checked', true);
                        } else {
                            $('#tidak_aktif').prop('checked', true);
                        }
                        if (data[i].statusppn.trim() == 't') {
                            $('#yes_ppn').prop('checked', true);
                        } else {
                            $('#no_ppn').prop('checked', true);
                        }
						// if(data[i].outstandingar.trim() > 0){
						// 	$("#carirekening").hide();
						// } else {
						// 	$("#carirekening").show();
						// }
                    }
                    GetDataDetail(nomor);
                    $('#statuspkp').prop('disabled', false);
                    $('#save').prop('disabled', true);
                    $('#update').prop('disabled', false);
                    $('#nomor').prop('disabled', true);
                    $('.aktif').show();
                }
            }, false);
        });

        function GetDataDetail(nomor) {
            $.ajax({
                url: "<?= base_url('masterdata/Customer/DataCustomerDetail'); ?>",
                method: "POST",
                dataType: "json",
                async: false,
                data: {
                    nomor: nomor
                },
                success: function(data) {
                    for (var i = 0; i < data.length; i++) {
                        // var kode = data[i].kode;
                        var kodesalesman = data[i].kodesalesman;
                        var namasalesman = data[i].namasalesman.trim();

                        InsertDataDetail(kodesalesman, namasalesman);
                    }
                }
            });
        };

        /* Update */
        document.getElementById('update').addEventListener("click", function(event) {
            event.preventDefault();
            var nomor = $('#nomor').val();
            var nama = $('#nama').val();
            var kodepos = $('#kodepos').val();
            var alamat = $('#alamat').val();
            var alamatpengiriman = $('#alamatpengiriman').val();
            var nohp = $('#nohp').val();
            var notlp = $('#notlp').val();
            var namatoko = $('#namatoko').val();
            var statuspkp = $('input[name="statuspkp"]:checked').val();
            var npwp = $('#npwp').val();
            var statusppn = $('input[name="statusppn"]:checked').val();
            var ktp = $('#ktp').val();
            var rekening = $('#rekening').val();
            var email = $('#email').val();
            var top = $('#top').val();
            var status = $('#status').val();
            var limit = $('#limit').val();
            var gruppelanggan = $('#gruppelanggan').val();
            var nocoa = $('#nocoa').val();
            var cabang = $('#kdcabang').val();
            var aktif = $('input[name="aktif"]:checked').val();
            var datadetail = AmbilDataDetail();

            if (ValidasiSave(datadetail) == true) {
                $.ajax({
                    url: "<?= base_url("masterdata/Customer/Update") ?>",
                    method: "POST",
                    dataType: "json",
                    async: true,
                    data: {
                        nomor: nomor,
                        nama: nama,
                        kodepos: kodepos,
                        alamat: alamat,
                        alamatpengiriman: alamatpengiriman,
                        nohp: nohp,
                        notlp: notlp,
                        statuspkp: statuspkp,
                        statusppn: statusppn,
                        npwp: npwp,
                        ktp: ktp,
                        rekening: rekening,
                        namatoko: namatoko,
                        email: email,
                        top: top,
                        status: status,
                        limit: limit,
                        gruppelanggan: gruppelanggan,
                        nocoa: nocoa,
                        cabang: cabang,
                        aktif: aktif,
                        datadetail: datadetail
                    },
                    success: function(data) {
                        if (data.nomor != "") {
                            Toast.fire({
                                icon: 'success',
                                title: data.message
                            });
                            $('#nomor').val(data.nomor);
                            $('#nama').prop('disabled', true);
                            $('#alamat').prop('disabled', true);
                            $('#alamatpengiriman').prop('disabled', true);
                            $('#nohp').prop('disabled', true);
                            $('#notlp').prop('disabled', true);
                            $('input[name="statuspkp"]').prop('disabled', true);
                            $('#npwp').prop('disabled', true);
                            $('#ktp').prop('disabled', true);
                            $('#email').prop('disabled', true);
                            $('#status').prop('disabled', true);
                            $('#limit').prop('disabled', true);
                            $('#alamatcek').prop('disabled', true);

                            $('#update').prop("disabled", true);
                            $('#save').prop("disabled", true);
                            ClearScreen()
                        } else {
                            Toast.fire({
                                icon: 'error',
                                title: data.message
                            });
                        }
                    }

                }, false)
            }
        });

        /* Clear */
        document.getElementById("clear").addEventListener("click", function(event) {
            event.preventDefault();
            ClearScreen();
        });

        ClearScreen();

    });
</script>
