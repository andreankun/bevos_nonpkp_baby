<?php

header("Content-type: application/vnd-ms-excel");

header("Content-Disposition: attachment; filename=Barang.xls");

header("Pragma: no-cache");

header("Expires: 0");

?>

<table border="1" width="100%">

    <thead>

        <tr>

            <th>Kode Cabang</th>
            <th>Kode Barang</th>
            <th>Nama Barang</th>
            <th>Merk</th>
            <th>Harga Jual</th>
            <th>Harga Beli</th>
            <th>Lokasi Barang</th>
            <th>Komisi Sales</th>

        </tr>

    </thead>

    <tbody>

        <?php $i = 1;
        foreach ($databarang  as $datatampil) { ?>

            <tr>

                <td><?php echo $datatampil->kodecabang; ?></td>
                <td><?php echo $datatampil->kodebarang; ?></td>
                <td><?php echo $datatampil->namabarang; ?></td>
                <td><?php echo $datatampil->kodemerk; ?></td>
                <td><?php echo $datatampil->hargajual; ?></td>
                <td><?php echo $datatampil->hargabeli; ?></td>
                <td><?php echo $datatampil->lokasi; ?></td>
                <td><?php echo $datatampil->komisi; ?> % </td>

            </tr>

        <?php $i++;
        } ?>

    </tbody>

</table>
