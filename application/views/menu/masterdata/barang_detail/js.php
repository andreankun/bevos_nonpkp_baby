<script type="text/javascript">
	$(document).ready(function() {

		function LoadSalesman() {
			$.ajax({
				url: "<?= base_url('masterdata/Salesman/LoadSalesman') ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					cabang: $('#cabang').val()
				},
				success: function(data) {
					if (data.length > 0) {
						for (var i = 0; i < data.length; i++) {
							InsertDataSalesDetail(data[i].kode, data[i].nama);
						}
					}
				}
			}, false);
		}

		/* Function ClearScreen */
		LoadSalesman();

		function ClearScreen() {
			// $('#tb_detail').empty();
			// $('#kodedetail').prop('disabled', false).val("");
			$('#kodecabang').prop('disabled', false).val("<?= $this->session->userdata('mycabang'); ?>");
			$('#kodebarang').prop('readonly', true).val("");
			$('#namabarang').prop('readonly', true).val("");
			$('#lokasi').prop('disabled', false).val("");
			$('#hargabeli').prop('disabled', false).val(0);
			$('#hargajual').prop('disabled', false).val("");
			// $('#kondisi1').prop('disabled', false).val("");
			// $('#kondisi2').prop('disabled', false).val("");
			// $('#discount').prop('disabled', false).val("");
			$('#komisi').prop('disabled', false).val("");
			$('#kodesalesman').prop('disabled', false).val("");
			$('#namasalesman').prop('disabled', false).val("");
			$('#aktif').prop('disabled', false);
			$('#selectall').prop('checked', false);
			$('.checksales').prop('checked', false);

			$('#update').prop('disabled', true);
			$('#save').prop('disabled', false);

			$('.aktif').hide();
			$('#adddetail').show();
			$('#caribarang').show();
			$('#caricabang').show();
			$('#carisalesman').show();

			// var arrData = [];

			// $('#t_detail tr').each(function() {
			// 	var currentRow = $(this);

			// 	// var K_true = currentRow.find('td:eq(2)').text('true');
			// 	var K_false = currentRow.find('td:eq(2)').text('false');
			// 	var obj = {};
			// 	// obj.kondisi_true = K_true;
			// 	obj.kondisi_false = K_false;

			// 	arrData.push(obj);
			// })
		};

		function DisableAction() {
			$('#kodecabang').prop('disabled', true).val("<?= $this->session->userdata('mycabang'); ?>");
			$('#kodebarang').prop('readonly', true).val("");
			$('#namabarang').prop('readonly', true).val("");
			$('#lokasi').prop('disabled', true).val("");
			$('#hargabeli').prop('disabled', true).val(0);
			$('#hargajual').prop('disabled', true).val("");
			$('#komisi').prop('disabled', true).val("");
			$('#kodesalesman').prop('disabled', true).val("");
			$('#namasalesman').prop('disabled', true).val("");
		}

		$('#selectall').on('click', function() {
			if (this.checked) {
				$('.checksales').each(function() {
					this.checked = true;
				});
			} else {
				$('.checksales').each(function() {
					this.checked = false;
				});
			}
		});

		$('.checksales').on('click', function() {
			if ($('.checksales:checked').length == $('.checksales').length) {
				$('#selectall').prop('checked', true);
			} else {
				$('#selectall').prop('checked', false);
			}
		});

		$(document).on('change', "#selectall", function() {
			var arrData = [];

			$('#t_detail tr').each(function() {
				var currentRow = $(this);

				var K_true = currentRow.find('td:eq(2)').text('true');
				// var K_false = currentRow.find('td:eq(2)').text('false');
				var obj = {};
				obj.kondisi_true = K_true;
				// obj.kondisi_false = K_false;

				arrData.push(obj);
			})
			// if(this.checked){
			//     $('#value_'+dataid+'').text('true');
			// } else {
			//     $('#value_'+dataid+'').text('false');
			// }
		});

		$(document).on('change', ".checksales", function() {
			var dataid = $(this).attr("data-id");
			if (this.checked) {
				$('#value_' + dataid + '').text('true');
			} else {
				$('#value_' + dataid + '').text('false');
			}
		});

		function InsertDataSalesDetail(kodesalesman, namasalesman) {
			var row = "";
			row =
				'<tr id="' + kodesalesman + '">' +
				'<td nowrap>' + kodesalesman + '</td>' +
				'<td nowrap>' + namasalesman + '</td>' +
				'<td style="display: none;" id="value_' + kodesalesman + '">false</td>' +
				'<td style="text-align: center;">' +
				'<input type="checkbox" class="checksales" data-id="' + kodesalesman + '" value="true">' +
				'</td>' +
				'</tr>';
			$('#tb_detail').append(row);
		}

		/* Format Rupiah */
		function FormatRupiah(angka, prefix) {
			if (!angka) {
				return '';
			}
			var vangka = angka.toString();
			var number_string = vangka.replace(/[^.\d]/g, '').replace(/[^\w\s]/gi, '').toString(),
				split = number_string.split('.'),
				sisa = split[0].length % 3,
				rupiah = split[0].substr(0, sisa),
				ribuan = split[0].substr(sisa).match(/\d{3}/gi);

			// tambahkan titik jika yang di input sudah menjadi angka ribuan
			if (ribuan) {
				separator = sisa ? ',' : '';
				rupiah += separator + ribuan.join(',');
			}

			rupiah = split[1] != undefined ? rupiah + '.' + split[1] : rupiah;
			return prefix == undefined ? rupiah : (rupiah ? rupiah : '');
		};

		/* DeFormat Rupiah */
		function DeFormatRupiah(angka) {
			var result = angka.replace(/[^\w\s]/gi, '');

			return result;
		};

		/* Format Rupiah Saat Event Keyup */
		$('#hargabeli').keyup(function() {
			$(this).val(FormatRupiah($(this).val()));
		})
		$('#chghargabeli').keyup(function() {
			$(this).val(FormatRupiah($(this).val()));
		})
		$('#hargajual').keyup(function() {
			$(this).val(FormatRupiah($(this).val()));
		})
		$('#hargajual1').keyup(function() {
			$(this).val(FormatRupiah($(this).val()));
		})
		$('#chghargajual').keyup(function() {
			$(this).val(FormatRupiah($(this).val()));
		})

		// $('#diskon').mask('99.99%', {
		// 	reverse: true
		// });

		// $('#diskon').keyup(function() {
		// 	var hargajual = DeFormatRupiah($('#hargajual').val());
		// 	var diskon = $('#diskon').val();

		// 	var discitem = parseFloat(hargajual) * (parseFloat(diskon) / 100);
		// 	$('#discitem').val(FormatRupiah(Math.round(discitem)));
		// })



		/* Validasi Kosong */
		function ValidasiSave(datadetail) {
			// var kode = $('#kodedetail').val();
			var kodecabang = $('#kodecabang').val();
			var kodebarang = $('#kodebarang').val();
			var namabarang = $('#namabarang').val();
			var hargabeli = $('#hargabeli').val();
			var komisi = $('#komisi').val();
			var hargajual = $('#hargajual').val();
			var kondisi1 = $('#kondisi1').val();
			var kondisi2 = $('#kondisi2').val();
			var discount = $('#discount').val();
			var lokasi = $('#lokasi').val();

			if (kodecabang == '' || kodecabang == 0) {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Kode Cabang tidak boleh kosong.',
					showCloseButton: true,
					width: 350,
				});
				$('#kodecabang').focus();
				var result = false;
			} else if (kodebarang == '') {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Kode Barang tidak boleh kosong.',
					showCloseButton: true,
					width: 350,
				});
				$('#kodebarang').focus();
				var result = false;
			} else if (namabarang == '') {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Nama Barang tidak boleh kosong.',
					showCloseButton: true,
					width: 350,
				});
				$('#kodemerk').focus();
				var result = false;
			} else if (komisi == 0) {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Komisi tidak boleh kosong.',
					showCloseButton: true,
					width: 350,
				});
				$('#komisi').focus();
				var result = false;
			} else if (hargajual == "") {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Harga Jual tidak boleh kosong.',
					showCloseButton: true,
					width: 350,
				});
				$('#hargajual').focus();
				var result = false;
			}
			// else if (lokasi == "") {
			// 	Swal.fire({
			// 		title: 'Informasi',
			// 		icon: 'info',
			// 		html: 'Lokasi tidak boleh kosong.',
			// 		showCloseButton: true,
			// 		width: 350,
			// 	});
			// 	$('#lokasi').focus();
			// 	var result = false;
			// }
			else {
				var result = true;
			}

			return result;
		}

		function ValidasiAddDetail(kodesalesman) {
			var table = document.getElementById('t_detail');
			var kodesalesman = $('#kode').val();
			var namasalesman = $('#nama').val();

			if (kodesalesman == '' || kodesalesman == 0) {
				Swal.fire({
					title: 'information',
					icon: 'info',
					html: 'Kode Salesman tidak boleh kosong',
					showCloseButton: true,
					width: 350,
				});
				$('#kode').focus();
				return "gagal";
			} else if (namasalesman == '' || namasalesman == 0) {
				Swal.fire({
					title: 'Information',
					icon: 'info',
					html: 'Nama Salesman tidak boleh kosong',
					showCloseButton: true,
					width: 350,
				});
				$('#nama').focus();
				return "gagal";
			} else {
				for (var r = 1, n = table.rows.length; r < n; r++) {
					var string = "";
					for (var c = 0, m = table.rows[r].cells.length; c < m; c++) {
						if (c == 0) {
							if (table.rows[r].cells[c].innerHTML == kodesalesman) {
								Swal.fire({
									title: 'Information',
									icon: 'info',
									html: 'Data ini sudah diinput',
									showCloseButton: true,
									width: 350,
								});
								return "gagal";
							}
						}
					}
				}
			}
			return "sukses";
		}

		function ValidasiImport() {
			var importdata = $('#importdata').val();
			if (importdata == "") {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Data Yang ingin di import tidak boleh kosong',
					showCloseButton: true,
					width: 350
				});
				$('#importdata').focus();
				var result = false;
			} else {
				var result = true;
			}
			return result;
		}

		/* Validasi Data Add Detail */

		/* Declare Toast */
		const Toast = Swal.mixin({
			toast: true,
			position: 'top-end',
			showConfirmButton: false,
			timer: 3000,
			timerProgressBar: true,
			didOpen: (toast) => {
				toast.addEventListener('mouseenter', Swal.stopTimer)
				toast.addEventListener('mouseleave', Swal.resumeTimer)
			}
		});

		$('#komisi').mask('99.99%', {
			reverse: true
		});

		$('#discount').mask('99.99%', {
			reverse: true
		});

		function InsertDataDetail(kodesalesman, namasalesman, check) {
			var row = "";
			// $('#tb_detail').empty();
			row =
				'<tr id="' + kodesalesman + '">' +
				'<td nowrap>' + kodesalesman + '</td>' +
				'<td nowrap>' + namasalesman + '</td>' +
				'<td style="display: none;" id="value_' + kodesalesman + '">' + check + '</td>' +
				'<td style="text-align: center;">' +
				'<input type="checkbox" class="checksales" data-id="' + kodesalesman + '" value="true" ' + (check == 'true' ? 'checked' : '') + '></td>' +
				'</tr>';
			console.log(row);
			$('#tb_detail').append(row);
		};

		function AmbilDataDetail() {
			var table = document.getElementById('t_detail');
			var arr2 = [];
			for (var r = 1, n = table.rows.length; r < n; r++) {
				var string = "";
				for (var c = 0, m = table.rows[r].cells.length; c < m - 1; c++) {
					if (c == 0) {
						string = "{" + table.rows[0].cells[c].innerHTML + " : '" + table.rows[r].cells[c].innerHTML + "'";
					} else {
						string = string + ", " + table.rows[0].cells[c].innerHTML + " : '" + table.rows[r].cells[c].innerHTML + "'";
					}
				}
				string = string + "}";
				var obj = JSON.stringify(eval('(' + string + ')'));
				var arr = $.parseJSON(obj);
				arr2.push(arr);
			}
			return arr2;
		};

		$("#adddetail").click(function() {
			var kodesalesman = $('#kode').val();
			var namasalesman = $('#nama').val();
			if (ValidasiAddDetail(kodesalesman) == "sukses") {
				var row = "";
				row =
					'<tr id="' + kodesalesman + '">' +
					'<td nowrap>' + kodesalesman + '</td>' +
					'<td nowrap>' + namasalesman + '</td>' +
					'<td nowrap style="text-align: center; width: 200px;">' +
					// '<button data-table="' + kodesalesman + '" data-toggle="modal" data-target="#changeqty" class="editdetail btn btn-sm btn-success mb-0 p-1"><i class="far fa-edit"></i> Edit</button>&nbsp;' +
					'<button data-table="' + kodesalesman + '" class="deldetail btn btn-sm btn-danger m-1 p-1"><i class="fa fa-trash mr-1"></i> Delete</button>' +
					'</td>' +
					'</tr>';
				$('#tb_detail').append(row);
				$('#kode').val("");
				$('#nama').val("");
			}
		});

		$(document).on('click', '.editdetail', function() {
			var table = document.getElementById('t_detail');
			var tr = document.getElementById($(this).attr("data-table"));
			var td = tr.getElementsByTagName("td");
			for (var c = 0, m = table.rows[0].cells.length; c < m - 1; c++) {
				var chgth = table.rows[0].cells[c].innerHTML.replace(/ /g, '').replace(/\s/g, '').toLowerCase();
				var valuetd = td[c].innerHTML;
				$('#chg' + chgth).val(valuetd);
			}
		});

		$(document).on('click', '#changedetail', function() {
			var kodesalesman = $('#chgkode').val();
			var namasalesman = $('#chgnama').val();
			$('#' + kodesalesman).remove();
			InsertDataDetail(kodesalesman, namasalesman);
		})

		$(document).on('click', '.deldetail', function() {
			var id = $(this).attr("data-table");
			$('#' + id).remove();
		});

		function GetDataDetail(kodebarang, cabang) {
			$.ajax({
				url: "<?= base_url('masterdata/BarangDetail/DataSalesmanDetail'); ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					kodebarang: kodebarang,
					cabang: cabang
				},
				success: function(data) {
					// console.log(data);
					if (data.length > 0) {
						for (var i = 0; i < data.length; i++) {
							var kodesalesman = data[i].kodesalesman;
							var namasalesman = data[i].namasalesman.trim();
							var checksales = data[i].checksales.trim();
							var check = '';
							if (checksales == 't') {
								check = 'true';
							} else {
								check = 'false';
							}
							InsertDataDetail(kodesalesman, namasalesman, check);
						}
					} else {
						LoadSalesman();
					}
				}
			});
		};

		/* Cari Data Satuan*/
		document.getElementById("caricabang").addEventListener("click", function(event) {
			event.preventDefault();
			$('#t_cabang').DataTable({
				"destroy": true,
				"searching": true,
				"processing": true,
				"serverSide": true,
				"lengthChange": true,
				"pageLength": 5,
				"lengthMenu": [5, 10, 25, 50],
				"order": [],
				"ajax": {
					"url": "<?= base_url('masterdata/Cabang/CariDataCabang'); ?>",
					"method": "POST",
					"data": {
						nmtb: "glbm_cabang",
						field: {
							kode: "kode",
							nama: "nama",
							// qtysatuan: "qtysatuan",
							// konversi: "konversi",
						},
						sort: "kode",
						where: {
							kode: "kode",
							nama: "nama",
						},
						value: "aktif = true"
					},
				}
			});
		});

		/*Get Data Satuan */
		$(document).on('click', ".searchcabang", function() {
			var kode = $(this).attr("data-id");;
			$.ajax({
				url: "<?= base_url('masterdata/Cabang/DataCabang'); ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					kode: kode
				},
				success: function(data) {
					for (var i = 0; i < data.length; i++) {
						$('#kodecabang').val(data[i].kode.trim());
					}
				}
			}, false);
		});

		/* Cari Data Gudang*/
		/* Cari Data Satuan*/
		// document.getElementById("carisalesman").addEventListener("click", function(event) {
		// 	event.preventDefault();
		// 	$('#t_salesman').DataTable({
		// 		"destroy": true,
		// 		"searching": true,
		// 		"processing": true,
		// 		"serverSide": true,
		// 		"lengthChange": true,
		// 		"pageLength": 5,
		// 		"lengthMenu": [5, 10, 25, 50],
		// 		"order": [],
		// 		"ajax": {
		// 			"url": "<?= base_url('masterdata/Salesman/CariDataSalesman'); ?>",
		// 			"method": "POST",
		// 			"data": {
		// 				nmtb: "glbm_salesman",
		// 				field: {
		// 					kode: "kode",
		// 					nama: "nama",
		// 					// qtysatuan: "qtysatuan",
		// 					// konversi: "konversi",
		// 				},
		// 				sort: "kode",
		// 				where: {
		// 					kode: "kode",
		// 					nama: "nama",
		// 				},
		// 				value: "aktif = true AND kodecabang = '" + $('#cabang').val() + "'"
		// 			},
		// 		}
		// 	});
		// });

		// /*Get Data Satuan */
		// $(document).on('click', ".searchsalesman", function() {
		// 	var kode = $(this).attr("data-id");;
		// 	$.ajax({
		// 		url: "<?= base_url('masterdata/Salesman/DataSalesman'); ?>",
		// 		method: "POST",
		// 		dataType: "json",
		// 		async: false,
		// 		data: {
		// 			kode: kode
		// 		},
		// 		success: function(data) {
		// 			for (var i = 0; i < data.length; i++) {
		// 				$('#kode').val(data[i].kode.trim());
		// 				$('#nama').val(data[i].nama.trim());
		// 			}
		// 		}
		// 	}, false);
		// });
		/* Save */
		document.getElementById('save').addEventListener("click", function(event) {
			event.preventDefault();
			var kodecabang = $('#kodecabang').val();
			var kodebarang = $('#kodebarang').val();
			var namabarang = $('#namabarang').val();
			var hargajual = $('#hargajual').val();
			var komisi = $('#komisi').val();
			var hargabeli = $('#hargabeli').val();
			var kondisi1 = $('#kondisi1').val();
			var kondisi2 = $('#kondisi2').val();
			var discount = $('#discount').val();
			var lokasi = $('#lokasi').val();
			var datadetail = AmbilDataDetail();
			var dataharga = AmbilDataHarga();
			if (ValidasiSave(datadetail) == true && ValidasiSave(dataharga)) {
				$.ajax({
					url: "<?= base_url('masterdata/BarangDetail/Save') ?>",
					method: "POST",
					dataType: "json",
					async: true,
					data: {
						kodecabang: kodecabang,
						kodebarang: kodebarang,
						namabarang: namabarang,
						hargajual: hargajual,
						komisi: komisi,
						hargabeli: hargabeli,
						kondisi1: kondisi1,
						kondisi2: kondisi2,
						discount: discount,
						lokasi: lokasi,
						datadetail: datadetail,
						dataharga: dataharga
					},
					success: function(data) {
						console.log(data);
						if (data.kodedetail != "" && data.kodecabang != "") {
							Toast.fire({
								icon: 'success',
								title: data.message
							});
							$('#adddetail').hide();
							// $('#carisalesman').hide();
							$('#kodecabang').prop('disabled', true);
							$('#kodebarang').prop('disabled', true);
							$('#namabarang').prop('disabled', true);
							$('#hargabeli').prop('disabled', true);
							$('#komisi').prop('disabled', true);
							$('#hargajual').prop('disabled', true);
							$('#kondisi1').prop('disabled', true);
							$('#kondisi2').prop('disabled', true);
							$('#discount').prop('disabled', true);
							$('#lokasi').prop('disabled', true);
							$('#save').prop('disabled', true);
							$('#update').prop('disabled', true);
						} else {
							Toast.fire({
								icon: 'error',
								title: data.message
							});
						}
					}
				}, false)
			}
		})

		document.getElementById('import').addEventListener("click", function(event) {
			event.preventDefault();
			var file_csv = $('#filecsv').prop('files');
			var form_data = new FormData();
			if (file_csv.length > 0) {
				for (let i = 0; i < file_csv.length; i++) {
					let file = file_csv[i];
					form_data.append('file_csv[]', file);
					if (ValidasiImport() == true) {
						$.ajax({
							url: "<?php echo base_url('masterdata/BarangDetail/Import') ?>",
							dataType: "json",
							cache: false,
							contentType: false,
							processData: false,
							data: form_data,
							type: 'post',
							success: function(data) {
								if (data.form_data != "") {
									Toast.fire({
										icon: 'success',
										title: data.message
									});
									$('#filecsv').val("");
								} else {
									Toast.fire({
										icon: 'error',
										title: data.message
									});
								}
							}
						});
					}
				}
			}
		});

		/* Cari Data Satuan*/
		document.getElementById("carimerk").addEventListener("click", function(event) {
			event.preventDefault();
			$('#t_merk').DataTable({
				"destroy": true,
				"searching": true,
				"processing": true,
				"serverSide": true,
				"lengthChange": true,
				"pageLength": 5,
				"lengthMenu": [5, 10, 25, 50],
				"order": [],
				"ajax": {
					"url": "<?= base_url('masterdata/Merk/CariDataMerk'); ?>",
					"method": "POST",
					"data": {
						nmtb: "glbm_merk",
						field: {
							kode: "kode",
							nama: "nama",
							// qtysatuan: "qtysatuan",
							// konversi: "konversi",
						},
						sort: "kode",
						where: {
							kode: "kode",
							nama: "nama",
						},
						value: "aktif = true"
					},
				}
			});
		});

		/*Get Data Satuan */
		$(document).on('click', ".searchmerk", function() {
			var kode = $(this).attr("data-id");;
			$.ajax({
				url: "<?= base_url('masterdata/Merk/DataMerk'); ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					kode: kode
				},
				success: function(data) {
					for (var i = 0; i < data.length; i++) {
						$('#kodemerk').val(data[i].kode.trim());
						// $('#namamerk').val(data[i].nama.trim());
					}
				}
			}, false);
		});

		document.getElementById("caribarang").addEventListener("click", function(event) {
			event.preventDefault();
			$('#t_barang').DataTable({
				"destroy": true,
				"searching": true,
				"processing": true,
				"serverSide": true,
				"lengthChange": true,
				"pageLength": 5,
				"lengthMenu": [5, 10, 25, 50],
				"order": [],
				"ajax": {
					"url": "<?= base_url('masterdata/Barang/CariDataBarang'); ?>",
					"method": "POST",
					"data": {
						nmtb: "glbm_barang",
						field: {
							kode: "kode",
							nama: "nama",
							// barcode1: "barcode1",
							// barcode2: "barcode2",
							kodesatuan: "kodesatuan",
						},
						sort: "kode",
						where: {
							kode: "kode",
							nama: "nama",
							kodesatuan: "kodesatuan"
						},
						// value: "aktif = true"
					},
				}
			});
		});

		$(document).on('click', ".searchbarang", function() {
			var kode = $(this).attr("data-id");
			GetData(kode);
		});

		function GetData(kode) {
			$.ajax({
				url: "<?= base_url('masterdata/Barang/DataBarang'); ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					kode: kode
				},
				success: function(data) {
					for (var i = 0; i < data.length; i++) {
						$('#kodebarang').val(data[i].kode.trim());
						$('#namabarang').val(data[i].nama.trim());
					}
				}
			});
		};

		document.getElementById('find').addEventListener("click", function(event) {
			event.preventDefault();
			$('#tb_detail').empty();
			$('#t_find').DataTable({
				"destroy": true,
				"searching": true,
				"processing": true,
				"serverSide": true,
				"lengthChange": true,
				"pageLength": 5,
				"lengthMenu": [5, 10, 25, 50],
				"order": [],
				"ajax": {
					"url": "<?= base_url('masterdata/BarangDetail/CariDataBarangDetail'); ?>",
					"method": "POST",
					"data": {
						nmtb: "glbm_barangdetail",
						field: {
							kodebarang: "kodebarang",
							namabarang: "namabarang",
							hargajual: "hargajual",
							lokasi: "lokasi",
							aktif: "aktif"

						},
						sort: "kodebarang",
						where: {
							kodebarang: "kodebarang",
							namabarang: "namabarang",
							lokasi: "lokasi",
						},
						value: "kodecabang = '" + $('#cabang').val() + "'"
					},
				},
				"columnDefs": [{
					"targets": 3,
					"render": $.fn.dataTable.render.number(',', '.')
				}, {
					"targets": 5,
					"data": "aktif",
					"render": function(data, type, row, meta) {
						return (row[5] == 't') ? 'Aktif' : 'Tidak Aktif';
					}
				}]
			});
		})

		$(document).on('click', ".searchdetail", function() {
			// ClearScreen();
			var kodebarang = $(this).attr("data-id");
			var cabang = $('#cabang').val();
			$.ajax({
				url: "<?= base_url('masterdata/BarangDetail/DataBarangDetail'); ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					kodebarang: kodebarang,
					cabang: cabang
				},
				success: function(data) {
					for (var i = 0; i < data.length; i++) {
						$('#kodecabang').val(data[i].kodecabang.trim());
						$('#kodebarang').val(data[i].kodebarang.trim());
						// $('#kodelevel').val(data[i].kode.trim());
						$('#namabarang').val(data[i].namabarang.trim());
						$('#komisi').val(data[i].komisi.trim() + '%');
						$('#hargabeli').val(FormatRupiah(data[i].hargabeli.trim()));
						$('#hargajual').val(FormatRupiah(data[i].hargajual.trim()));
						// $('#kondisi1').val(FormatRupiah(data[i].kondisi1.trim()));
						// $('#kondisi2').val(FormatRupiah(data[i].kondisi2.trim()));
						// $('#discount').val(data[i].diskon.trim() + '%');
						$('#lokasi').val(data[i].lokasi.trim());
						if (data[i].aktif.trim() == 't') {
							$('#aktif').prop('checked', true);
						} else {
							$('#tidak_aktif').prop('checked', true);
						}

					}
					GetDataDetail(kodebarang, cabang);
					GetDataHarga(kodebarang, cabang);
					if($('#grup').val() == 'ADM JKT' || $('#grup').val() == 'ADM LK' || $('#grup').val() == 'AUDIT LK'){
						$('#update').prop('disabled', false);
						$('#save').prop('disabled', true);
						$('#hargabeli').prop('disabled', true);
						$('#hargajual').prop('disabled', true);
						$('#lokasi').prop('disabled', true);
						$('#komisi').prop('disabled', true);
						$('#caribarang').hide();
						$('#caricabang').hide();
						$('#carisalesman').show();
						$('#adddetail').show();
					}else {
						$('#update').prop('disabled', false);
						$('#save').prop('disabled', true);
						$('#hargabeli').prop('disabled', false);
						$('#hargajual').prop('disabled', false);
						$('#lokasi').prop('disabled', false);
						$('#komisi').prop('disabled', false);
						$('#caribarang').show();
						$('#caricabang').show();
						$('#carisalesman').show();
						$('#adddetail').show();
					}
					// $('#kodedetail').prop('disabled', true);

					// $('#carisatuan').hide();
					// $('.aktif').show();
				}
			});
		});

		document.getElementById('export').addEventListener("click", function(event) {
			event.preventDefault();
			var kodemerk = $('#kodemerk').val();
			var kodecabang = $('#kodecabang').val();
			if (kodemerk == "") {
				window.open(
					"<?= base_url('masterdata/BarangDetail/Export/'); ?>" + kodemerk + ":" + kodecabang
				);
			} else {
				window.open(
					"<?= base_url('masterdata/BarangDetail/Export/'); ?>" + kodemerk + ":" + kodecabang
				);
			}
		});

		function ValidasiAddHarga(hargajual) {
			var table = document.getElementById('t_hdetail');
			var hargabeli1 = $('#hargabeli1').val();
			var hargajual1 = $('#hargajual1').val();
			var kodebarang = $('#kodebarang').val();

			 if (hargajual1 == '') {
				Swal.fire({
					title: 'Information',
					icon: 'info',
					html: 'Harga Jual Tidak Boleh Kosong',
					showCloseButton: true,
					width: 350,
				});
				$('#hargajual').focus();
				return "gagal";
			} else {
				for (var r = 1, n = table.rows.length; r < n; r++) {
					var string = "";
					for (var c = 0, m = table.rows[r].cells.length; c < m; c++) {
						if (c == 0) {
							if (table.rows[r].cells[c].innerHTML == hargajual) {
								Swal.fire({
									title: 'Information',
									icon: 'info',
									html: 'Data ini sudah diinput',
									showCloseButton: true,
									width: 350,
								});
								return "gagal";
							}
						}
					}
				}
			}
			return "sukses";
		}

		$("#addharga").click(function() {
			var hargabeli1 = $('#hargabeli1').val();
			var hargajual1 = $('#hargajual1').val();
			// var status = $('.status').val();
			if (ValidasiAddHarga(hargajual1) == "sukses") {
				var row = "";
				row =
					'<tr id="' + DeFormatRupiah(hargajual1) + '">' +
					'<td nowrap>' + hargabeli1 + '</td>' +
					'<td nowrap>' + FormatRupiah(hargajual1) + '</td>' +
					// '<td nowrap>' + status + '</td>' +
					'<td nowrap style="text-align: center; width: 200px;">' +
					// '<button data-table="' + kodesalesman + '" data-toggle="modal" data-target="#changeqty" class="editdetail btn btn-sm btn-success mb-0 p-1"><i class="far fa-edit"></i> Edit</button>&nbsp;' +
					'<button data-table="' + DeFormatRupiah(hargajual1) + '" class="delharga btn btn-sm btn-danger m-1 p-1"><i class="fa fa-trash mr-1"></i> Delete</button>' +
					'</td>' +
					'</tr>';
				$('#tb_harga').append(row);
				$('#hargabeli').val("0");
				$('#hargajual1').val("");
			}
		});

		$(document).on('click', '.delharga', function() {
			var id = $(this).attr("data-table");
			$('#' + id).remove();
		});

		function AmbilDataHarga() {
			var table = document.getElementById('t_hdetail');
			var arr2 = [];
			for (var r = 1, n = table.rows.length; r < n; r++) {
				var string = "";
				for (var c = 0, m = table.rows[r].cells.length; c < m - 1; c++) {
					if (c == 0) {
						string = "{" + table.rows[0].cells[c].innerHTML + " : '" + table.rows[r].cells[c].innerHTML + "'";
					} else {
						string = string + ", " + table.rows[0].cells[c].innerHTML + " : '" + table.rows[r].cells[c].innerHTML + "'";
					}
				}
				string = string + "}";
				var obj = JSON.stringify(eval('(' + string + ')'));
				var arr = $.parseJSON(obj);
				arr2.push(arr);
			}
			return arr2;
		};

		function GetDataHarga(kodebarang, cabang) {
			$.ajax({
				url: "<?= base_url('masterdata/BarangDetail/DataHargaBarang'); ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					kodebarang: kodebarang,
					cabang: cabang
				},
				success: function(data) {
					console.log(data);
					for (var i = 0; i < data.length; i++) {
						var hargabeli = data[i].hargabeli;
						var hargajual = FormatRupiah(data[i].hargajual.trim());
						InsertDataHarga(hargabeli, hargajual);
					}
				}
			});
		};

		function InsertDataHarga(hargabeli, hargajual) {
			var row = "";
			row =
			'<tr id="' + DeFormatRupiah(hargajual) + '">' +
					'<td nowrap>' + hargabeli + '</td>' +
					'<td nowrap>' + hargajual + '</td>' +
					'<td nowrap style="text-align: center; width: 200px;">' +
					// '<button data-table="' + kodesalesman + '" data-toggle="modal" data-target="#changeqty" class="editdetail btn btn-sm btn-success mb-0 p-1"><i class="far fa-edit"></i> Edit</button>&nbsp;' +
					'<button data-table="' + DeFormatRupiah(hargajual) + '" class="delharga btn btn-sm btn-danger m-1 p-1"><i class="fa fa-trash mr-1"></i> Delete</button>' +
					'</td>' +
					'</tr>';
			$('#tb_harga').append(row);
		};



		/* Update */
		document.getElementById('update').addEventListener("click", function(event) {
			event.preventDefault();
			var kodecabang = $('#kodecabang').val();
			var kodebarang = $('#kodebarang').val();
			var kodelevel = $('#kodelevel').val();
			var namabarang = $('#namabarang').val();
			var hargajual = $('#hargajual').val();
			var komisi = $('#komisi').val();
			var hargabeli = $('#hargabeli').val();
			var kondisi1 = $('#kondisi1').val();
			var kondisi2 = $('#kondisi2').val();
			var discount = $('#discount').val();
			var lokasi = $('#lokasi').val();
			var aktif = $('input[name="aktif"]:checked').val();
			var datadetail = AmbilDataDetail();
			var dataharga = AmbilDataHarga();

			if (ValidasiSave(datadetail) == true && ValidasiSave(dataharga) == true) {
				$.ajax({
					url: "<?= base_url("masterdata/BarangDetail/Update") ?>",
					method: "POST",
					dataType: "json",
					async: true,
					data: {
						kodecabang: kodecabang,
						kodebarang: kodebarang,
						kodelevel: kodelevel,
						namabarang: namabarang,
						hargajual: hargajual,
						komisi: komisi,
						hargabeli: hargabeli,
						kondisi1: kondisi1,
						kondisi2: kondisi2,
						discount: discount,
						lokasi: lokasi,
						datadetail: datadetail,
						dataharga: dataharga,
						aktif: aktif
					},
					success: function(data) {
						if (data.kode != "") {
							Toast.fire({
								icon: 'success',
								title: data.message
							});
							$('#kodedetail').prop('disabled', true);
							$('#kodecabang').prop('disabled', true);
							$('#kodebarang').prop('disabled', true);
							$('#namabarang').prop('disabled', true);
							$('#hargabeli').prop('disabled', true);
							$('#hargajual').prop('disabled', true);
							$('#kondisi1').prop('disabled', true);
							$('#kondisi2').prop('disabled', true);
							$('#discount').prop('disabled', true);
							$('#komisi').prop('disabled', true);
							$('#lokasi').prop('disabled', true);
							$('#aktif').prop('disabled', true);

							$('#adddetail').hide();
							// $('#carisalesman').hide();
							$('#update').prop("disabled", true);
							$('#save').prop("disabled", true);
						} else {
							Toast.fire({
								icon: 'error',
								title: data.message
							});
						}
					}
				}, false)
			}

		});

		/* Clear */
		document.getElementById("clear").addEventListener("click", function(event) {
			event.preventDefault();
			// ClearScreen();
			location.reload(true);
		});

		ClearScreen();

	});
</script>
