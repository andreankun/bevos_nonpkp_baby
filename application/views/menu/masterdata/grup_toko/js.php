<script>
	$(document).ready(function() {
		function ClearScreen() {
			// $('#kode').prop('disabled', false).val("");
			$('#nomorpelanggan').prop('disabled', false).val("");
			$('#namapelanggan').prop('disabled', false).val("");
			$('#limitth').prop('disabled', false).val("");
			$('#limitbln').prop('disabled', false).val("");
			$('#bank').prop('disabled', false).val("");
			$('#rekening').prop('disabled', false).val("");
			$('#userid').prop('disabled', false).val("");
			$('#passwordatm').prop('disabled', false).val("");
			$('#noktp').prop('disabled', false).val("");
			$('#namapelanggan1').prop('disabled', false).val("");
			$('#aktif').prop('disabled', false);

			$('#update').prop('disabled', true);
			$('#save').prop('disabled', false);
			$('#tb_detail').empty();

			$('.aktif').hide();
		}

		// /* Maxlength Nomor HP */
		// $('#diskon[max]:not([max=""])').on('input', function(event) {
		//     var $this = $(this);
		//     var maxlength = $this.attr('max').length;
		//     var value = $this.val();
		//     if (value && value.length >= maxlength) {
		//         $this.val(value.substr(0, maxlength));
		//     }
		// })

		/* Format Rupiah */
		function FormatRupiah(angka, prefix) {
			if (!angka) {
				return '';
			}
			var vangka = angka.toString();
			var number_string = vangka.replace(/[^.\d]/g, '').replace(/[^\w\s]/gi, '').toString(),
				split = number_string.split('.'),
				sisa = split[0].length % 3,
				rupiah = split[0].substr(0, sisa),
				ribuan = split[0].substr(sisa).match(/\d{3}/gi);

			// tambahkan titik jika yang di input sudah menjadi angka ribuan
			if (ribuan) {
				separator = sisa ? ',' : '';
				rupiah += separator + ribuan.join(',');
			}

			rupiah = split[1] != undefined ? rupiah + '.' + split[1] : rupiah;
			return prefix == undefined ? rupiah : (rupiah ? rupiah : '');
		};

		$('#limitth').keyup(function() {
			$(this).val(FormatRupiah($(this).val()));
		});
		$('#limitbln').keyup(function() {
			$(this).val(FormatRupiah($(this).val()));
		})

		function ValidasiSave(datadetail) {
			// var kode = $('#kode').val();
			var nomorpelanggan = $('#nomorpelanggan').val();
			var namapelanggan = $('#namapelanggan').val();
			var limitth = $('#limitth').val();
			var limitbln = $('#limitbln').val();
			var bank = $('#bank').val();
			var userid = $('#userid').val();
			var passwordatm = $('#passwordatm').val();
			var passwordkey = $('#passwordkey').val();
			var passwordbanking = $('#passwordbanking').val();
			var rekening = $('#rekening').val();
			var ktp = $('#noktp').val();

			// if (kode == '' || kode == 0) {
			//     Swal.fire({
			//         title: 'Informasi',
			//         icon: 'info',
			//         html: 'Kode tidak boleh kosong',
			//         showCloseButton: true,
			//         width: 350
			//     });
			//     $('#kode').focus();
			//     var result = false;
			// } else 
			if (nomorpelanggan == '' || nomorpelanggan == 0) {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Nomor Pelanggan tidak boleh kosong',
					showCloseButton: true,
					width: 350
				});
				$('#nomorpelanggan').focus();
				var result = false;
			} else if (namapelanggan == '' || namapelanggan == 0) {
				Swal.fire({
					title: 'informasi',
					icon: 'info',
					html: 'Nama Pelanggan tidak boleh kosong',
					showCloseButton: true,
					width: 350
				});
				$('#namapelanggan').focus();
				var result = false;
			} else if (datadetail.length == 0 || datadetail.length == '0') {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Data Detail tidak boleh kosong.',
					showCloseButton: true,
					width: 350,
				});
				$('#namagudang').focus();
				result = false;
			} else if (limitth == '' || limitth == 0) {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Kode Barang tidak boleh kosong',
					showCloseButton: true,
					width: 350
				});
				$('#kodebarang').focus();
				var result = false;
			} else if (ktp == '' || ktp == 0) {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Kode Barang tidak boleh kosong',
					showCloseButton: true,
					width: 350
				});
				$('#kodebarang').focus();
				var result = false;
			} else if (limitbln == '' || limitbln == 0) {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Nama Barang tidak boleh kosong',
					showCloseButton: true,
					width: 350
				});
				$('#namabarang').focus();
				var result = false;
			} else if (bank == '') {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Diskon Tidak boleh kosong',
					showCloseButton: true,
					width: 350
				});
				$('#diskon').focus();
				var result = false;
			} else if (userid == '') {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Diskon Tidak boleh kosong',
					showCloseButton: true,
					width: 350
				});
				$('#diskon').focus();
				var result = false;
			} else if (rekening == '') {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Diskon Tidak boleh kosong',
					showCloseButton: true,
					width: 350
				});
				$('#diskon').focus();
				var result = false;
			} else if (passwordatm == '') {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Diskon Tidak boleh kosong',
					showCloseButton: true,
					width: 350
				});
				$('#diskon').focus();
				var result = false;
			}  else if (passwordkey == '') {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Password Key boleh kosong',
					showCloseButton: true,
					width: 350
				});
				$('#diskon').focus();
				var result = false;
			} else if (passwordbanking == '') {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Password Banking boleh kosong',
					showCloseButton: true,
					width: 350
				});
				$('#diskon').focus();
				var result = false;
			}  else {
				var result = true;
			}

			return result;
		}

		/* Validasi Data Add Detail */
		function ValidasiAddDetail(nocustomer) {
			var table = document.getElementById('t_detail');
			var nocustomer = $('#nocustomer').val();
			var namacustomer = $('#namacustomer').val();

			if (nocustomer == '' || nocustomer == 0) {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'no customer tidak boleh kosong.',
					showCloseButton: true,
					width: 350,
				});
				$('#kodebarang').focus();
				return "gagal";
			} else if (namacustomer == '' || namacustomer == 0) {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Nama Customer tidak boleh kosong.',
					showCloseButton: true,
					width: 350,
				});
				$('#namacustomer').focus();
				return "gagal";
			} else {
				for (var r = 1, n = table.rows.length; r < n; r++) {
					var string = "";
					for (var c = 0, m = table.rows[r].cells.length; c < m; c++) {
						if (c == 0) {
							if (table.rows[r].cells[c].innerHTML == nocustomer) {
								Swal.fire({
									title: 'Informasi',
									icon: 'info',
									html: 'Data ini sudah diinput.',
									showCloseButton: true,
									width: 350,
								});
								return "gagal";
							}
						}
					}
				}
			}
			return "sukses"
		}

		/* Declare Toast */
		const Toast = Swal.mixin({
			toast: true,
			position: 'top-end',
			showConfirmButton: false,
			timer: 3000,
			timerProgressBar: true,
			didOpen: (toast) => {
				toast.addEventListener('mouseenter', Swal.stopTimer)
				toast.addEventListener('mouseleave', Swal.resumeTimer)
			}
		});

		$('#diskon').mask('99.99%', {
			reverse: true
		});

		$("#adddetail").click(function() {

			var nocustomer = $('#nocustomer').val();
			var namacustomer = $('#namacustomer').val();
			var penanggungjawab = $('#namapelanggan1').val();
			if (ValidasiAddDetail(nocustomer) == "sukses") {
				var row = "";
				row =
					'<tr id="' + nocustomer + '">' +
					'<td nowrap>' + nocustomer + '</td>' +
					'<td nowrap>' + namacustomer + '</td>' +
					'<td nowrap>' + penanggungjawab + '</td>' +
					'<td nowrap style="text-align: center; width: 200px;">' +
					// '<button data-table="' + nocustomer + '" data-toggle="modal" data-target="#changediskon" class="editdetail btn btn-success m-1 p-1"><i class="fas fa-edit"></i> Edit</button>' +
					'<button data-table="' + nocustomer + '" class="deldetail btn btn-danger m-1 p-1"><i class="fa fa-trash mr-1"></i> Delete</button>' +
					'</td>' +
					'</tr>';
				$('#tb_detail').append(row);
				$('#nocustomer').val("");
				$('#namacustomer').val("");
			}
		});

		$(document).on('click', '.deldetail', function() {
			var id = $(this).attr("data-table");
			$('#' + id).remove();
		});

		function AmbilDataDetail() {
			var table = document.getElementById('t_detail');
			var arr2 = [];
			for (var r = 1, n = table.rows.length; r < n; r++) {
				var string = "";
				for (var c = 0, m = table.rows[r].cells.length; c < m - 1; c++) {
					if (c == 0) {
						string = "{" + table.rows[0].cells[c].innerHTML + " : '" + table.rows[r].cells[c].innerHTML + "'";
					} else {
						string = string + ", " + table.rows[0].cells[c].innerHTML + " : '" + table.rows[r].cells[c].innerHTML + "'";
					}
				}
				string = string + "}";
				var obj = JSON.stringify(eval('(' + string + ')'));
				var arr = $.parseJSON(obj);
				arr2.push(arr);
			}
			return arr2;
		};

		function GetDataDetail(nomor) {
			$.ajax({
				url: "<?= base_url('masterdata/KelompokToko/DataKelompokDetail'); ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					nomor: nomor
				},
				success: function(data) {
					for (var i = 0; i < data.length; i++) {
						// var kode = data[i].kode;
						var nopelanggan = data[i].nopelanggan;
						var namapelanggan = data[i].namapelanggan.trim();
						var namajwb = data[i].namajwb.trim();
						console.log(nopelanggan);

						InsertDataDetail(nopelanggan, namapelanggan, namajwb);
					}
				}
			});
		};

		function InsertDataDetail(nopelanggan, namapelanggan, namajwb) {

			var row = "";
			row =
				// '<tr id="' + nocustomer + '">' +
				// '<td nowrap>' + nocustomer + '</td>' +
				// '<td nowrap>' + namacustomer + '</td>' +
				// '<td nowrap>' + namajwb + '</td>' +
				// '<td nowrap style="text-align: center; width: 200px;">' +
				// // '<button data-table="' + nocustomer + '" data-toggle="modal" data-target="#changediskon" class="editdetail btn btn-success m-1 p-1"><i class="fas fa-edit"></i> Edit</button>' +
				// '<button data-table="' + nocustomer + '" class="deldetail btn btn-danger m-1 p-1"><i class="fa fa-trash mr-1"></i> Delete</button>' +
				// '</td>' +
				// '</tr>';
				'<tr id="' + nopelanggan + '">' +
                '<td>' + nopelanggan + '</td>' +
                '<td>' + namapelanggan + '</td>' +
                '<td>' + namajwb + '</td>' +
                '<td style="text-align: center; width: 200px;">' +
                // '<button data-table="' + kodesalesman + '" data-toggle="modal" data-target="#changeqty" class="editdetail btn btn-sm btn-success m-1 p-1"><i class="fas fa-edit"></i> Edit</button>' +
                '<button data-table="' + nocustomer + '" class="deldetail btn btn-sm btn-danger m-1 p-1"><i class="fa fa-trash mr-1"></i> Delete</button>' +
                '</td>' +
                '</tr>';
			$('#tb_detail').append(row);
		};

		document.getElementById("caripelanggan").addEventListener("click", function(event) {
			var cabang = $('#kodecabang').val();
			console.log(cabang);
			event.preventDefault();
			$('#t_pelanggan').DataTable({
				"destroy": true,
				"searching": true,
				"processing": true,
				"serverSide": true,
				"lengthChange": true,
				"pageLength": 5,
				"lengthMenu": [5, 10, 25, 50],
				"order": [],
				"ajax": {
					"url": "<?= base_url('masterdata/Customer/CariDataCustomer'); ?>",
					"method": "POST",
					"data": {
						nmtb: "glbm_customer",
						field: {
							nomor: "nomor",
							nama: "nama",
							nama_toko: "nama_toko",
						},
						sort: "nomor",
						where: {
							nomor: "nomor",
							nama: "nama",
							nama_toko: "nama_toko",
						},
						value: "aktif = true AND kodecabang = '" + cabang + "'"
					},
				}
			});
		});

		$(document).on('click', ".searchcustomer", function() {
			var nomor = $(this).attr("data-id");
			$.ajax({
				url: "<?= base_url('masterdata/Customer/DataCustomer') ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					nomor: nomor
				},
				success: function(data) {
					for (var i = 0; i < data.length; i++) {
						$('#nomorpelanggan').val(data[i].nomor.trim());
						$('#namapelanggan1').val(data[i].nama.trim());
						$('#namapelanggan').val(data[i].nama.trim());
					}
				}
			})
		});

		document.getElementById("caricustomer1").addEventListener("click", function(event) {
			var cabang = $('#kodecabang').val();
			event.preventDefault();
			$('#t_diskon').DataTable({
				"destroy": true,
				"searching": true,
				"processing": true,
				"serverSide": true,
				"lengthChange": true,
				"pageLength": 5,
				"lengthMenu": [5, 10, 25, 50],
				"order": [],
				"ajax": {
					"url": "<?= base_url('masterdata/Customer/CariDataCustomer1'); ?>",
					"method": "POST",
					"data": {
						nmtb: "glbm_customer",
						field: {
							nomor: "nomor",
							nama: "nama",
							nama_toko: "nama_toko",
						},
						sort: "nomor",
						where: {
							nomor: "nomor",
							nama: "nama",
							nama_toko: "nama_toko",
						},
						value: "aktif = true AND kodecabang = '" + cabang + "'"
					},
				}
			});
		});

		$(document).on('click', ".searchcustomer1", function() {
			var nomor = $(this).attr("data-id");
			$.ajax({
				url: "<?= base_url('masterdata/Customer/DataCustomer1') ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					nomor: nomor
				},
				success: function(data) {
					for (var i = 0; i < data.length; i++) {
						$('#nocustomer').val(data[i].nomor.trim());
						// $('#namapelanggan1').val(data[i].nama.trim());
						$('#namacustomer').val(data[i].nama.trim());
					}
				}
			})
		});

		document.getElementById("find").addEventListener("click", function(event) {
			event.preventDefault();
			$('#tb_detail').empty();
			$('#t_diskon').DataTable({
				"destroy": true,
				"searching": true,
				"processing": true,
				"serverSide": true,
				"lengthChange": true,
				"pageLength": 5,
				"lengthMenu": [5, 10, 25, 50],
				"order": [],
				"ajax": {
					"url": "<?= base_url('masterdata/KelompokToko/CariDataKelompok'); ?>",
					"method": "POST",
					"data": {
						nmtb: "glbm_kelompoktoko",
						field: {
							// kode: "kode",
							nomor: "nomor",
							nama: "nama",
							aktif: "aktif"
						},
						sort: "nomor",
						where: {
							nomor: "nomor",
							nama: "nama",
						}
					},
				},
				"columnDefs": [{
					"targets": 3,
					"data": "aktif",
					"render": function(data, type, row, meta) {
						return (row[3] == 't') ? 'Aktif' : 'Tidak Aktif';
					}
				}],
			});
		});

		$(document).on('click', ".searchkelompok", function() {
			var nomor = $(this).attr("data-id");
			$.ajax({
				url: "<?= base_url('masterdata/KelompokToko/DataKelompok') ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					nomor: nomor
				},
				success: function(data) {
					for (var i = 0; i < data.length; i++) {
						$('#nomorpelanggan').val(data[i].nomor.trim());
						$('#namapelanggan').val(data[i].nama.trim());
						$('#limitth').val(data[i].limitth.trim());
						$('#limitbln').val(data[i].limitbln.trim());
						$('#bank').val(data[i].bank.trim());
						$('#userid').val(data[i].userid.trim());
						$('#rekening').val(data[i].rekeningtf.trim());
						$('#passwordatm').val(data[i].password.trim());
						$('#noktp').val(data[i].ktp.trim());
						$('#namapelanggan1').val(data[i].nama.trim());
						if (data[i].aktif.trim() == 't') {
							$('#aktif').prop('checked', true);
						} else {
							$('#tidak_aktif').prop('checked', true);
						}
					}
					GetDataDetail(nomor)
					$('#save').prop('disabled', true);
					$('#update').prop('disabled', false);
					$('#nomor').prop('disabled', true);
					$('.aktif').show();
				}
			})
		});


		document.getElementById("save").addEventListener("click", function(event) {
			event.preventDefault();
			// var kode = $('#kode').val();
			var nomorpelanggan = $('#nomorpelanggan').val();
			var namapelanggan = $('#namapelanggan').val();
			var limitth = $('#limitth').val();
			var limitbln = $('#limitbln').val();
			var bank = $('#bank').val();
			var userid = $('#userid').val();
			var rekening = $('#rekening').val();
			var passwordatm = $('#passwordatm').val();
			var passwordkey = $('#passwordkey').val();
			var passwordbanking = $('#passwordbanking').val();
			var ktp = $('#noktp').val();
			var datadetail = AmbilDataDetail();
			console.log(limitth)
			if (ValidasiSave(datadetail) == true) {
				$.ajax({
					url: "<?= base_url("masterdata/KelompokToko/Save") ?>",
					method: "POST",
					dataType: "json",
					async: true,
					data: {
						// kode: kode,
						nomorpelanggan: nomorpelanggan,
						namapelanggan: namapelanggan,
						limitth: limitth,
						limitbln: limitbln,
						bank: bank,
						ktp: ktp,
						rekening: rekening,
						userid: userid,
						passwordatm: passwordatm,
						passwordkey: passwordkey,
						passwordbanking: passwordbanking,
						datadetail: datadetail
					},
					success: function(data) {
						console.log(data);
						if (data.nomorpelanggan != "") {
							Toast.fire({
								icon: 'success',
								title: data.message
							});
							// $('#kode').prop('disabled', true);
							$('#nomorpelanggan').prop('disabled', true);
							$('#namapelanggan').prop('disabled', true);
							$('#limitth').prop('disabled', true);
							$('#limitbln').prop('disabled', true);
							$('#bank').prop('disabled', true);
							$('#rekening').prop('disabled', true);
							$('#userid').prop('disabled', true);
							$('#passwordatm').prop('disabled', true);
							$('#aktif').prop('disabled', true);

							$('#update').prop("disabled", true);
							$('#save').prop("disabled", true);
						} else {
							Toast.fire({
								icon: 'error',
								title: data.message
							});
						}
					}
				}, false)
			}
		})


		document.getElementById('update').addEventListener("click", function(event) {
			event.preventDefault();
			// var kode = $('#kode').val();
			var nomorpelanggan = $('#nomorpelanggan').val();
			var namapelanggan = $('#namapelanggan').val();
			var limitth = $('#limitth').val();
			var limitbln = $('#limitbln').val();
			var bank = $('#bank').val();
			var userid = $('#userid').val();
			var rekening = $('#rekening').val();
			var passwordatm = $('#passwordatm').val();
			var passwordkey = $('#passwordkey').val();
			var passwordbanking = $('#passwordbanking').val();
			var ktp = $('#noktp').val();
			var aktif = $('input[name="aktif"]:checked').val();
			var datadetail = AmbilDataDetail();

			if (ValidasiSave(datadetail) == true) {
				$.ajax({
					url: "<?= base_url("masterdata/KelompokToko/Update") ?>",
					method: "POST",
					dataType: "json",
					async: true,
					data: {
						// kode: kode,
						nomorpelanggan: nomorpelanggan,
						namapelanggan: namapelanggan,
						limitth: limitth,
						limitbln: limitbln,
						bank: bank,
						ktp: ktp,
						rekening: rekening,
						userid: userid,
						passwordatm: passwordatm,
						passwordkey: passwordkey,
						passwordbanking: passwordbanking,
						datadetail: datadetail
					},
					success: function(data) {
						if (data.nomorpelanggan != "") {
							Toast.fire({
								icon: 'success',
								title: data.message
							});
							// $('#kode').prop('disabled', true);
							$('#nomorpelanggan').prop('disabled', true);
							$('#namapelanggan').prop('disabled', true);
							$('#kodebarang').prop('disabled', true);
							$('#namabarang').prop('disabled', true);
							$('#diskon').prop('disabled', true);
							$('#aktif').prop('disabled', true);

							$('#update').prop("disabled", true);
							$('#save').prop("disabled", true);
						} else {
							Toast.fire({
								icon: 'error',
								title: data.message
							});
						}
					}
				}), false;
			}
		})

		document.getElementById("clear").addEventListener("click", function(event) {
			event.preventDefault();
			ClearScreen();
		});
		ClearScreen();
	});
</script>
