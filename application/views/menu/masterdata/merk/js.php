<script type="text/javascript">
    $(document).ready(function() {

        /* Function ClearScreen */
        function ClearScreen() {
            $('#kodemerk').prop("disabled", false).val("");
            $('#namamerk').prop("disabled", false).val("");
            $('#aktif').prop("disabled", false);

            $('#update').prop("disabled", true);
            $('#save').prop("disabled", false);

            $('.aktif').hide();
        }

        /* Validasi Kosong */
        function ValidasiSave() {
            var kode = $('#kodemerk').val();
            var nama = $('#namamerk').val();

            if (kode == '' || kode == 0) {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Kode tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#kodemerk').focus();
                var result = false;
            } else if (nama == '' || nama == 0) {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Nama tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#namamerk').focus();
                var result = false;
            } else {
                var result = true;
            }

            return result;
        }


        /* Declare Toast */
        const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000,
            timerProgressBar: true,
            didOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
            }
        });

        /* Save */
        document.getElementById('save').addEventListener("click", function(event) {
            event.preventDefault();
            var kode = $('#kodemerk').val();
            var nama = $('#namamerk').val();

            if(ValidasiSave() == true){
                $.ajax({
                    url: "<?= base_url("masterdata/Merk/Save") ?>",
                    method: "POST",
                    dataType: "json",
                    async: true,
                    data: {
                        kode: kode,
                        nama: nama
                    },
                    success: function(data) {
                        if (data.kode != "") {
                            Toast.fire({
                                icon: 'success',
                                title: data.message
                            });
                            $('#kodemerk').prop("disabled", true);
                            $('#namamerk').prop("disabled", true);

                            $('#update').prop("disabled", true);
                            $('#save').prop("disabled", true);
                        } else {
                            Toast.fire({
                                icon: 'error',
                                title: data.message
                            });
                        }
                    }
                }, false)
            }
        })

        /* Cari Data */
        document.getElementById("find").addEventListener("click", function(event) {
            event.preventDefault();
            $('#t_merk').DataTable({
                "destroy": true,
                "searching": true,
                "processing": true,
                "serverSide": true,
                "lengthChange": true,
                "pageLength": 5,
                "lengthMenu": [5, 10, 25, 50],
                "order": [],
                "ajax": {
                    "url": "<?= base_url('masterdata/Merk/CariDataMerk'); ?>",
                    "method": "POST",
                    "data": {
                        nmtb: "glbm_merk",
                        field: {
                            kode: "kode",
                            nama: "nama",
                            aktif: "aktif",
                        },
                        sort: "kode",
                        where: {
                            kode: "kode",
                            nama: "nama",
                        },
                    },
                },
				'columnDefs':[{
					"targets": 3,
					"data": "aktif",
					"render": function(data, type, row, meta) {
						return (row[3] == "t") ? "<p>Aktif</p>" : "<p>Tidak Aktif</p>"
					}
				}]
            });
        });

        /*Get Data*/
        $(document).on('click', ".searchmerk", function() {
            var kode = $(this).attr("data-id");;
            $.ajax({
                url: "<?= base_url('masterdata/Merk/DataMerk'); ?>",
                method: "POST",
                dataType: "json",
                async: false,
                data: {
                    kode: kode
                },
                success: function(data) {
                    for (var i = 0; i < data.length; i++) {
                        $('#kodemerk').val(data[i].kode.trim());
                        $('#namamerk').val(data[i].nama.trim());
                        if (data[i].aktif.trim() == 't') {
                            $('#aktif').prop('checked', true);
                        } else {
                            $('#tidak_aktif').prop('checked', true);
                        }
                    }
                    $('#update').prop('disabled', false);
                    $('#save').prop('disabled', true);
                    $('#kodemerk').prop('disabled', true);
                    $('.aktif').show();
                }
            }, false);
        });

        /* Update */
        document.getElementById('update').addEventListener("click", function(event) {
            event.preventDefault();
            var kode = $('#kodemerk').val();
            var nama = $('#namamerk').val();
            var aktif = $('input[name="aktif"]:checked').val();

            if(ValidasiSave() == true){
                $.ajax({
                    url: "<?= base_url("masterdata/Merk/Update") ?>",
                    method: "POST",
                    dataType: "json",
                    async: true,
                    data: {
                        kode: kode,
                        nama: nama,
                        aktif: aktif
                    },
                    success: function(data) {
                        if (data.kode != "") {
                            Toast.fire({
                                icon: 'success',
                                title: data.message
                            });
                            $('#kodemerk').prop("disabled", true);
                            $('#namamerk').prop("disabled", true);
                            $('#aktif').prop("disabled", true);

                            $('#update').prop("disabled", true);
                            $('#save').prop("disabled", true);
                        } else {
                            Toast.fire({
                                icon: 'error',
                                title: data.message
                            });
                        }
                    }
                }, false)
            }
        });

        /* Clear */
        document.getElementById('clear').addEventListener("click", function(event) {
            event.preventDefault();
            ClearScreen();
        })

        ClearScreen();
    });
</script>
