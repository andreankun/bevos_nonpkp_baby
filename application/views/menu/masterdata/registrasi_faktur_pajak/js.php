<script type="text/javascript">
    $(document).ready(function() {
        function ClearScreen() {
            // $('#nomor1').mask('000.000-00.00000000', {
            //     placeholder: "Mulai Nomor"
            // });
            // $('#nomor2').mask('000.000-00.00000000', {
            //     placeholder: "Akhir Nomor"
            // });

            $('#nomor1').val("");
            $('#nomor2').val("");
        }

        function ValidasiSave() {
            var nomor1 = $('#nomor1').val();
            var nomor2 = $('#nomor2').val();

            if (nomor1 == '' || nomor1 == 0) {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Awal Nomor tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#nomor1').focus();
                var result = false;
            } else if (nomor2 == '' || nomor2 == 0) {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Akhir Nomor tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#nomor2').focus();
                var result = false;
            } else {
                var result = true;
            }
            return result;
        }

        /* Declare Toast */
        const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000,
            timerProgressBar: true,
            didOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
            }
        });

        document.getElementById("save").addEventListener("click", function(event) {
            event.preventDefault();
            var nomor1 = $('#nomor1').val();
            var nomor2 = $('#nomor2').val();

            if (ValidasiSave() == true) {
                $.ajax({
                    url: "<?= base_url("masterdata/RegistrasiNomorFakturPajak/Save") ?>",
                    method: "POST",
                    dataType: "json",
                    async: true,
                    data: {
                        nomor1: nomor1,
                        nomor2: nomor2,
                    },
                    success: function(data) {
                        if (data.nomor != "") {
                            Toast.fire({
                                icon: 'success',
                                title: data.message
                            });
                            ClearScreen();
                        } else {
                            Toast.fire({
                                icon: 'error',
                                title: data.message
                            });
                        }
                    }
                }, false)
            }

        })

        var tableregister = $('#t_detail').DataTable({
            "destroy": true,
            "searching": true,
            "serverSide": true,
            "lengthChange": true,
            "pageLength": 10,
            "lengthMenu": [10, 20, 50, 100],
            "order": [],
            "ajax": {
                "url": "<?= base_url('masterdata/RegistrasiNomorFakturPajak/CariDataRegistrasiNomorFakturPajak'); ?>",
                "method": "POST",
                "data": {
                    nmtb: "cari_regfakturpajak",
                    field: {
                        nomor: "nomor",
                        tanggal: "tanggal",
                        status: "status",
                        sumberfaktur: "sumberfaktur",
                        nofaktur: "nofaktur",
                        nama_toko: "nama_toko"
                    },
                    sort: "nomor",
                    where: {
                        nomor: "nomor",
                        nofaktur: "nofaktur",
                        nama_toko: "nama_toko"
                    },
                },
            },
            "columnDefs": [{
                "targets": 1,
                "render": $.fn.dataTable.render.moment('YYYY-MM-DD HH:mm:ss', 'D MMM YY')
            }, {
                "targets": 2,
                "data": "aktif",
                "render": function(data, type, row, meta) {
                    return (row[2] == 't') ? 'Terpakai' : 'Belum Terpakai';
                }
            }],
        });

        setInterval(function() {
            tableregister.ajax.reload(null, false); // user paging is not reset on reload
        }, 3000);

        document.getElementById("clear").addEventListener("click", function(event) {
            event.preventDefault();
            ClearScreen();
        })

        ClearScreen();
    })
</script>