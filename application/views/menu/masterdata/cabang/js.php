<script type="text/javascript">
    $(document).ready(function() {

        /* Function ClearScreen */
        function ClearScreen() {
            $('#kodecbg').prop("disabled", false).val("");
            $('#namacbg').prop("disabled", false).val("");
            $('#alamat').prop("disabled", false).val("");
            $('#nocoa').val("");
            $('#namacoa').val("");
            $('#aktif').prop("disabled", false);

            $('#update').prop("disabled", true);
            $('#save').prop("disabled", false);

            $('.aktif').hide();
        }

        /* Validasi Kosong */
        function ValidasiSave() {
            var kode = $('#kodecbg').val();
            var nama = $('#namacbg').val();
            var alamat = $('#alamat').val();
            var nocoa = $('#nocoa').val();
            var namacoa = $('#namacoa').val();

            if (kode == '' || kode == 0) {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Kode tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#kodecbg').focus();
                var result = false;
            } else if (nama == '' || nama == 0) {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Nama tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#namacbg').focus();
                var result = false;
            } else if (nocoa == '' || nocoa == 0 || namacoa == '' || namacoa == 0) {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Akun Deposit Belum Diisi.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#namacoa').focus();
                var result = false;
            } else if (alamat == '' || alamat == 0) {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Alamat tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#alamat').focus();
                var result = false;
            } else {
                var result = true;
            }

            return result;
        }


        /* Declare Toast */
        const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000,
            timerProgressBar: true,
            didOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
            }
        });

        /* Save */
        document.getElementById('save').addEventListener("click", function(event) {
            event.preventDefault();
            var kode = $('#kodecbg').val();
            var nama = $('#namacbg').val();
            var alamat = $('#alamat').val();
            var coadeposit = $('#nocoa').val();

            $.ajax({
                url: "<?= base_url("masterdata/Cabang/Save") ?>",
                method: "POST",
                dataType: "json",
                async: true,
                data: {
                    kode: kode.toUpperCase(),
                    nama: nama,
                    alamat: alamat,
                    coadeposit: coadeposit
                },
                success: function(data) {
                    if (data.kode != "") {
                        Toast.fire({
                            icon: 'success',
                            title: data.message
                        });
                        $('#kodecbg').prop("disabled", true);
                        $('#namacbg').prop("disabled", true);
                        $('#alamat').prop("disabled", true);

                        $('#update').prop("disabled", true);
                        $('#save').prop("disabled", true);
                    } else {
                        Toast.fire({
                            icon: 'error',
                            title: data.message
                        });
                    }
                }
            }, false)
        })

        /* Cari Data Cabang*/
        document.getElementById("find").addEventListener("click", function(event) {
            event.preventDefault();
            $('#t_cabang').DataTable({
                "destroy": true,
                "searching": true,
                "processing": true,
                "serverSide": true,
                "lengthChange": true,
                "pageLength": 5,
                "lengthMenu": [5, 10, 25, 50],
                "order": [],
                "ajax": {
                    "url": "<?= base_url('masterdata/Cabang/CariDataCabang'); ?>",
                    "method": "POST",
                    "data": {
                        nmtb: "glbm_cabang",
                        field: {
                            kode: "kode",
                            nama: "nama",
                            alamat: "alamat",
                            aktif: "aktif"
                        },
                        sort: "kode",
                        where: {
                            kode: "kode",
                            nama: "nama",
                            alamat: "alamat"
                        },
                        // value: "aktif = true"
                    },
                },
                "columnDefs": [{
                    "targets": 4,
                    "data": "aktif",
                    "render": function(data, type, row, meta) {
                        return (row[4] == 't') ? 'Aktif' : 'Tidak Aktif'
                    }
                }]
            });
        });

        /*Get Data Cabang*/
        $(document).on('click', ".searchcabang", function() {
            var kode = $(this).attr("data-id");;
            $.ajax({
                url: "<?= base_url('masterdata/Cabang/DataCabang'); ?>",
                method: "POST",
                dataType: "json",
                async: false,
                data: {
                    kode: kode
                },
                success: function(data) {
                    for (var i = 0; i < data.length; i++) {
                        $('#kodecbg').val(data[i].kode.trim());
                        $('#namacbg').val(data[i].nama.trim());
                        $('#alamat').val(data[i].alamat.trim());
                        $('#nocoa').val(data[i].coadeposit.trim());

                        TampilDataCoaDeposit(data[i].coadeposit.trim());
                        if (data[i].aktif.trim() == 't') {
                            $('#aktif').prop('checked', true);
                        } else {
                            $('#tidak_aktif').prop('checked', true);
                        }
                    }
                    $('#save').prop('disabled', true);
                    $('#update').prop('disabled', false);
                    $('#kodecbg').prop('disabled', true);
                    $('.aktif').show();
                }
            }, false);
        });

        document.getElementById("caricoa").addEventListener("click", function(event) {
            event.preventDefault();
            $('#t_coa').DataTable({
                "destroy": true,
                "searching": true,
                "processing": true,
                "serverSide": true,
                "lengthChange": true,
                "pageLength": 5,
                "lengthMenu": [5, 10, 25, 50],
                "order": [],
                "ajax": {
                    "url": "<?= base_url('masterdata/Cabang/CariDataCoa'); ?>",
                    "method": "POST",
                    "data": {
                        nmtb: "glbm_accountlainlain",
                        field: {
                            nomor: "nomor",
                            nama: "nama"
                        },
                        sort: "nomor",
                        where: {
                            nomor: "nomor",
                            nama: "nama"
                        },
                        value: "aktif = true"
                    },
                }
            });
        });

        $(document).on('click', ".searchcoa", function() {
            var nomor = $(this).attr("data-id");
            $('#nocoa').val(nomor.trim());

            TampilDataCoaDeposit(nomor);
        });

        function TampilDataCoaDeposit(nomor) {
            $.ajax({
                url: "<?= base_url('masterdata/Cabang/DataCoa'); ?>",
                method: "POST",
                dataType: "json",
                async: false,
                data: {
                    nomor: nomor
                },
                success: function(data) {
                    for (var i = 0; i < data.length; i++) {
                        $('#namacoa').val(data[i].nama.trim());
                    }
                }
            }, false);
        }



        /* Update */
        document.getElementById('update').addEventListener("click", function(event) {
            event.preventDefault();
            var kode = $('#kodecbg').val();
            var nama = $('#namacbg').val();
            var alamat = $('#alamat').val();
            var aktif = $('input[name="aktif"]:checked').val();
            var coadeposit = $('#nocoa').val();

            $.ajax({
                url: "<?= base_url("masterdata/Cabang/Update") ?>",
                method: "POST",
                dataType: "json",
                async: true,
                data: {
                    kode: kode,
                    nama: nama,
                    alamat: alamat,
                    coadeposit: coadeposit,
                    aktif: aktif
                },
                success: function(data) {
                    console.log(data)
                    if (data.kode != "") {
                        Toast.fire({
                            icon: 'success',
                            title: data.message
                        });
                        $('#kodecbg').prop("disabled", true);
                        $('#namacbg').prop("disabled", true);
                        $('#alamat').prop("disabled", true);
                        $('#aktif').prop("disabled", true);

                        $('#update').prop("disabled", true);
                        $('#save').prop("disabled", true);
                    } else {
                        Toast.fire({
                            icon: 'error',
                            title: data.message
                        });
                    }
                }
            }, false)
        });

        /* Clear */
        document.getElementById('clear').addEventListener("click", function(event) {
            event.preventDefault();
            ClearScreen();
        })

        ClearScreen();
    });
</script>
