<script type="text/javascript">
	$(document).ready(function() {

		/* Function ClearScreen */
		function ClearScreen() {
			$('#kodecabang').prop('disabled', false).val("<?= $this->session->userdata('mycabang'); ?>");
			$('#update').prop("disabled", true);
			$('#save').prop("disabled", false);

			$('#norek').prop('disabled', false).val("");
			$('#jenisacc').prop('disabled', false).val("");
			$('#keterangan').prop('disabled', false).val("");
			$('#nama').prop('disabled', false).val("");
			$('#aktif').prop('disabled', false);
			$('#coa').prop('readonly', true).val("");

			$('#caricoa').show();
			$('.aktif').hide();
		};

		/* Validasi Kosong */
		function ValidasiSave() {
			var norek = $('#norek').val();
			// var namabank = $('#namabank').val();
			var nama = $('#nama').val();
			var nama = $('#namabisnis').val();
			var coa = $('#coa').val();

			if (coa == '' || coa == 0) {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'COA tidak boleh kosong.',
					showCloseButton: true,
					width: 350,
				});
				$('#coa').focus();
				var result = false;
			} else if (nama == '' || nama == 0) {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Nama tidak boleh kosong.',
					showCloseButton: true,
					width: 350,
				});
				$('#nama').focus();
				var result = false;
			}
			else if (namabisnis == '' || namabisnis == 0) {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Nama Bisnis tidak boleh kosong.',
					showCloseButton: true,
					width: 350,
				});
				$('#namabisnis').focus();
				var result = false;
			} 
			// else if (norek == '' || norek == 0) {
			// 	Swal.fire({
			// 		title: 'Informasi',
			// 		icon: 'info',
			// 		html: 'No. Rekening tidak boleh kosong.',
			// 		showCloseButton: true,
			// 		width: 350,
			// 	});
			// 	$('#norek').focus();
			// 	var result = false;
			// }
			else {
				var result = true;
			}

			return result;
		}

		/* Declare Toast */
		const Toast = Swal.mixin({
			toast: true,
			position: 'top-end',
			showConfirmButton: false,
			timer: 3000,
			timerProgressBar: true,
			didOpen: (toast) => {
				toast.addEventListener('mouseenter', Swal.stopTimer)
				toast.addEventListener('mouseleave', Swal.resumeTimer)
			}
		});

		/* Save */
		document.getElementById('save').addEventListener("click", function(event) {
			event.preventDefault();
			var norek = $('#norek').val();
			var jenisacc = $('#jenisacc').val();
			var nama = $('#nama').val();
			var namabisnis = $('#namabisnis').val();
			var keterangan = $('#keterangan').val();
			var kodecabang = $('#kodecabang').val();
			var coa = $('#coa').val();

			if (ValidasiSave() == true) {
				$.ajax({
					url: "<?= base_url("masterdata/Rekening_transfer/Save") ?>",
					method: "POST",
					dataType: "json",
					async: true,
					data: {
						norek: norek,
						jenisacc: jenisacc,
						nama: nama,
						namabisnis: namabisnis,
						keterangan: keterangan,
						kodecabang: kodecabang,
						coa: coa
					},
					success: function(data) {
						console.log(data);
						if (data.nomor != "") {
							Toast.fire({
								icon: 'success',
								title: data.message
							});
							$('#norek').prop('disabled', true);
							$('#jenisacc').prop('disabled', true);
							$('#nama').prop('disabled', true);
							$('#namabisnis').prop('disabled', true);

							$('#caricoa').hide();
							$('#update').prop("disabled", true);
							$('#save').prop("disabled", true);
						} else {
							Toast.fire({
								icon: 'error',
								title: data.message
							});
						}
					}
				}, false)
			}

		})

		/* Cari Data Coa*/
		document.getElementById("caricoa").addEventListener("click", function(event) {
			event.preventDefault();
			$('#t_coa').DataTable({
				"destroy": true,
				"searching": true,
				"processing": true,
				"serverSide": true,
				"lengthChange": true,
				"pageLength": 5,
				"lengthMenu": [5, 10, 25, 50],
				"order": [],
				"ajax": {
					"url": "<?= base_url('masterdata/Coa/CariDataCoa'); ?>",
					"method": "POST",
					"data": {
						nmtb: "glbm_accountlainlain",
						field: {
							nomor: "nomor",
							nama: "nama",
							jenis: "jenis"
						},
						sort: "nomor",
						where: {
							nomor: "nomor"
						},
						value: "aktif = true"
					},
				},
				"columnDefs": [{
					"targets": 3,
					"data": "jenis",
					"render": function(data, type, row, meta) {
						return (row[3] == 1 ? 'Assets' : (row[3] == 2 ? 'Liabilities & Capital' : (row[3] == 3 ? 'Income' : (row[3] == 4 ? 'Expense' : (row[3] == 5 ? 'Additional/Others' : '')))));
					}
				}]
			});
		});

		/* Cari Data Satuan*/
		document.getElementById("caricabang").addEventListener("click", function(event) {
			event.preventDefault();
			$('#t_cabang').DataTable({
				"destroy": true,
				"searching": true,
				"processing": true,
				"serverSide": true,
				"lengthChange": true,
				"pageLength": 5,
				"lengthMenu": [5, 10, 25, 50],
				"order": [],
				"ajax": {
					"url": "<?= base_url('masterdata/Cabang/CariDataCabang'); ?>",
					"method": "POST",
					"data": {
						nmtb: "glbm_cabang",
						field: {
							kode: "kode",
							nama: "nama",
							// qtysatuan: "qtysatuan",
							// konversi: "konversi",
						},
						sort: "kode",
						where: {
							kode: "kode",
							nama: "nama",
						},
						value: "aktif = true"
					},
				}
			});
		});

		/*Get Data Satuan */
		$(document).on('click', ".searchcabang", function() {
			var kode = $(this).attr("data-id");;
			$.ajax({
				url: "<?= base_url('masterdata/Cabang/DataCabang'); ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					kode: kode
				},
				success: function(data) {
					for (var i = 0; i < data.length; i++) {
						$('#kodecabang').val(data[i].kode.trim());
					}
				}
			}, false);
		});

		/*Get Data Coa*/
		$(document).on('click', ".searchcoa", function() {
			var nomor = $(this).attr("data-id");;
			$.ajax({
				url: "<?= base_url('masterdata/Coa/DataCoa'); ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					nomor: nomor
				},
				success: function(data) {
					for (var i = 0; i < data.length; i++) {
						$('#coa').val(data[i].nomor.trim());
					}
				}
			}, false);
		});

		/* Cari Data Rekening Transfer*/
		document.getElementById("find").addEventListener("click", function(event) {
			event.preventDefault();
			$('#t_rekening').DataTable({
				"destroy": true,
				"searching": true,
				"processing": true,
				"serverSide": true,
				"lengthChange": true,
				"pageLength": 5,
				"lengthMenu": [5, 10, 25, 50],
				"order": [],
				"ajax": {
					"url": "<?= base_url('masterdata/Rekening_transfer/CariDataRekening'); ?>",
					"method": "POST",
					"data": {
						nmtb: "cari_account",
						field: {
							nomor: "nomor",
							nama: "nama",
							jenisaccount: "jenisaccount",
							norekening: "norekening",
							namacabang: "namacabang",
							keterangan: "keterangan",
							aktif: "aktif"

						},
						sort: "nomor",
						where: {
							nomor: "nomor",
							nama: "nama",
						},
						// value: "aktif = true"
					},
				},
				"columnDefs": [{
					"targets": 7,
					"data": "aktif",
					"render": function(data, type, row, meta) {
						return (row[7] == 't') ? 'Aktif' : 'Tidak Aktif';
					}
				}, {
					"targets": 3,
					"data": "jenisaccount",
					"render": function(data, type, row, meta) {
						return (row[3] == 0) ? 'Tunai' : 'Bank';
					}
				}],
			});
		});

		/*Get Data Rekening Transfer*/
		$(document).on('click', ".searchrekening", function() {
			var norek = $(this).attr("data-id");;
			$.ajax({
				url: "<?= base_url('masterdata/Rekening_transfer/DataRekening'); ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					norek: norek
				},
				success: function(data) {
					for (var i = 0; i < data.length; i++) {
						$('#norek').val(data[i].norekening.trim());
						$('#jenisacc').val(data[i].jenisaccount.trim());
						$('#nama').val(data[i].nama.trim());
						$('#keterangan').val(data[i].keterangan.trim());
						$('#kodecabang').val(data[i].kode_cabang.trim());
						$('#coa').val(data[i].nomor.trim());
						if (data[i].aktif.trim() == 't') {
							$('#aktif').prop('checked', true);
						} else {
							$('#tidak_aktif').prop('checked', true);
						}
					}
					$('#update').prop('disabled', false);
					$('#save').prop('disabled', true);
					$('#norek').prop('disabled', true);
					$('#caricoa').hide();
					$('.aktif').show();
				}
			}, false);
		});

		/* Update */
		document.getElementById('update').addEventListener("click", function(event) {
			event.preventDefault();
			var norek = $('#norek').val();
			var jenisacc = $('#jenisacc').val();
			var nama = $('#nama').val();
			var namabisnis = $('#namabisnis').val();
			var keterangan = $('#keterangan').val();
			var kodecabang = $('#kodecabang').val();
			var coa = $('#coa').val();
			var aktif = $('input[name="aktif"]:checked').val();

			if (ValidasiSave() == true) {
				$.ajax({
					url: "<?= base_url("masterdata/Rekening_transfer/Update") ?>",
					method: "POST",
					dataType: "json",
					async: true,
					data: {
						norek: norek,
						jenisacc: jenisacc,
						nama: nama,
						namabisnis: namabisnis,
						keterangan: keterangan,
						kodecabang: kodecabang,
						coa: coa,
						aktif: aktif
					},
					success: function(data) {
						// console.log(data)
						if (data.nomor != "") {
							Toast.fire({
								icon: 'success',
								title: data.message
							});
							$('#norek').prop('disabled', true);
							$('#namabank').prop('disabled', true);
							$('#nama').prop('disabled', true);
							$('#aktif').prop('disabled', true);

							$('#caricoa').hide();
							$('#update').prop("disabled", true);
							$('#save').prop("disabled", true);
						} else {
							Toast.fire({
								icon: 'error',
								title: data.message
							});
						}
					}
				}, false)
			}
		});

		/* Clear */
		document.getElementById("clear").addEventListener("click", function(event) {
			event.preventDefault();
			ClearScreen();
		});

		ClearScreen();

	});
</script>
