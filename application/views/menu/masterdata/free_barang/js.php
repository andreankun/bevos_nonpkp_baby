<script>
	$(document).ready(function() {
		function ClearScreen() {
			// $('#kode').prop('disabled', false).val("");
			$('#kode').prop('disabled', false).val("");
			$('#nama').prop('disabled', false).val("");
			$('#kodebarang1').prop('disabled', false).val("");
			$('#namabarang1').prop('disabled', false).val("");
			$('#qty').prop('disabled', false).val("");
			// $('#kodemerk').prop('disabled', false).val("");
			$('#aktif').prop('disabled', false);

			$('#update').prop('disabled', true);
			$('#save').prop('disabled', false);
			$('#tb_detail').empty();

			$('.aktif').hide();
		}

		// /* Maxlength Nomor HP */
		// $('#diskon[max]:not([max=""])').on('input', function(event) {
		//     var $this = $(this);
		//     var maxlength = $this.attr('max').length;
		//     var value = $this.val();
		//     if (value && value.length >= maxlength) {
		//         $this.val(value.substr(0, maxlength));
		//     }
		// })

		function ValidasiSave(datadetail) {
			var kodebarang = $('#kode').val();
			var namabarang = $('#nama').val();
			var qty = $('#qty').val();

			// if (kode == '' || kode == 0) {
			//     Swal.fire({
			//         title: 'Informasi',
			//         icon: 'info',
			//         html: 'Kode tidak boleh kosong',
			//         showCloseButton: true,
			//         width: 350
			//     });
			//     $('#kode').focus();
			//     var result = false;
			// } else 
			if (kodebarang == '' || kodebarang == 0) {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Nomor Pelanggan tidak boleh kosong',
					showCloseButton: true,
					width: 350
				});
				$('#kodebarang').focus();
				var result = false;
			} else if (namabarang == '' || namabarang == 0) {
				Swal.fire({
					title: 'informasi',
					icon: 'info',
					html: 'Nama Pelanggan tidak boleh kosong',
					showCloseButton: true,
					width: 350
				});
				$('#namabarang').focus();
				var result = false;
			} else if (datadetail.length == 0 || datadetail.length == '0') {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Data Detail tidak boleh kosong.',
					showCloseButton: true,
					width: 350,
				});
				$('#namagudang').focus();
				result = false;
			} else if (qty == '' || qty == 0) {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Diskon Tidak boleh kosong',
					showCloseButton: true,
					width: 350
				});
				$('#qty').focus();
				var result = false;
			} else {
				var result = true;
			}

			return result;
		}

		/* Validasi Data Add Detail */
		function ValidasiAddDetail(kodebarang) {
			var table = document.getElementById('t_detail');
			var kodebarang1 = $('#kodebarang1').val();
			var namabarang1 = $('#namabarang1').val();
			var qty1 = $('#qty1').val();

			if (kodebarang1 == '' || kodebarang1 == 0) {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Kode Barang tidak boleh kosong.',
					showCloseButton: true,
					width: 350,
				});
				$('#kodebarang1').focus();
				return "gagal";
			} else if (namabarang1 == '' || namabarang1 == 0) {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Nama Barang tidak boleh kosong.',
					showCloseButton: true,
					width: 350,
				});
				$('#namabarang1').focus();
				return "gagal";
			} else if (qty1 == '' || qty1 == 0) {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Nama Barang tidak boleh kosong.',
					showCloseButton: true,
					width: 350,
				});
				$('#namabarang').focus();
				return "gagal";
			} else {
				for (var r = 1, n = table.rows.length; r < n; r++) {
					var string = "";
					for (var c = 0, m = table.rows[r].cells.length; c < m; c++) {
						if (c == 0) {
							if (table.rows[r].cells[c].innerHTML == kodebarang1) {
								Swal.fire({
									title: 'Informasi',
									icon: 'info',
									html: 'Data ini sudah diinput.',
									showCloseButton: true,
									width: 350,
								});
								return "gagal";
							}
						}
					}
				}
			}
			return "sukses"
		}

		/* Validasi Data Add Detail */
		function ValidasiAddHeader(kodebarang) {
			var table = document.getElementById('t_header');
			var kodebarang = $('#kode').val();
			var namabarang = $('#nama').val();
			var qty = $('#qty').val();

			// if (kode == '' || kode == 0) {
			//     Swal.fire({
			//         title: 'Informasi',
			//         icon: 'info',
			//         html: 'Kode tidak boleh kosong',
			//         showCloseButton: true,
			//         width: 350
			//     });
			//     $('#kode').focus();
			//     var result = false;
			// } else 
			if (kodebarang == '' || kodebarang == 0) {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Nomor Pelanggan tidak boleh kosong',
					showCloseButton: true,
					width: 350
				});
				$('#kodebarang').focus();
				return "gagal";
			} else if (namabarang == '' || namabarang == 0) {
				Swal.fire({
					title: 'informasi',
					icon: 'info',
					html: 'Nama Pelanggan tidak boleh kosong',
					showCloseButton: true,
					width: 350
				});
				$('#namabarang').focus();
				return "gagal";
			} else if (qty == '' || qty == 0) {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Diskon Tidak boleh kosong',
					showCloseButton: true,
					width: 350
				});
				$('#qty').focus();
				return "gagal";
			} else {
				for (var r = 1, n = table.rows.length; r < n; r++) {
					var string = "";
					for (var c = 0, m = table.rows[r].cells.length; c < m; c++) {
						if (c == 0) {
							if (table.rows[r].cells[c].innerHTML == kodebarang) {
								Swal.fire({
									title: 'Informasi',
									icon: 'info',
									html: 'Data ini sudah diinput.',
									showCloseButton: true,
									width: 350,
								});
								return "gagal";
							}
						}
					}
				}
			}
			return "sukses"
		}

		/* Declare Toast */
		const Toast = Swal.mixin({
			toast: true,
			position: 'top-end',
			showConfirmButton: false,
			timer: 3000,
			timerProgressBar: true,
			didOpen: (toast) => {
				toast.addEventListener('mouseenter', Swal.stopTimer)
				toast.addEventListener('mouseleave', Swal.resumeTimer)
			}
		});

		$('#diskon').mask('99.99%', {
			reverse: true
		});

		$("#adddetail").click(function() {

			var kodebarang1 = $('#kodebarang1').val();
			var namabarang1 = $('#namabarang1').val();
			var qty1 = $('#qty1').val();
			var kodepencarian = kodebarang1.replace(/[^A-Z0-9]/ig, '');
			if (ValidasiAddDetail(kodebarang1) == "sukses") {
				var row = "";
				row =
					'<tr id="' + kodepencarian + '">' +
					'<td nowrap>' + kodebarang1 + '</td>' +
					'<td nowrap>' + namabarang1 + '</td>' +
					'<td nowrap>' + qty1 + '</td>' +
					'<td nowrap style="text-align: center; width: 200px;">' +
					// '<button data-table="' + kodebarang + '" data-toggle="modal" data-target="#changediskon" class="editdetail btn btn-success m-1 p-1"><i class="fas fa-edit"></i> Edit</button>' +
					'<button data-table="' + kodepencarian + '" class="deldetail btn btn-danger m-1 p-1"><i class="fa fa-trash mr-1"></i> Delete</button>' +
					'</td>' +
					'</tr>';
				$('#tb_detail').append(row);
				$('#kodebarang1').val("");
				$('#namabarang1').val("");
				$('#qty1').val("");
			}
		});

		$(document).on('click', '.deldetail', function() {
			var id = $(this).attr("data-table");
			$('#' + id).remove();
		});

		function AmbilDataDetail() {
			var table = document.getElementById('t_detail');
			var arr2 = [];
			for (var r = 1, n = table.rows.length; r < n; r++) {
				var string = "";
				for (var c = 0, m = table.rows[r].cells.length; c < m - 1; c++) {
					if (c == 0) {
						string = "{" + table.rows[0].cells[c].innerHTML + " : '" + table.rows[r].cells[c].innerHTML + "'";
					} else {
						string = string + ", " + table.rows[0].cells[c].innerHTML + " : '" + table.rows[r].cells[c].innerHTML + "'";
					}
				}
				string = string + "}";
				var obj = JSON.stringify(eval('(' + string + ')'));
				var arr = $.parseJSON(obj);
				arr2.push(arr);
			}
			return arr2;
		};

		function GetDataDetail(kode) {
			$.ajax({
				url: "<?= base_url('masterdata/FreeBarang/DataFreeBarangDetail') ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					kode: kode,
				},
				success: function(data) {
					console.log(data);
					for (var i = 0; i < data.length; i++) {
						// var kode = data[i].kode;
						var kodebarang1 = data[i].kodebarang;
						var namabarang1 = data[i].namabarang.trim();
						var qty1 = data[i].qty.trim();

						InsertDataDetail(kodebarang1, namabarang1, qty1);
					}
				}
			});
		};

		function InsertDataDetail(kodebarang1, namabarang1, qty1) {
			var kodepencarian = kodebarang1.replace(/[^A-Z0-9]/ig, '');
			var row = "";
			row =
				'<tr id="' + kodepencarian + '">' +
				'<td nowrap>' + kodebarang1 + '</td>' +
				'<td nowrap>' + namabarang1 + '</td>' +
				'<td nowrap>' + qty1 + '</td>' +
				'<td nowrap style="text-align: center; width: 200px;">' +
				// '<button data-table="' + kodebarang + '" data-toggle="modal" data-target="#changediskon" class="editdetail btn btn-success m-1 p-1"><i class="fas fa-edit"></i> Edit</button>' +
				'<button data-table="' + kodepencarian + '" class="deldetail btn btn-danger m-1 p-1"><i class="fa fa-trash mr-1"></i> Delete</button>' +
				'</td>' +
				'</tr>';
			$('#tb_detail').append(row);
		};

		$("#addheader").click(function() {

			var kodebarang = $('#kode').val();
			var namabarang = $('#nama').val();
			// var qty = $('#qty').val();
			// var cabang = $('#kodecabang').val();
			var kodepencarian = kodebarang.replace(/[^A-Z0-9]/ig, '');
			if (ValidasiAddHeader(kodebarang) == "sukses") {
				var row = "";
				row =
					'<tr id="' + kodepencarian + '">' +
					'<td nowrap>' + kodebarang + '</td>' +
					'<td nowrap>' + namabarang + '</td>' +
					// '<td nowrap>' + qty + '</td>' +
					// '<td nowrap>' + cabang + '</td>' +
					'<td nowrap style="text-align: center; width: 200px;">' +
					// '<button data-table="' + kodebarang + '" data-toggle="modal" data-target="#changediskon" class="editdetail btn btn-success m-1 p-1"><i class="fas fa-edit"></i> Edit</button>' +
					'<button data-table="' + kodepencarian + '" class="delheader btn btn-danger m-1 p-1"><i class="fa fa-trash mr-1"></i> Delete</button>' +
					'</td>' +
					'</tr>';
				$('#tb_header').append(row);
				$('#kode').val("");
				$('#nama').val("");
				// $('#qty').val("");
				// $('#kodecabang').val("");
			}
		});

		$(document).on('click', '.delheader', function() {
			var id = $(this).attr("data-table");
			$('#' + id).remove();
		});

		function insertDataHeader(kodebarang1, namabarang1) {
			var kodepencarian = kodebarang1.replace(/[^A-Z0-9]/ig, '');
			var row = "";
			row =
				'<tr id="' + kodepencarian + '">' +
				'<td nowrap>' + kodebarang1 + '</td>' +
				'<td nowrap>' + namabarang1 + '</td>' +
				// '<td nowrap>' + qty + '</td>' +
				// '<td nowrap>' + kodecabang + '</td>' +
				'<td nowrap style="text-align: center; width: 200px;">' +
				// '<button data-table="' + kodebarang + '" data-toggle="modal" data-target="#changediskon" class="editdetail btn btn-success m-1 p-1"><i class="fas fa-edit"></i> Edit</button>' +
				'<button data-table="' + kodepencarian + '" class="delheader btn btn-danger m-1 p-1"><i class="fa fa-trash mr-1"></i> Delete</button>' +
				'</td>' +
				'</tr>';
			$('#tb_header').append(row);
		};

		function GetDataHeader(kode) {
			$.ajax({
				url: "<?= base_url('masterdata/FreeBarang/DataFreeBarangHeader') ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					kode: kode,
				},
				success: function(data) {
					console.log(data);
					for (var i = 0; i < data.length; i++) {
						// var kode = data[i].kode;
						var kodebarang1 = data[i].kodebarang;
						var namabarang1 = data[i].namabarang.trim();
						// var kodecabang = data[i].kodecabang.trim();

						insertDataHeader(kodebarang1, namabarang1);
					}
				}
			});
		};

		function AmbilDataHeader() {
			var table = document.getElementById('t_header');
			var arr2 = [];
			for (var r = 1, n = table.rows.length; r < n; r++) {
				var string = "";
				for (var c = 0, m = table.rows[r].cells.length; c < m - 1; c++) {
					if (c == 0) {
						string = "{" + table.rows[0].cells[c].innerHTML + " : '" + table.rows[r].cells[c].innerHTML + "'";
					} else {
						string = string + ", " + table.rows[0].cells[c].innerHTML + " : '" + table.rows[r].cells[c].innerHTML + "'";
					}
				}
				string = string + "}";
				var obj = JSON.stringify(eval('(' + string + ')'));
				var arr = $.parseJSON(obj);
				arr2.push(arr);
			}
			return arr2;
		};

		document.getElementById("caribarang").addEventListener("click", function(event) {
			event.preventDefault();
			$('#t_barang').DataTable({
				"destroy": true,
				"searching": true,
				"processing": true,
				"serverSide": true,
				"lengthChange": true,
				"pageLength": 5,
				"lengthMenu": [5, 10, 25, 50],
				"order": [],
				"ajax": {
					"url": "<?= base_url('masterdata/Barang/CariDataBarang'); ?>",
					"method": "POST",
					"data": {
						nmtb: "glbm_barang",
						field: {
							kode: "kode",
							nama: "nama",
							kodesatuan: "kodesatuan",
							kodemerk: "kodemerk",
						},
						sort: "kode",
						where: {
							kode: "kode",
							nama: "nama",
							kodesatuan: "kodesatuan",
							kodemerk: "kodemerk",
						}
					},
				}
			});
		});

		$(document).on('click', ".searchbarang", function() {
			var kode = $(this).attr("data-id");
			$.ajax({
				url: "<?= base_url('masterdata/Barang/DataBarang') ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					kode: kode
				},
				success: function(data) {
					for (var i = 0; i < data.length; i++) {
						$('#kode').val(data[i].kode.trim());
						$('#nama').val(data[i].nama.trim());
					}
				}
			}, false);
		});

		document.getElementById("caribarang1").addEventListener("click", function(event) {
			event.preventDefault();
			$('#t_barang1').DataTable({
				"destroy": true,
				"searching": true,
				"processing": true,
				"serverSide": true,
				"lengthChange": true,
				"pageLength": 5,
				"lengthMenu": [5, 10, 25, 50],
				"order": [],
				"ajax": {
					"url": "<?= base_url('masterdata/FreeBarang/CariDataBarang'); ?>",
					"method": "POST",
					"data": {
						nmtb: "glbm_barang",
						field: {
							kode: "kode",
							nama: "nama",
							kodesatuan: "kodesatuan",
							kodemerk: "kodemerk",
						},
						sort: "kode",
						where: {
							kode: "kode",
							nama: "nama",
							kodesatuan: "kodesatuan",
							kodemerk: "kodemerk",
						}
					},
				}
			});
		});

		$(document).on('click', ".seacrhb", function() {
			var kode = $(this).attr("data-id");
			$.ajax({
				url: "<?= base_url('masterdata/FreeBarang/DataBarang') ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					kode: kode
				},
				success: function(data) {
					for (var i = 0; i < data.length; i++) {
						$('#kodebarang1').val(data[i].kode.trim());
						$('#namabarang1').val(data[i].nama.trim());
					}
				}
			}, false);
		});

		document.getElementById("find").addEventListener("click", function(event) {
			event.preventDefault();
			$('#tb_detail').empty();
			$('#tb_header').empty();
			$('#t_diskon').DataTable({
				"destroy": true,
				"searching": true,
				"processing": true,
				"serverSide": true,
				"lengthChange": true,
				"pageLength": 5,
				"lengthMenu": [5, 10, 25, 50],
				"order": [],
				"ajax": {
					"url": "<?= base_url('masterdata/FreeBarang/CariDataFreeBarang'); ?>",
					"method": "POST",
					"data": {
						nmtb: "glbm_freebarang",
						field: {
							// kode: "kode",
							kode: "kode",
							kodecabang: "kodecabang",
							aktif: "aktif"
						},
						sort: "kode",
						where: {
							kode: "kode",
							// nama: "nama",
						},
                        value: "kodecabang = '" + $('#cabang').val() + "'"
					},
				},
				"columnDefs": [{
					"targets": 3,
					"data": "aktif",
					"render": function(data, type, row, meta) {
						return (row[3] == 't') ? 'Aktif' : 'Tidak Aktif';
					}
				}],
			});
		});

		$(document).on('click', ".searchfree", function() {
			var kode = $(this).attr("data-id");
			console.log(kode);
			var cabang = $('#kodecabang').val();
			$.ajax({
				url: "<?= base_url('masterdata/FreeBarang/DataFreeBarang') ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					kode: kode
				},
				success: function(data) {
					for (var i = 0; i < data.length; i++) {
						// $('#kode').val(data[i].kode.trim());
						$('#kodepromo').val(data[i].kode.trim());
						// $('#nama').val(data[i].nama.trim());
						// $('#kodebarang').val(data[i].kodebarang.trim());
						// $('#namabarang').val(data[i].namabarang.trim());
						$('#qty').val(data[i].qty.trim());
						if (data[i].aktif.trim() == 't') {
							$('#aktif').prop('checked', true);
						} else {
							$('#tidak_aktif').prop('checked', true);
						}
					}
					GetDataDetail(kode);
					GetDataHeader(kode);
					$('#save').prop('disabled', true);
					$('#update').prop('disabled', false);
					$('#kode').prop('disabled', true);
					$('.aktif').show();
				}
			});
		});

		document.getElementById("save").addEventListener("click", function(event) {
			event.preventDefault();
			var kode = $('#kode').val();
			var kodecabang = $('#kodecabang').val();
			var qty = $('#qty').val();
			var dataheader = AmbilDataHeader();
			var datadetail = AmbilDataDetail();

			$.ajax({
				url: "<?= base_url("masterdata/FreeBarang/Save") ?>",
				method: "POST",
				dataType: "json",
				async: true,
				data: {
					kode: kode,
					kodecabang: kodecabang,
					qty:qty,
					dataheader: dataheader,
					datadetail: datadetail
				},
				success: function(data) {
					if (data.dataheader != "") {
						Toast.fire({
							icon: 'success',
							title: data.message
						});
						// $('#kode').prop('disabled', true);
						// $('#kodebarang').prop('disabled', true);
						// $('#namabarang').prop('disabled', true);
						// $('#qty').prop('disabled', true);
						$('#aktif').prop('disabled', true);

						$('#update').prop("disabled", true);
						$('#save').prop("disabled", true);
					} else {
						Toast.fire({
							icon: 'error',
							title: data.message
						});
					}
				}
			}, false)
		})


		document.getElementById('update').addEventListener("click", function(event) {
			event.preventDefault();
			var kode = $('#kodepromo').val();
			console.log(kode);
			// var kodebarang = $('#kode').val();
			// var namabarang = $('#nama').val();
			var qty = $('#qty').val();
			var dataheader = AmbilDataHeader();
			var kodecabang = $('#kodecabang').val();
			var aktif = $('input[name="aktif"]:checked').val();
			var datadetail = AmbilDataDetail();

			$.ajax({
					url: "<?= base_url("masterdata/FreeBarang/Update") ?>",
					method: "POST",
					dataType: "json",
					async: true,
					data: {
						kode: kode,
						// kodebarang: kodebarang,
						// namabarang: namabarang,
						qty: qty,
						kodecabang: kodecabang,
						aktif: aktif,
						datadetail: datadetail,
						dataheader:dataheader
					},
					success: function(data) {
						if (data.dataheader != "") {
							Toast.fire({
								icon: 'success',
								title: data.message
							});
							$('#kode').prop('disabled', true);
							// $('#kodebarang').prop('disabled', true);
							// $('#namabarang').prop('disabled', true);
							$('#qty').prop('disabled', true);
							$('#aktif').prop('disabled', true);

							$('#update').prop("disabled", true);
							$('#save').prop("disabled", true);
						} else {
							Toast.fire({
								icon: 'error',
								title: data.message
							});
						}
					}
				}), false;
		})

		document.getElementById("clear").addEventListener("click", function(event) {
			event.preventDefault();
			ClearScreen();
		});
		ClearScreen();
	});
</script>
