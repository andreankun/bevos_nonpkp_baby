<script type="text/javascript">
	$(document).ready(function() {

		/* Function ClearScreen */
		function ClearScreen() {
			$('#kode').prop('disabled', false).val("");
			$('#nama').prop('disabled', false).val("");
			// $('#barcode1').prop('disabled', false).val("");
			// $('#barcode2').prop('disabled', false).val("");
			$('#kodesatuan').prop('readonly', true).val("");
			$('#kodemerk').prop('readonly', true).val("");
			$('#komisi').prop('disabled', false).val("");
			$('#kodeitem').prop('disabled', false).val("");
			// $('#hargabeli').prop('disabled', false).val("");
			$('#keterangan').prop('disabled', false).val("");
			// $('#discitem').val("");
			// $('#kodegudang').prop('readonly', true).val("");
			// $('#namagudang').prop('readonly', true).val("");
			$('#aktif').prop('disabled', false);

			$('#update').prop('disabled', true);
			$('#save').prop('disabled', false);

			$('#carisatuan').show();
			$('#carimerk').show();

			$('.aktif').hide();
		};

		/* Validasi Kosong */
		function ValidasiSave() {
			var kode = $('#kode').val();
			var nama = $('#nama').val();
			// var barcode1 = $('#barcode1').val();
			// var barcode2 = $('#barcode2').val();
			var kodesatuan = $('#kodesatuan').val();
			var kodemerk = $('#kodemerk').val();
			var keterangan = $('#keterangan').val();
			// var kodeitem = $('#kodeitem').val();

			if (kode == '' || kode == 0) {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Kode tidak boleh kosong.',
					showCloseButton: true,
					width: 350,
				});
				$('#kode').focus();
				var result = false;
			} else if (nama == '' || nama == 0) {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Nama tidak boleh kosong.',
					showCloseButton: true,
					width: 350,
				});
				$('#nama').focus();
				var result = false;
			} else if (kodesatuan == '') {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Kode Satuan tidak boleh kosong.',
					showCloseButton: true,
					width: 350,
				});
				$('#kodesatuan').focus();
				var result = false;
			} else if (kodemerk == '') {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Kode Merk tidak boleh kosong.',
					showCloseButton: true,
					width: 350,
				});
				$('#kodemerk').focus();
				var result = false;
			}
			// else if (keterangan == '') {
			// 	Swal.fire({
			// 		title: 'Informasi',
			// 		icon: 'info',
			// 		html: 'Keterangan tidak boleh kosong.',
			// 		showCloseButton: true,
			// 		width: 350,
			// 	});
			// 	$('#keterangan').focus();
			// 	var result = false;
			// }
			else {
				var result = true;
			}

			return result;
		}

		/* Declare Toast */
		const Toast = Swal.mixin({
			toast: true,
			position: 'top-end',
			showConfirmButton: false,
			timer: 3000,
			timerProgressBar: true,
			didOpen: (toast) => {
				toast.addEventListener('mouseenter', Swal.stopTimer)
				toast.addEventListener('mouseleave', Swal.resumeTimer)
			}
		});

		// $('#komisi').mask('99.99%', {
		//     reverse: true
		// });

		/* Cari Data Satuan*/
		document.getElementById("carisatuan").addEventListener("click", function(event) {
			event.preventDefault();
			$('#t_satuan').DataTable({
				"destroy": true,
				"searching": true,
				"processing": true,
				"serverSide": true,
				"lengthChange": true,
				"pageLength": 5,
				"lengthMenu": [5, 10, 25, 50],
				"order": [],
				"ajax": {
					"url": "<?= base_url('masterdata/Satuan/CariDataSatuan'); ?>",
					"method": "POST",
					"data": {
						nmtb: "glbm_satuan",
						field: {
							kode: "kode",
							nama: "nama",
							// qtysatuan: "qtysatuan",
							// konversi: "konversi",
						},
						sort: "kode",
						where: {
							kode: "kode",
							nama: "nama",
						},
						value: "aktif = true"
					},
				}
			});
		});

		/*Get Data Satuan */
		$(document).on('click', ".searchsatuan", function() {
			var kode = $(this).attr("data-id");;
			$.ajax({
				url: "<?= base_url('masterdata/Satuan/DataSatuan'); ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					kode: kode
				},
				success: function(data) {
					for (var i = 0; i < data.length; i++) {
						$('#kodesatuan').val(data[i].kode.trim());
					}
				}
			}, false);
		});

		/* Cari Data Merk*/
		document.getElementById("carimerk").addEventListener("click", function(event) {
			event.preventDefault();
			$('#t_merk').DataTable({
				"destroy": true,
				"searching": true,
				"processing": true,
				"serverSide": true,
				"lengthChange": true,
				"pageLength": 5,
				"lengthMenu": [5, 10, 25, 50],
				"order": [],
				"ajax": {
					"url": "<?= base_url('masterdata/Merk/CariDataMerk'); ?>",
					"method": "POST",
					"data": {
						nmtb: "glbm_merk",
						field: {
							kode: "kode",
							nama: "nama",
						},
						sort: "kode",
						where: {
							kode: "kode",
							nama: "nama",
						},
						value: "aktif = true"
					},
				}
			});
		});

		/*Get Data Merk */
		$(document).on('click', ".searchmerk", function() {
			var kode = $(this).attr("data-id");
			$.ajax({
				url: "<?= base_url('masterdata/Merk/DataMerk'); ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					kode: kode
				},
				success: function(data) {
					for (var i = 0; i < data.length; i++) {
						$('#kodemerk').val(data[i].kode.trim());
					}
				}
			}, false);
		});

		/* Save */
		document.getElementById('save').addEventListener("click", function(event) {
			event.preventDefault();
			var kode = $('#kode').val();
			var nama = $('#nama').val();
			// var barcode1 = $('#barcode1').val();
			// var barcode2 = $('#barcode2').val();
			var kodesatuan = $('#kodesatuan').val();
			var kodemerk = $('#kodemerk').val();
			var keterangan = $('#keterangan').val();
			var kodeitem = $('#kodeitem').val();
			// var datadetail = AmbilDataDetail();

			if (ValidasiSave() == true) {
				$.ajax({
					url: "<?= base_url("masterdata/Barang/Save") ?>",
					method: "POST",
					dataType: "json",
					async: true,
					data: {
						kode: kode,
						nama: nama,
						// barcode1: barcode1,
						// barcode2: barcode2,
						kodesatuan: kodesatuan,
						kodemerk: kodemerk,
						kodeitem: kodeitem,
						keterangan: keterangan,
					},
					success: function(data) {
						if (data.kode != "") {
							Toast.fire({
								icon: 'success',
								title: data.message
							});
							$('#kode').prop('disabled', true);
							$('#nama').prop('disabled', true);
							// $('#barcode1').prop('disabled', true);
							// $('#barcode2').prop('disabled', true);
							$('#kodesatuan').prop('readonly', true);
							$('#kodemerk').prop('readonly', true);
							$('#kodeitem').prop('readonly', true);
							$('#keterangan').prop('disabled', true);

							$('#update').prop("disabled", true);
							$('#save').prop("disabled", true);
						} else {
							Toast.fire({
								icon: 'error',
								title: data.message
							});
						}
					}
				}, false)
			}
		})

		document.getElementById("find").addEventListener("click", function(event) {
			event.preventDefault();
			$('#tb_detail').empty();
			$('#t_barang').DataTable({
				"destroy": true,
				"searching": true,
				"processing": true,
				"serverSide": true,
				"lengthChange": true,
				"pageLength": 5,
				"lengthMenu": [5, 10, 25, 50],
				"order": [],
				"ajax": {
					"url": "<?= base_url('masterdata/Barang/CariDataBarang'); ?>",
					"method": "POST",
					"data": {
						nmtb: "glbm_barang",
						field: {
							kode: "kode",
							nama: "nama",
							kodesatuan: "kodesatuan",
							kodemerk: "kodemerk",
							aktif: "aktif"
						},
						sort: "kode",
						where: {
							kode: "kode",
							nama: "nama",
							kodesatuan: "kodesatuan",
							kodemerk: "kodemerk",
						},
						// value: "aktif = true"
					},
				},
				"columnDefs": [{
					"targets": 5,
					"data": "aktif",
					"render": function(data, type, row, meta) {
						return (row[5] == 't') ? 'Aktif' : 'Tidak Aktif'
					}
				}],
			});
		});

		$(document).on('click', ".searchbarang", function() {
			var kode = $(this).attr("data-id");
			GetData(kode);
			$('#save').prop('disabled', true);
			$('#update').prop('disabled', false);
		});

		function GetData(kode) {
			$.ajax({
				url: "<?= base_url('masterdata/Barang/DataBarang'); ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					kode: kode
				},
				success: function(data) {
					console.log(data);
					for (var i = 0; i < data.length; i++) {
						$('#kode').val(data[i].kode.trim());
						$('#nama').val(data[i].nama.trim());
						// $('#barcode1').val(data[i].barcode1.trim());
						// $('#barcode2').val(data[i].barcode2.trim());
						$('#kodesatuan').val(data[i].kodesatuan.trim());
						$('#kodemerk').val(data[i].kodemerk.trim());
						$('#kodeitem').val(data[i].kode_item.trim());
						$('#keterangan').val(data[i].keterangan.trim());
						if (data[i].aktif.trim() == 't') {
							$('#aktif').prop('checked', true);
						} else {
							$('#tidak_aktif').prop('checked', true);
						}

						if (data[i].stock > 0) {
							$('#update').prop('disabled', false);
							$('#save').prop('disabled', true);
							$('#kode').prop('disabled', true);
							$('#nama').prop('disabled', false);
							$('#carisatuan').hide();
						} else {
							$('#update').prop('disabled', false);
							$('#save').prop('disabled', true);
							$('#kode').prop('disabled', true);
							$('#nama').prop('disabled', false);
							$('#carisatuan').show();
						}
					}
					// GetDataDetail(kode);

					// $('#carisatuan').hide();
					$('.aktif').show();
				}
			});
		};

		/* Update */
		document.getElementById('update').addEventListener("click", function(event) {
			event.preventDefault();
			var kode = $('#kode').val();
			var nama = $('#nama').val();
			// var barcode1 = $('#barcode1').val();
			// var barcode2 = $('#barcode2').val();
			var kodesatuan = $('#kodesatuan').val();
			var kodemerk = $('#kodemerk').val();
			var kodeitem = $('#kodeitem').val();
			var keterangan = $('#keterangan').val();
			// var lokasi = $('#lokasi').val();
			// var hargabeli = $('#hargabeli').val();
			// var hargajual = $('#hargajual').val();
			// var kodegudang = $('#kodegudang').val();
			// var datadetail = AmbilDataDetail();
			var aktif = $('input[name="aktif"]:checked').val();

			if (ValidasiSave() == true) {
				$.ajax({
					url: "<?= base_url("masterdata/Barang/Update") ?>",
					method: "POST",
					dataType: "json",
					async: true,
					data: {
						kode: kode,
						nama: nama,
						// barcode1: barcode1,
						keterangan: keterangan,
						kodesatuan: kodesatuan,
						kodemerk: kodemerk,
						kodeitem: kodeitem,
						aktif: aktif
					},
					success: function(data) {
						if (data.kode != "") {
							Toast.fire({
								icon: 'success',
								title: data.message
							});
							$('#kode').prop('disabled', true);
							$('#nama').prop('disabled', true);
							// $('#barcode1').prop('disabled', true);
							// $('#barcode2').prop('disabled', true);
							$('#kodesatuan').prop('readonly', true);
							$('#kodemerk').prop('readonly', true);
							$('#kodeitem').prop('readonly', true);
							$('#keterangan').prop('disabled', true);
							$('#aktif').prop('disabled', true);

							$('#update').prop("disabled", true);
							$('#save').prop("disabled", true);
						} else {
							Toast.fire({
								icon: 'error',
								title: data.message
							});
						}
					}
				}, false)
			}

		});

		/* Clear */
		document.getElementById("clear").addEventListener("click", function(event) {
			event.preventDefault();
			ClearScreen();
		});

		ClearScreen();

	});
</script>
