<script type="text/javascript">
	$(document).ready(function() {

		/* Function ClearScreen */
		function ClearScreen() {
			$('#kode').prop("disabled", false).val("");
			$('#nama').prop("disabled", false).val("");
			$('#diskon').prop("disabled", false).val("");
			$('#aktif').prop("disabled", false);

			$('input[name="tokosendiri"]:checked').prop('checked', false);
			$('input[name="diskonatas"]:checked').prop('checked', false);
			$('input[name="tokosendiri"]').prop('disabled', false);
			$('input[name="diskonatas"]').prop('disabled', false);

			$('#update').prop("disabled", true);
			$('#save').prop("disabled", false);

			$('.aktif').hide();
		}

		/* Validasi Kosong */
		function ValidasiSave() {
			var nama = $('#nama').val();

			if (nama == '' || nama == 0) {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Nama tidak boleh kosong.',
					showCloseButton: true,
					width: 350,
				});
				$('#nama').focus();
				var result = false;
			} else {
				var result = true;
			}
			return result;
		}

		$('#diskon').mask('99.99%', {
			reverse: true
		});

		$('input[name="tokosendiri"]').change(function() {
			var tokosendiri = $('input[name="tokosendiri"]:checked').val();
			if (tokosendiri == 'true') {
				$('#diskon').prop('disabled', false).val("");
			} else {
				$('#diskon').prop('disabled', true).val(0 + '%');
			}
		})

		/* Declare Toast */
		const Toast = Swal.mixin({
			toast: true,
			position: 'top-end',
			showConfirmButton: false,
			timer: 3000,
			timerProgressBar: true,
			didOpen: (toast) => {
				toast.addEventListener('mouseenter', Swal.stopTimer)
				toast.addEventListener('mouseleave', Swal.resumeTimer)
			}
		});

		/* Save */
		document.getElementById('save').addEventListener("click", function(event) {
			event.preventDefault();
			var kode = $('#kode').val();
			var nama = $('#nama').val();
			var tokosendiri = $('input[name="tokosendiri"]:checked').val();
			var diskonatas = $('input[name="diskonatas"]:checked').val();
			var diskon = $('#diskon').val();

			if (ValidasiSave() == true) {
				$.ajax({
					url: "<?= base_url("masterdata/GrupCustomer/Save") ?>",
					method: "POST",
					dataType: "json",
					async: true,
					data: {
						kode: kode,
						nama: nama,
						tokosendiri: tokosendiri,
						diskonatas: diskonatas,
						diskon: diskon
					},
					success: function(data) {
						if (data.kode != "") {
							Toast.fire({
								icon: 'success',
								title: data.message
							});
							$('#kode').val(data.kode);
							$('#nama').prop("disabled", true);
							$('input[name="tokosendiri"]').prop('disabled', true);
							$('input[name="diskonatas"]').prop('disabled', true);
							$('#diskon').prop("disabled", true);
							$('#update').prop("disabled", true);
							$('#save').prop("disabled", true);
						} else {
							Toast.fire({
								icon: 'error',
								title: data.message
							});
						}
					}
				}, false)
			}

		})

		/* Cari Data */
		document.getElementById("find").addEventListener("click", function(event) {
			event.preventDefault();
			$('#t_gruppelanggan').DataTable({
				"destroy": true,
				"searching": true,
				"processing": true,
				"serverSide": true,
				"lengthChange": true,
				"pageLength": 5,
				"lengthMenu": [5, 10, 25, 50],
				"order": [],
				"ajax": {
					"url": "<?= base_url('masterdata/GrupCustomer/CariDataGrupCustomer'); ?>",
					"method": "POST",
					"data": {
						nmtb: "glbm_grupcustomer",
						field: {
							kode: "kode",
							nama: "nama",
							aktif: "aktif",
						},
						sort: "kode",
						where: {
							kode: "kode",
							nama: "nama",
						},
						value: "aktif = true"
					},
				},
				'columnDefs': [{
					"targets": 3,
					"data": "aktif",
					"render": function(data, type, row, meta) {
						return (row[3] == "t") ? "<p>Aktif</p>" : "<p>Tidak Aktif</p>"
					}
				}]
			});
		});

		/*Get Data*/
		$(document).on('click', ".searchgrupcustomer", function() {
			var kode = $(this).attr("data-id");;
			$.ajax({
				url: "<?= base_url('masterdata/GrupCustomer/DataGrupCustomer'); ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					kode: kode
				},
				success: function(data) {
					for (var i = 0; i < data.length; i++) {
						$('#kode').val(data[i].kode.trim());
						$('#nama').val(data[i].nama.trim());
						$('#diskon').val(data[i].diskon.trim() + '%');
						if (data[i].aktif.trim() == 't') {
							$('#aktif').prop('checked', true);
						} else {
							$('#tidak_aktif').prop('checked', true);
						}
						if (data[i].tokosendiri.trim() == 't') {
							$('#yes_tokosendiri').prop('checked', true);
							$('#diskon').prop('disabled', false);
						} else {
							$('#tidak_tokosendiri').prop('checked', true);
							$('#diskon').prop('disabled', true);
						}
						if (data[i].discountatas.trim() == 't') {
							$('#yes_diskonatas').prop('checked', true);
						} else {
							$('#tidak_diskonatas').prop('checked', true);
						}
					}
					$('#update').prop('disabled', false);
					$('#save').prop('disabled', true);
					$('#kode').prop('disabled', true);
					// $('#yes_tokosendiri').prop('disabled', true);
					// $('#tidak_tokosendiri').prop('disabled', true);
					// $('#yes_diskonatas').prop('disabled', true);
					// $('#tidak_diskonatas').prop('disabled', true);
					$('.aktif').show();
				}
			}, false);
		});

		/* Update */
		document.getElementById('update').addEventListener("click", function(event) {
			event.preventDefault();
			var kode = $('#kode').val();
			var nama = $('#nama').val();
			var tokosendiri = $('input[name="tokosendiri"]:checked').val();
			var diskonatas = $('input[name="diskonatas"]:checked').val();
			var diskon = $('#diskon').val();
			var aktif = $('input[name="aktif"]:checked').val();

			if (ValidasiSave() == true) {
				$.ajax({
					url: "<?= base_url("masterdata/GrupCustomer/Update") ?>",
					method: "POST",
					dataType: "json",
					async: true,
					data: {
						kode: kode,
						nama: nama,
						tokosendiri: tokosendiri,
						diskonatas: diskonatas,
						diskon: diskon,
						aktif: aktif
					},
					success: function(data) {
						if (data.kode != "") {
							Toast.fire({
								icon: 'success',
								title: data.message
							});
							$('#kode').val(data.kode);
							$('#nama').prop("disabled", true);
							$('input[name="tokosendiri"]').prop('disabled', true);
							$('input[name="diskonatas"]').prop('disabled', true);
							$('#diskon').prop("disabled", true);
							$('#aktif').prop("disabled", true);

							$('#update').prop("disabled", true);
							$('#save').prop("disabled", true);
						} else {
							Toast.fire({
								icon: 'error',
								title: data.message
							});
						}
					}
				}, false)
			}

		});

		/* Clear */
		document.getElementById('clear').addEventListener("click", function(event) {
			event.preventDefault();
			ClearScreen();
		})

		ClearScreen();
	});
</script>