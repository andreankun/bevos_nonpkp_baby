<script type="text/javascript">
    $(document).ready(function() {

        /* Function ClearScreen */
        function ClearScreen() {
            $('#kode').prop('disabled', false).val("");
            $('#nama').prop('disabled', false).val("");
            $('#diskon').prop('disabled', false).val("");
            // $('#harga').prop('disabled', false).val("");
            $('#aktif').prop('disabled', false);

            $('#update').prop('disabled', true);
            $('#save').prop('disabled', false);

            $('.aktif').hide();
            $('#tb_detail').empty();
            $('#adddetail').show();
            $('#caribarang').show();
        };

        /* Format Rupiah */
        function FormatRupiah(angka, prefix) {
            if (!angka) {
                return '';
            }
            var vangka = angka.toString();
            var number_string = vangka.replace(/[^.\d]/g, '').replace(/[^\w\s]/gi, '').toString(),
                split = number_string.split('.'),
                sisa = split[0].length % 3,
                rupiah = split[0].substr(0, sisa),
                ribuan = split[0].substr(sisa).match(/\d{3}/gi);

            // tambahkan titik jika yang di input sudah menjadi angka ribuan
            if (ribuan) {
                separator = sisa ? ',' : '';
                rupiah += separator + ribuan.join(',');
            }

            rupiah = split[1] != undefined ? rupiah + '.' + split[1] : rupiah;
            return prefix == undefined ? rupiah : (rupiah ? rupiah : '');
        };

        /* DeFormat Rupiah */
        // function DeFormatRupiah(angka) {
        //     var result = angka.replace(/[^\w\s]/gi, '');

        //     return result;
        // };

        $('#diskon').mask('99.99%', {
            reverse: true
        });

        $('#minorder').keyup(function() {
            $(this).val(FormatRupiah($(this).val()));
        })
        // $('#harga').keyup(function() {
        //     $(this).val(FormatRupiah($(this).val()));
        // })

        /* Validasi Kosong */
        function ValidasiSave(datadetail) {
            // var kode = $('#kode').val();
            var nama = $('#nama').val();
            var diskon = $('#diskon').val();
            // var harga = $('#harga').val();

            // if (kode == '' || kode == 0) {
            //     Swal.fire({
            //         title: 'Informasi',
            //         icon: 'info',
            //         html: 'Kode tidak boleh kosong.',
            //         showCloseButton: true,
            //         width: 350,
            //     });
            //     $('#kode').focus();
            //     var result = false;
            // } else 
            if (nama == '' || nama == 0) {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Nama tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#nama').focus();
                var result = false;
            } else if (diskon == '' || diskon == 0) {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Diskon tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#diskon').focus();
                return "gagal";
            } else if (datadetail.length == 0 || datadetail.length == '0') {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Data Detail tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#namagudang').focus();
                var result = false;
            } else {
                var result = true;
            }

            return result;
        }

        /* Validasi Data Add Detail */
        function ValidasiAddDetail(kodebarang) {
            var table = document.getElementById('t_detail');
            var kodebarang = $('#kodebarang').val();
            var namabarang = $('#namabarang').val();

            if (kodebarang == '' || kodebarang == 0) {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Kode Barang tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#kodebarang').focus();
                return "gagal";
            } else if (namabarang == '' || namabarang == 0) {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Nama Barang tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#namabarang').focus();
                return "gagal";
            } else {
                for (var r = 1, n = table.rows.length; r < n; r++) {
                    var string = "";
                    for (var c = 0, m = table.rows[r].cells.length; c < m; c++) {
                        if (c == 0) {
                            if (table.rows[r].cells[c].innerHTML == kodebarang) {
                                Swal.fire({
                                    title: 'Informasi',
                                    icon: 'info',
                                    html: 'Data ini sudah diinput.',
                                    showCloseButton: true,
                                    width: 350,
                                });
                                return "gagal";
                            }
                        }
                    }
                }
            }
            return "sukses"
        }

        /* Declare Toast */
        const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000,
            timerProgressBar: true,
            didOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
            }
        });

        document.getElementById("find").addEventListener("click", function(event) {
            event.preventDefault();
            $('#tb_detail').empty();
            $('#t_kelompokaj').DataTable({
                "destroy": true,
                "searching": true,
                "processing": true,
                "serverSide": true,
                "lengthChange": true,
                "pageLength": 5,
                "lengthMenu": [5, 10, 25, 50],
                "order": [],
                "ajax": {
                    "url": "<?= base_url('masterdata/KelompokAJ/CariDataKelompokAJ'); ?>",
                    "method": "POST",
                    "data": {
                        nmtb: "glbm_kelompokaj",
                        field: {
                            kode: "kode",
                            nama: "nama",
                            diskon: "diskon",
                            aktif: "aktif"
                        },
                        sort: "kode",
                        where: {
                            kode: "kode",
                            nama: "nama",
                        },
                        // value: "aktif = true"
                    },
                },
                "columnDefs": [{
                    "targets": 3,
                    "data": "diskon",
                    "render": function(data, type, row, meta) {
                        return row[3] + "%";
                    }
                }, {
                    "targets": 4,
                    "data": "aktif",
                    "render": function(data, type, row, meta) {
                        return (row[4] == 't') ? 'Aktif' : 'Tidak Aktif';
                    }
                }],
            });
        });

        $(document).on('click', ".searchkelompokaj", function() {
            var kode = $(this).attr("data-id");
            GetData(kode);
            $('#save').prop('disabled', true);
        });

        function GetData(kode) {
            $.ajax({
                url: "<?= base_url('masterdata/KelompokAJ/DataKelompokAJ'); ?>",
                method: "POST",
                dataType: "json",
                async: false,
                data: {
                    kode: kode
                },
                success: function(data) {
                    for (var i = 0; i < data.length; i++) {
                        $('#kode').val(data[i].kode.trim());
                        $('#nama').val(data[i].nama.trim());
                        $('#diskon').val(data[i].diskon.trim() + '%');
                        // $('#harga').val(FormatRupiah(data[i].harga.trim()));
                        if (data[i].aktif.trim() == 't') {
                            $('#aktif').prop('checked', true);
                        } else {
                            $('#tidak_aktif').prop('checked', true);
                        }
                    }
                    GetDataDetail(kode);
                    $('#update').prop('disabled', false);
                    $('#save').prop('disabled', true);
                    $('#kode').prop('disabled', true);

                    $('.aktif').show();
                }
            });
        };

        $("#adddetail").click(function() {

            var kodebarang = $('#kodebarang').val();
            var namabarang = $('#namabarang').val();
            if (ValidasiAddDetail(kodebarang) == "sukses") {
                var row = "";
                row =
                    '<tr id="' + kodebarang + '">' +
                    '<td nowrap>' + kodebarang + '</td>' +
                    '<td nowrap>' + namabarang + '</td>' +
                    '<td nowrap style="text-align: center; width: 200px;">' +
                    // '<button data-table="' + kodebarang + '" data-toggle="modal" data-target="#changediskon" class="editdetail btn btn-success m-1 p-1"><i class="fas fa-edit"></i> Edit</button>' +
                    '<button data-table="' + kodebarang + '" class="deldetail btn btn-danger m-1 p-1"><i class="fa fa-trash mr-1"></i> Delete</button>' +
                    '</td>' +
                    '</tr>';
                $('#tb_detail').append(row);
                $('#kodebarang').val("");
                $('#namabarang').val("");
            }
        });

        $(document).on('click', '.editdetail', function() {
            var table = document.getElementById('t_detail');
            var tr = document.getElementById($(this).attr("data-table"));
            var td = tr.getElementsByTagName("td");
            for (var c = 0, m = table.rows[0].cells.length; c < m - 1; c++) {
                var chgth = table.rows[0].cells[c].innerHTML.replace(/ /g, '').replace(/\s/g, '').toLowerCase();
                var valuetd = td[c].innerHTML;
                $('#chg' + chgth).val(valuetd);
            }
        });

        $(document).on('click', '#changedetail', function() {
            // var kode = $('#chgkode').val();
            var kodebarang = $('#chgkode').val();
            // var namabarang = $('#chgnama').val();
            $('#' + kodebarang).remove();
            InsertDataDetail(kodebarang);
        });

        $(document).on('click', '.deldetail', function() {
            var id = $(this).attr("data-table");
            $('#' + id).remove();
        });

        function AmbilDataDetail() {
            var table = document.getElementById('t_detail');
            var arr2 = [];
            for (var r = 1, n = table.rows.length; r < n; r++) {
                var string = "";
                for (var c = 0, m = table.rows[r].cells.length; c < m - 1; c++) {
                    if (c == 0) {
                        string = "{" + table.rows[0].cells[c].innerHTML + " : '" + table.rows[r].cells[c].innerHTML + "'";
                    } else if (c == 1) {
                        string = string + ", " + table.rows[0].cells[c].innerHTML + " : '" + table.rows[r].cells[c].innerHTML.replace("'", " ") + "'";
                    } else {
                        string = string + ", " + table.rows[0].cells[c].innerHTML + " : '" + table.rows[r].cells[c].innerHTML + "'";
                    }
                }
                string = string + "}";
                var obj = JSON.stringify(eval('(' + string + ')'));
                var arr = $.parseJSON(obj);
                arr2.push(arr);
            }
            return arr2;
        };

        /* Save */
        document.getElementById('save').addEventListener("click", function(event) {
            event.preventDefault();
            var kode = $('#kode').val();
            var nama = $('#nama').val();
            var diskon = $('#diskon').val();
            // var harga = $('#harga').val();
            var datadetail = AmbilDataDetail();

            if (ValidasiSave(datadetail) == true) {
                $.ajax({
                    url: "<?= base_url("masterdata/KelompokAJ/Save") ?>",
                    method: "POST",
                    dataType: "json",
                    async: true,
                    data: {
                        kode: kode,
                        nama: nama,
                        diskon: diskon,
                        // harga: harga,
                        datadetail: datadetail
                    },
                    success: function(data) {
                        if (data.kode != "") {
                            Toast.fire({
                                icon: 'success',
                                title: data.message
                            });
                            $('#kode').val(data.kode);
                            $('#nama').prop('disabled', true);
                            $('#diskon').prop('disabled', true);
                            // $('#harga').prop('disabled', true);

                            $('#update').prop("disabled", true);
                            $('#save').prop("disabled", true);

                            $('.deldetail').hide();
                            $('.editdetail').hide();

                            $('#adddetail').hide();

                            $('#caribarang').hide();
                        } else {
                            Toast.fire({
                                icon: 'error',
                                title: data.message
                            });
                        }
                    }
                }, false)
            }
        })

        document.getElementById("caribarang").addEventListener("click", function(event) {
            event.preventDefault();
            $('#t_barang').DataTable({
                "destroy": true,
                "searching": true,
                "processing": true,
                "serverSide": true,
                "lengthChange": true,
                "pageLength": 5,
                "lengthMenu": [5, 10, 25, 50],
                "order": [],
                "ajax": {
                    "url": "<?= base_url('masterdata/Barang/CariDataBarang'); ?>",
                    "method": "POST",
                    "data": {
                        nmtb: "glbm_barang",
                        field: {
                            kode: "kode",
                            nama: "nama",
                            // barcode1: "barcode1",
                            // barcode2: "barcode2",
                            // kodesatuan: "kodesatuan",
                            // kodemerk: "kodemerk",
                            // lokasi: "lokasi",
                            // hargabeli: "hargabeli",
                            // hargajual: "hargajual",
                            // kodegudang: "kodegudang"
                        },
                        sort: "kode",
                        where: {
                            kode: "kode",
                            nama: "nama",
                        },
                        value: "kode Not In (SELECT kodebarang FROM glbm_kelompokajdetail) AND aktif = true"
                    },
                }
            });
        });

        $(document).on('click', ".searchbarang", function() {
            var kode = $(this).attr("data-id");
            GetDataBarang(kode);
        });

        function GetDataBarang(kode) {
            $.ajax({
                url: "<?= base_url('masterdata/Barang/DataBarang'); ?>",
                method: "POST",
                dataType: "json",
                async: false,
                data: {
                    kode: kode
                },
                success: function(data) {
                    for (var i = 0; i < data.length; i++) {
                        $('#kodebarang').val(data[i].kode.trim());
                        $('#namabarang').val(data[i].nama.trim());
                    }
                }
            });
        };

        function GetDataDetail(kode) {
            $.ajax({
                url: "<?= base_url('masterdata/KelompokAJ/DataKelompokAJDetail'); ?>",
                method: "POST",
                dataType: "json",
                async: false,
                data: {
                    kode: kode
                },
                success: function(data) {
                    for (var i = 0; i < data.length; i++) {
                        // var kode = data[i].kode;
                        var kodebarang = data[i].kodebarang;
                        var namabarang = data[i].namabarang.toString();

                        InsertDataDetail(kodebarang, namabarang);
                    }
                }
            });
        };

        function InsertDataDetail(kodebarang, namabarang) {

            var row = "";
            row =
                '<tr id="' + kodebarang + '">' +
                '<td nowrap>' + kodebarang + '</td>' +
                '<td nowrap>' + namabarang + '</td>' +
                '<td nowrap style="text-align: center; width: 200px;">' +
                // '<button data-table="' + kodebarang + '" data-toggle="modal" data-target="#changediskon" class="editdetail btn btn-success m-1 p-1"><i class="fas fa-edit"></i> Edit</button>' +
                '<button data-table="' + kodebarang + '" class="deldetail btn btn-danger m-1 p-1"><i class="fa fa-trash mr-1"></i> Delete</button>' +
                '</td>' +
                '</tr>';
            $('#tb_detail').append(row);
        };

        /* Update */
        document.getElementById('update').addEventListener("click", function(event) {
            event.preventDefault();
            var kode = $('#kode').val();
            var nama = $('#nama').val();
            var diskon = $('#diskon').val();
            // var harga = $('#harga').val();
            var datadetail = AmbilDataDetail();
            var aktif = $('input[name="aktif"]:checked').val();

            if (ValidasiSave(datadetail) == true) {
                $.ajax({
                    url: "<?= base_url("masterdata/KelompokAJ/Update") ?>",
                    method: "POST",
                    dataType: "json",
                    async: true,
                    data: {
                        kode: kode,
                        nama: nama,
                        diskon: diskon,
                        // harga: harga,
                        datadetail: datadetail,
                        aktif: aktif
                    },
                    success: function(data) {
                        if (data.kode != "") {
                            Toast.fire({
                                icon: 'success',
                                title: data.message
                            });
                            $('#kode').val(data.kode);
                            $('#nama').prop('disabled', true);
                            $('#diskon').prop('disabled', true);
                            // $('#harga').prop('disabled', true);
                            $('#aktif').prop('disabled', true);

                            $('#update').prop("disabled", true);
                            $('#save').prop("disabled", true);

                            $('#caribarang').hide();
                        } else {
                            Toast.fire({
                                icon: 'error',
                                title: data.message
                            });
                        }
                    }
                }, false)
            }

        });

        /* Clear */
        document.getElementById("clear").addEventListener("click", function(event) {
            event.preventDefault();
            ClearScreen();
        });

        ClearScreen();

    });
</script>