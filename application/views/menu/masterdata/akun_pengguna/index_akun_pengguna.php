<div class="col-md-12 mb-2">
    <div class="modal-header" style="padding: 5px;">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-3">
                    <h2>
                        <span class="logo-menu"><?php echo $icons ?></span>
                        <span><?php echo $title ?></span>
                    </h2>
                </div>
                <div class="col-md-9">
                    <div class="form-group" style="float: right;">
                        <!-- <button id="save" class="btn btn-sm btn-dark m-1" style="margin-bottom: 0px;"><i class="far fa-save" style="margin-right: 10px;"></i>Save</button> -->
                        <button id="update" class="btn btn-sm btn-dark m-1" style="margin-bottom: 0px;"><i class="far fa-money-check-edit" style="margin-right: 10px;"></i>Update</button>
                        <!-- <button data-toggle="modal" data-target="#finduser" id="find" class="btn btn-sm btn-dark m-1" style="margin-bottom: 0px;"><i class="far fa-file-search" style="margin-right: 10px;"></i>Find</button> -->
                        <button id="clear" class="btn btn-sm btn-dark m-1" style="margin-bottom: 0px;"><i class="far fa-refresh" style="margin-right: 10px;"></i>Clear</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="col-md-12 table-responsive hidescroll" style="height: 555px; overflow-y: scroll;">
    <div class="row">
        <div class="col-md-6">
            <div class="modal-content mb-2">
                <div class="modal-body">
                    <div class="row mb-2 mt-2">
                        <div class="col-sm-6 input-group">
                            <div class="col-sm-6 p-0 input-group-prepend">
                                <span class="col-sm-12 input-group-text">Username</span>
                            </div>
                            <input class="form-control" type="text" name="login" id="login" maxlength="50" placeholder="Username" readonly required autocomplete="off" />
                        </div>
                        <div class="col-sm-6 input-group">
                            <div class="col-sm-6 p-0 input-group-prepend">
                                <span class="col-sm-12 input-group-text">Password</span>
                            </div>
                            <input class="form-control" type="password" name="password" id="password" maxlength="50" placeholder="Password" required autocomplete="off" />
                        </div>
                    </div>
                    <div class="row mb-2" style="margin-left: 23%;">
                        <div class="col-sm-12 input-group">
                            <div class="col-sm-3 p-0 input-group-prepand">
                                <span class="col-sm-12 input-group-text">
                                    <input type="checkbox" name="showpassword" id="showpassword" class="showpassword" />&nbsp;
                                    Show Password
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>