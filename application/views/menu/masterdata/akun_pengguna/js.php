<script type="text/javascript">
    $(document).ready(function() {
        function LoadUser() {
            $.ajax({
                url: "<?= base_url('masterdata/Akun/LoadUser') ?>",
                method: "POST",
                dataType: "json",
                async: false,
                data: {
                    username: $('#username').val()
                },
                success: function(data) {
                    for (var i = 0; i < data.length; i++) {
                        $('#login').val(data[i].username.trim());
                        $('input[name="password"]').val(atob(data[i].password.trim()));
                    }
                }
            }, false);
        }

        $('#showpassword').click(function() {
            $(this).is(':checked') ? $('input[name="password"]').attr('type', 'text') : $('input[name="password"]').attr('type', 'password');
        })

        /* Declare Toast */
		const Toast = Swal.mixin({
			toast: true,
			position: 'top-end',
			showConfirmButton: false,
			timer: 3000,
			timerProgressBar: true,
			didOpen: (toast) => {
				toast.addEventListener('mouseenter', Swal.stopTimer)
				toast.addEventListener('mouseleave', Swal.resumeTimer)
			}
		});

        function ClearScreen() {
            $('#login').val('');
            $('#password').val('').prop('disabled', false);
            $('input[name="password"]').attr('type', 'password')
            $('#showpassword').prop('checked', false);
            LoadUser();
        }

        document.getElementById("update").addEventListener("click", function(event) {
            event.preventDefault();

            var username = $('#login').val();
            var password = $('#password').val();

            // if (validasiSave() == true) {
            $.ajax({
                url: "<?php echo base_url('masterdata/Akun/Update') ?>",
                method: "POST",
                dataType: "json",
                async: true,
                data: {
                    username: username,
                    password: password
                },
                success: function(data) {
                    if (data.username != "") {
                        Toast.fire({
                            icon: 'success',
                            title: data.message
                        });
                        $('#login').val(data.username).prop('disabled', true);
                        $('#password').prop('disabled', true);
                    } else {
                        Toast.fire({
                            icon: 'error',
                            title: data.message
                        });
                    }
                }
            });
            // }
        });

        document.getElementById("clear").addEventListener("click", function(event) {
            ClearScreen();
        })

        ClearScreen();
    })
</script>