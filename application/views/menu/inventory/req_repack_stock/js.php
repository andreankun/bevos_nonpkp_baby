<script text="text/javascript">
    $(document).ready(function() {
        function LoadGudang() {
            $.ajax({
                url: "<?= base_url('sales/SalesOrder/LoadGudang') ?>",
                method: "POST",
                dataType: "json",
                async: false,
                data: {
                    cabang: $('#cabang').val()
                },
                success: function(data) {
                    for (var i = 0; i < data.length; i++) {
                        $("#lokasigdg").append('<option value = ' + data[i].kode + ' >' + data[i].nama + '</option>');
                    }
                }
            }, false);
        }

        function ClearScreen() {
            $('#lokasigdg').empty();
            $("#lokasigdg").append('<option value = "" >Pilih Gudang</option>');
            LoadGudang();
            $('#nomorreq').val("");
            $('#tanggalreq').val("<?= date('Y-m-d') ?>");
            $('#keterangan').val("").prop('disabled', false);
            $('#keteranganbatal').val("");
            $('#kodebrg').val("").prop('readonly', true);
            $('#namabrg').val("").prop('readonly', true);
            $('#qty').val(0);
            $('#qtysistem').val(0);

            $('#caribarang').show();
            $('#adddetail').show();

            $('#tb_detail').empty();

            $('.editdetail').show();
            $('.deldetail').show();

            $('#cancel').prop("disabled", true);
            $('#update').prop("disabled", true);
            $('#save').prop("disabled", false);
            $('#changedetail').show();
        }

        function setDateFormat(date) {
            dateObj = new Date(date);
            var day = DateObj.getDate();
            var month = dateObj.getMonth() + 1;
            var fullYear = DateObj.getFullYear().toString();
            var setformattedDate = '';
            setformattedDate = fullYear + '-' + getDigitToFormat(month) + '-' + getDigitToFormat(day);
            return setformattedDate;
        }

        function FormatDate(input) {
            var date = new Date(input);
            var yyyy = date.getFullYear();
            var mm = date.getMonth() + 1;
            var dd = date.getDate();
            var hrs = date.getHours();
            var mnt = date.getMinutes();


            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }
            if (hrs < 10) {
                hrs = '0' + hrs;
            }
            if (mnt < 10) {
                mnt = '0' + mnt;
            }
            return yyyy + '-' + mm + '-' + dd;
        };

        function getDigitToFormat(val) {
            if (val < 10) {
                val = '0' + val;
            }
            return val.toString();
        };

        /* Format Rupiah */
        function FormatRupiah(angka, prefix) {
            if (!angka) {
                return '';
            }
            var vangka = angka.toString();
            var number_string = vangka.replace(/[^.\d]/g, '').replace(/[^\w\s]/gi, '').toString(),
                split = number_string.split('.'),
                sisa = split[0].length % 3,
                rupiah = split[0].substr(0, sisa),
                ribuan = split[0].substr(sisa).match(/\d{3}/gi);

            // tambahkan titik jika yang di input sudah menjadi angka ribuan
            if (ribuan) {
                separator = sisa ? ',' : '';
                rupiah += separator + ribuan.join(',');
            }

            rupiah = split[1] != undefined ? rupiah + '.' + split[1] : rupiah;
            return prefix == undefined ? rupiah : (rupiah ? rupiah : '');
        };

        /* DeFormat Rupiah */
        function DeFormatRupiah(angka) {
            var result = angka.replace(/[^\w\s]/gi, '');

            return result;
        };

        $('#qty').keyup(function() {
            $(this).val(FormatRupiah($(this).val()));
        })

        $('#chgqty').keyup(function() {
            $(this).val(FormatRupiah($(this).val()));
            if (Number($('#chgqty').val()) > Number($('#chgqtysistem').val())) {
                $('#changedetail').hide();
            } else if (Number($('#chgqty').val()) == 0 || $('#chgqty').val() == '0') {
                $('#changedetail').hide();
            } else {
                $('#changedetail').show();
            }
        })

        function CurrentDate(input) {
            var date = new Date(input);
            var yyyy = date.getFullYear();
            var mm = date.getMonth() + 1;
            var dd = date.getDate();
            var hrs = date.getHours();
            var mnt = date.getMinutes();


            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }
            if (hrs < 10) {
                hrs = '0' + hrs;
            }
            if (mnt < 10) {
                mnt = '0' + mnt;
            }
            return yyyy + '' + mm;
        };

        function ValidasiSave(datadetail) {
            var tanggal = $('#tanggalreq').val();
            var keterangan = $('#keterangan').val();

            if (tanggal == '' || tanggal == 0) {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'icon',
                    html: 'Tanggal Request Tidak Boleh Kosong',
                    showCloseButton: true,
                    width: 350,
                })
                $('#tanggal').focus();
                var result = false;
            } else if (keterangan == '' || keterangan == 0) {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'icon',
                    html: 'Keterangan Tidak Boleh Kosong',
                    showCloseButton: true,
                    width: 350,
                })
                $('#keterangan').focus();
                var result = false;
            } else if (datadetail.length == 0 || datadetail.length == '0') {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Data Detail tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#namagudang').focus();
                var result = false;
            } else {
                var result = true;
            }
            return result;
        }

        /* Validasi Data Add Detail */
        function ValidasiAddDetail(kodebarang) {
            var table = document.getElementById('t_detail');

            var kodebarang = $('#kodebrg').val();
            var namabarang = $('#namabrg').val();
            var qty = $('#qty').val();
            var qtysistem = $('#qtysistem').val();

            if (kodebarang == '' || kodebarang == 0) {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Kode Barang tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#kodebarang').focus();
                return "gagal";
            } else if (namabarang == '' || namabarang == 0) {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Nama Barang tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#namabarang').focus();
                return "gagal";
            } else if (qty == '' || qty == 0) {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Qty tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#qty').focus();
                return "gagal";
            } else if (Number(qty) > Number(qtysistem)) {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Qty tidak boleh lebih dari Qty Sistem.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#qty').focus();
                return "gagal";
            } else {
                for (var r = 1, n = table.rows.length; r < n; r++) {
                    var string = "";
                    for (var c = 0, m = table.rows[r].cells.length; c < m; c++) {
                        if (c == 0) {
                            if (table.rows[r].cells[c].innerHTML == kodebarang) {
                                Swal.fire({
                                    title: 'Informasi',
                                    icon: 'info',
                                    html: 'Data ini sudah diinput.',
                                    showCloseButton: true,
                                    width: 350,
                                });
                                return "gagal";
                            }
                        }
                    }
                }
            }
            return "sukses"
        }

        const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000,
            timerProgressBar: true,
            didOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer);
                toast.addEventListener('mouseleave', Swal.resumeTimer);
            }
        });

        document.getElementById("caribarang").addEventListener("click", function(event) {
            event.preventDefault();
            var currentDate = CurrentDate(new Date());
            $('#t_barang').DataTable({
                "destroy": true,
                "searching": true,
                "processing": true,
                "serverSide": true,
                "lengthChange": false,
                "pageLength": 5,
                "lengthMenu": [5, 25, 50, 100],
                "order": [],
                "scrollX": true,
                "ajax": {
                    "url": "<?= base_url('inventory/ReqRepackStock/CariDataBarang'); ?>",
                    "method": "POST",
                    "data": {
                        nmtb: "cari_barang",
                        field: {
                            kodebarang: "kodebarang",
                            namabarang: "namabarang",
                            namasatuan: "namasatuan",
                            namamerk: "namamerk",
                            stock: "stock"
                        },
                        sort: "kodebarang",
                        where: {
                            kodebarang: "kodebarang",
                            namabarang: "namabarang",
                            namasatuan: "namasatuan",
                            namamerk: "namamerk"
                        },
                        value: "aktif = true AND kodecabang = '" + $('#cabang').val() + "' AND kodegudang = '" + $('#lokasigdg').val() + "' AND stock >= 0 AND periode = '" + currentDate + "'"
                    }
                },
                "columnDefs": [{
                    "targets": 5,
                    "data": "stock",
                    "render": function(data, type, row, meta) {
                        return FormatRupiah(row[5]);
                    }
                }]
            });
        });

        $(document).on('click', ".searchbarang", function() {
            var kode = $(this).attr("data-id");
            var kodegudang = $('#lokasigdg').val();
            $.ajax({
                url: "<?= base_url('inventory/ReqRepackStock/DataBarang'); ?>",
                method: "POST",
                dataType: "json",
                async: false,
                data: {
                    kode: kode,
                    kodegudang: kodegudang
                },
                success: function(data) {
                    console.log(data);
                    for (var i = 0; i < data.length; i++) {
                        $('#kodebrg').val(data[i].kodebarang.trim());
                        $('#namabrg').val(data[i].namabarang.trim());
                        $('#qtysistem').val(data[i].stock.trim());
                    }
                }
            }, false);
        });

        $("#adddetail").click(function() {
            var kodebarang = $('#kodebrg').val();
            var namabarang = $('#namabrg').val();
            var qty = $('#qty').val();
            var qtysistem = $('#qtysistem').val();

            if (ValidasiAddDetail(kodebarang) == 'sukses') {
                var row = "";
                row =
                    '<tr id="' + kodebarang + '">' +
                    '<td nowrap>' + kodebarang + '</td>' +
                    '<td nowrap>' + namabarang + '</td>' +
                    '<td nowrap style="display: none;">' + FormatRupiah(qtysistem) + '</td>' +
                    '<td nowrap>' + FormatRupiah(qty) + '</td>' +
                    '<td nowrap style="text-align: center; width: 200px;">' +
                    '<button data-table="' + kodebarang + '" data-toggle="modal" data-target="#changeqty" class="editdetail btn btn-sm btn-success mb-0 p-1"><i class="far fa-edit"></i> Edit</button>&nbsp;' +
                    '<button data-table="' + kodebarang + '" class="deldetail btn btn-sm btn-danger mb-0 p-1"><i class="fa fa-trash"></i> Delete</button>' +
                    '</td>' +
                    '</tr>';
                $('#tb_detail').append(row);
                $('#kodebrg').val("");
                $('#namabrg').val("");
                $('#qty').val(0);
                $('#qtysistem').val(0);
            }
        });

        $(document).on('click', '.deldetail', function() {
            var id = $(this).attr("data-table");
            $('#' + id).remove();
        });

        $(document).on('click', '.editdetail', function() {
            var table = document.getElementById('t_detail');
            var tr = document.getElementById($(this).attr("data-table"));
            var td = tr.getElementsByTagName("td");
            for (var c = 0, m = table.rows[0].cells.length; c < m - 1; c++) {
                var chgth = table.rows[0].cells[c].innerHTML.replace(/ /g, '').replace(/\s/g, '').toLowerCase();
                var valuetd = td[c].innerHTML;
                $('#chg' + chgth).val(valuetd);
            }
        });

        function InsertDataDetail(kodebarang, namabarang, qtysistem, qty) {

            var row = "";
            row =
                '<tr id="' + kodebarang + '">' +
                '<td nowrap>' + kodebarang + '</td>' +
                '<td nowrap>' + namabarang + '</td>' +
                '<td nowrap style="display: none;">' + FormatRupiah(qtysistem) + '</td>' +
                '<td nowrap>' + FormatRupiah(qty) + '</td>' +
                '<td nowrap style="text-align: center; width: 200px;">' +
                '<button data-table="' + kodebarang + '" data-toggle="modal" data-target="#changeqty" class="editdetail btn btn-sm btn-success m-1 p-1"><i class="fas fa-edit"></i> Edit</button>' +
                '<button data-table="' + kodebarang + '" class="deldetail btn btn-sm btn-danger m-1 p-1"><i class="fa fa-trash mr-1"></i> Delete</button>' +
                '</td>' +
                '</tr>';
            $('#tb_detail').append(row);
        };

        $(document).on('click', '#changedetail', function() {
            // var kode = $('#chgkode').val(); 
            var kodebarang = $('#chgkode').val();
            var namabarang = $('#chgnama').val();
            var qty = $('#chgqty').val();
            var qtysistem = $('#chgqtysistem').val();
            $('#' + kodebarang).remove();
            InsertDataDetail(kodebarang, namabarang, qtysistem, qty);
        });

        function AmbilDataDetail() {
            var table = document.getElementById('t_detail');
            var arr2 = [];
            for (var r = 1, n = table.rows.length; r < n; r++) {
                var string = "";
                for (var c = 0, m = table.rows[r].cells.length; c < m - 1; c++) {
                    if (c == 0) {
                        string = "{" + table.rows[0].cells[c].innerHTML + " : '" + table.rows[r].cells[c].innerHTML + "'";
                    } else {
                        string = string + ", " + table.rows[0].cells[c].innerHTML + " : '" + table.rows[r].cells[c].innerHTML + "'";
                    }
                }
                string = string + "}";
                var obj = JSON.stringify(eval('(' + string + ')'));
                var arr = $.parseJSON(obj);
                arr2.push(arr);
            }
            return arr2;
        };

        /* Save */
        document.getElementById('save').addEventListener("click", function(event) {
            event.preventDefault();
            var nomor = $('#nomorreq').val();
            var kodegudang = $('#lokasigdg').val();
            var tanggal = $('#tanggalreq').val();
            var keterangan = $('#keterangan').val();

            var datadetail = AmbilDataDetail();

            if (ValidasiSave(datadetail) == true) {
                $.ajax({
                    url: "<?= base_url("inventory/ReqRepackStock/Save") ?>",
                    method: "POST",
                    dataType: "json",
                    async: true,
                    data: {
                        nomor: nomor,
                        kodegudang: kodegudang,
                        tanggal: tanggal,
                        keterangan: keterangan,
                        datadetail: datadetail
                    },
                    success: function(data) {
                        if (data.nomor != "") {
                            Toast.fire({
                                icon: 'success',
                                title: data.message
                            });
                            $('#nomorreq').val(data.nomor);
                            $('#tanggalreq').prop('disabled', true);
                            $('#keterangan').prop('disabled', true);
                            $('#kodebrg').prop('readonly', true);
                            $('#namabrg').prop('readonly', true);
                            $('#qty').prop('disabled', true);

                            $('#update').prop("disabled", true);
                            $('#save').prop("disabled", false);

                            $('#caribarang').hide();
                            $('#adddetail').hide();

                            $('.editdetail').hide();
                            $('.deldetail').hide();
                        } else {
                            Toast.fire({
                                icon: 'error',
                                title: data.message
                            });
                        }
                    }
                }, false)
            }
        })

        document.getElementById("find").addEventListener("click", function(event) {
            event.preventDefault();
            $('#tb_detail').empty();
            $('#t_rrs').DataTable({
                "destroy": true,
                "searching": true,
                "processing": true,
                "serverSide": true,
                "lengthChange": true,
                "pageLength": 5,
                "lengthMenu": [5, 10, 25, 50],
                "order": [],
                "ajax": {
                    "url": "<?= base_url('inventory/ReqRepackStock/CariDataReq'); ?>",
                    "method": "POST",
                    "data": {
                        nmtb: "srvt_reqrepackstock",
                        field: {
                            nomor: "nomor",
                            tanggal: "tanggal"
                        },
                        sort: "nomor",
                        where: {
                            nomor: "nomor"
                        },
                        value: "batal = false AND kodecabang = '" + $("#cabang").val() + "'"
                        // value: "batal = false AND terima = false AND status = 0 AND kodecabang = '" + $("#cabang").val() + "'"
                    },
                },
                "columnDefs": [{
                    "targets": 2,
                    "render": $.fn.dataTable.render.moment('YYYY-MM-DD HH:mm:ss', 'D MMM YY')
                }]
            });
        })

        $(document).on('click', ".searchreq", function() {
            var nomor = $(this).attr("data-id");
            GetData(nomor);
        });

        function GetData(nomor) {
            $.ajax({
                url: "<?= base_url('inventory/ReqRepackStock/DataReq'); ?>",
                method: "POST",
                dataType: "json",
                async: false,
                data: {
                    nomor: nomor
                },
                success: function(data) {
                    console.log(data);
                    for (var i = 0; i < data.length; i++) {
                        $('#nomorreq').val(data[i].nomor.trim());
                        $('#lokasigdg').val(data[i].kodegudang.trim());
                        $('#tanggalreq').val(FormatDate(data[i].tanggal.trim()));
                        $('#keterangan').val(data[i].keterangan.trim());

                        if (data[i].status == 1 || data[i].terima == 't' || data[i].status == 2) {
                            $('#cancel').prop('disabled', true);
                            $('#update').prop('disabled', true);
                            $('#save').prop('disabled', true);
                            $('#keterangan').prop('disabled', true);
                        } else {
                            $('#cancel').prop('disabled', false);
                            $('#update').prop('disabled', false);
                            $('#save').prop('disabled', true);
                        }
                    }
                    GetDataDetail(nomor);
                }
            });
        };

        function GetDataDetail(nomor) {
            $.ajax({
                url: "<?= base_url('inventory/ReqRepackStock/DataReqDetail'); ?>",
                method: "POST",
                dataType: "json",
                async: false,
                data: {
                    nomor: nomor
                },
                success: function(data) {
                    console.log(data);
                    for (var i = 0; i < data.length; i++) {
                        // var kode = data[i].kode;
                        var kodebarang = data[i].kodebarang.trim();
                        var namabarang = data[i].namabarang.trim();
                        var qty = data[i].qty.trim();
                        var qtysistem = data[i].qtysistem.trim();

                        InsertDataDetail(kodebarang, namabarang, qtysistem, qty);
                    }
                }
            });
        };

        /* Update */
        document.getElementById('update').addEventListener("click", function(event) {
            event.preventDefault();
            var nomor = $('#nomorreq').val();
            var kodegudang = $('#lokasigdg').val();
            var tanggal = $('#tanggalreq').val();
            var keterangan = $('#keterangan').val();

            var datadetail = AmbilDataDetail();

            if (ValidasiSave(datadetail) == true) {
                $.ajax({
                    url: "<?= base_url("inventory/ReqRepackStock/Update") ?>",
                    method: "POST",
                    dataType: "json",
                    async: true,
                    data: {
                        nomor: nomor,
                        kodegudang: kodegudang,
                        tanggal: tanggal,
                        keterangan: keterangan,
                        datadetail: datadetail
                    },
                    success: function(data) {
                        if (data.nomor != "") {
                            Toast.fire({
                                icon: 'success',
                                title: data.message
                            });
                            $('#nomorreq').val(data.nomor);
                            $('#tanggalreq').prop('disabled', true);
                            $('#keterangan').prop('disabled', true);
                            $('#jenis').prop('disabled', true);
                            $('#kodebrg').prop('readonly', true);
                            $('#namabrg').prop('readonly', true);
                            $('#qty').prop('disabled', true);

                            $('#update').prop("disabled", true);
                            $('#save').prop("disabled", false);

                            $('#caribarang').hide();
                            $('#adddetail').hide();

                            $('.editdetail').hide();
                            $('.deldetail').hide();
                        } else {
                            Toast.fire({
                                icon: 'error',
                                title: data.message
                            });
                        }
                    }
                }, false)
            }
        })

        document.getElementById('okcancel').addEventListener("click", function(event) {
            event.preventDefault();
            var nomor = $('#nomorreq').val();
            var keterangan = $('#keteranganbatal').val();
            var datadetail = AmbilDataDetail();
            if (nomor == "") {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Pilih Dulu Nomor Yang Ingin dibatalkan',
                    showCloseButton: true,
                    width: 350,
                });
            } else if (keterangan == "") {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Keterangan Batal Tidak Boleh Kosong',
                    showCloseButton: true,
                    width: 350,
                });
            } else {
                $.ajax({
                    url: "<?= base_url("inventory/ReqRepackStock/Cancel") ?>",
                    method: "POST",
                    dataType: "json",
                    async: true,
                    data: {
                        nomor: nomor,
                        keterangan: keterangan,
                        datadetail: datadetail
                    },
                    success: function(data) {
                        console.log(data);
                        if (data.nomor != "") {
                            Toast.fire({
                                icon: 'success',
                                title: data.message
                            });
                            // RefreshLayar();
                        } else {
                            Toast.fire({
                                icon: 'error',
                                title: data.message
                            });
                        }
                    }
                }, false)
            }
        })

        document.getElementById("clear").addEventListener("click", function(event) {
            event.preventDefault();
            ClearScreen();
        })

        ClearScreen();
    })
</script>