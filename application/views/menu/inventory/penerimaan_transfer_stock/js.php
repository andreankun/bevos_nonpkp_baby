<script type="text/javascript">
    $(document).ready(function() {
        $('#tanggalterimatransfer').datepicker({
            format: "yyyy-mm-dd",
            autoclose: true,
            todayHighlight: true
        });

        function ClearScreen() {
            $('#save').prop("disabled", false);
            $('#update').prop("disabled", true);
            $('#cancel').prop("disabled", true);

            $('#noterimatransfer').val("");
            $('#tanggalterimatransfer').val("<?= date('Y-m-d') ?>");
            $('#notransfer').val("");
            $('#kodegdasal').val("");
            $('#kodegdtujuan').val("");
            $('#kodecbasal').val("");
            $('#kodecbtujuan').val("");
            $('#tanggaltransfer').val("<?= date('Y-m-d') ?>");
            $('#kodebarang').val("");
            $('#namabarang').val("");
            $('#qty').val("").prop("disabled", false);
            $('#keteranganbatal').val("");

            $('#tb_detail').empty();

            $('#caritransferstock').show();
            $('#caribarang').show();
            $('#adddetail').show();
        }

        /* Declare Toast */
        const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000,
            timerProgressBar: true,
            didOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
            }
        });

        /* Format Rupiah */
        function FormatRupiah(angka, prefix) {
            if (!angka) {
                return '';
            }
            var vangka = angka.toString();
            var number_string = vangka.replace(/[^.\d]/g, '').replace(/[^\w\s]/gi, '').toString(),
                split = number_string.split('.'),
                sisa = split[0].length % 3,
                rupiah = split[0].substr(0, sisa),
                ribuan = split[0].substr(sisa).match(/\d{3}/gi);

            // tambahkan titik jika yang di input sudah menjadi angka ribuan
            if (ribuan) {
                separator = sisa ? ',' : '';
                rupiah += separator + ribuan.join(',');
            }

            rupiah = split[1] != undefined ? rupiah + '.' + split[1] : rupiah;
            return prefix == undefined ? rupiah : (rupiah ? rupiah : '');
        };

        function FormatDate(input) {
            var date = new Date(input);
            var yyyy = date.getFullYear();
            var mm = date.getMonth() + 1;
            var dd = date.getDate();
            var hrs = date.getHours();
            var mnt = date.getMinutes();


            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }
            if (hrs < 10) {
                hrs = '0' + hrs;
            }
            if (mnt < 10) {
                mnt = '0' + mnt;
            }
            return yyyy + '-' + mm + '-' + dd;
        };

        function getDigitToFormat(val) {
            if (val < 10) {
                val = '0' + val;
            }
            return val.toString();
        };

        /* Format Rupiah Saat Event Keyup */
        $('#qty').keyup(function() {
            $(this).val(FormatRupiah($(this).val()));
        })
        $('#chgqty').keyup(function() {
            $(this).val(FormatRupiah($(this).val()));
        })

        // document.getElementById("caribarang").addEventListener("click", function(event) {
        //     event.preventDefault();
        //     var notransfer = $('#notransfer').val();
        //     $('#tb_detail').empty();
        //     $('#t_barang').DataTable({
        //         "destroy": true,
        //         "searching": true,
        //         "processing": true,
        //         "serverSide": true,
        //         "lengthChange": true,
        //         "pageLength": 5,
        //         "lengthMenu": [5, 10, 25, 50],
        //         "order": [],
        //         "ajax": {
        //             "url": "<?= base_url('inventory/PenerimaanTransferStok/CariDataBarang'); ?>",
        //             "method": "POST",
        //             "data": {
        //                 nmtb: "srvt_transferstockdetail",
        //                 field: {
        //                     kodebarang: "kodebarang",
        //                     namabarang: "namabarang",
        //                     qty: "qty"
        //                 },
        //                 sort: "kodebarang",
        //                 where: {
        //                     kodebarang: "kodebarang"
        //                 },
        //                 value: "nomor = '" + notransfer + "'"
        //             },
        //         }
        //     });
        // });

        // $(document).on('click', ".searchbarang", function() {
        //     var nomor = $('#notransfer').val();
        //     var kode = $(this).attr("data-id");
        //     $.ajax({
        //         url: "<?= base_url('inventory/PenerimaanTransferStok/DataBarang'); ?>",
        //         method: "POST",
        //         dataType: "json",
        //         async: false,
        //         data: {
        //             nomor: nomor,
        //             kode: kode
        //         },
        //         success: function(data) {
        //             console.log(data)
        //             for (var i = 0; i < data.length; i++) {
        //                 $('#kodebarang').val(data[i].kodebarang.trim());
        //                 $('#namabarang').val(data[i].namabarang.trim());
        //                 $('#qty').val(data[i].qty.trim());
        //             }
        //         }
        //     });
        // });

        $("#adddetail").click(function() {
            var kodebarang = $('#kodebarang').val();
            var namabarang = $('#namabarang').val();
            var qty = $('#qty').val();
            if (ValidasiAddDetail(kodebarang) == "sukses") {
                var row = "";
                row =
                    '<tr id="' + kodebarang + '">' +
                    '<td nowrap>' + kodebarang + '</td>' +
                    '<td nowrap>' + namabarang + '</td>' +
                    '<td nowrap>' + qty + '</td>' +
                    '<td nowrap style="text-align: center; width: 200px;">' +
                    // '<button data-table="' + kodebarang + '" data-toggle="modal" data-target="#changeqty" class="editdetail btn btn-sm btn-success m-1 p-1"><i class="fas fa-edit"></i> Edit</button>' +
                    // '<button data-table="' + kodebarang + '" class="deldetail btn btn-sm btn-danger m-1 p-1"><i class="fa fa-trash mr-1"></i> Delete</button>' +
                    '</td>' +
                    '</tr>';
                $('#tb_detail').append(row);
                $('#kodebarang').val("");
                $('#namabarang').val("");
                $('#qty').val("");
            }
        });

        $(document).on('click', '.editdetail', function() {
            var table = document.getElementById('t_detail');
            var tr = document.getElementById($(this).attr("data-table"));
            var td = tr.getElementsByTagName("td");
            for (var c = 0, m = table.rows[0].cells.length; c < m - 1; c++) {
                var chgth = table.rows[0].cells[c].innerHTML.replace(/ /g, '').replace(/\s/g, '').toLowerCase();
                var valuetd = td[c].innerHTML;
                $('#chg' + chgth).val(valuetd);
            }
        });

        $(document).on('click', '#changeqty', function() {
            var kodebarang = $('#chgkode').val();
            var namabarang = $('#chgnama').val();
            var qty = $('#chgqty').val();
            $('#' + kodebarang).remove();
            InsertDataDetail(kodebarang, namabarang, qty);
        });

        $(document).on('click', '.deldetail', function() {
            var id = $(this).attr("data-table");
            $('#' + id).remove();
        });

        function AmbilDataDetail() {
            var table = document.getElementById('t_detail');
            var arr2 = [];
            for (var r = 1, n = table.rows.length; r < n; r++) {
                var string = "";
                for (var c = 0, m = table.rows[r].cells.length; c < m - 1; c++) {
                    if (c == 0) {
                        string = "{" + table.rows[0].cells[c].innerHTML + " : '" + table.rows[r].cells[c].innerHTML + "'";
                    } else {
                        string = string + ", " + table.rows[0].cells[c].innerHTML + " : '" + table.rows[r].cells[c].innerHTML + "'";
                    }
                }
                string = string + "}";
                var obj = JSON.stringify(eval('(' + string + ')'));
                var arr = $.parseJSON(obj);
                arr2.push(arr);
            }
            return arr2;
        };

        function InsertDataDetail(kodebarang, namabarang, qty) {
            var row = "";
            row =
                '<tr id="' + kodebarang + '">' +
                '<td nowrap>' + kodebarang + '</td>' +
                '<td nowrap>' + namabarang + '</td>' +
                '<td nowrap>' + FormatRupiah(qty) + '</td>' +
                '<td nowrap style="text-align: center; width: 200px;">' +
                // '<button data-table="' + kodebarang + '" data-toggle="modal" data-target="#changeqty" class="editdetail btn btn-sm btn-success m-1 p-1"><i class="fas fa-edit"></i> Edit</button>' +
                // '<button data-table="' + kodebarang + '" class="deldetail btn btn-sm btn-danger m-1 p-1"><i class="fa fa-trash mr-1"></i> Delete</button>' +
                '</td>' +
                '</tr>';
            $('#tb_detail').append(row);
        };

        /*Cari Data Transfer Stok*/
        document.getElementById("caritransferstock").addEventListener("click", function(event) {
            event.preventDefault();
            $('#tb_detail').empty();
            $('#t_ts').DataTable({
                "destroy": true,
                "searching": true,
                "processing": true,
                "serverSide": true,
                "lengthChange": true,
                "pageLength": 5,
                "lengthMenu": [5, 10, 25, 50],
                "order": [],
                "scrollX": true,
                "ajax": {
                    "url": "<?= base_url('inventory/PenerimaanTransferStok/CariDataSuratJalanGudang'); ?>",
                    "method": "POST",
                    "data": {
                        nmtb: "cari_suratjalangudang",
                        field: {
                            nomor: "nomor",
                            tanggal: "tanggal",
                            nomorts: "nomorts",
                            namagdasal: "namagdasal",
                            namagdtujuan: "namagdtujuan",
                            keterangan: "keterangan",
                            terima: "terima"
                        },
                        sort: "nomor",
                        where: {
                            nomor: "nomor",
                            nomorts: "nomorts",
                            namagdasal: "namagdasal",
                            namagdtujuan: "namagdtujuan",
                            keterangan: "keterangan"
                        },
                        // value: "status = false AND batal = false"
                        value: "terima = false AND batal = false AND kodecbtujuan = '" + $('#cabang').val() + "'"
                    },
                },
                "columnDefs": [{
                        "targets": 2,
                        "render": $.fn.dataTable.render.moment('YYYY-MM-DD HH:mm:ss', 'D MMM YY'),
                    },
                    {
                        "targets": 7,
                        "data": "terima",
                        "render": function(data, type, row, meta) {
                            return (row[7] == 't') ? '<p>Sudah Diterima</p>' : '<p>Belum Diterima</p>';
                        }
                    }
                ]
            });
        })

        $(document).on('click', ".searchsj", function() {
            var nomor = $(this).attr("data-id");
            GetData(nomor);
        });

        function GetData(nomor) {
            $.ajax({
                url: "<?= base_url('inventory/PenerimaanTransferStok/DataSuratJalanGudang'); ?>",
                method: "POST",
                dataType: "json",
                async: false,
                data: {
                    nomor: nomor
                },
                success: function(data) {
                    console.log(data);
                    for (var i = 0; i < data.length; i++) {
                        $('#notransfer').val(data[i].nomor.trim());
                        $('#tanggaltransfer').val(FormatDate(data[i].tanggal.trim()));
                        $('#kodegdasal').val(data[i].kodegdasal.trim());
                        $('#kodegdtujuan').val(data[i].kodegdtujuan.trim());
                        $('#kodecbasal').val(data[i].kodecbasal.trim());
                        $('#kodecbtujuan').val(data[i].kodecbtujuan.trim());
                    }
                    GetDataDetail(nomor);
                }
            });
        };

        function myFunction(nomor, kodebarang, namabarang, qty, action) {
            var table = document.getElementById('t_detail').getElementsByTagName('tbody')[0];
            var row = table.insertRow(0);
            var kodepencarian = kodebarang.replace(/[^a-zA-Z0-9 ]/g, '');
            row.setAttribute("id", kodepencarian);
            // console.log(row)
            var cell1 = row.insertCell(0);
            var cell2 = row.insertCell(1);
            var cell3 = row.insertCell(2);
            var cell4 = row.insertCell(3);
            var cell5 = row.insertCell(4);
            // var cell3 = '<button data-table="' + RejectSpecialChar(kodebarang) + '" class="deldetail btn btn-danger m-1 p-1"><i class="fa fa-trash mr-1"></i> Delete</button>';
            cell1.innerHTML = nomor;
            cell2.innerHTML = kodebarang;
            cell3.innerHTML = namabarang;
            cell4.innerHTML = FormatRupiah(qty);
            cell5.innerHTML = action;
            // cell1.style.display = 'none';
            // row.innerHTML = kodebarang;

            var tbody = $('#t_detail').find('tbody');
            var newRows = tbody.find('tr').sort(function(a, b) {
                return $('td:eq(2)', a).text().localeCompare($('td:eq(2)', b).text());
            });
            console.log(newRows);
            tbody.append(newRows);
        }

        function GetDataDetail(nomor) {
            $.ajax({
                url: "<?= base_url('inventory/PenerimaanTransferStok/DataSuratJalanGudangDetail'); ?>",
                method: "POST",
                dataType: "json",
                async: false,
                data: {
                    nomor: nomor
                },
                success: function(data) {
                    for (var i = 0; i < data.length; i++) {
                        var kodebarang = data[i].kodebarang;
                        var namabarang = data[i].namabarang.trim();
                        var qty = data[i].qty.trim();

                        var nomor = "";
                        var action = "-";
                        myFunction(nomor, kodebarang, namabarang, qty, action);
                    }
                }
            });
        };

        function ValidasiSave(datadetail) {
            var notransfer = $('#notransfer').val();
            var tanggaltransfer = $('#tanggaltransfer').val();
            var kodegdasal = $('#kodegdasal').val();
            var kodegdtujuan = $('#kodegdtujuan').val();
            var kodecbasal = $('#kodecbasal').val();
            var kodecbtujuan = $('#kodecbtujuan').val();

            if (notransfer == '' || notransfer == 0) {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Kode Gudang tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#notransfer').focus();
                var result = false;
            } else if (tanggaltransfer == '' || tanggaltransfer == 0) {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Tanggal Transfer tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#tanggaltransfer').focus();
                var result = false;
            } else if (kodecbasal == '' || kodecbasal == 0) {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Cabang Asal tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#kodecbasal').focus();
                var result = false;
            } else if (kodecbtujuan == '' || kodecbtujuan == 0) {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Cabang Tujuan tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#kodecbtujuan').focus();
                var result = false;
            } else if (kodegdasal == '' || kodegdasal == 0) {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Gudang Asal tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#kodegdasal').focus();
                var result = false;
            } else if (kodegdtujuan == '' || kodegdtujuan == 0) {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Gudang Tujuan tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#kodegdtujuan').focus();
                var result = false;
            } else if (datadetail.length == 0) {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Data Detail tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#namabarang').focus();
                var result = false;
            } else {
                var result = true;
            }
            return result;
        }

        /* Validasi Data Add Detail */
        function ValidasiAddDetail(kodebarang) {
            var table = document.getElementById('t_detail');
            var kodebarang = $('#kodebarang').val();
            var namabarang = $('#namabarang').val();
            var qty = $('#qty').val();

            if (kodebarang == '' || kodebarang == 0) {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Kode Barang tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#kodebarang').focus();
                return "gagal";
            } else if (namabarang == '' || namabarang == 0) {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Nama Barang tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#namabarang').focus();
                return "gagal";
            } else if (qty == '' || qty == 0) {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Qty tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#qty').focus();
                return "gagal";
            } else {
                for (var r = 1, n = table.rows.length; r < n; r++) {
                    var string = "";
                    for (var c = 0, m = table.rows[r].cells.length; c < m; c++) {
                        if (c == 0) {
                            if (table.rows[r].cells[c].innerHTML == kodebarang) {
                                Swal.fire({
                                    title: 'Informasi',
                                    icon: 'info',
                                    html: 'Data ini sudah diinput.',
                                    showCloseButton: true,
                                    width: 350,
                                });
                                return "gagal";
                            }
                        }
                    }
                }
            }
            return "sukses"
        }

        /* Save */
        document.getElementById("save").addEventListener("click", function(event) {
            event.preventDefault();
            var nomor = $('#noterimatransfer').val();
            var tanggal = $('#tanggalterimatransfer').val();
            var nomortransfer = $('#notransfer').val();
            var tanggaltransfer = $('#tanggaltransfer').val();
            var kodegdasal = $('#kodegdasal').val();
            var kodegdtujuan = $('#kodegdtujuan').val();
            var kodecbasal = $('#kodecbasal').val();
            var kodecbtujuan = $('#kodecbtujuan').val();

            var datadetail = AmbilDataDetail();

            if (ValidasiSave(datadetail) == true) {
                $.ajax({
                    url: "<?= base_url("inventory/PenerimaanTransferStok/Save") ?>",
                    method: "POST",
                    dataType: "json",
                    async: true,
                    data: {
                        nomor: nomor,
                        tanggal: tanggal,
                        nomortransfer: nomortransfer,
                        tanggaltransfer: tanggaltransfer,
                        kodecbasal: kodecbasal,
                        kodecbtujuan: kodecbtujuan,
                        kodegdasal: kodegdasal,
                        kodegdtujuan: kodegdtujuan,
                        datadetail: datadetail
                    },
                    success: function(data) {
                        if (data.nomor != "") {
                            Toast.fire({
                                icon: 'success',
                                title: data.message
                            });
                            $('#noterimatransfer').val(data.nomor);
                            $('#notransfer').prop("disabled", true);
                            $('#tanggaltransfer').prop("disabled", true);
                            $('#qty').prop("disabled", true);

                            $('#caritransferstock').hide();
                            $('#adddetail').hide();
                        } else {
                            Toast.fire({
                                icon: 'error',
                                title: data.message
                            });
                        }
                    }
                }, false)
            }
        })

        /*Cari Data Penerimaan Transfer Stok*/
        document.getElementById("find").addEventListener("click", function(event) {
            event.preventDefault();
            $('#tb_detail').empty();
            $('#t_pts').DataTable({
                "destroy": true,
                "searching": true,
                "processing": true,
                "serverSide": true,
                "lengthChange": true,
                "pageLength": 5,
                "lengthMenu": [5, 10, 25, 50],
                "order": [],
                "ajax": {
                    "url": "<?= base_url('inventory/PenerimaanTransferStok/CariDataPenerimaanTransferStok'); ?>",
                    "method": "POST",
                    "data": {
                        nmtb: "cari_penerimaantransferstok",
                        field: {
                            nomor: "nomor",
                            tanggal: "tanggal",
                            noproses: "noproses",
                            notransfer: "notransfer",
                            tanggaltransfer: "tanggaltransfer"
                        },
                        sort: "nomor",
                        where: {
                            nomor: "nomor",
                            noproses: "noproses",
                            notransfer: "notransfer",
                        },
                        value: "kodecbtujuan = '" + $('#cabang').val() + "'"
                    },
                },
                "columnDefs": [{
                        "targets": 2,
                        "render": $.fn.dataTable.render.moment('YYYY-MM-DD HH:mm:ss', 'D MMM YY'),
                    },
                    {
                        "targets": 5,
                        "render": $.fn.dataTable.render.moment('YYYY-MM-DD HH:mm:ss', 'D MMM YY'),
                    }
                ]
            });
        })

        $(document).on('click', ".searchpts", function() {
            var nomor = $(this).attr("data-id");
            GetDataPTS(nomor);
            $('#save').prop('disabled', true);
            // $('#update').prop('disabled', false);
            $('#cancel').prop('disabled', false);
        });

        function GetDataPTS(nomor) {
            $.ajax({
                url: "<?= base_url('inventory/PenerimaanTransferStok/DataPenerimaanTransferStok'); ?>",
                method: "POST",
                dataType: "json",
                async: false,
                data: {
                    nomor: nomor
                },
                success: function(data) {
                    for (var i = 0; i < data.length; i++) {
                        $('#noterimatransfer').val(data[i].nomor.trim());
                        $('#tanggalterimatransfer').val(FormatDate(data[i].tanggal.trim()));
                        $('#notransfer').val(data[i].notransfer.trim());
                        $('#tanggaltransfer').val(FormatDate(data[i].tanggaltransfer.trim()));
                        $('#kodegdasal').val(data[i].kodegdasal.trim());
                        $('#kodegdtujuan').val(data[i].kodegdtujuan.trim());
                        $('#kodecbasal').val(data[i].kodecbasal.trim());
                        $('#kodecbtujuan').val(data[i].kodecbtujuan.trim());
                    }
                    GetDataPTSDetail(nomor);
                }
            });
        };

        function GetDataPTSDetail(nomor) {
            $.ajax({
                url: "<?= base_url('inventory/PenerimaanTransferStok/DataPenerimaanTransferStokDetail'); ?>",
                method: "POST",
                dataType: "json",
                async: false,
                data: {
                    nomor: nomor
                },
                success: function(data) {
                    for (var i = 0; i < data.length; i++) {
                        var kodebarang = data[i].kodebarang;
                        var namabarang = data[i].namabarang.trim();
                        var qty = data[i].qty.trim();

                        var nomor = "";
                        var action = "-";
                        myFunction(nomor, kodebarang, namabarang, qty, action);
                    }
                    $('.editdetail').hide();
                    $('.deldetail').hide();
                }
            });
        };

        document.getElementById('okcancel').addEventListener("click", function(event) {
            event.preventDefault();
            var nomor = $('#noterimatransfer').val();
            var notransfer = $('#notransfer').val();
            var keterangan = $('#keteranganbatal').val();
            var kodegudang = $('#kodegdtujuan').val();
            var kodecabang = $('#kodecbtujuan').val();
            var kodecbasal = $('#kodecbasal').val();
            var kodegdasal = $('#kodegdasal').val();
            var datadetail = AmbilDataDetail();
            if (nomor == "") {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Pilih Dulu Nomor Yang Ingin dibatalkan',
                    showCloseButton: true,
                    width: 350,
                });
            } else if (keterangan == "") {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Keterangan Batal Tidak Boleh Kosong',
                    showCloseButton: true,
                    width: 350,
                });
            } else {
                $.ajax({
                    url: "<?= base_url("inventory/PenerimaanTransferStok/Cancel") ?>",
                    method: "POST",
                    dataType: "json",
                    async: true,
                    data: {
                        nomor: nomor,
                        notransfer: notransfer,
                        keterangan: keterangan,
                        kodecabang: kodecabang,
                        kodegudang: kodegudang,
                        kodecbasal: kodecbasal,
                        kodegdasal: kodegdasal,
                        datadetail: datadetail
                    },
                    success: function(data) {
                        console.log(data);
                        if (data.nomor != "") {
                            Toast.fire({
                                icon: 'success',
                                title: data.message
                            });
                            // RefreshLayar();
                        } else {
                            Toast.fire({
                                icon: 'error',
                                title: data.message
                            });
                        }
                    }
                }, false)
            }
        })

        document.getElementById("clear").addEventListener("click", function(event) {
            event.preventDefault();
            ClearScreen();
        })

        ClearScreen();
    });
</script>