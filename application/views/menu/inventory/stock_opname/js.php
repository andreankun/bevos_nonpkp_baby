<script>
	$(document).ready(function() {
		function ClearScreen() {
			$('#noopname').prop('disabled', false).val("");
			$('#keterangan').prop('disabled', false).val("");
			$('#tanggalopname').prop('disablec', false).val("<?= date('Y-m-d') ?>");
			$('#kodegudang').prop('disabled', false).val("");
			$('#kodebarang').prop('disabled', false).val("");
			$('#kodemerk').prop('disabled', false).val("");
			$('#namabarang').prop('disabled', false).val("");
			$('#qty').prop('disabled', false).val("");
			$('#tanggalexpired').prop('disabled', false).val("<?= date('Y-m-d') ?>");
			$('#ketdetail').prop('disabled', false).val("");

			$('#update').prop('disabled', true);
			$('#save').prop('disabled', false);
			$('#find').prop('disabled', false);
			$('#batal').hide();
			$('#tb_detail').empty()
		}

		$('#tanggalexpired').datepicker({
			format: "yyyy-mm-dd",
			autoclose: true,
			todayHightlight: true,
			startDate: new Date()
		});

		$('#chgexpired').datepicker({
			format: "yyyy-mm-dd",
			autoclose: true,
			todakHightlight: true,
			startDate: new Date()
		});

		function FormatDate(input) {
			var date = new Date(input);
			var yyyy = date.getFullYear();
			var mm = date.getMonth() + 1;
			var dd = date.getDate();
			var hrs = date.getHours();
			var mnt = date.getMinutes();


			if (dd < 10) {
				dd = '0' + dd;
			}
			if (mm < 10) {
				mm = '0' + mm;
			}
			if (hrs < 10) {
				hrs = '0' + hrs;
			}
			if (mnt < 10) {
				mnt = '0' + mnt;
			}
			return yyyy + '-' + mm + '-' + dd;
		};

		/* Format Rupiah */
		function FormatRupiah(angka, prefix) {
			if (!angka) {
				return '';
			}
			var vangka = angka.toString();
			var number_string = vangka.replace(/[^.\d]/g, '').replace(/[^\w\s]/gi, '').toString(),
				split = number_string.split('.'),
				sisa = split[0].length % 3,
				rupiah = split[0].substr(0, sisa),
				ribuan = split[0].substr(sisa).match(/\d{3}/gi);

			// tambahkan titik jika yang di input sudah menjadi angka ribuan
			if (ribuan) {
				separator = sisa ? ',' : '';
				rupiah += separator + ribuan.join(',');
			}

			rupiah = split[1] != undefined ? rupiah + '.' + split[1] : rupiah;
			return prefix == undefined ? rupiah : (rupiah ? rupiah : '');
		};

		/* DeFormat Rupiah */
		function DeFormatRupiah(angka) {
			var result = angka.replace(/[^\w\s]/gi, '');

			return result;
		};

		/* Format Rupiah Saat Event Keyup */
		$('#qty').keyup(function() {
			$(this).val(FormatRupiah($(this).val()));
		})
		$('#qtysistem').keyup(function() {
			$(this).val(FormatRupiah($(this).val()));
		})
		$('#chgqty').keyup(function() {
			$(this).val(FormatRupiah($(this).val()));
		})


		function ValidasiSave(datadetail) {
			var tanggalopname = $('#tanggalopname').val();
			var keterangan = $('#keterangan').val();
			var kodegudang = $('#kodegudang').val();

			if (tanggalopname == '' || tanggalopname == 0) {
				Swal.fire({
					title: 'Informas',
					icon: 'info',
					html: "Tangga Opname tidak boleh kosong",
					showCloseButton: true,
					width: 350,
				});
				$('#tanggalopname').focus();
				var result = false;
			} else if (keterangan == '' || keterangan == 0) {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: "Keterangan Opname tidak boleh kosong",
					showCloseButton: true,
					width: 350
				});
				$('#keterang').focus();
				var result = false;
			} else if (kodegudang == '' || kodegudang == 0) {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: "Kode Gudang tidak boleh kosong",
					showCloseButton: true,
					width: 350
				});
				$('#kodegudang').focus();
				var result = false;
			} else {
				var result = true;
			}
			return result;
		}



		function ValidasiSaveDetail(kodebarang) {
			var table = document.getElementById('t_detail');
			var kodebarang = $('#kodebarang').val();
			var nambarang = $('#namabarang').val();
			var qty = $('#qty').val();
			var ketdetail = $('#ketdetail').val();
			var tanggalexpired = $('#tanggalexpired').val();
			if (kodebarang == '' || kodebarang == 0) {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Kode Barang tidak boleh kosong',
					showCloseButton: true,
					width: 350
				});
				$('#kodebarang').focus();
				var result = false;
			} else if (namabarang == '' || namabarang == '') {
				Swal.fire({
					title: 'Informas',
					icon: 'info',
					html: 'Nama Barang tidak boleh kosong',
					showCloseButton: true,
					width: 350,
				});
				$('#namabarang').focus();
				var result = false;
			} else if (tanggalexpired == '' || tanggalexpired == 0) {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Tanggal Expired tidak boleh kosong',
					showCloseButton: true,
					width: 350
				});
				$('#tanggalexpired').focus();
				var result = false;
			} else if (qtysistem == '' || qtysistem == 0) {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Quantity Sistem tidak boleh kosong',
					showCloseButton: true,
					width: 350
				});
				$('#qtysistem').focus();
				var result = false;
			} else if (ketdetail == '' || ketdetail == 0) {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Keterangan detail Tidak boleh kosong',
					showCloseButton: true,
					width: 350
				});
				$('#ketdetail').focus();
				var result = false;
			} else {
				for (var r = 1, n = table.rows.length; r < n; r++) {
					var string = "";
					for (var c = 0, m = table.rows[r].cells.length; c < m; c++) {
						if (c == 0) {
							if (table.rows[r].cells[c].innerHTML == kodebarang) {
								Swal.fire({
									title: 'Information',
									icon: 'info',
									html: 'Data ini sudah diinput',
									showCloseButton: true,
									width: 350,
								});
								return "gagal";
							}
						}
					}
				}
			}
			return "sukses";
		}

		/* Declare Toast */
		const Toast = Swal.mixin({
			toast: true,
			position: 'top-end',
			showConfirmButton: false,
			timer: 3000,
			timerProgressBar: true,
			didOpen: (toast) => {
				toast.addEventListener('mouseenter', Swal.stopTimer)
				toast.addEventListener('mouseleave', Swal.resumeTimer)
			}
		});

		/* Cari Data Satuan*/
		document.getElementById("carimerk").addEventListener("click", function(event) {
			event.preventDefault();
			$('#t_merk').DataTable({
				"destroy": true,
				"searching": true,
				"processing": true,
				"serverSide": true,
				"lengthChange": true,
				"pageLength": 5,
				"lengthMenu": [5, 10, 25, 50],
				"order": [],
				"ajax": {
					"url": "<?= base_url('masterdata/Merk/CariDataMerk'); ?>",
					"method": "POST",
					"data": {
						nmtb: "glbm_merk",
						field: {
							kode: "kode",
							nama: "nama",
							// qtysatuan: "qtysatuan",
							// konversi: "konversi",
						},
						sort: "kode",
						where: {
							kode: "kode"
						},
						value: "aktif = true"
					},
				}
			});
		});

		/*Get Data Satuan */
		$(document).on('click', ".searchmerk", function() {
			var kode = $(this).attr("data-id");;
			$.ajax({
				url: "<?= base_url('masterdata/Merk/DataMerk'); ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					kode: kode
				},
				success: function(data) {
					for (var i = 0; i < data.length; i++) {
						$('#kodemerk').val(data[i].kode.trim());
						// $('#namamerk').val(data[i].nama.trim());
					}
				}
			}, false);
		});

		function CurrentDate(input) {
			var date = new Date(input);
			var yyyy = date.getFullYear();
			var mm = date.getMonth() + 1;
			var dd = date.getDate();
			var hrs = date.getHours();
			var mnt = date.getMinutes();


			if (dd < 10) {
				dd = '0' + dd;
			}
			if (mm < 10) {
				mm = '0' + mm;
			}
			if (hrs < 10) {
				hrs = '0' + hrs;
			}
			if (mnt < 10) {
				mnt = '0' + mnt;
			}
			return yyyy + '' + mm;
		};

		document.getElementById("caribarang").addEventListener("click", function(event) {
			event.preventDefault();
			var currentDate = CurrentDate(new Date());
			var kodegudang = $('#kodegudang').val();
			var kodemerk = $('#kodemerk').val();
			var value = "";
			if (kodemerk == "") {
				value = "kodegudang = '" + kodegudang + "' AND periode = '" + currentDate + "'";
			} else {
				value = "kodegudang = '" + kodegudang + "' AND kodemerk = '" + kodemerk + "' AND periode = '" + currentDate + "'";
			}
			$('#t_barang').DataTable({
				"destroy": true,
				"searching": true,
				"processing": true,
				"serverSide": true,
				"lengthChange": true,
				"pageLength": 5,
				"lengthMenu": [5, 10, 25, 50],
				"order": [],
				"ajax": {
					"url": "<?= base_url('inventory/InventoryStock/CariDataInventory'); ?>",
					"method": "POST",
					"data": {
						nmtb: "cari_inventorystock",
						field: {
							kode: "kodebarang",
							nama: "namabarang",
							qty: "stock"
						},
						sort: "kodebarang",
						where: {
							kode: "kodebarang",
						},
						value: value
					},
				}
			});
		});

		$(document).on('click', ".searchinventory", function() {
			var kodebarang = $(this).attr("data-id");
			var kodegudang = $('#kodegudang').val();
			$.ajax({
				url: "<?= base_url('inventory/StokOpname/DataInventoryStock') ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					kodebarang: kodebarang,
					kodegudang: kodegudang
				},
				success: function(data) {
					for (var i = 0; i < data.length; i++) {
						$('#kodebarang').val(data[i].kodebarang.trim());
						$('#namabarang').val(data[i].namabarang.trim());
						$('#qtysistem').val(FormatRupiah(data[i].stock.trim()));
					}
				}
			});
		});

		document.getElementById("carigudang").addEventListener("click", function(event) {
			event.preventDefault();
			$('#t_gudang').DataTable({
				"destroy": true,
				"searching": true,
				"processing": true,
				"serverSide": true,
				"lengthChange": true,
				"pageLength": 5,
				"lengthMenu": [5, 10, 25, 50],
				"order": [],
				"ajax": {
					"url": "<?= base_url('masterdata/Gudang/CariDataGudang'); ?>",
					"method": "POST",
					"data": {
						nmtb: "glbm_gudang",
						field: {
							kode: "kode",
							nama: "nama",
							alamat: "alamat"

						},
						sort: "kode",
						where: {
							kode: "kode"
						},
						value: "aktif = true AND kodecabang = '" + $('#cabang').val() + "'"
					},
				}
			});
		});

		$(document).on('click', ".searchgudang", function() {
			var kode = $(this).attr("data-id");
			$.ajax({
				url: "<?= base_url('masterdata/Gudang/DataGudang') ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					kode: kode
				},
				success: function(data) {
					for (var i = 0; i < data.length; i++) {
						$('#kodegudang').val(data[i].kode.trim());
					}
				}
			});
		});

		function AmbilDataDetail() {
			var table = document.getElementById('t_detail');
			var arr2 = [];
			for (var r = 1, n = table.rows.length; r < n; r++) {
				var string = "";
				for (var c = 0, m = table.rows[r].cells.length; c < m - 1; c++) {
					if (c == 0) {
						string = "{" + table.rows[0].cells[c].innerHTML + " : '" + table.rows[r].cells[c].innerHTML + "'";
					} else {
						string = string + ", " + table.rows[0].cells[c].innerHTML + " : '" + table.rows[r].cells[c].innerHTML + "'";
					}
				}
				string = string + "}";
				var obj = JSON.stringify(eval('(' + string + ')'));
				var arr = $.parseJSON(obj);
				arr2.push(arr);
			}
			return arr2;
		};

		$("#adddetail").click(function() {
			var kodebarang = $('#kodebarang').val();
			var namabarang = $('#namabarang').val();
			var qty = $('#qty').val();
			var qtysistem = $('#qtysistem').val();
			var tanggalexpired = $('#tanggalexpired').val();
			var ketdetail = $('#ketdetail').val();
			if (ValidasiSaveDetail(kodebarang) == "sukses") {
				var row = "";
				row =
					'<tr id="' + kodebarang + '">' +
					'<td nowrap>' + kodebarang + '</td>' +
					'<td nowrap>' + namabarang + '</td>' +
					'<td nowrap>' + qty + '</td>' +
					'<td nowrap>' + qtysistem + '</td>' +
					'<td nowrap>' + tanggalexpired + '</td>' +
					'<td nowrap>' + ketdetail + '</td>' +
					'<td nowrap style="text-align: center; width: 200px;">' +
					'<button data-table="' + kodebarang + '" data-toggle="modal" data-target="#changebarang" class="editdetail btn btn-sm btn-success m-1 p-1"><i class="fas fa-edit"></i> Edit</button>' +
					'<button data-table="' + kodebarang + '" class="deldetail btn btn-sm btn-danger m-1 p-1"><i class="fa fa-trash mr-1"></i> Delete</button>' +
					'</td>' +
					'</tr>';
				console.log(row);
				$('#tb_detail').append(row);
				$('#kodebarang').val("");
				$('#namabarang').val("");
				$('#qty').val("");
				$('#qtysistem').val("");
				$('#tanggalexpired').val();
				$('#ketdetail').val("");
			}
		});

		$(document).on('click', '.editdetail', function() {
			var table = document.getElementById('t_detail');
			var tr = document.getElementById($(this).attr("data-table"));
			var td = tr.getElementsByTagName("td");
			for (var c = 0, m = table.rows[0].cells.length; c < m - 1; c++) {
				var chgth = table.rows[0].cells[c].innerHTML.replace(/ /g, '').replace(/\s/g, '').toLowerCase();
				var valuetd = td[c].innerHTML;
				$('#chg' + chgth).val(valuetd);
			}
		});

		$(document).on('click', '#changedetail', function() {
			var kodebarang = $('#chgkode').val();
			var namabarang = $('#chgnama').val();
			var qty = $('#chgqty').val();
			var qtysistem = $('#chgqtysistem').val();
			var tanggalexpired = $('#chgexpired').val();
			var ketdetail = $('#chgketerangan').val();
			$('#' + kodebarang).remove();
			InsertDataDetail(kodebarang, namabarang, qty, qtysistem, tanggalexpired, ketdetail);
		});

		function InsertDataDetail(kodebarang, namabarang, qty, qtysistem, tanggalexpired, ketdetail) {
			var row = "";
			row =
				'<tr id="' + kodebarang + '">' +
				'<td nowrap>' + kodebarang + '</td>' +
				'<td nowrap>' + namabarang + '</td>' +
				'<td nowrap>' + qty + '</td>' +
				'<td nowrap>' + qtysistem + '</td>' +
				'<td nowrap>' + tanggalexpired + '</td>' +
				'<td nowrap>' + ketdetail + '</td>' +
				'<td nowrap style="text-align: center; width: 200px;">' +
				'<button data-table="' + kodebarang + '" data-toggle="modal" data-target="#changebarang" class="editdetail btn btn-success m-1 p-1"><i class="fas fa-edit"></i> Edit</button>' +
				'<button data-table="' + kodebarang + '" class="deldetail btn btn-danger m-1 p-1"><i class="fa fa-trash mr-1"></i> Delete</button>' +
				'</td>' +
				'</tr>';
			$('#tb_detail').append(row);
		};

		$(document).on('click', '.deldetail', function() {
			var id = $(this).attr("data-table");
			$('#' + id).remove();
		});

		function GetDataDetail(nomor) {
			$.ajax({
				url: "<?= base_url('inventory/StokOpname/DataStockOpnameDetail'); ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					nomor: nomor
				},
				success: function(data) {

					for (var i = 0; i < data.length; i++) {
						var kodebarang = data[i].kodebarang;
						var namabarang = data[i].namabarang.trim();
						var qty = FormatRupiah(data[i].qty.trim());
						var qtysistem = FormatRupiah(data[i].qtysistem.trim());
						var tanggalexpired = FormatDate(data[i].expired.trim());
						var ketdetail = data[i].keterangan.trim();

						InsertDataDetail(kodebarang, namabarang, qty, qtysistem, tanggalexpired, ketdetail);
					}
				}
			});
		};

		document.getElementById('save').addEventListener("click", function(event) {
			event.preventDefault();
			var nomor = $('#noopname').val();
			var tanggalopname = $('#tanggalopname').val();
			var norefrensi = $('#norefrensi').val();
			var kodegudang = $('#kodegudang').val()
			var keterangan = $('#keterangan').val();
			var datadetail = AmbilDataDetail();

			if (ValidasiSave(datadetail) == true) {
				if (norefrensi != "") {
					$.ajax({
						url: "<?= base_url('inventory/StokOpname/SaveRecount') ?>",
						method: "POST",
						dataType: "json",
						async: true,
						data: {
							nomor: nomor,
							tanggalopname: tanggalopname,
							kodegudang: kodegudang,
							norefrensi: norefrensi,
							keterangan: keterangan,
							datadetail: datadetail
						},
						success: function(data) {
							if (data.nomor != "") {
								Toast.fire({
									icon: 'success',
									title: data.message
								});
								$('#noopname').val(data.nomor);
								$('#tanggalopname').prop('disabled', true);
								$('#kodegudang').prop('disabled', true);
								$('#keterangan').prop('disabled', true);
								$('#kodebarang').prop('disabled', true);
								$('#namabarang').prop('disabled', true);
								$('#qty').prop('disabled', true);
								$('#qtysistem').prop('disabled', true);
								$('#tanggalexpired').prop('disabled', true);
								$('#ketdetail').prop('disabled', true);

								$('#update').prop('disabled', true);
								$('#save').prop('disabled', true);
							} else {
								Toast.fire({
									icon: 'error',
									title: data.message
								});
							}
						}
					}, false)
				} else {
					$.ajax({
						url: "<?= base_url('inventory/StokOpname/Save') ?>",
						method: "POST",
						dataType: "json",
						async: true,
						data: {
							nomor: nomor,
							tanggalopname: tanggalopname,
							kodegudang: kodegudang,
							keterangan: keterangan,
							datadetail: datadetail
						},
						success: function(data) {
							if (data.nomor != "") {
								Toast.fire({
									icon: 'success',
									title: data.message
								});
								$('#noopname').val(data.nomor);
								$('#tanggalopname').prop('disabled', true);
								$('#kodegudang').prop('disabled', true);
								$('#keterangan').prop('disabled', true);
								$('#kodebarang').prop('disabled', true);
								$('#namabarang').prop('disabled', true);
								$('#qty').prop('disabled', true);
								$('#qtysistem').prop('disabled', true);
								$('#tanggalexpired').prop('disabled', true);
								$('#ketdetail').prop('disabled', true);

								$('#update').prop('disabled', true);
								$('#save').prop('disabled', true);
							} else {
								Toast.fire({
									icon: 'error',
									title: data.message
								});
							}
						}
					}, false)
				}
			}
		});

		document.getElementById("find").addEventListener("click", function(event) {
			event.preventDefault();
			$('#tb_detail').empty();
			$('#t_stockopname').DataTable({
				"destroy": true,
				"searching": true,
				"processing": true,
				"serverSide": true,
				"lengthChange": true,
				"pageLength": 5,
				"lengthMenu": [5, 10, 25, 50],
				"order": [],
				"ajax": {
					"url": "<?= base_url('inventory/StokOpname/CariDataStockOpname'); ?>",
					"method": "POST",
					"data": {
						nmtb: "cari_approvesto",
						field: {
							nomor: "nomor",
							tanggal: "tanggal",
							keterangan: "keterangan",
							status: "status"

						},
						sort: "nomor",
						where: {
							nomor: "nomor"
						},
						value: "status = false AND batal = false AND status_proses = false AND kodecabang = '" + $("#cabang").val() + "'"
					},
				},
				"columnDefs": [{
						"targets": 2,
						"render": $.fn.dataTable.render.moment('YYYY-MM-DD HH:mm:ss', 'D MMM YY')
					},
					{
						"targets": 4,
						"data": "status",
						"render": function(data, type, row, meta) {
							return (row[4] == "t") ? "<p>Sudah di approve</p>" : "<p>Belum di approve</p>"
						}

					}
				],
			});
			$('#tb_detail')
		});

		$(document).on('click', '.searchopname', function() {
			var nomor = $(this).attr("data-id");
			$.ajax({
				url: "<?= base_url('inventory/StokOpname/DataStokOpname') ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					nomor: nomor
				},
				success: function(data) {
					for (var i = 0; i < data.length; i++) {
						$('#noopname').val(data[i].nomor.trim());
						$('#tanggalopname').val(FormatDate(data[i].tanggal.trim()));
						$('#kodegudang').val(data[i].kodegudang.trim());
						$('#keterangan').val(data[i].keterangan);
					}
					GetDataDetail(nomor);
					$('#save').prop('disabled', true);
					$('#update').prop('disabled', false);
					$('#carigudang').hide();
				}
			});
		});

		document.getElementById('recount').addEventListener("click", function(event) {
			event.preventDefault();
			$('#tb_detail').empty();
			$('#t_recount').DataTable({
				"destroy": true,
				"searching": true,
				"processing": true,
				"serverSide": true,
				"lengthChange": true,
				"pageLength": 5,
				"lengthMenu": [5, 10, 25, 50],
				"order": [],
				"ajax": {
					"url": "<?= base_url('inventory/StokOpname/CariDataStockOpnameBRecount'); ?>",
					"method": "POST",
					"data": {
						nmtb: "cari_findsto",
						field: {
							nomor: "nomor",
							kodegudang: "kodegudang",
						},
						sort: "nomor",
						where: {
							nomor: "nomor"
						},
						value: "status_recount = true"
					},
				}
			});
		});

		$(document).on("click", ".searchopnamebrecount", function() {
			var nomor = $(this).attr("data-id");
			$.ajax({
				url: "<?= base_url('inventory/StokOpname/DataStokOpname') ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					nomor: nomor
				},
				success: function(data) {
					for (var i = 0; i < data.length; i++) {
						$('#noopnamehide').val(data[i].nomor.trim());
						$('#tanggalopname').val(FormatDate(data[i].tanggal.trim()));
						$('#norefrensi').val(data[i].norecount.trim());
						$('#keterangan').val(data[i].keterangan.trim());
						$('#kodegudang').val(data[i].kodegudang.trim());
					}
					GetDataDetail(nomor);
					$('#save').prop('disabled', false);
					$('#update').prop('disabled', true);
					$('.adddetail').hide();
					$('#batal').show();
					$('#carigudang').hide();

				}
			});
		});

		document.getElementById('batal').addEventListener("click", function(event) {
			event.preventDefault();
			var nomor = $('#noopname').val();
			var tanggal = $('tanggalopname').val();
			var kodegudang = $('#kodegudang').val();
			var keterangan = $('#keterangan').val();
			var datadetail = AmbilDataDetail();
			$.ajax({
				url: "<?= base_url('inventory/StokOpname/CancelRecount') ?>",
				method: "POST",
				dataType: "json",
				async: true,
				data: {
					nomor: nomor,
					tanggal: tanggal,
					kodegudang: kodegudang,
					keterangan: keterangan,
					datadetail: datadetail
				},
				success: function(data) {
					if (data.nomor != "") {
						Toast.fire({
							icon: 'success',
							title: data.message
						});
						ClearScreen();
						$('#t_detail').empty();
					}
				}
			})
		});
		document.getElementById('update').addEventListener("click", function(event) {
			event.preventDefault();
			var nomor = $('#noopname').val();
			var tanggalopname = $('#tanggalopname').val();
			var keterangan = $('#keterangan').val();
			var kodegudang = $('#kodegudang').val();
			var datadetail = AmbilDataDetail();

			if (ValidasiSave(datadetail) == true) {
				$.ajax({
					url: "<?= base_url('inventory/StokOpname/Update') ?>",
					method: "POST",
					dataType: "json",
					async: true,
					data: {
						nomor: nomor,
						tanggalopname: tanggalopname,
						keterangan: keterangan,
						kodegudang: kodegudang,
						datadetail: datadetail
					},
					success: function(data) {
						if (data.nomor != "") {
							Toast.fire({
								icon: 'success',
								title: data.message
							});
							$('#noopname').prop('disabled', true);
							$('#tanggalopname').prop('disabled', true);
							$('#keterangan').prop('disabled', true);
							$('#kodebarang').prop('disabled', true);
							$('#namabarang').prop('disabled', true);
							$('#qty').prop('disabled', true);
							$('#qtysistem').prop('disabled', true);
							$('#tanggalexpired').prop('disabled', true);
							$('#ketdetail').prop('disabled', true);

							$('#update').prop('disabled', true);
							$('#save').prop('disabled', true);
						} else {
							Toast.fire({
								icon: 'error',
								title: data.message
							});
						}
					}
				}, false)
			}
		});


		document.getElementById("clear").addEventListener("click", function(event) {
			event.preventDefault();
			ClearScreen();
		});


		ClearScreen();
	});
</script>
