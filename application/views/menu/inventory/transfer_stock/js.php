<script type="text/javascript">
    $(document).ready(function() {

        function ClearScreen() {
            $('#cancel').prop("disabled", true);
            $('#update').prop("disabled", true);
            $('#save').prop("disabled", false);

            $('#notransfer').val("");
            $('#tanggaltransfer').val("<?= date('Y-m-d') ?>");
            $('#jenistujuan').prop("disabled", false).val("");
            $('#kodecbasal').val($('#cabang').val());
            $('#kodegdasal').val($('#gudang').val());
            $('#kodecbtujuan').val("");
            $('#kodegdtujuan').val("");
            $('#keterangan').prop("disabled", false).val("");
            $('#keteranganbatal').val("");
            $('#kodemerk').val("");
            $('#nomorsupplier').val("");
            $('#namasupplier').val("");
            $('#kodebarang').val("");
            $('#namabarang').val("");
            $('#qty').prop("disabled", false).val("");
            $('#qtysistem').prop("disabled", false).val("");

            $('#carigudang').show();
            $('#carisupplier').show();
            $('#caribarang').show();
            $('#adddetail').show();
            $('#changedetail').show();

            $('#tb_detail').empty();
        }

        if ($('#grup').val() == 'HO1' || $('#grup').val() == 'HO2' || $('#grup').val() == 'SUPERADMIN' || $('#grup').val() == 'ADM JKT' || $('#grup').val() == 'ADM LK') {
            $('#cariasal').show();
        } else {
            $('#cariasal').hide();
        }

        if ($('#grup').val() == 'ADM JKT') {
            $('#cancel').hide();
        } else {
            $('#cancel').show();
        }


        /* Declare Toast */
        const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000,
            timerProgressBar: true,
            didOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
            }
        });

        /* Format Rupiah */
        function FormatRupiah(angka, prefix) {
            if (!angka) {
                return '';
            }
            var vangka = angka.toString();
            var number_string = vangka.replace(/[^.\d]/g, '').replace(/[^\w\s]/gi, '').toString(),
                split = number_string.split('.'),
                sisa = split[0].length % 3,
                rupiah = split[0].substr(0, sisa),
                ribuan = split[0].substr(sisa).match(/\d{3}/gi);

            // tambahkan titik jika yang di input sudah menjadi angka ribuan
            if (ribuan) {
                separator = sisa ? ',' : '';
                rupiah += separator + ribuan.join(',');
            }

            rupiah = split[1] != undefined ? rupiah + '.' + split[1] : rupiah;
            return prefix == undefined ? rupiah : (rupiah ? rupiah : '');
        };

        function FormatDate(input) {
            var date = new Date(input);
            var yyyy = date.getFullYear();
            var mm = date.getMonth() + 1;
            var dd = date.getDate();
            var hrs = date.getHours();
            var mnt = date.getMinutes();


            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }
            if (hrs < 10) {
                hrs = '0' + hrs;
            }
            if (mnt < 10) {
                mnt = '0' + mnt;
            }
            return yyyy + '-' + mm + '-' + dd;
        };

        function getDigitToFormat(val) {
            if (val < 10) {
                val = '0' + val;
            }
            return val.toString();
        };

        function CurrentDate(input) {
            var date = new Date(input);
            var yyyy = date.getFullYear();
            var mm = date.getMonth() + 1;
            var dd = date.getDate();
            var hrs = date.getHours();
            var mnt = date.getMinutes();


            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }
            if (hrs < 10) {
                hrs = '0' + hrs;
            }
            if (mnt < 10) {
                mnt = '0' + mnt;
            }
            return yyyy + '' + mm;
        };

        /* Format Rupiah Saat Event Keyup */
        $('#qty').keyup(function() {
            $(this).val(FormatRupiah($(this).val()));
        })
        $('#chgqty').keyup(function() {
            $(this).val(FormatRupiah($(this).val()));
        })

        /* DeFormat Rupiah */
        function DeFormatRupiah(angka) {
            var result = angka.replace(/[^\w\s]/gi, '');

            return result;
        };

        // $('#chgqty').keyup(function() {
        //     if (DeFormatRupiah($('#chgqty').val()) > DeFormatRupiah($('#chgqtysistem').val())) {
        //         $('#changedetail').hide();
        //     }
        //     else if (DeFormatRupiah($('#chgqty').val()) < DeFormatRupiah($('#chgqtysistem').val())) {
        //         $('#changedetail').hide();
        //     } 
        //     else {
        //         $('#changedetail').show();
        //     }
        // })

        $('#carigudang').click(function() {
            $('#kodebarang').val('');
            $('#namabarang').val('');
            $('#qty').val('');
            // $('#tb_detail').empty();
        })

        /* Cari Data Gudang*/
        document.getElementById("carigudang").addEventListener("click", function(event) {
            event.preventDefault();
            $('#t_gudang').DataTable({
                "destroy": true,
                "searching": true,
                "processing": true,
                "serverSide": true,
                "lengthChange": true,
                "pageLength": 5,
                "lengthMenu": [5, 10, 25, 50],
                "order": [],
                "ajax": {
                    "url": "<?= base_url('masterdata/Gudang/CariDataGudang'); ?>",
                    "method": "POST",
                    "data": {
                        nmtb: "glbm_gudang",
                        field: {
                            kode: "kode",
                            nama: "nama",
                            alamat: "alamat",
                            kodecabang: "kodecabang"
                        },
                        sort: "kode",
                        where: {
                            kode: "kode",
                            nama: "nama"
                        },
                        value: "aktif = true AND kodecabang = '" + $('#cabang').val() + "'"
                    },
                }
            });
        });

        /*Get Data Gudang*/
        $(document).on('click', ".searchgudang", function() {
            var kode = $(this).attr("data-id");
            $.ajax({
                url: "<?= base_url('masterdata/Gudang/DataGudang'); ?>",
                method: "POST",
                dataType: "json",
                async: false,
                data: {
                    kode: kode
                },
                success: function(data) {
                    for (var i = 0; i < data.length; i++) {
                        $('#kodegudang').val(data[i].kode.trim());
                        $('#namagudang').val(data[i].nama.trim());
                    }
                }
            }, false);
        });

        /* Cari Data Asal*/
        document.getElementById("cariasal").addEventListener("click", function(event) {
            event.preventDefault();
            $('#t_asal').DataTable({
                "destroy": true,
                "searching": true,
                "processing": true,
                "serverSide": true,
                "lengthChange": true,
                "pageLength": 5,
                "lengthMenu": [5, 10, 25, 50],
                "order": [],
                "ajax": {
                    "url": "<?= base_url('inventory/TransferStok/CariDataAsal'); ?>",
                    "method": "POST",
                    "data": {
                        nmtb: "glbm_gudang",
                        field: {
                            kode: "kode",
                            nama: "nama",
                            alamat: "alamat",
                            kodecabang: "kodecabang"
                        },
                        sort: "kode",
                        where: {
                            kode: "kode",
                            nama: "nama"
                        },
                        value: "aktif = true AND kodecabang = '" + $('#cabang').val() + "'"
                    },
                }
            });
        });

        /*Get Data Asal*/
        $(document).on('click', ".searchasal", function() {
            var kode = $(this).attr("data-id");
            $.ajax({
                url: "<?= base_url('inventory/TransferStok/DataAsal'); ?>",
                method: "POST",
                dataType: "json",
                async: false,
                data: {
                    kode: kode
                },
                success: function(data) {
                    for (var i = 0; i < data.length; i++) {
                        $('#kodegdasal').val(data[i].kode.trim());
                        // $('#namagudang').val(data[i].nama.trim());
                    }
                }
            }, false);
        });

        /* Cari Data Cabang Tujuan*/
        document.getElementById("caricabang").addEventListener("click", function(event) {
            event.preventDefault();
            $('#t_cabang').DataTable({
                "destroy": true,
                "searching": true,
                "processing": true,
                "serverSide": true,
                "lengthChange": true,
                "pageLength": 5,
                "lengthMenu": [5, 10, 25, 50],
                "order": [],
                "ajax": {
                    "url": "<?= base_url('inventory/TransferStok/CariDataCabangTujuan'); ?>",
                    "method": "POST",
                    "data": {
                        nmtb: "glbm_cabang",
                        field: {
                            kode: "kode",
                            nama: "nama",
                            alamat: "alamat"
                            // kodecabang: "kodecabang"
                        },
                        sort: "kode",
                        where: {
                            kode: "kode",
                            nama: "nama"
                        },
                        value: "aktif = true"
                    },
                }
            });
        });

        /*Get Data Cabang Tujuan*/
        $(document).on('click', ".searchcbtujuan", function() {
            var kode = $(this).attr("data-id");
            $.ajax({
                url: "<?= base_url('inventory/TransferStok/DataCabangTujuan'); ?>",
                method: "POST",
                dataType: "json",
                async: false,
                data: {
                    kode: kode
                },
                success: function(data) {
                    for (var i = 0; i < data.length; i++) {
                        $('#kodecbtujuan').val(data[i].kode.trim());
                        // $('#namagudang').val(data[i].nama.trim());
                    }
                }
            }, false);
        });

        /* Cari Data Gudang Tujuan*/
        document.getElementById("carigudang").addEventListener("click", function(event) {
            event.preventDefault();
            $('#t_gudang').DataTable({
                "destroy": true,
                "searching": true,
                "processing": true,
                "serverSide": true,
                "lengthChange": true,
                "pageLength": 5,
                "lengthMenu": [5, 10, 25, 50],
                "order": [],
                "ajax": {
                    "url": "<?= base_url('inventory/TransferStok/CariDataGudangTujuan'); ?>",
                    "method": "POST",
                    "data": {
                        nmtb: "glbm_gudang",
                        field: {
                            kode: "kode",
                            nama: "nama",
                            alamat: "alamat",
                            kodecabang: "kodecabang"
                        },
                        sort: "kode",
                        where: {
                            kode: "kode",
                            nama: "nama",
                            alamat: "alamat",
                            kodecabang: "kodecabang"
                        },
                        value: "aktif = true AND kodecabang = '" + $('#kodecbtujuan').val() + "'"
                    },
                }
            });
        });

        /*Get Data Gudang Tujuan*/
        $(document).on('click', ".searchgdtujuan", function() {
            var kode = $(this).attr("data-id");
            $.ajax({
                url: "<?= base_url('inventory/TransferStok/DataGudangTujuan'); ?>",
                method: "POST",
                dataType: "json",
                async: false,
                data: {
                    kode: kode
                },
                success: function(data) {
                    for (var i = 0; i < data.length; i++) {
                        $('#kodegdtujuan').val(data[i].kode.trim());
                        // $('#namagudang').val(data[i].nama.trim());
                    }
                }
            }, false);
        });

        document.getElementById("caribarang").addEventListener("click", function(event) {
            event.preventDefault();
            // $('#tb_detail').empty();
            var currentDate = CurrentDate(new Date());
            // var kodegudang = $('#kodegudang').val();
            var kodemerk = $('#kodemerk').val();
            var value = "";
            if (kodemerk == "") {
                value = "aktif = true AND kodecabang = '" + $('#kodecbasal').val() + "' AND kodegudang = '" + $('#kodegdasal').val() + "' AND stock >= 0 AND periode = '" + currentDate + "'";
            } else {
                value = "aktif = true AND kodecabang = '" + $('#kodecbasal').val() + "' AND kodegudang = '" + $('#kodegdasal').val() + "' AND stock >= 0 AND periode = '" + currentDate + "' AND kodemerk = '" + kodemerk + "'";
            }
            $('#t_barang').DataTable({
                "destroy": true,
                "searching": true,
                "processing": true,
                "serverSide": true,
                "lengthChange": true,
                "pageLength": 5,
                "lengthMenu": [5, 10, 25, 50],
                "order": [],
                "scrollX": true,
                "ajax": {
                    "url": "<?= base_url('inventory/TransferStok/CariDataBarang'); ?>",
                    "method": "POST",
                    "data": {
                        nmtb: "cari_barangstock",
                        field: {
                            kodebarang: "kodebarang",
                            namabarang: "namabarang",
                            namasatuan: "namasatuan",
                            namamerk: "namamerk",
                            stock: "stock"
                        },
                        sort: "kodebarang",
                        where: {
                            kodebarang: "kodebarang",
                            namabarang: "namabarang",
                            namasatuan: "namasatuan",
                            namamerk: "namamerk"
                        },
                        value: value
                    }
                },
                "columnDefs": [{
                    "targets": 5,
                    "data": "stock",
                    "render": function(data, type, row, meta) {
                        return FormatRupiah(row[5]);
                    }
                }]
            });
        });

        $(document).on('click', ".searchbarang", function() {
            var kode = $(this).attr("data-id");
            var kodecabang = $('#kodecbasal').val();
            var kodegudang = $('#kodegdasal').val();
            $.ajax({
                url: "<?= base_url('inventory/TransferStok/DataBarang'); ?>",
                method: "POST",
                dataType: "json",
                async: false,
                data: {
                    kode: kode,
                    kodecabang: kodecabang,
                    kodegudang: kodegudang
                },
                success: function(data) {
                    console.log(data);
                    for (var i = 0; i < data.length; i++) {
                        $('#kodebarang').val(data[i].kodebarang.trim());
                        $('#namabarang').val(data[i].namabarang.trim());
                        $('#qtysistem').val(FormatRupiah(data[i].stock.trim()));
                    }
                }
            });
        });

        function ValidasiSave(datadetail) {
            var jenistujuan = $('#jenistujuan').val();
            var kodecbasal = $('#kodecbasal').val();
            var kodegdasal = $('#kodegdasal').val();
            var kodecbtujuan = $('#kodecbtujuan').val();
            var kodegdtujuan = $('#kodegdtujuan').val();
            var keterangan = $('#keterangan').val();
            var nomorsupplier = $('#nomorsupplier').val();
            var namasupplier = $('#namasupplier').val();

            if (jenistujuan == '' || jenistujuan == 0) {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Pilih salah satu Jenis Tujuan.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#jenistujuan').focus();
                var result = false;
            } else if (kodecbasal == '' || kodecbasal == 0) {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Kode Cabang Asal tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#kodecbasal').focus();
                var result = false;
            } else if (kodegdasal == '' || kodegdasal == 0) {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Kode Gudang Asal tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#kodegdasal').focus();
                var result = false;
            } else if (kodecbtujuan == '' || kodecbtujuan == 0) {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Kode Cabang Tujuan tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#kodecbtujuan').focus();
                var result = false;
            } else if (kodegdtujuan == '' || kodegdtujuan == 0) {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Kode Gudang Tujuan tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#kodegdtujuan').focus();
                var result = false;
            } else if (keterangan == '' || keterangan == 0) {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Keterangan tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#keterangan').focus();
                var result = false;
            } else if (nomorsupplier == '' || nomorsupplier == 0) {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Nomor Supplier tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#nomorsupplier').focus();
                var result = false;
            } else if (namasupplier == '' || namasupplier == 0) {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Nama Supplier tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#namasupplier').focus();
                var result = false;
            } else if (kodegdtujuan == kodegdasal) {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Tidak dapat mengirim ke gudang yang sama dengan gudang asal.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#kodegdasal').focus();
                var result = false;
            } else if (datadetail.length == 0) {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Data Detail tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#namabarang').focus();
                var result = false;
            } else {
                var result = true;
            }
            return result;
        }

        /* Validasi Data Add Detail */
        function ValidasiAddDetailGudang(kodebarang) {
            var table = document.getElementById('t_detail');
            var kodebarang = $('#kodebarang').val();
            var namabarang = $('#namabarang').val();
            var qty = $('#qty').val();
            var qtysistem = $('#qtysistem').val();

            if (kodebarang == '' || kodebarang == 0) {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Kode Barang tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#kodebarang').focus();
                return "gagal";
            } else if (namabarang == '' || namabarang == 0) {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Nama Barang tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#namabarang').focus();
                return "gagal";
            } else if (qty == '' || qty == 0) {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Qty tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#qty').focus();
                return "gagal";
            } else if (Number(qty) > Number(qtysistem)) {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Qty tidak boleh lebih dari Qty Sistem.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#qty').focus();
                return "gagal";
            } else if (Number(qty) < Number(qtysistem)) {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Qty tidak boleh kurang dari Qty Sistem.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#qty').focus();
                return "gagal";
            } else {
                for (var r = 1, n = table.rows.length; r < n; r++) {
                    var string = "";
                    for (var c = 0, m = table.rows[r].cells.length; c < m; c++) {
                        if (c == 0) {
                            if (table.rows[r].cells[c].innerHTML == kodebarang) {
                                Swal.fire({
                                    title: 'Informasi',
                                    icon: 'info',
                                    html: 'Data ini sudah diinput.',
                                    showCloseButton: true,
                                    width: 350,
                                });
                                return "gagal";
                            }
                        }
                    }
                }
            }
            return "sukses"
        }

        /* Validasi Data Add Detail */
        function ValidasiAddDetailCabang(kodebarang) {
            var table = document.getElementById('t_detail');
            var kodebarang = $('#kodebarang').val();
            var namabarang = $('#namabarang').val();
            var qty = $('#qty').val();
            var qtysistem = $('#qtysistem').val();

            if (kodebarang == '' || kodebarang == 0) {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Kode Barang tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#kodebarang').focus();
                return "gagal";
            } else if (namabarang == '' || namabarang == 0) {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Nama Barang tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#namabarang').focus();
                return "gagal";
            } else if (qty == '' || qty == 0) {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Qty tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#qty').focus();
                return "gagal";
            } else if (Number(qty) > Number(qtysistem)) {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Qty tidak boleh lebih dari Qty Sistem.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#qty').focus();
                return "gagal";
            } else {
                for (var r = 1, n = table.rows.length; r < n; r++) {
                    var string = "";
                    for (var c = 0, m = table.rows[r].cells.length; c < m; c++) {
                        if (c == 0) {
                            if (table.rows[r].cells[c].innerHTML == kodebarang) {
                                Swal.fire({
                                    title: 'Informasi',
                                    icon: 'info',
                                    html: 'Data ini sudah diinput.',
                                    showCloseButton: true,
                                    width: 350,
                                });
                                return "gagal";
                            }
                        }
                    }
                }
            }
            return "sukses"
        }

        function InsertDataDetail(kodebarang, namabarang, qtysistem, qty) {
            var row = "";
            row =
                '<tr id="' + kodebarang + '">' +
                '<td nowrap>' + kodebarang + '</td>' +
                '<td nowrap>' + namabarang + '</td>' +
                '<td nowrap style="display:none;">' + FormatRupiah(qtysistem) + '</td>' +
                '<td nowrap>' + FormatRupiah(qty) + '</td>' +
                '<td nowrap style="text-align: center; width: 200px;">' +
                '<button data-table="' + kodebarang + '" data-toggle="modal" data-target="#changeqty" class="editdetail btn btn-sm btn-success m-1 p-1"><i class="fas fa-edit"></i> Edit</button>' +
                '<button data-table="' + kodebarang + '" class="deldetail btn btn-sm btn-danger m-1 p-1"><i class="fa fa-trash mr-1"></i> Delete</button>' +
                '</td>' +
                '</tr>';
            $('#tb_detail').append(row);
        };

        // $("#adddetail").click(function() {
        //     var kodebarang = $('#kodebarang').val();
        //     var namabarang = $('#namabarang').val();
        //     var qtysistem = $('#qtysistem').val();
        //     var qty = $('#qty').val();
        //     var jenistujuan = $('#jenistujuan').val();

        //     if (jenistujuan == '1' || jenistujuan == 1) {
        //         if (ValidasiAddDetailCabang(kodebarang) == "sukses") {
        //             var row = "";
        //             row =
        //                 '<tr id="' + kodebarang + '">' +
        //                 '<td nowrap>' + kodebarang + '</td>' +
        //                 '<td nowrap>' + namabarang + '</td>' +
        //                 '<td nowrap style="display:none;">' + FormatRupiah(qtysistem) + '</td>' +
        //                 '<td nowrap>' + FormatRupiah(qty) + '</td>' +
        //                 '<td nowrap style="text-align: center; width: 200px;">' +
        //                 '<button data-table="' + kodebarang + '" data-toggle="modal" data-target="#changeqty" class="editdetail btn btn-sm btn-success m-1 p-1"><i class="fas fa-edit"></i> Edit</button>' +
        //                 '<button data-table="' + kodebarang + '" class="deldetail btn btn-sm btn-danger m-1 p-1"><i class="fa fa-trash mr-1"></i> Delete</button>' +
        //                 '</td>' +
        //                 '</tr>';
        //             $('#tb_detail').append(row);
        //             $('#kodebarang').val("");
        //             $('#namabarang').val("");
        //             $('#qty').val("");
        //             $('#qtysistem').val("");
        //         }
        //     } else {
        //         if (ValidasiAddDetailGudang(kodebarang) == "sukses") {
        //             var row = "";
        //             row =
        //                 '<tr id="' + kodebarang + '">' +
        //                 '<td nowrap>' + kodebarang + '</td>' +
        //                 '<td nowrap>' + namabarang + '</td>' +
        //                 '<td nowrap style="display:none;">' + FormatRupiah(qtysistem) + '</td>' +
        //                 '<td nowrap>' + FormatRupiah(qty) + '</td>' +
        //                 '<td nowrap style="text-align: center; width: 200px;">' +
        //                 '<button data-table="' + kodebarang + '" data-toggle="modal" data-target="#changeqty" class="editdetail btn btn-sm btn-success m-1 p-1"><i class="fas fa-edit"></i> Edit</button>' +
        //                 '<button data-table="' + kodebarang + '" class="deldetail btn btn-sm btn-danger m-1 p-1"><i class="fa fa-trash mr-1"></i> Delete</button>' +
        //                 '</td>' +
        //                 '</tr>';
        //             $('#tb_detail').append(row);
        //             $('#kodebarang').val("");
        //             $('#namabarang').val("");
        //             $('#qty').val("");
        //             $('#qtysistem').val("");
        //         }
        //     }
        // });

        function myFunction(nomor, kodebarang, namabarang, qtysistem, qty, action) {
            var table = document.getElementById('t_detail').getElementsByTagName('tbody')[0];
            var row = table.insertRow(0);
            var kodepencarian = kodebarang.replace(/[^a-zA-Z0-9 ]/g, '');
            row.setAttribute("id", kodepencarian);
            // console.log(row)
            var cell1 = row.insertCell(0);
            var cell2 = row.insertCell(1);
            var cell3 = row.insertCell(2);
            var cell4 = row.insertCell(3);
            var cell5 = row.insertCell(4);
            var cell6 = row.insertCell(5);
            // var cell3 = '<button data-table="' + RejectSpecialChar(kodebarang) + '" class="deldetail btn btn-danger m-1 p-1"><i class="fa fa-trash mr-1"></i> Delete</button>';
            cell1.innerHTML = nomor;
            cell2.innerHTML = kodebarang;
            cell3.innerHTML = namabarang;
            cell4.innerHTML = FormatRupiah(qtysistem);
            cell5.innerHTML = FormatRupiah(qty);
            cell6.innerHTML = action;
            cell5.setAttribute("id", "qty" + kodepencarian);
            cell4.style.display = 'none';
            // row.innerHTML = kodebarang;

            var tbody = $('#t_detail').find('tbody');
            var newRows = tbody.find('tr').sort(function(a, b) {
                return $('td:eq(2)', a).text().localeCompare($('td:eq(2)', b).text());
            });
            console.log(newRows);
            tbody.append(newRows);
        }

        $('#adddetail').click(function() {
            var kodebarang = $('#kodebarang').val();
            var namabarang = $('#namabarang').val();
            var qtysistem = $('#qtysistem').val();
            var qty = $('#qty').val();
            var jenistujuan = $('#jenistujuan').val();
            if (jenistujuan == '1' || jenistujuan == 1) {
                if (ValidasiAddDetailCabang(kodebarang) == 'sukses') {
                    myFunction(
                        '',
                        kodebarang,
                        namabarang,
                        qtysistem,
                        qty,
                        '<div class="text-center"><button data-toggle="modal" data-target="#changeqty" class="editdetail btn btn-sm btn-success mb-0 p-1"><i class="far fa-edit"></i> Edit</button>&nbsp;<button class="deldetail btn btn-danger m-1 p-1"><i class="fa fa-trash mr-1"></i> Delete</button></div>');

                    $('#kodebarang').val("");
                    $('#namabarang').val("");
                    $('#qty').val(0);
                }
            } else {
                if (ValidasiAddDetailGudang(kodebarang) == "sukses") {
                    myFunction(
                        '',
                        kodebarang,
                        namabarang,
                        qtysistem,
                        qty,
                        '<div class="text-center"><button data-toggle="modal" data-target="#changeqty" class="editdetail btn btn-sm btn-success mb-0 p-1"><i class="far fa-edit"></i> Edit</button>&nbsp;<button class="deldetail btn btn-danger m-1 p-1"><i class="fa fa-trash mr-1"></i> Delete</button></div>');

                    $('#kodebarang').val("");
                    $('#namabarang').val("");
                    $('#qtysistem').val(0);
                    $('#qty').val(0);
                }
            }
        });

        $('#t_detail').on('click', '.deldetail', function() {
            $(this).closest('tr').remove();
        })

        $(document).on('click', '.editdetail', function() {
            _row = $(this);
            $('#chgkode').val(_row.closest("tr").find("td:eq(1)").text());
            $('#chgnama').val(_row.closest("tr").find("td:eq(2)").text());
            $('#chgqtysistem').val(_row.closest("tr").find("td:eq(3)").text());
            $('#chgqty').val(_row.closest("tr").find("td:eq(4)").text());
        });

        // $(document).on('click', '.editdetail', function() {
        //     var table = document.getElementById('t_detail');
        //     var tr = document.getElementById($(this).attr("data-table"));
        //     var td = tr.getElementsByTagName("td");
        //     for (var c = 0, m = table.rows[1].cells.length; c < m - 1; c++) {
        //         var chgth = table.rows[1].cells[c].innerHTML.replace(/ /g, '').replace(/\s/g, '').toLowerCase();
        //         var valuetd = td[c].innerHTML;
        //         $('#chg' + chgth).val(valuetd);
        //     }
        // });

        $(document).on('click', '#changedetail', function() {
            var kodebarang = $('#chgkode').val();
            var kodepencarian = kodebarang.replace(/[^a-zA-Z0-9 ]/g, '');
            var namabarang = $('#chgnama').val();
            var qty = $('#chgqty').val();
            var qtysistem = $('#chgqtysistem').val();
            var nomor = "";
            var action = '<div class="text-center"><button data-toggle="modal" data-target="#changeqty" class="editdetail btn btn-sm btn-success mb-0 p-1"><i class="far fa-edit"></i> Edit</button>&nbsp;<button class="deldetail btn btn-danger m-1 p-1"><i class="fa fa-trash mr-1"></i> Delete</button></div>';
            $('#' + kodepencarian).remove();
            myFunction(nomor, kodebarang, namabarang, qtysistem, qty, action);
        });

        // $(document).on('click', '.deldetail', function() {
        //     var id = $(this).attr("data-table");
        //     $('#' + id).remove();
        // });

        function AmbilDataDetail() {
            var table = document.getElementById('t_detail');
            var arr2 = [];
            for (var r = 1, n = table.rows.length; r < n; r++) {
                var string = "";
                for (var c = 0, m = table.rows[r].cells.length; c < m - 1; c++) {
                    if (c == 0) {
                        string = "{" + table.rows[0].cells[c].innerHTML + " : '" + table.rows[r].cells[c].innerHTML + "'";
                    } else {
                        string = string + ", " + table.rows[0].cells[c].innerHTML + " : '" + table.rows[r].cells[c].innerHTML + "'";
                    }
                }
                string = string + "}";
                var obj = JSON.stringify(eval('(' + string + ')'));
                var arr = $.parseJSON(obj);
                arr2.push(arr);
            }
            return arr2;
        };

        /* Cari Data Merk*/
        document.getElementById("carimerk").addEventListener("click", function(event) {
            event.preventDefault();
            $('#t_merk').DataTable({
                "destroy": true,
                "searching": true,
                "processing": true,
                "serverSide": true,
                "lengthChange": true,
                "pageLength": 5,
                "lengthMenu": [5, 10, 25, 50],
                "order": [],
                "ajax": {
                    "url": "<?= base_url('masterdata/Merk/CariDataMerk'); ?>",
                    "method": "POST",
                    "data": {
                        nmtb: "glbm_merk",
                        field: {
                            kode: "kode",
                            nama: "nama",
                            // qtysatuan: "qtysatuan",
                            // konversi: "konversi",
                        },
                        sort: "kode",
                        where: {
                            kode: "kode",
                            nama: "nama"
                        },
                        value: "aktif = true"
                    },
                }
            });
        });

        /*Get Data Merk */
        $(document).on('click', ".searchmerk", function() {
            var kode = $(this).attr("data-id");;
            $.ajax({
                url: "<?= base_url('masterdata/Merk/DataMerk'); ?>",
                method: "POST",
                dataType: "json",
                async: false,
                data: {
                    kode: kode
                },
                success: function(data) {
                    for (var i = 0; i < data.length; i++) {
                        $('#kodemerk').val(data[i].kode.trim());
                        // $('#namamerk').val(data[i].nama.trim());
                    }
                }
            }, false);
        });

        /* Save */
        document.getElementById("save").addEventListener("click", function(event) {
            event.preventDefault();
            var nomor = $('#notransfer').val();
            var tanggal = $('#tanggaltransfer').val();
            var jenistujuan = $('#jenistujuan').val();
            var kodecbasal = $('#kodecbasal').val();
            var kodegdasal = $('#kodegdasal').val();
            var kodecbtujuan = $('#kodecbtujuan').val();
            var kodegdtujuan = $('#kodegdtujuan').val();
            var keterangan = $('#keterangan').val();
            var nomorsupplier = $('#nomorsupplier').val();
            var namasupplier = $('#namasupplier').val();

            var datadetail = AmbilDataDetail();

            if (ValidasiSave(datadetail) == true) {
                $.ajax({
                    url: "<?= base_url("inventory/TransferStok/Save") ?>",
                    method: "POST",
                    dataType: "json",
                    async: true,
                    data: {
                        nomor: nomor,
                        tanggal: tanggal,
                        jenistujuan: jenistujuan,
                        kodecbasal: kodecbasal,
                        kodegdasal: kodegdasal,
                        kodecbtujuan: kodecbtujuan,
                        kodegdtujuan: kodegdtujuan,
                        keterangan: keterangan,
                        nomorsupplier: nomorsupplier,
                        namasupplier: namasupplier,
                        datadetail: datadetail
                    },
                    success: function(data) {
                        if (data.nomor != "") {
                            ClearScreen();
                            Swal.fire({
                                icon: 'success',
								title: data.message,
								text: data.nomor
							})
                            // Toast.fire({
                            //     icon: 'success',
                            //     title: data.message
                            // });
                            // $('#notransfer').val(data.nomor);
                            // $('#jenistujuan').prop("disabled", true);
                            // $('#kodecbasal').prop("disabled", true);
                            // $('#kodegdasal').prop("disabled", true);
                            // $('#kodecbtujuan').prop("disabled", true);
                            // $('#kodegdtujuan').prop("disabled", true);
                            // $('#keterangan').prop("disabled", true);
                            // $('#kodebarang').prop("disabled", true);
                            // $('#namabarang').prop("disabled", true);
                            // $('#qty').prop("disabled", true);

                            // $('#carigudang').hide();
                            // $('#carisupplier').hide();
                            // $('#caribarang').hide();
                            // $('#adddetail').hide();
                        } else {
                            // Toast.fire({
                            //     icon: 'error',
                            //     title: data.message
                            // });
                            Swal.fire({
								icon: 'error',
								title: data.message,
								text: data.nomor
							})
                        }
                    }
                }, false)
            }
        })

        /* Cari Data Supplier*/
        document.getElementById("carisupplier").addEventListener("click", function(event) {
            event.preventDefault();
            $('#t_supplier').DataTable({
                "destroy": true,
                "searching": true,
                "processing": true,
                "serverSide": true,
                "lengthChange": true,
                "pageLength": 5,
                "lengthMenu": [5, 10, 25, 50],
                "order": [],
                "ajax": {
                    "url": "<?= base_url('masterdata/Supplier/CariDataSupplier'); ?>",
                    "method": "POST",
                    "data": {
                        nmtb: "glbm_supplier",
                        field: {
                            nomor: "nomor",
                            nama: "nama",
                            alamat: "alamat",
                            statuspkp: "statuspkp",
                            npwp: "npwp",
                            alamatnpwp: "alamatnpwp",
                            notlp: "notlp"
                        },
                        sort: "nomor",
                        where: {
                            nomor: "nomor",
                            nama: "nama"
                        },
                        value: "aktif = true"
                        // value: "aktif = true AND kodecabang = '" + $('#cabang').val() + "'"
                    },
                },
                "columnDefs": [{
                    "targets": 4,
                    "data": "statuspkp",
                    "render": function(data, type, row, meta) {
                        return (row[4] == 't') ? 'Ya' : 'Tidak';
                    }
                }],
            });
        });

        /*Get Data Supplier*/
        $(document).on('click', ".searchsupplier", function() {
            var nomor = $(this).attr("data-id");
            $.ajax({
                url: "<?= base_url('masterdata/Supplier/DataSupplier'); ?>",
                method: "POST",
                dataType: "json",
                async: false,
                data: {
                    nomor: nomor
                },
                success: function(data) {
                    for (var i = 0; i < data.length; i++) {
                        $('#nomorsupplier').val(data[i].nomor.trim());
                        $('#namasupplier').val(data[i].nama.trim());
                    }
                }
            }, false);
        });

        /*Cari Data Transfer Stok*/
        document.getElementById("find").addEventListener("click", function(event) {
            event.preventDefault();
            $('#tb_detail').empty();
            $('#t_ts').DataTable({
                "destroy": true,
                "searching": true,
                "processing": true,
                "serverSide": true,
                "lengthChange": true,
                "pageLength": 5,
                "lengthMenu": [5, 10, 25, 50],
                "order": [],
                "ajax": {
                    "url": "<?= base_url('inventory/TransferStok/CariDataTransferStok'); ?>",
                    "method": "POST",
                    "data": {
                        nmtb: "cari_transferstock",
                        field: {
                            nomor: "nomor",
                            tanggal: "tanggal",
                            keterangan: "keterangan",
                            status: "status",
                            namagdasal: "namagdasal",
                            namagdtujuan: "namagdtujuan",
                            pemakai: "pemakai",
                            tglsimpan: "tglsimpan",
                        },
                        sort: "nomor",
                        where: {
                            nomor: "nomor",
                            namagdasal: "namagdasal",
                            namagdtujuan: "namagdtujuan",
                            keterangan: "keterangan"
                        },
                        value: "batal = false AND kodecbasal = '" + $("#cabang").val() + "'"
                        // value: "batal = false AND kodecbasal = '" + $("#cabang").val() + "'"
                    },
                },
                "columnDefs": [{
                        "targets": 3,
                        "render": $.fn.dataTable.render.moment('YYYY-MM-DD HH:mm:ss', 'D MMM YY'),
                    },
                    {
                        "targets": 5,
                        "data": "status",
                        "render": function(data, type, row, meta) {
                            return (row[5] == 't') ? '<b>Sudah Diterima</b>' : '<p>Belum Diterima</p>';
                        }
                    },
                    {
                        "targets": 9,
                        "render": $.fn.dataTable.render.moment('YYYY-MM-DD HH:mm:ss', 'D MMM YYYY HH:mm')
                    }
                ]
            });
        })

        $(document).on('click', ".searchts", function() {
            var nomor = $(this).attr("data-id");
            GetData(nomor);
        });

        function GetData(nomor) {
            $.ajax({
                url: "<?= base_url('inventory/TransferStok/DataTransferStok'); ?>",
                method: "POST",
                dataType: "json",
                async: false,
                data: {
                    nomor: nomor
                },
                success: function(data) {
                    console.log(data);
                    for (var i = 0; i < data.length; i++) {
                        $('#notransfer').val(data[i].nomor.trim());
                        $('#tanggaltransfer').val(FormatDate(data[i].tanggal.trim()));
                        $('#jenistujuan').val(data[i].pilihan.trim());
                        $('#nomorsupplier').val(data[i].nosupplier.trim());
                        $('#namasupplier').val(data[i].namasupplier.trim());
                        $('#kodecbasal').val(data[i].kodecbasal.trim());
                        $('#kodegdasal').val(data[i].kodegdasal.trim());
                        $('#kodecbtujuan').val(data[i].kodecbtujuan.trim());
                        $('#kodegdtujuan').val(data[i].kodegdtujuan.trim());
                        $('#keterangan').val(data[i].keterangan.trim());
                        if (data[i].status.trim() == 't') {
                            $('#update').prop('disabled', true);
                            $('#cancel').prop('disabled', true);
                            $('#save').prop('disabled', true);
                        } else {
                            $('#update').prop('disabled', false);
                            $('#save').prop('disabled', true);
                            $('#cancel').prop('disabled', false);
                        }
                        
                        // if (data[i].nomorsj.trim() != '') {
                        //     $('#cancel').prop('disabled', true);
                        //     $('#save').prop('disabled', true);
                        //     $('#update').prop('disabled', true);
                        // } else {
                        //     $('#cancel').prop('disabled', false);
                        //     $('#save').prop('disabled', true);
                        //     $('#update').prop('disabled', false);
                        // }
                    }
                    GetDataDetail(nomor);
                }
            });
        };

        function GetDataDetail(nomor) {
            $.ajax({
                url: "<?= base_url('inventory/TransferStok/DataTransferStokDetail'); ?>",
                method: "POST",
                dataType: "json",
                async: false,
                data: {
                    nomor: nomor
                },
                success: function(data) {
                    console.log(data);
                    for (var i = 0; i < data.length; i++) {
                        var kodebarang = data[i].kodebarang;
                        var namabarang = data[i].namabarang.trim();
                        var qtysistem = data[i].qtysistem.trim();
                        var qty = data[i].qty.trim();
                        var nomor = "";

                        if (data[i].status.trim() == 'f') {
                            var action = '<div class="text-center"><button data-toggle="modal" data-target="#changeqty" class="editdetail btn btn-sm btn-success mb-0 p-1"><i class="far fa-edit"></i> Edit</button>&nbsp;<button class="deldetail btn btn-danger m-1 p-1"><i class="fa fa-trash mr-1"></i> Delete</button></div>'
                            // $('#cancel').prop('disabled', false);
                            // $('#update').prop('disabled', false);
                            // $('#save').prop('disabled', true);
                        } else {
                            var action = '-'
                            // $('#cancel').prop('disabled', true);
                            // $('#update').prop('disabled', true);
                            // $('#save').prop('disabled', true);
                            $('.editdetail').hide();
                            $('.deldetail').hide();
                        }
                        myFunction(nomor, kodebarang, namabarang, qtysistem, qty, action);
                    }
                }
            });
        };

        /* Update */
        document.getElementById("update").addEventListener("click", function(event) {
            event.preventDefault();
            var nomor = $('#notransfer').val();
            var tanggal = $('#tanggaltransfer').val();
            var jenistujuan = $('#jenistujuan').val();
            var kodecbasal = $('#kodecbasal').val();
            var kodegdasal = $('#kodegdasal').val();
            var kodecbtujuan = $('#kodecbtujuan').val();
            var kodegdtujuan = $('#kodegdtujuan').val();
            var keterangan = $('#keterangan').val();
            var nomorsupplier = $('#nomorsupplier').val();
            var namasupplier = $('#namasupplier').val();

            var datadetail = AmbilDataDetail();

            if (ValidasiSave(datadetail) == true) {
                $.ajax({
                    url: "<?= base_url("inventory/TransferStok/Update") ?>",
                    method: "POST",
                    dataType: "json",
                    async: true,
                    data: {
                        nomor: nomor,
                        tanggal: tanggal,
                        jenistujuan: jenistujuan,
                        kodecbasal: kodecbasal,
                        kodegdasal: kodegdasal,
                        kodecbtujuan: kodecbtujuan,
                        kodegdtujuan: kodegdtujuan,
                        keterangan: keterangan,
                        nomorsupplier: nomorsupplier,
                        namasupplier: namasupplier,
                        datadetail: datadetail
                    },
                    success: function(data) {
                        if (data.nomor != "") {
                            Toast.fire({
                                icon: 'success',
                                title: data.message
                            });
                            $('#notransfer').val(data.nomor);
                            $('#jenistujuan').prop("disabled", true);
                            $('#kodecbasal').prop("disabled", true);
                            $('#kodegdasal').prop("disabled", true);
                            $('#kodecbtujuan').prop("disabled", true);
                            $('#kodegdtujuan').prop("disabled", true);
                            $('#keterangan').prop("disabled", true);
                            $('#kodebarang').prop("disabled", true);
                            $('#namabarang').prop("disabled", true);
                            $('#qty').prop("disabled", true);

                            $('#update').prop("disabled", true);
                            $('#save').prop("disabled", true);

                            $('#carigudang').hide();
                            $('#carisupplier').hide();
                            $('#caribarang').hide();
                            $('#adddetail').hide();
                        } else {
                            Toast.fire({
                                icon: 'error',
                                title: data.message
                            });
                        }
                    }
                }, false)
            }
        })

        document.getElementById('okcancel').addEventListener("click", function(event) {
            event.preventDefault();
            var nomor = $('#notransfer').val();
            var kodegdasal = $('#kodegdasal').val();
            var keterangan = $('#keteranganbatal').val();
            var datadetail = AmbilDataDetail();
            if (nomor == "") {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Pilih Dulu Nomor Yang Ingin dibatalkan',
                    showCloseButton: true,
                    width: 350,
                });
            } else if (keterangan == "") {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Keterangan Batal Tidak Boleh Kosong',
                    showCloseButton: true,
                    width: 350,
                });
            } else {
                $.ajax({
                    url: "<?= base_url("inventory/TransferStok/Cancel") ?>",
                    method: "POST",
                    dataType: "json",
                    async: true,
                    data: {
                        nomor: nomor,
                        kodegdasal: kodegdasal,
                        keterangan: keterangan,
                        datadetail: datadetail
                    },
                    success: function(data) {
                        console.log(data);
                        if (data.nomor != "") {
                            Toast.fire({
                                icon: 'success',
                                title: data.message
                            });
                            // RefreshLayar();
                        } else {
                            Toast.fire({
                                icon: 'error',
                                title: data.message
                            });
                        }
                    }
                }, false)
            }
        })

        document.getElementById("clear").addEventListener("click", function(event) {
            event.preventDefault();
            ClearScreen();
        })

        ClearScreen();

        $(document).on("click", ".cetaktransferstok", function() {
            var nomor = $(this).attr("data-id");
            CetakTransferStok(nomor);
            CetakTransferStokUpdate(nomor);
        });

        function CetakTransferStok(nomor) {
            window.open("<?= base_url() ?>cetak_pdf/TransferStok/Print/" + nomor);
        }

        function CetakTransferStokUpdate(nomor) {
            $.ajax({
                url: "<?= base_url("inventory/TransferStok/CetakTransferStokUpdate") ?>",
                method: "POST",
                dataType: "json",
                async: true,
                data: {
                    nomor: nomor
                },
                success: function(data) {
                    // if (data.nomor != "") {
                    //     Toast.fire({
                    //         icon: 'success',
                    //         title: data.message
                    //     });
                    // } else {
                    //     Toast.fire({
                    //         icon: 'error',
                    //         title: data.message
                    //     });
                    // }
                }
            }, false)
        }

    })
</script>