<script type="text/javascript">
	$(document).ready(function() {
		function FormatDate(input) {
			var date = new Date(input);
			var yyyy = date.getFullYear();
			var mm = date.getMonth() + 1;
			var dd = date.getDate();
			var hrs = date.getHours();
			var mnt = date.getMinutes();


			if (dd < 10) {
				dd = '0' + dd;
			}
			if (mm < 10) {
				mm = '0' + mm;
			}
			if (hrs < 10) {
				hrs = '0' + hrs;
			}
			if (mnt < 10) {
				mnt = '0' + mnt;
			}
			return yyyy + '-' + mm + '-' + dd;
		};

		/* Format Rupiah */
		function FormatRupiah(angka, prefix) {
			if (!angka) {
				return '';
			}
			var vangka = angka.toString();
			var number_string = vangka.replace(/[^.\d]/g, '').replace(/[^\w\s]/gi, '').toString(),
				split = number_string.split('.'),
				sisa = split[0].length % 3,
				rupiah = split[0].substr(0, sisa),
				ribuan = split[0].substr(sisa).match(/\d{3}/gi);

			// tambahkan titik jika yang di input sudah menjadi angka ribuan
			if (ribuan) {
				separator = sisa ? ',' : '';
				rupiah += separator + ribuan.join(',');
			}

			rupiah = split[1] != undefined ? rupiah + '.' + split[1] : rupiah;
			return prefix == undefined ? rupiah : (rupiah ? rupiah : '');
		};

		/* DeFormat Rupiah */
		function DeFormatRupiah(angka) {
			var result = angka.replace(/[^\w\s]/gi, '');

			return result;
		};

		function ValidasiConfirm() {
			var keterangan = $('#keteranganbag').val();
			var jenissistem = $('#jenissistem').val();

			if (keterangan == "" || keterangan == 0) {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Keterangan Recount tidak boleh kosong',
					showCloseButton: true,
					width: 350
				});
				$('#keteranganbag').focus();
				var result = false;
			} else if (jenissistem == "" || jenissistem == 0) {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Jenis Tidak boleh kosong Pilih Salah Satu',
					showCloseButton: true,
					width: 350
				});
				$('#jenissistem').focus();
				var result = false;
			} else {
				var result = true
			}
			return result;
		}

		const Toast = Swal.mixin({
			toast: true,
			position: 'top-end',
			showConfirmButton: false,
			timer: 3000,
			timerProgressBar: true,
			didOpen: (toast) => {
				toast.addEventListener('mouseenter', Swal.stopTimer)
				toast.addEventListener('mouseleave', Swal.resumeTimer)
			}
		})


		function AmbilDataSelisih() {
			var table = document.getElementById('t_selisih');
			var arr2 = [];
			for (var r = 1, n = table.rows.length; r < n; r++) {
				var string = "";
				for (var c = 0, m = table.rows[r].cells.length; c < m - 1; c++) {
					if (c == 0) {
						string = "{" + table.rows[0].cells[c].innerHTML + " : '" + table.rows[r].cells[c].innerHTML + "'";
					} else {
						string = string + ", " + table.rows[0].cells[c].innerHTML + " : '" + table.rows[r].cells[c].innerHTML + "'";
					}
				}
				string = string + "}";
				var obj = JSON.stringify(eval('(' + string + ')'));
				var arr = $.parseJSON(obj);
				arr2.push(arr);
			}
			return arr2;
		}

		function AmbilDataMinus() {
			var table = document.getElementById('t_min');
			var arr2 = [];
			for (var r = 1, n = table.rows.length; r < n; r++) {
				var string = "";
				for (var c = 0, m = table.rows[r].cells.length; c < m - 1; c++) {
					if (c == 0) {
						string = "{" + table.rows[0].cells[c].innerHTML + " : '" + table.rows[r].cells[c].innerHTML + "'";
					} else {
						string = string + ", " + table.rows[0].cells[c].innerHTML + " : '" + table.rows[r].cells[c].innerHTML + "'";
					}
				}
				string = string + "}";
				var obj = JSON.stringify(eval('(' + string + ')'));
				var arr = $.parseJSON(obj);
				arr2.push(arr);
			}
			return arr2;
		}

		function AmbilDataPass() {
			var table = document.getElementById('t_pass');
			var arr2 = [];
			for (var r = 1, n = table.rows.length; r < n; r++) {
				var string = "";
				for (var c = 0, m = table.rows[r].cells.length; c < m - 1; c++) {
					if (c == 0) {
						string = "{" + table.rows[0].cells[c].innerHTML + " : '" + table.rows[r].cells[c].innerHTML + "'";
					} else {
						string = string + ", " + table.rows[0].cells[c].innerHTML + " : '" + table.rows[r].cells[c].innerHTML + "'";
					}
				}
				string = string + "}";
				var obj = JSON.stringify(eval('(' + string + ')'));
				var arr = $.parseJSON(obj);
				arr2.push(arr);
			}
			return arr2;
		}


		function InsertDataSelisih(nomorsto, tanggalsto, kodebarang, namabarang, qty, qtysistem, selisih, expired, ketdetail) {
			var row = "";
			row =
				'<tr id="' + nomorsto + '">' +
				'<td nowrap>' + nomorsto + '</td>' +
				'<td nowrap>' + FormatDate(tanggalsto) + '</td>' +
				'<td nowrap>' + kodebarang + '</td>' +
				'<td nowrap>' + namabarang + '</td>' +
				'<td nowrap>' + FormatDate(expired) + '</td>' +
				'<td nowrap>' + FormatRupiah(qty) + '</td>' +
				'<td nowrap>' + FormatRupiah(qtysistem) + '</td>' +
				'<td nowrap>' + FormatRupiah(selisih) + '</td>' +
				'<td nowrap>' + ketdetail + '</td>' +
				'</tr>';
			$('#tb_selisih').append(row);
		};

		function InsertDataMin(nomorsto, tanggalsto, kodebarang, namabarang, qty, qtysistem, selisih, expired, ketdetail) {
			var row = "";
			row = '<tr id="' + nomorsto + '">' +
				'<td nowrap>' + nomorsto + '</td>' +
				'<td nowrap>' + FormatDate(tanggalsto) + '</td>' +
				'<td nowrap>' + kodebarang + '</td>' +
				'<td nowrap>' + namabarang + '</td>' +
				'<td nowrap>' + FormatDate(expired) + '</td>' +
				'<td nowrap>' + FormatRupiah(qty) + '</td>' +
				'<td nowrap>' + FormatRupiah(qtysistem) + '</td>' +
				'<td nowrap>' + FormatRupiah(selisih) + '</td>' +
				'<td nowrap>' + ketdetail + '</td>' +
				'</tr>';
			$('#tb_min').append(row);
		}

		function InsertDataPass(nomorsto, tanggalsto, kodebarang, namabarang, qty, qtysistem, selisih, expired, ketdetail) {
			var row = "";
			row = '<tr id="' + nomorsto + '">' +
				'<td nowrap>' + nomorsto + '</td>' +
				'<td nowrap>' + FormatDate(tanggalsto) + '</td>' +
				'<td nowrap>' + kodebarang + '</td>' +
				'<td nowrap>' + namabarang + '</td>' +
				'<td nowrap>' + FormatDate(expired) + '</td>' +
				'<td nowrap>' + FormatRupiah(qty) + '</td>' +
				'<td nowrap>' + FormatRupiah(qtysistem) + '</td>' +
				'<td nowrap>' + FormatRupiah(selisih) + '</td>' +
				'<td nowrap>' + ketdetail + '</td>' +
				'</tr>';
			$('#tb_pass').append(row);
		}

		function DataSelisih(nomor) {
			$.ajax({
				url: "<?= base_url('inventory/StockOpnameRecount/DataStockRecountSelisih'); ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					nomor: nomor
				},
				success: function(data) {

					for (var i = 0; i < data.length; i++) {
						var nomorsto = data[i].nomorsto.trim();
						var tanggalsto = FormatDate(data[i].tglsto.trim());
						var kodebarang = data[i].kodebarang.trim();
						var namabarang = data[i].namabarang.trim();
						var qty = FormatRupiah(data[i].qty.trim());
						var qtysistem = FormatRupiah(data[i].qtysistem.trim());
						var selisih = FormatRupiah(data[i].selisih.trim());
						var expired = FormatDate(data[i].tglexpired.trim());
						var ketdetail = data[i].keterangan.trim();

						InsertDataSelisih(nomorsto, tanggalsto, kodebarang, namabarang, qty, qtysistem, selisih, expired, ketdetail);
					}
				}
			});
		}

		function DataHide(nomor) {
			$.ajax({
				url: "<?= base_url('inventory/StockOpnameRecount/DataStockRecountSelisih') ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					nomor: nomor
				},
				success: function(data) {
					for (var i = 0; i < data.length; i++) {
						if (data[i].selisih < 0) {
							var nomorsto = data[i].nomorsto.trim();
							var tanggalsto = FormatDate(data[i].tglsto.trim());
							var kodebarang = data[i].kodebarang.trim();
							var namabarang = data[i].namabarang.trim();
							var qty = data[i].qty.trim();
							var qtysistem = data[i].qtysistem.trim();
							var selisih = data[i].selisih.trim();
							var expired = FormatDate(data[i].tglexpired.trim());
							var ketdetail = data[i].keterangan.trim();
							InsertDataMin(nomorsto, tanggalsto, kodebarang, namabarang, qty, qtysistem, selisih, expired, ketdetail);
						} else if (data[i].selisih > 0) {
							var nomorsto = data[i].nomorsto.trim();
							var tanggalsto = FormatDate(data[i].tglsto.trim());
							var kodebarang = data[i].kodebarang.trim();
							var namabarang = data[i].namabarang.trim();
							var qty = data[i].qty.trim();
							var qtysistem = data[i].qtysistem.trim();
							var selisih = data[i].selisih.trim();
							var expired = FormatDate(data[i].tglexpired.trim());
							var ketdetail = data[i].keterangan.trim();
							InsertDataPass(nomorsto, tanggalsto, kodebarang, namabarang, qty, qtysistem, selisih, expired, ketdetail);
						}
					}
				}
			});
		}

		var tableapprove = $('#t_approve').DataTable({
			"destroy": true,
			"searching": true,
			"serverSide": true,
			"lengthChange": true,
			"pageLength": 5,
			"lengthMenu": [5, 10, 25, 50],
			"order": [],
			"ajax": {
				"url": "<?php echo base_url('inventory/StockOpnameRecount/CariDataStockRecountAfterConfirm'); ?>",
				"method": "POST",
				"data": {
					nmtb: "srvt_stockopnamerecount",
					field: {
						nomor: "nomor",
						tanggal: "tanggal",
						kodegudang: "kodegudang",
						status: "status_management",
						keterangan: "keterangan"
					},
					sort: "nomor",
					where: {
						nomor: "nomor"
					},
					value: "status = true AND kodecabang = '" + $("#cabang").val() + "'"
				},
			},
			"columnDefs": [{
					"targets": 2,
					"render": $.fn.dataTable.render.moment('YYYY-MM-DD HH:mm:ss', 'D MMM YY')
				},
				{
					"targets": 4,
					"data": "status_management",
					"render": function(data, type, row, meta) {
						return (row[4] == "t") ? "<p>Sudah di approve</p>" : "<p>Belum di approve</p>"
					}

				}
			],

		});

		setInterval(function() {
			tableapprove.ajax.reload(null, false); // user paging is not reset on reload
		}, 3000);


		$(document).on("click", ".viewdata", function() {
			var nomor = $(this).attr("data-id");
			$('#tb_selisih').empty();
			$.ajax({
				url: "<?= base_url('inventory/StockOpnameRecount/DataStockRecount') ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					nomor: nomor
				},
				success: function(data) {
					for (var i = 0; i < data.length; i++) {
						$('#norecount').val(data[i].nomor.trim());
						$('#tanggalrecount').val(FormatDate(data[i].tanggal.trim()));
						$('#keterangan').val(data[i].keterangan.trim());
						$('#jenissistem').val(data[i].jenissistem.trim());
						$('#keteranganbag').val(data[i].keteranganbag.trim());
						
						var status = data[i].status_management.trim();

						if (status == 't') {
							$('#confirmdata').hide();
							$('#jenissistem').prop('disabled', true);
							$('#keteranganbag').prop('disabled', true);
						} else {
							$('#confirmdata').show();
						}
					}

					DataSelisih(nomor);
					DataHide(nomor);
					$('#t_min').hide();
					$('#t_pass').hide();
				}

			})
		});


		document.getElementById('confirmdata').addEventListener("click", function(event) {
			event.preventDefault();
			var nomor = $('#norecount').val();
			var keterangan = $('#keteranganbag').val();
			var jenissistem = $('#jenissistem').val();
			var datamin = AmbilDataMinus();
			var datapass = AmbilDataPass();
			var dataselisih = AmbilDataSelisih();
			if (ValidasiConfirm() == true) {
				if (jenissistem == 1) {
					$.ajax({
						url: "<?= base_url('inventory/StockOpnameRecount/Adjust') ?>",
						method: "POST",
						dataType: "json",
						async: true,
						data: {
							nomor: nomor,
							keterangan: keterangan,
							jenissistem: jenissistem,
							datamin: datamin,
							datapass: datapass,
							dataselisih: dataselisih
						},
						success: function(data) {
							if (data.nomor) {
								Toast.fire({
									icon: 'success',
									title: data.message
								});
								setInterval();
								$('#find').prop('disabled', true);
								$('#proses').prop('disabled', true);
							}
						}
					});
				} else {
					$.ajax({
						url: "<?= base_url('inventory/StockOpnamerecount/Denda') ?>",
						method: "POST",
						dataType: "json",
						async: true,
						data: {
							nomor: nomor,
							kodebarang: kodebarang,
							namabarang: namabarang,
							selisih: selisih,
							keterangan: keterangan
						},
						success: function(data) {
							if (data.nomor) {
								Toast.fire({
									icon: 'sucess',
									title: data.message
								});
								setInterval();
								$('#find').prop('disabled', true);
								$('#proses').prop('disabled', true);
							}
						}
					})
				}
			}
		});
	});
</script>
