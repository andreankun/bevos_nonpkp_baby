<div class="col-xl-12 mb-2">
	<div class="modal-header" style="padding: 5px;">
		<div class="col-xl-12">
			<div class="row">
				<div class="col-md-5">
					<h2>
						<span class="logo-menu"><?= $icons ?></span>
						<span class="text-uppercase"><?= $title ?></span>
					</h2>
				</div>
				<div class="col-md-7">
					<div class="form-group" style="float: right;">
						<!-- <button id="proses" class="btn btn-sm btn-dark mb-1 mt-1 mr-1 ml-1" style="margin-bottom: 0px;"><i class="far fa-save" style="margin-right: 10px;"></i>Proses</button>
						<button id="recount" class="btn btn-sm btn-dark mb-1 mt-1 mr-1 ml-1" style="margin-bottom: 0px;"><i class="far fa-save" style="margin-right: 10px;"></i>Recount</button>
						<button id="confirm" class="btn btn-sm btn-dark mb-1 mt-1 mr-1 ml-1" style="margin-bottom: 0px;"><i class="far fa-save" style="margin-right: 10px;"></i>Confrim</button>
						<button id="adjustsistem" class="btn btn-sm btn-dark mb-1 mt-1 ml-1" style="margin-bottom: 0px;"><i class="far fa-check-circle" style="margin-right: 10px;"></i>Adjust By sistem</button>
						<button id="denda" class="btn btn-sm btn-danger mb-1 mt-1 ml-1" style="margin-bottom: 0px;"><i class="far fa-check-circle" style="margin-right: 10px;"></i>Denda</button>
						<button data-toggle="modal" data-target="#findrecount" id="find" class="btn btn-sm btn-dark mb-1 mt-1 mr-1 ml-1" style="margin-bottom: 0px;"><i class="far fa-file-search" style="margin-right: 10px;"></i>Find</button>
						<button id="clear" class="btn btn-sm btn-dark mb-1 mt-1 mr-1 ml-1" style="margin-bottom: 0px;"><i class="far fa-refresh" style="margin-right: 10px;"></i>Clear</button> -->
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="col-md-12 table-responsive hidescroll" style="height: 555px; overflow-y: scroll;">
	<div class="row">
		<div class="col-12 col-md-12 col-lg-12 col-xl-12">
			<div class="modal-content mb-2">
				<div class="modal-header p-2">
					<h5 class="mb-0">List Data Approve</h5>
				</div>
				<div class="modal-body">
					<div class="row my-3">
						<div class="table-responsive">
							<table id="t_approve" class="table table-bordered table-striped table-default mb-0" style="width:100%;">
								<thead style="background-color: #68478D; color: #eee;">
									<tr style="line-height: 0.5 cm; ">
										<th style="text-align: center;">
											Action
										</th>
										<th style="text-align: center; ">
											Nomor
										</th>
										<th style="text-align: center; ">
											Tanggal
										</th>
										<th style="text-align: center; ">
											KodeGudang
										</th>
										<th style="text-align: center;">
											Status
										</th>
										<th style="text-align: center; ">
											Keterangan
										</th>
									</tr>
								</thead>
								<tbody class="tbody-scroll scroller" id="tb_detail" style="text-align: center;">

								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="datatarget">
	<div class="modal-dialog modal-lg">
		<div class="modal-content" style="border-radius: 5px;">
			<div class="modal-header" style="padding: 10px; color: #000;">
				<h7 class="modal-title" style="margin-left: 5px;"></h7>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="table-responsive">
					<div class="row mb-2">
						<div class="col-sm-6 input-group">
							<div class="col-sm-6 p-0 input-group-prepend">
								<span class="col-sm-12 input-group-text">Nomor Recount</span>
							</div>
							<input class="form-control" type="text" name="norecount" id="norecount" maxlength="50" placeholder="Kode" readonly required />
							<input class="form-control" type="hidden" name="status" id="status" maxlength="50" placeholder="Kode" readonly required />

						</div>
						<div class="col-sm-6 input-group box">
							<div class="col-sm-6 p-0 input-group-prepend">
								<span class="col-sm-12 input-group-text">Tanggal Recount</span>
							</div>
							<input class="form-control" type="text" name="tanggalrecount" id="tanggalrecount" maxlength="50" placeholder="Nama Gudang" required readonly />
						</div>
					</div>
					<div class="row mb-2">
						<div class="col-sm-6 input-group box">
							<div class="col-sm-6 p-0 input-group-prepend">
								<span class="col-sm-12 input-group-text">Jenis Sistem</span>
							</div>
							<select class="form-control" style="height: 29px; padding: 0.175rem 0.50rem;" id="jenissistem">
								<option value="">Pilih Sistem</option>
								<option value="1">Adjust By Sistem</option>
								<option value="2">Denda Ke Karyawan</option>

							</select>
						</div>
					</div>
					<div class="row mb-2">
						<div class="col-sm-12 input-group">
							<div class="col-sm-3 p-0 input-group-prepend">
								<span class="col-sm-12 input-group-text">Keterangan Recount</span>
							</div>
							<input class="form-control" type="text" name="keterangan" id="keterangan" maxlength="50" placeholder="Keterangan recount" required readonly />

						</div>
					</div>
					<div class="row mb-2">
						<div class="col-sm-12 input-group">
							<div class="col-sm-3 p-0 input-group-prepend">
								<span class="col-sm-12 input-group-text">Keterangan Berita Acara</span>
							</div>
							<input class="form-control" type="text" name="keteranganbag" id="keteranganbag" maxlength="50" placeholder="Keterangan Berita Acata" required />

						</div>
					</div>
				</div>
				<div class="table-responsive">
					<table id="t_selisih" class="table table-bordered table-striped dt-responsive display nowrap" style="width:100%">
						<thead class="thead-dark">
							<tr>
								<th width="10">Nomor</th>
								<th width="150">Tanggal</th>
								<th width="150">Kode</th>
								<th width="150">Nama</th>
								<th width="150">Expired</th>
								<th width="150">Qty</th>
								<th width="150">QtySistem</th>
								<th width="150">Selisih</th>
								<th width="150">Keterangan</th>

							</tr>
						</thead>
						<tbody class="tbody-scroll scroller" id="tb_selisih"></tbody>

					</table>

				</div>
				<div class="table-responsive">
					<table id="t_min" class="table table-bordered table-striped dt-responsive display nowrap" style="width:100%">
						<thead class="thead-dark">
							<tr>
								<th width="10">Nomor</th>
								<th width="150">Tanggal</th>
								<th width="150">Kode</th>
								<th width="150">Nama</th>
								<th width="150">Expired</th>
								<th width="150">Qty</th>
								<th width="150">QtySistem</th>
								<th width="150">Selisih</th>
								<th width="150">Keterangan</th>

							</tr>
						</thead>
						<tbody class="tbody-scroll scroller" id="tb_min"></tbody>

					</table>

				</div>
				<div class="table-responsive">
					<table id="t_pass" class="table table-bordered table-striped dt-responsive display nowrap" style="width:100%">
						<thead class="thead-dark">
							<tr>
								<th width="10">Nomor</th>
								<th width="150">Tanggal</th>
								<th width="150">Kode</th>
								<th width="150">Nama</th>
								<th width="150">Expired</th>
								<th width="150">Qty</th>
								<th width="150">QtySistem</th>
								<th width="150">Selisih</th>
								<th width="150">Keterangan</th>

							</tr>
						</thead>
						<tbody class="tbody-scroll scroller" id="tb_pass"></tbody>

					</table>

				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-success" data-dismiss="modal" id="confirmdata">Save</button>
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="findbarang">
	<div class="modal-dialog modal-lg">
		<div class="modal-content" style="border-radius: 5px;">
			<div class="modal-header" style="padding: 10px; color: #000;">
				<h7 class="modal-title" style="margin-left: 5px;">Data Barang</h7>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="table-responsive">
					<table id="t_barang" class="table table-bordered table-striped dt-responsive display nowrap" style="width:100%">
						<thead class="thead-dark">
							<tr>
								<th width="10">Action</th>
								<th width="150">Kode</th>
								<th width="150">Nama</th>
								<th width="150">Qty</th>

							</tr>
						</thead>
						<tbody></tbody>

					</table>

				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="findrecount">
	<div class="modal-dialog modal-lg">
		<div class="modal-content" style="border-radius: 5px;">
			<div class="modal-header" style="padding: 10px; color: #000;">
				<h7 class="modal-title" style="margin-left: 5px;">Data Barang</h7>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="table-responsive">
					<table id="t_recount" class="table table-bordered table-striped dt-responsive display nowrap" style="width:100%">
						<thead class="thead-dark">
							<tr>
								<th width="10">Action</th>
								<th width="150">Nomor</th>
								<th width="150">Tanggal</th>
								<th width="150">Keterangan</th>
							</tr>
						</thead>
						<tbody></tbody>

					</table>

				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
