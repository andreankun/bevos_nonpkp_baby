<script>
	$(document).ready(function() {

		function ClearScreen() {
			$('#lokasicabang').val("");
			$('#lokasigdg').val("");
			$('#nosjgudang').prop('disabled', false).val("");
			$('#tglsjgudang').prop('disabled', false).val("<?= date('Y-m-d') ?>")
			$('#nodokumen').prop('disabled', false).val("");
			$('#keterangan').prop('disabled', false).val("");
			$('#keteranganbatal').val("");
			$('#jenisdokumen').prop('disabled', false).val("");
			$('#lokasigdg').prop('disabled', false).val("");
			$('#kodebarang').prop('disabled', false).val("");
			$('#kodegdg').val("");
			$('#qty').prop('disabled', false).val("");
			$('#tglpengiriman').prop('disabled', false).val("<?= date('Y-m-d') ?>");

			$('#caridokumen').show();

			$('#cancel').prop('disabled', true);
			$('#update').prop('disabled', true);
			$('#save').prop('disabled', false);
			$('#tb_detail').empty();
			$('#carikodecbg').show();
			$('#carikodegdg').show();
		}

		$('#tglpengiriman').datepicker({
			format: "yyyy-mm-dd",
			autoclose: true,
			todayHighlight: true,
			startDate: false
		});

		$('#jenisdokumen').change(function() {
			var jenisdokumen = $(this).val();

			if (jenisdokumen == '1') {
				$('#lokasicabang').val("");
				$('#lokasigdg').val("");
				$('#carikodecbg').show();
				$('#carikodegdg').show();
			} else if (jenisdokumen == '2') {
				$('#lokasicabang').val("");
				$('#lokasigdg').val("");
				$('#carikodecbg').show();
				$('#carikodegdg').show();
			} else {
				$('#lokasicabang').val("");
				$('#lokasigdg').val("");
				$('#carikodecbg').hide();
				$('#carikodegdg').hide();
			}
		})

		/* Cari Data Cabang*/
		document.getElementById("carikodecbg").addEventListener("click", function(event) {
			event.preventDefault();
			$('#t_cabang').DataTable({
				"destroy": true,
				"searching": true,
				"processing": true,
				"serverSide": true,
				"lengthChange": true,
				"pageLength": 5,
				"lengthMenu": [5, 10, 25, 50],
				"order": [],
				// "scrollX": true,
				"ajax": {
					"url": "<?= base_url('masterdata/Cabang/CariDataCabang'); ?>",
					"method": "POST",
					"data": {
						nmtb: "glbm_cabang",
						field: {
							kode: "kode",
							nama: "nama",
							alamat: "alamat"
						},
						sort: "kode",
						where: {
							kode: "kode",
							nama: "nama",
							alamat: "alamat"
						},
						value: "aktif = true"
					},
				}
			});
		});

		/*Get Data Cabang*/
		$(document).on('click', ".searchcabang", function() {
			var kode = $(this).attr("data-id");;
			$.ajax({
				url: "<?= base_url('masterdata/Cabang/DataCabang'); ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					kode: kode
				},
				success: function(data) {
					for (var i = 0; i < data.length; i++) {
						$('#lokasicabang').val(data[i].kode.trim());
					}
				}
			}, false);
		});

		/* Cari Data Gudang*/
		document.getElementById("carikodegdg").addEventListener("click", function(event) {
			event.preventDefault();
			var kodecabang = $('#lokasicabang').val();
			$('#t_gudang').DataTable({
				"destroy": true,
				"searching": true,
				"processing": true,
				"serverSide": true,
				"lengthChange": true,
				"pageLength": 5,
				"lengthMenu": [5, 10, 25, 50],
				"order": [],
				// "scrollX": true,
				"ajax": {
					"url": "<?= base_url('masterdata/Gudang/CariDataGudang'); ?>",
					"method": "POST",
					"data": {
						nmtb: "glbm_gudang",
						field: {
							kode: "kode",
							nama: "nama",
							alamat: "alamat",
							kodecabang: "kodecabang",
							status: "status"
						},
						sort: "kode",
						where: {
							kode: "kode",
							nama: "nama",
							alamat: "alamat",
							kodecabang: "kodecabang",
						},
						value: "aktif = true AND kodecabang = '" + kodecabang + "'"
					},
				}
			});
		});

		/*Get Data Gudang*/
		$(document).on('click', ".searchgudang", function() {
			var kode = $(this).attr("data-id");;
			$.ajax({
				url: "<?= base_url('masterdata/Gudang/DataGudang'); ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					kode: kode
				},
				success: function(data) {
					for (var i = 0; i < data.length; i++) {
						$('#lokasigdg').val(data[i].kode.trim());
					}
				}
			}, false);
		});

		function ValidasiSave() {
			var tanggal = $('#tanggalsjgudang').val();
			var nodokumen = $('#nodokumen').val();
			var keterangan = $('#keterangan').val();
			var jenisdokumen = $('#jenisdokumen').val();
			var tglpengiriman = $('#tglpengiriman').val();
			var lokasi = $('#lokasigdg').val();

			if (tanggal == "" || tanggal == 0) {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: "Tanggal Surat Jalan tidak boleh kosong",
					shiwCloseButton: true,
					width: 350,
				});
				$('#tanggalsjgudang').focus();
				var result = false
			} else if (nodokumen == "" || nodokumen == 0) {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: "Nomor Proses tidak boleh kosong",
					showCloseButton: true,
					width: 350
				});
				$('#noproses').focus();
				var result = false;
			} else if (tglpengiriman == "" || tglpengiriman == 0) {
				Swal.fire({
					title: "Informasi",
					icon: 'info',
					html: "Tanggal Pengiriman tidak boleh kosong",
					showCloseButton: true,
					width: 350
				});
				$('#tglpengiriman').focus();
				var result = false;
			}
			// else if (keterangan == "" || keterangan == 0) {
			// 	Swal.fire({
			// 		title: "Informasi",
			// 		icon: 'info',
			// 		html: "Keterangan tidak boleh kosong",
			// 		showCloseButton: true,
			// 		width: 350
			// 	});
			// 	$('#keterangan').focus();
			// 	var result = false;
			// }
			else if (jenisdokumen == "" || jenisdokumen == 0) {
				Swal.fire({
					title: "Informasi",
					icon: 'info',
					html: "Jenis Dokumen tidak boleh kosong",
					showCloseButton: true,
					width: 350
				});
				$('#jenisdokumen').focus();
				var result = false;
			} else if (lokasi == "" || lokasi == 0) {
				Swal.fire({
					title: "Informasi",
					icon: 'info',
					html: "Lokasi tidak boleh kosong",
					showCloseButton: true,
					width: 350
				});
				$('#lokasigdg').focus();
				var result = false;
			} 
			// else if (lokasi == $('#gudang').val()) {
			// 	Swal.fire({
			// 		title: "Informasi",
			// 		icon: 'info',
			// 		html: "Lokasi tidak boleh sama dengan lokasi sekarang",
			// 		showCloseButton: true,
			// 		width: 350
			// 	});
			// 	$('#lokasigdg').focus();
			// 	var result = false;
			// } 
			else {
				var result = true;
			}
			return result;
		}

		function ValidasiSaveDetail(kodebarang) {
			var table = document.getElementById('t_detail');
			var kodebarang = $('#kodebarang').val();
			var namabarang = $('#namabarang').val();
			var qty = $('#qty').val();
			if (kodebarang == "" || kodebarang == 0) {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: "Kode Barang tidak boleh kosong",
					showCloseButton: true,
					width: 350
				});
				$('#kodebarang').focus();
				var result = false
			} else if (namabarang == "" || namabarang == 0) {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: "Nama Barang tidak boleh kosong",
					showCloseButton: true,
					width: 350
				});
				$('#namabarang').focus();
				var result = false;
			} else if (qty == "" || qty == 0) {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: "Quantity tidak boleh kosong",
					showCloseButton: true,
					width: 350
				});
				$('#qty').focus();
				var result = false;
			} else {
				for (var r = 1, n = table.rows.length; r < n; r++) {
					var string = "";
					for (var c = 0, m = table.rows[r].cells.length; c < m; c++) {
						if (c == 0) {
							if (table.rows[r].cells[c].innerHTML == kodebarang) {
								Swal.fire({
									title: 'Information',
									icon: 'info',
									html: 'Data ini sudah diinput',
									showCloseButton: true,
									width: 350,
								});
								return "gagal";
							}
						}
					}
				}
			}
			return "sukses";
		}

		/* Declare Toast */
		const Toast = Swal.mixin({
			toast: true,
			position: 'top-end',
			showConfirmButton: false,
			timer: 3000,
			timerProgressBar: true,
			didOpen: (toast) => {
				toast.addEventListener('mouseenter', Swal.stopTimer)
				toast.addEventListener('mouseleave', Swal.resumeTimer)
			}
		});

		function FormatDate(input) {
			var date = new Date(input);
			var yyyy = date.getFullYear();
			var mm = date.getMonth() + 1;
			var dd = date.getDate();
			var hrs = date.getHours();
			var mnt = date.getMinutes();


			if (dd < 10) {
				dd = '0' + dd;
			}
			if (mm < 10) {
				mm = '0' + mm;
			}
			if (hrs < 10) {
				hrs = '0' + hrs;
			}
			if (mnt < 10) {
				mnt = '0' + mnt;
			}
			return yyyy + '-' + mm + '-' + dd;
		};

		/* Format Rupiah */
		function FormatRupiah(angka, prefix) {
			if (!angka) {
				return '';
			}
			var vangka = angka.toString();
			var number_string = vangka.replace(/[^.\d]/g, '').replace(/[^\w\s]/gi, '').toString(),
				split = number_string.split('.'),
				sisa = split[0].length % 3,
				rupiah = split[0].substr(0, sisa),
				ribuan = split[0].substr(sisa).match(/\d{3}/gi);

			// tambahkan titik jika yang di input sudah menjadi angka ribuan
			if (ribuan) {
				separator = sisa ? ',' : '';
				rupiah += separator + ribuan.join(',');
			}

			rupiah = split[1] != undefined ? rupiah + '.' + split[1] : rupiah;
			return prefix == undefined ? rupiah : (rupiah ? rupiah : '');
		};

		/* DeFormat Rupiah */
		function DeFormatRupiah(angka) {
			var result = angka.replace(/[^\w\s]/gi, '');

			return result;
		};

		/* Format Rupiah Saat Event Keyup */
		$('#qty').keyup(function() {
			$(this).val(FormatRupiah($(this).val()));
		});

		$('#chgqty').keyup(function() {
			$(this).val(FormatRupiah($(this).val()));
		});


		function AmbilDataDetail() {
			var table = document.getElementById('t_detail');
			var arr2 = [];
			for (var r = 1, n = table.rows.length; r < n; r++) {
				var string = "";
				for (var c = 0, m = table.rows[r].cells.length; c < m - 1; c++) {
					if (c == 0) {
						string = "{" + table.rows[0].cells[c].innerHTML + " : '" + table.rows[r].cells[c].innerHTML + "'";
					} else {
						string = string + ", " + table.rows[0].cells[c].innerHTML + " : '" + table.rows[r].cells[c].innerHTML + "'";
					}
				}
				string = string + "}";
				var obj = JSON.stringify(eval('(' + string + ')'));
				var arr = $.parseJSON(obj);
				arr2.push(arr);
			}
			return arr2;
		};

		$("#adddetail").click(function() {
			var kodebarang = $('#kodebarang').val();
			var namabarang = $('#namabarang').val();
			var qty = $('#qty').val();
			if (ValidasiSaveDetail(kodebarang) == "sukses") {
				var row = "";
				row =
					'<tr id="' + kodebarang + '">' +
					'<td nowrap>' +  + '</td>' +
					'<td nowrap>' + kodebarang + '</td>' +
					'<td nowrap>' + namabarang + '</td>' +
					'<td nowrap>' + FormatRupiah(qty) + '</td>' +
					'<td nowrap style="text-align: center; width: 200px;">' +
					// '<button data-table="' + kodebarang + '" data-toggle="modal" data-target="#changeqty" class="editdetail btn btn-sm btn-success m-1 p-1"><i class="fas fa-edit"></i> Edit</button>' +
					// '<button data-table="' + kodebarang + '" class="deldetail btn btn-sm btn-danger m-1 p-1"><i class="fa fa-trash mr-1"></i> Delete</button>' +
					'</td>' +
					'</tr>';
				// console.log(row);
				$('#tb_detail').append(row);
				$('#kodebarang').val("");
				$('#namabarang').val("");
				$('#qty').val("");

			}
		});

		$(document).on('click', '.editdetail', function() {
			var table = document.getElementById('t_detail');
			var tr = document.getElementById($(this).attr("data-table"));
			var td = tr.getElementsByTagName("td");
			for (var c = 0, m = table.rows[0].cells.length; c < m - 1; c++) {
				var chgth = table.rows[0].cells[c].innerHTML.replace(/ /g, '').replace(/\s/g, '').toLowerCase();
				var valuetd = td[c].innerHTML;
				$('#chg' + chgth).val(valuetd);
			}
		});

		$(document).on('click', '#changedetail', function() {
			var kodebarang = $('#chgkode').val();
			var namabarang = $('#chgnama').val();
			var qty = $('#chgqty').val();
			$('#' + kodebarang).remove();
			InsertDataDetail(kodebarang, namabarang, qty);
		});

		function InsertDataDetail(kodebarang, namabarang, qty) {
			var row = "";
			row =
				'<tr id="' + kodebarang + '">' +
				'<td nowrap>' +  + '</td>' +
				'<td nowrap>' + kodebarang + '</td>' +
				'<td nowrap>' + namabarang + '</td>' +
				'<td nowrap>' + FormatRupiah(qty) + '</td>' +
				'<td nowrap style="text-align: center; width: 200px;">' +
				// '<button data-table="' + kodebarang + '" data-toggle="modal" data-target="#changeqty" class="editdetail btn btn-success m-1 p-1"><i class="fas fa-edit"></i> Edit</button>' +
				// '<button data-table="' + kodebarang + '" class="deldetail btn btn-danger m-1 p-1"><i class="fa fa-trash mr-1"></i> Delete</button>' +
				'</td>' +
				'</tr>';
			$('#tb_detail').append(row);
		};

		$(document).on('click', '.deldetail', function() {
			var id = $(this).attr("data-table");
			$('#' + id).remove();
		});

		function GetDataDetail(nomor) {
			$.ajax({
				url: "<?= base_url('inventory/SuratJalanGudang/DataSuratJalanGudangDetail'); ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					nomor: nomor
				},
				success: function(data) {

					for (var i = 0; i < data.length; i++) {
						var kodebarang = data[i].kodebarang;
						var namabarang = data[i].namabarang.trim();
						var qty = data[i].qty.trim();
						var nomor = "";
						var action = "-";
						myFunction(nomor, kodebarang, namabarang, qty, action);
					}
				}
			});
		};

		var values = "";
		if ($('#grup').val() == 'GUDANG JKT') {
			values = "kodecbasal = '" + $('#cabang').val() + "' AND status = false AND batal = false AND kodestatus = 0 AND kodegdasal = '" + $('#gudang').val() + "'"
		} else {
			values = "kodecbasal = '" + $('#cabang').val() + "' AND status = false AND batal = false AND kodestatus = 0"
		}
		document.getElementById('caridokumen').addEventListener("click", function(event) {
			event.preventDefault();
			$('#tb_detail').empty();
			var jenis = $('#jenisdokumen').val();
			if (jenis == "1") {
				$('.repack').show();
				$('.proses').hide();
				$('.transfer').hide();
				$('#t_repack').DataTable({
					"destroy": true,
					"searching": true,
					"processing": true,
					"serverSide": true,
					"lengthChange": true,
					"pageLength": 5,
					"lengthMenu": [5, 10, 25, 50],
					"order": [],
					"ajax": {
						"url": "<?php echo base_url('inventory/ReqRepackStock/CariDataReq'); ?>",
						"method": "POST",
						"data": {
							nmtb: "srvt_reqrepackstock",
							field: {
								nomor: "nomor",
								tanggal: "tanggal",
								keterangan: "keterangan"
							},
							sort: "nomor",
							where: {
								nomor: "nomor"
							},
							value: "terima = false AND status = 0 AND proses = false AND kodecabang = '" + $('#cabang').val() + "' "
							// value: "terima = false AND status = 0 AND proses = false "
						},
					},
					"columnDefs": [{
						"targets": 2,
						"render": $.fn.dataTable.render.moment('YYYY-MM-DD HH:mm:ss', 'D MMM YY')
					}]
				});
			} else if (jenis == "2") {
				$('.repack').hide();
				$('.proses').show();
				$('.transfer').hide();
				$('#t_proses').DataTable({
					"destroy": true,
					"searching": true,
					"processing": true,
					"serverSide": true,
					"lengthChange": true,
					"pageLength": 5,
					"lengthMenu": [5, 10, 25, 50],
					"order": [],
					"ajax": {
						"url": "<?php echo base_url('inventory/ProsesRepackStock/CariDataProses'); ?>",
						"method": "POST",
						"data": {
							nmtb: "srvt_prosesrepact",
							field: {
								nomor: "nomor",
								tanggal: "tanggal"
							},
							sort: "nomor",
							where: {
								nomor: "nomor"
							},
							value: "kirim = false AND batal = false AND kodecabang = '" + $('#cabang').val() + "' "
							// value: "kirim = false AND batal = false "
						},
					},
					"columnDefs": [{
						"targets": 2,
						"render": $.fn.dataTable.render.moment('YYYY-MM-DD HH:mm:ss', 'D MMM YY')
					}]
				});
			} else if (jenis == "3") {
				$('.repack').hide();
				$('.proses').hide();
				$('.transfer').show();
				$('#t_transfer').DataTable({
					"destroy": true,
					"searching": true,
					"processing": true,
					"serverSide": true,
					"lengthChange": true,
					"pageLength": 5,
					"lengthMenu": [5, 10, 25, 50],
					"order": [],
					"ajax": {
						"url": "<?php echo base_url('inventory/SuratJalanGudang/CariDataTransferStok'); ?>",
						"method": "POST",
						"data": {
							nmtb: "srvt_transferstock",
							field: {
								nomor: "nomor",
								tanggal: "tanggal",
								keterangan: "keterangan"
							},
							sort: "nomor",
							where: {
								nomor: "nomor"
							},
							value: values
							// value: "kodecabang = '" + $('#cabang').val() + "' AND nomor Not In (SELECT noproses FROM srvt_suratjalangudang)"
							// value: "nomor Not In (SELECT noproses FROM srvt_suratjalangudang) AND kodecbasal = '" + $('#cabang').val() + "' AND status = false AND batal = false"
						},
					},
					"columnDefs": [{
						"targets": 3,
						"render": $.fn.dataTable.render.moment('YYYY-MM-DD HH:mm:ss', 'D MMM YY')
					}]
				});
			} else {
				$('.repack').hide();
				$('.proses').hide();
				$('.transfer').hide();
			}
		});

		$(document).on("click", ".searchts", function() {
			var nomor = $(this).attr("data-id");
			$.ajax({
				url: "<?= base_url('inventory/SuratJalanGudang/DataTransferStok') ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					nomor: nomor
				},
				success: function(data) {
					for (var i = 0; i < data.length; i++) {
						$('#nodokumen').val(data[i].nomor.trim());
						$('#lokasicabang').val(data[i].kodecbtujuan.trim()).prop('disabled', true);
						$('#lokasigdg').val(data[i].kodegdtujuan.trim()).prop('disabled', true);
						$('#keterangan').val(data[i].keterangan.trim()).prop('disabled', true);
					}
					$('#carikodecbg').hide();
					$('#carikodegdg').hide();

					GetDataTransferDetail(nomor);
				}
			});
		});

		function myFunction(nomor, kodebarang, namabarang, qty, action) {
			var table = document.getElementById('t_detail').getElementsByTagName('tbody')[0];
			var row = table.insertRow(0);
			var kodepencarian = kodebarang.replace(/[^a-zA-Z0-9 ]/g, '');
			row.setAttribute("id", kodepencarian);
			// console.log(row)
			var cell1 = row.insertCell(0);
			var cell2 = row.insertCell(1);
			var cell3 = row.insertCell(2);
			var cell4 = row.insertCell(3);
			var cell5 = row.insertCell(4);
			// var cell3 = '<button data-table="' + RejectSpecialChar(kodebarang) + '" class="deldetail btn btn-danger m-1 p-1"><i class="fa fa-trash mr-1"></i> Delete</button>';
			cell1.innerHTML = nomor;
			cell2.innerHTML = kodebarang;
			cell3.innerHTML = namabarang;
			cell4.innerHTML = FormatRupiah(qty);
			cell5.innerHTML = action;
			// cell1.style.display = 'none';
			// row.innerHTML = kodebarang;

			var tbody = $('#t_detail').find('tbody');
			var newRows = tbody.find('tr').sort(function(a, b) {
				return $('td:eq(2)', a).text().localeCompare($('td:eq(2)', b).text());
			});
			console.log(newRows);
			tbody.append(newRows);
		}

		function GetDataTransferDetail(nomor) {
			$.ajax({
				url: "<?= base_url('inventory/SuratJalanGudang/DataTransferStokDetail'); ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					nomor: nomor
				},
				success: function(data) {

					for (var i = 0; i < data.length; i++) {
						var kodebarang = data[i].kodebarang;
						var namabarang = data[i].namabarang.trim();
						var qty = data[i].qty.trim();
						var nomor = "";
						var action = "-";
						myFunction(nomor, kodebarang, namabarang, qty, action);
					}
				}
			});
		};

		$(document).on("click", ".searchproses", function() {
			var nomor = $(this).attr("data-id");
			$.ajax({
				url: "<?= base_url('inventory/ProsesRepackStock/DataProses') ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					nomor: nomor
				},
				success: function(data) {
					for (var i = 0; i < data.length; i++) {
						$('#nodokumen').val(data[i].nomor.trim());
					}
					GetDataProsesDetail(nomor);
				}
			});
		});

		function GetDataProsesDetail(nomor) {
			$.ajax({
				url: "<?= base_url('inventory/SuratJalanGudang/DataProsesDetail'); ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					nomor: nomor
				},
				success: function(data) {

					for (var i = 0; i < data.length; i++) {
						var kodebarang = data[i].kodebarang;
						var namabarang = data[i].namabarang.trim();
						var qty = data[i].qty.trim();
						InsertDataDetail(kodebarang, namabarang, qty);
					}
				}
			});
		};

		$(document).on("click", ".searchreq", function() {
			var nomor = $(this).attr("data-id");
			$.ajax({
				url: "<?= base_url('inventory/ReqRepackStock/DataReq') ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					nomor: nomor
				},
				success: function(data) {
					for (var i = 0; i < data.length; i++) {
						$('#nodokumen').val(data[i].nomor.trim());
					}
					GetDataReqDetail(nomor);
				}
			});
		});

		function GetDataReqDetail(nomor) {
			$.ajax({
				url: "<?= base_url('inventory/ReqRepackStock/DataReqDetail'); ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					nomor: nomor
				},
				success: function(data) {

					for (var i = 0; i < data.length; i++) {
						var kodebarang = data[i].kodebarang;
						var namabarang = data[i].namabarang.trim();
						var qty = data[i].qty.trim();
						InsertDataDetail(kodebarang, namabarang, qty);
					}
				}
			});
		};

		// document.getElementById('caribarang').addEventListener("click", function(event) {
		// 	event.preventDefault();
		// 	$('#t_barang').DataTable({
		// 		"destroy": true,
		// 		"searching": true,
		// 		"processing": true,
		// 		"serverSide": true,
		// 		"lengthChange": false,
		// 		"order": [],
		// 		"ajax": {
		// 			"url": "<?php echo base_url('masterdata/Barang/CariDataBarang'); ?>",
		// 			"method": "POST",
		// 			"data": {
		// 				nmtb: "glbm_barang",
		// 				field: {
		// 					kode: "kode",
		// 					nama: "nama",
		// 				},
		// 				sort: "kode",
		// 				where: {
		// 					kode: "kode"
		// 				},
		// 				value: "aktif = true AND kodecabang = '" + $('#kodegudang').val() + "'"
		// 			},
		// 		}
		// 	});
		// });

		// $(document).on('click', ".searchbarang", function() {
		// 	var kode = $(this).attr("data-id");
		// 	$.ajax({
		// 		url: "<?= base_url('masterdata/Barang/DataBarang') ?>",
		// 		method: "POST",
		// 		dataType: "json",
		// 		async: false,
		// 		data: {
		// 			kode: kode
		// 		},
		// 		success: function(data) {
		// 			for (var i = 0; i < data.length; i++) {
		// 				$('#kodebarang').val(data[i].kode.trim());
		// 				$('#namabarang').val(data[i].nama.trim());
		// 			}
		// 		}
		// 	})
		// });
		var valuesfind = "";
		if ($('#grup').val() == 'GUDANG JKT') {
			valuesfind = "kodecabang = '" + $('#cabang').val() + "' AND batal = false AND asal = '" + $('#gudang').val() + "'"
		} else {
			valuesfind = "kodecabang = '" + $('#cabang').val() + "' AND batal = false"
		}
		document.getElementById('find').addEventListener("click", function(event) {
			event.preventDefault();
			$('#tb_detail').empty();
			$('#t_find').DataTable({
				"language": {
					"url": "<?= base_url() ?>assets/json/id.json"
				},
				"destroy": true,
				"searching": true,
				"processing": true,
				"serverSide": true,
				"lengthChange": true,
				"pageLength": 5,
				"lengthMenu": [5, 10, 25, 50],
				"order": [],
				"ajax": {
					"url": "<?php echo base_url('inventory/SuratJalanGudang/CariDataSuratGudang'); ?>",
					"method": "POST",
					"data": {
						nmtb: "srvt_suratjalangudang",
						field: {
							nomor: "nomor",
							tanggal: "tanggal",
							noproses: "noproses",
							keterangan: "keterangan",
							terima: "terima",
							jenisdokumen: "jenisdokumen",
						},
						sort: "nomor",
						where: {
							nomor: "nomor",
							noproses: "noproses",
						},
						value: valuesfind
						// value: "kodecabang = '" + $('#cabang').val() + "'"
					},
				},
				"columnDefs": [{
						"targets": 3,
						"render": $.fn.dataTable.render.moment('YYYY-MM-DD HH:mm:ss', 'D MMM YY')
					},
					{
						"targets": 6,
						"data": "terima",
						"render": function(data, type, row, meta) {
							return (row[6] == 't') ? "<b>Sudah Diterima</b>" : "<p>Belum Diterima</p>"
						}
					},
					{
						"targets": 7,
						"data": "jenisdokumen",
						"render": function(data, type, row, meta) {
							return (row[7] == 1) ? "Req Repack" : (row[7] == 2) ? "Repack Result" : "Transfer Stock"
						}
					}
				]
			});
		});

		$(document).on('click', ".searchsjgudang", function() {
			var nomor = $(this).attr("data-id");
			$.ajax({
				url: "<?= base_url('inventory/SuratJalanGudang/DataSuratJalanGudang') ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					nomor: nomor
				},
				success: function(data) {
					for (var i = 0; i < data.length; i++) {
						$('#nosjgudang').val(data[i].nomor.trim());
						$('#tanggalsjgudang').val(FormatDate(data[i].tanggal.trim()));
						$('#nodokumen').val(data[i].noproses.trim());
						$('#tglpengiriman').val(FormatDate(data[i].tglpengiriman.trim()));
						$('#jenisdokumen').val(data[i].jenisdokumen.trim());
						$('#lokasigdg').val(data[i].lokasi.trim());
						$('#keterangan').val(data[i].keterangan.trim());
					}
					GetDataDetail(nomor);
					$('#save').prop('disabled', true);
					$('#jenisdokumen').prop('disabled', true);
					$('#lokasigdg').prop('disabled', true);
					$('#update').prop('disabled', false);
					$('#cancel').prop('disabled', false);
					$('#caridokumen').hide();
				}
			});
		});

		document.getElementById('save').addEventListener("click", function(event) {
			event.preventDefault();
			var nomor = $('#nosjgudang').val();
			var tanggal = $('#tglsjgudang').val();
			var nodokumen = $('#nodokumen').val();
			var tglpengiriman = $('#tglpengiriman').val();
			var keterangan = $('#keterangan').val();
			var jenisdokumen = $('#jenisdokumen').val();
			var lokasi = $('#lokasigdg').val();
			var cabang = $('#lokasicabang').val();
			var datadetail = AmbilDataDetail();
			if (ValidasiSave(datadetail)) {
				$.ajax({
					url: "<?= base_url('inventory/SuratJalanGudang/Save') ?>",
					method: "POST",
					dataType: "json",
					async: true,
					data: {
						nomor: nomor,
						tanggal: tanggal,
						nodokumen: nodokumen,
						tglpengiriman: tglpengiriman,
						keterangan: keterangan,
						jenisdokumen: jenisdokumen,
						lokasi: lokasi,
						cabang: cabang,
						datadetail: datadetail
					},
					success: function(data) {
						// console.log(data);
						if (data.nomor != "") {
							Toast.fire({
								icon: 'success',
								title: data.message
							});
							Cetak(data.nomor);
							$('#nosjgudang').val(data.nomor);
							$('#tanggalsjgudang').prop('disabled', true);
							$('#nodokumen').prop('disabled', true);
							$('#tglpengiriman').prop('disabled', true);
							$('#keterangan').prop('disabled', true);
							$('#jenisdokumen').prop('disabled', true);
							$('#lokasigdg').prop('disabled', true);
							$('#kodebarang').prop('disabled', true);
							$('#qty').prop('disabled', true);

							$('#update').prop('disabled', true);
							$('#save').prop('disabled', true);
							$('#cariproses').hide();
							$('#caribarang').hide();
						} else {
							Toast.fire({
								icon: 'error',
								title: data.message
							});
						}
					}
				}, false);
			}
		});

		function Cetak(nomor) {
			// console.log(nomor);
			window.open("<?= base_url() ?>cetak_pdf/SuratJalanGudang/Print/" + nomor);
		}

		document.getElementById('update').addEventListener("click", function(event) {
			event.preventDefault();
			var nomor = $('#nosjgudang').val();
			var tanggal = $('#tglsjgudang').val();
			var nodokumen = $('#nodokumen').val();
			var keterangan = $('#keterangan').val();
			var jenisdokumen = $('#jenisdokumen').val();
			var lokasi = $('#lokasigdg').val();
			var cabang = $('#lokasicabang').val();
			var tglpengiriman = $('#tglpengiriman').val();
			var datadetail = AmbilDataDetail();
			if (ValidasiSave(datadetail) == true) {
				$.ajax({
					url: "<?= base_url('inventory/SuratJalanGudang/Update') ?>",
					method: "POST",
					dataType: "json",
					async: true,
					data: {
						nomor: nomor,
						tanggal: tanggal,
						nodokumen: nodokumen,
						keterangan: keterangan,
						jenisdokumen: jenisdokumen,
						tglpengiriman: tglpengiriman,
						lokasi: lokasi,
						cabang: cabang,
						datadetail: datadetail
					},
					success: function(data) {
						if (data.nomor != "") {
							Toast.fire({
								icon: 'success',
								title: data.message
							});
							$('#nosjgudang').prop('disabled', true);
							$('#tanggalsjgudang').prop('disabled', true);
							$('#noproses').prop('disabled', true);
							$('#keterangan').prop('disabled', true);
							$('#jenisdokumen').prop('disabled', true);
							$('#lokasigdg').prop('disabled', true);
							$('#kodebarang').prop('disabled', true);
							$('#qty').prop('disabled', true);
						} else {
							Toast.fire({
								icon: 'error',
								title: data.message
							});
						}
					}
				}, false);
			}
		});

		document.getElementById('okcancel').addEventListener("click", function(event) {
			event.preventDefault();
			var nomor = $('#nosjgudang').val();
			var nodokumen = $('#nodokumen').val();
			var keterangan = $('#keteranganbatal').val();
			var datadetail = AmbilDataDetail();
			if (nomor == "") {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Pilih Dulu Nomor Yang Ingin dibatalkan',
					showCloseButton: true,
					width: 350,
				});
			} else if (keterangan == "") {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Keterangan Batal Tidak Boleh Kosong',
					showCloseButton: true,
					width: 350,
				});
			} else {
				$.ajax({
					url: "<?= base_url("inventory/SuratJalanGudang/Cancel") ?>",
					method: "POST",
					dataType: "json",
					async: true,
					data: {
						nomor: nomor,
						nodokumen: nodokumen,
						keterangan: keterangan,
						datadetail: datadetail
					},
					success: function(data) {
						console.log(data);
						if (data.nomor != "") {
							Toast.fire({
								icon: 'success',
								title: data.message
							});
							// RefreshLayar();
						} else {
							Toast.fire({
								icon: 'error',
								title: data.message
							});
						}
					}
				}, false)
			}
		})

		document.getElementById('clear').addEventListener("click", function(event) {
			event.preventDefault();
			ClearScreen();
		});

		ClearScreen();

		$(document).on("click", ".cetaktransferstok", function() {
			var nomor = $(this).attr("data-id");
			CetakTransferStok(nomor);
			CetakTransferStokUpdate(nomor);
		});

		function CetakTransferStok(nomor) {
			window.open("<?= base_url() ?>cetak_pdf/TransferStok/Print/" + nomor);
		}

		function CetakTransferStokUpdate(nomor) {
            $.ajax({
                url: "<?= base_url("inventory/TransferStok/CetakTransferStokUpdate") ?>",
                method: "POST",
                dataType: "json",
                async: true,
                data: {
                    nomor: nomor
                },
                success: function(data) {
                    // if (data.nomor != "") {
                    //     Toast.fire({
                    //         icon: 'success',
                    //         title: data.message
                    //     });
                    // } else {
                    //     Toast.fire({
                    //         icon: 'error',
                    //         title: data.message
                    //     });
                    // }
                }
            }, false)
        }

		$(document).on('click', ".cetaksuratjalangudang", function() {
			var nomor = $(this).attr("data-id");
			Cetak(nomor);
			CetakSuratJalanGudangUpdate(nomor);
		});

		function Cetak(nomor) {
			window.open("<?= base_url() ?>cetak_pdf/SuratJalanGudang/Print/" + nomor);
		}

		function CetakSuratJalanGudangUpdate(nomor) {
			$.ajax({
				url: "<?= base_url("inventory/SuratJalanGudang/CetakSuratJalanGudangUpdate") ?>",
				method: "POST",
				dataType: "json",
				async: true,
				data: {
					nomor: nomor
				},
				success: function(data) {
					// if (data.nomor != "") {
					//     Toast.fire({
					//         icon: 'success',
					//         title: data.message
					//     });
					// } else {
					//     Toast.fire({
					//         icon: 'error',
					//         title: data.message
					//     });
					// }
				}
			}, false)
		}

	});
</script>