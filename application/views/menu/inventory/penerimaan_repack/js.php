<script text="text/javascript">
    $(document).ready(function() {

        function ClearScreen() {
            $('#nopenerimaan').val("");
            $('#tanggalpenerimaan').val("<?= date('Y-m-d') ?>");
            $('#nosjgudang').val("");
            $('#jenisdokumen').val("").prop('disabled', true);
            $('#keteranganbatal').val("");

            $('#carisjgudang').show();

            $('#tb_detail').empty();

            $('#cancel').prop("disabled", true);
            $('#update').prop("disabled", true);
            $('#save').prop("disabled", false);
        }

        function setDateFormat(date) {
            dateObj = new Date(date);
            var day = DateObj.getDate();
            var month = dateObj.getMonth() + 1;
            var fullYear = DateObj.getFullYear().toString();
            var setformattedDate = '';
            setformattedDate = fullYear + '-' + getDigitToFormat(month) + '-' + getDigitToFormat(day);
            return setformattedDate;
        }

        function FormatDate(input) {
            var date = new Date(input);
            var yyyy = date.getFullYear();
            var mm = date.getMonth() + 1;
            var dd = date.getDate();
            var hrs = date.getHours();
            var mnt = date.getMinutes();


            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }
            if (hrs < 10) {
                hrs = '0' + hrs;
            }
            if (mnt < 10) {
                mnt = '0' + mnt;
            }
            return yyyy + '-' + mm + '-' + dd;
        };

        function getDigitToFormat(val) {
            if (val < 10) {
                val = '0' + val;
            }
            return val.toString();
        };

        /* Format Rupiah */
        function FormatRupiah(angka, prefix) {
            if (!angka) {
                return '';
            }
            var vangka = angka.toString();
            var number_string = vangka.replace(/[^.\d]/g, '').replace(/[^\w\s]/gi, '').toString(),
                split = number_string.split('.'),
                sisa = split[0].length % 3,
                rupiah = split[0].substr(0, sisa),
                ribuan = split[0].substr(sisa).match(/\d{3}/gi);

            // tambahkan titik jika yang di input sudah menjadi angka ribuan
            if (ribuan) {
                separator = sisa ? ',' : '';
                rupiah += separator + ribuan.join(',');
            }

            rupiah = split[1] != undefined ? rupiah + '.' + split[1] : rupiah;
            return prefix == undefined ? rupiah : (rupiah ? rupiah : '');
        };

        /* DeFormat Rupiah */
        function DeFormatRupiah(angka) {
            var result = angka.replace(/[^\w\s]/gi, '');

            return result;
        };

        const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000,
            timerProgressBar: true,
            didOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer);
                toast.addEventListener('mouseleave', Swal.stopTimer);
            }
        });

        function InsertDataDetail(kodebarang, namabarang, qty) {

            var row = "";
            row =
                '<tr id="' + kodebarang + '">' +
                '<td nowrap>' + kodebarang + '</td>' +
                '<td nowrap>' + namabarang + '</td>' +
                '<td nowrap>' + FormatRupiah(qty) + '</td>' +
                '</tr>';
            $('#tb_detail').append(row);
        };

        function AmbilDataDetail() {
            var table = document.getElementById('t_detail');
            var arr2 = [];
            for (var r = 1, n = table.rows.length; r < n; r++) {
                var string = "";
                for (var c = 0, m = table.rows[r].cells.length; c < m; c++) {
                    if (c == 0) {
                        string = "{" + table.rows[0].cells[c].innerHTML + " : '" + table.rows[r].cells[c].innerHTML + "'";
                    } else {
                        string = string + ", " + table.rows[0].cells[c].innerHTML + " : '" + table.rows[r].cells[c].innerHTML + "'";
                    }
                }
                string = string + "}";
                var obj = JSON.stringify(eval('(' + string + ')'));
                var arr = $.parseJSON(obj);
                arr2.push(arr);
            }
            return arr2;
        };

        function ValidasiSave(datadetail) {
            var tanggal = $('#tanggalpenerimaan').val();
            var nosjgudang = $('#nosjgudang').val();
            var jenisdokumen = $('#jenisdokumen').val();

            if (tanggal == '' || tanggal == 0) {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'icon',
                    html: 'Tanggal Tidak Boleh Kosong',
                    showCloseButton: true,
                    width: 350,
                })
                $('#tanggal').focus();
                var result = false;
            } else if (nosjgudang == '' || nosjgudang == 0) {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'icon',
                    html: 'Nomor Repack Tidak Boleh Kosong',
                    showCloseButton: true,
                    width: 350,
                })
                $('#nosjgudang').focus();
                var result = false;
            } else if (jenisdokumen == '' || jenisdokumen == 0) {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'icon',
                    html: 'Jenis Dokumen Tidak Boleh Kosong',
                    showCloseButton: true,
                    width: 350,
                })
                $('#jenisdokumen').focus();
                var result = false;
            } else if (datadetail.length == 0 || datadetail.length == '0') {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Data Detail tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                var result = false;
            } else {
                var result = true;
            }
            return result;
        }

        document.getElementById("carisjgudang").addEventListener("click", function(event) {
            event.preventDefault();
            $('#tb_detail').empty();
            $('#t_sjgudang').DataTable({
                "destroy": true,
                "searching": true,
                "processing": true,
                "serverSide": true,
                "lengthChange": true,
                "pageLength": 5,
                "lengthMenu": [5, 10, 25, 50],
                "order": [],
                "ajax": {
                    "url": "<?= base_url('inventory/PenerimaanRepack/CariDataSJGudang'); ?>",
                    "method": "POST",
                    "data": {
                        nmtb: "srvt_suratjalangudang",
                        field: {
                            nomor: "nomor",
                            tanggal: "tanggal"
                        },
                        sort: "nomor",
                        where: {
                            nomor: "nomor"
                        },
                        value: "terima = false"
                    },
                },
                "columnDefs": [{
                    "targets": 2,
                    "render": $.fn.dataTable.render.moment('YYYY-MM-DD HH:mm:ss', 'D MMM YY')
                }]
            });
        })

        $(document).on('click', ".searchsjgd", function() {
            var nomor = $(this).attr("data-id");
            GetDataSJGudang(nomor);
        });

        function GetDataSJGudang(nomor) {
            $.ajax({
                url: "<?= base_url('inventory/PenerimaanRepack/DataSJGudang'); ?>",
                method: "POST",
                dataType: "json",
                async: false,
                data: {
                    nomor: nomor
                },
                success: function(data) {
                    console.log(data);
                    for (var i = 0; i < data.length; i++) {
                        $('#nosjgudang').val(data[i].nomor.trim());
                        $('#jenisdokumen').val(data[i].jenisdokumen.trim());
                    }
                    GetDataSJGudangDetail(nomor);
                }
            });
        };

        function GetDataSJGudangDetail(nomor) {
            $.ajax({
                url: "<?= base_url('inventory/PenerimaanRepack/DataSJGudangDetail'); ?>",
                method: "POST",
                dataType: "json",
                async: false,
                data: {
                    nomor: nomor
                },
                success: function(data) {
                    console.log(data);
                    for (var i = 0; i < data.length; i++) {
                        // var kode = data[i].kode;
                        var kodebarang = data[i].kodebarang.trim();
                        var namabarang = data[i].namabarang.trim();
                        var qty = data[i].qty.trim();

                        InsertDataDetail(kodebarang, namabarang, qty);
                    }
                }
            });
        };

        /* Save */
        document.getElementById('save').addEventListener("click", function(event) {
            event.preventDefault();
            var nomor = $('#nopenerimaan').val();
            var tanggal = $('#tanggalpenerimaan').val();
            var nosjgudang = $('#nosjgudang').val();
            var jenisdokumen = $('#jenisdokumen').val();

            var datadetail = AmbilDataDetail();

            if (ValidasiSave(datadetail) == true) {
                $.ajax({
                    url: "<?= base_url("inventory/PenerimaanRepack/Save") ?>",
                    method: "POST",
                    dataType: "json",
                    async: true,
                    data: {
                        nomor: nomor,
                        tanggal: tanggal,
                        nosjgudang: nosjgudang,
                        jenisdokumen: jenisdokumen,
                        datadetail: datadetail
                    },
                    success: function(data) {
                        console.log(data);
                        if (data.nomor != "") {
                            Toast.fire({
                                icon: 'success',
                                title: data.message
                            });
                            $('#nopenerimaan').val(data.nomor);
                            $('#tanggalpenerimaan').prop("disabled", true);
                            $('#nosjgudang').prop("disabled", true);

                            $('#carisjgudang').hide();

                            $('#update').prop("disabled", true);
                            $('#save').prop("disabled", true);
                        } else {
                            Toast.fire({
                                icon: 'error',
                                title: data.message
                            });
                        }
                    }
                }, false)
            }
        })

        document.getElementById("find").addEventListener("click", function(event) {
            event.preventDefault();
            $('#tb_detail').empty();
            $('#t_prs').DataTable({
                "destroy": true,
                "searching": true,
                "processing": true,
                "serverSide": true,
                "lengthChange": true,
                "pageLength": 5,
                "lengthMenu": [5, 10, 25, 50],
                "order": [],
                "ajax": {
                    "url": "<?= base_url('inventory/PenerimaanRepack/CariDataPenerimaan'); ?>",
                    "method": "POST",
                    "data": {
                        nmtb: "srvt_penerimaanrepack",
                        field: {
                            nomor: "nomor",
                            tanggal: "tanggal",
                            nosjgudang: "nosjgudang",
                            jenis: "jenis",
                        },
                        sort: "nomor",
                        where: {
                            nomor: "nomor",
                            nosjgudang: "nosjgudang",
                        },
                        values: "batal = false AND proses = false AND kodecabang = '" + $("#cabang").val() + "'"
                    },
                },
                "columnDefs": [{
                        "targets": 2,
                        "render": $.fn.dataTable.render.moment('YYYY-MM-DD HH:mm:ss', 'D MMM YY')
                    },
                    {
                        "targets": 4,
                        "data": "jenis",
                        "render": function(data, type, row, meta) {
                            return (row['4'] == '1') ? 'Penerimaan Request Repack' : 'Penerimaan Result Repack'
                        }
                    }
                ]
            });
        })

        $(document).on('click', ".searchpenerimaan", function() {
            var nomor = $(this).attr("data-id");
            GetData(nomor);
        });

        function GetData(nomor) {
            $.ajax({
                url: "<?= base_url('inventory/PenerimaanRepack/DataPenerimaan'); ?>",
                method: "POST",
                dataType: "json",
                async: false,
                data: {
                    nomor: nomor
                },
                success: function(data) {
                    console.log(data);
                    for (var i = 0; i < data.length; i++) {
                        $('#nopenerimaan').val(data[i].nomor.trim());
                        $('#tanggalpenerimaan').val(FormatDate(data[i].tanggal.trim()));
                        $('#nosjgudang').val(data[i].nosjgudang.trim());
                        $('#jenisdokumen').val(data[i].jenis.trim());
                    }
                    GetDataDetail(nomor);
                    $('#cancel').prop('disabled', false);
                    // $('#update').prop('disabled', false);
                    $('#save').prop('disabled', true);
                }
            });
        };

        function GetDataDetail(nomor) {
            $.ajax({
                url: "<?= base_url('inventory/PenerimaanRepack/DataPenerimaanDetail'); ?>",
                method: "POST",
                dataType: "json",
                async: false,
                data: {
                    nomor: nomor
                },
                success: function(data) {
                    console.log(data);
                    for (var i = 0; i < data.length; i++) {
                        // var kode = data[i].kode;
                        var kodebarang = data[i].kodebarang.trim();
                        var namabarang = data[i].namabarang.trim();
                        var qty = data[i].qty.trim();

                        InsertDataDetail(kodebarang, namabarang, qty);
                    }
                }
            });
        };

        /* Update */
        document.getElementById('update').addEventListener("click", function(event) {
            event.preventDefault();
            var nomor = $('#nopenerimaan').val();
            var tanggal = $('#tanggalpenerimaan').val();
            var nosjgudang = $('#nosjgudang').val();
            var jenisdokumen = $('#jenisdokumen').val();

            var datadetail = AmbilDataDetail();

            if (ValidasiSave(datadetail) == true) {
                $.ajax({
                    url: "<?= base_url("inventory/PenerimaanRepack/Update") ?>",
                    method: "POST",
                    dataType: "json",
                    async: true,
                    data: {
                        nomor: nomor,
                        tanggal: tanggal,
                        nosjgudang: nosjgudang,
                        jenisdokumen: jenisdokumen,
                        datadetail: datadetail
                    },
                    success: function(data) {
                        if (data.nomor != "") {
                            Toast.fire({
                                icon: 'success',
                                title: data.message
                            });
                            $('#nopenerimaan').val(data.nomor);
                            $('#tanggalpenerimaan').prop("disabled", true);
                            $('#nosjgudang').prop("disabled", true);

                            $('#carisjgudang').hide();

                            $('#update').prop("disabled", true);
                            $('#save').prop("disabled", true);
                        } else {
                            Toast.fire({
                                icon: 'error',
                                title: data.message
                            });
                        }
                    }
                }, false)
            }
        })

        document.getElementById('okcancel').addEventListener("click", function(event) {
            event.preventDefault();
            var nomor = $('#nopenerimaan').val();
            var keterangan = $('#keteranganbatal').val();
            var datadetail = AmbilDataDetail();
            if (nomor == "") {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Pilih Dulu Nomor Yang Ingin dibatalkan',
                    showCloseButton: true,
                    width: 350,
                });
            } else if (keterangan == "") {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Keterangan Batal Tidak Boleh Kosong',
                    showCloseButton: true,
                    width: 350,
                });
            } else {
                $.ajax({
                    url: "<?= base_url("inventory/PenerimaanRepack/Cancel") ?>",
                    method: "POST",
                    dataType: "json",
                    async: true,
                    data: {
                        nomor: nomor,
                        keterangan: keterangan,
                        datadetail: datadetail
                    },
                    success: function(data) {
                        console.log(data);
                        if (data.nomor != "") {
                            Toast.fire({
                                icon: 'success',
                                title: data.message
                            });
                            // RefreshLayar();
                        } else {
                            Toast.fire({
                                icon: 'error',
                                title: data.message
                            });
                        }
                    }
                }, false)
            }
        })

        document.getElementById("clear").addEventListener("click", function(event) {
            event.preventDefault();
            ClearScreen();
        })

        ClearScreen();
    })
</script>