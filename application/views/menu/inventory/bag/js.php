<script type="text/javascript">
	$(document).ready(function() {
		function LoadGudang() {
			$.ajax({
				url: "<?= base_url('inventory/Bag/LoadGudang') ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					cabang: $('#cabang').val()
				},
				success: function(data) {
					for (var i = 0; i < data.length; i++) {
						$("#lokasigdg").append('<option value = ' + data[i].kode + ' >' + data[i].nama + '</option>');
					}
				}
			}, false);
		}

		function ClearScreen() {
			$('#lokasigdg').empty();
			$("#lokasigdg").append('<option value = "" >Pilih Gudang</option>');
			LoadGudang();
			$('#lokasigdg').val($('#gudang').val());
			$('#update').prop("disabled", true);
			$('#save').prop("disabled", false);
			$('#approve').prop("disabled", true);
			$('#cancel').prop('disabled', true);

			$('#nobag').val("");
			$('#tanggalbag').val("<?= date('Y-m-d') ?>");
			$('#jenis').val("").prop('disabled', false);
			$('#lokasigdg').val("").prop('disabled', false);
			$('#keterangan').val("").prop('disabled', false);;
			$('#keteranganbatal').val("").prop('disabled', false);;
			$('#kodebarang').val("");
			$('#namabarang').val();
			$('#qty').val(0).prop('disabled', false);

			$('#caribarang').show();
			$('#tb_detail').empty();
		}

		/*Format Date*/
		function FormatDate(input) {
			var date = new Date(input);
			var yyyy = date.getFullYear();
			var mm = date.getMonth() + 1;
			var dd = date.getDate();
			var hrs = date.getHours();
			var mnt = date.getMinutes();


			if (dd < 10) {
				dd = '0' + dd;
			}
			if (mm < 10) {
				mm = '0' + mm;
			}
			if (hrs < 10) {
				hrs = '0' + hrs;
			}
			if (mnt < 10) {
				mnt = '0' + mnt;
			}
			return yyyy + '-' + mm + '-' + dd;
		};

		function FormatRupiah(angka, prefix) {
			if (!angka) {
				return '';
			}
			var vangka = angka.toString();
			var number_string = vangka.replace(/[^.\d]/g, '').replace(/[^\w\s]/gi, '').toString(),
				split = number_string.split('.'),
				sisa = split[0].length % 3,
				rupiah = split[0].substr(0, sisa),
				ribuan = split[0].substr(sisa).match(/\d{3}/gi);

			// tambahkan titik jika yang di input sudah menjadi angka ribuan
			if (ribuan) {
				separator = sisa ? ',' : '';
				rupiah += separator + ribuan.join(',');
			}

			rupiah = split[1] != undefined ? rupiah + '.' + split[1] : rupiah;
			return prefix == undefined ? rupiah : (rupiah ? rupiah : '');
		};

		/* DeFormat Rupiah */
		function DeFormatRupiah(angka) {
			var result = angka.replace(/[^\w\s]/gi, '');

			return result;
		};

		/* Format Rupiah Saat Event Keyup */
		$('#qty').keyup(function() {
			$(this).val(FormatRupiah($(this).val()));
		})

		function ValidasiSave() {
			var tanggalbag = $('#tanggalbag').val();
			var jenis = $('#jenis').val();
			var lokasi = $('#lokasigdg').val();
			var keterangan = $('#keterangan').val();

			if (tanggalbag == "" || tanggalbag == 0) {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Tanggal BAG Tidak Boleh kosong',
					showCloseButton: true,
					width: 350,
				});
				$('#tanggalbag').focus();
				var result = false;
			} else if (jenis == "" || jenis == 0) {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Jenis tidak boleh kosong',
					showCloseButton: true,
					width: 350,
				});
				$('#jenis').focus();
				var result = false;
			} else if (lokasi == "" || lokasi == 0) {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Lokasi Gudang tidak boleh kosong',
					showCloseButton: true,
					width: 350,
				});
				$('#lokasigdg').focus();
				var result = false;
			} else if (keterangan == "" || keterangan == 0) {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Keterangan Tidak boleh kosong',
					showCLoseButton: true,
					width: 350,
				});
				$('#keterangan').focus();
				var result = false;
			} else {
				var result = true;
			}
			return result;
		}

		function ValidasiAddDetail(kodebarang) {
			var table = document.getElementById('t_detail');
			var kodebarang = $('#kodebarang').val();
			var namabarang = $('#namabarang').val();
			var qty = $('#qty').val();

			if (kodebarang == "" || kodebarang == 0) {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Kode barang tidak boleh kosong',
					showCloseButton: true,
					width: 350,
				});
				$('#kodebarang').focus();
				return "gagal";
			} else if (namabarang == "" || namabarang == "") {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Nama barang tidak boleh kosong',
					showCloseButton: true,
					width: 350,
				});
				$('#namabarang').focus();
				return "gagal";
			} else if (qty == "" || qty == "") {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Qty tidak boleh kosong',
					showCloseButton: true,
					width: 350,
				});
				$('#qty').focus();
				return "gagal";
			} else {
				for (var r = 1, n = table.rows.length; r < n; r++) {
					var string = "";
					for (var c = 0, m = table.rows[r].cells.length; c < m; c++) {
						if (c == 0) {
							if (table.rows[r].cells[c].innerHTML == kodebarang) {
								Swal.fire({
									title: 'Information',
									icon: 'info',
									html: 'Data ini sudah diinput',
									showCloseButton: true,
									width: 350,
								});
								return "gagal";
							}
						}
					}
				}
			}
			return "sukses";
		}

		/* Declare Toast */
		const Toast = Swal.mixin({
			toast: true,
			position: 'top-end',
			showConfirmButton: false,
			timer: 3000,
			timerProgressBar: true,
			didOpen: (toast) => {
				toast.addEventListener('mouseenter', Swal.stopTimer)
				toast.addEventListener('mouseleave', Swal.resumeTimer)
			}
		});

		function CurrentDate(input) {
			var date = new Date(input);
			var yyyy = date.getFullYear();
			var mm = date.getMonth() + 1;
			var dd = date.getDate();
			var hrs = date.getHours();
			var mnt = date.getMinutes();


			if (dd < 10) {
				dd = '0' + dd;
			}
			if (mm < 10) {
				mm = '0' + mm;
			}
			if (hrs < 10) {
				hrs = '0' + hrs;
			}
			if (mnt < 10) {
				mnt = '0' + mnt;
			}
			return yyyy + '' + mm;
		};

		document.getElementById('caribarang').addEventListener('click', function(event) {
			event.preventDefault();
			var currentDate = CurrentDate(new Date());
			var kodegudang = $('#lokasigdg').val();
			var kodecabang = $('#kodecabang').val();
			$('#t_barang').DataTable({
				"destroy": true,
				"searching": true,
				"processing": true,
				"serverSide": true,
				"lengthChange": true,
				"pageLength": 5,
				"lengthMenu": [5, 10, 25, 50],
				"order": [],
				"scrollX": true,
				"ajax": {
					"url": "<?= base_url('masterdata/Barang/CariDataBarang'); ?>",
					"method": "POST",
					"data": {
						nmtb: "cari_barangbag",
						field: {
							kodebarang: "kodebarang",
							namabarang: "namabarang",
							namamerk: "namamerk",
							namasatuan: "namasatuan",
							stock: "stock"
						},
						sort: "kodebarang",
						where: {
							kodebarang: "kodebarang",
							namabarang: "namabarang",
							namamerk: "namamerk",
							namasatuan: "namasatuan",
						},
						value: "aktif = true AND kodecabang = '" + $('#cabang').val() + "' AND kodegudang = '" + $('#lokasigdg').val() + "' AND periode = '" + currentDate + "'"
					}
					// AND kodegudang = '" + kodegudang + "' AND kodecabang = '" + kodecabang + "'
				},
				"columnDefs": [{
					"targets": 5,
					"data": "stock",
					"render": function(data, type, row, meta) {
						return FormatRupiah(row[5]);
					}
				}]
			});
		});

		$(document).on("click", ".searchbarang", function() {
			var kode = $(this).attr("data-id");
			$.ajax({
				url: "<?= base_url('masterdata/Barang/DataBarang') ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					kode: kode
				},
				success: function(data) {
					for (var i = 0; i < data.length; i++) {
						$('#kodebarang').val(data[i].kode.trim());
						$('#namabarang').val(data[i].nama.trim());
					}
				}
			}, false);
		});

		function AmbilDataDetail() {
			var table = document.getElementById('t_detail');
			var arr2 = [];
			for (var r = 1, n = table.rows.length; r < n; r++) {
				var string = "";
				for (var c = 0, m = table.rows[r].cells.length; c < m - 1; c++) {
					if (c == 0) {
						string = "{" + table.rows[0].cells[c].innerHTML + " : '" + table.rows[r].cells[c].innerHTML + "'";
					} else {
						string = string + ", " + table.rows[0].cells[c].innerHTML + " : '" + table.rows[r].cells[c].innerHTML + "'";
					}
				}
				string = string + "}";
				var obj = JSON.stringify(eval('(' + string + ')'));
				var arr = $.parseJSON(obj);
				arr2.push(arr);
			}
			return arr2;
		};

		$("#adddetail").click(function() {
			var kodebarang = $('#kodebarang').val();
			var namabarang = $('#namabarang').val();
			var qty = $('#qty').val();
			if (ValidasiAddDetail(kodebarang) == "sukses") {
				var row = "";
				row =
					'<tr id="' + kodebarang + '">' +
					'<td nowrap>' + kodebarang + '</td>' +
					'<td nowrap>' + namabarang + '</td>' +
					'<td nowrap>' + FormatRupiah(qty) + '</td>' +
					'<td nowrap style="text-align: center; width: 200px;">' +
					'<button data-table="' + kodebarang + '" data-toggle="modal" data-target="#changeqty" class="editdetail btn btn-success m-1 p-1"><i class="fas fa-edit"></i> Edit</button>' +
					'<button data-table="' + kodebarang + '" class="deldetail btn btn-danger m-1 p-1"><i class="fa fa-trash mr-1"></i> Delete</button>' +
					'</td>' +
					'</tr>';
				$('#tb_detail').append(row);
				$('#kodebarang').val("");
				$('#namabarang').val("");
				$('#qty').val(0);
			}
		});

		$(document).on('click', '.editdetail', function() {
			var table = document.getElementById('t_detail');
			var tr = document.getElementById($(this).attr("data-table"));
			var td = tr.getElementsByTagName('td');
			for (var c = 0, m = table.rows[0].cells.length; c < m - 1; c++) {
				var chgth = table.rows[0].cells[c].innerHTML.replace(/ /g, '').replace(/\s/g, '').toLowerCase();
				var valuetd = td[c].innerHTML;
				$('#chg' + chgth).val(valuetd);
			}
		});

		$(document).on('click', '#changedetail', function() {
			var kodebarang = $('#chgkode').val();
			var namabarang = $('#chgnama').val();
			var qty = $('#chgqty').val();
			$('#' + kodebarang).remove();
			InsertDataDetail(kodebarang, namabarang, qty);
		});

		$(document).on('click', '.deldetail', function() {
			var id = $(this).attr("data-table");
			$('#' + id).remove();
		});

		function InsertDataDetail(kodebarang, namabarang, qty) {
			var row = "";
			row =
				'<tr id="' + kodebarang + '">' +
				'<td nowrap>' + kodebarang + '</td>' +
				'<td nowrap>' + namabarang + '</td>' +
				'<td nowrap>' + FormatRupiah(qty) + '</td>' +
				'<td nowrap style="text-align: center; width: 200px;">' +
				'<button data-table="' + kodebarang + '" data-toggle="modal" data-target="#changeqty" class="editdetail btn btn-sm btn-success m-1 p-1"><i class="fas fa-edit"></i> Edit</button>' +
				'<button data-table="' + kodebarang + '" class="deldetail btn btn-sm btn-danger m-1 p-1"><i class="fa fa-trash mr-1"></i> Delete</button>' +
				'</td>' +
				'</tr>';
			$('#tb_detail').append(row);
		};

		function GetDataDetail(nomor) {
			$.ajax({
				url: "<?= base_url('inventory/Bag/DataBagDetail') ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					nomor: nomor
				},
				success: function(data) {
					for (var i = 0; i < data.length; i++) {
						var kodebarang = data[i].kodebarang;
						var namabarang = data[i].namabarang.trim();
						var qty = data[i].qty.trim();

						InsertDataDetail(kodebarang, namabarang, qty);
					}
				}
			});
		};

		document.getElementById('save').addEventListener("click", function(event) {
			event.preventDefault();
			var nomorbag = $('#nobag').val();
			var tanggalbag = $('#tanggalbag').val();
			var jenis = $('#jenis').val();
			var lokasi = $('#lokasigdg').val();
			var keterangan = $('#keterangan').val();
			var datadetail = AmbilDataDetail();

			if (ValidasiSave(datadetail) == true) {
				$.ajax({
					url: "<?= base_url("inventory/Bag/Save") ?>",
					method: "POST",
					dataType: "json",
					async: true,
					data: {
						nomorbag: nomorbag,
						tanggalbag: tanggalbag,
						jenis: jenis,
						lokasi: lokasi,
						keterangan: keterangan,
						datadetail: datadetail
					},
					success: function(data) {
						if (data.nomorbag != "") {
							Toast.fire({
								icon: 'success',
								title: data.message
							});
							$('#nobag').prop('disabled', true).val(data.nomor);
							$('#tanggalbag').prop('disabled', true);
							$('#jenis').prop('disabled', true);
							$('#lokasigdg').prop('disabled', true);
							$('#keterangan').prop('disabled', true);
							$('#kodebarang').prop('disabled', true);
							$('#namabarang').prop('disabled', true);

							Cetak(data.nomor);

							$('#caribarang').hide();
							$('#update').prop("disabled", true);
							$('#save').prop("disabled", true);
							$('#approve').prop("disabled", false);
							$('#cancel').prop("disabled", false);
						}
					}
				});
			}
		});

		function Cetak(nomor) {
			// console.log(nomor);
			window.open("<?= base_url() ?>cetak_pdf/BarangRusak/Print/" + nomor);
		}

		document.getElementById("find").addEventListener("click", function(event) {
			event.preventDefault();
			$('#tb_detail').empty();
			$('#t_bag').DataTable({
				"destroy": true,
				"searching": true,
				"processing": true,
				"serverSide": true,
				"lengthChange": true,
				"pageLength": 5,
				"lengthMenu": [5, 10, 25, 50],
				"order": [],
				"ajax": {
					"url": "<?= base_url('inventory/Bag/CariDataBag'); ?>",
					"method": "POST",
					"data": {
						nmtb: "srvt_bag",
						field: {
							nomor: "nomor",
							tanggal: "tanggal",
							jenis: "jenis",
							keterangan: "keterangan",
							approve: "approve",
						},
						sort: "nomor",
						where: {
							nomor: "nomor"
						},
						value: "approve = false AND batal = false AND kodecabang = '" + $('#cabang').val() + "' "
					},
				},
				"columnDefs": [{
						"targets": 3,
						"render": $.fn.dataTable.render.moment('YYYY-MM-DD HH:mm:ss', 'D MMM YY'),
					},
					{
						"targets": 4,
						"data": "jenis",
						"render": function(data, type, row, meta) {
							return (row[4] == '1') ? "Barang Hilang" : (row[4] == '2') ? "Ditemukan" : (row[4] == '3') ? "Barang Rusak" : "Barang Buang";
						}
					},
					{
						"targets": 6,
						"data": "approve",
						"render": function(data, type, row, meta) {
							return (row[6] == 't') ? "Sudah di Approve" : "Belum di Approve";
						}
					}
				]
			});
		});

		document.getElementById("findbagapprove").addEventListener("click", function(event) {
			event.preventDefault();
			$('#tb_detail').empty();
			$('#t_approve').DataTable({
				"destroy": true,
				"searching": true,
				"processing": true,
				"serverSide": true,
				"lengthChange": true,
				"pageLength": 5,
				"lengthMenu": [5, 10, 25, 50],
				"order": [],
				"ajax": {
					"url": "<?= base_url('inventory/Bag/CariDataBag'); ?>",
					"method": "POST",
					"data": {
						nmtb: "srvt_bag",
						field: {
							nomor: "nomor",
							tanggal: "tanggal",
							jenis: "jenis",
							keterangan: "keterangan",
							approve: "approve",
						},
						sort: "nomor",
						where: {
							nomor: "nomor"
						},
						value: "batal = false AND kodecabang = '" + $('#cabang').val() + "' "
					},
				},
				"columnDefs": [{
						"targets": 3,
						"render": $.fn.dataTable.render.moment('YYYY-MM-DD HH:mm:ss', 'D MMM YY'),
					},
					{
						"targets": 4,
						"data": "jenis",
						"render": function(data, type, row, meta) {
							return (row[4] == '1') ? "Barang Hilang" : (row[4] == '2') ? "Ditemukan" : (row[4] == '3') ? "Barang Rusak" : "Barang Buang";
						}
					},
					{
						"targets": 6,
						"data": "approve",
						"render": function(data, type, row, meta) {
							return (row[6] == 't') ? "Sudah di Approve" : "Belum di Approve";
						}
					}
				]
			});
		});

		$(document).on('click', ".printbag", function() {
			var nomor = $(this).attr("data-id");
			Cetak(nomor);
		});

		function Cetak(nomor) {
			// console.log(nomor);
			window.open("<?= base_url() ?>cetak_pdf/BarangRusak/Print/" + nomor);
		}

		$(document).on('click', ".searchbag", function() {
			var nomor = $(this).attr("data-id");
			$.ajax({
				url: "<?= base_url('inventory/Bag/DataBag'); ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					nomor: nomor
				},
				success: function(data) {
					console.log(data)
					for (var i = 0; i < data.length; i++) {
						$('#nobag').val(data[i].nomor.trim());
						$('#tanggalbag').val(FormatDate(data[i].tanggal.trim()));
						$('#jenis').val(data[i].jenis.trim());
						$('#lokasigdg').val(data[i].lokasi.trim());
						$('#keterangan').val(data[i].keterangan.trim());

						if (data[i].approve == 't') {
							$('#save').prop('disabled', true);
							$('#update').prop('disabled', true);
							$('#approve').prop('disabled', true);
							$('#cancel').prop('disabled', false);
						} else {
							$('#save').prop('disabled', true);
							$('#update').prop('disabled', false);
							$('#approve').prop('disabled', false);
							$('#cancel').prop('disabled', true);
						}
					}
					GetDataDetail(nomor);
				}
			}, false);
		});

		document.getElementById('update').addEventListener("click", function(event) {
			event.preventDefault();
			var nomorbag = $('#nobag').val();
			var tanggalbag = $('#tanggalbag').val();
			var jenis = $('#jenis').val();
			var lokasi = $('#lokasigdg').val();
			var keterangan = $('#keterangan').val();
			var datadetail = AmbilDataDetail();

			if (ValidasiSave() == true) {
				$.ajax({
					url: "<?= base_url("inventory/Bag/Update") ?>",
					method: "POST",
					dataType: "json",
					async: true,
					data: {
						nomorbag: nomorbag,
						tanggalbag: tanggalbag,
						jenis: jenis,
						lokasi: lokasi,
						keterangan: keterangan,
						datadetail: datadetail
					},
					success: function(data) {
						if (data.nomorbag != "") {
							Toast.fire({
								icon: 'success',
								title: data.message
							});
							$('#nobag').val(data.nomor);
							$('#tanggalbag').prop('disabled', true);
							$('#jenis').prop('disabled', true);
							$('#lokasigdg').prop('disabled', true);
							$('#keterangan').prop('disabled', true);
							$('#kodebarang').prop('disabled', true);
							$('#nambarang').prop('disabled', true);
							$('#qty').prop('disabled', true);

							$('#update').prop('disabled', true);
							$('#save').prop('disabled', true);
							$('#approve').prop('disabled', false);
							$('#cancel').prop('disabled', false);
						} else {
							Toast.fire({
								icon: 'error',
								title: data.message
							});
						}
					}
				}, false);
			}
		});

		document.getElementById('approve').addEventListener("click", function(event) {
			event.preventDefault();
			var nomorbag = $('#nobag').val();
			var tanggalbag = $('#tanggalbag').val();
			var jenis = $('#jenis').val();
			var lokasi = $('#lokasigdg').val();
			var keterangan = $('#keterangan').val();
			var datadetail = AmbilDataDetail();

			if (ValidasiSave() == true) {
				$.ajax({
					url: "<?= base_url("inventory/Bag/Approve") ?>",
					method: "POST",
					dataType: "json",
					async: true,
					data: {
						nomorbag: nomorbag,
						tanggalbag: tanggalbag,
						jenis: jenis,
						lokasi: lokasi,
						keterangan: keterangan,
						datadetail: datadetail
					},
					success: function(data) {
						if (data.nomor != "") {
							Toast.fire({
								icon: 'success',
								title: data.message
							});
							$('#nomorbag').prop('disabled', true);
							$('#tanggalbag').prop('disabled', true);
							$('#jenis').prop('disabled', true);
							$('#lokasigdg').prop('disabled', true);
							$('#keterangan').prop('disabled', true);
							$('#kodebarang').prop('disabled', true);
							$('#namabarang').prop('disabled', true);
							$('#qty').prop('disabled', true);

							$('#update').prop('disabled', true);
							$('#save').prop('disabled', true);
							$('#approve').prop('disabled', true);
							$('#cancel').prop('disabled', false);
						}
					}
				});
			}
		});

		function ValidasiCancel() {
			var keterangan = $('#keteranganbatal').val();
			if (keterangan == '' || keterangan == 0) {
				Swal.fire({
					title: 'Informasi',
					iconL: 'info',
					html: 'Keterangan tidak boleh kosong',
					showCLoseButton: true,
					width: 350
				});
				$('#keteranganbatal').focus();
				var result = false;
			} else {
				var result = true
			}

			return result;
		}


		document.getElementById("okcancel").addEventListener("click", function(event) {
			event.preventDefault();
			var keterangan = $('#keteranganbatal').val();
			var nomor = $('#nobag').val();
			var jenis = $('#jenis').val();
			var lokasi = $('#lokasigdg').val();
			var datadetail = AmbilDataDetail();
			if (ValidasiCancel() == true) {
				$.ajax({
					url: "<?= base_url('inventory/Bag/Cancel') ?>",
					method: "POST",
					dataType: "json",
					async: true,
					data: {
						nomor: nomor,
						keterangan: keterangan,
						jenis: jenis,
						lokasi: lokasi,
						datadetail: datadetail
					},
					success: function(data) {
						if (data.nomor != "") {
							Toast.fire({
								icon: 'success',
								title: data.message
							});
							$('#nomorbag').prop('disabled', true);
							$('#tanggalbag').prop('disabled', true);
							$('#jenis').prop('disabled', true);
							$('#keterangan').prop('disabled', true);
							$('#kodebarang').prop('disabled', true);
							$('#namabarang').prop('disabled', true);
							$('#qty').prop('disabled', true);

							$('#update').prop('disabled', true);
							$('#save').prop('disabled', true);
							$('#approve').prop('disabled', true);
							$('#cancel').prop('disabled', false);
						}
					}
				});
			}

		});

		// Test


		document.getElementById("clear").addEventListener("click", function(event) {
			event.preventDefault();
			ClearScreen();
		});

		ClearScreen();

	})
</script>