<script type="text/javascript">
    $(document).ready(function() {
        $('#tanggalretur').datepicker({
            format: "yyyy-mm-dd",
            autoclose: true,
            todayHighlight: true
        });
        $('#tanggalnotaretur').datepicker({
            format: "yyyy-mm-dd",
            autoclose: true,
            todayHighlight: true
        });
        $('#tanggalfaktur').datepicker({
            format: "yyyy-mm-dd",
            autoclose: true,
            todayHighlight: true
        });

        function LoadKonfigurasi() {
            $.ajax({
                url: "<?= base_url('sales/SalesOrder/LoadKonfigurasi') ?>",
                method: "POST",
                dataType: "json",
                async: false,
                data: {},
                success: function(data) {
                    for (var i = 0; i < data.length; i++) {
                        ppn = data[i].ppn;
                        discCash = data[i].disccash;
                        discCod = data[i].disccod;
                    }
                }
            }, false);
        }

        function ClearScreen() {
            LoadKonfigurasi();
            DisableEnableFind(false);
            $('#noretur').val("");
            $('#keteranganbatal').val("");

            $('#tglretur').val("<?= date('Y-m-d') ?>");
            $('#noinvoice').val("");
            $('#nopelanggan').val("");
            $('#notaretur').val("").prop('disabled', false);
            $('#tglnotaretur').val("<?= date('Y-m-d') ?>");
            $('#nofakturpajak').val("").prop('disabled', false);
            $('#tglfaktur').val("<?= date('Y-m-d') ?>");
            $('#discash').val(0);
            $('#discustomer').val(0);
            $('#total').val(0);
            $('#dpp').val(0);
            $('#ppn').val(0);
            $('#grandtotal').val(0);

            $('#pengembalian').prop('disabled', false);

            $('#kodebrg').val("");
            $('#namabrg').val("");
            $('#qty').val(0);
            $('#hargajual').val(0);
            $('#disc').val(0);
            $('#discitem').val(0);
            $('#discpaket').val(0);
            $('#discpaketitem').val(0);
            $('#nilaicash').val(0);
            $('#nilaicustomer').val(0);
            $('#totalprice').val(0);

            $('#tb_detail').empty();

            $('#save').prop('disabled', false);
            // $('#update').prop('disabled', true);
            $('#cancel').prop('disabled', true);

            $('#cariinvoice').show();
            $('#caribarang').show();
            $('#adddetail').show();
            $('.editdetail').show();
            $('.deldetail').show();

            LoadGudang();
        }

        function ValidasiSave(datadetail) {
            var tglretur = $('#tglretur').val();
            var noinvoice = $('#noinvoice').val();
            var nopelanggan = $('#nopelanggan').val();
            var notaretur = $('#notaretur').val();
            var tglnotaretur = $('#tglnotaretur').val();
            var nofakturpajak = $('#nofakturpajak').val();
            var tglfakturpajak = $('#tglfaktur').val();
            var discash = $('#discash').val();
            var discustomer = $('#discustomer').val();
            var total = $('#total').val();
            var dpp = $('#dpp').val();
            var ppn = $('#ppn').val();
            var grandtotal = $('#grandtotal').val();

            if (tglretur == '' || tglretur == 0) {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Tanggal Retur tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#tglretur').focus();
                var result = false;
            } else if (noinvoice == '' || noinvoice == 0) {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Nomor Invoice tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#noinvoice').focus();
                var result = false;
            } else if (nopelanggan == '' || nopelanggan == 0) {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Nomor Pelanggan tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#nopelanggan').focus();
                var result = false;
            } else if (notaretur == '' || notaretur == 0) {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Nota Retur tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#notaretur').focus();
                var result = false;
            } else if (tglnotaretur == '' || tglnotaretur == 0) {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Tanggal Nota Retur tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#tglnotaretur').focus();
                var result = false;
            } else if (nofakturpajak == '' || nofakturpajak == 0) {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Nomor Faktur Pajak tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#nofakturpajak').focus();
                var result = false;
            } else if (tglfakturpajak == '' || tglfakturpajak == 0) {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Tanggal Faktur Pajak tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#tglfaktur').focus();
                var result = false;
            } else if (discash == '') {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Diskon Cash tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#discash').focus();
                var result = false;
            } else if (discustomer == '') {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Diskon Customer tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#discustomer').focus();
                var result = false;
            } else if (total == '') {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Total tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#total').focus();
                var result = false;
            } else if (dpp == '') {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'DPP tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#dpp').focus();
                var result = false;
            } else if (ppn == '') {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'PPN tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#ppn').focus();
                var result = false;
            } else if (grandtotal == '') {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Grand Total tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#grandtotal').focus();
                var result = false;
            } else if (datadetail.length == 0 || datadetail.length == '0') {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Data Detail tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#kodebrg').focus();
                var result = false;
            } else {
                var result = true;
            }
            return result;
        }

        /* Validasi Data Add Detail */
        function ValidasiAddDetail(kodebarang) {
            var table = document.getElementById('t_detail');

            var kodebarang = $('#kodebrg').val();
            var namabarang = $('#namabrg').val();
            var qty = $('#qty').val();
            var harga = $('#hargajual').val();
            // var disc = $('#disc').val();
            // var discitem = $('#discitem').val();
            // var discpaket = $('#discpaket').val();
            // var discpaketitem = $('#discpaketitem').val();
            var totalprice = $('#totalprice').val();

            if (kodebarang == '' || kodebarang == 0) {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Kode Barang tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#kodebarang').focus();
                return "gagal";
            } else if (namabarang == '' || namabarang == 0) {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Nama Barang tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#namabarang').focus();
                return "gagal";
            } else if (qty == '' || qty == 0) {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Qty tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#qty').focus();
                return "gagal";
            } else if (harga == '' || harga == 0) {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Harga tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#harga').focus();
                return "gagal";
            } else if (totalprice == '' || totalprice == 0) {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Total tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#totalprice').focus();
                return "gagal";
            } else {
                for (var r = 1, n = table.rows.length; r < n; r++) {
                    var string = "";
                    for (var c = 0, m = table.rows[r].cells.length; c < m; c++) {
                        if (c == 0) {
                            if (table.rows[r].cells[c].innerHTML == kodebarang) {
                                Swal.fire({
                                    title: 'Informasi',
                                    icon: 'info',
                                    html: 'Data ini sudah diinput.',
                                    showCloseButton: true,
                                    width: 350,
                                });
                                return "gagal";
                            }
                        }
                    }
                }
            }
            return "sukses"
        }

        $('#disc[max]:not([max=""])').on('input', function(event) {
            var $this = $(this);
            var maxlength = $this.attr('max').length;
            var value = $this.val();
            if (value && value.length >= maxlength) {
                $this.val(value.substr(0, maxlength));
            }
        });

        $('#discpaket[max]:not([max=""])').on('input', function(event) {
            var $this = $(this);
            var maxlength = $this.attr('max').length;
            var value = $this.val();
            if (value && value.length >= maxlength) {
                $this.val(value.substr(0, maxlength));
            }
        });

        $('#qty').keyup(function() {
            // (Harga - Disc Barang per Item - Disc Paket per Item) x qty   
            var qty = $(this).val();
            var harga = DeFormatRupiah($('#hargajual').val());
            var discitem = DeFormatRupiah($('#discitem').val());
            var discpaketitem = DeFormatRupiah($('#discpaketitem').val());
            var disc = DeFormatRupiah($('#disc').val());

            var subtotal = ((harga - discitem - discpaketitem) * qty);
            $('#totalprice').val(FormatRupiah(Math.round(subtotal)));
        })

        $('#disc').keyup(function() {
            // (Harga - Disc Barang per Item - Disc Paket per Item) x qty   
            var disc = $(this).val();
            var harga = DeFormatRupiah($('#hargajual').val());
            var discitem = DeFormatRupiah($('#discitem').val());
            var discpaketitem = DeFormatRupiah($('#discpaketitem').val());
            var qty = DeFormatRupiah($('#qty').val());

            var discperitem = (parseFloat(harga) * (disc / 100));
            $('#discitem').val(FormatRupiah(Math.round(parseFloat(discperitem))));
            var subtotal = ((harga - discperitem - discpaketitem) * qty);
            $('#totalprice').val(FormatRupiah(Math.round(subtotal)));
        })

        $('#discpaket').keyup(function() {
            // (Harga - Disc Barang per Item - Disc Paket per Item) x qty
            var discpaket = $(this).val();
            var disc = $('#disc').val();
            var harga = DeFormatRupiah($('#hargajual').val());
            var discitem = DeFormatRupiah($('#discitem').val());
            var discpaketitem = DeFormatRupiah($('#discpaketitem').val());
            var qty = DeFormatRupiah($('#qty').val());

            var discperitem = (parseFloat(harga) * (disc / 100));
            $('#discitem').val(FormatRupiah(Math.round(parseFloat(discperitem))));
            var discpaketperitem = (parseFloat(harga) - (discperitem)) * (discpaket / 100);
            $('#discpaketitem').val(FormatRupiah(Math.round(parseFloat(discpaketperitem))));
            var subtotal = ((harga - discperitem - discpaketperitem) * qty);
            $('#totalprice').val(FormatRupiah(Math.round(subtotal)));
        })

        $('#chgqty').keyup(function() {
            // (Harga - Disc Barang per Item - Disc Paket per Item) x qty   
            var qty = $(this).val();
            var harga = DeFormatRupiah($('#chgharga').val());
            var discitem = DeFormatRupiah($('#chgdiscitem').val());
            var discpaketitem = DeFormatRupiah($('#chgdiscpaketitem').val());
            var disc = DeFormatRupiah($('#chgdisc').val());

            var subtotal = ((harga - discitem - discpaketitem) * qty);
            $('#chgsubtotal').val(FormatRupiah(Math.round(subtotal)));
        })

        $('#chgdisc').keyup(function() {
            // (Harga - Disc Barang per Item - Disc Paket per Item) x qty   
            var disc = $(this).val();
            var harga = DeFormatRupiah($('#chgharga').val());
            var discitem = DeFormatRupiah($('#chgdiscitem').val());
            var discpaketitem = DeFormatRupiah($('#chgdiscpaketitem').val());
            var qty = DeFormatRupiah($('#chgqty').val());

            var discperitem = (parseFloat(harga) * (disc / 100));
            $('#chgdiscitem').val(FormatRupiah(Math.round(parseFloat(discperitem))));
            var subtotal = ((harga - discperitem - discpaketitem) * qty);
            $('#chgsubtotal').val(FormatRupiah(Math.round(subtotal)));
        })

        $('#chgdiscpaket').keyup(function() {
            // (Harga - Disc Barang per Item - Disc Paket per Item) x qty
            var discpaket = $(this).val();
            var disc = $('#chgdisc').val();
            var harga = DeFormatRupiah($('#chgharga').val());
            var discitem = DeFormatRupiah($('#chgdiscitem').val());
            var discpaketitem = DeFormatRupiah($('#chgdiscpaketitem').val());
            var qty = DeFormatRupiah($('#chgqty').val());

            var discperitem = (parseFloat(harga) * (disc / 100));
            $('#chgdiscitem').val(FormatRupiah(Math.round(parseFloat(discperitem))));
            var discpaketperitem = (parseFloat(harga) - (discperitem)) * (discpaket / 100);
            $('#chgdiscpaketitem').val(FormatRupiah(Math.round(parseFloat(discpaketperitem))));
            var subtotal = ((harga - discperitem - discpaketperitem) * qty);
            $('#chgsubtotal').val(FormatRupiah(Math.round(subtotal)));
        })

        function HitungSummary() {
            var table = document.getElementById('t_detail');
            var sublist = 0;
            var subtotal = 0;
            var nilaidiscustomer = 0;
            var nilaidiscash = 0;
            var discash = DeFormatRupiah($('#discash').val());
            var discustomer = DeFormatRupiah($('#discustomer').val());
            var ppntunai = DeFormatRupiah($('#ppntunai').val());

            for (var r = 1, n = table.rows.length; r < n; r++) {
                for (var c = 0, m = table.rows[r].cells.length; c < m - 1; c++) {
                    sublist = parseFloat(DeFormatRupiah(table.rows[r].cells[8].innerHTML));
                }
                subtotal += parseFloat(sublist);
            }

            $('#total').val(FormatRupiah(subtotal));
            nilaidiscustomer = Math.round(parseFloat(subtotal) * parseFloat(discustomer) / 100);
            nilaidiscash = Math.round((parseFloat(subtotal) - parseFloat(nilaidiscustomer)) * parseFloat(discash) / 100);
            dpp = subtotal - nilaidiscustomer - nilaidiscash;
            ppn = Math.round(parseFloat(dpp) * (parseFloat(ppntunai) / 100));
            grandtotal = parseFloat(dpp) + parseFloat(ppn);

            if ((discash == 0 || discash == '0') && (discustomer == 0 || discustomer == '0')) {
                $('#nilaicash').val(0);
                $('#nilaicustomer').val(0);
            } else if ((discash != 0 || discash != '0') && (discustomer == 0 || discustomer == '0')) {
                $('#nilaicash').val(FormatRupiah(nilaidiscash));
                $('#nilaicustomer').val(0);
            } else if ((discash == 0 || discash == '0') && (discustomer != 0 || discustomer != '0')) {
                $('#nilaicash').val(0);
                $('#nilaicustomer').val(FormatRupiah(nilaidiscustomer));
            } else {
                $('#nilaicash').val(FormatRupiah(nilaidiscash));
                $('#nilaicustomer').val(FormatRupiah(nilaidiscustomer));
            }
            $('#dpp').val(FormatRupiah(Math.round(dpp)));
            $('#ppn').val(FormatRupiah(Math.round(ppn)));
            $('#grandtotal').val(FormatRupiah(Math.round(grandtotal)));
        };

        /*Format Date*/
        function FormatDate(input) {
            var date = new Date(input);
            var yyyy = date.getFullYear();
            var mm = date.getMonth() + 1;
            var dd = date.getDate();
            var hrs = date.getHours();
            var mnt = date.getMinutes();


            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }
            if (hrs < 10) {
                hrs = '0' + hrs;
            }
            if (mnt < 10) {
                mnt = '0' + mnt;
            }
            return yyyy + '-' + mm + '-' + dd;
        };

        /* Declare Toast */
        const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000,
            timerProgressBar: true,
            didOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
            }
        });

        /* Format Rupiah */
        function FormatRupiah(angka, prefix) {
            if (!angka) {
                return '';
            }
            var vangka = angka.toString();
            var number_string = vangka.replace(/[^.\d]/g, '').replace(/[^\w\s]/gi, '').toString(),
                split = number_string.split('.'),
                sisa = split[0].length % 3,
                rupiah = split[0].substr(0, sisa),
                ribuan = split[0].substr(sisa).match(/\d{3}/gi);

            // tambahkan titik jika yang di input sudah menjadi angka ribuan
            if (ribuan) {
                separator = sisa ? ',' : '';
                rupiah += separator + ribuan.join(',');
            }

            rupiah = split[1] != undefined ? rupiah + '.' + split[1] : rupiah;
            return prefix == undefined ? rupiah : (rupiah ? rupiah : '');
        };

        /* DeFormat Rupiah */
        function DeFormatRupiah(angka) {
            var result = angka.replace(/[^\w\s]/gi, '');

            return result;
        };

        document.getElementById("cariinvoice").addEventListener("click", function(event) {
            event.preventDefault();
            $('#tb_detail').empty();
            $('#t_invoice').DataTable({
                "destroy": true,
                "searching": true,
                "processing": true,
                "serverSide": true,
                "lengthChange": true,
                "pageLength": 5,
                "lengthMenu": [5, 10, 25, 50],
                "order": [],
                "ajax": {
                    "url": "<?= base_url('inventory/ReturBarang/CariDataInvoice'); ?>",
                    "method": "POST",
                    "data": {
                        nmtb: "srvt_invoice",
                        field: {
                            nomor: "nomor",
                            tanggal: "tanggal",
                            tgljthtempo: "tgljthtempo",
                            namapelanggan: "namapelanggan"
                        },
                        sort: "nomor",
                        where: {
                            nomor: "nomor",
                            namapelanggan: "namapelanggan"
                        },
                        value: "batal = false AND statusretur = false AND kodecabang = '" + $('#cabang').val() + "'"
                    },
                },
                "columnDefs": [{
                        "targets": 2,
                        "render": $.fn.dataTable.render.moment('YYYY-MM-DD HH:mm:ss', 'D MMM YY')
                    },
                    {
                        "targets": 3,
                        "render": $.fn.dataTable.render.moment('YYYY-MM-DD HH:mm:ss', 'D MMM YY')
                    }
                ]
            });
        })

        $(document).on('click', ".searchinvoice", function() {
            var nomor = $(this).attr("data-id");
            GetDataInvoice(nomor);
        });

        function GetDataInvoice(nomor) {
            $.ajax({
                url: "<?= base_url('inventory/ReturBarang/DataInvoice'); ?>",
                method: "POST",
                dataType: "json",
                async: false,
                data: {
                    nomor: nomor
                },
                success: function(data) {
                    console.log(data);
                    for (var i = 0; i < data.length; i++) {
                        $('#noinvoice').val(data[i].nomor.trim());
                        $('#nopelanggan').val(data[i].nopelanggan.trim());
                        $('#discash').val(FormatRupiah(data[i].disccash.trim()));
                        $('#discustomer').val(FormatRupiah(data[i].disccustomer.trim()));
                        $('#lokasigdg').val(data[i].lokasi.toString());
                        // $('#total').val(FormatRupiah(data[i].total.trim()));
                        // $('#dpp').val(FormatRupiah(data[i].dpp.trim()));
                        // $('#ppn').val(FormatRupiah(data[i].ppn.trim()));
                        // $('#grandtotal').val(FormatRupiah(data[i].grandtotal.trim()));
                    }
                }
            });
        };

        /*Cari Data Barang*/
        document.getElementById("caribarang").addEventListener("click", function(event) {
            event.preventDefault();

            var nomor = $('#noinvoice').val();
            $('#t_barang').DataTable({
                "destroy": true,
                "searching": true,
                "processing": true,
                "serverSide": true,
                "lengthChange": false,
                "pageLength": 5,
                "lengthMenu": [5, 25, 50, 100],
                "order": [],
                "ajax": {
                    "url": "<?= base_url('inventory/ReturBarang/CariDataInvoiceDetail'); ?>",
                    "method": "POST",
                    "data": {
                        nmtb: "srvt_invoicedetail",
                        field: {
                            kode: "kodebarang",
                            namabarang: "namabarang"
                        },
                        sort: "kodebarang",
                        where: {
                            kodebarang: "kodebarang",
                            namabarang: "namabarang"
                        },
                        // value: "aktif = true AND kodecabang = '"+$('#cabang').val()+"' AND kodegudang = '"+$('#gudang').val()+"'"
                        value: "nomor = '" + nomor + "' "
                    },
                }
            });
        });

        $(document).on('click', ".searchinvoicedetail", function() {
            var nomor = $('#noinvoice').val();
            var kodebarang = $(this).attr("data-id");
            GetDataInvoiceDetail(nomor, kodebarang);
        });

        function GetDataInvoiceDetail(nomor, kodebarang) {
            $.ajax({
                url: "<?= base_url('inventory/ReturBarang/DataInvoiceDetail'); ?>",
                method: "POST",
                dataType: "json",
                async: false,
                data: {
                    nomor: nomor,
                    kodebarang: kodebarang
                },
                success: function(data) {
                    for (var i = 0; i < data.length; i++) {
                        $('#kodebrg').val(data[i].kodebarang.trim());
                        $('#namabrg').val(data[i].namabarang.trim());
                        $('#qty').val(data[i].qty.trim());
                        $('#hargajual').val(FormatRupiah(data[i].harga.trim()));
                    }
                }
            }, false);
        };

        $("#adddetail").click(function() {
            var kodebarang = $('#kodebrg').val();
            var namabarang = $('#namabrg').val();
            var qty = $('#qty').val();
            var harga = $('#hargajual').val();
            var disc = $('#disc').val();
            var discitem = $('#discitem').val();
            var discpaket = $('#discpaket').val();
            var discpaketitem = $('#discpaketitem').val();
            var totalprice = $('#totalprice').val();

            if (ValidasiAddDetail(kodebarang) == 'sukses') {
                var row = "";
                row =
                    '<tr id="' + kodebarang + '">' +
                    '<td nowrap>' + kodebarang + '</td>' +
                    '<td nowrap>' + namabarang + '</td>' +
                    '<td nowrap>' + qty + '</td>' +
                    '<td nowrap>' + harga + '</td>' +
                    '<td nowrap">' + disc + '</td>' +
                    '<td nowrap">' + discitem + '</td>' +
                    '<td nowrap">' + discpaket + '</td>' +
                    '<td nowrap">' + discpaketitem + '</td>' +
                    '<td nowrap>' + FormatRupiah(totalprice) + '</td>' +
                    '<td nowrap>' +
                    '<button data-table="' + kodebarang + '" data-toggle="modal" data-target="#changeqty" class="editdetail btn btn-sm btn-success mb-0 p-1"><i class="far fa-edit"></i> Edit</button>&nbsp;' +
                    '<button data-table="' + kodebarang + '" class="deldetail btn btn-sm btn-danger mb-0 p-1"><i class="fa fa-trash"></i> Delete</button>' +
                    '</td>' +
                    '</tr>';
                $('#tb_detail').append(row);
                HitungSummary();
                $('#kodebrg').val("");
                $('#namabrg').val("");
                $('#qty').val(0);
                $('#hargajual').val(0);
                $('#disc').val(0);
                $('#discitem').val(0);
                $('#discpaket').val(0);
                $('#discpaketitem').val(0);
                $('#totalprice').val(0);
            }
        });

        $(document).on('click', '.deldetail', function() {
            var id = $(this).attr("data-table");
            $('#' + id).remove();
            HitungSummary();
        });

        $(document).on('click', '.editdetail', function() {
            var table = document.getElementById('t_detail');
            var tr = document.getElementById($(this).attr("data-table"));
            var td = tr.getElementsByTagName("td");
            for (var c = 0, m = table.rows[0].cells.length; c < m - 1; c++) {
                var chgth = table.rows[0].cells[c].innerHTML.replace(/ /g, '').replace(/\s/g, '').toLowerCase();
                var valuetd = td[c].innerHTML;
                $('#chg' + chgth).val(valuetd);
            }
        });

        $(document).on('click', '#changedetail', function() {
            // var kode = $('#chgkode').val(); 
            var kodebarang = $('#chgkode').val();
            var namabarang = $('#chgnama').val();
            var qty = $('#chgqty').val();
            var harga = $('#chgharga').val();
            var disc = $('#chgdisc').val();
            var discitem = $('#chgdiscitem').val();
            var discpaket = $('#chgdiscpaket').val();
            var discpaketitem = $('#chgdiscpaketitem').val();
            var subtotal = $('#chgsubtotal').val();
            $('#' + kodebarang).remove();
            InsertDataDetail(kodebarang, namabarang, qty, harga, disc, discitem, discpaket, discpaketitem, subtotal);
            HitungSummary();
        });

        function InsertDataDetail(kodebarang, namabarang, qty, harga, disc, discitem, discpaket, discpaketitem, subtotal) {

            var row = "";
            row =
                '<tr id="' + kodebarang + '">' +
                '<td nowrap>' + kodebarang + '</td>' +
                '<td nowrap>' + namabarang + '</td>' +
                '<td nowrap>' + FormatRupiah(qty) + '</td>' +
                '<td nowrap>' + FormatRupiah(harga) + '</td>' +
                '<td nowrap">' + disc + '</td>' +
                '<td nowrap">' + FormatRupiah(discitem) + '</td>' +
                '<td nowrap">' + discpaket + '</td>' +
                '<td nowrap">' + FormatRupiah(discpaketitem) + '</td>' +
                '<td nowrap >' + FormatRupiah(subtotal) + '</td>' +
                '<td nowrap style="text-align: center; width: 200px;">' +
                '</td>' +
                '</tr>';
            $('#tb_detail').append(row);
        };

        function AmbilDataDetail() {
            var table = document.getElementById('t_detail');
            var arr2 = [];
            for (var r = 1, n = table.rows.length; r < n; r++) {
                var string = "";
                for (var c = 0, m = table.rows[r].cells.length; c < m; c++) {
                    if (c == 0) {
                        string = "{" + table.rows[0].cells[c].innerHTML + " : '" + table.rows[r].cells[c].innerHTML + "'";
                    } else {
                        string = string + ", " + table.rows[0].cells[c].innerHTML + " : '" + table.rows[r].cells[c].innerHTML + "'";
                    }
                }
                string = string + "}";
                var obj = JSON.stringify(eval('(' + string + ')'));
                var arr = $.parseJSON(obj);
                arr2.push(arr);
            }
            return arr2;
        };

        function LoadGudang() {
            $("#lokasigdg").empty();
            $("#lokasigdg").append('<option value="">Pilih Gudang</option>');
            $.ajax({
                url: "<?= base_url('inventory/ReturBarang/LoadGudang') ?>",
                method: "POST",
                dataType: "json",
                async: false,
                data: {
                    cabang: $('#cabang').val()
                },
                success: function(data) {
                    for (var i = 0; i < data.length; i++) {
                        $("#lokasigdg").append('<option value = ' + data[i].kode + ' >' + data[i].nama + '</option>');
                    }
                }
            }, false);
        }

        document.getElementById("save").addEventListener("click", function(event) {
            console.log($('#lokasigdg').val());
            event.preventDefault();
            var noretur = $('#noretur').val();
            var lokasigdg = $('#lokasigdg').val();
            var tglretur = $('#tglretur').val();
            var noinvoice = $('#noinvoice').val();
            var nopelanggan = $('#nopelanggan').val();
            var notaretur = $('#notaretur').val();
            var tglnotaretur = $('#tglnotaretur').val();
            var nofakturpajak = $('#nofakturpajak').val();
            var tglfakturpajak = $('#tglfaktur').val();
            var discash = $('#discash').val();
            var discustomer = $('#discustomer').val();
            var nilaicash = $('#nilaicash').val();
            var nilaicustomer = $('#nilaicustomer').val();
            var total = $('#total').val();
            var dpp = $('#dpp').val();
            var ppn = $('#ppn').val();
            var grandtotal = $('#grandtotal').val();

            var pengembalian = $('input[name="pengembalian"]:checked').val();

            var datadetail = AmbilDataDetail();

            if (ValidasiSave(datadetail) == true) {
                $.ajax({
                    url: "<?= base_url("inventory/ReturBarang/Save") ?>",
                    method: "POST",
                    dataType: "json",
                    async: true,
                    data: {
                        noretur: noretur,
                        lokasigdg: lokasigdg,
                        tglretur: tglretur,
                        noinvoice: noinvoice,
                        nopelanggan: nopelanggan,
                        notaretur: notaretur,
                        tglnotaretur: tglnotaretur,
                        nofakturpajak: nofakturpajak,
                        tglfakturpajak: tglfakturpajak,
                        discash: discash,
                        discustomer: discustomer,
                        nilaicash: nilaicash,
                        nilaicustomer: nilaicustomer,
                        total: total,
                        pengembalian: pengembalian,
                        dpp: dpp,
                        ppn: ppn,
                        grandtotal: grandtotal,
                        datadetail: datadetail
                    },
                    success: function(data) {
                        console.log(data);
                        if (data.nomor != "") {
                            Toast.fire({
                                icon: 'success',
                                title: data.message
                            });
                            $('#noretur').val(data.nomor);
                            $('#tglretur').prop('disabled', true);
                            $('#noinvoice').prop('disabled', true);
                            $('#nopelanggan').prop('disabled', true);
                            $('#notaretur').prop('disabled', true);
                            $('#tglnotaretur').prop('disabled', true);
                            $('#nofakturpajak').prop('disabled', true);
                            $('#tglfakturpajak').prop('disabled', true);
                            $('#discash').prop('disabled', true);
                            $('#discustomer').prop('disabled', true);
                            $('#total').prop('disabled', true);
                            $('#dpp').prop('disabled', true);
                            $('#ppn').prop('disabled', true);
                            $('#grandtotal').prop('disabled', true);

                            CetakRetur(data.nomor);
                            CetakNotaRetur(data.nomor);

                            $('#save').prop('disabled', true);
                            // $('#update').prop('disabled', true);

                            $('#cancel').prop('disabled', false);

                            $('#cariinvoice').hide();
                            $('#caribarang').hide();
                            $('#adddetail').hide();
                            $('.editdetail').hide();
                            $('.deldetail').hide();
                        } else {
                            Toast.fire({
                                icon: 'error',
                                title: data.message
                            });
                        }
                    }
                }, false)
            }
        })

        function CetakRetur(nomor) {
            // console.log(nomor);
            window.open("<?= base_url() ?>cetak_pdf/ReturPenjualan/Print/" + nomor);
        }

        function CetakNotaRetur(nomor) {
            // console.log(nomor);
            window.open("<?= base_url() ?>cetak_pdf/NotaRetur/Print/" + nomor);
        }

        document.getElementById("find").addEventListener("click", function(event) {
            event.preventDefault();
            $('#tb_detail').empty();
            $('#t_rb').DataTable({
                "destroy": true,
                "searching": true,
                "processing": true,
                "serverSide": true,
                "lengthChange": true,
                "pageLength": 5,
                "lengthMenu": [5, 10, 25, 50],
                "order": [],
                "ajax": {
                    "url": "<?= base_url('inventory/ReturBarang/CariDataReturBarang'); ?>",
                    "method": "POST",
                    "data": {
                        nmtb: "cari_rb",
                        field: {
                            nomor: "nomor",
                            tanggal: "tanggal",
                            notaretur: "notaretur",
                            tglnotaretur: "tglnotaretur",
                            noinvoice: "noinvoice",
                            nama: "nama"
                        },
                        sort: "nomor",
                        where: {
                            nomor: "nomor",
                            noinvoice: "noinvoice",
                            nama: "nama"
                        },
                        value: "kodecabang = '" + $('#cabang').val() + "' "
                    },
                },
                "columnDefs": [{
                        "targets": 2,
                        "render": $.fn.dataTable.render.moment('YYYY-MM-DD HH:mm:ss', 'D MMM YY')
                    },
                    {
                        "targets": 4,
                        "render": $.fn.dataTable.render.moment('YYYY-MM-DD HH:mm:ss', 'D MMM YY')
                    }
                ]
            });
        })

        $(document).on('click', ".searchrb", function() {
            var nomor = $(this).attr("data-id");
            GetData(nomor);
        });

        function GetData(nomor) {
            $.ajax({
                url: "<?= base_url('inventory/ReturBarang/DataReturBarang'); ?>",
                method: "POST",
                dataType: "json",
                async: false,
                data: {
                    nomor: nomor
                },
                success: function(data) {
                    for (var i = 0; i < data.length; i++) {
                        $('#noretur').val(data[i].nomor.trim());
                        $('#tglretur').val(FormatDate(data[i].tanggal.trim()));
                        $('#noinvoice').val(data[i].noinvoice.trim());
                        $('#nopelanggan').val(data[i].nopelanggan.trim());
                        $('#notaretur').val(data[i].notaretur.trim());
                        $('#tglnotaretur').val(FormatDate(data[i].tglnotaretur.trim()));
                        $('#nofakturpajak').val(data[i].nofakturpajak.trim());
                        $('#tglfaktur').val(FormatDate(data[i].tglfakturpajak.trim()));
                        $('#discash').val(FormatRupiah(data[i].disccash.trim()));
                        $('#discustomer').val(FormatRupiah(data[i].disccustomer.trim()));
                        $('#nilaicash').val(FormatRupiah(data[i].nilaicash.trim()));
                        $('#nilaicustomer').val(FormatRupiah(data[i].nilaicustomer.trim()));
                        $('#total').val(FormatRupiah(data[i].total.trim()));
                        $('#dpp').val(FormatRupiah(data[i].dpp.trim()));
                        $('#ppn').val(FormatRupiah(data[i].ppn.trim()));
                        $('#grandtotal').val(FormatRupiah(data[i].grandtotal.trim()));
                        $('#lokasigdg').val(data[i].lokasigudang.trim());

                        if (data[i].pengembalian.trim() == 't') {
                            $('#pengembalian').prop('checked', true);
                        } else {
                            $('#tidak_pengembalian').prop('checked', true);
                        }

                    }
                    DisableEnableFind(true);
                    GetDataDetail(nomor);

                    // $('#update').prop('disabled', false);
                    $('#cancel').prop('disabled', false);
                    $('#save').prop('disabled', true);
                }
            });
        };

        function GetDataDetail(nomor) {
            $.ajax({
                url: "<?= base_url('inventory/ReturBarang/DataReturBarangDetail'); ?>",
                method: "POST",
                dataType: "json",
                async: false,
                data: {
                    nomor: nomor
                },
                success: function(data) {
                    for (var i = 0; i < data.length; i++) {
                        // var kode = data[i].kode;
                        var kodebarang = data[i].kodebarang.trim();
                        var namabarang = data[i].namabarang.trim();
                        var qty = data[i].qty.trim();
                        var harga = data[i].harga.trim();
                        var disc = data[i].discbarang.trim();
                        var discitem = data[i].discbrgitem.trim();
                        var discpaket = data[i].discpaket.trim();
                        var discpaketitem = data[i].discpaketitem.trim();
                        var subtotal = data[i].total.trim();

                        InsertDataDetail(kodebarang, namabarang, qty, harga, disc, discitem, discpaket, discpaketitem, subtotal);
                    }
                }
            });
        };

        // document.getElementById("update").addEventListener("click", function(event) {
        //     event.preventDefault();
        //     var noretur = $('#noretur').val();
        //     var tglretur = $('#tglretur').val();
        //     var noinvoice = $('#noinvoice').val();
        //     var nopelanggan = $('#nopelanggan').val();
        //     var notaretur = $('#notaretur').val();
        //     var tglnotaretur = $('#tglnotaretur').val();
        //     var nofakturpajak = $('#nofakturpajak').val();
        //     var tglfakturpajak = $('#tglfaktur').val();
        //     var discash = $('#discash').val();
        //     var discustomer = $('#discustomer').val();
        //     var nilaicash = $('#nilaicash').val();
        //     var nilaicustomer = $('#nilaicustomer').val();
        //     var total = $('#total').val();
        //     var dpp = $('#dpp').val();
        //     var ppn = $('#ppn').val();
        //     var grandtotal = $('#grandtotal').val();

        //     var datadetail = AmbilDataDetail();

        //     if (ValidasiSave(datadetail) == true) {
        //         $.ajax({
        //             url: "<?= base_url("inventory/ReturBarang/Update") ?>",
        //             method: "POST",
        //             dataType: "json",
        //             async: true,
        //             data: {
        //                 noretur: noretur,
        //                 tglretur: tglretur,
        //                 noinvoice: noinvoice,
        //                 nopelanggan: nopelanggan,
        //                 notaretur: notaretur,
        //                 tglnotaretur: tglnotaretur,
        //                 nofakturpajak: nofakturpajak,
        //                 tglfakturpajak: tglfakturpajak,
        //                 discash: discash,
        //                 discustomer: discustomer,
        //                 nilaicash: nilaicash,
        //                 nilaicustomer: nilaicustomer,
        //                 total: total,
        //                 dpp: dpp,
        //                 ppn: ppn,
        //                 grandtotal: grandtotal,
        //                 datadetail: datadetail
        //             },
        //             success: function(data) {
        //                 if (data.nomor != "") {
        //                     Toast.fire({
        //                         icon: 'success',
        //                         title: data.message
        //                     });
        //                     $('#noretur').val(data.nomor);
        //                     $('#tglretur').prop('disabled', true);
        //                     $('#noinvoice').prop('disabled', true);
        //                     $('#nopelanggan').prop('disabled', true);
        //                     $('#notaretur').prop('disabled', true);
        //                     $('#tglnotaretur').prop('disabled', true);
        //                     $('#nofakturpajak').prop('disabled', true);
        //                     $('#tglfakturpajak').prop('disabled', true);
        //                     $('#discash').prop('disabled', true);
        //                     $('#discustomer').prop('disabled', true);
        //                     $('#total').prop('disabled', true);
        //                     $('#dpp').prop('disabled', true);
        //                     $('#ppn').prop('disabled', true);
        //                     $('#grandtotal').prop('disabled', true);

        //                     $('#save').prop('disabled', true);
        //                     $('#update').prop('disabled', true);

        //                     $('#cariinvoice').hide();
        //                     $('#caribarang').hide();
        //                     $('#adddetail').hide();
        //                     $('.editdetail').hide();
        //                     $('.deldetail').hide();
        //                 } else {
        //                     Toast.fire({
        //                         icon: 'error',
        //                         title: data.message
        //                     });
        //                 }
        //             }
        //         }, false)
        //     }
        // })

        function DisableEnableFind(status) {
            $('#tanggalretur').prop('disabled', status);
            $('#cariinvoice').prop('disabled', status);
            $('#notaretur').prop('disabled', status);
            $('#tanggalnotaretur').prop('disabled', status);
            $('#nofakturpajak').prop('disabled', status);
            $('#tanggalfaktur').prop('disabled', status);
            $('#lokasigdg').prop('disabled', status);
            $('#caribarang').prop('disabled', status);
            $('#adddetail').prop('disabled', status);
            $('#tglfaktur').prop('disabled', status);
            $('#tglnotaretur').prop('disabled', status);
            $('#tglretur').prop('disabled', status);
            $('#pengembalian').prop('disabled', status);
            $('#tidak_pengembalian').prop('disabled', status);
        }

        document.getElementById("okcancel").addEventListener("click", function(event) {
            event.preventDefault();
            var noretur = $('#noretur').val();
            var lokasigdg = $('#lokasigdg').val();
            var tglretur = $('#tglretur').val();
            var noinvoice = $('#noinvoice').val();
            var nopelanggan = $('#nopelanggan').val();
            var keteranganbatal = $('#keteranganbatal').val();
            var pengembalian = $('input[name="pengembalian"]:checked').val();
            var grandtotal = $('#grandtotal').val();
            var datadetail = AmbilDataDetail();

            if (noretur == "") {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Silahkan Pilih No Sales Order yang diinginkan.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#noretur').focus();
            } else if (keteranganbatal == "") {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Keterangan Batal Belum diMasukkan.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#keteranganbatal').focus();
            } else {
                $.ajax({
                    url: "<?= base_url("inventory/ReturBarang/Cancel") ?>",
                    method: "POST",
                    dataType: "json",
                    async: true,
                    data: {
                        noretur: noretur,
                        lokasigdg: lokasigdg,
                        tglretur: tglretur,
                        noinvoice: noinvoice,
                        nopelanggan: nopelanggan,
                        keteranganbatal: keteranganbatal,
                        pengembalian: pengembalian,
                        grandtotal: grandtotal,
                        datadetail: datadetail
                    },
                    success: function(data) {
                        if (data.nomor != "") {
                            Toast.fire({
                                icon: 'success',
                                title: data.message
                            });
                            $('#noretur').val(data.nomor);
                            $('#tglretur').prop('disabled', true);
                            $('#noinvoice').prop('disabled', true);
                            $('#nopelanggan').prop('disabled', true);
                            $('#notaretur').prop('disabled', true);
                            $('#tglnotaretur').prop('disabled', true);
                            $('#nofakturpajak').prop('disabled', true);
                            $('#tglfakturpajak').prop('disabled', true);
                            $('#discash').prop('disabled', true);
                            $('#discustomer').prop('disabled', true);
                            $('#total').prop('disabled', true);
                            $('#dpp').prop('disabled', true);
                            $('#ppn').prop('disabled', true);
                            $('#grandtotal').prop('disabled', true);

                            $('#save').prop('disabled', true);
                            // $('#update').prop('disabled', true);

                            $('#cariinvoice').hide();
                            $('#caribarang').hide();
                            $('#adddetail').hide();
                            $('.editdetail').hide();
                            $('.deldetail').hide();
                            location.reload(true);
                        } else {
                            Toast.fire({
                                icon: 'error',
                                title: data.message
                            });
                        }

                    }
                }, false)
            }
        })

        document.getElementById('clear').addEventListener('click', function(event) {
            event.preventDefault();
            location.reload(true);
        })

        ClearScreen();

    })
</script>