<div class="col-12 col-xl-12 mb-2">
	<div class="modal-header" style="padding: 5px;">
		<div class="col-12 col-md-12">
			<div class="row">
				<div class="col-5 col-md-5">
					<h2>
						<span class="logo-menu"><?= $icons ?></span>
						<span class="text-uppercase"><?= $title ?></span>
					</h2>
				</div>
				<div class="col-7 col-md-7">
					<div class="form-group" style="float: right;">
						<button id="save" class="btn btn-sm btn-dark mb-1 mt-1 mr-1 ml-1" style="margin-bottom: 0px;"><i class="far fa-save" style="margin-right: 10px;"></i>Save</button>
						<!-- <button id="update" class="btn btn-sm btn-dark mb-1 mt-1 mr-1 ml-1" style="margin-bottom: 0px;"><i class="far fa-money-check-edit" style="margin-right: 10px;"></i>Update</button> -->
						<button data-toggle="modal" data-target="#findpacking" id="find" class="btn btn-sm btn-dark mb-1 mt-1 mr-1 ml-1" style="margin-bottom: 0px;"><i class="far fa-file-search" style="margin-right: 10px;"></i>Find</button>
						<button id="cancel" class="btn btn-sm btn-danger m-1" data-toggle="modal" data-target="#modalcancel"><i class="far fa-times-circle" style="margin-right: 10px;"></i>Cancel</button>
						<button id="clear" class="btn btn-sm btn-dark mb-1 mt-1 mr-1 ml-1" style="margin-bottom: 0px;"><i class="far fa-refresh" style="margin-right: 10px;"></i>Clear</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="col-md-12 table-responsive hidescroll" style="height: 555px; overflow-y: scroll;">
	<div class="row">
		<div class="col-12 col-md-12 col-lg-12 col-xl-6">
			<div class="modal-content mb-2">
				<div class="modal-header p-2">
					<h5 class="mb-0">DATA DOKUMEN</h5>
				</div>
				<div class="modal-body">
					<div class="row mb-2 mt-2">
						<div class="col-sm-6 input-group">
							<div class="col-sm-6 p-0 input-group-prepend">
								<span class="col-sm-12 input-group-text">Nomor Packing</span>
							</div>
							<input class="form-control" type="text" name="nopacking" id="nopacking" maxlength="50" placeholder="Nomor Packing" readonly required />
							<input class="form-control" type="hidden" name="cabang" id="cabang" maxlength="50" value="<?php echo $this->session->userdata('mycabang') ?>" readonly required />
							<input class="form-control" type="hidden" name="gruplogin" id="gruplogin" maxlength="50" value="<?php echo $this->session->userdata('mygrup') ?>" readonly required />
						</div>
						<div class="col-sm-6 input-group box">
							<div class="col-sm-6 p-0 input-group-prepend">
								<span class="col-sm-12 input-group-text">Tanggal Packing</span>
							</div>
							<input class="form-control" type="text" name="tanggalpacking" id="tanggalpacking" maxlength="50" value="<?= date('Y-m-d') ?>" readonly required />
							<div class="input-group-text btn-dark">
								<span class="input-group-addon">
									<i class="fa fa-calendar-alt text-white"></i>
								</span>
							</div>
						</div>
					</div>
					<div class="row mb-2">
						<div class="col-sm-6 input-group">
							<div class="col-sm-6 p-0 input-group-prepend">
								<span class="col-sm-12 input-group-text">Nomor SO</span>
							</div>
							<input class="form-control" type="text" name="nomorso" id="nomorso" maxlength="50" placeholder="Nomor SO" readonly required />
							<div class="input-group-text btn-dark" data-toggle="modal" data-target="#findso" id="cariso">
								<span class="input-group-addon">
									<i class="fa fa-search text-white"></i>
								</span>
							</div>
						</div>
						<div class="col-sm-6 input-group box">
							<div class="col-sm-6 p-0 input-group-prepend">
								<span class="col-sm-12 input-group-text">Tanggal SO</span>
							</div>
							<input class="form-control" type="text" name="tanggalso" id="tanggalso" maxlength="50" value="<?= date('Y-m-d') ?>" readonly required />
							<div class="input-group-text btn-dark">
								<span class="input-group-addon">
									<i class="fa fa-calendar-alt text-white"></i>
								</span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-12 col-md-12 col-lg-12 col-xl-12">
			<div class="modal-content mb-2">
				<div class="modal-header p-2">
					<h5 class="mb-0">DATA DETAIL DOKUMEN</h5>
				</div>
				<div class="modal-body">
					<div class="row my-3">
						<div class="table-responsive">
							<table id="t_detail" class="table table-bordered table-striped table-default mb-0 penomoran" style="width:100%;">
								<thead style="background-color: #68478D; color: #eee;">
									<tr style="line-height: 0.5 cm; ">
										<th style="text-align: center; ">
											No
										</th>
										<th style="text-align: center; ">
											Kode
										</th>
										<th style="text-align: center; ">
											Nama
										</th>
										<th style="text-align: center; ">
											Qty
										</th>
									</tr>
								</thead>
								<tbody class="tbody-scroll scroller" id="tb_detail"></tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="findpacking">
	<div class="modal-dialog modal-lg">
		<div class="modal-content" style="border-radius: 5px;">
			<div class="modal-header" style="padding: 10px; color: #000;">
				<h7 class="modal-title" style="margin-left: 5px;">Data Packing Barang</h7>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="table-responsive">
					<table id="t_packing" class="table table-bordered table-striped dt-responsive display nowrap" style="width:100%">
						<thead class="thead-dark">
							<tr>
								<th width="10">Action</th>
								<th width="150">Nomor Packing</th>
								<th width="150">Tanggal Packing</th>
								<th width="150">Nomor SO</th>
								<th width="150">Tanggal SO</th>
								<th width="150">Nama Toko</th>
								<th width="150">Status Surat Jalan</th>
							</tr>
						</thead>
						<tbody></tbody>

					</table>

				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="findbarang">
	<div class="modal-dialog modal-lg">
		<div class="modal-content" style="border-radius: 5px;">
			<div class="modal-header" style="padding: 10px; color: #000;">
				<h7 class="modal-title" style="margin-left: 5px;">Data Barang</h7>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="table-responsive">
					<table id="t_barang" class="table table-bordered table-striped dt-responsive display nowrap" style="width:100%">
						<thead class="thead-dark">
							<tr>
								<th>Action</th>
								<th>Kode</th>
								<th>Nama</th>
								<!-- <th>Merk</th>
								<th>Barcode 1</th>
								<th>Barcode 2</th>
								<th>Satuan</th> -->
							</tr>
						</thead>
						<tbody></tbody>

					</table>

				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="findso">
	<div class="modal-dialog modal-lg">
		<div class="modal-content" style="border-radius: 5px;">
			<div class="modal-header" style="padding: 10px; color: #000;">
				<h7 class="modal-title" style="margin-left: 5px;">Data Sales Order</h7>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="table-responsive">
					<table id="t_so" class="table table-bordered table-striped dt-responsive display nowrap" style="width:100%">
						<thead class="thead-dark">
							<tr>
								<th>Action</th>
								<th>Cetak Proforma</th>
								<th>Nomor SO</th>
								<th>Tanggal SO</th>
								<th>Jenis SO</th>
								<th>Nama Toko</th>
								<th>Nama Salesman</th>
								<th>Jenis Penjualan</th>
								<th>User Input</th>
								<th>Lokasi</th>
								<th>Racikan</th>
								<!-- <th>Keterangan</th> -->
							</tr>
						</thead>
						<tbody></tbody>
					</table>

				</div>
			</div>
			<div class="modal-footer p-2">
				<button type="button" class="btn btn-danger mb-0" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="modalcancel">
	<div class="modal-dialog modal-md">
		<div class="modal-content" style="border-radius: 5px;">
			<div class="modal-header" style="padding: 10px;">
				<h5 class="modal-title" style="margin-left: 5px; color: #f44336;"><i class="far fa-info-circle mr-1"></i>CANCEL DOKUMEN</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<center>
					<h5 class="modal-title"></i>Apakah Anda Yakin ?</h5>
				</center>
				<div class="row mb-2">
					<div class="col-sm-12 input-group box">
						<!-- <input class="col-sm-12 form-control" type="hidden" name="cancelnopr" id="cancelnopr" maxlength="50" placeholder="Nomor PR" readonly required/> -->
					</div>
				</div>
				<div class="row mb-2">
					<div class="col-sm-12 input-group box">
						<div class="col-sm-3 p-0 input-group-prepend">
							<span class="col-sm-12 input-group-text">Alasan Batal</span>
						</div>
						<textarea name="keteranganbatal" id="keteranganbatal" class="form-control" placeholder="Alasan Batal ..."></textarea>
					</div>
				</div>
			</div>
			<div class="modal-footer p-2">
				<button id="okcancel" type="button" class="btn btn-dark mb-0" data-dismiss="modal"><i class="far fa-check-circle mr-1"></i>Confirm</button>
			</div>
		</div>
	</div>
</div>