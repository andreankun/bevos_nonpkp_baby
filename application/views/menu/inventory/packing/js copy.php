<script type="text/javascript">
    $(document).ready(function() {

        /* Function ClearScreen */
        function ClearScreen() {
            $('#update').prop("disabled", true);
            $('#save').prop("disabled", false);
            $('#cancel').prop("disabled", true);

            $('#nopacking').val("");
            $('#tanggalpacking').val("<?= date('Y-m-d') ?>");
            $('#nomorso').val("");
            $('#tanggalso').val("<?= date('Y-m-d') ?>");
            $('#kodebrg').val("");
            $('#namabrg').val("");
            $('#keteranganbatal').val("");
            $('#qty').val(0).prop('disabled', true);
            // $('#qty').prop('disabled', false).val(0);

            $('#tb_detail').empty();

            $('#adddetail').show();
            $('#cariso').show();
            $('#caribarang').show();
        }

        /* Validasi Qty */
        $('#qty').keyup(function() {
            var qty = parseFloat($(this).val());
            var qty = parseFloat($('#qty').val());

            if (qty > qty) {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Qty tidak boleh lebih besar dari Qty SO.',
                    showCloseButton: true,
                    width: 350,
                });
            }
        })

        $('#cariso').click(function() {
            $('#kodebrg').val('');
            $('#namabrg').val('');
            $('#qty').val(0);
            // $('#qty').val(0);
            $('#tb_detail').empty();
        })

        /*Format Date*/
        function FormatDate(input) {
            var date = new Date(input);
            var yyyy = date.getFullYear();
            var mm = date.getMonth() + 1;
            var dd = date.getDate();
            var hrs = date.getHours();
            var mnt = date.getMinutes();


            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }
            if (hrs < 10) {
                hrs = '0' + hrs;
            }
            if (mnt < 10) {
                mnt = '0' + mnt;
            }
            return yyyy + '-' + mm + '-' + dd;
        };

        /* Validasi Save */
        function ValidasiSave(datadetail) {
            var nomorso = $('#nomorso').val();
            var tanggalso = $('#tanggalso').val();

            if (nomorso == '' || nomorso == 0) {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Nomor SO tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#nomorso').focus();
                var result = false;
            } else if (tanggalso == '' || tanggalso == 0) {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Tanggal SO tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#tanggalso').focus();
                var result = false;
            } else if (datadetail.length == 0 || datadetail.length == '0') {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Data Detail tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#kodebrg').focus();
                var result = false;
            } else {
                var result = true;
            }
            return result;
        }

        /* Validasi Data Add Detail */
        function ValidasiAddDetail(kodebarang) {
            var table = document.getElementById('t_detail');
            var kodebarang = $('#kodebrg').val();
            var namabarang = $('#namabrg').val();
            var qty = $('#qty').val();
            var qty = $('#qty').val();

            if (kodebarang == '' || kodebarang == 0) {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Kode Barang tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#kodebrg').focus();
                return "gagal";
            } else if (namabarang == '' || namabarang == 0) {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Nama Barang tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#namabrg').focus();
                return "gagal";
            } else if (qty == '' || qty == 0) {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Qty SO tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#qty').focus();
                return "gagal";
            } else {
                for (var r = 1, n = table.rows.length; r < n; r++) {
                    var string = "";
                    for (var c = 0, m = table.rows[r].cells.length; c < m; c++) {
                        if (c == 0) {
                            if (table.rows[r].cells[c].innerHTML == kodebarang) {
                                Swal.fire({
                                    title: 'Information',
                                    icon: 'info',
                                    html: 'Data ini sudah diinput',
                                    showCloseButton: true,
                                    width: 350,
                                });
                                return "gagal";
                            }
                        }
                    }
                }
            }
            return "sukses"
        }

        /* Declare Toast */
        const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000,
            timerProgressBar: true,
            didOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
            }
        });

        /* Cari Data Sales Order */
        document.getElementById("cariso").addEventListener("click", function(event) {
            event.preventDefault();
            const cabang = $('#cabang').val();
            $('#t_so').DataTable({
                "destroy": true,
                "searching": true,
                "processing": true,
                "serverSide": true,
                "lengthChange": true,
                "pageLength": 5,
                "lengthMenu": [5, 10, 25, 50],
                "order": [],
                // "scrollX": true,
                "ajax": {
                    "url": "<?= base_url('inventory/PackingBarang/CariDataSalesOrder'); ?>",
                    "method": "POST",
                    "data": {
                        nmtb: "cari_sotopacking",
                        field: {
                            nomor: "nomor",
                            tanggal: "tanggal",
                            namapelanggan: "namapelanggan",
                            namasalesman: "namasalesman",
                            jenisso: "jenisso",
                            jenisjual: "jenisjual",
                            lokasi: "lokasi",
                            jenisdisc: "jenisdisc"
                        },
                        sort: "nomor",
                        where: {
                            nomor: "nomor",
                            namapelanggan: "namapelanggan",
                            namasalesman: "namasalesman",
                            jenisjual: "jenisjual",
                            lokasi: "lokasi",
                            jenisdisc: "jenisdisc"
                        },
                        value: "kodecabang = '" + cabang + "'"
                        // value: "(nilaiuangmuka = grandtotal AND jenisjual = 1) OR jenisjual = 2"
                    },
                },
                "columnDefs": [{
                        "targets": 2,
                        "render": $.fn.dataTable.render.moment('YYYY-MM-DD HH:mm:ss', 'D MMM YY')
                    },
                    {
                        "targets": 5,
                        "data": "jenisjual",
                        "render": function(data, type, row, meta) {
                            return (row[5] == '1') ? "<p>Tunai</p>" : "<p>Kredit</p>"
                        }
                    }, {
                        "targets": 6,
                        "data": "jenisso",
                        "render": function(data, type, row, meta) {
                            return (row[6] == '1') ? "<p>Customer</p>" : "<p>Internal</p>"
                        }
                    }
                ]
            });
        })

        /*Get Data Sales Order*/
        $(document).on('click', ".searchso", function() {
            var nomor = $(this).attr("data-id");;
            $.ajax({
                url: "<?= base_url('inventory/PackingBarang/DataSalesOrder'); ?>",
                method: "POST",
                dataType: "json",
                async: false,
                data: {
                    nomor: nomor
                },
                success: function(data) {
                    for (var i = 0; i < data.length; i++) {
                        $('#nomorso').val(data[i].nomor.trim());
                        $('#tanggalso').val(FormatDate(data[i].tanggal.trim()));
                    }
                    GetDataSalesDetail(nomor);
                }
            }, false);
        });

        function GetDataSalesDetail(nomor) {
            $.ajax({
                url: "<?= base_url('inventory/PackingBarang/DataSalesDetail'); ?>",
                method: "POST",
                dataType: "json",
                async: false,
                data: {
                    nomor: nomor
                },
                success: function(data) {
                    console.log(data);
                    for (var i = 0; i < data.length; i++) {
                        var kodebarang = data[i].kodebarang.trim();
                        var namabarang = data[i].namabarang.trim();
                        var qty = data[i].qty;
                        InsertDataDetail(kodebarang, namabarang, qty);
                    }
                }
            }, false);
        };

        /*Cari Data Barang*/
        document.getElementById("caribarang").addEventListener("click", function(event) {
            event.preventDefault();

            var nomorso = $('#nomorso').val();
            $('#t_barang').DataTable({
                "destroy": true,
                "searching": true,
                "processing": true,
                "serverSide": true,
                "lengthChange": false,
                "pageLength": 5,
                "lengthMenu": [5, 25, 50, 100],
                "order": [],
                // "scrollX": true,
                "ajax": {
                    "url": "<?= base_url('inventory/PackingBarang/CariDataSODetail'); ?>",
                    "method": "POST",
                    "data": {
                        nmtb: "srvt_sodetail",
                        field: {
                            kodebarang: "kodebarang",
                            namabarang: "namabarang",
                            // namamerk: "namamerk",
                            // barcode1: "barcode1",
                            // barcode2: "barcode2",
                            // namasatuan: "namasatuan"
                        },
                        sort: "kodebarang",
                        where: {
                            kodebarang: "kodebarang",
                            namabarang: "namabarang",
                        },
                        value: "nomor = '" + nomorso + "' "
                    },
                }
            });
        });

        $(document).on('click', ".searchsodetail", function() {
            var nomorso = $('#nomorso').val();
            var kodebarang = $(this).attr("data-id");
            GetDataSODetail(nomorso, kodebarang);
        });

        function GetDataSODetail(nomorso, kodebarang) {
            $.ajax({
                url: "<?= base_url('inventory/PackingBarang/DataSODetail'); ?>",
                method: "POST",
                dataType: "json",
                async: false,
                data: {
                    nomorso: nomorso,
                    kodebarang: kodebarang
                },
                success: function(data) {
                    console.log(data);
                    for (var i = 0; i < data.length; i++) {
                        $('#kodebrg').val(data[i].kodebarang.trim());
                        $('#namabrg').val(data[i].namabarang.trim());
                        $('#qty').val(data[i].qty);
                    }
                }
            }, false);
        };

        $("#adddetail").click(function() {
            var kodebarang = $('#kodebrg').val();
            var namabarang = $('#namabrg').val();
            var qty = parseFloat($('#qty').val());
            if (ValidasiAddDetail(kodebarang) == "sukses") {
                var row = "";
                row =
                    '<tr id="' + kodebarang + '">' +
                    '<td nowrap>' + kodebarang + '</td>' +
                    '<td nowrap>' + namabarang + '</td>' +
                    '<td nowrap>' + qty + '</td>' +
                    // '<td nowrap>' + qty + '</td>' +
                    '<td nowrap style="text-align: center; width: 200px;">' +
                    '<button data-table="' + kodebarang + '" data-toggle="modal" data-target="#changeqty" class="editdetail btn btn-success m-1 p-1"><i class="fas fa-edit"></i> Edit</button>' +
                    '<button data-table="' + kodebarang + '" class="deldetail btn btn-danger m-1 p-1"><i class="fa fa-trash mr-1"></i> Delete</button>' +
                    '</td>' +
                    '</tr>';
                $('#tb_detail').append(row);

                $('#kodebrg').val("");
                $('#namabrg').val("");
                $('#qty').val(0);
                // $('#qty').val(0);
            }
        });

        $(document).on('click', '.editdetail', function() {
            var table = document.getElementById('t_detail');
            var tr = document.getElementById($(this).attr("data-table"));
            var td = tr.getElementsByTagName("td");
            for (var c = 0, m = table.rows[0].cells.length; c < m - 1; c++) {
                var chgth = table.rows[0].cells[c].innerHTML.replace(/ /g, '').replace(/\s/g, '').toLowerCase();
                var valuetd = td[c].innerHTML;
                $('#chg' + chgth).val(valuetd);
            }
        });

        $(document).on('click', '#changedetail', function() {
            // var kode = $('#chgkode').val();
            var kodebarang = $('#chgkode').val();
            var namabarang = $('#chgnama').val();
            var qty = $('#chgqty').val();
            // var qty = $('#chgqty').val();
            $('#' + kodebarang).remove();
            InsertDataDetail(kodebarang, namabarang, qty);
        });

        function InsertDataDetail(kodebarang, namabarang, qty) {
            var row = "";
            row =
                '<tr id="' + kodebarang + '">' +
                '<td nowrap>' + kodebarang + '</td>' +
                '<td nowrap>' + namabarang + '</td>' +
                '<td nowrap>' + qty + '</td>' +
                // '<td nowrap>' + qty + '</td>' +
                '<td nowrap style="text-align: center; width: 200px;">' +
                // '<button data-table="' + kodebarang + '" data-toggle="modal" data-target="#changeqty" class="editdetail btn btn-success m-1 p-1"><i class="fas fa-edit"></i> Edit</button>' +
                // '<button data-table="' + kodebarang + '" class="deldetail btn btn-danger m-1 p-1"><i class="fa fa-trash mr-1"></i> Delete</button>' +
                '</td>' +
                '</tr>';
            console.log(row);
            $('#tb_detail').append(row);
        };

        $(document).on('click', '.deldetail', function() {
            var id = $(this).attr("data-table");
            $('#' + id).remove();
        });

        function AmbilDataDetail() {
            var table = document.getElementById('t_detail');
            var arr2 = [];
            for (var r = 1, n = table.rows.length; r < n; r++) {
                var string = "";
                for (var c = 0, m = table.rows[r].cells.length; c < m - 1; c++) {
                    if (c == 0) {
                        string = "{" + table.rows[0].cells[c].innerHTML + " : '" + table.rows[r].cells[c].innerHTML + "'";
                    } else {
                        string = string + ", " + table.rows[0].cells[c].innerHTML + " : '" + table.rows[r].cells[c].innerHTML + "'";
                    }
                }
                string = string + "}";
                var obj = JSON.stringify(eval('(' + string + ')'));
                var arr = $.parseJSON(obj);
                arr2.push(arr);
            }
            return arr2;
        };

        /* Save */
        document.getElementById('save').addEventListener("click", function(event) {
            event.preventDefault();
            var nomor = $('#nopacking').val();
            var tanggal = $('#tanggalpacking').val();
            var nomorso = $('#nomorso').val();
            var tanggalso = $('#tanggalso').val();
            var datadetail = AmbilDataDetail();

            if (ValidasiSave(datadetail) == true) {
                $.ajax({
                    url: "<?= base_url("inventory/PackingBarang/Save") ?>",
                    method: "POST",
                    dataType: "json",
                    async: true,
                    data: {
                        nomor: nomor,
                        tanggal: tanggal,
                        nomorso: nomorso,
                        tanggalso: tanggalso,
                        datadetail: datadetail
                    },
                    success: function(data) {
                        if (data.nomor != "") {
                            Toast.fire({
                                icon: 'success',
                                title: data.message
                            });
                            $('#nopacking').val(data.nomor);
                            $('#tanggalpacking').prop('disabled', true);
                            $('#nomorso').prop('disabled', true);
                            $('#tanggalso').prop('disabled', true);
                            $('#qty').prop('disabled', true);

                            $('#update').prop("disabled", true);
                            $('#save').prop("disabled", true);

                            $('.deldetail').hide();
                            $('.editdetail').hide();

                            $('#adddetail').hide();

                            $('#cariso').hide();
                            $('#caribarang').hide();
                        } else {
                            Toast.fire({
                                icon: 'error',
                                title: data.message
                            });
                        }
                    }
                }, false)
            }
        })

        /*Cari Data Packing Barang*/
        document.getElementById("find").addEventListener("click", function(event) {
            event.preventDefault();
            $('#tb_detail').empty();
            $('#t_packing').DataTable({
                "destroy": true,
                "searching": true,
                "processing": true,
                "serverSide": true,
                "lengthChange": true,
                "pageLength": 5,
                "lengthMenu": [5, 10, 25, 50],
                "order": [],
                // "scrollX": true,
                "ajax": {
                    "url": "<?= base_url('inventory/PackingBarang/CariDataPackingBarang'); ?>",
                    "method": "POST",
                    "data": {
                        nmtb: "srvt_packingbarang",
                        field: {
                            nomor: "nomor",
                            tanggal: "tanggal",
                            nomorso: "nomorso",
                            tglso: "tglso",
                            status: "status"
                        },
                        sort: "nomor",
                        where: {
                            nomor: "nomor",
                            nomorso: "nomorso",
                        },
                        value: "batal = false AND kodecabang = '" + $("#cabang").val() + "'"
                    },
                },
                "columnDefs": [{
                    "targets": 2,
                    "render": $.fn.dataTable.render.moment('YYYY-MM-DD HH:mm:ss', 'D MMM YY'),
                }, {
                    "targets": 4,
                    "render": $.fn.dataTable.render.moment('YYYY-MM-DD HH:mm:ss', 'D MMM YY'),
                }, {
                    "targets": 5,
                    "data": "status",
                    "render": function(data, type, row, meta) {
                        return (row[5] == 't') ? '<p>Sudah Cetak Surat Jalan</p>' : '<p>Belum Cetak Surat Jalan</p>';
                    },
                }]
            });
        })

        $(document).on('click', ".searchpacking", function() {
            var nomor = $(this).attr("data-id");
            GetData(nomor);
            $('#save').prop('disabled', true);
            $('#cancel').prop('disabled', false);
        });

        function GetData(nomor) {
            $.ajax({
                url: "<?= base_url('inventory/PackingBarang/DataPackingBarang'); ?>",
                method: "POST",
                dataType: "json",
                async: false,
                data: {
                    nomor: nomor
                },
                success: function(data) {
                    for (var i = 0; i < data.length; i++) {
                        $('#nopacking').val(data[i].nomor.trim());
                        $('#tanggalpacking').val(FormatDate(data[i].tanggal.trim()));
                        $('#nomorso').val(data[i].nomorso.trim());
                        $('#tanggalso').val(FormatDate(data[i].tglso.trim()));
                    }
                    GetDataDetail(nomor);
                }
            });
        };

        function GetDataDetail(nomor) {
            $.ajax({
                url: "<?= base_url('inventory/PackingBarang/DataPackingBarangDetail'); ?>",
                method: "POST",
                dataType: "json",
                async: false,
                data: {
                    nomor: nomor
                },
                success: function(data) {
                    for (var i = 0; i < data.length; i++) {
                        // var kode = data[i].kode;
                        var kodebarang = data[i].kodebarang;
                        var namabarang = data[i].namabarang.trim();
                        var qty = data[i].qty.trim();
                        // var qty = data[i].qty.trim();

                        InsertDataDetail(kodebarang, namabarang, qty);
                    }
                }
            });
        };

        document.getElementById('okcancel').addEventListener("click", function(event) {
            event.preventDefault();
            var nomor = $('#nopacking').val();
            var keterangan = $('#keteranganbatal').val();
            var nomorso = $('#nomorso').val();

            var datadetail = AmbilDataDetail();
            if (nomor == "") {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Pilih Dulu Nomor Yang Ingin dibatalkan',
                    showCloseButton: true,
                    width: 350,
                });
            } else if (keterangan == "") {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Keterangan Batal Tidak Boleh Kosong',
                    showCloseButton: true,
                    width: 350,
                });
            } else {
                $.ajax({
                    url: "<?= base_url("inventory/PackingBarang/Cancel") ?>",
                    method: "POST",
                    dataType: "json",
                    async: true,
                    data: {
                        nomor: nomor,
                        nomorso: nomorso,
                        keterangan: keterangan,
                        datadetail: datadetail
                    },
                    success: function(data) {
                        if (data.nomor != "") {
                            Toast.fire({
                                icon: 'success',
                                title: data.message
                            });
                            location.reload(true);
                        } else {
                            Toast.fire({
                                icon: 'error',
                                title: data.message
                            });
                        }
                    }
                }, false)
            }
        })

        /* Clear */
        document.getElementById("clear").addEventListener("click", function(event) {
            event.preventDefault();
            location.reload(true);
        })

        ClearScreen();
    });
</script>