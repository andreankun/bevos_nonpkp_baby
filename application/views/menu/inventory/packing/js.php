<script type="text/javascript">
    $(document).ready(function() {

        /* Function ClearScreen */
        function ClearScreen() {
            $('#update').prop("disabled", true);
            $('#save').prop("disabled", false);
            $('#cancel').prop("disabled", true);

            $('#nopacking').val("");
            $('#tanggalpacking').val("<?= date('Y-m-d') ?>");
            $('#nomorso').val("");
            $('#tanggalso').val("<?= date('Y-m-d') ?>");
            $('#keteranganbatal').val("");

            $('#tb_detail').empty();

            $('#adddetail').show();
            $('#cariso').show();
        }

        $('#tanggalpacking').datepicker({
            format: "yyyy-mm-dd",
            autoclose: true,
            todayHighlight: true
        });

        /*Format Date*/
        function FormatDate(input) {
            var date = new Date(input);
            var yyyy = date.getFullYear();
            var mm = date.getMonth() + 1;
            var dd = date.getDate();
            var hrs = date.getHours();
            var mnt = date.getMinutes();


            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }
            if (hrs < 10) {
                hrs = '0' + hrs;
            }
            if (mnt < 10) {
                mnt = '0' + mnt;
            }
            return yyyy + '-' + mm + '-' + dd;
        };

        /* Format Rupiah */
        function FormatRupiah(angka, prefix) {
            if (!angka) {
                return '';
            }
            var vangka = angka.toString();
            var number_string = vangka.replace(/[^.\d]/g, '').replace(/[^\w\s]/gi, '').toString(),
                split = number_string.split('.'),
                sisa = split[0].length % 3,
                rupiah = split[0].substr(0, sisa),
                ribuan = split[0].substr(sisa).match(/\d{3}/gi);

            // tambahkan titik jika yang di input sudah menjadi angka ribuan
            if (ribuan) {
                separator = sisa ? ',' : '';
                rupiah += separator + ribuan.join(',');
            }

            rupiah = split[1] != undefined ? rupiah + '.' + split[1] : rupiah;
            return prefix == undefined ? rupiah : (rupiah ? rupiah : '');
        };

        /* Validasi Save */
        function ValidasiSave(datadetail) {
            var nomorso = $('#nomorso').val();
            var tanggalso = $('#tanggalso').val();

            if (nomorso == '' || nomorso == 0) {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Nomor SO tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#nomorso').focus();
                var result = false;
            } else if (tanggalso == '' || tanggalso == 0) {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Tanggal SO tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#tanggalso').focus();
                var result = false;
            } else if (datadetail.length == 0 || datadetail.length == '0') {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Data Detail tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#kodebrg').focus();
                var result = false;
            } else {
                var result = true;
            }
            return result;
        }

        /* Declare Toast */
        const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000,
            timerProgressBar: true,
            didOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
            }
        });

        /* Cari Data Sales Order */
        document.getElementById("cariso").addEventListener("click", function(event) {
            event.preventDefault();
            gruplogin
            if ($('#grup').val() == "HO1" || $('#grup').val() == "HO2" || $('#grup').val() == "SUPERADMIN" || $('#grup').val() == "KPSALES" || $('#grup').val() == "ADMSALES") {
                values = "kodecabang = '" + $('#cabang').val() + "' AND kodestatus < 1"
            } else if ($('#grup').val() == "GUDANG JKT") {
                values = "kodecabang = '" + $('#cabang').val() + "' AND lokasi = '" + $('#gudang').val() + "' AND kodestatus < 1"
            } else {
                values = "kodecabang = '" + $('#cabang').val() + "' AND gudang = true AND kodestatus < 1"
            }

            $('#tb_detail').empty();
            $('#t_so').DataTable({
                "language": {
                    "url": "<?= base_url() ?>assets/json/id.json"
                },
                "destroy": true,
                "searching": true,
                "processing": true,
                "serverSide": true,
                "lengthChange": true,
                "pageLength": 5,
                "lengthMenu": [5, 10, 25, 50],
                "order": [],
                "ajax": {
                    "url": "<?= base_url('inventory/PackingBarang/CariDataSalesOrder'); ?>",
                    "method": "POST",
                    "data": {
                        nmtb: "cari_sotopacking",
                        field: {
                            nomor: "nomor",
                            tanggal: "tanggal",
                            jenisjual: "jenisjual",
                            namapelanggan: "namapelanggan",
                            namasalesman: "namasalesman",
                            jenisso: "jenisso",
                            user_so: "user_so",
                            nama_lokasi: "nama_lokasi",
                            nama_promo: "nama_promo"
                        },
                        sort: "nomor",
                        where: {
                            nomor: "nomor",
                            user_so: "user_so",
                            namapelanggan: "namapelanggan",
                            namasalesman: "namasalesman",
                            jenisjual: "jenisjual",
                            nama_lokasi: "nama_lokasi",
                            nama_promo: "nama_promo",
                            jenisjual: "jenisjual"
                        },
                        value: values
                        // value: "(nilaiuangmuka = grandtotal AND jenisjual = 1) OR jenisjual = 2"
                    },
                },
                "columnDefs": [{
                        "targets": 3,
                        "render": $.fn.dataTable.render.moment('YYYY-MM-DD HH:mm:ss', 'D MMM YY')
                    }, {
                        "targets": 4,
                        "data": "jenisjual",
                        "render": function(data, type, row, meta) {
                            return (row[4] == "1") ? "Tunai" : (row[4] == "2") ? "Kredit" : (row[4] == "3") ? "COD" : "Tunai 0%"
                        }
                    },
                    {
                        "targets": 7,
                        "data": "jenisso",
                        "render": function(data, type, row, meta) {
                            return (row[7] == '1') ? "<p>Customer</p>" : "<p>Internal</p>"
                        }
                    }
                ]
            });
        })

        /*Get Data Sales Order*/
        $(document).on('click', ".searchso", function() {
            var nomor = $(this).attr("data-id");
            $.ajax({
                url: "<?= base_url('inventory/PackingBarang/DataSalesOrder'); ?>",
                method: "POST",
                dataType: "json",
                async: false,
                data: {
                    nomor: nomor
                },
                success: function(data) {
                    for (var i = 0; i < data.length; i++) {
                        $('#nomorso').val(data[i].nomor.trim());
                        $('#tanggalso').val(FormatDate(data[i].tanggal.trim()));
                    }
                    GetDataSalesDetail(nomor);
                }
            }, false);
        });

        $(document).on('click', ".cetakproforma", function() {
            var nomor = $(this).attr("data-id");
            Cetak(nomor);
            CetakProformaUpdate(nomor);
        });

        function Cetak(nomor) {
            // console.log(nomor);
            window.open("<?= base_url() ?>cetak_pdf/SalesOrder/Print/" + nomor);
        }

        function CetakProformaUpdate(nomor) {
            $.ajax({
                url: "<?= base_url("sales/SalesOrder/CetakProformaUpdate") ?>",
                method: "POST",
                dataType: "json",
                async: true,
                data: {
                    nomor: nomor
                },
                success: function(data) {
                    // if (data.nomor != "") {
                    //     Toast.fire({
                    //         icon: 'success',
                    //         title: data.message
                    //     });
                    // } else {
                    //     Toast.fire({
                    //         icon: 'error',
                    //         title: data.message
                    //     });
                    // }
                }
            }, false)
        }

        function GetDataSalesDetail(nomor) {
            $.ajax({
                url: "<?= base_url('inventory/PackingBarang/DataSalesDetail'); ?>",
                method: "POST",
                dataType: "json",
                async: false,
                data: {
                    nomor: nomor
                },
                success: function(data) {
                    console.log(data);
                    for (var i = 0; i < data.length; i++) {
                        var kodebarang = data[i].kodebarang.trim();
                        var namabarang = data[i].namabarang.trim();
                        var qty = data[i].qty;
                        var nomor = "";
                        myFunction(nomor, kodebarang, namabarang, qty);
                    }
                }
            }, false);
        };

        function myFunction(nomor, kodebarang, namabarang, qty) {
            var table = document.getElementById('t_detail').getElementsByTagName('tbody')[0];
            var row = table.insertRow(0);
            var kodepencarian = kodebarang.replace(/[^a-zA-Z0-9 ]/g, '');
            row.setAttribute("id", kodepencarian);
            // console.log(row)
            var cell1 = row.insertCell(0);
            var cell2 = row.insertCell(1);
            var cell3 = row.insertCell(2);
            var cell4 = row.insertCell(3);
            // var cell3 = '<button data-table="' + RejectSpecialChar(kodebarang) + '" class="deldetail btn btn-danger m-1 p-1"><i class="fa fa-trash mr-1"></i> Delete</button>';
            cell1.innerHTML = nomor;
            cell2.innerHTML = kodebarang;
            cell3.innerHTML = namabarang;
            cell4.innerHTML = FormatRupiah(qty);
            // cell1.style.display = 'none';
            // row.innerHTML = kodebarang;

            var tbody = $('#t_detail').find('tbody');
            var newRows = tbody.find('tr').sort(function(a, b) {
                return $('td:eq(2)', a).text().localeCompare($('td:eq(2)', b).text());
            });
            console.log(newRows);
            tbody.append(newRows);
        }

        function InsertDataDetail(kodebarang, namabarang, qty) {
            var row = "";
            row =
                '<tr id="' + kodebarang + '">' +
                '<td nowrap>' + kodebarang + '</td>' +
                '<td nowrap>' + namabarang + '</td>' +
                '<td nowrap>' + FormatRupiah(qty) + '</td>' +
                // '<td nowrap style="text-align: center; width: 200px;">' +
                // '<button data-table="' + kodebarang + '" data-toggle="modal" data-target="#changeqty" class="editdetail btn btn-success m-1 p-1"><i class="fas fa-edit"></i> Edit</button>' +
                // '<button data-table="' + kodebarang + '" class="deldetail btn btn-danger m-1 p-1"><i class="fa fa-trash mr-1"></i> Delete</button>' +
                // '</td>' +
                '</tr>';
            console.log(row);
            $('#tb_detail').append(row);
        };

        function AmbilDataDetail() {
            var table = document.getElementById('t_detail');
            var arr2 = [];
            for (var r = 1, n = table.rows.length; r < n; r++) {
                var string = "";
                for (var c = 0, m = table.rows[r].cells.length; c < m; c++) {
                    if (c == 0) {
                        string = "{" + table.rows[0].cells[c].innerHTML + " : '" + table.rows[r].cells[c].innerHTML + "'";
                    } else {
                        string = string + ", " + table.rows[0].cells[c].innerHTML + " : '" + table.rows[r].cells[c].innerHTML + "'";
                    }
                }
                string = string + "}";
                var obj = JSON.stringify(eval('(' + string + ')'));
                var arr = $.parseJSON(obj);
                arr2.push(arr);
            }
            return arr2;
        };

        /* Save */
        document.getElementById('save').addEventListener("click", function(event) {
            event.preventDefault();
            var nomor = $('#nopacking').val();
            var tanggal = $('#tanggalpacking').val();
            var nomorso = $('#nomorso').val();
            var tanggalso = $('#tanggalso').val();
            var datadetail = AmbilDataDetail();
            console.log(datadetail);

            if (ValidasiSave(datadetail) == true) {
                $('#save').hide();
                $.ajax({
                    url: "<?= base_url("inventory/PackingBarang/Save") ?>",
                    method: "POST",
                    dataType: "json",
                    async: true,
                    data: {
                        nomor: nomor,
                        tanggal: tanggal,
                        nomorso: nomorso,
                        tanggalso: tanggalso,
                        datadetail: datadetail
                    },
                    success: function(data) {
                        if (data.nomor != "") {
                            // Toast.fire({
                            //     icon: 'success',
                            //     title: data.message
                            // });
                            $('#nopacking').val(data.nomor);
                            $('#tanggalpacking').prop('disabled', true);
                            $('#nomorso').prop('disabled', true);
                            $('#tanggalso').prop('disabled', true);
                            $('#qty').prop('disabled', true);

                            $('#update').prop("disabled", true);
                            $('#save').prop("disabled", true).show();

                            $('.deldetail').hide();
                            $('.editdetail').hide();

                            $('#adddetail').hide();

                            $('#cariso').hide();
                            $('#caribarang').hide();
                            Swal.fire({
                                icon: 'success',
                                title: data.message,
                                text: data.nomor
                            })
                        } else {
                            // Toast.fire({
                            //     icon: 'error',
                            //     title: data.message
                            // });
                            $('#save').prop("disabled", false).show();
                            Swal.fire({
                                icon: 'error',
                                title: data.message,
                                // text: data.nomor
                            })
                        }
                    }
                }, false)
            }
        })

        /*Cari Data Packing Barang*/
        document.getElementById("find").addEventListener("click", function(event) {
            event.preventDefault();
            $('#tb_detail').empty();
            $('#t_packing').DataTable({
                "language": {
                    "url": "<?= base_url() ?>assets/json/id.json"
                },
                "destroy": true,
                "searching": true,
                "processing": true,
                "serverSide": true,
                "lengthChange": true,
                "pageLength": 5,
                "lengthMenu": [5, 10, 25, 50],
                "order": [],
                "ajax": {
                    "url": "<?= base_url('inventory/PackingBarang/CariDataPackingBarang'); ?>",
                    "method": "POST",
                    "data": {
                        nmtb: "cari_packing",
                        field: {
                            nomor: "nomor",
                            tanggal: "tanggal",
                            nomorso: "nomorso",
                            tglso: "tglso",
                            namapelanggan: "namapelanggan",
                            status: "status"
                        },
                        sort: "nomor",
                        where: {
                            nomor: "nomor",
                            nomorso: "nomorso",
                            namapelanggan: "namapelanggan",
                        },
                        value: "status = false AND batal = false AND kodecabang = '" + $('#cabang').val() + "'"
                    },
                },
                "columnDefs": [{
                    "targets": 2,
                    "render": $.fn.dataTable.render.moment('YYYY-MM-DD HH:mm:ss', 'D MMM YY'),
                }, {
                    "targets": 4,
                    "render": $.fn.dataTable.render.moment('YYYY-MM-DD HH:mm:ss', 'D MMM YY'),
                }, {
                    "targets": 6,
                    "data": "status",
                    "render": function(data, type, row, meta) {
                        return (row[6] == 't') ? '<p>Sudah Cetak Surat Jalan</p>' : '<p>Belum Cetak Surat Jalan</p>';
                    },
                }]
            });
        })

        $(document).on('click', ".searchpacking", function() {
            var nomor = $(this).attr("data-id");
            GetData(nomor);
            $('#save').prop('disabled', true);
            $('#cancel').prop('disabled', false);
            $('#cariso').hide();
        });

        function GetData(nomor) {
            $.ajax({
                url: "<?= base_url('inventory/PackingBarang/DataPackingBarang'); ?>",
                method: "POST",
                dataType: "json",
                async: false,
                data: {
                    nomor: nomor
                },
                success: function(data) {
                    for (var i = 0; i < data.length; i++) {
                        $('#nopacking').val(data[i].nomor.trim());
                        $('#tanggalpacking').val(FormatDate(data[i].tanggal.trim()));
                        $('#nomorso').val(data[i].nomorso.trim());
                        $('#tanggalso').val(FormatDate(data[i].tglso.trim()));
                    }
                    GetDataDetail(nomor);
                }
            });
        };

        function GetDataDetail(nomor) {
            $.ajax({
                url: "<?= base_url('inventory/PackingBarang/DataPackingBarangDetail'); ?>",
                method: "POST",
                dataType: "json",
                async: false,
                data: {
                    nomor: nomor
                },
                success: function(data) {
                    for (var i = 0; i < data.length; i++) {
                        // var kode = data[i].kode;
                        var kodebarang = data[i].kodebarang;
                        var namabarang = data[i].namabarang.trim();
                        var qty = data[i].qty.trim();
                        // var qty = data[i].qty.trim();
                        var nomor = "";
                        myFunction(nomor, kodebarang, namabarang, qty);
                    }
                }
            });
        };

        document.getElementById('okcancel').addEventListener("click", function(event) {
            event.preventDefault();
            var nomor = $('#nopacking').val();
            var keterangan = $('#keteranganbatal').val();
            var nomorso = $('#nomorso').val();

            var datadetail = AmbilDataDetail();
            if (nomor == "") {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Pilih Dulu Nomor Yang Ingin dibatalkan',
                    showCloseButton: true,
                    width: 350,
                });
            } else if (keterangan == "") {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Keterangan Batal Tidak Boleh Kosong',
                    showCloseButton: true,
                    width: 350,
                });
            } else {
                $.ajax({
                    url: "<?= base_url("inventory/PackingBarang/Cancel") ?>",
                    method: "POST",
                    dataType: "json",
                    async: true,
                    data: {
                        nomor: nomor,
                        nomorso: nomorso,
                        keterangan: keterangan,
                        datadetail: datadetail
                    },
                    success: function(data) {
                        if (data.nomor != "") {
                            Toast.fire({
                                icon: 'success',
                                title: data.message
                            });
                            location.reload(true);
                        } else {
                            Toast.fire({
                                icon: 'error',
                                title: data.message
                            });
                        }
                    }
                }, false)
            }
        })

        /* Clear */
        document.getElementById("clear").addEventListener("click", function(event) {
            event.preventDefault();
            location.reload(true);
        })

        ClearScreen();
    });
</script>