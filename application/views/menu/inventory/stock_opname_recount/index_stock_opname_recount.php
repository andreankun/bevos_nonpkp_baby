<div class="col-xl-12 mb-2">
	<div class="modal-header" style="padding: 5px;">
		<div class="col-xl-12">
			<div class="row">
				<div class="col-md-5">
					<h2>
						<span class="logo-menu"><?= $icons ?></span>
						<span class="text-uppercase"><?= $title ?></span>
					</h2>
				</div>
				<div class="col-md-7">
					<div class="form-group" style="float: right;">
						<button id="proses" class="btn btn-sm btn-dark mb-1 mt-1 mr-1 ml-1" style="margin-bottom: 0px;"><i class="far fa-save" style="margin-right: 10px;"></i>Proses</button>
						<button id="recount" class="btn btn-sm btn-dark mb-1 mt-1 mr-1 ml-1" style="margin-bottom: 0px;"><i class="far fa-save" style="margin-right: 10px;"></i>Recount</button>
						<button id="confirm" class="btn btn-sm btn-dark mb-1 mt-1 mr-1 ml-1" style="margin-bottom: 0px;"><i class="far fa-save" style="margin-right: 10px;"></i>Confrim</button>
						<button id="adjustsistem" class="btn btn-sm btn-dark mb-1 mt-1 ml-1" style="margin-bottom: 0px;"><i class="far fa-check-circle" style="margin-right: 10px;"></i>Adjust By sistem</button>
						<button id="denda" class="btn btn-sm btn-danger mb-1 mt-1 ml-1" style="margin-bottom: 0px;"><i class="far fa-check-circle" style="margin-right: 10px;"></i>Denda</button>
						<button data-toggle="modal" data-target="#findrecount" id="find" class="btn btn-sm btn-dark mb-1 mt-1 mr-1 ml-1" style="margin-bottom: 0px;"><i class="far fa-file-search" style="margin-right: 10px;"></i>Find</button>
						<button id="clear" class="btn btn-sm btn-dark mb-1 mt-1 mr-1 ml-1" style="margin-bottom: 0px;"><i class="far fa-refresh" style="margin-right: 10px;"></i>Clear</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="col-md-12 table-responsive hidescroll" style="height: 555px; overflow-y: scroll;">
	<div class="row">
		<div class="col-xs-6 col-lg-6 col-xl-6">
			<div class="modal-content mb-2">
				<div class="modal-header p-2">
					<h5 class="mb-0">DATA DOKUMEN</h5>
				</div>
				<div class="modal-body">
					<div class="row mb-2 mt-2">
						<div class="col-sm-6 input-group">
							<div class="col-sm-6 p-0 input-group-prepend">
								<span class="col-sm-12 input-group-text">Nomor Recount</span>
							</div>
							<input class="form-control" type="text" name="norecount" id="norecount" maxlength="50" placeholder="Nomor recount" readonly required />
							<input type="hidden" name="nomorsto" id="nomorsto">
						</div>
						<div class="col-sm-6 input-group box">
							<div class="col-sm-6 p-0 input-group-prepend">
								<span class="col-sm-12 input-group-text">Tanggal Recount</span>
							</div>
							<input class="form-control" type="text" name="tanggalrecount" id="tanggalrecount" maxlength="50" value="<?= date('d-m-Y') ?>" readonly required />
							<div class="input-group-text btn-dark">
								<span class="input-group-addon">
									<i class="fa fa-calendar-alt text-white"></i>
								</span>
							</div>
						</div>
					</div>
					<div class="row mb-2">
						<div class="col-sm-6 input-group">
							<div class="col-sm-6 p-0 input-group-prepend">
								<span class="col-sm-12 input-group-text">Kode Gudang</span>
							</div>
							<input type="text" name="kodegudang" class="form-control" id="kodegudang" maxlength="50" placeholder="Kode Gudang" required readonly>
							<div class="input-group-text btn-dark" data-toggle="modal" data-target="#findgudang" id="carigudang">
								<span class="input-group-addon">
									<i class="fa fa-search text-white"></i>
								</span>
							</div>
						</div>
						<div class="col-sm-6 input-group">
							<div class="col-sm-6 p-0 input-group-prepend">
								<span class="col-sm-12 input-group-text">Merk</span>
							</div>
							<input type="text" name="kodegudang" class="form-control" id="kodemerk" maxlength="50" placeholder="Kode Merk" required readonly>
							<div class="input-group-text btn-dark" data-toggle="modal" data-target="#findmerk" id="carimerk">
								<span class="input-group-addon">
									<i class="fa fa-search text-white"></i>
								</span>
							</div>
						</div>
					</div>
					<div class="row mb-2 mt-2">
						<div class="col-sm-6 input-group">
							<div class="col-sm-6 p-0 input-group-prepend">
								<span class="col-sm-12 input-group-text">Tanggal Awal</span>
							</div>
							<input class="form-control" type="text" name="tglawal" id="tglawal" maxlength="50" value="<?= date('Y-m-d') ?>" required />
						</div>
						<div class="col-sm-6 input-group box">
							<div class="col-sm-6 p-0 input-group-prepend">
								<span class="col-sm-12 input-group-text">Tanggal Akhir</span>
							</div>
							<input class="form-control" type="text" name="tglakhir" id="tglakhir" maxlength="50" value="<?= date('Y-m-d') ?>" required />
							<div class="input-group-text btn-dark" id="cari_data">
								<span class="input-group-addon">
									<i class="fa fa-gear text-white"></i>
								</span>
							</div>
						</div>
					</div>
					<div class="row mb-2">
						<div class="col-sm-12 input-group">
							<div class="col-sm-3 p-0 input-group-prepend">
								<span class="col-sm-12 input-group-text">Keterangan</span>
							</div>
							<textarea name="keterangan" id="keterangan" rows="1" class="form-control" placeholder="Keterangan"></textarea>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-xs-6 col-lg-6 col-xl-6">
			<div class="modal-content mb-2">
				<div class="modal-header p-2">
					<h5 class="mb-0">DATA DETAIL DOKUMEN</h5>
				</div>
				<div class="modal-body">
					<div class="row my-3">
						<div class="table-responsive">
							<table id="t_selisih" class="table table-bordered table-striped table-default mb-0 dt-responsive display nowrap" style="width:100%;">
								<thead style="background-color: #68478D; color: #eee;">
									<tr style="line-height: 0.5 cm; ">
										<!-- <th style="text-align: center;">
											Action
										</th> -->
										<th style="text-align: center; ">
											Nomor
										</th>
										<th style="text-align: center; ">
											Tanggal
										</th>
										<th style="text-align: center; ">
											Kode
										</th>
										<th style="text-align: center; ">
											Nama
										</th>
										<th style="text-align: center; ">
											Expired
										</th>
										<th style="text-align: center; ">
											Qty
										</th>
										<th style="text-align: center; ">
											QtySistem
										</th>
										<th style="text-align: center; ">
											Selisih
										</th>
										<th style="text-align: center; ">
											Keterangan
										</th>
									</tr>
								</thead>
								<tbody class="tbody-scroll scroller" id="tb_selisih">

								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-xs-6 col-lg-6 col-xl-6">
			<div class="modal-content mb-2">
				<div class="modal-header p-2">
					<h5 class="mb-0">DATA DETAIL DOKUMEN</h5>
				</div>
				<div class="modal-body">
					<div class="row my-3">
						<div class="table-responsive">
							<table id="t_detail" class="table table-bordered table-striped table-default mb-0  dt-responsive display nowrap" style="width:100%;">
								<thead style="background-color: #68478D; color: #eee;">
									<tr style="line-height: 0.5 cm; ">
										<!-- <th style="text-align: center;">
											Action
										</th> -->
										<th style="text-align: center; ">
											Nomor
										</th>
										<th style="text-align: center; ">
											Tanggal
										</th>
										<th style="text-align: center; ">
											Kode
										</th>
										<th style="text-align: center; ">
											Nama
										</th>
										<th style="text-align: center; ">
											Expired
										</th>
										<th style="text-align: center; ">
											Qty
										</th>
										<th style="text-align: center; ">
											QtySistem
										</th>
										<th style="text-align: center; ">
											Keterangan
										</th>
									</tr>
								</thead>
								<tbody class="tbody-scroll scroller" id="tb_detail">

								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="findopname">
	<div class="modal-dialog modal-lg">
		<div class="modal-content" style="border-radius: 5px;">
			<div class="modal-header" style="padding: 10px; color: #000;">
				<h7 class="modal-title" style="margin-left: 5px;">Data Stock Opname</h7>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="table-responsive">
					<table id="t_stockopname" class="table table-bordered table-striped  dt-responsive display nowrap" style="width:100%">
						<thead class="thead-dark">
							<tr>
								<th width="10">Action</th>
								<th width="150">Nomor Opname</th>
								<th width="150">Tanggal Opname</th>
								<th width="150">Keterangan</th>
							</tr>
						</thead>
						<tbody></tbody>

					</table>

				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="findmerk">
	<div class="modal-dialog modal-lg">
		<div class="modal-content" style="border-radius: 5px;">
			<div class="modal-header" style="padding: 10px; color: #000;">
				<h7 class="modal-title" style="margin-left: 5px;">Data Kategori</h7>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="table-responsive">
					<table id="t_merk" class="table table-bordered table-striped dt-responsive display nowrap" style="width:100%">
						<thead class="thead-dark">
							<tr>
								<th width="10">Action</th>
								<th width="150">Kode Kategori</th>
								<th width="150">Nama Kategori</th>
							</tr>
						</thead>
						<tbody></tbody>
					</table>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="findbarang">
	<div class="modal-dialog modal-lg">
		<div class="modal-content" style="border-radius: 5px;">
			<div class="modal-header" style="padding: 10px; color: #000;">
				<h7 class="modal-title" style="margin-left: 5px;">Data Barang</h7>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="table-responsive">
					<table id="t_barang" class="table table-bordered table-striped  dt-responsive display nowrap" style="width:100%">
						<thead class="thead-dark">
							<tr>
								<th width="10">Action</th>
								<th width="150">Kode</th>
								<th width="150">Nama</th>
								<th width="150">Qty</th>

							</tr>
						</thead>
						<tbody></tbody>

					</table>

				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="findrecount">
	<div class="modal-dialog modal-lg">
		<div class="modal-content" style="border-radius: 5px;">
			<div class="modal-header" style="padding: 10px; color: #000;">
				<h7 class="modal-title" style="margin-left: 5px;">Data Barang</h7>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="table-responsive">
					<table id="t_recount" class="table table-bordered table-striped  dt-responsive display nowrap" style="width:100%">
						<thead class="thead-dark">
							<tr>
								<th width="10">Action</th>
								<th width="150">Nomor</th>
								<th width="150">Tanggal</th>
								<th width="150">Keterangan</th>
							</tr>
						</thead>
						<tbody></tbody>

					</table>

				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="changeopname">
	<div class="modal-dialog modal-md">
		<div class="modal-content" style="border-radius: 5px;">
			<div class="modal-header" style="padding: 10px; color: #000;">
				<h7 class="modal-title" style="margin-left: 5px;">UBAH DETAIL</h7>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="row mb-2">
					<div class="col-sm-6 input-group">
						<div class="col-sm-6 p-0 input-group-prepend">
							<span class="col-sm-12 input-group-text">Nomor Opname</span>
						</div>
						<input class="form-control" type="text" name="chgnomor" id="chgnomor" maxlength="50" placeholder="Kode" readonly required />
					</div>
					<div class="col-sm-6 input-group box">
						<div class="col-sm-6 p-0 input-group-prepend">
							<span class="col-sm-12 input-group-text">Tanggal Opname</span>
						</div>
						<input class="form-control" type="text" name="chgtanggal" id="chgtanggal" maxlength="50" placeholder="Nama Gudang" required readonly />
					</div>
				</div>
				<div class="row mb-2">
					<div class="col-sm-6 input-group">
						<div class="col-sm-6 p-0 input-group-prepend">
							<span class="col-sm-12 input-group-text">Kode Barang</span>
						</div>
						<input class="form-control" type="text" name="chgkode" id="chgkode" maxlength="50" placeholder="Kode" readonly required />
					</div>
					<div class="col-sm-6 input-group box">
						<div class="col-sm-6 p-0 input-group-prepend">
							<span class="col-sm-12 input-group-text">Nama Barang</span>
						</div>
						<input class="form-control" type="text" name="chgnama" id="chgnama" maxlength="50" placeholder="Nama Gudang" required readonly />
					</div>
				</div>
				<div class="row mb-2">
					<div class="col-sm-6 input-group">
						<div class="col-sm-6 p-0 input-group-prepend">
							<span class="col-sm-6 input-group-text">Qty</span>
						</div>
						<input class="form-control" type="text" name="chgqty" id="chgqty" maxlength="50" placeholder="Harga Beli" required />

					</div>
					<div class="col-sm-6 input-group">
						<div class="col-sm-6 p-0 input-group-prepend">
							<span class="col-sm-6 input-group-text">Qty Sistem</span>
						</div>
						<input class="form-control" type="text" name="chgqtysistem" id="chgqtysistem" maxlength="50" placeholder="Harga Beli" required />

					</div>
				</div>
				<div class="row mb-2">
					<div class="col-sm-6 input-group">
						<div class="col-sm-6 p-0 input-group-prepend">
							<span class="col-sm-6 input-group-text">Expired</span></span>
						</div>
						<input class="form-control" type="text" name="chgexpired" id="chgexpired" maxlength="50" placeholder="Harga Beli" required />

					</div>
					<div class="col-sm-6 input-group">
						<div class="col-sm-6 p-0 input-group-prepend">
							<span class="col-sm-6 input-group-text">Keterangan</span>
						</div>
						<input class="form-control" type="text" name="chgketerangan" id="chgketerangan" maxlength="50" placeholder="Harga Beli" required />
						<div class="input-group-text btn-dark" id="changedetail" data-dismiss="modal">
							<span class="input-group-addon">
								<i class="fa fa-random" style="color: #fff;"></i>
							</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="findgudang">
	<div class="modal-dialog modal-lg">
		<div class="modal-content" style="border-radius: 5px;">
			<div class="modal-header" style="padding: 10px; color: #000;">
				<h7 class="modal-title" style="margin-left: 5px;">Data Gudang</h7>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="table-responsive">
					<table id="t_gudang" class="table table-bordered table-striped dt-responsive display nowrap" style="width:100%">
						<thead class="thead-dark">
							<tr>
								<th width="10">Action</th>
								<th width="150">Kode</th>
								<th width="150">Nama Gudang</th>
								<th width="150">Alamat</th>
							</tr>
						</thead>
						<tbody></tbody>

					</table>

				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
