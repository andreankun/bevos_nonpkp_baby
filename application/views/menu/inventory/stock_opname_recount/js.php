<script>
	$(document).ready(function() {
		function ClearScreen() {
			$('#norecount').prop('disabled', false).val("");
			$('#tanggalrecount').prop('disabled', false).val("<?= date('Y-m-d') ?>");
			$('#tglawal').prop('disabled', false).val("<?= date('Y-m-d') ?>");
			$('#tglakhir').prop('disabled', false).val("<?= date('Y-m-d') ?>");
			$('#keterangan').prop('disabled', false).val("");
			$('#noopname').prop('disabled', false).val("");
			$('#tanggalopname').prop('disabled', false).val("");
			$('#kodebarang').prop('disabled', false).val("");
			$('#kodegudang').prop('disabled', false).val("");
			$('#namabarang').prop('disabled', false).val("");
			$('#qty').prop('disabled', false).val("");
			$('#kodemerk').prop('disabled', false).val("");
			$('#qtysistem').prop('disabled', false).val("");
			$('#ketdetail').prop('disabled', false).val("");
			$('#t_detail').prop('disabled', false).val("");

			$('#update').prop('disabled', true);
			$('#save').prop('disabled', false);
			$('#find').prop('disabled', false);
			$('#confirm').hide();
			$('#recount').hide();
			$('#adjustsistem').hide();
			$('#denda').hide();
			$('#tb_detail').empty();
			$('#tb_selisih').empty();
			$('#cari_data').show();
		};

		$('#tanggalexpired').datepicker({
			format: "yyyy-mm-dd",
			autoclose: true,
			todayHighlight: true,
			startDate: new Date()
		});

		$('#chgexpired').datepicker({
			format: "yyyy-mm-dd",
			autoclose: true,
			todayHighlight: true,
			startDate: new Date()
		});

		$('#tglawal').datepicker({
			format: "yyyy-mm-dd",
			autoclose: true,
			todayHighlight: true,
			// startDate: new Date()
		});

		$('#tglakhir').datepicker({
			format: "yyyy-mm-dd",
			autoclose: true,
			todayHighlight: true,
			// startDate: new Date()
		});

		// untuk membuat simbol data jika data kurang dari 
		// data sistem maka qty bakal muncul simbol ( - ) pada angka
		function datasimbol(){
			var qty;
			var qtysistem
			if(qty < qtysistem){
				"-"+FormatRupiah(qty);
			} else {
				FormatRupiah(qty);
			}
		}

		function FormatDate(input) {
			var date = new Date(input);
			var yyyy = date.getFullYear();
			var mm = date.getMonth() + 1;
			var dd = date.getDate();
			var hrs = date.getHours();
			var mnt = date.getMinutes();


			if (dd < 10) {
				dd = '0' + dd;
			}
			if (mm < 10) {
				mm = '0' + mm;
			}
			if (hrs < 10) {
				hrs = '0' + hrs;
			}
			if (mnt < 10) {
				mnt = '0' + mnt;
			}
			return yyyy + '-' + mm + '-' + dd;
		};

		/* Format Rupiah */
		function FormatRupiah(angka, prefix) {
			if (!angka) {
				return '';
			}
			var vangka = angka.toString();
			var number_string = vangka.replace(/[^.\d]/g, '').replace(/[^\w\s]/gi, '').toString(),
				split = number_string.split('.'),
				sisa = split[0].length % 3,
				rupiah = split[0].substr(0, sisa),
				ribuan = split[0].substr(sisa).match(/\d{3}/gi);

			// tambahkan titik jika yang di input sudah menjadi angka ribuan
			if (ribuan) {
				separator = sisa ? ',' : '';
				rupiah += separator + ribuan.join(',');
			}

			rupiah = split[1] != undefined ? rupiah + '.' + split[1] : rupiah;
			return prefix == undefined ? rupiah : (rupiah ? rupiah : '');
		};

		/* DeFormat Rupiah */
		function DeFormatRupiah(angka) {
			var result = angka.replace(/[^\w\s]/gi, '');

			return result;
		};

		/* Format Rupiah Saat Event Keyup */
		$('#qty').keyup(function() {
			$(this).val(FormatRupiah($(this).val()));
		})
		$('#qtysistem').keyup(function() {
			$(this).val(FormatRupiah($(this).val()));
		})


		function ValidasiSave(datadetail) {
			var tanggal = $('#tanggalrecount').val();
			var keterangan = $('#keterangan').val();

			if (tanggal == '' || tanggal == 0) {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Tanggal Recount tidak boleh kosong',
					showCloseButton: true,
					width: 350
				});
				$('#tanggalrecount').focus();
				var result = false;
			} else if (keterangan == '' || keterangan == 0) {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Keterangan recount tidak boleh kosong',
					showCloseButton: true,
					width: 350
				});
				$('#keterangan').focus();
				var result = false;
			} else {
				var result = true
			}
			return result;
		}

		function ValidasiAddDetail(datadetail) {
			var table = document.getElementById('t_detail');
			var noopname = $('#noopname').val();
			var tanggalopname = $('#tanggalopname').val();
			var kodebarang = $('#kodebarang').val();
			var namabarang = $('#namabarang').val();
			var qty = $('#qty').val();
			var qtysistem = $('#qtysistem').val();
			var expired = $('#tanggalexpired').val();
			var keterangan = $('#ketdetail').val();

			if (noopname == '' || noopname == 0) {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'No Opname tidak boleh kosong',
					showCloseButton: true,
					width: 350
				});
				$('#noopname').focus();
				var result = false;
			} else if (tanggalopname == '' || tanggalopname == "") {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Tanggal Opname tidak boleh kosong',
					showCloseButton: true,
					width: 350,
				});
				$('#tanggalopname').focus();
				var result = false;
			} else if (kodebarang == '' || kodebarang == 0) {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Kode Barang tidak boleh kosong',
					showCloseButton: true,
					width: 350
				});
				$('#kodebarang').focus();
				var result = false;
			} else if (namabarang == '' || namabarang == 0) {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Nama Barang tidak boleh kosong',
					showCloseButton: true,
					width: 350
				});
				$('#namabarang').focus();
				var result = false;
			} else if (qty == '' || qty == 0) {
				Swal.fire({
					title: 'Informas',
					icon: 'info',
					html: 'Quantity tidak boleh kosong',
					showCloseButton: true,
					width: 350
				});
				$('#qty').focus();
				var result = false;
			} else if (qtysistem == '' || qtysistem == 0) {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Quantity Sistem tidak boleh kosong',
					showCloseButton: true,
					width: 350,
				});
				$('#qtysistem').focus();
				var result = false;
			} else if (expired == '' || expired == 0) {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Tanggal Expired tidak boleh kosong',
					showCloseButton: true,
					width: 350
				});
				$('#tanggalexpired').focus();
				var result = false;
			} else if (keterangan == '' || keterangan == 0) {
				Swal.fire({
					title: 'Informasi',
					icon: 'info',
					html: 'Keterangan tidak boleh kosong',
					showCloseButton: true,
					width: 350
				});
				$('#ketdetail').focus();
				var result = false
			} else {
				for (var r = 1, n = table.rows.length; r < n; r++) {
					var string = "";
					for (var c = 0, m = table.rows[r].cells.length; c < m; c++) {
						if (c == 0) {
							if (table.rows[r].cells[c].innerHTML == kodebarang) {
								Swal.fire({
									title: 'Information',
									icon: 'info',
									html: 'Data ini sudah diinput',
									showCloseButton: true,
									width: 350,
								});
								return "gagal";
							}
						}
					}
				}
			}
			return "sukses";
		}

		const Toast = Swal.mixin({
			toast: true,
			position: 'top-end',
			showConfirmButton: false,
			timer: 3000,
			timerProgressBar: true,
			didOpen: (toast) => {
				toast.addEventListener('mouseenter', Swal.stopTimer)
				toast.addEventListener('mouseleave', Swal.resumeTimer)
			}
		});

		document.getElementById('carigudang').addEventListener("click", function(event) {
			event.preventDefault();
			$('#t_gudang').DataTable({
				"destroy": true,
				"searching": true,
				"processing": true,
				"serverSide": true,
				"lengthChange": true,
				"pageLength": 5,
				"lengthMenu": [5, 10, 25, 50],
				"order": [],
				"ajax": {
					"url": "<?= base_url('masterdata/Gudang/CariDataGudang'); ?>",
					"method": "POST",
					"data": {
						nmtb: "glbm_gudang",
						field: {
							kode: "kode",
							nama: "nama",
							alamat: "alamat"
						},
						sort: "kode",
						where: {
							kode: "kode"
						},
						value: "aktif = true AND kodecabang = '" + $('#cabang').val() + "'"
					},
				}
			});
		});

		$(document).on('click', '.searchgudang', function() {
			var kode = $(this).attr("data-id");
			$.ajax({
				url: "<?= base_url('masterdata/Gudang/DataGudang') ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					kode: kode
				},
				success: function(data) {
					for (var i = 0; i < data.length; i++) {
						$('#kodegudang').val(data[i].kode.trim());


					}
				}
			});
		});

		function AmbilDataDetail() {
			var table = document.getElementById('t_detail');
			var arr2 = [];
			for (var r = 1, n = table.rows.length; r < n; r++) {
				var string = "";
				for (var c = 0, m = table.rows[r].cells.length; c < m; c++) {
					if (c == 0) {
						string = "{" + table.rows[0].cells[c].innerHTML + " : '" + table.rows[r].cells[c].innerHTML + "'";
					} else {
						string = string + ", " + table.rows[0].cells[c].innerHTML + " : '" + table.rows[r].cells[c].innerHTML + "'";
					}
				}
				string = string + "}";
				var obj = JSON.stringify(eval('(' + string + ')'));
				var arr = $.parseJSON(obj);
				arr2.push(arr);
			}
			return arr2;
		}

		function AmbilDataSelisih() {
			var table = document.getElementById('t_selisih');
			var arr2 = [];
			for (var r = 1, n = table.rows.length; r < n; r++) {
				var string = "";
				for (var c = 0, m = table.rows[r].cells.length; c < m - 1; c++) {
					if (c == 0) {
						string = "{" + table.rows[0].cells[c].innerHTML + " : '" + table.rows[r].cells[c].innerHTML + "'";
					} else {
						string = string + ", " + table.rows[0].cells[c].innerHTML + " : '" + table.rows[r].cells[c].innerHTML + "'";
					}
				}
				string = string + "}";
				var obj = JSON.stringify(eval('(' + string + ')'));
				var arr = $.parseJSON(obj);
				arr2.push(arr);
			}
			return arr2;
		}

		// $('#adddetail').click(function() {
		// 	var nomorsto = $('#noopname').val();
		// 	var tanggal = $('#tanggalopname').val();
		// 	var qty = $('#qty').val();
		// 	var qtysistem = $('#qtysistem').val();
		// 	var kodebarang = $('#kodebarang').val();
		// 	var namabarang = $('#namabarang').val();
		// 	var expired = $('#tanggalexpired').val();
		// 	var ketdetail = $('#ketdetail').val();
		// 	if (ValidasiAddDetail(nomorsto) == "sukses") {
		// 		var row = "";
		// 		row =
		// 			'<tr id="' + nomorsto + '">' +
		// 			'<td nowrap>' + nomorsto + '</td>' +
		// 			'<td nowrap>' + FormatDate(tanggal) + '</td>' +
		// 			'<td nowrap>' + kodebarang + '</td>' +
		// 			'<td nowrap>' + namabarang + '</td>' +
		// 			'<td nowrap>' + expired + '</td>' +
		// 			'<td nowrap>' + qty + '</td>' +
		// 			'<td nowrap>' + qtysistem + '</td>' +
		// 			'<td nowrap>' + ketdetail + '</td>' +
		// 			'<td nowrap style="text-align: center; width: 200px;">' +
		// 			'<button data-table="' + nomorsto + '" data-toggle="modal" data-target="#changeopname" class="editdetail btn btn-sm btn-success m-1 p-1"><i class="fas fa-edit"></i> Edit</button>' +
		// 			'<button data-table="' + nomorsto + '" class="deldetail btn btn-sm btn-danger m-1 p-1"><i class="fa fa-trash mr-1"></i> Delete</button>' +
		// 			'</td>' +
		// 			'</tr>';

		// 		$('#tb_detail').append(row);
		// 		$('#noopname').val("");
		// 		$('#tanggalopname').val("");
		// 		$('#kodebarang').val("");
		// 		$('#namabarang').val("");
		// 		$('#qty').val("");
		// 		$('#qtysistem').val("");
		// 		$('#tanggalexpired').val("");
		// 		$('#ketdetail').val("");
		// 	}
		// });

		// $(document).on('click', '.editdetail', function() {
		//     var table = document.getElementById('t_detail');
		//     var tr = document.getElementById($(this).attr("data-table"));
		//     var td = tr.getElementsByTagName("td");
		//     for (var c = 0, m = table.rows[0].cells.length; c < m - 1; c++) {
		//         var chgth = table.rows[0].cells[c].innerHTML.replace(/ /g, '').replace(/\s/g, '').toLowerCase();
		//         var valuetd = td[c].innerHTML;
		//         $('#chg' + chgth).val(valuetd);
		//     }
		// });

		// $(document).on('click', '#changedetail', function() {
		// 	var nomorsto = $('#chgnomor').val();
		// 	var tanggalsto = $('#chgtanggal').val();
		// 	var kodebarang = $('#chgkode').val();
		// 	var namabarang = $('#chgnama').val();
		// 	var qty = $('#chgqty').val();
		// 	var qtysistem = $('#chgqtysistem').val();
		// 	var expired = $('#chgexpired').val();
		// 	var ketdetail = $('#chgketerangan').val();
		// 	$('#' + nomorsto).remove();
		// 	InsertDataDetail(nomorsto, tanggalsto, kodebarang, namabarang, qty, qtysistem, expired, ketdetail);
		// });

		function InsertDataDetail(nomorsto, tanggalsto, kodebarang, namabarang, qty, qtysistem, expired, ketdetail) {
			var row = "";
			row =
				'<tr id="' + nomorsto + '">' +
				'<td nowrap>' + nomorsto + '</td>' +
				'<td nowrap>' + FormatDate(tanggalsto) + '</td>' +
				'<td nowrap>' + kodebarang + '</td>' +
				'<td nowrap>' + namabarang + '</td>' +
				'<td nowrap>' + FormatDate(expired) + '</td>' +
				'<td nowrap>' + FormatRupiah(qty) + '</td>' +
				'<td nowrap>' + FormatRupiah(qtysistem) + '</td>' +
				'<td nowrap>' + ketdetail + '</td>' +

				'</tr>';
			$('#tb_detail').append(row);
		};

		function InsertDataSelisih(nomorsto, tanggalsto, kodebarang, namabarang, qty, qtysistem, selisih, expired, ketdetail) {
			var row = "";
			var selisihmin
			if(Number(qty) < Number(qtysistem)){
				 selisihmin = "-" + FormatRupiah(selisih);
			} else {
				 selisihmin = "+"+FormatRupiah(selisih);
			}
			row =
				'<tr id="' + nomorsto + '">' +
				'<td nowrap>' + nomorsto + '</td>' +
				'<td nowrap>' + FormatDate(tanggalsto) + '</td>' +
				'<td nowrap>' + kodebarang + '</td>' +
				'<td nowrap>' + namabarang + '</td>' +
				'<td nowrap>' + FormatDate(expired) + '</td>' +
				'<td nowrap>' + FormatRupiah(qty) + '</td>' +
				'<td nowrap>' + FormatRupiah(qtysistem) + '</td>' +
				'<td nowrap>' + selisihmin + '</td>' +
				'<td nowrap>' + ketdetail + '</td>' +
				'</tr>';
			$('#tb_selisih').append(row);
		};

		$(document).on('click', '.deldetail', function() {
			var id = $(this).attr("data-table");
			$('#' + id).remove();
		});

		function GetDataDetail(nomor) {
			$.ajax({
				url: "<?= base_url('inventory/StockOpnameRecount/DataStockRecountDetail'); ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					nomor: nomor
				},
				success: function(data) {

					for (var i = 0; i < data.length; i++) {
						var nomorsto = data[i].nomorsto.trim();
						var tanggalsto = FormatDate(data[i].tglsto.trim());
						var kodebarang = data[i].kodebarang.trim();
						var namabarang = data[i].namabarang.trim();
						var qty = FormatRupiah(data[i].qty.trim());
						var qtysistem = FormatRupiah(data[i].qtysistem.trim());
						var expired = FormatDate(data[i].tglexpired.trim());
						var ketdetail = data[i].keterangan.trim();

						InsertDataDetail(nomorsto, tanggalsto, kodebarang, namabarang, qty, qtysistem, expired, ketdetail);
					}
				}
			});
		}

		function DataSelisih(nomor) {
			$.ajax({
				url: "<?= base_url('inventory/StockOpnameRecount/DataStockRecountSelisih'); ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					nomor: nomor
				},
				success: function(data) {

					for (var i = 0; i < data.length; i++) {
						var nomorsto = data[i].nomorsto.trim();
						var tanggalsto = FormatDate(data[i].tglsto.trim());
						var kodebarang = data[i].kodebarang.trim();
						var namabarang = data[i].namabarang.trim();
						var qty = FormatRupiah(data[i].qty.trim());
						var qtysistem = FormatRupiah(data[i].qtysistem.trim());
						var selisih = FormatRupiah(data[i].selisih.trim());
						var expired = FormatDate(data[i].tglexpired.trim());
						var ketdetail = data[i].keterangan.trim();

						InsertDataSelisih(nomorsto, tanggalsto, kodebarang, namabarang, qty, qtysistem, selisih, expired, ketdetail);
					}
				}
			});
		}



		$(document).on("click", ".viewdata", function() {
			var nomor = $(this).attr("data-id");
			$.ajax({
				url: "<?= base_url('inventory/StockOpnameRecount/DataStockRecountSelisih') ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					nomor: nomor
				},
				success: function(data) {
					for (var i = 0; i < data.length; i++) {
						$('#nomor').val(data[i].nomor.trim());
						$('#chgkode').val(data[i].kodebarang.trim());
						$('#chgnama').val(data[i].namabarang.trim());
						$('#chgqty').val(data[i].qty.trim());
						$('#chgqtysistem').val(data[i].qtysistem.trim());
						$('#chgselisih').val(data[i].selisih.trim());
					}
				}
			})
		});

		document.getElementById('proses').addEventListener("click", function(event) {
			event.preventDefault();
			var nomor = $('#norecount').val();
			var tanggal = $('#tanggalrecount').val();
			var kodegudang = $('#kodegudang').val();
			var keterangan = $('#keterangan').val();
			var datadetail = AmbilDataDetail();
			if (ValidasiSave(datadetail) == true) {
				$.ajax({
					url: "<?= base_url('inventory/StockOpnameRecount/Proses') ?>",
					method: "POST",
					dataType: "json",
					asybc: true,
					data: {
						nomor: nomor,
						tanggal: tanggal,
						kodegudang: kodegudang,
						keterangan: keterangan,
						datadetail: datadetail
					},
					success: function(data) {
						if (data.nomor != "") {
							Toast.fire({
								icon: 'success',
								title: data.message
							});
							DataSelisih(data.nomor);
							$('#norecount').val(data.nomor);
							$('#proses').prop('disabled', true);
							$('#recount').show();
							$('#approve').show();
						} else {
							Toast.fire({
								icon: 'error',
								title: data.message
							});
						}
					}
				}, false);
			}
		});

				/* Cari Data Satuan*/
				document.getElementById("carimerk").addEventListener("click", function(event) {
			event.preventDefault();
			$('#t_merk').DataTable({
				"destroy": true,
				"searching": true,
				"processing": true,
				"serverSide": true,
				"lengthChange": true,
				"pageLength": 5,
				"lengthMenu": [5, 10, 25, 50],
				"order": [],
				"ajax": {
					"url": "<?= base_url('masterdata/Merk/CariDataMerk'); ?>",
					"method": "POST",
					"data": {
						nmtb: "glbm_merk",
						field: {
							kode: "kode",
							nama: "nama",
							// qtysatuan: "qtysatuan",
							// konversi: "konversi",
						},
						sort: "kode",
						where: {
							kode: "kode"
						},
						value: "aktif = true"
					},
				}
			});
		});

		/*Get Data Satuan */
		$(document).on('click', ".searchmerk", function() {
			var kode = $(this).attr("data-id");;
			$.ajax({
				url: "<?= base_url('masterdata/Merk/DataMerk'); ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					kode: kode
				},
				success: function(data) {
					for (var i = 0; i < data.length; i++) {
						$('#kodemerk').val(data[i].kode.trim());
						// $('#namamerk').val(data[i].nama.trim());
					}
				}
			}, false);
		});

		document.getElementById("cari_data").addEventListener("click", function(event) {
			event.preventDefault();
			var tglawal = $('#tglawal').val();
			var tglakhir = $('#tglakhir').val();
			var kodegudang = $('#kodegudang').val();
			var kodemerk = $('#kodemerk').val();
			var values = "";
			if(kodemerk == ""){
				values = "tanggal >= '" + tglawal + "' AND tanggal <= '" + tglakhir + "' AND kodegudang = '" + kodegudang + "' AND status = false AND batal = false AND status_proses = false ";
			} else {
				values = "tanggal >= '" + tglawal + "' AND tanggal <= '" + tglakhir + "' AND kodegudang = '" + kodegudang + "' AND status = false AND batal = false AND status_proses = false AND kodemerk = '"+kodemerk+"' ";
			}
			$('#t_detail').DataTable({
				"destroy": true,
				"searching": true,
				"processing": true,
				"serverSide": true,
				"lengthChange": false,
				"order": [],
				"ajax": {
					"url": "<?php echo base_url('inventory/StokOpname/CariDataStockOpnameTanggal'); ?>",
					"method": "POST",
					"data": {
						nmtb: "cari_approvesto",
						field: {
							nomor: "nomor",
							tanggal: "tanggal",
							kodebarang: "kodebarang",
							namabarang: "namabarang",
							expired: "expired",
							qty: "qty",
							qtysistem: "qtysistem",
							ketdetail: "ketdetail"
						},
						sort: "nomor",
						where: {
							nomor: "nomor"
						},
						value: values
					},
				},
				"columnDefs": [{
						"targets": 1,
						"render": $.fn.dataTable.render.moment('YYYY-MM-DD HH:mm:ss', 'D MMM YY')
					},
					{
						"targets": 4,
						"render": $.fn.dataTable.render.moment('YYYY-MM-DD HH:mm:ss', 'D MMM YY')

					},
					{
						"targets": 5,
						"render": $.fn.dataTable.render.number(',', '.')
					},
					{
						"targets": 6,
						"render": $.fn.dataTable.render.number(',', '.')
					}
				]
			});

			$('#confirm').show();
			$('#cari_data').hide();
			$('.seacrhopname').hide();

		}, false);



		document.getElementById('find').addEventListener("click", function(event) {
			event.preventDefault();
			$('#tb_detail').empty();
			$('#tb_selisih').empty();
			$('#t_recount').DataTable({
				"destroy": true,
				"searching": true,
				"processing": true,
				"serverSide": true,
				"lengthChange": true,
				"pageLength": 5,
				"lengthMenu": [5, 10, 25, 50],
				"order": [],
				"ajax": {
					"url": "<?= base_url('inventory/StockOpnameRecount/CariDataStockRecount'); ?>",
					"method": "POST",
					"data": {
						nmtb: "srvt_stockopnamerecount",
						field: {
							nomor: "nomor",
							tanggal: "tanggal",
							keterangan: "keterangan",
						},
						sort: "nomor",
						where: {
							nomor: "nomor"
						},
						value: "status = false AND status_management = false AND status_recount = false AND kodecabang = '" + $("#cabang").val() + "'"
					},
				}
			});
			$('#tb_detail').empty();
			$('#tb_selisih').empty();
		});



		$(document).on('click', '.searchstockrecount', function() {
			var nomor = $(this).attr("data-id");
			$.ajax({
				url: "<?= base_url('inventory/StockOpnameRecount/DataStockRecount') ?>",
				method: "POST",
				dataType: "json",
				async: false,
				data: {
					nomor: nomor
				},
				success: function(data) {
					for (var i = 0; i < data.length; i++) {
						$('#norecount').val(data[i].nomor.trim());
						$('#tanggalrecount').val(FormatDate(data[i].tanggal.trim()));
						$('#kodegudang').val(data[i].kodegudang.trim());
						$('#keterangan').val(data[i].keterangan.trim());
						$('#nomorsto').val(data[i].nomorsto.trim());
					}
					GetDataDetail(nomor);
					DataSelisih(nomor);
					$('#proses').prop('disabled', true);
					$('#confirm').show();
					$('#recount').show();
					$('#carigudang').hide();
				}
			});
		});

		document.getElementById('recount').addEventListener("click", function(event) {
			event.preventDefault();
			var nomor = $('#norecount').val();
			var tanggal = $('#tanggalrecount').val();
			var keterangan = $('#keterangan').val();
			var nomorsto = $('#nomorsto').val();
			var datadetail = AmbilDataDetail();

			if (ValidasiSave(datadetail) == true) {
				$.ajax({
					url: "<?= base_url("inventory/StockOpnameRecount/Recount") ?>",
					method: "POST",
					dataType: "json",
					async: true,
					data: {
						nomor: nomor,
						tanggal: tanggal,
						keterangan: keterangan,
						nomorsto: nomorsto,
						datadetail: datadetail
					},
					success: function(data) {
						if (data.nomor != "") {
							Toast.fire({
								icon: 'success',
								title: data.message
							});
							$('#norecount').prop('disabled', true),
								$('#tanggalrecount').prop('disabled', true),
								$('#keterangan').prop('disabled', true),

								$('#save').prop('disabled', true),
								$('#update').prop('disabled', true),

								$('#adddetail').hide()
						}
					}
				});
			}
		});

		document.getElementById('confirm').addEventListener("click", function(event) {
			event.preventDefault();
			var nomor = $('#norecount').val();
			var tanggal = $('#tanggalrecount').val();
			var keterangan = $('#keterangan').val();
			var nomorsto = $('#nomorsto').val();
			var datadetail = AmbilDataDetail();

			if (ValidasiSave(datadetail) == true) {
				$.ajax({
					url: "<?= base_url("inventory/StockOpnameRecount/Confirm") ?>",
					method: "POST",
					dataType: "json",
					async: true,
					data: {
						nomor: nomor,
						tanggal: tanggal,
						keterangan: keterangan,
						nomorsto: nomorsto,
						datadetail: datadetail
					},
					success: function(data) {
						if (data.nomor != "") {
							Toast.fire({
								icon: 'success',
								title: data.message
							});
							$('#norecount').prop('disabled', true),
								$('#tanggalrecount').prop('disabled', true),
								$('#keterangan').prop('disabled', true),

								$('#save').prop('disabled', true),
								$('#update').prop('disabled', true),

								$('#adddetail').hide()
						}
					}
				});
			}
		});

		document.getElementById("clear").addEventListener("click", function(event) {
			event.preventDefault();
			ClearScreen();
		});

		ClearScreen();
	});
</script>
