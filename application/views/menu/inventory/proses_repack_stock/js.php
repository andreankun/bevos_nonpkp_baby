<script text="text/javascript">
    $(document).ready(function() {

        function ClearScreen() {
            $('#nomorproses').val("");
            $('#tanggalproses').val("<?= date('Y-m-d') ?>");
            $('#nopenerimaan').val("");
            $('#kodebrg').val("");
            $('#namabrg').val("");
            $('#lokasicabang').val("");
            $('#lokasigdg').val("");
            $('#qty').val(0).prop('disabled', false);
            $('#keteranganbatal').val("");

            $('#carirepack').show();

            $('#tb_detail').empty();
            $('#tb_hasildetail').empty();

            $('#cancel').prop("disabled", true);
            $('#update').prop("disabled", true);
            $('#save').prop("disabled", false);
        }

        /* Cari Data Cabang*/
        // document.getElementById("carikodecbg").addEventListener("click", function(event) {
        //     event.preventDefault();
        //     $('#t_cabang').DataTable({
        //         "destroy": true,
        //         "searching": true,
        //         "processing": true,
        //         "serverSide": true,
        //         "lengthChange": true,
        //         "pageLength": 5,
        //         "lengthMenu": [5, 10, 25, 50],
        //         "order": [],
        //         // "scrollX": true,
        //         "ajax": {
        //             "url": "<?= base_url('masterdata/Cabang/CariDataCabang'); ?>",
        //             "method": "POST",
        //             "data": {
        //                 nmtb: "glbm_cabang",
        //                 field: {
        //                     kode: "kode",
        //                     nama: "nama",
        //                     alamat: "alamat"
        //                 },
        //                 sort: "kode",
        //                 where: {
        //                     kode: "kode",
        //                     nama: "nama",
        //                     alamat: "alamat"
        //                 },
        //                 value: "aktif = true"
        //             },
        //         }
        //     });
        // });

        /*Get Data Cabang*/
        $(document).on('click', ".searchcabang", function() {
            var kode = $(this).attr("data-id");;
            $.ajax({
                url: "<?= base_url('masterdata/Cabang/DataCabang'); ?>",
                method: "POST",
                dataType: "json",
                async: false,
                data: {
                    kode: kode
                },
                success: function(data) {
                    for (var i = 0; i < data.length; i++) {
                        $('#lokasicabang').val(data[i].kode.trim());
                    }
                }
            }, false);
        });

        /* Cari Data Gudang*/
        // document.getElementById("carikodegdg").addEventListener("click", function(event) {
        //     event.preventDefault();
        //     var kodecabang = $('#lokasicabang').val();
        //     $('#t_gudang').DataTable({
        //         "destroy": true,
        //         "searching": true,
        //         "processing": true,
        //         "serverSide": true,
        //         "lengthChange": true,
        //         "pageLength": 5,
        //         "lengthMenu": [5, 10, 25, 50],
        //         "order": [],
        //         // "scrollX": true,
        //         "ajax": {
        //             "url": "<?= base_url('masterdata/Gudang/CariDataGudang'); ?>",
        //             "method": "POST",
        //             "data": {
        //                 nmtb: "glbm_gudang",
        //                 field: {
        //                     kode: "kode",
        //                     nama: "nama",
        //                     alamat: "alamat",
        //                     kodecabang: "kodecabang",
        //                     status: "status"
        //                 },
        //                 sort: "kode",
        //                 where: {
        //                     kode: "kode",
        //                     nama: "nama",
        //                     alamat: "alamat",
        //                     kodecabang: "kodecabang",
        //                 },
        //                 value: "aktif = true AND kodecabang = '" + kodecabang + "'"
        //             },
        //         }
        //     });
        // });

        /*Get Data Gudang*/
        $(document).on('click', ".searchgudang", function() {
            var kode = $(this).attr("data-id");;
            $.ajax({
                url: "<?= base_url('masterdata/Gudang/DataGudang'); ?>",
                method: "POST",
                dataType: "json",
                async: false,
                data: {
                    kode: kode
                },
                success: function(data) {
                    for (var i = 0; i < data.length; i++) {
                        $('#lokasigdg').val(data[i].kode.trim());
                    }
                }
            }, false);
        });

        function setDateFormat(date) {
            dateObj = new Date(date);
            var day = DateObj.getDate();
            var month = dateObj.getMonth() + 1;
            var fullYear = DateObj.getFullYear().toString();
            var setformattedDate = '';
            setformattedDate = fullYear + '-' + getDigitToFormat(month) + '-' + getDigitToFormat(day);
            return setformattedDate;
        }

        function FormatDate(input) {
            var date = new Date(input);
            var yyyy = date.getFullYear();
            var mm = date.getMonth() + 1;
            var dd = date.getDate();
            var hrs = date.getHours();
            var mnt = date.getMinutes();


            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }
            if (hrs < 10) {
                hrs = '0' + hrs;
            }
            if (mnt < 10) {
                mnt = '0' + mnt;
            }
            return yyyy + '-' + mm + '-' + dd;
        };

        function getDigitToFormat(val) {
            if (val < 10) {
                val = '0' + val;
            }
            return val.toString();
        };

        function CurrentDate(input) {
            var date = new Date(input);
            var yyyy = date.getFullYear();
            var mm = date.getMonth() + 1;
            var dd = date.getDate();
            var hrs = date.getHours();
            var mnt = date.getMinutes();


            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }
            if (hrs < 10) {
                hrs = '0' + hrs;
            }
            if (mnt < 10) {
                mnt = '0' + mnt;
            }
            return yyyy + '' + mm;
        };

        /* Format Rupiah */
        function FormatRupiah(angka, prefix) {
            if (!angka) {
                return '';
            }
            var vangka = angka.toString();
            var number_string = vangka.replace(/[^.\d]/g, '').replace(/[^\w\s]/gi, '').toString(),
                split = number_string.split('.'),
                sisa = split[0].length % 3,
                rupiah = split[0].substr(0, sisa),
                ribuan = split[0].substr(sisa).match(/\d{3}/gi);

            // tambahkan titik jika yang di input sudah menjadi angka ribuan
            if (ribuan) {
                separator = sisa ? ',' : '';
                rupiah += separator + ribuan.join(',');
            }

            rupiah = split[1] != undefined ? rupiah + '.' + split[1] : rupiah;
            return prefix == undefined ? rupiah : (rupiah ? rupiah : '');
        };

        /* DeFormat Rupiah */
        function DeFormatRupiah(angka) {
            var result = angka.replace(/[^\w\s]/gi, '');

            return result;
        };

        const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000,
            timerProgressBar: true,
            didOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer);
                toast.addEventListener('mouseleave', Swal.resumeTimer);
            }
        });

        function InsertDataDetail(kodebarang, namabarang, qty) {

            var row = "";
            row =
                '<tr id="' + kodebarang + '">' +
                '<td nowrap>' + kodebarang + '</td>' +
                '<td nowrap>' + namabarang + '</td>' +
                '<td nowrap>' + FormatRupiah(qty) + '</td>' +
                '</tr>';
            $('#tb_detail').append(row);
        };

        function InsertDataHasilDetail(kodebarang, namabarang, qty) {

            var row = "";
            row =
                '<tr id="' + kodebarang + '">' +
                '<td nowrap>' + kodebarang + '</td>' +
                '<td nowrap>' + namabarang + '</td>' +
                '<td nowrap>' + FormatRupiah(qty) + '</td>' +
                '<td nowrap style="text-align: center; width: 200px;">' +
                '<button data-table="' + kodebarang + '" data-toggle="modal" data-target="#changeqty" class="editdetail btn btn-sm btn-success m-1 p-1"><i class="fas fa-edit"></i> Edit</button>' +
                '<button data-table="' + kodebarang + '" class="deldetail btn btn-sm btn-danger m-1 p-1"><i class="fa fa-trash mr-1"></i> Delete</button>' +
                '</td>' +
                '</tr>';
            $('#tb_hasildetail').append(row);
        };

        $('#qty').keyup(function() {
            $(this).val(FormatRupiah($(this).val()));
        })

        $('#chgqty').keyup(function() {
            $(this).val(FormatRupiah($(this).val()));
        })

        document.getElementById("caribarang").addEventListener("click", function(event) {
            event.preventDefault();
            var currentDate = CurrentDate(new Date());
            $('#t_barang').DataTable({
                "destroy": true,
                "searching": true,
                "processing": true,
                "serverSide": true,
                "lengthChange": false,
                "pageLength": 5,
                "lengthMenu": [5, 25, 50, 100],
                "order": [],
                "scrollX": true,
                "ajax": {
                    "url": "<?= base_url('inventory/ReqRepackStock/CariDataBarang'); ?>",
                    "method": "POST",
                    "data": {
                        nmtb: "cari_barang",
                        field: {
                            kodebarang: "kodebarang",
                            namabarang: "namabarang",
                            namasatuan: "namasatuan",
                            namamerk: "namamerk",
                            stock: "stock"
                        },
                        sort: "kodebarang",
                        where: {
                            kodebarang: "kodebarang",
                            namabarang: "namabarang",
                            namasatuan: "namasatuan",
                            namamerk: "namamerk"
                        },
                        value: "aktif = true AND kodecabang = '" + $('#lokasicabang').val() + "' AND kodegudang = '" + $('#lokasigdg').val() + "' AND periode = '" + currentDate + "'"
                    }
                },
                "columnDefs": [{
                    "targets": 5,
                    "data": "stock",
                    "render": function(data, type, row, meta) {
                        return FormatRupiah(row[5]);
                    }
                }]
            });
        });

        $(document).on('click', ".searchbarang", function() {
            var kode = $(this).attr("data-id");
            var kodegudang = $('#lokasigdg').val();
            $.ajax({
                url: "<?= base_url('inventory/ReqRepackStock/DataBarang'); ?>",
                method: "POST",
                dataType: "json",
                async: false,
                data: {
                    kode: kode,
                    kodegudang: kodegudang
                },
                success: function(data) {
                    console.log(data);
                    for (var i = 0; i < data.length; i++) {
                        $('#kodebrg').val(data[i].kodebarang.trim());
                        $('#namabrg').val(data[i].namabarang.trim());
                    }
                }
            }, false);
        });

        /* Validasi Data Add Detail */
        function ValidasiAddDetail(kodebarang) {
            var table = document.getElementById('t_hasildetail');

            var kodebarang = $('#kodebrg').val();
            var namabarang = $('#namabrg').val();
            var qty = $('#qty').val();

            if (kodebarang == '' || kodebarang == 0) {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Kode Barang tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#kodebarang').focus();
                return "gagal";
            } else if (namabarang == '' || namabarang == 0) {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Nama Barang tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#namabarang').focus();
                return "gagal";
            } else if (qty == '' || qty == 0) {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Qty tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                $('#qty').focus();
                return "gagal";
            } else {
                for (var r = 1, n = table.rows.length; r < n; r++) {
                    var string = "";
                    for (var c = 0, m = table.rows[r].cells.length; c < m; c++) {
                        if (c == 0) {
                            if (table.rows[r].cells[c].innerHTML == kodebarang) {
                                Swal.fire({
                                    title: 'Informasi',
                                    icon: 'info',
                                    html: 'Data ini sudah diinput.',
                                    showCloseButton: true,
                                    width: 350,
                                });
                                return "gagal";
                            }
                        }
                    }
                }
            }
            return "sukses"
        }

        function AmbilDataDetail() {
            var table = document.getElementById('t_detail');
            var arr2 = [];
            for (var r = 1, n = table.rows.length; r < n; r++) {
                var string = "";
                for (var c = 0, m = table.rows[r].cells.length; c < m; c++) {
                    if (c == 0) {
                        string = "{" + table.rows[0].cells[c].innerHTML + " : '" + table.rows[r].cells[c].innerHTML + "'";
                    } else {
                        string = string + ", " + table.rows[0].cells[c].innerHTML + " : '" + table.rows[r].cells[c].innerHTML + "'";
                    }
                }
                string = string + "}";
                var obj = JSON.stringify(eval('(' + string + ')'));
                var arr = $.parseJSON(obj);
                arr2.push(arr);
            }
            return arr2;
        };

        function AmbilDataHasilDetail() {
            var table = document.getElementById('t_hasildetail');
            var arr2 = [];
            for (var r = 1, n = table.rows.length; r < n; r++) {
                var string = "";
                for (var c = 0, m = table.rows[r].cells.length; c < m; c++) {
                    if (c == 0) {
                        string = "{" + table.rows[0].cells[c].innerHTML + " : '" + table.rows[r].cells[c].innerHTML + "'";
                    } else {
                        string = string + ", " + table.rows[0].cells[c].innerHTML + " : '" + table.rows[r].cells[c].innerHTML + "'";
                    }
                }
                string = string + "}";
                var obj = JSON.stringify(eval('(' + string + ')'));
                var arr = $.parseJSON(obj);
                arr2.push(arr);
            }
            return arr2;
        };

        function ValidasiSave(datadetail) {
            var tanggal = $('#tanggalproses').val();
            var nopenerimaan = $('#nopenerimaan').val();

            if (tanggal == '' || tanggal == 0) {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'icon',
                    html: 'Jenis jual Tidak Boleh Kosong',
                    showCloseButton: true,
                    width: 350,
                })
                $('#tanggal').focus();
                var result = false;
            } else if (nopenerimaan == '' || nopenerimaan == 0) {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'icon',
                    html: 'Nomor Penerimaan Tidak Boleh Kosong',
                    showCloseButton: true,
                    width: 350,
                })
                $('#nopenerimaan').focus();
                var result = false;
            } else if (datadetail.length == 0 || datadetail.length == '0') {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Data Detail tidak boleh kosong.',
                    showCloseButton: true,
                    width: 350,
                });
                var result = false;
            } else {
                var result = true;
            }
            return result;
        }

        document.getElementById("caripenerimaan").addEventListener("click", function(event) {
            event.preventDefault();
            $('#tb_detail').empty();
            $('#t_penerimaan').DataTable({
                "destroy": true,
                "searching": true,
                "processing": true,
                "serverSide": true,
                "lengthChange": true,
                "pageLength": 5,
                "lengthMenu": [5, 10, 25, 50],
                "order": [],
                "ajax": {
                    "url": "<?= base_url('inventory/ProsesRepackStock/CariDataPenerimaan'); ?>",
                    "method": "POST",
                    "data": {
                        nmtb: "srvt_penerimaanrepack",
                        field: {
                            nomor: "nomor",
                            tanggal: "tanggal"
                        },
                        sort: "nomor",
                        where: {
                            nomor: "nomor"
                        },
                        value: "proses = false"
                    },
                },
                "columnDefs": [{
                    "targets": 2,
                    "render": $.fn.dataTable.render.moment('YYYY-MM-DD HH:mm:ss', 'D MMM YY')
                }]
            });
        })

        $(document).on('click', ".searchprs", function() {
            var nomor = $(this).attr("data-id");
            GetDataPenerimaan(nomor);
        });

        function GetDataPenerimaan(nomor) {
            $.ajax({
                url: "<?= base_url('inventory/ProsesRepackStock/DataPenerimaan'); ?>",
                method: "POST",
                dataType: "json",
                async: false,
                data: {
                    nomor: nomor
                },
                success: function(data) {
                    console.log(data);
                    for (var i = 0; i < data.length; i++) {
                        $('#nopenerimaan').val(data[i].nomor.trim());
                        $('#lokasicabang').val(data[i].kodecabang.trim());
                        $('#lokasigdg').val(data[i].kodegudang.trim());
                    }
                    GetDataPenerimaanDetail(nomor);
                }
            });
        };

        function GetDataPenerimaanDetail(nomor) {
            $.ajax({
                url: "<?= base_url('inventory/ProsesRepackStock/DataPenerimaanDetail'); ?>",
                method: "POST",
                dataType: "json",
                async: false,
                data: {
                    nomor: nomor
                },
                success: function(data) {
                    for (var i = 0; i < data.length; i++) {
                        // var kode = data[i].kode;
                        var kodebarang = data[i].kodebarang.trim();
                        var namabarang = data[i].namabarang.trim();
                        // var jenis = data[i].jenis.trim();
                        var qty = data[i].qty.trim();

                        InsertDataDetail(kodebarang, namabarang, qty);
                    }
                }
            });
        };

        /* Save */
        document.getElementById('save').addEventListener("click", function(event) {
            event.preventDefault();
            var nomor = $('#nomorproses').val();
            var cabang = $('#lokasicabang').val();
            var gudang = $('#lokasigdg').val();
            var tanggal = $('#tanggalproses').val();
            var nopenerimaan = $('#nopenerimaan').val();

            var datadetail = AmbilDataDetail();
            var datahasildetail = AmbilDataHasilDetail();

            if (ValidasiSave(datadetail) == true) {
                $.ajax({
                    url: "<?= base_url("inventory/ProsesRepackStock/Save") ?>",
                    method: "POST",
                    dataType: "json",
                    async: true,
                    data: {
                        nomor: nomor,
                        cabang: cabang,
                        gudang: gudang,
                        tanggal: tanggal,
                        nopenerimaan: nopenerimaan,
                        datadetail: datadetail,
                        datahasildetail: datahasildetail,
                    },
                    success: function(data) {
                        if (data.nomor != "") {
                            Toast.fire({
                                icon: 'success',
                                title: data.message
                            });
                            $('#nomorproses').val(data.nomor);
                            $('#tanggalproses').prop("disabled", true);
                            $('#nopenerimaan').prop("disabled", true);

                            $('#carirepack').hide();

                            $('#update').prop("disabled", true);
                            $('#save').prop("disabled", true);
                        } else {
                            Toast.fire({
                                icon: 'error',
                                title: data.message
                            });
                        }
                    }
                }, false)
            }
        })

        document.getElementById("find").addEventListener("click", function(event) {
            event.preventDefault();
            $('#tb_detail').empty();
            $('#t_pr').DataTable({
                "destroy": true,
                "searching": true,
                "processing": true,
                "serverSide": true,
                "lengthChange": true,
                "pageLength": 5,
                "lengthMenu": [5, 10, 25, 50],
                "order": [],
                "ajax": {
                    "url": "<?= base_url('inventory/ProsesRepackStock/CariDataProses'); ?>",
                    "method": "POST",
                    "data": {
                        nmtb: "srvt_prosesrepact",
                        field: {
                            nomor: "nomor",
                            tanggal: "tanggal",
                            nopenerimaan: "nopenerimaan",
                        },
                        sort: "nomor",
                        where: {
                            nomor: "nomor"
                        },
                        values: "batal = false AND kodecabang = '" + $("#cabang").val() + "'"
                    },
                },
                "columnDefs": [{
                    "targets": 2,
                    "render": $.fn.dataTable.render.moment('YYYY-MM-DD HH:mm:ss', 'D MMM YY')
                }]
            });
        })

        $(document).on('click', ".searchproses", function() {
            var nomor = $(this).attr("data-id");
            GetData(nomor);
        });

        function GetData(nomor) {
            $.ajax({
                url: "<?= base_url('inventory/ProsesRepackStock/DataProses'); ?>",
                method: "POST",
                dataType: "json",
                async: false,
                data: {
                    nomor: nomor
                },
                success: function(data) {
                    console.log(data);
                    for (var i = 0; i < data.length; i++) {
                        $('#nomorproses').val(data[i].nomor.trim());
                        $('#tanggalproses').val(FormatDate(data[i].tanggal.trim()));
                        $('#nopenerimaan').val(data[i].nopenerimaan.trim());
                        if (data[i].kirim == 't') {
                            $('#cancel').prop('disabled', true);
                            $('#update').prop('disabled', true);
                            $('#save').prop('disabled', true);
                            $('#keterangan').prop('disabled', true);
                        } else {
                            $('#cancel').prop('disabled', false);
                            $('#update').prop('disabled', false);
                            $('#save').prop('disabled', true);
                        }
                    }
                    GetDataDetail(nomor);
                    GetDataHasilDetail(nomor);
                }
            });
        };

        function GetDataDetail(nomor) {
            $.ajax({
                url: "<?= base_url('inventory/ProsesRepackStock/DataProsesDetail'); ?>",
                method: "POST",
                dataType: "json",
                async: false,
                data: {
                    nomor: nomor
                },
                success: function(data) {
                    console.log(data);
                    for (var i = 0; i < data.length; i++) {
                        // var kode = data[i].kode;
                        var kodebarang = data[i].kodebarang.trim();
                        var namabarang = data[i].namabarang.trim();
                        // var jenis = data[i].jenis.trim();
                        var qty = data[i].qty.trim();

                        InsertDataDetail(kodebarang, namabarang, qty);
                    }
                }
            });
        };

        $("#adddetail").click(function() {
            var kodebarang = $('#kodebrg').val();
            var namabarang = $('#namabrg').val();
            var qty = $('#qty').val();

            if (ValidasiAddDetail(kodebarang) == 'sukses') {
                var row = "";
                row =
                    '<tr id="' + kodebarang + '">' +
                    '<td nowrap>' + kodebarang + '</td>' +
                    '<td nowrap>' + namabarang + '</td>' +
                    '<td nowrap>' + FormatRupiah(qty) + '</td>' +
                    '<td nowrap style="text-align: center; width: 200px;">' +
                    '<button data-table="' + kodebarang + '" data-toggle="modal" data-target="#changeqty" class="editdetail btn btn-sm btn-success mb-0 p-1"><i class="far fa-edit"></i> Edit</button>&nbsp;' +
                    '<button data-table="' + kodebarang + '" class="deldetail btn btn-sm btn-danger mb-0 p-1"><i class="fa fa-trash"></i> Delete</button>' +
                    '</td>' +
                    '</tr>';
                $('#tb_hasildetail').append(row);
                $('#kodebrg').val("");
                $('#namabrg').val("");
                $('#qty').val(0);
            }
        });

        $(document).on('click', '.deldetail', function() {
            var id = $(this).attr("data-table");
            $('#' + id).remove();
        });

        $(document).on('click', '.editdetail', function() {
            var table = document.getElementById('t_hasildetail');
            var tr = document.getElementById($(this).attr("data-table"));
            var td = tr.getElementsByTagName("td");
            for (var c = 0, m = table.rows[0].cells.length; c < m - 1; c++) {
                var chgth = table.rows[0].cells[c].innerHTML.replace(/ /g, '').replace(/\s/g, '').toLowerCase();
                var valuetd = td[c].innerHTML;
                $('#chg' + chgth).val(valuetd);
            }
        });

        $(document).on('click', '#changedetail', function() {
            // var kode = $('#chgkode').val(); 
            var kodebarang = $('#chgkode').val();
            var namabarang = $('#chgnama').val();
            var qty = $('#chgqty').val();
            $('#' + kodebarang).remove();
            InsertDataHasilDetail(kodebarang, namabarang, qty);
        });

        function GetDataHasilDetail(nomor) {
            $.ajax({
                url: "<?= base_url('inventory/ProsesRepackStock/DataHasilDetail'); ?>",
                method: "POST",
                dataType: "json",
                async: false,
                data: {
                    nomor: nomor
                },
                success: function(data) {
                    console.log(data);
                    for (var i = 0; i < data.length; i++) {
                        // var kode = data[i].kode;
                        var kodebarang = data[i].kodebarang.trim();
                        var namabarang = data[i].namabarang.trim();
                        // var jenis = data[i].jenis.trim();
                        var qty = data[i].qty.trim();

                        InsertDataHasilDetail(kodebarang, namabarang, qty);
                    }
                }
            });
        };

        /* Update */
        document.getElementById('update').addEventListener("click", function(event) {
            event.preventDefault();
            var nomor = $('#nomorproses').val();
            var cabang = $('#lokasicabang').val();
            var gudang = $('#lokasigdg').val();
            var tanggal = $('#tanggalproses').val();
            var nopenerimaan = $('#nopenerimaan').val();

            var datadetail = AmbilDataDetail();
            var datahasildetail = AmbilDataHasilDetail();

            if (ValidasiSave(datadetail) == true) {
                $.ajax({
                    url: "<?= base_url("inventory/ProsesRepackStock/Update") ?>",
                    method: "POST",
                    dataType: "json",
                    async: true,
                    data: {
                        nomor: nomor,
                        cabang: cabang,
                        gudang: gudang,
                        tanggal: tanggal,
                        nopenerimaan: nopenerimaan,
                        datadetail: datadetail,
                        datahasildetail: datahasildetail
                    },
                    success: function(data) {
                        if (data.nomor != "") {
                            Toast.fire({
                                icon: 'success',
                                title: data.message
                            });
                            $('#nomorproses').val(data.nomor);
                            $('#tanggalproses').prop("disabled", true);
                            $('#nopenerimaan').prop("disabled", true);

                            $('#carirepack').hide();

                            $('#update').prop("disabled", true);
                            $('#save').prop("disabled", true);
                        } else {
                            Toast.fire({
                                icon: 'error',
                                title: data.message
                            });
                        }
                    }
                }, false)
            }
        })

        document.getElementById('okcancel').addEventListener("click", function(event) {
            event.preventDefault();
            var nomor = $('#nomorproses').val();
            var keterangan = $('#keteranganbatal').val();
            var datadetail = AmbilDataDetail();
            if (nomor == "") {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Pilih Dulu Nomor Yang Ingin dibatalkan',
                    showCloseButton: true,
                    width: 350,
                });
            } else if (keterangan == "") {
                Swal.fire({
                    title: 'Informasi',
                    icon: 'info',
                    html: 'Keterangan Batal Tidak Boleh Kosong',
                    showCloseButton: true,
                    width: 350,
                });
            } else {
                $.ajax({
                    url: "<?= base_url("inventory/ProsesRepackStock/Cancel") ?>",
                    method: "POST",
                    dataType: "json",
                    async: true,
                    data: {
                        nomor: nomor,
                        keterangan: keterangan,
                        datadetail: datadetail
                    },
                    success: function(data) {
                        console.log(data);
                        if (data.nomor != "") {
                            Toast.fire({
                                icon: 'success',
                                title: data.message
                            });
                            // RefreshLayar();
                        } else {
                            Toast.fire({
                                icon: 'error',
                                title: data.message
                            });
                        }
                    }
                }, false)
            }
        })

        document.getElementById("clear").addEventListener("click", function(event) {
            event.preventDefault();
            ClearScreen();
        })

        ClearScreen();
    })
</script>