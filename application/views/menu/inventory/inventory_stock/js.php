<script type="text/javascript">
    $(document).ready(function() {

        /* Function ClearScreen */
        function ClearScreen() {
            // $('#periode').val('');
            $('#kodecabang').val('<?= $this->session->userdata('mycabang'); ?>');
            // $('#namacabang').val('');
            $('#kodegudang').val('<?= $this->session->userdata('mygudang'); ?>');
            // $('#namagudang').val('');
            $('#kodebarang').val('');
            $('#namabarang').val('');
            $('#qtyawal').val('');
            $('#qtymasuk').val('');
            $('#qtykeluar').val('');
            $('#qtypick').val('');
            $('#qtyakhir').val('');
            $('#carikodecbg').hide();
        }

        // $('#carikodecbg').click(function() {
        //     $('#kodegudang').val('');
        //     // $('#namagudang').val('');
        //     $('#kodebarang').val('');
        //     $('#namabarang').val('');
        //     $('#qtyawal').val('');
        //     $('#qtymasuk').val('');
        //     $('#qtykeluar').val('');
        //     $('#qtypick').val('');
        //     $('#qtyakhir').val('');
        // })

        $('#carikodegdg').click(function() {
            $('#kodebarang').val('');
            $('#namabarang').val('');
            $('#qtyawal').val('');
            $('#qtymasuk').val('');
            $('#qtykeluar').val('');
            $('#qtypick').val('');
            $('#qtyakhir').val('');
        })

        $('#carikodebrg').click(function() {
            $('#qtyawal').val('');
            $('#qtymasuk').val('');
            $('#qtykeluar').val('');
            $('#qtypick').val('');
            $('#qtyakhir').val('');
        })

        function CurrentDate(input) {
            var date = new Date(input);
            var yyyy = date.getFullYear();
            var mm = date.getMonth() + 1;
            var dd = date.getDate();
            var hrs = date.getHours();
            var mnt = date.getMinutes();


            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }
            if (hrs < 10) {
                hrs = '0' + hrs;
            }
            if (mnt < 10) {
                mnt = '0' + mnt;
            }
            return yyyy + '' + mm;
        };

        /* Cari Data Cabang*/
        document.getElementById("carikodecbg").addEventListener("click", function(event) {
            event.preventDefault();
            $('#t_cabang').DataTable({
                "destroy": true,
                "searching": true,
                "processing": true,
                "serverSide": true,
                "lengthChange": true,
                "pageLength": 5,
                "lengthMenu": [5, 10, 25, 50],
                "order": [],
                // "scrollX": true,
                "ajax": {
                    "url": "<?= base_url('masterdata/Cabang/CariDataCabang'); ?>",
                    "method": "POST",
                    "data": {
                        nmtb: "glbm_cabang",
                        field: {
                            kode: "kode",
                            nama: "nama",
                            alamat: "alamat"
                        },
                        sort: "kode",
                        where: {
                            kode: "kode",
                            nama: "nama",
                            alamat: "alamat"
                        },
                        value: "aktif = true"
                    },
                }
            });
        });

        /*Get Data Cabang*/
        $(document).on('click', ".searchcabang", function() {
            var kode = $(this).attr("data-id");;
            $.ajax({
                url: "<?= base_url('masterdata/Cabang/DataCabang'); ?>",
                method: "POST",
                dataType: "json",
                async: false,
                data: {
                    kode: kode
                },
                success: function(data) {
                    for (var i = 0; i < data.length; i++) {
                        $('#kodecabang').val(data[i].kode.trim());
                        $('#namacabang').val(data[i].nama.trim());
                    }
                }
            }, false);
        });

        /* Cari Data Gudang*/
        document.getElementById("carikodegdg").addEventListener("click", function(event) {
            event.preventDefault();
            var kodecabang = $('#kodecabang').val();
            $('#t_gudang').DataTable({
                "destroy": true,
                "searching": true,
                "processing": true,
                "serverSide": true,
                "lengthChange": true,
                "pageLength": 5,
                "lengthMenu": [5, 10, 25, 50],
                "order": [],
                // "scrollX": true,
                "ajax": {
                    "url": "<?= base_url('masterdata/Gudang/CariDataGudang'); ?>",
                    "method": "POST",
                    "data": {
                        nmtb: "glbm_gudang",
                        field: {
                            kode: "kode",
                            nama: "nama",
                            alamat: "alamat",
                            kodecabang: "kodecabang",
                            status: "status"
                        },
                        sort: "kode",
                        where: {
                            kode: "kode",
                            nama: "nama",
                            alamat: "alamat",
                            kodecabang: "kodecabang",
                        },
                        value: "aktif = true AND kodecabang = '" + kodecabang + "'"
                    },
                }
            });
        });

        /*Get Data Gudang*/
        $(document).on('click', ".searchgudang", function() {
            var kode = $(this).attr("data-id");;
            $.ajax({
                url: "<?= base_url('masterdata/Gudang/DataGudang'); ?>",
                method: "POST",
                dataType: "json",
                async: false,
                data: {
                    kode: kode
                },
                success: function(data) {
                    for (var i = 0; i < data.length; i++) {
                        $('#kodegudang').val(data[i].kode.trim());
                        $('#namagudang').val(data[i].nama.trim());
                    }
                }
            }, false);
        });

        /* Cari Data */
        document.getElementById("carikodebrg").addEventListener("click", function(event) {
            event.preventDefault();
            var kodecabang = $('#kodecabang').val();
            var kodegudang = $('#kodegudang').val();
            var currentDate = CurrentDate(new Date());
            $('#tb_detail').empty();
            $('#t_barang').DataTable({
                "destroy": true,
                "searching": true,
                "processing": true,
                "serverSide": true,
                "lengthChange": true,
                "pageLength": 5,
                "lengthMenu": [5, 10, 25, 50],
                "order": [],
                // "scrollX": true,
                "ajax": {
                    "url": "<?= base_url('inventory/InventoryStock/CariDataInventory'); ?>",
                    "method": "POST",
                    "data": {
                        nmtb: "cari_kartustok",
                        field: {
                            // periode: "periode",
                            kodebarang: "kodebarang",
                            namabarang: "namabarang",
                            kodegudang: "kodegudang",
                            namagudang: "namagudang",
                            kodecabang: "kodecabang",
                            namacabang: "namacabang",
                        },
                        sort: "periode",
                        where: {
                            // periode: "periode",
                            kodebarang: "kodebarang",
                            namabarang: "namabarang",
                            kodegudang: "kodegudang",
                            namagudang: "namagudang",
                            kodecabang: "kodecabang",
                            namacabang: "namacabang",
                        },
                        value: "periode = '" + currentDate + "' AND kodegudang = '" + kodegudang + "' AND kodecabang = '" + kodecabang + "'"
                    },
                }
            });
        });

        $(document).on('click', ".searchinventory", function() {
            var kodebarang = $(this).attr("data-id");
            var kodecabang = $('#kodecabang').val();
            var kodegudang = $('#kodegudang').val();
            GetData(kodebarang, kodecabang, kodegudang);
        });

        function GetData(kodebarang, kodecabang, kodegudang) {
            $.ajax({
                url: "<?= base_url('inventory/InventoryStock/DataInventory'); ?>",
                method: "POST",
                dataType: "json",
                async: false,
                data: {
                    kodebarang: kodebarang,
                    kodecabang: kodecabang,
                    kodegudang: kodegudang
                },
                success: function(data) {
                    for (var i = 0; i < data.length; i++) {
                        // $('#periode').val(data[i].periode.trim());
                        // $('#kodecabang').val(data[i].kodecabang.trim());
                        // $('#namacabang').val(data[i].namacabang.trim());
                        // $('#kodegudang').val(data[i].kodegudang.trim());
                        // $('#namagudang').val(data[i].namagudang.trim());
                        $('#kodebarang').val(data[i].kodebarang);
                        $('#namabarang').val(data[i].namabarang);
                        $('#qtyawal').val(data[i].qtyawal);
                        $('#qtymasuk').val(data[i].qtymasuk);
                        $('#qtykeluar').val(data[i].qtykeluar);
                        $('#qtypick').val(data[i].qtypick);
                        $('#qtyakhir').val(data[i].stock);
                    }
                }
            });
        };

        document.getElementById("clear").addEventListener("click", function(event) {
            event.preventDefault();
            ClearScreen();
        });

        ClearScreen();

    });
</script>