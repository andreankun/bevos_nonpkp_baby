<style>
	.logo-menu {
		background: #11101D;
		width: 80px;
		height: 80px;
		border-radius: 8px;
		padding: 5px 10px;
		color: #FFF;
		margin-right: 5px;
	}

	.modal-content {
		border-color: #EEE;
	}

	.hidescroll {
		scrollbar-width: none !important;
	}

	.hidescroll::-webkit-scrollbar {
		display: none !important;
	}

	.table-responsive {
		scrollbar-width: thin;
		scrollbar-color: #47404F #DEDEDE;
	}

	.table-responsive::-webkit-scrollbar {
		width: 10px;
		height: 10px;
	}

	.table-responsive::-webkit-scrollbar-track {
		background-color: #DEDEDE;
		border-radius: 100px;
	}

	.table-responsive::-webkit-scrollbar-thumb {
		border-radius: 100px;
		background-color: #47404F;
	}
</style>
