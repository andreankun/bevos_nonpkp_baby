<div class="col-12 col-xl-12 mb-2">
	<div class="modal-header" style="padding: 5px;">
		<div class="col-12 col-md-12">
			<div class="row">
				<div class="col-3 col-md-3">
					<h2>
						<span class="logo-menu"><?= $icons ?></span>
						<span><?= $title ?></span>
					</h2>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- <div class="col-12 hidescroll" style="height: 555px; overflow-y: scroll;">
	<div class="row">
		<div class="modal-content mb-2">
			<div class="modal-body">
				<div class="row my-2">
					<div class="col-md-3 so">
						<div class="row">
							<div class="col-md-6">
								SO Outstanding
							</div>
						</div>
						<a href="#" style="display: inline-block; text-align: center;">Test</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div> -->