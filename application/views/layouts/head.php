<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!-- <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"> -->
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">

<title><?= SITE_NAME . " | " . $title ?></title>

<!-- Site Icons -->
<link rel="shortcut icon" href="<?= base_url(); ?>assets/img/pet-shop.png" type="image/x-icon" />

<link href="<?= base_url(); ?>assets/css/lite-purple.min.css" rel="stylesheet" />
<link href="<?= base_url(); ?>assets/css/font-awesome.css" rel="stylesheet" />
<link href="<?= base_url('assets/fontawesome/css/all.min.css') ?>" rel="stylesheet" type="text/css">
<link href="<?= base_url('assets/boxicons/boxicons.min.css') ?>" rel="stylesheet" type="text/css">

<link href="<?= base_url(); ?>assets/css/perfect-scrollbar.min.css" rel="stylesheet" />

<link href="<?= base_url(); ?>assets/css/jquery-confirm.min.css" rel="stylesheet" />
<link href="<?= base_url(); ?>assets/jTippy-master/jTippy.min.css" rel="stylesheet" />

<link href="<?= base_url(); ?>assets/datepicker/css/bootstrap-datepicker.css" rel="stylesheet">

<link href="<?= base_url(); ?>assets/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
<link href="<?= base_url(); ?>assets/css/select2.min.css" rel="stylesheet">
<!-- <link rel="stylesheet" href="<?= base_url(); ?>assets/css/jquery.scrolling-tabs.css">     -->
<!-- <-?php !empty($css) ? $this->load->view($css) : ""; ?> -->