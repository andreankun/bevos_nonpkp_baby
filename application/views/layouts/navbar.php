<style>
	.layout-sidebar-large .main-header {
		background: #008FD7;
		padding: 0rem 0rem;
		padding-right: 0.2rem;
	}
	.main-header .header-icon:hover {
		background: #F8F9FA;
	}
	.nav-cabang {
		background-color: #FFF;
		background-image: -webkit-linear-gradient(90deg, #185E82,#185E82,#52a3ff,#52a3ff,#185E82);
		background-size: 100%;
		-webkit-background-clip: text;
		-webkit-text-fill-color: transparent;
		-moz-background-clip: text;
		-moz-text-fill-color: transparent;
	}
	.nav-menu-icon, .nav-menu-title {
		color: #FFF;
	}
	@media (max-width: 576px) {
		.layout-sidebar-large .main-header {
			height: 40px;
			padding: 0rem;
		}
		/* .main-header .nav-menu {
			width: 85px;
			overflow: hidden;
		} */
		.nav-company {
			width: 60px;
			overflow: hidden;
		}
	}
	.nav-company {
		padding-left: 10px;
		background-color: #FFF;
		/* border-right-style: solid;
		border-right-width: 7px;
		border-color: #008FD7; */
		height: 100%;
		align-items: center;
	}
	.nav-company-sep {
		background-color: #FFF;
		border-right-style: solid;
		border-right-width: 7px;
		border-color: #008FD7;
		height: 100%;
		align-items: center;
	}
	.nav-company-sep2 {
		background-color: #FFF;
		border-right-style: solid;
		border-right-width: 7px;
		border-color: #FFCE3A;
		height: 100%;
		align-items: center;
	}
</style>
<!-- warna : style="background-color: #52a3ff;" -->
<!-- warna : style="background-color: #e60029;" -->
<!-- warna : style="background-color: #47404f;" -->
<nav class="main-header navbar-expand-lg" style="border-bottom: thin solid #ddd;">
	<ul class="navbar-nav nav-company ml-0 mt-0 mt-lg-0">
        <a class="navbar-brand ml-0" style="padding-top: 0px; padding-bottom: 0px;" href="<?php echo base_url(); ?>">
			<img src="<?php echo base_url(); ?>assets/img/pet-shop.png" width="40px" height="30px">
			<span class="nav-cabang ml-2" style="font-size: 16px; font-weight: bold;">BEVOS NON PKP</span>
		</a>
	</ul>
	<ul class="navbar-nav nav-company-sep ml-0 mt-0 mt-lg-0">
        
	</ul>
	<ul class="navbar-nav nav-company-sep2 ml-0 mt-0 mt-lg-0">
        
	</ul>
	<ul class="navbar-nav nav-menu ml-1 mt-0 mt-lg-0" style="padding-top: 5px;">
		<span class="nav-menu-icon" style="font-size: 22px; font-weight: bold;"><?php echo $icons ?></span>
		<span class="nav-menu-title" style="font-size: 22px; font-weight: bold;"><?php echo strtoupper($title) ?></span>
	</ul>
	<ul class="navbar-nav ml-auto mt-0 mt-lg-0">
		<li class="nav-item dropdown">
			<a class="fas fa-power-off text-light header-icon" role="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></a>
			<div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
				<a class="dropdown-item" href="#logoutModal" data-toggle="modal">Log Out</a>
				<div class="dropdown-divider" ></div>
				<a class="dropdown-item" href="#">Something else here</a>
			</div>
		</li>
	</ul>
</nav>
