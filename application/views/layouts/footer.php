<!-- JQUERY SCRIPTS -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-1.10.2.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-ui.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/autocomplete.js"></script>

<!-- BOOTSTRAP SCRIPTS -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>

<!-- CUSTOM SCRIPTS -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/custom.js"></script>

<script src="<?php echo base_url('assets/jquery/jquery.min.js') ?>"></script>
<script src="<?php echo base_url('assets/bootstrap/js/bootstrap.bundle.min.js') ?>"></script>

<!-- GULL TEMPLATE SCRIPT -->
<script src="<?php echo base_url(); ?>assets/js/perfect-scrollbar.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/script.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/sidebar.large.script.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/mhpro-sidebar.js"></script>

<!-- Core plugin JavaScript-->
<script src="<?php echo base_url('assets/jquery-easing/jquery.easing.min.js') ?>"></script>

<!-- Custom scripts for all pages-->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/jquery/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/jquery/jquery-confirm.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/jquery/popover.min.js"></script>

<!-- FULL CALENDAR SCRIPTS -->
<script src="<?php echo base_url('assets/fullcalender/js/jquery-ui.min.js') ?>"></script>
<script src="<?php echo base_url('assets/fullcalender/js/moment.min.js') ?>"></script>
<script src="<?php echo base_url('assets/fullcalender/js/fullcalendar.min.js') ?>"></script>
<script src="<?php echo base_url('assets/daypilot/daypilot-all.min.js') ?>"></script>

<!-- DATEPICKER SCRIPTS -->
<script src="<?php echo base_url('assets/datepicker/js/bootstrap-datepicker.min.js') ?>"></script>
<script src="<?php echo base_url('assets/datepicker/js/bootstrap-datepicker.js') ?>"></script>

<!-- Page level plugins -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/chart.js/Chart.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/chart.js/chartjs-plugin-datalabels.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jspdf.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/fuse.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/jTippy-master/jTippy.min.js"></script>
<script src="<?php echo base_url('assets/datatables/jquery.dataTables.min.js') ?>"></script>
<script src="<?php echo base_url('assets/datatables/dataTables.bootstrap4.min.js') ?>"></script>
<script src="<?php echo base_url(); ?>assets/js/demo/datatables-demo.js"></script>


<!-- SweetAlert2 -->
<script src="<?= base_url() ?>assets/js/sweetalert2@11.js"></script>

<!-- Datetime & moment.js -->
<script src="<?php echo base_url(); ?>assets/js/datetime.js"></script>
<script src="<?php echo base_url(); ?>assets/js/moment-with-locales.min.js"></script>

<!-- Mask -->
<script src="<?= base_url(); ?>assets/js/jquery.mask.min.js"></script>

<script src="<?= base_url(); ?>assets/js/select2.min.js" rel="stylesheet"></script>
<!-- <script src="<?= base_url(); ?>assets/js/html2canvas.min.js" rel="stylesheet"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/FileSaver.js/1.3.8/FileSaver.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.min.js"></script>

<!-- Idle JS -->
<script src="<?php echo base_url(); ?>assets/js/jquery.idle.js"></script>
<script type="text/javascript">

    $(document).idle({
        onIdle: function() {
            location.href = '<?php echo base_url(); ?>login/login/logout';
        },
        idle: 36000000
    })
    const grup = $('#grup').val();

    if (grup == 'HO1') {
        $('.menu-sales').show();
        $('.menu-finance').hide();
        $('.menu-purchasing').show();
        $('.menu-dashboard').hide();
        $('.menu-masterdata').show();
        $('.sub-barang').show();
        $('.sub-cabang').show();
        $('.sub-diskoncabang').show();
        $('.sub-diskonglobal').show();
        $('.sub-global').show();
        $('.menu-inventory').show();
        $('.menu-report').show();
        $('#cancel').show();
    } else if (grup == 'HO2') {
        $('.menu-sales').show();
        $('.menu-finance').hide();
        $('.menu-purchasing').show();
        $('.menu-dashboard').hide();
        $('.menu-masterdata').show();
        $('.sub-barang').show();
        $('.sub-cabang').show();
        $('.sub-diskoncabang').show();
        $('.sub-diskonglobal').show();
        $('.sub-global').show();
        $('.menu-inventory').show();
        $('.menu-report').show();
        $('#cancel').hide();
    } else if (grup == 'ADM JKT') {
        $('.menu-sales').show();
        $('.menu-finance').hide();
        $('.menu-purchasing').hide();
        $('.menu-dashboard').hide();
        $('.menu-masterdata').show();
        $('.sub-barang').show();
        $('.sub-cabang').show();
        $('.sub-diskoncabang').hide();
        $('.sub-diskonglobal').hide();
        $('.sub-global').show();
        $('.menu-inventory').show();
        $('.menu-report').show();
        // $('#cancel').hide();
    } else if (grup == 'KEP. LK') {
        $('.menu-sales').show();
        $('.menu-finance').hide();
        $('.menu-purchasing').hide();
        $('.menu-dashboard').hide();
        $('.menu-masterdata').show();
        $('.sub-barang').show();
        $('.sub-cabang').show();
        $('.sub-diskoncabang').show();
        $('.sub-diskonglobal').show();
        $('.sub-global').show();
        $('.menu-inventory').show();
        $('.menu-report').show();
    } else if (grup == 'AUDIT LK') {
        $('.menu-sales').show();
        $('.menu-finance').hide();
        $('.menu-purchasing').hide();
        $('.menu-dashboard').hide();
        $('.menu-masterdata').show();
        $('.sub-barang').show();
        $('.sub-cabang').hide();
        $('.sub-diskoncabang').hide();
        $('.sub-diskonglobal').hide();
        $('.sub-global').show();
        $('.menu-inventory').show();
        $('.menu-report').show();
        $('#cancel').hide();
    } else if (grup == 'FINANCE JKT') {
        $('.menu-sales').show();
        $('.menu-finance').show();
        $('.menu-purchasing').hide();
        $('.menu-dashboard').hide();
        $('.menu-masterdata').show();
        $('.menu-inventory').hide();
        $('.menu-report').show();
        // $('#cancel').hide();
    } else if (grup == 'ADMFINANCE') {
        $('.menu-sales').hide();
        $('.menu-finance').show();
        $('.menu-purchasing').hide();
        $('.menu-dashboard').hide();
        $('.menu-masterdata').show();
        $('.menu-inventory').hide();
        $('.menu-report').show();
        $('#cancel').hide();
    } else if (grup == 'SALES') {
        $('.menu-sales').show();
        $('.menu-finance').hide();
        $('.menu-purchasing').hide();
        $('.menu-dashboard').hide();
        $('.menu-masterdata').hide();
        $('.menu-inventory').hide();
        $('.menu-report').hide();
        $('#save').show();
        $('#update').hide();
        $('#cancel').hide();
    } else if (grup == 'ADM LK') {
        $('.menu-sales').show();
        $('.menu-finance').show();
        $('.menu-purchasing').show();
        $('.menu-dashboard').hide();
        $('.menu-masterdata').show();
        $('.sub-barang').show();
        $('.sub-cabang').show();
        $('.sub-diskoncabang').hide();
        $('.sub-diskonglobal').hide();
        $('.sub-global').show();
        $('.menu-inventory').show();
        $('.menu-report').show();
        $('#cancel').hide();
    } else if (grup == 'GUDANG JKT') {
        $('.menu-sales').show();
        $('.menu-finance').hide();
        $('.menu-purchasing').hide();
        $('.menu-dashboard').hide();
        $('.menu-inventory').show();
        $('.menu-masterdata').hide();
        $('.menu-report').show();
        // $('#save').hide();
        $('#update').hide();
        $('#cancel').hide();
    } else if(grup == 'UPDATE MASTER'){
        $('.menu-sales').hide();
        $('.menu-inventory').hide();
        $('.menu-finance').hide();
        $('.menu-purchasing').hide();
        $('.menu-dashboard').hide();
        $('.menu-masterdata').show();
        $('.sub-barang').show();
        $('.sub-cabang').show();
        $('.sub-diskoncabang').show();
        $('.sub-diskonglobal').show();
        $('.sub-global').show();
        $('.menu-report').hide();
        $('#save').hide();
        $('#update').show();
        $('#cancel').hide();
    } else if (grup == 'PAJAK') {
        $('.menu-sales').show();
        $('.menu-finance').show();
        $('.menu-inventory').show();
        $('.menu-purchasing').hide();
        $('.menu-dashboard').hide();
        $('.menu-masterdata').show();
        $('.sub-barang').show();
        $('.sub-cabang').hide();
        $('.sub-diskoncabang').hide();
        $('.sub-diskonglobal').hide();
        $('.sub-global').show();
        $('.menu-pajak').hide();
        $('.menu-report').show();
        $('#update').show();
        $('#cancel').hide();
    } else {
        $('.menu-sales').show();
        $('.menu-inventory').show();
        $('.menu-finance').show();
        $('.menu-purchasing').show();
        $('.menu-dashboard').hide();
        $('.menu-masterdata').show();
        $('.sub-barang').show();
        $('.sub-cabang').show();
        $('.sub-diskoncabang').show();
        $('.sub-diskonglobal').show();
        $('.sub-global').show();
        $('.menu-report').show();
    }
</script>

<?php !empty($script) ? $this->load->view($script) : ""; ?>