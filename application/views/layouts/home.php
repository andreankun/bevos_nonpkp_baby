<!DOCTYPE html>
<html lang="en">

<head>
	<?php $this->load->view("layouts/head.php") ?>
</head>
<?php $this->load->view("layouts/css.php") ?>

<body class="text-left">
	<div class="app-admin-wrap layout-sidebar-large">

		<!-- <-?php $this->load->view("layouts/navbar") ?> -->
		<?php $this->load->view("layouts/sidebar.php") ?>

		<div class="main-content-wrap sidenav" style="width: calc(100% - 78px); margin-top: 0px;">

			<div class="main-content">

				<?php $this->load->view($content); ?>

			</div>

			<div class="flex-grow-1"></div>

			<!-- <div class="app-footer"> -->

			<!-- FOOTER  -->
			<?php $this->load->view("layouts/footer.php") ?>

			<!-- </div> -->

		</div>

	</div>
	<?php $this->load->view('layouts/modal.php'); ?>
</body>

</html>
