<!-- <-?php

// $grup = $this->session->userdata('mygrup'); 
$grup = "SUPERADMIN";

// $dataspk = $this->db->query("SELECT * FROM Stpm_menu WHERE head = '".$headspk."' AND grup = '".$grup."' order by pos")->result();
// $otoritasmenu = $this->db->query("SELECT * FROM stpm_otorisasimenu WHERE grup = '".$grup."' order by nomenu")->result();

?> -->

<div class="sidebar">
    <div class="logo-details">
        <img class="icon" src="<?= base_url(); ?>assets/img/c3-logo.svg" width="45px" height="35px"/>
        <div class="ml-2 logo_name text-uppercase">bevos</div>
        <i class="bx bx-menu btn-sidebar-toogle" id="btn-sidebar-toogle" ></i>
    </div>
    <div class="wrap-nav-list rtl-ps-none" data-perfect-scrollbar="" data-suppress-scroll-x="true">
        <ul class="nav-list">
            <li class="nav-list-item" data-tooltip="tooltip_dashboard">
                <a class="list-item-menu" href="<?= base_url() ?>main/dashboard">
                    <i class="links_icon bx bx-grid-alt" style="font-size: 22px;"></i>
                    <span class="links_name">Dashboard</span>
                </a>
            </li>
            <li class="nav-list-item collapsed" data-tooltip="tooltip_sales" data-toggle="collapse" data-target="#entry_order">
                <a class="list-item-menu" href="#">
                    <i class="links_icon far fa-file-invoice" style="font-size: 20px;"></i>
                    <span class="links_name">Sales</span>
                    <span class="badge badge-pill badge-warning"></span>
                </a>
                <div id="entry_order" class="collapse">
                    <ul class="nav-sublist">
                        <!-- <-?php foreach ($otoritasmenu as $value): 
                            $menu = $this->db->query("SELECT * FROM stpm_menu WHERE kodemenu = '".$value->kodemenu."' AND grupmenu = 'Sales' AND aktif = true order by nourut")->result();
                        ?>
                        <-?php foreach ($menu as $value): ?> -->
                        <li class="nav-sublist-item">
                            <a class="sublist-item-menu" style="padding-left: 10px;" href="<?= base_url() ?>main/sales">
                                <i class="links_icon far fa-circle fa-sm"></i>
                                <span class="links_name">
									Sales
									<!-- <-?= $value->namamenu ?> -->
								</span>
                            </a>
                        </li>
                        <!-- <-?php endforeach ?>
                        <-?php endforeach ?> -->
                    </ul>
                </div>
            </li>
            <li class="nav-list-item collapsed" data-tooltip="tooltip_finance" data-toggle="collapse" data-target="#finance_menu">
                <a class="list-item-menu" href="#">
                    <i class="links_icon far fa-envelope-open-dollar" ></i>
                    <span class="links_name">Finance</span>
                    <span class="badge badge-pill badge-warning">New</span>
                </a>
                <div id="finance_menu" class="collapse">
                    <ul class="nav-sublist">
                        <!-- <-?php foreach ($otoritasmenu as $value): 
                            $menu = $this->db->query("SELECT * FROM stpm_menu WHERE kodemenu = '".$value->kodemenu."' AND grupmenu = 'Finance' AND aktif = true order by nourut")->result();
                        ?>
                        <-?php foreach ($menu as $value): ?> -->
                        <li class="nav-sublist-item">
                            <a class="sublist-item-menu" style="padding-left: 10px;" href="<?= base_url(); ?>main/">
                                <i class="links_icon far fa-circle fa-sm"></i>
                                <span class="links_name">
									<!-- <-?= $value->namamenu ?> -->
								</span>
                            </a>
                        </li>
                        <!-- <-?php endforeach ?>
                        <-?php endforeach ?> -->
                    </ul>
                </div>
            </li>
            <li class="nav-list-item collapsed" data-tooltip="tooltip_production" data-toggle="collapse" data-target="#production_menu">
                <a class="list-item-menu" href="#">
                    <i class="links_icon far fa-conveyor-belt" style="font-size: 19px;"></i>
                    <span class="links_name">Production</span>
                    <span class="badge badge-pill badge-warning">New</span>
                </a>
                <div id="production_menu" class="collapse">
                    <ul class="nav-sublist">
                        <!-- <-?php foreach ($otoritasmenu as $value): 
                            $menu = $this->db->query("SELECT * FROM stpm_menu WHERE kodemenu = '".$value->kodemenu."' AND grupmenu = 'Production' AND aktif = true order by nourut")->result();
                        ?> -->
                        <!-- <-?php foreach ($menu as $value): ?> -->
                        <li class="nav-sublist-item">
                            <a class="sublist-item-menu" style="padding-left: 10px;" href="<?= base_url(); ?>main/">
                                <i class="links_icon far fa-circle fa-sm"></i>
                                <span class="links_name">
									<!-- <-?= $value->namamenu ?> -->
								</span>
                            </a>
                        </li>
                        <!-- <-?php endforeach ?>
                        <-?php endforeach ?> -->
                    </ul>
                </div>
            </li>
            <li class="nav-list-item collapsed" data-tooltip="tooltip_inventory" data-toggle="collapse" data-target="#Inventory_menu">
                <a class="list-item-menu" href="#">
                    <i class="links_icon fas fa-inventory" style="font-size: 14px;"></i>
                    <span class="links_name">Inventory</span>
                    <span class="badge badge-pill badge-warning">New</span>
                </a>
                <div id="Inventory_menu" class="collapse">
                    <ul class="nav-sublist">
                        <li class="nav-sublist-item dropdown">
                            <a class="sublist-item-menu" style="padding-left: 10px;" id="bahanbaku" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">
                                <i class="links_icon far fa-circle fa-sm"></i>
                                <span class="links_name">Raw Material</span>
                            </a>
                            <div class="dropdown-menu" aria-labelledby="bahanbaku">
                                <!-- <-?php foreach ($otoritasmenu as $value): 
                                    $menu = $this->db->query("SELECT * FROM stpm_menu WHERE kodemenu = '".$value->kodemenu."' AND grupmenu = 'Raw Material' AND aktif = true order by nourut")->result();
                                ?>
                                <-?php foreach ($menu as $value): ?> -->
                                <a class="dropdown-item p-1" style="padding-left: 40px !important;" href="<?= base_url(); ?>main/">
                                    <i class="far fa-circle fa-sm"></i>
                                    <!-- <-?= $value->namamenu ?> -->
                                </a>
                                <!-- <-?php endforeach ?>
                                <-?php endforeach ?> -->
                            </div>
                        </li>
                        <li class="nav-sublist-item dropdown">
                            <a class="sublist-item-menu" style="padding-left: 10px;" id="bahanpembantu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">
                                <i class="links_icon far fa-circle fa-sm"></i>
                                <span class="links_name">Bahan Pembantu</span>
                            </a>
                            <div class="dropdown-menu" aria-labelledby="bahanbaku">
                                <!-- <-?php foreach ($otoritasmenu as $value): 
                                    $menu = $this->db->query("SELECT * FROM stpm_menu WHERE kodemenu = '".$value->kodemenu."' AND grupmenu = 'Bahan Pembantu' AND aktif = true order by nourut")->result();
                                ?>
                                <-?php foreach ($menu as $value): ?> -->
                                <a class="dropdown-item p-1" style="padding-left: 40px !important;" href="<?= base_url(); ?>main/">
                                    <i class="far fa-circle fa-sm"></i>
                                    <!-- <-?= $value->namamenu ?> -->
                                </a>
                                <!-- <-?php endforeach ?> -->
                                <!-- <-?php endforeach ?> -->
                            </div>
                        </li>
                        <li class="nav-sublist-item dropdown">
                            <a class="sublist-item-menu" style="padding-left: 10px;" id="hasiljadi" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">
                                <i class="links_icon far fa-circle fa-sm"></i>
                                <span class="links_name">Hasi Produksi</span>
                            </a>
                            <div class="dropdown-menu" aria-labelledby="hasiljadi">
                                <!-- <-?php foreach ($otoritasmenu as $value): 
                                    $menu = $this->db->query("SELECT * FROM stpm_menu WHERE kodemenu = '".$value->kodemenu."' AND grupmenu = 'Hasil Produksi' AND aktif = true order by nourut")->result();
                                ?>
                                <-?php foreach ($menu as $value): ?> -->
                                <a class="dropdown-item p-1" style="padding-left: 40px !important;" href="<?= base_url(); ?>main/">
                                    <i class="far fa-circle fa-sm"></i>
                                    <!-- <-?= $value->namamenu ?> -->
                                </a>
                                <!-- <-?php endforeach ?>
                                <-?php endforeach ?> -->
                            </div>
                        </li>
                        <!-- <-?php foreach ($otoritasmenu as $value): 
                            $menu = $this->db->query("SELECT * FROM stpm_menu WHERE kodemenu = '".$value->kodemenu."' AND grupmenu = 'Inventory' AND aktif = true order by nourut")->result();
                        ?>
                        <-?php foreach ($menu as $value): ?> -->
                        <li class="nav-sublist-item">
                            <a class="sublist-item-menu" style="padding-left: 10px;" href="<?= base_url(); ?>main/">
                                <i class="links_icon far fa-circle fa-sm"></i>
                                <span class="links_name">
									<!-- <-?= $value->namamenu ?>-->
								</span>
                            </a>
                        </li>
                        <!-- <-?php endforeach ?>
                        <-?php endforeach ?> -->
                    </ul>
                </div>
            </li>
            <li class="nav-list-item" data-tooltip="tooltip_reporting">
                <a class="list-item-menu" href="<?= base_url(); ?>main/reporting">
                    <i class="links_icon far fa-file-invoice" ></i>
                    <span class="links_name">Reporting</span>
                </a>
            </li>
            <li class="nav-list-item collapsed" data-tooltip="tooltip_purchasing" data-toggle="collapse" data-target="#purchase_menu">
                <a class="list-item-menu" href="#">
                    <i class="links_icon far fa-shopping-cart" ></i>
                    <span class="links_name">Purchasing</span>
                    <span class="badge badge-pill badge-warning">New</span>
                </a>
                <div id="purchase_menu" class="collapse">
                    <ul class="nav-sublist">
                        <!-- <-?php foreach ($otoritasmenu as $value): 
                            $menu = $this->db->query("SELECT * FROM stpm_menu WHERE kodemenu = '".$value->kodemenu."' AND grupmenu = 'Purchasing' AND aktif = true order by nourut")->result();
                        ?>
                        <-?php foreach ($menu as $value): ?> -->
                        <li class="nav-sublist-item">
                            <a class="sublist-item-menu" style="padding-left: 10px;" href="<?= base_url(); ?>main/">
                                <i class="links_icon far fa-circle fa-sm"></i>
                                <span class="links_name">
									<!-- <-?= $value->namamenu ?> -->
								</span>
                            </a>
                        </li>
                        <!-- <-?php endforeach ?>
                        <-?php endforeach ?> -->
                    </ul>
                </div>
            </li>
            <li class="nav-list-item collapsed" data-tooltip="tooltip_masterdata" data-toggle="collapse" data-target="#masterdata_menu">
                <a class="list-item-menu" href="#">
                    <i class="links_icon fas fa-box-full" style="font-size: 16px;"></i>
                    <span class="links_name">Master Data</span>
                    <span class="badge badge-pill badge-warning">New</span>
                </a>
                <div id="masterdata_menu" class="collapse">
                    <ul class="nav-sublist">
                        <!-- <-?php foreach ($otoritasmenu as $value): 
                            $menu = $this->db->query("SELECT * FROM stpm_menu WHERE kodemenu = '".$value->kodemenu."' AND grupmenu = 'Master Data' AND aktif = true order by nourut")->result();
                        ?>
                        <-?php foreach ($menu as $value): ?> -->
                        <li class="nav-sublist-item">
                            <a class="sublist-item-menu" style="padding-left: 10px;" href="<?= base_url(); ?>main/">
                                <i class="links_icon far fa-circle fa-sm"></i>
                                <span class="links_name">
									<!-- <-?= $value->namamenu ?> -->
								</span>
                            </a>
                        </li>
                        <!-- <-?php endforeach ?>
                        <-?php endforeach ?> -->
                    </ul>
                </div>
            </li>
            <li class="profile">
                <div class="profile-details">
                    <div class="name_job">
                        <div class="name text-uppercase">vikry surya pangestu</div>
                        <div class="job">Web designer</div>
                    </div>
                </div>
                <i class="bx bx-log-out btn-sidebar-logout" id="log_out" href="#logoutModal" data-toggle="modal" ></i>
            </li>
        </ul>
    </div>


</div>
<div class="sidebar-hoverlay"></div>
