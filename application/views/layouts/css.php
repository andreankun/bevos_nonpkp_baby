<style>
	/* CSS Sidebar */
	* {
		margin: 0;
		padding: 0;
		box-sizing: border-box;
	}

	.sidebar {
		position: fixed;
		left: 0;
		top: 0;
		height: 100%;
		width: 78px;
		background: #68478D;
		padding: 6px 14px;
		z-index: 99;
		transition: all 0.5s ease;
	}

	.sidebar.open {
		width: 250px;
	}

	.sidebar .logo-details {
		height: 60px;
		display: flex;
		align-items: center;
		position: relative;
	}

	.sidebar .logo-details .icon {
		opacity: 0;
		transition: all 0.5s ease;
	}

	.sidebar .logo-details .logo_name {
		color: #FFF;
		font-size: 14px;
		font-weight: 600;
		opacity: 0;
		transition: all 0.5s ease;
	}

	.sidebar.open .logo-details .icon,
	.sidebar.open .logo-details .logo_name {
		opacity: 1;
	}

	.sidebar .logo-details #btn-sidebar-toogle {
		position: absolute;
		top: 50%;
		right: 0;
		transform: translateY(-50%);
		font-size: 22px;
		transition: all 0.4s ease;
		font-size: 23px;
		text-align: center;
		cursor: pointer;
		transition: all 0.5s ease;
	}

	.sidebar.open .logo-details #btn-sidebar-toogle {
		text-align: right;
	}

	.sidebar .btn-sidebar-toogle,
	.sidebar .btn-sidebar-logout {
		color: #FFF;
		height: 60px;
		min-width: 50px;
		font-size: 28px;
		text-align: center;
		line-height: 60px;
	}

	.sidebar .btn-sidebar-toogle:hover,
	.sidebar .btn-sidebar-logout:hover {
		color: #16C7FF;
		text-shadow: 0px 0px 10px rgba(22, 199, 255, 1);
	}

	.sidebar .links_icon {
		color: #FFF;
		/* height: 60px; */
		height: 50px;
		min-width: 50px;
		text-align: center;
		/* line-height: 60px; */
		line-height: 50px;
	}

	.sidebar .wrap-nav-list {
		position: relative;
		max-height: 500px;
	}

	.sidebar .nav-list {
		margin-top: 20px;
		height: 100%;
	}

	.sidebar .nav-list-item {
		position: relative;
		margin: 8px 0;
		list-style: none;
	}

	.sidebar .nav-sublist-item {
		position: relative;
		margin: 8px 0;
		list-style: none;
	}

	.sidebar .nav-list-item .badge {
		float: right;
		margin-left: 5px;
	}

	.sidebar .nav-list-item .tooltip {
		position: absolute;
		top: -20px;
		left: calc(100% + 15px);
		z-index: 3;
		background: #FFF;
		box-shadow: 0 5px 10px rgba(0, 0, 0, 0.3);
		padding: 6px 12px;
		border-radius: 4px;
		font-size: 15px;
		font-weight: 400;
		opacity: 0;
		pointer-events: none;
		transition: 0s;
	}

	.sidebar .wrap-nav-list .nav-list-item .list-item-menu:hover .tooltip {
		display: block;
		opacity: 1;
		pointer-events: auto;
		transition: all 0.4s ease;
		top: 50%;
		transform: translateY(-50%);
		z-index: 99;
	}

	.sidebar.open .nav-list-item .tooltip {
		display: none;
	}

	.sidebar input {
		font-size: 15px;
		color: #FFF;
		font-weight: 400;
		outline: none;
		height: 50px;
		width: 100%;
		width: 50px;
		border: none;
		border-radius: 12px;
		transition: all 0.5s ease;
		background: #68478D;
	}

	.sidebar.open input {
		padding: 0 20px 0 50px;
		width: 100%;
	}

	.sidebar .bx-search {
		position: absolute;
		top: 50%;
		left: 0;
		transform: translateY(-50%);
		font-size: 22px;
		background: #68478D;
		color: #FFF;
	}

	.sidebar.open .bx-search:hover {
		background: #68478D;
		color: #FFF;
	}

	.sidebar .bx-search:hover {
		background: #FFF;
		color: #68478D;
	}

	.sidebar .nav-list-item .list-item-menu {
		display: flex;
		height: 100%;
		width: 100%;
		/* border-radius: 12px; */
		border-radius: 5px;
		align-items: center;
		text-decoration: none;
		transition: all 0.4s ease;
		background: #68478D;
	}

	.sidebar .nav-sublist-item .sublist-item-menu {
		display: flex;
		height: 100%;
		width: 100%;
		/* border-radius: 12px; */
		border-radius: 5px;
		align-items: center;
		text-decoration: none;
		transition: all 0.4s ease;
		background: #68478D;
	}

	.sidebar .nav-list-item .list-item-menu:hover {
		background: #FFF;
		color: #68478D;
	}

	.sidebar .nav-list-item .list-item-menu .links_name {
		color: #FFF;
		font-size: 14px;
		font-weight: 400;
		white-space: nowrap;
		opacity: 0;
		pointer-events: none;
		transition: 0.4s;
	}

	.sidebar.open .nav-list-item .list-item-menu .links_name {
		opacity: 1;
		pointer-events: auto;
	}

	.sidebar .nav-list-item .list-item-menu:hover .links_name,
	.sidebar .nav-list-item .list-item-menu:hover .links_icon {
		transition: all 0.5s ease;
		color: #68478D;
	}

	.sidebar .nav-list-item .links_icon {
		/* height: 50px; */
		height: 40px;
		/* line-height: 50px; */
		line-height: 40px;
		font-size: 18px;
		border-radius: 12px;
	}

	.sidebar .nav-sublist-item .links_icon {
		/* height: 50px; */
		height: 40px;
		/* line-height: 50px; */
		line-height: 40px;
		font-size: 14px;
		border-radius: 12px;
	}

	.sidebar li.profile {
		position: fixed;
		height: 60px;
		width: 78px;
		left: 0;
		bottom: -8px;
		bottom: 0px;
		padding: 10px 14px;
		background: #68478D;
		transition: all 0.5s ease;
		overflow: hidden;
	}

	.sidebar.open li.profile {
		width: 250px;
	}

	.sidebar li .profile-details {
		display: flex;
		align-items: center;
		flex-wrap: nowrap;
	}

	.sidebar li img {
		height: 45px;
		width: 45px;
		object-fit: cover;
		border-radius: 6px;
		margin-right: 10px;
	}

	.sidebar li.profile .name,
	.sidebar li.profile .job {
		font-size: 15px;
		font-weight: 400;
		color: #FFF;
		white-space: nowrap;
	}

	.sidebar li.profile .job {
		font-size: 12px;
	}

	.sidebar .profile #log_out {
		position: absolute;
		top: 50%;
		right: 0;
		transform: translateY(-50%);
		background: #68478D;
		width: 100%;
		/* height: 60px; */
		height: 50px;
		/* line-height: 60px; */
		line-height: 50px;
		border-radius: 0px;
		transition: all 0.5s ease;
	}

	.sidebar.open .profile #log_out {
		width: 50px;
		background: none;
	}

	.home-section {
		position: relative;
		background: #E4E9F7;
		min-height: 100vh;
		top: 0;
		left: 78px;
		width: calc(100% - 78px);
		transition: all 0.5s ease;
		z-index: 2;
	}

	.sidebar.open~.home-section {
		left: 250px;
		width: calc(100% - 250px);
	}

	.home-section .text {
		display: inline-block;
		color: #68478D;
		font-size: 25px;
		font-weight: 500;
		margin: 18px
	}

	.nav-list-item[data-toggle="collapse"] .list-item-menu::after {
		content: "\f078";
		/* content: '\f054'; */
		font-family: fontAwesome;
		font-weight: 400;
		font-style: normal;
		display: inline-block;
		position: relative;
		justify-content: center;
		text-align: center;
		text-decoration: none;
		-webkit-font-smoothing: antialiased;
		-moz-osx-font-smoothing: grayscale;
		margin-top: auto;
		margin-bottom: auto;
		margin-right: 10px;
		margin-left: auto;
		font-size: 12px;
	}

	.nav-list-item.collapsed[data-toggle="collapse"] .list-item-menu::after {
		/* content: '\f078'; */
		content: '\f054';
	}

	.sublist-item-menu:hover {
		background: #68478D !important;
	}

	.sublist-item-menu:hover::after,
	.sublist-item-menu:hover .links_icon,
	.sublist-item-menu:hover .links_name {
		color: #16C7FF;
		text-shadow: 0px 0px 10px rgba(22, 199, 255, 1);
	}

	.nav-sublist-item .dropdown-menu {
		width: 100%;
		background: #fff;
		color: #68478D;
	}

	.nav-sublist-item .sublist-item-menu[aria-expanded="true"] {
		background: #68478D !important;
		border-radius: 0.45em 0.45em 0 0 !important;
	}

	.nav-sublist-item .dropdown-menu.show {
		background: #fff;
		border-radius: 0 0 0.45em 0.45em;
	}

	.nav-sublist-item .dropdown-menu .dropdown-item {
		color: #333;
	}

	.nav-sublist-item .dropdown-menu .dropdown-item i {
		min-width: 30px;
		height: 30px;
		line-height: 30px;
		text-align: center;
	}

	.nav-sublist-item .dropdown-menu .dropdown-item:hover,
	.nav-sublist-item .dropdown-menu .dropdown-item:hover i {
		color: #68478D;
		background: none;
		text-shadow: 0px 0px 10px #68478D;
	}

	.dropdown .sublist-item-menu::after {
		content: "\f054";
		font-family: fontAwesome;
		font-weight: 400;
		font-style: normal;
		display: inline-block;
		position: relative;
		justify-content: center;
		text-align: center;
		text-decoration: none;
		-webkit-font-smoothing: antialiased;
		-moz-osx-font-smoothing: grayscale;
		margin-top: auto;
		margin-bottom: auto;
		margin-right: 10px;
		margin-left: auto;
		font-size: 10px;
	}

	.dropdown.show .sublist-item-menu::after {
		content: "\f078";
	}

	.jt-white-theme {
		background: #68478D;
		border-radius: 0.65em;
	}

	.jt-white-theme .links_icon,
	.jt-white-theme .links_name {
		color: #FFF;
		font-weight: normal;
	}

	.sidebar-hoverlay {
		display: none;
		position: fixed;
		z-index: 100;
		top: 0px;
		right: 0px;
		background: #000000;
		width: calc(100% - 250px - 0px);
		height: calc(100vh - 0px);
		opacity: 0.1;
		cursor: pointer;
	}

	.sidebar-hoverlay.open {
		display: block;
	}

	/* End of CSS Sidebar */

	/* CSS Home */
	.input-group-text {
		padding: 0.15rem 0.55rem;
	}

	/* .input-group [type="text"].form-control {
        height: calc(1.5rem + 2px);
        height: 29px;
    } */
	.input-group [type="number"].form-control {
		height: 29px;
	}

	.box select {
		-webkit-appearance: button;
		appearance: button;
		outline: none;
	}

	.box:hover::before {
		color: rgba(255, 255, 255, 0.6);
		background-color: rgba(255, 255, 255, 0.2);
	}

	.box select option {
		padding: 30px;
	}

	.form-control {
		/* padding: 0.375rem 0.55rem; */
		padding: 0.375rem 0.375rem;
		/* border-radius: 4px; */
		/* border: none; */
		/* background: #FFF; */
		/* border-bottom: 1px solid #ced4da; */
	}

	.form-control:focus {
		/* color: #665c70; */
		/* background-color: #fff; */
		border-color: #a679d2;
		outline: 0;
		/* box-shadow: -2px -2px -2px 0.1rem rgba(102, 51, 153, 0.25); */
	}

	/* .form-control:disabled, .form-control[readonly] {
        background-color: #d8f0fa;
        opacity: 0.7;
    } */
	/* .input-group-prepend {
		background: #F3F3F3;
		border-right: thin solid #ced4da;
		border-left: thin solid #ced4da;
		border-top: thin solid #ced4da;
	} */

	.input-group-text {
		/* border: none; */
		background: #FFF;
		/* border-radius: 4px 4px 4px 0px; */
		border-radius: 0px 4px 4px 0px;
		/* border-bottom: thin solid #ced4da; */
	}

	.switchbox {
		position: relative;
		width: 60px;
		height: 15px;
		-webkit-appearance: none;
		background: linear-gradient(0deg, #333, #000);
		outline: none;
		border-radius: 20px;
		box-shadow: 0 0 0 4px #353535, 0 0 0 5px #3e3e3e, inset 0 0 10px rgba(0, 0, 0, 1), 0 5px 5px rgba(0, 0, 0, .5), inset 0 0 15px rgba(0, 0, 0, .2);

	}

	.switchbox:checked {

		background: linear-gradient(0deg, #6dd1ff, #20b7ff);
		box-shadow: 0 0 2px #6dd1ff, 0 0 0 4px #353535, 0 0 0 5px #3e3e3e, inset 0 0 10px rgba(0, 0, 0, 1), 0 5px 5px rgba(0, 0, 0, .5), inset 0 0 15px rgba(0, 0, 0, .2);

	}

	.switchbox:before {
		content: '';
		position: absolute;
		top: 0px;
		left: 0px;
		width: 30px;
		height: 15px;
		background: linear-gradient(0deg, #000, #6b6b6b);
		border-radius: 20px;
		box-shadow: 0 0 0 1px #232323;
		transform: scale(.98, .96);
		transition: .5s;
	}

	.switchbox:checked:before {
		left: 30px;
	}

	.switchbox:after {
		content: '';
		position: absolute;
		top: calc(50% - 2px);
		left: 25px;
		width: 2px;
		height: 2px;
		background: linear-gradient(0deg, #6b6b6b, #000);
		border-radius: 50%;
		transition: .5s;

	}

	.switchbox:checked:after {
		background: #63cdff;
		left: 55px;
		box-shadow: 0 0 5px #13b3ff, 0 0 15px #13b3ff;
	}

	.radio {
		margin-bottom: 5px;
	}

	.display-checkbox {
		position: absolute;
		width: 15px;
		height: 15px;
		background: none;
		/* border: thin solid #ddd; */
		/* border-radius: 50%; */
		/* box-shadow: 0 3px 4px rgba(220,35,35,1), inset 0 -2px 5px rgba(220,35,35,1), inset 0 2px 2px rgba(255,255,255,0.5); */
		display: -moz-flex;
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-pack: center;
		-ms-flex-pack: center;
		justify-content: center;
		-webkit-box-align: center;
		-ms-flex-align: center;
		align-items: center;
		color: #000;
		font-size: 14px;
	}

	.checkon {
		display: none;
	}

	.pickdetail::before {
		content: "\f044";
		font-family: fontAwesome;
		font-weight: 400;
		font-style: normal;
		display: inline-block;
		position: relative;
		justify-content: center;
		text-align: center;
		text-decoration: none;
		-webkit-font-smoothing: antialiased;
		-moz-osx-font-smoothing: grayscale;
		height: 25px;
		width: 27px;
		vertical-align: middle;
		background: #4caf50;
		color: #fff;
		border-color: #3d8b40;
		border-radius: 0.25rem;
		/* border-radius: 50%; */
		margin-top: auto;
		margin-bottom: auto;
		margin-right: 10px;
		margin-left: auto;
		font-size: 16px;
		cursor: pointer;
	}

	[type="number"]::-webkit-inner-spin-button,
	[type="number"]::-webkit-outer-spin-button {
		-webkit-appearance: none !important;
		height: auto;
	}

	input[type=number] {
		-moz-appearance: textfield;
	}

	/* End of CSS Home */

	.logo-menu {
		background: #68478D;
		width: 80px;
		height: 80px;
		border-radius: 8px;
		padding: 5px 10px;
		color: #FFF;
		margin-right: 5px;
	}

	.modal-content {
		border-color: #EEE;
	}

	.modal-header h5 {
		color: #68478D;
	}

	.hidescroll {
		scrollbar-width: none !important;
	}

	.hidescroll::-webkit-scrollbar {
		display: none !important;
	}

	.table-responsive {
		scrollbar-width: thin;
		scrollbar-color: #47404F #DEDEDE;
	}

	*::-webkit-scrollbar {
		width: 10px;
		height: 10px;
	}

	.table-responsive::-webkit-scrollbar-track {
		background-color: #DEDEDE;
		border-radius: 100px;
	}

	.table-responsive::-webkit-scrollbar-thumb {
		border-radius: 100px;
		background-color: #47404F;
	}

	span {
		color: #68478D;
	}

	.links_name {
		color: #fff;
	}

	.input-group,
	.input-group-prepend,
	.input-group-text,
	.form-control {
		border: none;
	}

	input.form-control,
	select.form-control,
	textarea.form-control {
		background: transparent;
		border: none;
		border-bottom: 2px solid #68478D;
		-webkit-box-shadow: none;
		box-shadow: none;
		border-radius: 0;
	}

	input.form-control:focus,
	select.form-control:focus,
	textarea.form-control:focus {
		outline: none;
		-webkit-box-shadow: none;
		box-shadow: none;
	}

	input.form-control:read-only,
	select.form-control:disabled {
		background: rgb(238, 238, 238);
		opacity: 0.9;
	}

	/* small.error {
		display: block;
		color: #a94442;
		margin-left: 25%;
		margin-right: 25%;
	} */

	.btn-dark,
	.btn-info {
		background-color: #68478D;
		border-color: #68478D;
	}

	span.text-uppercase {
		font-size: 1.7vw;
	}

	table td:empty::before {
		content: "-";
	}

	.tabbable .nav-tabs {
		overflow-x: auto;
		overflow-y: hidden;
		flex-wrap: nowrap;
	}

	.tabbable .nav-tabs .nav-link {
		white-space: nowrap;
	}

	nav.tabbable {
		overflow: auto;
		white-space: nowrap;
	}

	nav.tabbable ul li a {
		display: inline-block;
		color: white;
		text-align: center;
		padding: 14px;
		text-decoration: none;
	}

	nav.tabbable ul li a:hover {
		background-color: #68478D;
	}

	.nav-tabs .nav-item .nav-link.active {
		border-top: #68478D 2.5px solid;
	}

	/* Works on Firefox */
	* {
		scrollbar-width: revert;
		scrollbar-color: #68478D #f0f0f0;
	}

	/* Works on Chrome, Edge, and Safari */
	*::-webkit-scrollbar {
		width: 0;
	}

	*::-webkit-scrollbar-track {
		background: #f0f0f0;
	}

	*::-webkit-scrollbar-thumb {
		background-color: #68478D;
		border-radius: 20px;
		border: 31x solid #68478D;
	}

	/* div.dataTables_wrapper {
		width: 800px;
		margin: 0 auto;
	} */

	body {
		--table-width: 100%;
		/* Or any value, this will change dinamically */
	}

	.t-scroll tbody {
		display: block;
		max-height: 300px;
		overflow-y: auto;
	}

	.t-scroll thead,
	.t-scroll tbody tr {
		display: table;
		width: var(--table-width);
		table-layout: fixed;
	}

	.penomoran {
		width: 50%;
		counter-reset: row-num - 1;
	}

	.penomoran tr {
		counter-increment: row-num;
	}

	.penomoran tr td:first-child::before {
		content: counter(row-num) ". ";
	}
</style>