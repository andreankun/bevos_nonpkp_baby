<?php
$grup = $this->session->userdata('mygrup');
$username = $this->session->userdata('myusername');
$cabang = $this->session->userdata('mycabang');
$gudang = $this->session->userdata('mygudang');

$userData = $this->db->query("SELECT * FROM stpm_login WHERE username = '" . $username . "'")->result();
// $cabangData = $this->db->query("SELECT kode, nama FROM glbm_cabang WHERE kode = '" . $cabang . "'")->row();
$companyData = $this->db->query("SELECT * FROM stpm_konfigurasi")->result();
$otoritasmenu = $this->db->query("SELECT * FROM stpm_otorisasimenu WHERE grup = '" . $grup . "' ORDER by nomenu")->result();
$namacabang = $this->db->query("SELECT * FROM glbm_cabang WHERE kode = '" . $cabang . "'")->row();
?>
<div class="sidebar">
	<input class="form-control" type="hidden" name="grup" id="grup" maxlength="50" value="<?= $this->session->userdata('mygrup'); ?>" required />
	<input class="form-control" type="hidden" name="username" id="username" maxlength="50" value="<?= $this->session->userdata('myusername'); ?>" required />
	<input class="form-control" type="hidden" name="cabang" id="cabang" maxlength="50" value="<?= $this->session->userdata('mycabang'); ?>" required />
	<!-- <input class="form-control" type="hidden" name="namacbg" id="namacbg" maxlength="50" value="<?= $cabangData->nama; ?>" required /> -->
	<input class="form-control" type="hidden" name="gudang" id="gudang" maxlength="50" value="<?= $this->session->userdata('mygudang'); ?>" required />
	<div class="logo-details">
		<img class="icon" src="<?= base_url(); ?>assets/img/pet-shop.png" width="45px" />
		<?php foreach ($companyData as $val) : ?>
			<div class="ml-2 logo_name text-uppercase"><?= $val->namaperusahaan ?></div>
		<?php endforeach; ?>
		<i class="bx bx-menu btn-sidebar-toogle" id="btn-sidebar-toogle"></i>
	</div>
	<div class="text-white text-bold text-center cabang"><?= $namacabang->nama ?></div>
	<div class="wrap-nav-list rtl-ps-none" data-perfect-scrollbar="" data-suppress-scroll-x="true">
		<ul class="nav-list">
			<!-- <li>
                <i class='bx bx-search' ></i>
                <input type="text" placeholder="Search...">
                <span class="tooltip">Search</span>
            </li> -->
			<li class="nav-list-item menu-dashboard" data-tooltip="tooltip_dashboard">
				<a class="list-item-menu" href="#">
					<a class="list-item-menu" href="<?= base_url(); ?>main/dashboard">
						<i class="links_icon bx bx-grid-alt" style="font-size: 22px;"></i>
						<span class="links_name">Dashboard</span>
					</a>
					<span class="tooltip">Dashboard</span>
			</li>
			<!-- <li class="nav-list-item" data-tooltip="tooltip_reporting">
				<a class="list-item-menu" href="<-?= base_url(); ?>main/reporting">
					<i class="links_icon far fa-file-invoice"></i>
					<span class="links_name">Reporting</span>
					<span class="badge badge-pill badge-warning">New</span>
				</a>
			</li> -->
			<li class="nav-list-item collapsed menu-sales" data-tooltip="tooltip_sales" data-toggle="collapse" data-target="#sales">
				<a class="list-item-menu" href="#">
					<i class="links_icon far fa-file-invoice" style="font-size: 22px;"></i>
					<span class="links_name">Sales</span>
					<!-- <span class="badge badge-pill badge-warning">New</span> -->
				</a>
				<span class="tooltip">Sales</span>
				<div id="sales" class="collapse">
					<ul class="nav-sublist">
						<?php foreach ($otoritasmenu as $value) :
							$menu = $this->db->query("SELECT * FROM stpm_menu WHERE kodemenu = '" . $value->kodemenu . "' AND grupmenu = 'Sales' AND aktif = true ORDER BY nourut")->result();
						?>
							<?php foreach ($menu as $value) : ?>
								<li class="nav-sublist-item">
									<a class="sublist-item-menu" style="padding-left: 10px;" href="<?= base_url(); ?>main/<?php echo $value->linkmenu ?>">
										<i class="links_icon far fa-circle fa-sm"></i>
										<span class="links_name"><?= $value->namamenu ?></span>
									</a>
								</li>
							<?php endforeach ?>
						<?php endforeach ?>
						<!-- <li class="nav-sublist-item">
							<a class="sublist-item-menu" href="<?= base_url(); ?>main/retur_ar">
								<i class="links_icon far fa-circle fa-sm"></i>
								<span class="links_name">Retur AR</span>
							</a>
						</li> -->
					</ul>
				</div>
			</li>
			<li class="nav-list-item collapsed menu-finance" data-tooltip="tooltip_finance" data-toggle="collapse" data-target="#finance">
				<a class="list-item-menu" href="#">
					<i class="links_icon far fa-envelope-open-dollar" style="font-size: 20px;"></i>
					<span class="links_name">Finance</span>
					<!-- <span class="badge badge-pill badge-warning">New</span> -->
				</a>
				<span class="tooltip">Finance</span>
				<div id="finance" class="collapse">
					<ul class="nav-sublist">
						<?php foreach ($otoritasmenu as $value) :
							$menu = $this->db->query("SELECT * FROM stpm_menu WHERE kodemenu = '" . $value->kodemenu . "' AND grupmenu = 'Finance' AND aktif = true ORDER BY nourut")->result();
						?>
							<?php foreach ($menu as $value) : ?>
								<li class="nav-sublist-item">
									<a class="sublist-item-menu" style="padding-left: 10px;" href="<?= base_url(); ?>main/<?php echo $value->linkmenu ?>">
										<i class="links_icon far fa-circle fa-sm"></i>
										<span class="links_name"><?= $value->namamenu ?></span>
									</a>
								</li>
							<?php endforeach ?>
						<?php endforeach ?>
						<!-- <li class="nav-sublist-item">
							<a class="sublist-item-menu" href="<-?= base_url(); ?>main/coa">
								<i class="links_icon far fa-circle fa-sm"></i>
								<span class="links_name">COA</span>
							</a>
						</li> -->
						<!-- <li class="nav-sublist-item">
							<a class="sublist-item-menu" href="<-?= base_url(); ?>main/piutang">
								<i class="links_icon far fa-circle fa-sm"></i>
								<span class="links_name">Piutang AR</span>
							</a>
						</li> -->
					</ul>
				</div>
			</li>
			<li class="nav-list-item collapsed menu-inventory" data-tooltip="tooltip_inventory" data-toggle="collapse" data-target="#inventory">
				<a class="list-item-menu" href="#">
					<i class="links_icon fas fa-inventory" style="font-size: 16px;"></i>
					<span class="links_name">Inventory</span>
					<!-- <span class="badge badge-pill badge-warning">New</span> -->
				</a>
				<span class="tooltip">Inventory</span>
				<div id="inventory" class="collapse">
					<ul class="nav-sublist">
						<?php foreach ($otoritasmenu as $value) :
							$menu = $this->db->query("SELECT * FROM stpm_menu WHERE kodemenu = '" . $value->kodemenu . "' AND grupmenu = 'Inventory' AND aktif = true ORDER BY nourut")->result();
						?>
							<?php foreach ($menu as $value) : ?>
								<li class="nav-sublist-item">
									<a class="sublist-item-menu" style="padding-left: 10px;" href="<?= base_url(); ?>main/<?php echo $value->linkmenu ?>">
										<i class="links_icon far fa-circle fa-sm"></i>
										<span class="links_name"><?= $value->namamenu ?></span>
									</a>
								</li>
							<?php endforeach ?>
						<?php endforeach ?>
					</ul>
				</div>
			</li>
			<li class="nav-list-item collapsed menu-purchasing" data-tooltip="tooltip_purchaseorder" data-toggle="collapse" data-target="#purchaseorder">
				<a class="list-item-menu" href="#">
					<i class="links_icon far fa-shopping-cart" style="font-size: 20px;"></i>
					<span class="links_name">Purchase Order</span>
					<!-- <span class="badge badge-pill badge-warning">New</span> -->
				</a>
				<span class="tooltip">Purchase Order</span>
				<div id="purchaseorder" class="collapse">
					<ul class="nav-sublist">
						<?php foreach ($otoritasmenu as $value) :
							$menu = $this->db->query("SELECT * FROM stpm_menu WHERE kodemenu = '" . $value->kodemenu . "' AND grupmenu = 'Purchasing' AND aktif = true ORDER BY nourut")->result();
						?>
							<?php foreach ($menu as $value) : ?>
								<li class="nav-sublist-item">
									<a class="sublist-item-menu" style="padding-left: 10px;" href="<?= base_url(); ?>main/<?php echo $value->linkmenu ?>">
										<i class="links_icon far fa-circle fa-sm"></i>
										<span class="links_name"><?= $value->namamenu ?></span>
									</a>
								</li>
							<?php endforeach ?>
						<?php endforeach ?>
					</ul>
				</div>
			</li>
			<li class="nav-list-item collapsed menu-masterdata" data-tooltip="tooltip_masterdata" data-toggle="collapse" data-target="#masterdata_menu">
				<a class="list-item-menu" href="#">
					<i class="links_icon fas fa-box-full" style="font-size: 18px;"></i>
					<span class="links_name">Master Data</span>
				</a>
				<span class="tooltip">Master Data</span>
				<div id="masterdata_menu" class="collapse">
					<ul class="nav-sublist">
						<li class="nav-sublist-item dropdown sub-barang">
							<a class="sublist-item-menu" style="padding-left: 10px;" id="barang" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">
								<i class="links_icon far fa-circle fa-sm"></i>
								<span class="links_name">Barang</span>
							</a>
							<div class="dropdown-menu" aria-labelledby="barang">
								<?php foreach ($otoritasmenu as $value) :
									$menu = $this->db->query("SELECT * FROM stpm_menu WHERE kodemenu = '" . $value->kodemenu . "' AND grupmenu = 'Barang' AND aktif = true order by nourut")->result();
								?>
									<?php foreach ($menu as $value) : ?>
										<a class="dropdown-item p-1" style="padding-left: 40px !important;" href="<?php echo base_url(); ?>main/<?php echo $value->linkmenu ?>">
											<i class="far fa-circle fa-sm"></i>
											<?php echo $value->namamenu ?>
										</a>
									<?php endforeach ?>
								<?php endforeach ?>
							</div>
						</li>
						<li class="nav-sublist-item dropdown sub-cabang">
							<a class="sublist-item-menu" style="padding-left: 10px;" id="cabang" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">
								<i class="links_icon far fa-circle fa-sm"></i>
								<span class="links_name">Cabang</span>
							</a>
							<div class="dropdown-menu" aria-labelledby="cabang">
								<?php foreach ($otoritasmenu as $value) :
									$menu = $this->db->query("SELECT * FROM stpm_menu WHERE kodemenu = '" . $value->kodemenu . "' AND grupmenu = 'Cabang' AND aktif = true order by nourut")->result();
								?>
									<?php foreach ($menu as $value) : ?>
										<a class="dropdown-item p-1" style="padding-left: 40px !important;" href="<?php echo base_url(); ?>main/<?php echo $value->linkmenu ?>">
											<i class="far fa-circle fa-sm"></i>
											<?php echo $value->namamenu ?>
										</a>
									<?php endforeach ?>
								<?php endforeach ?>
							</div>
						</li>
						<li class="nav-sublist-item dropdown sub-diskoncabang">
							<a class="sublist-item-menu" style="padding-left: 10px;" id="diskoncabang" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">
								<i class="links_icon far fa-circle fa-sm"></i>
								<span class="links_name">Diskon Cabang</span>
							</a>
							<div class="dropdown-menu" aria-labelledby="diskoncabang">
								<?php foreach ($otoritasmenu as $value) :
									$menu = $this->db->query("SELECT * FROM stpm_menu WHERE kodemenu = '" . $value->kodemenu . "' AND grupmenu = 'Diskon Cabang' AND aktif = true order by nourut")->result();
								?>
									<?php foreach ($menu as $value) : ?>
										<a class="dropdown-item p-1" style="padding-left: 40px !important;" href="<?php echo base_url(); ?>main/<?php echo $value->linkmenu ?>">
											<i class="far fa-circle fa-sm"></i>
											<?php echo $value->namamenu ?>
										</a>
									<?php endforeach ?>
								<?php endforeach ?>
							</div>
						</li>
						<li class="nav-sublist-item dropdown sub-diskonglobal">
							<a class="sublist-item-menu" style="padding-left: 10px;" id="diskonglobal" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">
								<i class="links_icon far fa-circle fa-sm"></i>
								<span class="links_name">Diskon Global</span>
							</a>
							<div class="dropdown-menu" aria-labelledby="diskonglobal">
								<?php foreach ($otoritasmenu as $value) :
									$menu = $this->db->query("SELECT * FROM stpm_menu WHERE kodemenu = '" . $value->kodemenu . "' AND grupmenu = 'Diskon Global' AND aktif = true order by nourut")->result();
								?>
									<?php foreach ($menu as $value) : ?>
										<a class="dropdown-item p-1" style="padding-left: 40px !important;" href="<?php echo base_url(); ?>main/<?php echo $value->linkmenu ?>">
											<i class="far fa-circle fa-sm"></i>
											<?php echo $value->namamenu ?>
										</a>
									<?php endforeach ?>
								<?php endforeach ?>
							</div>
						</li>
						<li class="nav-sublist-item dropdown sub-global">
							<a class="sublist-item-menu" style="padding-left: 10px;" id="global" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">
								<i class="links_icon far fa-circle fa-sm"></i>
								<span class="links_name">Global</span>
							</a>
							<div class="dropdown-menu" aria-labelledby="global">
								<?php foreach ($otoritasmenu as $value) :
									$menu = $this->db->query("SELECT * FROM stpm_menu WHERE kodemenu = '" . $value->kodemenu . "' AND grupmenu = 'Global' AND aktif = true order by nourut")->result();
								?>
									<?php foreach ($menu as $value) : ?>
										<a class="dropdown-item p-1" style="padding-left: 40px !important;" href="<?php echo base_url(); ?>main/<?php echo $value->linkmenu ?>">
											<i class="far fa-circle fa-sm"></i>
											<?php echo $value->namamenu ?>
										</a>
									<?php endforeach ?>
								<?php endforeach ?>
							</div>
						</li>
					</ul>
				</div>
			</li>
			<li class="nav-list-item collapsed menu-user" data-tooltip="tooltip_user" data-toggle="collapse" data-target="#user">
				<a class="list-item-menu" href="#">
					<i class="links_icon fas fa-user-circle" style="font-size: 20px;"></i>
					<span class="links_name">Akun</span>
					<!-- <span class="badge badge-pill badge-warning">New</span> -->
				</a>
				<span class="tooltip">Akun</span>
				<div id="user" class="collapse">
					<ul class="nav-sublist">
						<?php foreach ($otoritasmenu as $value) :
							$menu = $this->db->query("SELECT * FROM stpm_menu WHERE kodemenu = '" . $value->kodemenu . "' AND grupmenu = 'Akun' AND aktif = true ORDER BY nourut")->result();
						?>
							<?php foreach ($menu as $value) : ?>
								<li class="nav-sublist-item">
									<a class="sublist-item-menu" style="padding-left: 10px;" href="<?= base_url(); ?>main/<?php echo $value->linkmenu ?>">
										<i class="links_icon far fa-circle fa-sm"></i>
										<span class="links_name"><?= $value->namamenu ?></span>
									</a>
								</li>
							<?php endforeach ?>
						<?php endforeach ?>
					</ul>
				</div>
			</li>
			<li class="nav-list-item collapsed menu-report" data-tooltip="tooltip_report" data-toggle="collapse" data-target="#report">
				<a class="list-item-menu" href="#">
					<i class="links_icon fas fa-file-invoice" style="font-size: 20px;"></i>
					<span class="links_name">Reporting</span>
					<!-- <span class="badge badge-pill badge-warning">New</span> -->
				</a>
				<span class="tooltip">Reporting</span>
				<div id="report" class="collapse">
					<ul class="nav-sublist">
						<?php foreach ($otoritasmenu as $value) :
							$menu = $this->db->query("SELECT * FROM stpm_menu WHERE kodemenu = '" . $value->kodemenu . "' AND grupmenu = 'Report' AND aktif = true ORDER BY nourut")->result();
						?>
							<?php foreach ($menu as $value) : ?>
								<li class="nav-sublist-item">
									<a class="sublist-item-menu" style="padding-left: 10px;" href="<?= base_url(); ?>main/<?php echo $value->linkmenu ?>">
										<i class="links_icon far fa-circle fa-sm"></i>
										<span class="links_name"><?= $value->namamenu ?></span>
									</a>
								</li>
							<?php endforeach ?>
						<?php endforeach ?>
					</ul>
				</div>
			</li>
			<li class="profile">
				<div class="profile-details">
					<!--<img src="profile.jpg" alt="profileImg">-->
					<div class="name_job">
						<?php foreach ($userData as $val) : ?>
							<div class="name text-uppercase"><?= $val->username ?></div>
							<div class="job"><?= $val->grup ?></div>
						<?php endforeach; ?>
					</div>
				</div>
				<i class="bx bx-log-out btn-sidebar-logout" id="log_out" href="#logoutModal" data-toggle="modal"></i>
			</li>
		</ul>
	</div>
</div>
<div class="sidebar-hoverlay"></div>