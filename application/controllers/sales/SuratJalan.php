<?php
defined('BASEPATH') or exit('no direct script access allowed');

class SuratJalan extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('sales/Suratjalan_model');
		$this->load->model('global/global_model');
		$this->load->model('Caridataaktif_model');
		$this->load->library('form_validation');
		$this->load->library('session');
	}

	function CariDataSuratJalan()
	{
		$fetch_data = $this->Caridataaktif_model->make_datatables($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value'));
		$data = array();
		foreach ($fetch_data as $row) {
			$sub_array = array();
			$i = 1;
			$count = count($this->input->post('field'));
			foreach ($this->input->post('field') as $key => $value) {
				if ($i <= $count) {
					if ($i == 1) {
						$msearch = $row->$value;
						$sub_array[] = '<button class="btn btn-dark searchsj" data-dismiss="modal" style="margin: 0px;" data-id="' . $msearch . '"><i class="far fa-check-circle"></i></button>';
						$sub_array[] = $row->$value;
					} else {
						if ($i == $count) {
							$sub_array[] = $row->$value;
						} else {
							$sub_array[] = $row->$value;
						}
					}
					// $sub_array[] = $row->$value;
				}
				$i++;
			}
			$data[] = $sub_array;
		}
		$output = array(
			"draw"                    =>     intval($_POST["draw"]),
			"recordsTotal"          =>      $this->Caridataaktif_model->get_all_data($this->input->post('nmtb')),
			"recordsFiltered"     =>     $this->Caridataaktif_model->get_filtered_data($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value')),
			"data"                    =>     $data
		);
		echo json_encode($output);
	}

	function DataSuratJalan()
	{
		$result = $this->Suratjalan_model->DataSJ($this->input->post('nomor'));
		echo json_encode($result);
	}

	function DataSuratJalanDetail()
	{
		$result = $this->Suratjalan_model->DataSJDetail($this->input->post('nomor'));
		echo json_encode($result);
	}

	function CariDataInvoice()
	{
		$fetch_data = $this->Caridataaktif_model->make_datatables($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value'));
		$data = array();
		foreach ($fetch_data as $row) {
			$sub_array = array();
			$i = 1;
			$count = count($this->input->post('field'));
			foreach ($this->input->post('field') as $key => $value) {
				if ($i <= $count) {
					if ($i == 1) {
						$msearch = $row->$value;
						$sub_array[] = '<button class="btn btn-dark searchinvoice" data-dismiss="modal" style="margin: 0px;" data-id="' . $msearch . '"><i class="far fa-check-circle"></i></button>';
						$sub_array[] = $row->$value;
					} else {
						if ($i == $count) {
							$sub_array[] = $row->$value;
						} else {
							$sub_array[] = $row->$value;
						}
					}
					// $sub_array[] = $row->$value;
				}
				$i++;
			}
			$data[] = $sub_array;
		}
		$output = array(
			"draw"                    =>     intval($_POST["draw"]),
			"recordsTotal"          =>      $this->Caridataaktif_model->get_all_data($this->input->post('nmtb')),
			"recordsFiltered"     =>     $this->Caridataaktif_model->get_filtered_data($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value')),
			"data"                    =>     $data
		);
		echo json_encode($output);
	}

	function DataInvoice()
	{
		$result = $this->Suratjalan_model->DataInvoice($this->input->post('nomor'));
		// print_r($result);
		// die();
		echo json_encode($result);
	}

	function CariDataSJTanggal()
	{
		$fetch_data = $this->Caridataaktif_model->make_datatables($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value'));
		$data = array();
		foreach ($fetch_data as $row) {
			$sub_array = array();
			$i = 1;
			$count = count($this->input->post('field'));
			foreach ($this->input->post('field') as $key => $value) {
				if ($i <= $count) {
					if ($i == 1) {
						$msearch = $row->$value;
						// $sub_array[] = '<button class="btn btn-dark searchpdf" style="margin: 0px;" data-id="' . $msearch . '"><i class="far fa-check-circle"></i></button>';
						$sub_array[] = $row->$value;
					} else {
						if ($i == $count) {
							$sub_array[] = $row->$value;
						} else {
							$sub_array[] = $row->$value;
						}
					}
					// $sub_array[] = $row->$value;
				}
				$i++;
			}
			$data[] = $sub_array;
		}
		$output = array(
			"draw"                    =>     intval($_POST["draw"]),
			"recordsTotal"          =>      $this->Caridataaktif_model->get_all_data($this->input->post('nmtb')),
			"recordsFiltered"     =>     $this->Caridataaktif_model->get_filtered_data($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value')),
			"data"                    =>     $data
		);
		echo json_encode($output);
	}

	function Save()
	{
		$kodecabang = "CBGKDL001";
		$pemakai = "VSP";
		$nomor = $this->input->post('nomorsj');

		$this->db->trans_start();
		$this->db->trans_strict(FALSE);

		$ceknomor = $this->Suratjalan_model->CekSJ($nomor);

		$ambilnomor = "SJ" . "BVS" . substr(date("Y"), 2, 2) . date("m");
		$get["sj"] = $this->Suratjalan_model->GetMaxNomor($ambilnomor);
		if (!$get["sj"]) {
			$nomor = $ambilnomor . "0001";
		} else {
			$lastNomor = $get["sj"]->nomor;
			$lastNoUrut = substr($lastNomor, 9, 13);

			// nomor urut ditambah 1

			$nextNourut = $lastNoUrut + 1;
			$nomor = $ambilnomor . sprintf('%04s', $nextNourut);
		}

		$datapkg = $this->Suratjalan_model->DataPacking($this->input->post('noinvoice'));

		if (empty($ceknomor)) {
			if (!empty($this->input->post('datadetail'))) {
				foreach ($this->input->post('datadetail') as $key => $value) {
					$datadetail = [
						'nomor' => $nomor,
						'tanggal' => $this->input->post('tanggalsj'),
						'kodebarang' => $value['Kode'],
						'namabarang' => $value['Nama'],
						'qty' => $value['Qty'],
						'tglsimpan' => date('Y-m-d H:i:s'),
						'pemakai' => $pemakai
					];
					$this->Suratjalan_model->SaveDataDetail($datadetail);
				}
			}

			$data = [
				'nomor' => $nomor,
				'tanggal' => $this->input->post('tanggalsj'),
				'noinvoice' => $this->input->post('noinvoice'),
				'kodegudang' => $this->input->post('lokasi'),
				'kodecabang' => $kodecabang,
				'pemakai' => $pemakai,
				'tglsimpan' => date('Y-m-d H:i:s'),
			];
			$this->Suratjalan_model->SaveData($data);

			$datapacking = [
				'status' => true
			];
			$this->Suratjalan_model->UpdateDataPacking($datapkg->nopacking, $datapacking);

			$this->db->trans_complete();

			if ($this->db->trans_status() === FALSE) {
				$resultjson = array(
					'nomor' => "",
					'message' => "Data gagal disimpan, nomor '<b style='color: red'>" . $nomor . "</b>'  sudah pernah digunakan."
				);
				# Something went wrong.
				$this->db->trans_rollback();
			} else {
				$resultjson = array(
					'nomor' => $nomor,
					'message' => "Data berhasil disimpan."
				);
				# Everything is Perfect. 
				# Committing data to the database.
				$this->db->trans_commit();
			}
			echo json_encode($resultjson);
		} else {
			$this->db->trans_complete();
			$resultjson = array(
				'nomor' => "",
				'message' => "Data gagal disimpan, nomor '<b style='color: red'>" . $nomor . "</b>' sudah pernah digunakan."
			);
			# Something went wrong.
			$this->db->trans_rollback();
			echo json_encode($resultjson);
		}
	}

	function Update()
	{
		$kodecabang = "CBGKDL001";
		$pemakai = "VSP";
		$nomor = $this->input->post('nomorsj');

		$this->db->trans_start();
		$this->db->trans_strict(FALSE);

		if (!empty($this->input->post('datadetail'))) {
			$this->Suratjalan_model->DeleteDetail($nomor);
			foreach ($this->input->post('datadetail') as $key => $value) {
				$datadetail = [
					'nomor' => $nomor,
					'tanggal' => $this->input->post('tanggalsj'),
					'kodebarang' => $value['Kode'],
					'namabarang' => $value['Nama'],
					'qty' => $value['Qty'],
					'tglsimpan' => date('Y-m-d H:i:s'),
					'pemakai' => $pemakai
				];
				$this->Suratjalan_model->SaveDataDetail($datadetail);
			}
		}

		$data = [
			'nomor' => $nomor,
			'tanggal' => $this->input->post('tanggalsj'),
			'noinvoice' => $this->input->post('noinvoice'),
			'kodegudang' => $this->input->post('lokasi'),
			'kodecabang' => $kodecabang,
			'pemakai' => $pemakai,
			'tglsimpan' => date('Y-m-d H:i:s'),
		];
		$this->Suratjalan_model->UpdateData($data, $nomor);

		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE) {
			$resultjson = array(
				'nomor' => "",
				'message' => "Data gagal disimpan, nomor '<b style='color: red'>" . $nomor . "</b>'  sudah pernah digunakan."
			);
			# Something went wrong.
			$this->db->trans_rollback();
		} else {
			$resultjson = array(
				'nomor' => $nomor,
				'message' => "Data berhasil disimpan."
			);
			# Everything is Perfect. 
			# Committing data to the database.
			$this->db->trans_commit();
		}
		echo json_encode($resultjson);
	}

	// function getdatapdf()
	// {
	// 	$result = $this->db->get_where('srvt_suratjalan', array('nomor' => $this->input->post('id')))->result();
	// 	echo json_encode($result);
	// }
}
