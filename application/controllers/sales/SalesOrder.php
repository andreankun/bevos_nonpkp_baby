<?php
defined('BASEPATH') or exit('No direct script access allowed');

class SalesOrder extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('sales/Salesorder_model');
		$this->load->model('inventory/PackingBarang_model');
		$this->load->model('global/global_model');
		$this->load->model('Caridataaktif_model');
		$this->load->library('form_validation');
		$this->load->library('session');
	}

	function CariDataSalesOrder()
	{
		$fetch_data = $this->Caridataaktif_model->make_datatables($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value'));
		$data = array();
		foreach ($fetch_data as $row) {
			$sub_array = array();
			$i = 1;
			$count = count($this->input->post('field'));
			foreach ($this->input->post('field') as $key => $value) {
				if ($i <= $count) {
					if ($i == 1) {
						$msearch = $row->$value;
						$sub_array[] = '<button class="btn btn-dark searchso" data-dismiss="modal" style="margin: 0px;" data-id="' . $msearch . '"><i class="far fa-check-circle"></i></button>';
						$sub_array[1] = '<button class="btn btn-sm btn-dark cetakproforma" style="margin: 0px; background-color: #007bff; border-color: #007bff;" data-id="' . $msearch . '"><i class="fas fa-print"></i>&nbsp;Cetak Proforma</button>';
						$sub_array[2] = '<button class="btn btn-sm btn-dark cetaksalesorder" style="margin: 0px; background-color: salmon; border-color: salmon;" data-id="' . $msearch . '"><i class="fas fa-print"></i>&nbsp;Cetak Sales Order</button>';
						$sub_array[] = $row->$value;
					} else {
						if ($i == $count) {
							$sub_array[] = $row->$value;
						} else {
							$sub_array[] = $row->$value;
						}
					}
					// $sub_array[] = $row->$value;
				}
				$i++;
			}
			$data[] = $sub_array;
		}
		$output = array(
			"draw"                    =>     intval($_POST["draw"]),
			"recordsTotal"          =>      $this->Caridataaktif_model->get_all_data($this->input->post('nmtb')),
			"recordsFiltered"     =>     $this->Caridataaktif_model->get_filtered_data($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value')),
			"data"                    =>     $data
		);
		echo json_encode($output);
	}

	function CariDataRacikan()
	{
		$fetch_data = $this->Caridataaktif_model->make_datatables($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value'));
		$data = array();
		foreach ($fetch_data as $row) {
			$sub_array = array();
			$i = 1;
			$count = count($this->input->post('field'));
			foreach ($this->input->post('field') as $key => $value) {
				if ($i <= $count) {
					if ($i == 1) {
						$msearch = $row->$value;
						$sub_array[] = '<button class="btn btn-dark searchracikan" data-dismiss="modal" style="margin: 0px;" data-id="' . $msearch . '"><i class="far fa-check-circle"></i></button>';
						$sub_array[] = $row->$value;
					} else {
						if ($i == $count) {
							$sub_array[] = $row->$value;
						} else {
							$sub_array[] = $row->$value;
						}
					}
					// $sub_array[] = $row->$value;
				}
				$i++;
			}
			$data[] = $sub_array;
		}
		$output = array(
			"draw"                    =>     intval($_POST["draw"]),
			"recordsTotal"          =>      $this->Caridataaktif_model->get_all_data($this->input->post('nmtb')),
			"recordsFiltered"     =>     $this->Caridataaktif_model->get_filtered_data($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value')),
			"data"                    =>     $data
		);
		echo json_encode($output);
	}

	function DataRacikan()
	{
		$result = $this->Salesorder_model->DataRacikan($this->input->post('kode'));
		echo json_encode($result);
	}
	function CariDataBarang()
	{
		$fetch_data = $this->Caridataaktif_model->make_datatables($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value'));
		$data = array();
		foreach ($fetch_data as $row) {
			$sub_array = array();
			$i = 1;
			$count = count($this->input->post('field'));
			foreach ($this->input->post('field') as $key => $value) {
				if ($i <= $count) {
					if ($i == 1) {
						$msearch = $row->$value;
						$sub_array[] = '<button class="btn btn-dark searchbarang" data-dismiss="modal" style="margin: 0px;" data-id="' . $msearch . '"><i class="far fa-check-circle"></i></button>';
						$sub_array[] = $row->$value;
					} else {
						if ($i == $count) {
							$sub_array[] = $row->$value;
						} else {
							$sub_array[] = $row->$value;
						}
					}
					// $sub_array[] = $row->$value;
				}
				$i++;
			}
			$data[] = $sub_array;
		}
		$output = array(
			"draw"                    =>     intval($_POST["draw"]),
			"recordsTotal"          =>      $this->Caridataaktif_model->get_all_data($this->input->post('nmtb')),
			"recordsFiltered"     =>     $this->Caridataaktif_model->get_filtered_data($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value')),
			"data"                    =>     $data
		);
		echo json_encode($output);
	}

	function DataBarang()
	{
		$result = $this->Salesorder_model->DataBarang($this->input->post('kode'), $this->input->post('jenisracikan'), $this->input->post('cabang'), $this->input->post('gudang'), $this->input->post('sales'));
		// print_r($result);
		// die();
		echo json_encode($result);
	}

	function CariDataHarga()
	{
		$fetch_data = $this->Caridataaktif_model->make_datatables($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value'));
		$data = array();
		foreach ($fetch_data as $row) {
			$sub_array = array();
			$i = 1;
			$count = count($this->input->post('field'));
			foreach ($this->input->post('field') as $key => $value) {
				if ($i <= $count) {
					if ($i == 1) {
						$msearch = $row->$value;
						$sub_array[] = '<button class="btn btn-dark searchharga" data-dismiss="modal" style="margin: 0px;" data-id="' . $msearch . '"><i class="far fa-check-circle"></i></button>';
						$sub_array[] = $row->$value;
					} else {
						if ($i == $count) {
							$sub_array[] = $row->$value;
						} else {
							$sub_array[] = $row->$value;
						}
					}
					// $sub_array[] = $row->$value;
				}
				$i++;
			}
			$data[] = $sub_array;
		}
		$output = array(
			"draw"                    =>     intval($_POST["draw"]),
			"recordsTotal"          =>      $this->Caridataaktif_model->get_all_data($this->input->post('nmtb')),
			"recordsFiltered"     =>     $this->Caridataaktif_model->get_filtered_data($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value')),
			"data"                    =>     $data
		);
		echo json_encode($output);
	}

	function DataHarga()
	{
		$result = $this->Salesorder_model->DataHarga($this->input->post('nomor'), $this->input->post('cabang'));
		// print_r($result);
		// die();
		echo json_encode($result);
	}

	function CariDataFree()
	{
		$fetch_data = $this->Caridataaktif_model->make_datatables($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value'));
		$data = array();
		foreach ($fetch_data as $row) {
			$sub_array = array();
			$i = 1;
			$count = count($this->input->post('field'));
			foreach ($this->input->post('field') as $key => $value) {
				if ($i <= $count) {
					if ($i == 1) {
						$msearch = $row->$value;
						$sub_array[] = '<button class="btn btn-dark searchfree" data-dismiss="modal" style="margin: 0px;" data-table="'.$msearch.'" data-toggle="modal" data-target="#changefree"><i class="far fa-check-circle"></i></button>';
						$sub_array[] = $row->$value;
					} else {
						if ($i == $count) {
							$sub_array[] = $row->$value;
						} else {
							$sub_array[] = $row->$value;
						}
					}
					// $sub_array[] = $row->$value;
				}
				$i++;
			}
			$data[] = $sub_array;
		}
		$output = array(
			"draw"                    =>     intval($_POST["draw"]),
			"recordsTotal"          =>      $this->Caridataaktif_model->get_all_data($this->input->post('nmtb')),
			"recordsFiltered"     =>     $this->Caridataaktif_model->get_filtered_data($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value')),
			"data"                    =>     $data
		);
		echo json_encode($output);
	}

	public function DataFreeBarang(){
		$result = $this->Salesorder_model->DataFreeBarang($this->input->post('kode'), $this->input->post('cabang'), $this->input->post('kodebarang'));
		// print_r($result);
		// die();
		echo json_encode($result);
	}

	function DataPelanggan()
	{
		$result = $this->Salesorder_model->DataCustomer($this->input->post('nomor'));
		// print_r($result);
		// die();
		echo json_encode($result);
	}

	public function CariDataSO()
	{
		$fetch_data = $this->Caridataaktif_model->make_datatables($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value'));
		$data = array();
		foreach ($fetch_data as $row) {
			$sub_array = array();
			$i = 1;
			$count = count($this->input->post('field'));
			foreach ($this->input->post('field') as $key => $value) {
				if ($i <= $count) {
					if ($i == 1) {
						$msearch = $row->$value;
						$sub_array[] = '<button class="btn btn-dark searchso" data-dismiss="modal" style="margin: 0px;" data-id="' . $msearch . '"><i class="far fa-check-circle"></i></button>';
						$sub_array[] = $row->value;
					} else {
						if ($i == $count) {
							$sub_array[] = $row->value;
						} else {
							$sub_array[] = $row->value;
						}
					}
				}
				$i++;
			}
			$data[] = $sub_array;
		}
		$output = array(
			"draw"                    =>     intval($_POST["draw"]),
			"recordsTotal"          =>      $this->Caridataaktif_model->get_all_data($this->input->post('nmtb')),
			"recordsFiltered"     =>     $this->Caridataaktif_model->get_filtered_data($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value')),
			"data"                    =>     $data
		);
		echo json_encode($output);
	}

	function DataSO()
	{
		$result = $this->Salesorder_model->DataSO($this->input->post('nomor'));
		echo json_encode($result);
	}

	function DataSODetail()
	{
		$result = $this->Salesorder_model->DataSODetail($this->input->post('nomor'));
		echo json_encode($result);
	}

	public function ClearChr($param)
	{
		return $result = str_replace(array('\'', '"', ',', ';', '<', '>', '_', '-', ' ', '.', '!', '?', '/', '=', '+', ']', '[', '{', '}', '@', '#', '$', '%', '^', '&', "*", '(', ')'), '', $param);
	}


	function Save()
	{

		$pemakai =  $this->session->userdata('myusername');
		$nomor = "";
		$periode = date('Y') . date('m');
		$kodegudang = $this->input->post('lokasi');
		$jenisracikan = $this->input->post('jenisracikan');
		$cabang = $this->input->post('cabang');
		$gudang = $this->input->post('gudang');
		$errorvalidasi = FALSE;

		if (!empty($this->input->post('datadetail'))) {
			foreach ($this->input->post('datadetail') as $key => $value) {
				// print_r($value);
				// die();
				$where = array(
					"periode" => $periode,
					"kodecabang" => $cabang,
					"kodegudang" => $kodegudang,
					"kodebarang" => $value['Kode']
				);

				$whereracikan = [
					"kode" => $jenisracikan,
					"kodebarang" => $value['Kode'],
					"diskon" => $value['DiscPromo']
				];
				if ($jenisracikan != "") {
					$dataracikan = $this->global_model->GetDataGlobal("cari_promo", $whereracikan);
					if (empty($dataracikan)) {
						$resultjson = array(
							'nomor' => "",
							'message' => "Data gagal disimpan, Racikan Tidak Ada."
						);
						$errorvalidasi = TRUE;
						echo json_encode($resultjson);
						return FALSE;
					}
				}
				$datastock = $this->global_model->GetDataGlobal("srvt_inventorystok", $where);
				if (empty($datastock)) {
					$resultjson = array(
						'nomor' => "",
						'message' => "Data gagal disimpan, Barang '<b style='color: red'>" . $value['Nama'] . "</b>' Stock Tidak Ada."
					);
					$errorvalidasi = TRUE;
					echo json_encode($resultjson);
					return FALSE;
				} else {
					foreach ($datastock as $key => $values) {
						$stocksaatini = $values->qtyawal + $values->qtymasuk - $values->qtykeluar - $values->qtypick;
						if (floatval(str_replace(",", "", $value['Qty'])) > $stocksaatini) {
							$resultjson = array(
								'nomor' => "",
								'message' => "Data gagal disimpan, Barang '<b style='color: red'>" . $value['Nama'] . "</b>' Stock Tidak Cukup."
							);
							$errorvalidasi = TRUE;
							echo json_encode($resultjson);
							return FALSE;
						}
					}
				}
			}
		}

		if (empty($this->input->post('datadetail'))) {
			$resultjson = array(
				'nomor' => "",
				'message' => "Data gagal disimpan, Data Detail Tidak Ada."
			);
			$errorvalidasi = TRUE;
			echo json_encode($resultjson);
			return FALSE;
		} else {
			if ($jenisracikan != "") {
				foreach ($this->input->post('datadetail') as $key => $value) {
					if ($value['DiscPromo'] == 0 || $value['DiscPromo'] = '0') {
						$resultjson = array(
							'nomor' => "",
							'message' => "Data gagal disimpan, Diskon Promo  '<b style='color: red'>" . $jenisracikan . "</b>' Tidak ada terpakai."
						);
						$errorvalidasi = TRUE;
						echo json_encode($resultjson);
						return FALSE;
					}
				}
			}
		}


		$ambilnomor = "O" . $cabang . '-' . date("Y") . '-' . date("m");
		$get["so"] = $this->Salesorder_model->GetMaxNomor($ambilnomor);

		if (!$get["so"]->nomor) {
			$nomor = $ambilnomor . "-" . "00001";
		} else {
			$lastNomor = $get['so']->nomor;
			// print_r($lastNomor);
			// die();
			$lastNoUrut = substr($lastNomor, 11, 11);
			// nomor urut ditambah 1
			$nextNoUrut = $lastNoUrut + 1;
			$nomor = $ambilnomor . "-" . sprintf('%05s', $nextNoUrut);
		}
		if (empty($nomor) || $nomor == "") {
			$resultjson = array(
				'nomor' => "",
				'message' => "Data gagal disimpan, Error Generate Nomor."
			);
			$errorvalidasi = TRUE;
			echo json_encode($resultjson);
			return FALSE;
		}

		if ($errorvalidasi == FALSE) {
			$this->db->trans_start(); # Starting Transaction
			$this->db->trans_strict(FALSE);

			if ($this->input->post('gudang') == 'false') {
				foreach ($this->input->post('datadetail') as $key => $value) {
					// print_r($value);
					// die();
					$where = array(
						"periode" => $periode,
						"kodecabang" => $cabang,
						"kodegudang" => $kodegudang,
						"kodebarang" => $value['Kode']
					);
					$datastock = $this->global_model->GetDataGlobal("srvt_inventorystok", $where);
					if (!empty($datastock)) {
						foreach ($datastock as $key => $value2) {
							$datasodetail = array(
								'nomor' => $nomor,
								'kodebarang' => $value['Kode'],
								'namabarang' => $value['Nama'],
								'qty' => floatval(str_replace(",", "", $value['Qty'])),
								'harga' => floatval(str_replace(",", "", $value['Harga'])),
								'discbarang' => $value['Disc'],
								'discbrgitem' => floatval(str_replace(",", "", $value['DiscItem'])),
								'discpromo' => $value['DiscPromo'],
								'discpaketpromo' => floatval(str_replace(",", "", $value['DiscPromoItem'])),
								'total' => floatval(str_replace(",", "", $value['Subtotal'])),
								'pemakai' => $pemakai,
								'tglsimpan' => date('Y-m-d H:i:s'),
							);
							$this->global_model->InsertDataGlobal("srvt_sodetail", $datasodetail);

							$qtypicka = $value2->qtypick + floatval(str_replace(",", "", $value['Qty']));
							$setdata = "qtypick = '" . $qtypicka . "'";
							$wheredata = "periode = '" . $periode . "' AND kodecabang = '" . $cabang . "' AND kodegudang = '" . $kodegudang . "' AND kodebarang = '" . $value['Kode'] . "'";
							$this->Salesorder_model->UpdateDataInventory("srvt_inventorystok", $setdata, $wheredata);
						}
					}
				}

				$ambilnomor = "I" . $cabang . '-' . date("Y") . '-' . date("m");

				if (empty($nomorpacking)) {
					$get["pb"] = $this->PackingBarang_model->GetMaxNomor($ambilnomor);
					if (!$get["pb"]) {
						$nomorpacking = $ambilnomor . '-' . "00001";
					} else {
						$lastNomor = $get['pb']->nomor;
						$lastNoUrut = substr($lastNomor, 11, 11);

						// nomor urut ditambah 1
						$nextNoUrut = $lastNoUrut + 1;
						$nomorpacking = $ambilnomor . '-' . sprintf('%05s', $nextNoUrut);
					}
					if (empty($ceknomor)) {
						if (!empty($this->input->post('datadetail'))) {
							foreach ($this->input->post('datadetail') as $key => $valuepacking) {
								$datadetail = [
									'nomor' => $nomorpacking,
									'kodebarang' => $valuepacking['Kode'],
									'namabarang' => $valuepacking['Nama'],
									'qty' => floatval(str_replace(",", "", $valuepacking['Qty'])),
									'pemakai' => $pemakai,
									'tglsimpan' => date('Y-m-d H:i:s'),
								];
								$this->PackingBarang_model->SaveDataDetail($datadetail);
							}

							$data = [
								'nomor' => $nomorpacking,
								'tanggal' => $this->input->post('tanggal'),
								'nomorso' => $nomor,
								'tglso' => date('Y-m-d H:i:s'),
								'pemakai' => $pemakai,
								'tglsimpan' => date('Y-m-d H:i:s'),
								'kodecabang' => $cabang,
							];
							$this->PackingBarang_model->SaveData($data);
						}
					}
				}

				$dataheader = array(
					'nomor' => $nomor,
					'tanggal' => $this->input->post('tanggal'),
					'nopelanggan' => $this->input->post('nopelanggan'),
					'namapelanggan' => $this->input->post('namapelanggan'),
					'plafonso' => floatval(str_replace(",", "", $this->input->post('plafonso'))),
					'outstandingar' => floatval(str_replace(",", "", $this->input->post('outstandingar'))),
					'kodesalesman' => $this->input->post('kodeslm'),
					'namasalesman' => $this->input->post('namaslm'),
					'jenisjual' => $this->input->post('jenisjual'),
					'jenisso' => $this->input->post('jenisso'),
					'lokasi' => $this->input->post('lokasi'),
					'jenisdisc' => $this->input->post('jenisracikan'),
					'disccash' => floatval(str_replace(",", "", $this->input->post('discash'))),
					'disccustomer' => floatval(str_replace(",", "", $this->input->post('discustomer'))),
					'nilaicash' => floatval(str_replace(",", "", $this->input->post('nilaicash'))),
					'nilaicustomer' => floatval(str_replace(",", "", $this->input->post('nilaicustomer'))),
					'total' => floatval(str_replace(",", "", $this->input->post('total'))),
					'dpp' => floatval(str_replace(",", "", $this->input->post('dpp'))),
					'ppn' => floatval(str_replace(",", "", $this->input->post('ppn'))),
					'grandtotal' => floatval(str_replace(",", "", $this->input->post('grandtotal'))),
					'gudang' => $gudang,
					'kodestatus' => 1,
					'pemakai' => $pemakai,
					'tglsimpan' => date('Y-m-d H:i:s'),
					'kodecabang' => $cabang,
					'keterangan' => $this->input->post('keterangan'),
					'discmanual' => floatval(str_replace(",", "", $this->input->post('discmanual'))),
					'nilaimanual' => floatval(str_replace(",", "", $this->input->post('nilaimanual')))
				);
				// print_r($dataheader);
				// die();
				$this->Salesorder_model->InsertData("srvt_salesorder", $dataheader);
			} else {
				foreach ($this->input->post('datadetail') as $key => $value) {
					// print_r($value);
					// die();
					$where = array(
						"periode" => $periode,
						"kodecabang" => $cabang,
						"kodegudang" => $kodegudang,
						"kodebarang" => $value['Kode']
					);
					$datastock = $this->global_model->GetDataGlobal("srvt_inventorystok", $where);
					if (!empty($datastock)) {
						foreach ($datastock as $key => $value2) {
							$datasodetail = array(
								'nomor' => $nomor,
								'kodebarang' => $value['Kode'],
								'namabarang' => $value['Nama'],
								'qty' => floatval(str_replace(",", "", $value['Qty'])),
								'harga' => floatval(str_replace(",", "", $value['Harga'])),
								'discbarang' => $value['Disc'],
								'discbrgitem' => floatval(str_replace(",", "", $value['DiscItem'])),
								'discpromo' => $value['DiscPromo'],
								'discpaketpromo' => floatval(str_replace(",", "", $value['DiscPromoItem'])),
								'total' => floatval(str_replace(",", "", $value['Subtotal'])),
								'pemakai' => $pemakai,
								'tglsimpan' => date('Y-m-d H:i:s'),
							);
							$this->global_model->InsertDataGlobal("srvt_sodetail", $datasodetail);

							$qtypicka = $value2->qtypick + floatval(str_replace(",", "", $value['Qty']));
							$setdata = "qtypick = '" . $qtypicka . "'";
							$wheredata = "periode = '" . $periode . "' AND kodecabang = '" . $cabang . "' AND kodegudang = '" . $kodegudang . "' AND kodebarang = '" . $value['Kode'] . "'";
							$this->Salesorder_model->UpdateDataInventory("srvt_inventorystok", $setdata, $wheredata);
						}
					}
				}

				$dataheader = array(
					'nomor' => $nomor,
					'tanggal' => $this->input->post('tanggal'),
					'nopelanggan' => $this->input->post('nopelanggan'),
					'namapelanggan' => $this->input->post('namapelanggan'),
					'plafonso' => floatval(str_replace(",", "", $this->input->post('plafonso'))),
					'outstandingar' => floatval(str_replace(",", "", $this->input->post('outstandingar'))),
					'kodesalesman' => $this->input->post('kodeslm'),
					'namasalesman' => $this->input->post('namaslm'),
					'jenisjual' => $this->input->post('jenisjual'),
					'jenisso' => $this->input->post('jenisso'),
					'lokasi' => $this->input->post('lokasi'),
					'jenisdisc' => $this->input->post('jenisracikan'),
					'disccash' => floatval(str_replace(",", "", $this->input->post('discash'))),
					'disccustomer' => floatval(str_replace(",", "", $this->input->post('discustomer'))),
					'nilaicash' => floatval(str_replace(",", "", $this->input->post('nilaicash'))),
					'nilaicustomer' => floatval(str_replace(",", "", $this->input->post('nilaicustomer'))),
					'total' => floatval(str_replace(",", "", $this->input->post('total'))),
					'dpp' => floatval(str_replace(",", "", $this->input->post('dpp'))),
					'ppn' => floatval(str_replace(",", "", $this->input->post('ppn'))),
					'grandtotal' => floatval(str_replace(",", "", $this->input->post('grandtotal'))),
					'gudang' => $gudang,
					'pemakai' => $pemakai,
					'tglsimpan' => date('Y-m-d H:i:s'),
					'kodecabang' => $cabang,
					'keterangan' => $this->input->post('keterangan'),
					'discmanual' => floatval(str_replace(",", "", $this->input->post('discmanual'))),
					'nilaimanual' => floatval(str_replace(",", "", $this->input->post('nilaimanual')))
				);
				// print_r($dataheader);
				// die();
				$this->Salesorder_model->InsertData("srvt_salesorder", $dataheader);
			}

			$this->db->trans_complete();

			if ($this->db->trans_status() === FALSE) {
				$resultjson = array(
					'nomor' => "",
					'message' => "Data gagal disimpan, Nomor '<b style='color: red'>" . $nomor . "</b>' sudah pernah digunakan."
				);
				# Something went wrong.
				$this->db->trans_rollback();
			} else {
				$resultjson = array(
					'nomor' => $nomor,
					'message' => "Data berhasil disimpan."
				);
				# Everything is Perfect. 
				# Committing data to the database.
				$this->db->trans_commit();
			}

			echo json_encode($resultjson);
		}
	}


	function Cancel()
	{
		$pemakai =  $this->session->userdata('myusername');
		$nomor = $this->input->post('nomor');
		$periode = date('Y') . date('m');
		$kodegudang = $this->input->post('lokasi');
		$keterangan = $this->input->post('keterangan');
		$cabang = $this->input->post('cabang');
		$errorvalidasi = FALSE;

		// if (!empty($this->input->post('datadetail'))) {
		// 	foreach ($this->input->post('datadetail') as $key => $value) {
		// 		// print_r($value);
		// 		// die();
		// 		$where = array(
		// 			"periode" => $periode,
		// 			"kodecabang" => $cabang,
		// 			"kodegudang" => $kodegudang,
		// 			"kodebarang" => $value['Kode']
		// 		);
		// 		$datastock = $this->global_model->GetDataGlobal("srvt_inventorystok", $where);
		// 		if (empty($datastock)) {
		// 			$resultjson = array(
		// 				'nomor' => "",
		// 				'message' => "Data gagal dibatalkan, Barang '<b style='color: red'>" . $value['Nama'] . "</b>' Stock Tidak Ada."
		// 			);
		// 			$errorvalidasi = TRUE;
		// 			echo json_encode($resultjson);
		// 			return FALSE;
		// 		} else {
		// 			foreach ($datastock as $key => $values) {
		// 				$stocksaatini = $values->qtyawal + $values->qtymasuk - $values->qtykeluar - $values->qtypick;
		// 				if (floatval(str_replace(",", "", $value['Qty'])) > $stocksaatini) {
		// 					$resultjson = array(
		// 						'nomor' => "",
		// 						'message' => "Data gagal dibatalkan, Barang '<b style='color: red'>" . $value['Nama'] . "</b>' Stock Tidak Ada."
		// 					);
		// 					$errorvalidasi = TRUE;
		// 					echo json_encode($resultjson);
		// 					return FALSE;
		// 				}
		// 			}
		// 		}
		// 	}
		// }

		$wheredata = array("nomor" => $nomor);
		$datasalesorderdetail = $this->global_model->GetDataGlobal("srvt_salesorder", $wheredata);
		if (!empty($datasalesorderdetail)) {
			foreach ($datasalesorderdetail as $key => $value) {
				if ($value->nilaiuangmuka > 0) {
					$resultjson = array(
						'nomor' => "",
						'message' => "Data gagal dibatalkan, Sudah Dilakukan Penerimaan Uang Muka."
					);
					$errorvalidasi = TRUE;
					echo json_encode($resultjson);
					return FALSE;
				}
			}
		}

		$wheredata = array("nomor" => $nomor);
		$datasalesorderstatus = $this->global_model->GetDataGlobal("srvt_salesorder", $wheredata);
		if (!empty($datasalesorderstatus)) {
			foreach ($datasalesorderstatus as $key => $value) {
				if ($value->kodestatus > 0) {
					$resultjson = array(
						'nomor' => "",
						'message' => "Data gagal dibatalkan, Sudah Dilakukan Packing Barang Atau Faktur."
					);
					$errorvalidasi = TRUE;
					echo json_encode($resultjson);
					return FALSE;
				}
			}
		}

		if (empty($this->input->post('datadetail'))) {
			$resultjson = array(
				'nomor' => "",
				'message' => "Data gagal dibatalkan, Data Detail Tidak Ada."
			);
			$errorvalidasi = TRUE;
			echo json_encode($resultjson);
			return FALSE;
		}

		if ($errorvalidasi == FALSE) {
			$this->db->trans_start(); # Starting Transaction
			$this->db->trans_strict(FALSE);

			foreach ($this->input->post('datadetail') as $key => $value) {
				// print_r($value);
				// die();
				$where = array(
					"periode" => $periode,
					"kodecabang" => $cabang,
					"kodegudang" => $kodegudang,
					"kodebarang" => $value['Kode']
				);
				$datastock = $this->global_model->GetDataGlobal("srvt_inventorystok", $where);
				if (!empty($datastock)) {
					foreach ($datastock as $key => $value2) {
						$qtypicka = $value2->qtypick - floatval(str_replace(",", "", $value['Qty']));
						$setdata = "qtypick = '" . $qtypicka . "'";
						$wheredata = "periode = '" . $periode . "' AND kodecabang = '" . $cabang . "' AND kodegudang = '" . $kodegudang . "' AND kodebarang = '" . $value['Kode'] . "'";
						$this->Salesorder_model->UpdateDataInventory("srvt_inventorystok", $setdata, $wheredata);
					}
				} else {
					$datainventorystock = array(
						"periode" => $periode,
						"kodecabang" => $cabang,
						"kodegudang" => $kodegudang,
						"kodebarang" => $value['Kode'],
						"qtymasuk" => floatval(str_replace(",", "", $value['Qty'])),
						'pemakai' => $pemakai,
						'tglsimpan' => date('Y-m-d H:i:s'),
					);
					$this->global_model->InsertDataGlobal("srvt_inventorystok", $datainventorystock);
				}
			}

			$dataheader = array(
				'batal' => true,
				'userbatal' => $pemakai,
				'tglbatal' => date('Y-m-d H:i:s'),
				'alasanbatal' => $keterangan
			);
			$wheredata = array("nomor" => $nomor);
			$this->global_model->UpdateDataGlobal("srvt_salesorder", $dataheader, $wheredata);
			$this->db->trans_complete();

			if ($this->db->trans_status() === FALSE) {
				$resultjson = array(
					'nomor' => "",
					'message' => "Data gagal dibatalkan, Nomor '<b style='color: red'>" . $nomor . "</b>' sudah pernah digunakan."
				);
				# Something went wrong.
				$this->db->trans_rollback();
			} else {
				$resultjson = array(
					'nomor' => $nomor,
					'message' => "Data berhasil dibatalkan."
				);
				# Everything is Perfect. 
				# Committing data to the database.
				$this->db->trans_commit();
			}

			echo json_encode($resultjson);
		}
	}

	function Update()
	{

		// $interval->m;
		// date('Y-m-d H:i:s', strtotime('+1 seconds', strtotime($row2->start_event)));
		// $tanggal =  date('Y-m-d',strtotime(new Date()));
		$tanggalso = $this->input->post('tanggalso');
		// $dateold = new DateTime($tanggalso);
		// $datenow = new DateTime();
		$pemakai =  $this->session->userdata('myusername');
		$nomor = $this->input->post('nomor');
		// $periodelama = date('Ym', strtotime($tanggalso));
		$periode = date('Y') . date('m');
		$kodegudang = $this->input->post('lokasi');
		$cabang = $this->input->post('cabang');
		$errorvalidasi = FALSE;
		// $interval = date_diff($datenow, $dateold);
		// $jumlahbulan = 0;
		// if ($interval->y == 0 &&  $interval->m == 0) {
		// 	$jumlahbulan = $interval->m + 1;
		// } else {
		// 	$jumlahbulan = $interval->m;
		// }
		// $selisihbulan = ($interval->y * 12) + $jumlahbulan;



		$diff = abs(strtotime($tanggalso) - strtotime(date('Y-m-d')));
		$years = floor($diff / (365 * 60 * 60 * 24));
		$bulan = floor(($diff - $years * 365 * 60 * 60 * 24) / (30 * 60 * 60 * 24));
		if ((date('m') == date('m', strtotime($tanggalso)) && (date('Y') == date('Y', strtotime($tanggalso))))) {
			$selisihbulan = 0;
		} else if ((date('m') == date('m', strtotime($tanggalso)) && (date('Y') > date('Y', strtotime($tanggalso))))) {
			$selisihbulan = $bulan - 1 + ($years * 12);
		} else if ((date('m') != date('m', strtotime($tanggalso)) && (date('Y') == date('Y', strtotime($tanggalso))))) {
			$selisihbulan = $bulan + 1 + ($years * 12);
		} else {
			$selisihbulan = $bulan + ($years * 12);
		}


		$where = array(
			"nomor" => $nomor,
			"batal" => false,
			"kodestatus" => 1
		);
		$cekdataupdate = $this->global_model->GetDataGlobal("srvt_salesorder", $where);
		if (!empty($cekdataupdate)) {
			$resultjson = array(
				'nomor' => "",
				'message' => "Data gagal diubah, No SO '<b style='color: red'>" . $nomor . "</b>' Sudah Dilakukkan Packing."
			);
			$errorvalidasi = TRUE;
			echo json_encode($resultjson);
			return FALSE;
		}

		$where = array(
			"nomor" => $nomor,
			"batal" => false,
			"kodestatus" => 2
		);
		$cekdataupdate = $this->global_model->GetDataGlobal("srvt_salesorder", $where);
		if (!empty($cekdataupdate)) {
			$resultjson = array(
				'nomor' => "",
				'message' => "Data gagal diubah, No SO '<b style='color: red'>" . $nomor . "</b>' Sudah Dilakukkan Surat Jalan."
			);
			$errorvalidasi = TRUE;
			echo json_encode($resultjson);
			return FALSE;
		}

		if (!empty($this->input->post('datadetail'))) {
			foreach ($this->input->post('datadetail') as $key => $value) {
				$where = array(
					"periode" => $periode,
					"kodecabang" => $cabang,
					"kodegudang" => $kodegudang,
					"kodebarang" => $value['Kode']
				);
				$datastock = $this->global_model->GetDataGlobal("srvt_inventorystok", $where);
				if (empty($datastock)) {
					$resultjson = array(
						'nomor' => "",
						'message' => "Data gagal diubah, Barang '<b style='color: red'>" . $value['Nama'] . "</b>' Stock Tidak Ada."
					);
					$errorvalidasi = TRUE;
					echo json_encode($resultjson);
					return FALSE;
				} else {
					foreach ($datastock as $key => $values) {
						$qtylama = 0;
						$stocksaatini = $values->qtyawal + $values->qtymasuk - $values->qtykeluar - $values->qtypick;

						$wheredata = array("nomor" => $nomor, 'kodebarang' => $value['Kode']);
						$datasalesorderdetail = $this->global_model->GetDataGlobal("srvt_sodetail", $wheredata);
						foreach ($datasalesorderdetail as $key => $valuesodetail) {
							$qtylama = $valuesodetail->qty;
						}

						if (floatval(str_replace(",", "", $value['Qty'])) > ($stocksaatini + $qtylama)) {
							$resultjson = array(
								'nomor' => "",
								'message' => "Data gagal diubah, Barang '<b style='color: red'>" . $value['Nama'] . "</b>' Stock Tidak Cukup."
							);
							$errorvalidasi = TRUE;
							echo json_encode($resultjson);
							return FALSE;
						}
					}
				}
			}
		}
		// Cek Jika Update Backdate	
		if ($selisihbulan > 0) {
			if (!empty($this->input->post('datadetail'))) {
				foreach ($this->input->post('datadetail') as $key => $value) {
					// $periodestock = $periodelama;
					for ($i = 0; $i <= $selisihbulan; $i++) {
						# code...

						$periodestock = date('Ym', strtotime('+' . $i . ' month', strtotime($tanggalso)));
						// print_r($selisihbulan);
						// echo "<br>";
						// print_r($periodestock);
						// echo "<br>";
						// die();
						$where = array(
							"periode" => $periodestock,
							"kodecabang" => $cabang,
							"kodegudang" => $kodegudang,
							"kodebarang" => $value['Kode']
						);
						$datastock = $this->global_model->GetDataGlobal("srvt_inventorystok", $where);
						if (empty($datastock)) {
							$resultjson = array(
								'nomor' => "",
								'message' => "Data gagal diubah, Barang '<b style='color: red'>" . $value['Nama'] . "</b>' Stock Tidak Ada."
							);
							$errorvalidasi = TRUE;
							echo json_encode($resultjson);
							return FALSE;
						} else {
							foreach ($datastock as $key => $values) {
								$qtylama = 0;
								$stocksaatini = $values->qtyawal + $values->qtymasuk - $values->qtykeluar - $values->qtypick;

								$wheredata = array("nomor" => $nomor, 'kodebarang' => $value['Kode']);
								$datasalesorderdetail = $this->global_model->GetDataGlobal("srvt_sodetail", $wheredata);
								foreach ($datasalesorderdetail as $key => $valuesodetail) {
									$qtylama = $valuesodetail->qty;
								}

								if (floatval(str_replace(",", "", $value['Qty'])) > ($stocksaatini + $qtylama)) {
									$resultjson = array(
										'nomor' => "",
										'message' => "Data gagal diubah, Barang '<b style='color: red'>" . $value['Nama'] . "</b>' Stock Tidak Cukup."
									);
									$errorvalidasi = TRUE;
									echo json_encode($resultjson);
									return FALSE;
								}
							}
						}
					}
					// die();
				}
			}
		}
		// print_r("A");
		// die();

		if (empty($this->input->post('datadetail'))) {
			$resultjson = array(
				'nomor' => "",
				'message' => "Data gagal diubah, Data Detail Tidak Ada."
			);
			$errorvalidasi = TRUE;
			echo json_encode($resultjson);
			return FALSE;
		}



		if ($errorvalidasi == FALSE) {
			$this->db->trans_start(); # Starting Transaction
			$this->db->trans_strict(FALSE);

			//Catat History Data Lama Sebagai History
			//Catat Header
			$revisike = 0;
			$datarevisi = $this->Salesorder_model->MaxUrutan($nomor);
			$revisike = $datarevisi[0]->revisike + 1;
			$where = array(
				"nomor" => $nomor,
				"batal" => false
			);
			$dataheader = $this->global_model->GetDataGlobal("srvt_salesorder", $where);


			if (!empty($dataheader)) {
				foreach ($dataheader as $key => $value) {
					$data = array(
						'nomor' => $value->nomor,
						'tanggal' => $value->tanggal,
						'nopelanggan' => $value->nopelanggan,
						'namapelanggan' => $value->namapelanggan,
						'plafonso' => $value->plafonso,
						'outstandingar' => $value->outstandingar,
						'kodesalesman' => $value->kodesalesman,
						'namasalesman' => $value->namasalesman,
						'jenisjual' => $value->jenisjual,
						'jenisso' => $value->jenisso,
						'lokasi' => $value->lokasi,
						'jenisdisc' => $value->jenisdisc,
						'disccash' => $value->disccash,
						'disccustomer' => $value->disccustomer,
						'nilaicash' => $value->nilaicash,
						'nilaicustomer' => $value->nilaicustomer,
						'total' => $value->total,
						'dpp' => $value->dpp,
						'ppn' => $value->ppn,
						'grandtotal' => $value->grandtotal,
						'pemakai' => $value->pemakai,
						'tglsimpan' => $value->tglsimpan,
						'kodecabang' => $cabang,
						'userupdate' => $pemakai,
						'tglupdate' => date('Y-m-d H:i:s'),
						'revisike' => $revisike
					);
					$this->Salesorder_model->InsertData("srvt_salesorderhistory", $data);
				}
			}
			// Detail Data
			$where = array(
				"nomor" => $nomor
			);
			$datadetail = $this->global_model->GetDataGlobal("srvt_sodetail", $where);
			if (!empty($datadetail)) {
				foreach ($datadetail as $key => $value) {
					$data = array(
						'nomor' => $value->nomor,
						'kodebarang' => $value->kodebarang,
						'namabarang' => $value->namabarang,
						'qty' => $value->qty,
						'harga' => $value->harga,
						'discbarang' => $value->discbarang,
						'discbrgitem' => $value->discbrgitem,
						'discpromo' => $value->discpromo,
						'discpaketpromo' => $value->discpaketpromo,
						'total' => $value->total,
						'pemakai' => $value->pemakai,
						'tglsimpan' => $value->tglsimpan,
						'userupdate' => $pemakai,
						'tglupdate' => date('Y-m-d H:i:s'),
						'revisike' => $revisike
					);
					$this->Salesorder_model->InsertData("srvt_sodetailhistory", $data);
				}
			}
			//End History


			// Update Qty Pick Inventory

			$datasoDetail = $this->global_model->GetDataGlobal("srvt_sodetail", $where);
			if (!empty($datasoDetail)) {
				foreach ($datasoDetail as $key => $valuesod) {
					$qtylama = 0;
					if ($selisihbulan > 0) {
						for ($i = 0; $i <= $selisihbulan; $i++) {
							$periodestock = date('Ym', strtotime('+' . $i . ' month', strtotime($tanggalso)));
							$where = array(
								"periode" => $periodestock,
								"kodecabang" => $cabang,
								"kodegudang" => $kodegudang,
								"kodebarang" => $valuesod->kodebarang
							);
							$datastock = $this->global_model->GetDataGlobal("srvt_inventorystok", $where);
							if (!empty($datastock)) {
								foreach ($datastock as $key => $value2) {
									$qtypicka = ($value2->qtypick - $valuesod->qty);
									$setdata = "qtypick = '" . $qtypicka . "'";
									$wheredata = "periode = '" . $periodestock . "' AND kodecabang = '" . $cabang . "' AND kodegudang = '" . $kodegudang . "' AND kodebarang = '" . $valuesod->kodebarang . "'";
									$this->Salesorder_model->UpdateDataInventory("srvt_inventorystok", $setdata, $wheredata);
								}
							}
						}
					} else {
						$where = array(
							"periode" => $periode,
							"kodecabang" => $cabang,
							"kodegudang" => $kodegudang,
							"kodebarang" => $valuesod->kodebarang
						);
						$datastock = $this->global_model->GetDataGlobal("srvt_inventorystok", $where);
						if (!empty($datastock)) {
							foreach ($datastock as $key => $value2) {
								$qtypicka = ($value2->qtypick - $valuesod->qty);
								$setdata = "qtypick = '" . $qtypicka . "'";
								$wheredata = "periode = '" . $periode . "' AND kodecabang = '" . $cabang . "' AND kodegudang = '" . $kodegudang . "' AND kodebarang = '" . $valuesod->kodebarang . "'";
								$this->Salesorder_model->UpdateDataInventory("srvt_inventorystok", $setdata, $wheredata);
							}
						}
					}
				}
			}
			//-----------------------------------------------------------
			//Delete Detail SO
			$where = array(
				"nomor" => $nomor
			);
			$this->global_model->DeleteDataGlobal("srvt_sodetail", $where);
			//Insert Table SO Detail
			foreach ($this->input->post('datadetail') as $key => $value) {

				$where = array(
					"periode" => $periode,
					"kodecabang" => $cabang,
					"kodegudang" => $kodegudang,
					"kodebarang" => $value['Kode']
				);
				$datastock = $this->global_model->GetDataGlobal("srvt_inventorystok", $where);
				if (!empty($datastock)) {
					foreach ($datastock as $key => $value2) {
						if ($selisihbulan > 0) {
							for ($i = 0; $i <= $selisihbulan; $i++) {
								$periodestock = date('Ym', strtotime('+' . $i . ' month', strtotime($tanggalso)));
								$where = array(
									"periode" => $periodestock,
									"kodecabang" => $cabang,
									"kodegudang" => $kodegudang,
									"kodebarang" => $value['Kode']
								);
								$datastock = $this->global_model->GetDataGlobal("srvt_inventorystok", $where);
								if (!empty($datastock)) {
									foreach ($datastock as $key => $value2) {
										$qtypicka = ($value2->qtypick + (floatval(str_replace(",", "", $value['Qty']))));
										$setdata = "qtypick = '" . $qtypicka . "'";
										$wheredata = "periode = '" . $periodestock . "' AND kodecabang = '" . $cabang . "' AND kodegudang = '" . $kodegudang . "' AND kodebarang = '" . $value['Kode'] . "'";
										$this->Salesorder_model->UpdateDataInventory("srvt_inventorystok", $setdata, $wheredata);
									}
								}
							}
						} else {
							$where = array(
								"periode" => $periode,
								"kodecabang" => $cabang,
								"kodegudang" => $kodegudang,
								"kodebarang" => $value['Kode']
							);
							$datastock = $this->global_model->GetDataGlobal("srvt_inventorystok", $where);
							if (!empty($datastock)) {
								foreach ($datastock as $key => $value2) {
									$qtypicka = ($value2->qtypick + (floatval(str_replace(",", "", $value['Qty']))));
									$setdata = "qtypick = '" . $qtypicka . "'";
									$wheredata = "periode = '" . $periode . "' AND kodecabang = '" . $cabang . "' AND kodegudang = '" . $kodegudang . "' AND kodebarang = '" . $value['Kode'] . "'";
									$this->Salesorder_model->UpdateDataInventory("srvt_inventorystok", $setdata, $wheredata);
								}
							}
						}
					}
				}
				$datasodetail = array(
					'nomor' => $nomor,
					'kodebarang' => $value['Kode'],
					'namabarang' => $value['Nama'],
					'qty' => floatval(str_replace(",", "", $value['Qty'])),
					'harga' => floatval(str_replace(",", "", $value['Harga'])),
					'discbarang' => $value['Disc'],
					'discbrgitem' => floatval(str_replace(",", "", $value['DiscItem'])),
					'discpromo' => $value['DiscPromo'],
					'discpaketpromo' => floatval(str_replace(",", "", $value['DiscPromoItem'])),
					'total' => floatval(str_replace(",", "", $value['Subtotal'])),
					'pemakai' => $pemakai,
					'tglsimpan' => date('Y-m-d H:i:s'),
				);
				$this->global_model->InsertDataGlobal("srvt_sodetail", $datasodetail);
			}

			//UPDATE HEADER SO
			$dataheader = array(
				'disccash' => floatval(str_replace(",", "", $this->input->post('discash'))),
				'disccustomer' => floatval(str_replace(",", "", $this->input->post('discustomer'))),
				'nilaicash' => floatval(str_replace(",", "", $this->input->post('nilaicash'))),
				'nilaicustomer' => floatval(str_replace(",", "", $this->input->post('nilaicustomer'))),
				'total' => floatval(str_replace(",", "", $this->input->post('total'))),
				'dpp' => floatval(str_replace(",", "", $this->input->post('dpp'))),
				'ppn' => floatval(str_replace(",", "", $this->input->post('ppn'))),
				'grandtotal' => floatval(str_replace(",", "", $this->input->post('grandtotal'))),
				'pemakai' => $pemakai,
				'tglsimpan' => date('Y-m-d H:i:s'),
				'keterangan' => $this->input->post('keterangan')
			);

			$wheredata = array("nomor" => $nomor);
			$this->global_model->UpdateDataGlobal("srvt_salesorder", $dataheader, $wheredata);
			// END UPDATE Header SO

			// die();

			$this->db->trans_complete();

			if ($this->db->trans_status() === FALSE) {
				$resultjson = array(
					'nomor' => "",
					'message' => "Data gagal diubah, Nomor '<b style='color: red'>" . $nomor . "</b>' sudah pernah digunakan."
				);
				# Something went wrong.
				$this->db->trans_rollback();
			} else {
				$resultjson = array(
					'nomor' => $nomor,
					'message' => "Data berhasil diubah."
				);
				# Everything is Perfect. 
				# Committing data to the database.
				$this->db->trans_commit();
			}

			echo json_encode($resultjson);
		}
	}

	function CariJenisCustomer()
	{
		$result = $this->Salesorder_model->CariJenisCustomer($this->input->post('nomor'));
		echo json_encode($result);
	}

	function LoadKonfigurasi()
	{
		$result = $this->global_model->GetDataGlobal("stpm_konfigurasi", array());
		echo json_encode($result);
	}

	function LoadGudang()
	{
		$result = $this->global_model->GetDataGlobal("glbm_gudang", array("kodecabang" => $this->input->post('cabang')));
		echo json_encode($result);
	}
	function LoadRacikan()
	{
		$result = $this->global_model->GetDataGlobal("glbm_promo", array("kodecabang" => $this->input->post('cabang')));
		echo json_encode($result);
	}

	function CekDiskonPromo()
	{
		$result = $this->Salesorder_model->CekDiskonPromo($this->input->post('kodebarang'), $this->input->post('jenisracikan'), $this->input->post('cabang'));
		echo json_encode($result);
	}

	function CekDiskonKelompoAJ()
	{
		$result = $this->Salesorder_model->CekDiskonKelompoAJ($this->input->post('kodebarang'));
		echo json_encode($result);
	}
	function LoadOutStandingAR()
	{
		$result = $this->Salesorder_model->LoadOutStandingAR($this->input->post('nomorcustomer'));
		echo json_encode($result);
	}


	function CekDiskonGrupCustomerTokoSendiri()
	{
		$where =  array(
			'aktif' => true,
			'tokosendiri' => true,
			'discountatas' => true
		);

		$result = $this->global_model->GetDataGlobal("glbm_grupcustomer", $where);
		echo json_encode($result);
	}

	function CekDiskonGrupCustomer()
	{
		$where =  array(
			'aktif' => true,
			'tokosendiri' => false,
			'discountatas' => false
		);

		$result = $this->global_model->GetDataGlobal("glbm_grupcustomer", $where);
		echo json_encode($result);
	}

	function CekDiskonKhusus()
	{
		$where =  array(
			'aktif' => true,
			'kodebarang' => $this->input->post('kodebarang'),
			'nomorpelanggan' => $this->input->post('nomorcustomer')
		);

		$result = $this->global_model->GetDataGlobal("glbm_diskonkhusus", $where);
		echo json_encode($result);
	}

	function CekDiskonPelangganKategori()
	{
		$result = $this->Salesorder_model->CekDiskonPelangganKategori($this->input->post('kodebarang'), $this->input->post('nomorcustomer'));
		echo json_encode($result);
	}

	function CekDiskonLevel()
	{
		$result = $this->Salesorder_model->CekDiskonLevel($this->input->post('kodebarang'), $this->input->post('cabang'), $this->input->post('qty'));
		echo json_encode($result);
	}


	function CekDiskon()
	{
		$jenisso = $this->input->post('jenisso');
		$nomorpelanggan = $this->input->post('nomorpelanggan');
		$cabang = $this->input->post('cabang');
		$kodebarang = $this->input->post('kodebarang');
		$jenisracikan = $this->input->post('jenisracikan');
		$qty = $this->input->post('qty');
		$total = $this->input->post('total');
		$statusToko = false;
		$statusdiskonatas = false;
		$diskon = 0;
		$diskonracikan = 0;
		$minorder = 0;
		// $groupcustomer = $this->Salesorder_model->GetRowGroupDiskon($nomorpelanggan);
		$groupcustomer = $this->Salesorder_model->GetRowGroupDiskon($nomorpelanggan);
		$noreferensi =  $groupcustomer->gruppelanggan;
		$diskonlevelgroup = $this->Salesorder_model->GetRowDiskonLevelGroup($noreferensi, $cabang);
		// print_r($noreferensi);
		// die();
		// $grupcustomer = $this->Salesorder_model->GetRowGrupCustomer($nomorpelanggan);
		switch ($jenisso) {
			case "1":
								// $where =  array(
				// 	'aktif' => true,
				// 	// 'kodebarang' => $kodebarang,
				// 	'nomorpelanggan' => $nomorpelanggan
				// );
				$datadiskon = $this->Salesorder_model->CekDiskon($nomorpelanggan, $kodebarang);
				if (!empty($datadiskon)) {
					// print_r(1);
					$diskon = $datadiskon[0]->diskon;
					// print_r($diskon);
				}
				if ($diskon == "0") {
					$datadiskon = $this->Salesorder_model->CekDiskonPelangganKategori($kodebarang, $nomorpelanggan);
					if (!empty($datadiskon)) {
						$diskon = $datadiskon[0]->diskon;
					}
				}
				if($noreferensi !="" || $diskon == "0"){
					$datadiskon = $this->Salesorder_model->CekDiskonLevelGroup($kodebarang, $cabang, $qty, $noreferensi);
					// print_r($datadiskon);
					// die();
					if(!empty($datadiskon)){
						$diskon = $datadiskon[0]->diskon;
					} 
				}
				if($diskon == "0"){
					$where =  array(
						'aktif' => true,
						'tokosendiri' => false,
						'discountatas' => true,
						'kode' => $groupcustomer->gruppelanggan
						// 'kode' => $grupcustomer->gruppelanggan
					);
					
					// print_r($where);
					// die();
					$datadiskon = $this->Salesorder_model->CekDiskonGroup($where);
					if(!empty($datadiskon)){
						$diskon = $datadiskon[0]->diskon;
					}
				}
				if ($diskon == "0" && $noreferensi == "") {
					$datadiskon = $this->Salesorder_model->CekDiskonLevel($kodebarang, $cabang, $qty, $noreferensi);
					// print_r($datadiskon);
					if (!empty($datadiskon)) {
						$diskon = $datadiskon[0]->diskon;
					}
				}
				break;
			case "2":
				$datadiskon = $this->Salesorder_model->CekDiskonKelompoAJ($kodebarang);
				if (!empty($datadiskon)) {
					$diskon = $datadiskon[0]->diskon;
				}
				if ($diskon == "0") {
					$where =  array(
						'aktif' => true,
						'tokosendiri' => true,
						'discountatas' => true,
						'kode' => $groupcustomer->gruppelanggan
						// 'kode' => $grupcustomer->gruppelanggan
					);

					$datadiskon = $this->global_model->GetDataGlobal("glbm_grupcustomer", $where);
					if (!empty($datadiskon)) {
						// print_r($datadiskon);
						$diskon = $datadiskon[0]->diskon;
					}
				}
				break;
			default:
				$diskon = 0;
		}
		$result = array(
			"kodebarang" => $kodebarang,
			"diskon" => $diskon
		);
		// die();
		echo json_encode($result);
	}

	function CekDiskonRacikanPromo()
	{
		// $result = $this->Salesorder_model->CekDiskonRacikanPromo($this->input->post('kodebarang'), $this->input->post('jenisracikan'), $this->input->post('cabang'), $this->input->post('minorder'));
		$result = $this->Salesorder_model->CekDiskonRacikanPromo($this->input->post('jenisracikan'), $this->input->post('cabang'), $this->input->post('minorder'));
		echo json_encode($result);
	}

	function CetakProformaUpdate()
	{
		$kodecabang = $this->session->userdata('mycabang');
		$pemakai = $this->session->userdata('myusername');

		$nomor = $this->input->post('nomor');

		$this->db->trans_start(); # Starting Transaction
		$this->db->trans_strict(FALSE);

		$dataso = $this->Salesorder_model->CekSO($nomor);

		foreach ($dataso as $ambildata) {
			$cetakproforma = $ambildata->cetakproforma;
			$data = [
				'cetakproforma' => $cetakproforma + 1
			];
			$this->Salesorder_model->UpdateData($nomor, $data);
		}


		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE) {
			$resultjson = array(
				'nomor' => "",
				'message' => "Data gagal di update, Nomor '<b style='color: red'>" . $nomor . "</b>' sudah pernah digunakan."
			);
			# Something went wrong.
			$this->db->trans_rollback();
		} else {
			$resultjson = array(
				'nomor' => $nomor,
				'message' => "Data berhasil di update."
			);
			# Everything is Perfect. 
			# Committing data to the database.
			$this->db->trans_commit();
		}
		echo json_encode($resultjson);
	}
}
