<?php

defined('BASEPATH') or exit('No direct script access allowed');

class FakturPajak extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->load->model('sales/FakturPajak_model');
        $this->load->model('global/global_model');
        $this->load->model('Caridataaktif_model');
        $this->load->library('form_validation');
        $this->load->library('session');
    }

    function CariDataFakturPajak()
    {
        $fetch_data = $this->Caridataaktif_model->make_datatables($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value'));
        $data = array();
        foreach ($fetch_data as $row) {
            $sub_array = array();
            $i = 1;
            $count = count($this->input->post('field'));
            foreach ($this->input->post('field') as $key => $value) {
                if ($i <= $count) {
                    if ($i == 1) {
                        $msearch = $row->$value;
                        $sub_array[] = '<button class="btn btn-dark searchfakturpajak" data-dismiss="modal" style="margin: 0px;" data-id="' . $msearch . '"><i class="far fa-check-circle"></i></button>';
                        $sub_array[] = $row->$value;
                    } else {
                        if ($i == $count) {
                            $sub_array[] = $row->$value;
                        } else {
                            $sub_array[] = $row->$value;
                        }
                    }
                    // $sub_array[] = $row->$value;
                }
                $i++;
            }
            $data[] = $sub_array;
        }
        $output = array(
            "draw"                    =>     intval($_POST["draw"]),
            "recordsTotal"          =>      $this->Caridataaktif_model->get_all_data($this->input->post('nmtb')),
            "recordsFiltered"     =>     $this->Caridataaktif_model->get_filtered_data($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value')),
            "data"                    =>     $data
        );
        echo json_encode($output);
    }

    function DataFakturPajak()
    {
        $result = $this->FakturPajak_model->DataFakturPajak($this->input->post('nomor'));
        echo json_encode($result);
    }

    public function ClearChr($param)
    {
        return $result = str_replace(array('\'', '"', ',', ';', '<', '>', '_', '-', ' ', '.', '!', '?', '/', '=', '+', ']', '[', '{', '}', '@', '#', '$', '%', '^', '&', "*", '(', ')'), '', $param);
    }

    function Save()
    {
        $nomor = $this->input->post('nomor');

        if (empty($nomor)) {
            $this->db->trans_start(); # Starting Transaction
            $this->db->trans_strict(FALSE);

            // $nomor = $this->input->post('nomor');

            $get["fp"] = $this->FakturPajak_model->GetMaxNomor($nomor);
            if (!$get["fp"]) {
                $nomor = "0000000001";
            } else {
                $lastNomor = $get['fp']->nomor;
                $lastNoUrut = substr($lastNomor, 1, 10);

                // nomor urut ditambah 1
                $nextNoUrut = $lastNoUrut + 1;
                $nomor =  sprintf('%09s', $nextNoUrut);
            }

            $data = array(
                'nomor' => $nomor,
                'nofaktur' => $this->input->post('noinvoice'),
                'tanggal' => $this->input->post('tanggal'),
                'nofakturpajak' => $this->input->post('nofakturpajak'),
                'nopelanggan' => $this->input->post('nopelanggan'),
                'tglppn' => $this->input->post('tglppn'),
                'dpp' => $this->ClearChr($this->input->post('dpp')),
                'ppn' => $this->ClearChr($this->input->post('ppn')),
                'grandtotal' => $this->ClearChr($this->input->post('grandtotal')),
                'keterangan' => $this->input->post('keterangan1'),
                'keterangan2' => $this->input->post('keterangan2'),
                'penanggungjawab' => $this->input->post('penanggungjawab'),
                'jabatan' => $this->input->post('jabatan'),
                'tglsimpan' => date('Y-m-d H:i:s'),
                'pemakai' => $this->session->userdata('myusername'),
            );
            // print_r($data);
            $this->FakturPajak_model->SaveData($data);

            $dataupdate = [
                'status' => true,
                'sumberfaktur' => $nomor,
                'nofaktur' => $this->input->post('noinvoice'),
                'nopelanggan' => $this->input->post('nopelanggan')
            ];
            $this->FakturPajak_model->UpdateDataFakturPajak($this->input->post('nofakturpajak'), $dataupdate);

            $dataupdateinv = [
                'nofakturpajak' => $this->input->post('nofakturpajak')
            ];
            $this->FakturPajak_model->UpdateDataFaktur($this->input->post('noinvoice'), $dataupdateinv);

            // die();
            $this->db->trans_complete();

            if ($this->db->trans_status() === FALSE) {
                $resultjson = array(
                    'nomor' => "",
                    'message' => "Data gagal disimpan, Nomor '<b style='color: red'>" . $nomor . "</b>' sudah pernah digunakan."
                );
                # Something went wrong.
                $this->db->trans_rollback();
            } else {
                $resultjson = array(
                    'nomor' => $nomor,
                    'message' => "Data berhasil disimpan."
                );
                # Everything is Perfect. 
                # Committing data to the database.
                $this->db->trans_commit();
            }
        } else {
            $this->db->trans_complete();
            $resultjson = array(
                'nomor' => "",
                'message' => "Data gagal disimpan, Nomor '<b style='color: red'>" . $nomor . "</b>' sudah pernah digunakan."
            );
            # Something went wrong.
            $this->db->trans_rollback();
        }
        echo json_encode($resultjson);
    }


    function Cancel()
    {
        $dataheader = array(
            'batal' => true,
            'userbatal' => $this->session->userdata('myusername'),
            'keteranganbatal' => $this->input->post('keterangan'),
            'tglbatal' => date('Y-m-d H:i:s')
        );
        $wheredata = array("nomor" => $this->input->post('nomor'));
        $this->global_model->UpdateDataGlobal("srvt_fakturpajak", $dataheader, $wheredata);
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE) {
            $resultjson = array(
                'nomor' => "",
                'message' => "Data gagal dibatalkan, Nomor '<b style='color: red'>" . $this->input->post('nomor') . "</b>' tidak ada."
            );
            # Something went wrong.
            $this->db->trans_rollback();
        } else {
            $resultjson = array(
                'nomor' => $this->input->post('nomor'),
                'message' => "Data berhasil dibatalkan."
            );
            # Everything is Perfect. 
            # Committing data to the database.
            $this->db->trans_commit();
        }

        echo json_encode($resultjson);
    }
}
