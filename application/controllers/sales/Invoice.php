<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Invoice extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('sales/Invoice_model');
		$this->load->model('global/global_model');
		$this->load->model('Caridataaktif_model');
		$this->load->library('form_validation');
		$this->load->library('session');
	}

	function CariDataInvoice()
	{
		$fetch_data = $this->Caridataaktif_model->make_datatables($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value'));
		$data = array();
		foreach ($fetch_data as $row) {
			$sub_array = array();
			$i = 1;
			$count = count($this->input->post('field'));
			foreach ($this->input->post('field') as $key => $value) {
				if ($i <= $count) {
					if ($i == 1) {
						$msearch = $row->$value;
						$sub_array[] = '<button class="btn btn-dark searchinvoice" data-dismiss="modal" style="margin: 0px;" data-id="' . $msearch . '"><i class="far fa-check-circle"></i></button>';
						$sub_array[] = $row->$value;
					} else {
						if ($i == $count) {
							$sub_array[] = $row->$value;
						} else {
							$sub_array[] = $row->$value;
						}
					}
					// $sub_array[] = $row->$value;
				}
				$i++;
			}
			$data[] = $sub_array;
		}
		$output = array(
			"draw"                    =>     intval($_POST["draw"]),
			"recordsTotal"          =>      $this->Caridataaktif_model->get_all_data($this->input->post('nmtb')),
			"recordsFiltered"     =>     $this->Caridataaktif_model->get_filtered_data($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value')),
			"data"                    =>     $data
		);
		echo json_encode($output);
	}

	function DataInvoice()
	{
		$result = $this->Invoice_model->DataInvoice($this->input->post('nomor'));
		echo json_encode($result);
	}

	function DataInvoiceBatalFP()
	{
		$result = $this->Invoice_model->DataInvoiceBatalFP($this->input->post('nomor'));
		// print_r($result);
		// die();
		echo json_encode($result);
	}

	function DataInvoiceDetail()
	{
		$result = $this->Invoice_model->DataInvoiceDetail($this->input->post('nomor'));
		// print_r($result);
		// die();
		echo json_encode($result);
	}

	// function CariDataReprintInvoice()
	// {
	// 	$fetch_data = $this->Caridataaktif_model->make_datatables($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value'));
	// 	$data = array();
	// 	foreach ($fetch_data as $row) {
	// 		$sub_array = array();
	// 		$i = 1;
	// 		$count = count($this->input->post('field'));
	// 		foreach ($this->input->post('field') as $key => $value) {
	// 			if ($i <= $count) {
	// 				if ($i == 1) {
	// 					$msearch = $row->$value;
	// 					// print_r($msearch);
	// 					$sub_array[] = '<button class="btn btn-sm btn-success accreprint" id="accreprint" style="margin: 0px;" data-id="' . $msearch . '"><i class="fas fa-check"></i>&nbsp;Accept Re-Print</button>';
	// 					// $sub_array[] = '<a href="cetak_pdf/Invoice/Print/' . $msearch . '" title="' . $msearch . '" style="background-color: #007bff;" class="btn btn-sm" target="_blank" data-id="' . $msearch . '"><i class="fa fa-print"></i>&nbsp;Re-Print</a>';
	// 					$sub_array[] = $row->$value;
	// 				} else {
	// 					if ($i == $count) {
	// 						$sub_array[] = $row->$value;
	// 					} else {
	// 						$sub_array[] = $row->$value;
	// 					}
	// 				}
	// 				// $sub_array[] = $row->$value;
	// 			}
	// 			$i++;
	// 		}
	// 		$data[] = $sub_array;
	// 	}
	// 	$output = array(
	// 		"draw"                    =>     intval($_POST["draw"]),
	// 		"recordsTotal"          =>      $this->Caridataaktif_model->get_all_data($this->input->post('nmtb')),
	// 		"recordsFiltered"     =>     $this->Caridataaktif_model->get_filtered_data($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value')),
	// 		"data"                    =>     $data
	// 	);
	// 	// die();
	// 	echo json_encode($output);
	// }

	function CariDataReqReprintInvoice()
	{
		$fetch_data = $this->Caridataaktif_model->make_datatables($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value'));
		$data = array();
		foreach ($fetch_data as $row) {
			$sub_array = array();
			$i = 1;
			$count = count($this->input->post('field'));
			foreach ($this->input->post('field') as $key => $value) {
				if ($i <= $count) {
					if ($i == 1) {
						$msearch = $row->$value;
						// print_r($msearch);
						$sub_array[] = '<button class="btn btn-sm btn-success reqreprintinvoice" id="reqreprintinvoice" style="margin: 0px;" data-id="' . $msearch . '"><i class="fas fa-check"></i>&nbsp;Req Re-Print Invoice</button>';
						// $sub_array[] = '<a href="cetak_pdf/Invoice/Print/' . $msearch . '" title="' . $msearch . '" style="background-color: #007bff;" class="btn btn-sm" target="_blank" data-id="' . $msearch . '"><i class="fa fa-print"></i>&nbsp;Re-Print</a>';
						$sub_array[] = $row->$value;
					} else {
						if ($i == $count) {
							$sub_array[] = $row->$value;
						} else {
							$sub_array[] = $row->$value;
						}
					}
					// $sub_array[] = $row->$value;
				}
				$i++;
			}
			$data[] = $sub_array;
		}
		$output = array(
			"draw"                    =>     intval($_POST["draw"]),
			"recordsTotal"          =>      $this->Caridataaktif_model->get_all_data($this->input->post('nmtb')),
			"recordsFiltered"     =>     $this->Caridataaktif_model->get_filtered_data($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value')),
			"data"                    =>     $data
		);
		// die();
		echo json_encode($output);
	}

	function CariDataReqReprintSuratJalan()
	{
		$fetch_data = $this->Caridataaktif_model->make_datatables($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value'));
		$data = array();
		foreach ($fetch_data as $row) {
			$sub_array = array();
			$i = 1;
			$count = count($this->input->post('field'));
			foreach ($this->input->post('field') as $key => $value) {
				if ($i <= $count) {
					if ($i == 1) {
						$msearch = $row->$value;
						// print_r($msearch);
						$sub_array[] = '<button class="btn btn-sm btn-success reqreprintsuratjalan" id="reqreprintsuratjalan" style="margin: 0px;" data-id="' . $msearch . '"><i class="fas fa-check"></i>&nbsp;Req Re-Print Surat Jalan</button>';
						// $sub_array[] = '<a href="cetak_pdf/Invoice/Print/' . $msearch . '" title="' . $msearch . '" style="background-color: #007bff;" class="btn btn-sm" target="_blank" data-id="' . $msearch . '"><i class="fa fa-print"></i>&nbsp;Re-Print</a>';
						$sub_array[] = $row->$value;
					} else {
						if ($i == $count) {
							$sub_array[] = $row->$value;
						} else {
							$sub_array[] = $row->$value;
						}
					}
					// $sub_array[] = $row->$value;
				}
				$i++;
			}
			$data[] = $sub_array;
		}
		$output = array(
			"draw"                    =>     intval($_POST["draw"]),
			"recordsTotal"          =>      $this->Caridataaktif_model->get_all_data($this->input->post('nmtb')),
			"recordsFiltered"     =>     $this->Caridataaktif_model->get_filtered_data($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value')),
			"data"                    =>     $data
		);
		// die();
		echo json_encode($output);
	}
	function CariDataAccReprintInvoice()
	{
		$fetch_data = $this->Caridataaktif_model->make_datatables($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value'));
		$data = array();
		foreach ($fetch_data as $row) {
			$sub_array = array();
			$i = 1;
			$count = count($this->input->post('field'));
			foreach ($this->input->post('field') as $key => $value) {
				if ($i <= $count) {
					if ($i == 1) {
						$msearch = $row->$value;
						// print_r($msearch);
						$sub_array[] = '<button class="btn btn-sm btn-success accreprintinvoice" id="accreprintinvoice" style="margin: 0px;" data-id="' . $msearch . '"><i class="fas fa-check"></i>&nbsp;Acc Re-Print Invoice</button>';
						// $sub_array[] = '<a href="cetak_pdf/Invoice/Print/' . $msearch . '" title="' . $msearch . '" style="background-color: #007bff;" class="btn btn-sm" target="_blank" data-id="' . $msearch . '"><i class="fa fa-print"></i>&nbsp;Re-Print</a>';
						$sub_array[] = $row->$value;
					} else {
						if ($i == $count) {
							$sub_array[] = $row->$value;
						} else {
							$sub_array[] = $row->$value;
						}
					}
					// $sub_array[] = $row->$value;
				}
				$i++;
			}
			$data[] = $sub_array;
		}
		$output = array(
			"draw"                    =>     intval($_POST["draw"]),
			"recordsTotal"          =>      $this->Caridataaktif_model->get_all_data($this->input->post('nmtb')),
			"recordsFiltered"     =>     $this->Caridataaktif_model->get_filtered_data($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value')),
			"data"                    =>     $data
		);
		// die();
		echo json_encode($output);
	}

	function CariDataAccReprintSuratJalan()
	{
		$fetch_data = $this->Caridataaktif_model->make_datatables($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value'));
		$data = array();
		foreach ($fetch_data as $row) {
			$sub_array = array();
			$i = 1;
			$count = count($this->input->post('field'));
			foreach ($this->input->post('field') as $key => $value) {
				if ($i <= $count) {
					if ($i == 1) {
						$msearch = $row->$value;
						// print_r($msearch);
						$sub_array[] = '<button class="btn btn-sm btn-success accreprintsuratjalan" id="accreprintsuratjalan" style="margin: 0px;" data-id="' . $msearch . '"><i class="fas fa-check"></i>&nbsp;Acc Re-Print Surat Jalan</button>';
						// $sub_array[] = '<a href="cetak_pdf/Invoice/Print/' . $msearch . '" title="' . $msearch . '" style="background-color: #007bff;" class="btn btn-sm" target="_blank" data-id="' . $msearch . '"><i class="fa fa-print"></i>&nbsp;Re-Print</a>';
						$sub_array[] = $row->$value;
					} else {
						if ($i == $count) {
							$sub_array[] = $row->$value;
						} else {
							$sub_array[] = $row->$value;
						}
					}
					// $sub_array[] = $row->$value;
				}
				$i++;
			}
			$data[] = $sub_array;
		}
		$output = array(
			"draw"                    =>     intval($_POST["draw"]),
			"recordsTotal"          =>      $this->Caridataaktif_model->get_all_data($this->input->post('nmtb')),
			"recordsFiltered"     =>     $this->Caridataaktif_model->get_filtered_data($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value')),
			"data"                    =>     $data
		);
		// die();
		echo json_encode($output);
	}

	function CariDataReprintInvoice()
	{
		$fetch_data = $this->Caridataaktif_model->make_datatables($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value'));
		$data = array();
		foreach ($fetch_data as $row) {
			$sub_array = array();
			$i = 1;
			$count = count($this->input->post('field'));
			foreach ($this->input->post('field') as $key => $value) {
				if ($i <= $count) {
					if ($i == 1) {
						$msearch = $row->$value;
						// print_r($msearch);
						$sub_array[] = '<button class="btn btn-sm btn-success reprintinvoice" id="reprintinvoice" style="background-color: #007bff; border: 1px solid #007bff;" data-id="' . $msearch . '"><i class="fa fa-print"></i>&nbsp;Re-Print Invoice</button>';
						// $sub_array[] = '<a href="cetak_pdf/Invoice/Print/' . $msearch . '" title="' . $msearch . '" style="background-color: #007bff;" class="btn btn-sm" target="_blank" data-id="' . $msearch . '"><i class="fa fa-print"></i>&nbsp;Re-Print</a>';
						$sub_array[] = $row->$value;
					} else {
						if ($i == $count) {
							$sub_array[] = $row->$value;
						} else {
							$sub_array[] = $row->$value;
						}
					}
					// $sub_array[] = $row->$value;
				}
				$i++;
			}
			$data[] = $sub_array;
		}
		$output = array(
			"draw"                    =>     intval($_POST["draw"]),
			"recordsTotal"          =>      $this->Caridataaktif_model->get_all_data($this->input->post('nmtb')),
			"recordsFiltered"     =>     $this->Caridataaktif_model->get_filtered_data($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value')),
			"data"                    =>     $data
		);
		// die();
		echo json_encode($output);
	}

	function CariDataReprintSuratJalan()
	{
		$fetch_data = $this->Caridataaktif_model->make_datatables($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value'));
		$data = array();
		foreach ($fetch_data as $row) {
			$sub_array = array();
			$i = 1;
			$count = count($this->input->post('field'));
			foreach ($this->input->post('field') as $key => $value) {
				if ($i <= $count) {
					if ($i == 1) {
						$msearch = $row->$value;
						// print_r($msearch);
						$sub_array[] = '<button class="btn btn-sm btn-success reprintsuratjalan" id="reprintsuratjalan" style="background-color: #007bff; border: 1px solid #007bff;" data-id="' . $msearch . '"><i class="fa fa-print"></i>&nbsp;Re-Print Surat Jalan</button>';
						// $sub_array[] = '<a href="cetak_pdf/Invoice/Print/' . $msearch . '" title="' . $msearch . '" style="background-color: #007bff;" class="btn btn-sm" target="_blank" data-id="' . $msearch . '"><i class="fa fa-print"></i>&nbsp;Re-Print</a>';
						$sub_array[] = $row->$value;
					} else {
						if ($i == $count) {
							$sub_array[] = $row->$value;
						} else {
							$sub_array[] = $row->$value;
						}
					}
					// $sub_array[] = $row->$value;
				}
				$i++;
			}
			$data[] = $sub_array;
		}
		$output = array(
			"draw"                    =>     intval($_POST["draw"]),
			"recordsTotal"          =>      $this->Caridataaktif_model->get_all_data($this->input->post('nmtb')),
			"recordsFiltered"     =>     $this->Caridataaktif_model->get_filtered_data($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value')),
			"data"                    =>     $data
		);
		// die();
		echo json_encode($output);
	}

	function Reprint()
	{
		$fetch_data = $this->Caridataaktif_model->make_datatables($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value'));
		$data = array();
		foreach ($fetch_data as $row) {
			$sub_array = array();
			$i = 1;
			$count = count($this->input->post('field'));
			foreach ($this->input->post('field') as $key => $value) {
				if ($i <= $count) {
					if ($i == 1) {
						$msearch = $row->$value;
						// print_r($msearch);
						$sub_array[] = '<button class="btn btn-sm btn-success reprint" style="margin: 0px; background-color: #007bff; border-color: #007bff;" data-id="' . $msearch . '"><i class="fas fa-print"></i>&nbsp;Re-Print</button>';
						// $sub_array[] = '<a href="cetak_pdf/Invoice/Print/' . $msearch . '" title="' . $msearch . '" style="background-color: #007bff;" class="btn btn-sm" target="_blank" data-id="' . $msearch . '"><i class="fa fa-print"></i>&nbsp;Re-Print</a>';
						$sub_array[] = $row->$value;
					} else {
						if ($i == $count) {
							$sub_array[] = $row->$value;
						} else {
							$sub_array[] = $row->$value;
						}
					}
					// $sub_array[] = $row->$value;
				}
				$i++;
			}
			$data[] = $sub_array;
		}
		$output = array(
			"draw"                    =>     intval($_POST["draw"]),
			"recordsTotal"          =>      $this->Caridataaktif_model->get_all_data($this->input->post('nmtb')),
			"recordsFiltered"     =>     $this->Caridataaktif_model->get_filtered_data($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value')),
			"data"                    =>     $data
		);
		// die();
		echo json_encode($output);
	}

	function CetakAdmin()
	{
		$fetch_data = $this->Caridataaktif_model->make_datatables($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value'));
		$data = array();
		foreach ($fetch_data as $row) {
			$sub_array = array();
			$i = 1;
			$count = count($this->input->post('field'));
			foreach ($this->input->post('field') as $key => $value) {
				if ($i <= $count) {
					if ($i == 1) {
						$msearch = $row->$value;
						// print_r($msearch);
						$sub_array[] = '<button class="btn btn-sm btn-success cetakadmin" style="margin: 0px; background-color: #007bff; border-color: #007bff;" data-id="' . $msearch . '"><i class="fas fa-print"></i>&nbsp;Cetak</button>';
						// $sub_array[] = '<a href="cetak_pdf/Invoice/Print/' . $msearch . '" title="' . $msearch . '" style="background-color: #007bff;" class="btn btn-sm" target="_blank" data-id="' . $msearch . '"><i class="fa fa-print"></i>&nbsp;Re-Print</a>';
						$sub_array[] = $row->$value;
					} else {
						if ($i == $count) {
							$sub_array[] = $row->$value;
						} else {
							$sub_array[] = $row->$value;
						}
					}
					// $sub_array[] = $row->$value;
				}
				$i++;
			}
			$data[] = $sub_array;
		}
		$output = array(
			"draw"                    =>     intval($_POST["draw"]),
			"recordsTotal"          =>      $this->Caridataaktif_model->get_all_data($this->input->post('nmtb')),
			"recordsFiltered"     =>     $this->Caridataaktif_model->get_filtered_data($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value')),
			"data"                    =>     $data
		);
		// die();
		echo json_encode($output);
	}

	function ReprintSJ()
	{
		$result = $this->db->get_where('srvt_suratjalan', array('nomorgb' => $this->input->post('nomor')))->result();
		// print_r($result);
		// die();
		echo json_encode($result);
	}

	function CariDataPacking()
	{
		$fetch_data = $this->Caridataaktif_model->make_datatables($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value'));
		$data = array();
		foreach ($fetch_data as $row) {
			$sub_array = array();
			$i = 1;
			$count = count($this->input->post('field'));
			foreach ($this->input->post('field') as $key => $value) {
				if ($i <= $count) {
					if ($i == 1) {
						$msearch = $row->$value;
						$where = array(
							"nomor" => $row->$value,
							"batal" => false
						);
						$getdatastatusTombol = $this->global_model->GetDataGlobal("cari_packing", $where);
						if (!empty($getdatastatusTombol)) {
							foreach ($getdatastatusTombol as $key => $valuetombol) {
								if ($valuetombol->jenisjual == 	"1" || $valuetombol->jenisjual == "4") {
									$sub_array[] = '<button class="btn btn-dark searchpacking" data-dismiss="modal" style="margin: 0px; background-color: #007bff; border-color: #007bff;" data-id="' . $msearch . '"><i class="far fa-check-circle"></i></button>';
									// $sub_array[] = $row->$value;
								} else {
									$sub_array[] = '<button class="btn btn-dark searchpacking" data-dismiss="modal" style="margin: 0px; background-color: #439856; border-color: #439856;" data-id="' . $msearch . '"><i class="far fa-check-circle"></i></button>';
									// $sub_array[] = $row->$value;
								}
							}
						}
						$sub_array[] = $row->$value;
					} else {
						if ($i == $count) {
							$sub_array[] = $row->$value;
						} else {
							$sub_array[] = $row->$value;
						}
					}
					// $sub_array[] = $row->$value;
				}
				$i++;
			}
			$data[] = $sub_array;
		}
		$output = array(
			"draw"                    =>     intval($_POST["draw"]),
			"recordsTotal"          =>      $this->Caridataaktif_model->get_all_data($this->input->post('nmtb')),
			"recordsFiltered"     =>     $this->Caridataaktif_model->get_filtered_data($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value')),
			"data"                    =>     $data
		);
		echo json_encode($output);
	}

	function DataPacking()
	{
		$result = $this->Invoice_model->DataPacking($this->input->post('nomor'));
		echo json_encode($result);
	}

	function DataPckDetail()
	{
		$result = $this->Invoice_model->DataPckDetail($this->input->post('nomor'));
		echo json_encode($result);
	}

	function CariDataPackingDetail()
	{
		$fetch_data = $this->Caridataaktif_model->make_datatables($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value'));
		$data = array();
		foreach ($fetch_data as $row) {
			$sub_array = array();
			$i = 1;
			$count = count($this->input->post('field'));
			foreach ($this->input->post('field') as $key => $value) {
				if ($i <= $count) {
					if ($i == 1) {
						$msearch = $row->$value;
						$sub_array[] = '<button class="btn btn-dark searchpackingdetail" data-dismiss="modal" style="margin: 0px;" data-id="' . $msearch . '"><i class="far fa-check-circle"></i></button>';
						$sub_array[] = $row->$value;
					} else {
						if ($i == $count) {
							$sub_array[] = $row->$value;
						} else {
							$sub_array[] = $row->$value;
						}
					}
					// $sub_array[] = $row->$value;
				}
				$i++;
			}
			$data[] = $sub_array;
		}
		$output = array(
			"draw"                    =>     intval($_POST["draw"]),
			"recordsTotal"          =>      $this->Caridataaktif_model->get_all_data($this->input->post('nmtb')),
			"recordsFiltered"     =>     $this->Caridataaktif_model->get_filtered_data($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value')),
			"data"                    =>     $data
		);
		echo json_encode($output);
	}

	function CariDataInvBatal()
	{
		$fetch_data = $this->Caridataaktif_model->make_datatables($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value'));
		$data = array();
		foreach ($fetch_data as $row) {
			$sub_array = array();
			$i = 1;
			$count = count($this->input->post('field'));
			foreach ($this->input->post('field') as $key => $value) {
				if ($i <= $count) {
					if ($i == 1) {
						$msearch = $row->$value;
						$sub_array[] = '<button class="btn btn-dark searchinvbatal" data-dismiss="modal" style="margin: 0px;" data-id="' . $msearch . '"><i class="far fa-check-circle"></i></button>';
						$sub_array[] = $row->$value;
					} else {
						if ($i == $count) {
							$sub_array[] = $row->$value;
						} else {
							$sub_array[] = $row->$value;
						}
					}
					// $sub_array[] = $row->$value;
				}
				$i++;
			}
			$data[] = $sub_array;
		}
		$output = array(
			"draw"                    =>     intval($_POST["draw"]),
			"recordsTotal"          =>      $this->Caridataaktif_model->get_all_data($this->input->post('nmtb')),
			"recordsFiltered"     =>     $this->Caridataaktif_model->get_filtered_data($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value')),
			"data"                    =>     $data
		);
		echo json_encode($output);
	}



	function DataPackingDetail()
	{
		$result = $this->Invoice_model->DataPackingDetail($this->input->post('nomor'), $this->input->post('kodebarang'));
		echo json_encode($result);
	}

	public function ClearChr($param)
	{
		return $result = str_replace(array('\'', '"', ',', ';', '<', '>', '_', '-', ' ', '.', '!', '?', '/', '=', '+', ']', '[', '{', '}', '@', '#', '$', '%', '^', '&', "*", '(', ')'), '', $param);
	}

	function DataInvBatal()
	{
		$result = $this->Invoice_model->DataInvBatal($this->input->post('nomor'));
		// $where = array(
		// 	"noinvoice" => $this->input->post('nomor'),
		// 	"statuspakai" => false
		// );
		// $result = $this->global_model->GetDataGlobal('hist_cancel', $where);
		echo json_encode($result);
	}



	function Save()
	{
		$kodecabang = $this->session->userdata('mycabang');
		$pemakai = $this->session->userdata('myusername');
		$periode = date('Y') . date('m');
		$kodegudang = "";
		$tanggal = $this->input->post('tanggalinv');
		$nilaialokasi = 0;
		$where = array();
		$nomorinvoice = $this->input->post('noinvoice');
		$nomorgb = $this->input->post('nomorgb');
		$nomorsj = "";
		$errorvalidasi = FALSE;
		$statustarikbatal = FALSE;
		$datadetail = $this->input->post('datadetail');

		$nopacking = $this->input->post('nopacking');
		$tgljthtempo = $this->input->post('tgljthtempo');
		$nomorso = $this->input->post('nomorso');
		$tanggalso = $this->input->post('tanggalso');
		$nopelanggan = $this->input->post('nopelanggan');
		$namapelanggan = $this->input->post('namapelanggan');
		$nofakturpajak = $this->input->post('nofaktur');
		$rekeningtf = $this->input->post('rekeningtf');
		$discmanual = floatval(str_replace(",", "", $this->input->post('discmanual')));
		$nilaimanual = floatval(str_replace(",", "", $this->input->post('nilaimanual')));

		$kodetujuan = $this->input->post('lokasi');
		$disccash = floatval(str_replace(",", "", $this->input->post('discash')));
		$disccustomer = floatval(str_replace(",", "", $this->input->post('discustomer')));
		$nilaicash = floatval(str_replace(",", "", $this->input->post('nilaicash')));
		$nilaicustomer = floatval(str_replace(",", "", $this->input->post('nilaicustomer')));
		$total = floatval(str_replace(",", "", $this->input->post('total')));
		$dpp = floatval(str_replace(",", "", $this->input->post('dpp')));
		$ppn = floatval(str_replace(",", "", $this->input->post('ppn')));
		$grandtotal = floatval(str_replace(",", "", $this->input->post('grandtotal')));

		$selisihbulan = 0;

		$diff = abs(strtotime($tanggal) - strtotime(date('Y-m-d')));
		$years = floor($diff / (365 * 60 * 60 * 24));
		$bulan = floor(($diff - $years * 365 * 60 * 60 * 24) / (30 * 60 * 60 * 24));
		if ((date('m') == date('m', strtotime($tanggal)) && (date('Y') == date('Y', strtotime($tanggal))))) {
			$selisihbulan = 0;
		} else if ((date('m') == date('m', strtotime($tanggal)) && (date('Y') > date('Y', strtotime($tanggal))))) {
			$selisihbulan = $bulan - 1 + ($years * 12);
		} else if ((date('m') != date('m', strtotime($tanggal)) && (date('Y') == date('Y', strtotime($tanggal))))) {
			$selisihbulan = $bulan + 1 + ($years * 12);
		} else {
			$selisihbulan = $bulan + ($years * 12);
		}

		$where = array(
			"nomor" => $nomorso
		);
		$ceklokasigudang = $this->global_model->GetDataGlobal("srvt_salesorder", $where);
		if (empty($ceklokasigudang)) {
			$resultjson = array(
				'nomor' => "",
				'nosuratjalan' => "",
				'message' => "Data gagal dibatalkan, Kode Gudang Tidak Terdaftar."
			);
			$errorvalidasi = TRUE;
			echo json_encode($resultjson);
			return FALSE;
		} else {
			$kodegudang = $ceklokasigudang[0]->lokasi;
		}

		$KodeGlobal = $kodecabang;
		$cekcabang = $this->Invoice_model->CekKodeCabang($kodecabang);
		// print_r($cekcabang);
		// die();
		$pajangkode = strlen($kodecabang . $rekeningtf);
		// print_r($pajangkode);
		// die();
		$ambilkodeprefix = $kodecabang . $rekeningtf;
		$ambilnomor = $ambilkodeprefix . "-" . date("Y") . "-" . date("m");
		// print_r($ambilkodeprefix);
		// die();
		$ambilnomorSJ = "SJ" . "-" . date("Y") . "-" . date("m");
		$ambilnomorGB = $kodecabang . "HS" . "-" . date("Y") . "-" . date("m");
		if ($nomorinvoice == "") {
			$where = array("pkp" => true);
			$konfigurasi = $this->global_model->GetDataGlobal('stpm_konfigurasi', $where);
			if (!empty($konfigurasi)) {
				if (strlen($ambilnomor) == 12) {
					$get["invpkp"] = $this->Invoice_model->GetMaxNomorPKP($ambilnomor);
					// print_r($get["invpkp"]);
					// die();
				} else if (strlen($ambilnomor) == 11) {
					$get["invpkp"] = $this->Invoice_model->GetMaxNomorPKP1($ambilnomor);
					// print_r($get["invpkp"]);
					// die();
				}

				$get["invgbl"] = $this->Invoice_model->GetMaxNomorGlobal($ambilnomorGB);
				// print_r($get["invgbl"]);
				// die();
				if (!$get["invpkp"]) {
					$nomorinvoice = $ambilnomor . "-" . "00001";
					// $nomorsj = $ambilnomorSJ . "-" . "00001";
					// $nomorgb = $ambilnomorGB . "-" . "00001";
				} else {
					$lastNomor = $get['invpkp']->nomor;
					if (strlen($ambilnomor) == 12) {
						$lastNoUrut = substr($lastNomor, 13, 11);
					} else if (strlen($ambilnomor) == 11) {
						$lastNoUrut = substr($lastNomor, 12, 11);
					}

					// nomor urut ditambah 1
					$nextNoUrut = $lastNoUrut + 1;
					// $nomor = $ambilnomor . "-" . sprintf('%05s', $nextNoUrut);
					$nomorinvoice = $ambilnomor . "-" . sprintf('%05s', $nextNoUrut);
					// $nomorsj = $ambilnomorSJ . "-" . sprintf('%05s', $nextNoUrut);
					// $nomorgb = $ambilnomorGB . "-" . sprintf('%05s', $nextNoUrut);
					// print_r($nomorinvoice . "<br>");
					// print_r($nomorsj);
				}

				if (!$get["invgbl"]) {
					$nomorsj = $ambilnomorSJ . "-" . "00001";
					$nomorgb = $ambilnomorGB . "-" . "00001";
				} else {
					$lastglobal = $get['invgbl']->nomorgb;
					$lastnoglobal = substr($lastglobal, 12, 11);
					// print_r($lastnoglobal);
					// die();

					$nextnoglobal = $lastnoglobal + 1;
					$nomorsj = $ambilnomorSJ . "-" . sprintf('%05s', $nextnoglobal);
					$nomorgb = $ambilnomorGB . "-" . sprintf('%05s', $nextnoglobal);
				}

				// $where = array(
				// 	"noinvoice" => $nomorinvoice,
				// 	"nosuratjalan" => $nomorsj,
				// 	"noglobal" => $nomorgb,
				// 	"kodecabang" => $kodecabang,
				// 	"statuspakai" => false
				// );
				// $nobatal = $this->global_model->GetDataGlobal('hist_cancel', $where);
				// if (!empty($nobatal)) {
				// 	$resultjson = array(
				// 		'nomor' => "",
				// 		'nomorsj' => "",
				// 		'noglobal' => "",
				// 		'lokasi' => "",
				// 		'message' => "Data gagal disimpan, No Invoice '<b style='color: red'>" . $nomorinvoice . "</b>' Masih Ada di no yang Batal Silahkan Hubungi HO."
				// 	);
				// 	$errorvalidasi = TRUE;
				// 	echo json_encode($resultjson);
				// 	return FALSE;
				// }
			} else {
				$get["inv"] = $this->Invoice_model->GetMaxNomor($ambilnomor);
				if (!$get["inv"]) {
					$nomor = $ambilnomor . "-" . "00001";
				} else {
					$lastNomor = $get['inv']->nomor;
					if (strlen($rekeningtf) == 4) {
						$lastNoUrut = substr($lastNomor, 13, 11);
					} else if (strlen($rekeningtf) == 3) {
						$lastNoUrut = substr($lastNomor, 12, 11);
					}

					// nomor urut ditambah 1
					$nextNoUrut = $lastNoUrut + 1;
					// $nomor = $ambilnomor . "-" . sprintf('%05s', $nextNoUrut);
					$nomorinvoice = $ambilnomor . "-" . sprintf('%05s', $nextNoUrut);
					$nomorsj = $ambilnomorSJ . "-" . sprintf('%05s', $nextNoUrut);
					$nomorgb = $ambilnomorGB . "-" . sprintf('%05s', $nextNoUrut);
				}
				$where = array(
					"noinvoice" => $nomorinvoice,
					"nosuratjalan" => $nomorsj,
					"noglobal" => $nomorgb,
					"kodecabang" => $kodecabang,
					"statuspakai" => false
				);
				$nobatal = $this->global_model->GetDataGlobal('hist_cancel', $where);
				if (!empty($nobatal)) {
					$resultjson = array(
						'nomor' => "",
						'nomorsj' => "",
						'nomorgb' => "",
						'lokasi' => "",
						'message' => "Data gagal disimpan, No Invoice '<b style='color: red'>" . $nomorinvoice . "</b>' Masih Ada di no yang Batal Silahkan Hubungi HO."
					);
					$errorvalidasi = TRUE;
					echo json_encode($resultjson);
					return FALSE;
				}
			}
		} else {
			$where = array(
				"noinvoice" => $nomorinvoice,
				"kodecabang" => $kodecabang,
				"statuspakai" => false
			);
			$nobatal = $this->global_model->GetDataGlobal('hist_cancel', $where);
			if (empty($nobatal)) {
				$resultjson = array(
					'nomor' => "",
					'nomorsj' => "",
					'nomorgb' => "",
					'lokasi' => "",
					'message' => "Data gagal disimpan, No Invoice '<b style='color: red'>" . $nomorinvoice . "</b>' Sudah Terpakai."
				);
				$errorvalidasi = TRUE;
				echo json_encode($resultjson);
				return FALSE;
			} else {
				$nomorinvoice = $nobatal[0]->noinvoice;
				$nomorsj = $nobatal[0]->nosuratjalan;
				$statustarikbatal = TRUE;
			}
		}

		if (empty($datadetail)) {
			$resultjson = array(
				'nomor' => "",
				'nomorsj' => "",
				'lokasi' => "",
				'message' => "Data gagal disimpan, Data Detail Tidak Ada."
			);
			$errorvalidasi = TRUE;
			echo json_encode($resultjson);
			return FALSE;
		}

		if ($errorvalidasi == FALSE) {

			$this->db->trans_start(); # Starting Transaction
			$this->db->trans_strict(FALSE);


			foreach ($datadetail as $key => $value) {


				if ($selisihbulan > 0) {
					$saldoawal = 0;
					for ($i = 0; $i <= $selisihbulan; $i++) {
						$periodestock = date('Ym', strtotime('+' . $i . ' month', strtotime($tanggal)));
						$where = array(
							"periode" => $periodestock,
							"kodecabang" => $kodecabang,
							"kodegudang" => $kodegudang,
							"kodebarang" => $value['Kode']
						);
						$datastock = $this->global_model->GetDataGlobal("srvt_inventorystok", $where);
						if (!empty($datastock)) {
							foreach ($datastock as $key => $value2) {
								if ($saldoawal == 0) {
									$qtykeluar = ($value2->qtykeluar + floatval(str_replace(",", "", $value['Qty'])));
									$qtypick = ($value2->qtypick - floatval(str_replace(",", "", $value['Qty'])));
									$setdata = "qtykeluar = '" . $qtykeluar . "',qtypick = '" . $qtypick . "'";
									$wheredata = "periode = '" . $periodestock . "' AND kodecabang = '" . $kodecabang . "' AND kodegudang = '" . $kodegudang . "' AND kodebarang = '" . $value['Kode'] . "'";
									$this->Invoice_model->UpdateDataInventory("srvt_inventorystok", $setdata, $wheredata);
								} else {
									$qtykeluar = ($value2->qtykeluar + floatval(str_replace(",", "", $value['Qty'])));
									$qtypick = ($value2->qtypick - floatval(str_replace(",", "", $value['Qty'])));
									$setdata = "qtyawal = '" . $saldoawal . "',qtypick = '" . $qtypick . "'";
									$wheredata = "periode = '" . $periodestock . "' AND kodecabang = '" . $kodecabang . "' AND kodegudang = '" . $kodegudang . "' AND kodebarang = '" . $value['Kode'] . "'";
									$this->Invoice_model->UpdateDataInventory("srvt_inventorystok", $setdata, $wheredata);
								}
								$ceksaldoawal = $this->global_model->GetDataGlobal("srvt_inventorystok", $where);
								if (!empty($ceksaldoawal)) {
									foreach ($ceksaldoawal as $key => $valuesaldoawal) {
										$saldoawal = $valuesaldoawal->qtyawal + $valuesaldoawal->qtymasuk - $valuesaldoawal->qtykeluar;	# code...
									}
								}
							}
						} else {
							if ($saldoawal == 0) {
								$datainsert = array(
									"periode" => $periodestock,
									"kodecabang" => $kodecabang,
									"kodegudang" => $kodegudang,
									"kodebarang" => $value['Kode'],
									"qtyawal" => $saldoawal,
									"qtymasuk" =>  0,
									"qtykeluar" => floatval(str_replace(",", "", $value['Qty'])),
									"qtypick" => 0,
									"cogs" => 0,
									'pemakai' => $pemakai,
									'tglsimpan' => date('Y-m-d H:i:s'),
								);
								$datastock = $this->global_model->InsertDataGlobal("srvt_inventorystok", $datainsert);
							} else {
								$datainsert = array(
									"periode" => $periodestock,
									"kodecabang" => $kodecabang,
									"kodegudang" => $kodegudang,
									"kodebarang" => $value['Kode'],
									"qtyawal" => $saldoawal,
									"qtymasuk" =>  0,
									"qtykeluar" => 0,
									"qtypick" => 0,
									"cogs" => 0,
									'pemakai' => $pemakai,
									'tglsimpan' => date('Y-m-d H:i:s'),
								);
								$datastock = $this->global_model->InsertDataGlobal("srvt_inventorystok", $datainsert);
							}
							$ceksaldoawal = $this->global_model->GetDataGlobal("srvt_inventorystok", $where);
							if (!empty($ceksaldoawal)) {
								foreach ($ceksaldoawal as $key => $valuesaldoawal) {
									$saldoawal = $valuesaldoawal->qtyawal + $valuesaldoawal->qtymasuk - $valuesaldoawal->qtykeluar;	# code...
								}
							}
						}
					}
				} else {
					$where = array(
						"periode" => $periode,
						"kodecabang" => $kodecabang,
						"kodegudang" => $kodegudang,
						"kodebarang" => $value['Kode']
					);
					$datastock = $this->global_model->GetDataGlobal("srvt_inventorystok", $where);
					if (!empty($datastock)) {
						foreach ($datastock as $key => $value2) {
							$qtykeluar = ($value2->qtykeluar + floatval(str_replace(",", "", $value['Qty'])));
							$qtypick = ($value2->qtypick - floatval(str_replace(",", "", $value['Qty'])));
							$setdata = "qtykeluar = '" . $qtykeluar . "',qtypick = '" . $qtypick . "'";
							$wheredata = "periode = '" . $periode . "' AND kodecabang = '" . $kodecabang . "' AND kodegudang = '" . $kodegudang . "' AND kodebarang = '" . $value['Kode'] . "'";
							$this->Invoice_model->UpdateDataInventory("srvt_inventorystok", $setdata, $wheredata);
						}
					}
				}

				// $data['inven'] = $this->global_model->GetDataGlobal("srvt_inventorystok", $where);
				// foreach ($data['inven'] as $values) {

				// 	$datainventory = [
				// 		'qtypick' => $values->qtypick - floatval(str_replace(",", "", $value['Qty'])),
				// 		'qtykeluar' => $values->qtykeluar + floatval(str_replace(",", "", $value['Qty']))
				// 	];
				// 	$this->Invoice_model->UpdateDataInventory($periode, $kodegudang, $value['Kode'], $datainventory);
				// }

				$where = array(
					"periode" => $periode,
					"kodegudang" => $kodegudang,
					"kodebarang" => $value['Kode']
				);
				$datadetail = [
					'nomor' => $nomorinvoice,
					'kodebarang' => $value['Kode'],
					'namabarang' => $value['Nama'],
					'qty' => floatval(str_replace(",", "", $value['Qty'])),
					'harga' => floatval(str_replace(",", "", $value['Harga'])),
					'discbarang' => $value['Disc'],
					'discbrgitem' => floatval(str_replace(",", "", $value['DiscItem'])),
					'discpaket' => $value['DiscPaket'],
					'discpaketitem' => floatval(str_replace(",", "", $value['DiscPaketItem'])),
					'total' => floatval(str_replace(",", "", $value['Subtotal'])),
					'pemakai' => $pemakai,
					'tglsimpan' => date('Y-m-d H:i:s'),
					'kodecabang' => $kodecabang,
					'nomorgb' => $nomorgb
				];
				// print_r($datadetail);
				// die();
				$this->Invoice_model->SaveDataDetail($datadetail);

				$datadetailsj = [
					'nomor' => $nomorsj,
					'tanggal' => $tanggal,
					'kodebarang' => $value['Kode'],
					'namabarang' => $value['Nama'],
					'qty' => floatval(str_replace(",", "", $value['Qty'])),
					'pemakai' => $pemakai,
					'tglsimpan' => date('Y-m-d H:i:s'),
					'noreferensi' => $nomorinvoice,
					'kodecabang' => $kodecabang,
					'nomorgb' => $nomorgb
				];
				// print_r($datadetailsj);
				// die();
				$this->Invoice_model->SaveDataSJDetail($datadetailsj);
			}


			$data = [
				'nomor' => $nomorinvoice,
				'nomorgb' => $nomorgb,
				'tanggal' => $tanggal,
				'nopacking' => $nopacking,
				'tgljthtempo' => $tgljthtempo,
				'nomorso' => $nomorso,
				'tanggalso' => $tanggalso,
				'nopelanggan' => $nopelanggan,
				'namapelanggan' => $namapelanggan,
				'nofakturpajak' => $nofakturpajak,
				'lokasi' => $kodegudang,
				'kodetujuan' => $kodetujuan,
				'disccash' => floatval(str_replace(",", "", $disccash)),
				'disccustomer' => floatval(str_replace(",", "", $disccustomer)),
				'nilaicash' => floatval(str_replace(",", "", $nilaicash)),
				'nilaicustomer' => floatval(str_replace(",", "", $nilaicustomer)),
				'total' => floatval(str_replace(",", "", $total)),
				'dpp' => floatval(str_replace(",", "", $dpp)),
				'ppn' => floatval(str_replace(",", "", $ppn)),
				'grandtotal' => floatval(str_replace(",", "", $grandtotal)),
				'kodecabang' => $kodecabang,
				'pemakai' => $pemakai,
				'tglsimpan' => date('Y-m-d H:i:s'),
				'discmanual' => floatval(str_replace(",", "", $discmanual)),
				'nilaimanual' => floatval(str_replace(",", "", $nilaimanual)),
			];
			// print_r($data);
			// die();
			$this->Invoice_model->SaveData($data);
			$datasj = [
				'nomor' => $nomorsj,
				'tanggal' => $tanggal,
				'noinvoice' => $nomorinvoice,
				'kodegudang' => $this->input->post('lokasi'),
				'kodecabang' => $kodecabang,
				'pemakai' => $pemakai,
				'tglsimpan' => date('Y-m-d H:i:s'),
				'nomorgb' => $nomorgb
			];
			// print_r($datasj);
			// die();
			$this->Invoice_model->SaveDataSJ($datasj);

			$dataSales = $this->Invoice_model->GetDataSO($this->input->post('nomorso'));
			if (empty($dataSales)) {
				$nilaialokasi = 0;
			} else {
				$nilaialokasi = $dataSales[0]->nilaiuangmuka;
			}

			$dataar = [
				'nomor' => $nomorinvoice,
				'jenis' => 'AR',
				'tanggal' => $tanggal,
				'tgljthtempo' => $this->input->post('tgljthtempo'),
				'nopelanggan' => $this->input->post('nopelanggan'),
				'nilaipiutang' => floatval(str_replace(",", "", $this->input->post('grandtotal'))),
				'nilaialokasi' => $nilaialokasi,
				'nilaipenerimaan' => 0,
				'pemakai' => $pemakai,
				'tglsimpan' => date('Y-m-d H:i:s'),
				'kodecabang' => $kodecabang,
				'nomorgb' => $nomorgb
			];
			// print_r($dataar);
			// die();
			$this->Invoice_model->SaveDataAR($dataar);

			$datapacking = [
				'status' => true
			];
			$this->Invoice_model->UpdateDataPacking($this->input->post('nopacking'), $datapacking);

			$updateSO = [
				'kodestatus' => 2
			];
			$this->Invoice_model->UpdateStatusSO($this->input->post('nomorso'), $updateSO);

			if ($statustarikbatal == TRUE) {
				$dataupdate = array('statuspakai' => true);
				$where = array(
					'noinvoice' => $nomorinvoice,
					'nosuratjalan' => $nomorsj,
				);
				$this->global_model->UpdateDataGlobal("hist_cancel", $dataupdate, $where);

				$dataupdate = array('statusrevisi' => true);
				$where = array(
					'nomor' => $nomorinvoice,
				);
				$this->global_model->UpdateDataGlobal("srvt_invoice", $dataupdate, $where);
			}
			// die();

			$this->db->trans_complete();

			if ($this->db->trans_status() === FALSE) {
				$resultjson = array(
					'nomor' => "",
					'nomorsj' => "",
					'lokasi' => "",
					'message' => "Data gagal disimpan, Nomor '<b style='color: red'>" . $nomor . "</b>' sudah pernah digunakan."
				);
				# Something went wrong.
				$this->db->trans_rollback();
			} else {
				$resultjson = array(
					'nomor' => $nomorinvoice,
					'nomorsj' => $nomorsj,
					'lokasi' => $kodetujuan,
					'kodecabang' => $kodecabang,
					'nomorgb' => $nomorgb,
					'message' => "Data berhasil disimpan."
				);
				# Everything is Perfect. 
				# Committing data to the database.
				$this->db->trans_commit();
			}
			echo json_encode($resultjson);
		}
	}

	function AccReprintInvoice()
	{
		$kodecabang = $this->session->userdata('mycabang');
		$pemakai = $this->session->userdata('myusername');

		$nomor = $this->input->post('nomor');

		$this->db->trans_start(); # Starting Transaction
		$this->db->trans_strict(FALSE);

		$data = [
			// 'reqreprint' => false,
			'reprint' => true
		];
		$this->Invoice_model->UpdateData($nomor, $data);

		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE) {
			$resultjson = array(
				'nomor' => "",
				'message' => "Data gagal di Acc Re-Print Invoice, Nomor '<b style='color: red'>" . $nomor . "</b>' sudah pernah digunakan."
			);
			# Something went wrong.
			$this->db->trans_rollback();
		} else {
			$resultjson = array(
				'nomor' => $nomor,
				'message' => "Data berhasil di Acc Re-Print Invoice."
			);
			# Everything is Perfect. 
			# Committing data to the database.
			$this->db->trans_commit();
		}
		echo json_encode($resultjson);
	}

	function AccReprintSuratJalan()
	{
		$kodecabang = $this->session->userdata('mycabang');
		$pemakai = $this->session->userdata('myusername');

		$nomor = $this->input->post('nomor');

		$this->db->trans_start(); # Starting Transaction
		$this->db->trans_strict(FALSE);

		$data = [
			// 'reqreprint' => false,
			'reprint' => true
		];
		$this->Invoice_model->UpdateDataSJ($nomor, $data);

		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE) {
			$resultjson = array(
				'nomor' => "",
				'message' => "Data gagal di Acc Re-Print Surat Jalan, Nomor '<b style='color: red'>" . $nomor . "</b>' sudah pernah digunakan."
			);
			# Something went wrong.
			$this->db->trans_rollback();
		} else {
			$resultjson = array(
				'nomor' => $nomor,
				'message' => "Data berhasil di Acc Re-Print Surat Jalan."
			);
			# Everything is Perfect. 
			# Committing data to the database.
			$this->db->trans_commit();
		}
		echo json_encode($resultjson);
	}

	function UpdateReprintInvoice()
	{
		$kodecabang = $this->session->userdata('mycabang');
		$pemakai = $this->session->userdata('myusername');

		$nomor = $this->input->post('nomor');

		$this->db->trans_start(); # Starting Transaction
		$this->db->trans_strict(FALSE);

		$data = [
			'reqreprint' => false,
			'reprint' => false
		];
		$this->Invoice_model->UpdateData($nomor, $data);

		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE) {
			$resultjson = array(
				'nomor' => "",
				'message' => "Data gagal di Re-Print Invoice, Nomor '<b style='color: red'>" . $nomor . "</b>' sudah pernah digunakan."
			);
			# Something went wrong.
			$this->db->trans_rollback();
		} else {
			$resultjson = array(
				'nomor' => $nomor,
				'message' => "Data berhasil di Re-Print Invoice."
			);
			# Everything is Perfect. 
			# Committing data to the database.
			$this->db->trans_commit();
		}
		echo json_encode($resultjson);
	}

	function UpdateReprintSuratJalan()
	{
		$kodecabang = $this->session->userdata('mycabang');
		$pemakai = $this->session->userdata('myusername');

		$nomor = $this->input->post('nomor');

		$this->db->trans_start(); # Starting Transaction
		$this->db->trans_strict(FALSE);

		$data = [
			'reqreprint' => false,
			'reprint' => false
		];
		$this->Invoice_model->UpdateDataSJ($nomor, $data);

		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE) {
			$resultjson = array(
				'nomor' => "",
				'message' => "Data gagal di Re-Print Surat Jalan, Nomor '<b style='color: red'>" . $nomor . "</b>' sudah pernah digunakan."
			);
			# Something went wrong.
			$this->db->trans_rollback();
		} else {
			$resultjson = array(
				'nomor' => $nomor,
				'message' => "Data berhasil di Re-Print Surat Jalan."
			);
			# Everything is Perfect. 
			# Committing data to the database.
			$this->db->trans_commit();
		}
		echo json_encode($resultjson);
	}

	function ReqReprintInvoice()
	{
		$kodecabang = $this->session->userdata('mycabang');
		$pemakai = $this->session->userdata('myusername');

		$nomor = $this->input->post('nomor');

		$this->db->trans_start(); # Starting Transaction
		$this->db->trans_strict(FALSE);

		$data = [
			'reqreprint' => true
		];
		$this->Invoice_model->UpdateData($nomor, $data);

		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE) {
			$resultjson = array(
				'nomor' => "",
				'message' => "Data gagal di Req Re-Print Invoice, Nomor '<b style='color: red'>" . $nomor . "</b>' sudah pernah digunakan."
			);
			# Something went wrong.
			$this->db->trans_rollback();
		} else {
			$resultjson = array(
				'nomor' => $nomor,
				'message' => "Data berhasil di Req Re-Print Invoice."
			);
			# Everything is Perfect. 
			# Committing data to the database.
			$this->db->trans_commit();
		}
		echo json_encode($resultjson);
	}

	function ReqReprintSuratJalan()
	{
		$kodecabang = $this->session->userdata('mycabang');
		$pemakai = $this->session->userdata('myusername');

		$nomor = $this->input->post('nomor');

		$this->db->trans_start(); # Starting Transaction
		$this->db->trans_strict(FALSE);

		$data = [
			'reqreprint' => true
		];
		$this->Invoice_model->UpdateDataSJ($nomor, $data);

		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE) {
			$resultjson = array(
				'nomor' => "",
				'message' => "Data gagal di Req Re-Print Surat Jalan, Nomor '<b style='color: red'>" . $nomor . "</b>' sudah pernah digunakan."
			);
			# Something went wrong.
			$this->db->trans_rollback();
		} else {
			$resultjson = array(
				'nomor' => $nomor,
				'message' => "Data berhasil di Req Re-Print Surat Jalan."
			);
			# Everything is Perfect. 
			# Committing data to the database.
			$this->db->trans_commit();
		}
		echo json_encode($resultjson);
	}

	function ReprintOk()
	{
		$kodecabang = $this->session->userdata('mycabang');
		$pemakai = $this->session->userdata('myusername');

		$nomor = $this->input->post('noinvoice');

		$this->db->trans_start(); # Starting Transaction
		$this->db->trans_strict(FALSE);

		$data = [
			'reprint' => false
		];
		$this->Invoice_model->UpdateData($nomor, $data);

		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE) {
			$resultjson = array(
				'nomor' => "",
				'message' => "Data gagal di Re-Print, Nomor '<b style='color: red'>" . $nomor . "</b>' sudah pernah digunakan."
			);
			# Something went wrong.
			$this->db->trans_rollback();
		} else {
			$resultjson = array(
				'nomor' => $nomor,
				'message' => "Data berhasil di Re-Print."
			);
			# Everything is Perfect. 
			# Committing data to the database.
			$this->db->trans_commit();
		}
		echo json_encode($resultjson);
	}

	function Cancel()
	{

		$kodecabang =  $this->session->userdata('mycabang');
		$kodegudang = "";
		$pemakai =  $this->session->userdata('myusername');
		$errorvalidasi = FALSE;
		$noinvoice = $this->input->post('nomor');
		$nomorgb = $this->input->post('nomorgb');
		$nosuratjalan = "";
		$revisike = 0;
		$nomorso = $this->input->post('nomorso');
		$nopacking = $this->input->post('nopacking');
		$keterangan = $this->input->post('keterangan');
		$tanggalinv = $this->input->post('tanggalinv');
		$datadetail = $this->input->post('datadetail');
		$periode = date('Y') . date('m');

		$selisihbulan = 0;

		$diff = abs(strtotime($tanggalinv) - strtotime(date('Y-m-d')));
		$years = floor($diff / (365 * 60 * 60 * 24));
		$bulan = floor(($diff - $years * 365 * 60 * 60 * 24) / (30 * 60 * 60 * 24));
		if ((date('m') == date('m', strtotime($tanggalinv)) && (date('Y') == date('Y', strtotime($tanggalinv))))) {
			$selisihbulan = 0;
		} else if ((date('m') == date('m', strtotime($tanggalinv)) && (date('Y') > date('Y', strtotime($tanggalinv))))) {
			$selisihbulan = $bulan - 1 + ($years * 12);
		} else if ((date('m') != date('m', strtotime($tanggalinv)) && (date('Y') == date('Y', strtotime($tanggalinv))))) {
			$selisihbulan = $bulan + 1 + ($years * 12);
		} else {
			$selisihbulan = $bulan + ($years * 12);
		}

		$where = array(
			"noinvoice" => $noinvoice
		);
		$ceknosuratjalan = $this->global_model->GetDataGlobal("srvt_suratjalan", $where);
		if (empty($ceknosuratjalan)) {
			$resultjson = array(
				'nomor' => "",
				'message' => "Data gagal dibatalkan, Nomor Surat Jalan Tidak Ada."
			);
			$errorvalidasi = TRUE;
			echo json_encode($resultjson);
			return FALSE;
		} else {
			$nosuratjalan = $ceknosuratjalan[0]->nomor;
		}

		$where = array(
			"nofaktur" => $noinvoice,
			"batal" => false
		);

		$cekfakturpajak = $this->global_model->GetDataGlobal("srvt_fakturpajak", $where);
		if (!empty($cekfakturpajak)) {
			$resultjson = array(
				'nomor' => "",
				"message" => "Data gagal dibatalkan, Batalkan Faktur Pajak Terlebih Dahulu."
			);
			$errorvalidasi = TRUE;
			echo json_encode($resultjson);
			return FALSE;
		}

		if (empty($datadetail)) {
			$resultjson = array(
				'nomor' => "",
				'message' => "Data gagal dibatalkan, Data Detail Tidak Ada."
			);
			$errorvalidasi = TRUE;
			echo json_encode($resultjson);
			return FALSE;
		}
		$where = array(
			"nomor" => $noinvoice,
			"statusretur" => false,
		);
		$Statusretur = $this->global_model->GetDataGlobal("srvt_invoice", $where);
		if (empty($Statusretur)) {
			$resultjson = array(
				'nomor' => "",
				'message' => "Data gagal dibatalkan, Invoice dengan Nomor '<b style='color: red'>" . $noinvoice . "</b>' Sudah Dilakukan Retur Penjualan."
			);
			$errorvalidasi = TRUE;
			echo json_encode($resultjson);
			return FALSE;
		}
		$where = array(
			"nomor" => $noinvoice,
		);
		$datagudang = $this->global_model->GetDataGlobal("srvt_invoice", $where);
		if (empty($datagudang)) {
			$resultjson = array(
				'nomor' => "",
				'message' => "Data gagal dibatalkan, Kode Gudang Tidak Ada."
			);
			$errorvalidasi = TRUE;
			echo json_encode($resultjson);
			return FALSE;
		} else {
			$kodegudang = $datagudang[0]->lokasi;
		}

		$where = array(
			"nomor" => $noinvoice,
			"jenis" => "AR",
		);
		$cekpiutang = $this->global_model->GetDataGlobal("srvt_piutang", $where);
		foreach ($cekpiutang as $key => $val) {;
			if ($val->nilaipenerimaan <> 0) {
				$resultjson = array(
					'nomor' => "",
					'message' => "Data gagal dibatalkan, Invoice dengan Nomor '<b style='color: red'>" . $noinvoice . "</b>' Sudah Melakukan Nilai Penerimaan."
				);
				$errorvalidasi = TRUE;
				echo json_encode($resultjson);
				return FALSE;
			}
		}



		if ($errorvalidasi == FALSE) {
			$this->db->trans_start(); # Starting Transaction
			$this->db->trans_strict(FALSE);

			if (!empty($datadetail)) {
				foreach ($datadetail as $key => $value) {
					if ($selisihbulan > 0) {
						$saldoawal = 0;
						for ($i = 0; $i <= $selisihbulan; $i++) {
							$periodestock = date('Ym', strtotime('+' . $i . ' month', strtotime($tanggalinv)));
							$where = array(
								"periode" => $periodestock,
								"kodecabang" => $kodecabang,
								"kodegudang" => $kodegudang,
								"kodebarang" => $value['Kode']
							);
							$datastock = $this->global_model->GetDataGlobal("srvt_inventorystok", $where);
							if (!empty($datastock)) {
								foreach ($datastock as $key => $value2) {
									if ($saldoawal == 0) {
										$qtymasuk = ($value2->qtymasuk + floatval(str_replace(",", "", $value['Qty'])));
										$qtypick = ($value2->qtypick + floatval(str_replace(",", "", $value['Qty'])));
										$setdata = "qtymasuk = '" . $qtymasuk . "',qtypick = '" . $qtypick . "'";
										$wheredata = "periode = '" . $periodestock . "' AND kodecabang = '" . $kodecabang . "' AND kodegudang = '" . $kodegudang . "' AND kodebarang = '" . $value['Kode'] . "'";
										$this->Invoice_model->UpdateDataInventory("srvt_inventorystok", $setdata, $wheredata);
									} else {
										$qtymasuk = ($value2->qtymasuk + floatval(str_replace(",", "", $value['Qty'])));
										$qtypick = ($value2->qtypick + floatval(str_replace(",", "", $value['Qty'])));
										$setdata = "qtyawal = '" . $saldoawal . "',qtypick = '" . $qtypick . "'";
										$wheredata = "periode = '" . $periodestock . "' AND kodecabang = '" . $kodecabang . "' AND kodegudang = '" . $kodegudang . "' AND kodebarang = '" . $value['Kode'] . "'";
										$this->Invoice_model->UpdateDataInventory("srvt_inventorystok", $setdata, $wheredata);
									}
									$ceksaldoawal = $this->global_model->GetDataGlobal("srvt_inventorystok", $where);
									if (!empty($ceksaldoawal)) {
										foreach ($ceksaldoawal as $key => $valuesaldoawal) {
											$saldoawal = $valuesaldoawal->qtyawal + $valuesaldoawal->qtymasuk - $valuesaldoawal->qtykeluar;	# code...
										}
									}
								}
							} else {
								if ($saldoawal == 0) {
									$datainsert = array(
										"periode" => $periodestock,
										"kodecabang" => $kodecabang,
										"kodegudang" => $kodegudang,
										"kodebarang" => $value['Kode'],
										"qtyawal" => $saldoawal,
										"qtymasuk" =>  floatval(str_replace(",", "", $value['Qty'])),
										"qtykeluar" => 0,
										"qtypick" => floatval(str_replace(",", "", $value['Qty'])),
										"cogs" => 0,
										'pemakai' => $pemakai,
										'tglsimpan' => date('Y-m-d H:i:s'),
									);
									$datastock = $this->global_model->InsertDataGlobal("srvt_inventorystok", $datainsert);
								} else {
									$datainsert = array(
										"periode" => $periodestock,
										"kodecabang" => $kodecabang,
										"kodegudang" => $kodegudang,
										"kodebarang" => $value['Kode'],
										"qtyawal" => $saldoawal,
										"qtymasuk" =>  0,
										"qtykeluar" => 0,
										"qtypick" => floatval(str_replace(",", "", $value['Qty'])),
										"cogs" => 0,
										'pemakai' => $pemakai,
										'tglsimpan' => date('Y-m-d H:i:s'),
									);
									$datastock = $this->global_model->InsertDataGlobal("srvt_inventorystok", $datainsert);
								}
								$ceksaldoawal = $this->global_model->GetDataGlobal("srvt_inventorystok", $where);
								if (!empty($ceksaldoawal)) {
									foreach ($ceksaldoawal as $key => $valuesaldoawal) {
										$saldoawal = $valuesaldoawal->qtyawal + $valuesaldoawal->qtymasuk - $valuesaldoawal->qtykeluar;	# code...
									}
								}
							}
						}
					} else {
						$where = array(
							"periode" => $periode,
							"kodecabang" => $kodecabang,
							"kodegudang" => $kodegudang,
							"kodebarang" => $value['Kode']
						);
						$datastock = $this->global_model->GetDataGlobal("srvt_inventorystok", $where);
						if (!empty($datastock)) {
							foreach ($datastock as $key => $value2) {
								$qtymasuk = ($value2->qtymasuk + floatval(str_replace(",", "", $value['Qty'])));
								$qtypick = ($value2->qtypick + floatval(str_replace(",", "", $value['Qty'])));
								$setdata = "qtymasuk = '" . $qtymasuk . "',qtypick = '" . $qtypick . "'";
								$wheredata = "periode = '" . $periode . "' AND kodecabang = '" . $kodecabang . "' AND kodegudang = '" . $kodegudang . "' AND kodebarang = '" . $value['Kode'] . "'";
								$this->Invoice_model->UpdateDataInventory("srvt_inventorystok", $setdata, $wheredata);
							}
						}
					}
				}
			}

			$maxrevisi = $this->Invoice_model->MaxRevisiKe($noinvoice);
			if (empty($maxrevisi)) {
				$revisike = 1;
			} else {
				$revisike = $maxrevisi[0]->revisike + 1;
			}

			$dataheader = array(
				'noinvoice' => $noinvoice,
				'nosuratjalan' => $nosuratjalan,
				'noinvoicebatal' => 'X' . $revisike . '-' . $noinvoice,
				'nosuratjalanbatal' => 'X' . $revisike . '-' . $nosuratjalan,
				'tanggal' => $tanggalinv,
				'keteranganbatal' => $keterangan,
				'jenisdokumen' => "Invoice Penjualan",
				'statuspakai' => false,
				'tglsimpan' => date('Y-m-d H:i:s'),
				'pemakai' => $pemakai,
				'kodecabang' => $kodecabang,
				'revisike' => $revisike,
				'noglobal' => $nomorgb,
				'noglobalbatal' => 'X' . $revisike . '-' . $nomorgb
			);
			$this->global_model->InsertDataGlobal("hist_cancel", $dataheader);

			$dataheader = array(
				'nomor' => 'X' . $revisike . '-' . $noinvoice,
				'nomorgb' => 'X'.$revisike.'-'.$nomorgb,
				'batal' => true,
				'userbatal' => $pemakai,
				'alasanbatal' => $keterangan,
				'tglbatal' => date('Y-m-d H:i:s')
			);
			$wheredata = array("nomor" => $noinvoice);
			$this->global_model->UpdateDataGlobal("srvt_invoice", $dataheader, $wheredata);

			$dataheader = array(
				'nomor' => 'X' . $revisike . '-' . $noinvoice,
			);
			$wheredata = array("nomor" => $noinvoice);
			$this->global_model->UpdateDataGlobal("srvt_invoicedetail", $dataheader, $wheredata);

			$dataheader = array(
				'nomor' => 'X' . $revisike . '-' . $nosuratjalan,
				'noinvoice' => 'X' . $revisike . '-' . $noinvoice,
			);
			$wheredata = array(
				"nomor" => $nosuratjalan,
				"noinvoice" => $noinvoice
			);
			$this->global_model->UpdateDataGlobal("srvt_suratjalan", $dataheader, $wheredata);

			$dataheader = array(
				'nomor' => 'X' . $revisike . '-' . $nosuratjalan,
			);
			$wheredata = array("nomor" => $nosuratjalan);
			$this->global_model->UpdateDataGlobal("srvt_suratjalandetail", $dataheader, $wheredata);

			$dataheader = array(
				'nomor' => 'X' . $revisike . '-' . $noinvoice,
				'batal' => true,
				'userbatal' => $pemakai,
				'tglbatal' => date('Y-m-d H:i:s')
			);
			$wheredata = array(
				"nomor" => $noinvoice,
				"jenis" => "AR",
			);
			$this->global_model->UpdateDataGlobal("srvt_piutang", $dataheader, $wheredata);

			$updateSO = [
				'kodestatus' => 0,
				'gudang' => false
			];
			$this->Invoice_model->UpdateStatusSO($nomorso, $updateSO);

			$updatePacking = [
				'status' => false,
				'batal' => true,
				'userbatal' => $pemakai,
				'alasanbatal' => $keterangan,
				'tglbatal' => date('Y-m-d H:i:s')
			];
			$this->Invoice_model->UpdateStatusPacking($nopacking, $updatePacking);


			// die();

			$this->db->trans_complete();

			if ($this->db->trans_status() === FALSE) {
				$resultjson = array(
					'nomor' => "",
					'message' => "Data gagal dibatalkan, Nomor '<b style='color: red'>" . $noinvoice . "</b>' tidak ada."
				);
				# Something went wrong.
				$this->db->trans_rollback();
			} else {
				$resultjson = array(
					'nomor' => $noinvoice,
					'message' => "Data berhasil dibatalkan."
				);
				# Everything is Perfect. 
				# Committing data to the database.
				$this->db->trans_commit();
			}

			echo json_encode($resultjson);
			return FALSE;
		}
	}

	function LoadKonfigurasi()
	{
		$result = $this->global_model->GetDataGlobal("stpm_konfigurasi", array());
		echo json_encode($result);
	}

	function LoadGudang()
	{
		$result = $this->global_model->GetDataGlobal("glbm_gudang", array("kodecabang" => $this->input->post('cabang')));
		echo json_encode($result);
	}
	function LoadRacikan()
	{
		$result = $this->global_model->GetDataGlobal("glbm_promo", array("kodecabang" => $this->input->post('cabang')));
		echo json_encode($result);
	}
}
