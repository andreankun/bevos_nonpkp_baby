<?php

defined('BASEPATH') or exit('No direct script access allowed');

class PencatatanOngkir extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->load->model('sales/PencatatanOngkir_model');
        $this->load->model('global/global_model');
        $this->load->model('Caridataaktif_model');
        $this->load->library('form_validation');
        $this->load->library('session');
    }

    function CariDataPencatatanOngkir()
    {
        $fetch_data = $this->Caridataaktif_model->make_datatables($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value'));
        $data = array();
        foreach ($fetch_data as $row) {
            $sub_array = array();
            $i = 1;
            $count = count($this->input->post('field'));
            foreach ($this->input->post('field') as $key => $value) {
                if ($i <= $count) {
                    if ($i == 1) {
                        $msearch = $row->$value;
                        $sub_array[] = '<button class="btn btn-dark searchongkir" data-dismiss="modal" style="margin: 0px;" data-id="' . $msearch . '"><i class="far fa-check-circle"></i></button>';
                        $sub_array[] = $row->$value;
                    } else {
                        if ($i == $count) {
                            $sub_array[] = $row->$value;
                        } else {
                            $sub_array[] = $row->$value;
                        }
                    }
                    // $sub_array[] = $row->$value;
                }
                $i++;
            }
            $data[] = $sub_array;
        }
        $output = array(
            "draw"                    =>     intval($_POST["draw"]),
            "recordsTotal"          =>      $this->Caridataaktif_model->get_all_data($this->input->post('nmtb')),
            "recordsFiltered"     =>     $this->Caridataaktif_model->get_filtered_data($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value')),
            "data"                    =>     $data
        );
        echo json_encode($output);
    }

	function CariCustomer()
    {
        $fetch_data = $this->Caridataaktif_model->make_datatables($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value'));
        $data = array();
        foreach ($fetch_data as $row) {
            $sub_array = array();
            $i = 1;
            $count = count($this->input->post('field'));
            foreach ($this->input->post('field') as $key => $value) {
                if ($i <= $count) {
                    if ($i == 1) {
                        $msearch = $row->$value;
                        $sub_array[] = '<button class="btn btn-dark searchcustomer" data-dismiss="modal" style="margin: 0px;" data-id="' . $msearch . '"><i class="far fa-check-circle"></i></button>';
                        $sub_array[] = $row->$value;
                    } else {
                        if ($i == $count) {
                            $sub_array[] = $row->$value;
                        } else {
                            $sub_array[] = $row->$value;
                        }
                    }
                    // $sub_array[] = $row->$value;
                }
                $i++;
            }
            $data[] = $sub_array;
        }
        $output = array(
            "draw"                    =>     intval($_POST["draw"]),
            "recordsTotal"          =>      $this->Caridataaktif_model->get_all_data($this->input->post('nmtb')),
            "recordsFiltered"     =>     $this->Caridataaktif_model->get_filtered_data($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value')),
            "data"                    =>     $data
        );
        echo json_encode($output);
    }

    function DataPencatatanOngkir()
    {
        $result = $this->PencatatanOngkir_model->DataPencatatanOngkir($this->input->post('nomor'));
        echo json_encode($result);
    }

	function DataCustomer()
    {
        $result = $this->PencatatanOngkir_model->DataCustomer($this->input->post('nomor'));
        echo json_encode($result);
		// print_r($result);
		// die();
    }

    public function ClearChr($param)
    {
        return $result = str_replace(array('\'', '"', ',', ';', '<', '>', '_', '-', ' ', '.', '!', '?', '/', '=', '+', ']', '[', '{', '}', '@', '#', '$', '%', '^', '&', "*", '(', ')'), '', $param);
    }

    function Save()
    {
        $nomorongkir = $this->input->post('nomor');

        if (empty($nomor)) {
            $this->db->trans_start(); # Starting Transaction
            $this->db->trans_strict(FALSE);

            $kodeprefix = $this->input->post('kodeprefix');
			// print_r($kodeprefix);
			// die();
			$pajangkode = strlen($kodeprefix);
			// print_r($pajangkode);
			// die();
            $ambilnomor = $kodeprefix . "-" . date("Y") . "-" . date("m");
			$ambilnomorSJ = "SJ" . "-" . date("Y") . "-" . date("m");
			$get["invpkp"] = $this->PencatatanOngkir_model->GetMaxNomor($kodeprefix);
			// print_r($get["invpkp"]);
			// die();
				if (!$get["invpkp"]) {
					$nomorongkir = $ambilnomor . "-" . "00001";
					// $nomorsj = $ambilnomorSJ . "-" . "00001";
					// $nomorgb = $ambilnomorGB . "-" . "00001";
				} else {
					$lastNomor = $get['invpkp']->nomor;
					if (strlen($kodeprefix) == 3) {
						$lastNoUrut = substr($lastNomor, 13, 11);
					} else if (strlen($kodeprefix) == 2) {
						$lastNoUrut = substr($lastNomor, 11, 11);
					}

					// nomor urut ditambah 1
					$nextNoUrut = $lastNoUrut + 1;
					// $nomor = $ambilnomor . "-" . sprintf('%05s', $nextNoUrut);
					$nomorongkir = $ambilnomor . "-" . sprintf('%05s', $nextNoUrut);
					// $nomorsj = $ambilnomorSJ . "-" . sprintf('%05s', $nextNoUrut);
					// $nomorgb = $ambilnomorGB . "-" . sprintf('%05s', $nextNoUrut);
					// print_r($nomorinvoice . "<br>");
					// print_r($nomorsj);
				}

			$dataar = [
				'nomor' => $nomorongkir,
				'jenis' => 'AR',
				'tanggal' => $this->input->post('tanggal'),
				'tgljthtempo' => $this->input->post('tgljthtempo'),
				'nopelanggan' => $this->input->post('nopelanggan'),
				'nilaipiutang' => floatval(str_replace(",", "", $this->input->post('nilaiongkir'))),
				'nilaialokasi' => 0,
				'nilaipenerimaan' => 0,
				'pemakai' => $this->session->userdata('myusername'),
				'tglsimpan' => date('Y-m-d H:i:s'),
				'kodecabang' => $this->input->post('kodecabang')
			];
			// print_r($dataar);
            // die();
			$this->PencatatanOngkir_model->SaveDataAR($dataar);

            $data = array(
                'nomor' => $nomorongkir,
                'tanggal' => $this->input->post('tanggal'),
                // 'catatan' => $this->input->post('catatan'),
                // 'nilainominal' => $this->ClearChr($this->input->post('nilainominal')),
                'keterangan' => $this->input->post('keterangan'),
                'tanggalongkir' => $this->input->post('tanggalongkir'),
                'nilaiongkir' => $this->ClearChr($this->input->post('nilaiongkir')),
                'tglsimpan' => date('Y-m-d H:i:s'),
                'pemakai' => $this->session->userdata('myusername'),
				'kodecabang' => $this->input->post('kodecabang'),
				'nopelanggan' => $this->input->post('nopelanggan'),
				'namapelanggan' => $this->input->post('namapelanggan')
            );
            // print_r($data);
            // die();
            $this->PencatatanOngkir_model->SaveData($data);

            // $dataupdate = [
            //     'status' => true,
            //     'sumberfaktur' => $nomor,
            //     'nofaktur' => $this->input->post('noinvoice'),
            //     'nopelanggan' => $this->input->post('nopelanggan')
            // ];
            // $this->FakturPajak_model->UpdateDataFakturPajak($this->input->post('nofakturpajak'), $dataupdate);

            // $dataupdateinv = [
            //     'nofakturpajak' => $this->input->post('nofakturpajak')
            // ];
            // $this->FakturPajak_model->UpdateDataFaktur($this->input->post('noinvoice'), $dataupdateinv);

            // die();
            $this->db->trans_complete();

            if ($this->db->trans_status() === FALSE) {
                $resultjson = array(
                    'nomor' => "",
                    'message' => "Data gagal disimpan, Nomor '<b style='color: red'>" . $nomorongkir . "</b>' sudah pernah digunakan."
                );
                # Something went wrong.
                $this->db->trans_rollback();
            } else {
                $resultjson = array(
                    'nomor' => $nomorongkir,
                    // 'nomorsj' => $nomorsj,
                    'message' => "Data berhasil disimpan."
                );
                # Everything is Perfect. 
                # Committing data to the database.
                $this->db->trans_commit();
            }
        } else {
            $this->db->trans_complete();
            $resultjson = array(
                'nomor' => "",
                'message' => "Data gagal disimpan, Nomor '<b style='color: red'>" . $nomorongkir . "</b>' sudah pernah digunakan."
            );
            # Something went wrong.
            $this->db->trans_rollback();
        }
        echo json_encode($resultjson);
    }


    function Cancel()
    {
        $dataheader = array(
            'batal' => true,
            'userbatal' => $this->session->userdata('myusername'),
            'keteranganbatal' => $this->input->post('keterangan'),
            'tglbatal' => date('Y-m-d H:i:s')
        );
        $wheredata = array("nomor" => $this->input->post('nomor'));
        $this->global_model->UpdateDataGlobal("srvt_ongkir", $dataheader, $wheredata);
		$dataheaderp = array(
            'batal' => true,
            'userbatal' => $this->session->userdata('myusername'),
            // 'keteranganbatal' => $this->input->post('keterangan'),
            'tglbatal' => date('Y-m-d H:i:s')
        );
		$wheredatap = array("nomor" => $this->input->post('nomor'));
        $this->global_model->UpdateDataGlobal("srvt_piutang", $dataheaderp, $wheredatap);
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE) {
            $resultjson = array(
                'nomor' => "",
                'message' => "Data gagal dibatalkan, Nomor '<b style='color: red'>" . $this->input->post('nomor') . "</b>' tidak ada."
            );
            # Something went wrong.
            $this->db->trans_rollback();
        } else {
            $resultjson = array(
                'nomor' => $this->input->post('nomor'),
                'message' => "Data berhasil dibatalkan."
            );
            # Everything is Perfect. 
            # Committing data to the database.
            $this->db->trans_commit();
        }

        echo json_encode($resultjson);
    }

	function Update(){
		$nomor = $this->input->post('nomor');

		$this->db->trans_start(); # Starting Transaction
		$this->db->trans_strict(FALSE);

		// $nomor = $this->input->post('nomor');

		// $ambilnomor = "PO";
		// $get["po"] = $this->PencatatanOngkir_model->GetMaxNomor($ambilnomor);
		// if (!$get["po"]) {
		// 	$nomor = "000000001";
		// } else {
		// 	$lastNomor = $get['po']->nomor;
		// 	$lastNoUrut = substr($lastNomor, 2, 10);

		// 	// nomor urut ditambah 1
		// 	$nextNoUrut = $lastNoUrut + 1;
		// 	$nomor = $ambilnomor . sprintf('%09s', $nextNoUrut);
		// }

		$dataar = [
			// 'nomor' => $nomor,
			// 'jenis' => 'PS',
			// 'tanggal' => $this->input->post('tanggal'),
			// 'tgljthtempo' => $this->input->post('tanggal'),
			// 'nopelanggan' => $this->input->post('nopelanggan'),
			'nilaipiutang' => floatval(str_replace(",", "", $this->input->post('nilaiongkir'))),
			// 'nilaialokasi' => 0,
			// 'nilaipenerimaan' => 0,
			// 'pemakai' => $this->session->userdata('myusername'),
			// 'tglsimpan' => date('Y-m-d H:i:s'),
			// 'kodecabang' => $this->input->post('kodecabang')
		];
		// print_r($dataar);
		// die();
		$this->PencatatanOngkir_model->UpdatePiutang($nomor, $dataar);

		$data = array(
			// 'nomor' => $nomor,
			// 'tanggal' => $this->input->post('tanggal'),
			'catatan' => $this->input->post('catatan'),
			// 'nilainominal' => $this->ClearChr($this->input->post('nilainominal')),
			'keterangan' => $this->input->post('keterangan'),
			'tanggalongkir' => $this->input->post('tanggalongkir'),
			'nilaiongkir' => $this->ClearChr($this->input->post('nilaiongkir')),
			'tglsimpan' => date('Y-m-d H:i:s'),
			'pemakai' => $this->session->userdata('myusername'),
			// 'kodecabang' => $this->input->post('kodecabang'),
			// 'nopelanggan' => $this->input->post('nopelanggan'),
			// 'namapelanggan' => $this->input->post('namapelanggan')
		);
		// print_r($data);
		// die();
		$this->PencatatanOngkir_model->UpdateData($nomor, $data);

		// $dataupdate = [
		//     'status' => true,
		//     'sumberfaktur' => $nomor,
		//     'nofaktur' => $this->input->post('noinvoice'),
		//     'nopelanggan' => $this->input->post('nopelanggan')
		// ];
		// $this->FakturPajak_model->UpdateDataFakturPajak($this->input->post('nofakturpajak'), $dataupdate);

		// $dataupdateinv = [
		//     'nofakturpajak' => $this->input->post('nofakturpajak')
		// ];
		// $this->FakturPajak_model->UpdateDataFaktur($this->input->post('noinvoice'), $dataupdateinv);

		// die();
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE) {
			$resultjson = array(
				'nomor' => "",
				'message' => "Data gagal disimpan, Nomor '<b style='color: red'>" . $nomor . "</b>' sudah pernah digunakan."
			);
			# Something went wrong.
			$this->db->trans_rollback();
		} else {
			$resultjson = array(
				'nomor' => $nomor,
				'message' => "Data berhasil disimpan."
			);
			# Everything is Perfect. 
			# Committing data to the database.
			$this->db->trans_commit();
		}
        echo json_encode($resultjson);
	}

	
}
