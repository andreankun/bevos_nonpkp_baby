<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Login extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->load->model("login/Login_model");
        $this->load->model('global/global_model');
        $this->load->library('form_validation');
        $this->load->library('session');
    }

    function saldoawal()
    {

        $periode = date("Y") . date("m");
        $cekdata = $this->global_model->GetDataGlobal('srvt_inventorystok', array('periode' => $periode));
        $periodelama = date("Ym", strtotime('-1 month'));
        if (empty($cekdata)) {
            $datastockbulanlalu = $this->global_model->GetDataGlobal('srvt_inventorystok', array('periode' => $periodelama));
            if (!empty($datastockbulanlalu)) {
                foreach ($datastockbulanlalu as $key => $value) {
                    # code...
                    $this->global_model->InsertStockBarangPeriodeBaru($periodelama, $periode, $value->kodebarang, $value->kodecabang, $value->kodegudang);
                }
            }
        } else {
            foreach ($cekdata as $key => $value) {
                $datastockbulanlalu = $this->global_model->GetDataGlobal('srvt_inventorystok', array('periode' => $periodelama, 'kodecabang' => $value->kodecabang, 'kodegudang' => $value->kodegudang, 'kodebarang' => $value->kodebarang));
                if (empty($datastockbulanlalu)) {
                    foreach ($datastockbulanlalu as $key => $values) {
                        # code...
                        $this->global_model->InsertStockBarangPeriodeBaru($periodelama, $periode, $values->kodebarang, $values->kodecabang, $values->kodegudang);
                    }
                }
            }
        }
        return false;
    }

    public function index()
    {
        $this->load->view('menu/login/login');
    }

    public function cek_login()
    {
        $this->saldoawal();
        $username = $this->input->post('username');
        $password = base64_encode($this->input->post('password'));

        $get["login"] =  $this->Login_model->getlog($username, $password);

        if (!empty($get["login"]->username)) {
            if ($get["login"]->grup == 'IT SUPPORT' || $get["login"]->grup == 'HO2' || $get["login"]->grup == 'KEP. LK' || $get["login"]->grup == 'AUDIT LK' || $get["login"]->grup == 'SUPERADMIN' || $get["login"]->grup == 'PAJAK') {
            // if ($get["login"]->grup == 'HO' || $get["login"]->grup == 'SUPERADMIN') {
                $this->session->set_userdata('myusername', $username);
                $this->session->set_userdata('mygrup', $get["login"]->grup);
                $this->session->set_userdata('myho', $get["login"]->statusho);
                redirect('main/ho');
            } else {
                $this->session->set_userdata('myusername', $username);
                $this->session->set_userdata('mygrup', $get["login"]->grup);
                $this->session->set_userdata('mycabang', $get["login"]->kodecabang);
                $this->session->set_userdata('mygudang', $get["login"]->kodegudang);
                $this->session->set_userdata('myho', $get["login"]->statusho);

                redirect('main');
            }
        } else {
            $this->session->set_flashdata('pesan', '<center><div class="alert" style="color:salmon; font-size: 16pt;"><i class="fa fa-warning"></i><p>Username dan atau Password salah.</p></div></center>');
            $this->session->unset_userdata('myusername');
            redirect('main/index');
        }
        return false;
    }

    function set_cabang()
    {
        if (!empty($this->input->post('cabang'))) {
            $this->session->set_userdata('mycabang', $this->input->post('cabang'));
            redirect('main');
        } else {
            $this->session->set_flashdata('pesan', '<center><div class="alert" style="color:salmon; font-size: 16pt;"><i class="fa fa-warning"></i><p>Pilih cabang terlebih dahulu.</p></div></center>');
            $this->session->unset_userdata('myusername');
            redirect('main/ho');
        }
        return false;
    }

    function logout()
    {
        $this->session->unset_userdata('myusername');
        //$this->session->sess_destroy();
        redirect('main/index');
    }
}
