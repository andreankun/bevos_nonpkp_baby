<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Main extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('global/global_model');
		$this->load->library('form_validation');
		$this->load->library('session');
	}

	function saldoawal()
	{

		$periode = date("Y") . date("m");
		$cekdata = $this->global_model->GetDataGlobal('srvt_inventorystok', array('periode' => $periode));
		$periodelama = date("Ym", strtotime('-1 month'));
		if (empty($cekdata)) {
			$datastockbulanlalu = $this->global_model->GetDataGlobal('srvt_inventorystok', array('periode' => $periodelama));
			if (!empty($datastockbulanlalu)) {
				foreach ($datastockbulanlalu as $key => $value) {
					# code...
					$this->global_model->InsertStockBarangPeriodeBaru($periodelama, $periode, $value->kodebarang, $value->kodecabang, $value->kodegudang);
				}
			}
		} else {
			foreach ($cekdata as $key => $value) {
				$datastockbulanlalu = $this->global_model->GetDataGlobal('srvt_inventorystok', array('periode' => $periodelama, 'kodecabang' => $value->kodecabang, 'kodegudang' => $value->kodegudang, 'kodebarang' => $value->kodebarang));
				if (empty($datastockbulanlalu)) {
					foreach ($datastockbulanlalu as $key => $values) {
						# code...
						$this->global_model->InsertStockBarangPeriodeBaru($periodelama, $periode, $values->kodebarang, $values->kodecabang, $values->kodegudang);
					}
				}
			}
		}
		return false;
	}

	public function index()
	{
		$this->saldoawal();
		if (true == $this->session->has_userdata('myusername')) {
			redirect('main/dashboard');
		} else {
			$this->session->unset_userdata('myusername');
			$this->load->view('menu/login/login');
		}
		return false;

		// $data['title'] = "Dashboard";
		// $data['icons'] = "<i class='fal fa-chart-pie fa-lg' style='font-size: 22px;'></i>";
		// $data['script'] = "menu/dashboard/js";
		// $data['css'] = "menu/dashboard/css";
		// $this->template->view('menu/dashboard/index_dashboard', $data);
	}

	public function ho()
	{
		$this->load->view('menu/login/ho');
	}

	public function dashboard()
	{
		if (true == $this->session->has_userdata('myusername')) {
			$data['title'] = "Dashboard";
			$data['icons'] = "<i class='fal fa-chart-pie fa-lg' style='font-size: 22px;'></i>";
			$data['script'] = "menu/dashboard/js";
			$data['css'] = "menu/dashboard/css";
			$this->template->view('menu/dashboard/index_dashboard', $data);
		} else {
			$this->session->unset_userdata('myusername');
			$this->load->view('menu/login/login');
		}
		return false;
	}

	// REPORT

	public function laporan_penjualan()
	{
		$data['title'] = "Laporan Penjualan";
		$data['icons'] = "<i class='links_icon fal fa-file-invoice' style='font-size: 22px;'></i>";
		$data['script'] = "menu/reporting/penjualan/js";
		$this->template->view('menu/reporting/penjualan/index_laporan_penjualan', $data);
	}

	public function laporan_persediaan()
	{
		$data['title'] = "Laporan Persediaan";
		$data['icons'] = "<i class='links_icon fal fa-file-invoice' style='font-size: 22px;'></i>";
		$data['script'] = "menu/reporting/persedian/js";
		$this->template->view('menu/reporting/persedian/index_persediaan', $data);
	}

	public function laporan_pembelian()
	{
		$data['title'] = "Laporan Pembelian";
		$data['icons'] = "<i class='links_icon fal fa-file-invoice' style='font-size: 22px;'></i>";
		$data['script'] = "menu/reporting/pembelian/js";
		$this->template->view('menu/reporting/pembelian/index_pembelian', $data);
	}

	public function laporan_keuangan()
	{
		$data['title'] = "Laporan Keuangan";
		$data['icons'] = "<i class='links_icon fal fa-file-invoice' style='font-size: 22px;'></i>";
		$data['script'] = "menu/reporting/keuangan/js";
		$this->template->view('menu/reporting/keuangan/index_laporan_keuangan', $data);
	}

	public function laporan_pajak()
	{
		$data['title'] = "Laporan Pajak";
		$data['icons'] = "<i class='links_icon fal fa-file-invoice' style='font-size: 22px;'></i>";
		$data['script'] = "menu/reporting/pajak/js";
		$this->template->view('menu/reporting/pajak/index_laporan_pajak', $data);
	}
	// End of REPORT

	// SALES
	public function sales_order()
	{
		if (true == $this->session->has_userdata('myusername')) {
			$data['title'] = "Sales Order";
			$data['icons'] = "<i class='links_icon far fa-file-invoice' style='font-size: 22px;'></i>";
			$data['script'] = "menu/sales/sales_order/js";
			$this->template->view('menu/sales/sales_order/index_sales_order', $data);
		} else {
			$this->session->unset_userdata('myusername');
			$this->load->view('menu/login/login');
		}
		return false;
	}

	public function ongkir()
	{
		if (true == $this->session->has_userdata('myusername')) {
			$data['title'] = "Pencatatan Ongkir";
			$data['icons'] = "<i class='links_icon far fa-file-invoice' style='font-size: 22px;'></i>";
			$data['script'] = "menu/sales/pencatatan_ongkir/js";
			$this->template->view('menu/sales/pencatatan_ongkir/index_ongkir', $data);
		} else {
			$this->session->unset_userdata('myusername');
			$this->load->view('menu/login/login');
		}
		return false;
	}

	public function invoice()
	{
		if (true == $this->session->has_userdata('myusername')) {
			$data['title'] = "Invoice";
			$data['icons'] = "<i class='links_icon fa fa-print' style='font-size: 22px;'></i>";
			$data['script'] = "menu/sales/invoice/js";
			$this->template->view('menu/sales/invoice/index_invoice', $data);
		} else {
			$this->session->unset_userdata('myusername');
			$this->load->view('menu/login/login');
		}
		return false;
	}

	public function reprint()
	{
		if (true == $this->session->has_userdata('myusername')) {
			$data['title'] = "Re-Print";
			$data['icons'] = "<i class='links_icon fas fa-print' style='font-size: 22px;'></i>";
			$data['script'] = "menu/sales/reprint/js";
			$this->template->view('menu/sales/reprint/index_reprint', $data);
		} else {
			$this->session->unset_userdata('myusername');
			$this->load->view('menu/login/login');
		}
		return false;
	}

	public function acc_reprint()
	{
		if (true == $this->session->has_userdata('myusername')) {
			$data['title'] = "Accept Re-print";
			$data['icons'] = "<i class='links_icon fas fa-print' style='font-size: 22px;'></i>";
			$data['script'] = "menu/sales/acc_reprint/js";
			$this->template->view('menu/sales/acc_reprint/index_acc_reprint', $data);
		} else {
			$this->session->unset_userdata('myusername');
			$this->load->view('menu/login/login');
		}
	}

	public function request_reprint()
	{
		if (true == $this->session->has_userdata('myusername')) {
			$data['title'] = "Request Re-print";
			$data['icons'] = "<i class='links_icon fas fa-print' style='font-size: 22px;'></i>";
			$data['script'] = "menu/sales/req_reprint/js";
			$this->template->view('menu/sales/req_reprint/index_req_reprint', $data);
		} else {
			$this->session->unset_userdata('myusername');
			$this->load->view('menu/login/login');
		}
	}

	public function cetak_faktur_surat_jalan()
	{
		if (true == $this->session->has_userdata('myusername')) {
			$data['title'] = "Faktur &  Surat Jalan (Admin Only)";
			$data['icons'] = "<i class='links_icon fas fa-print' style='font-size: 22px;'></i>";
			$data['script'] = "menu/sales/cetak_faktur_surat_jalan/js";
			$this->template->view('menu/sales/cetak_faktur_surat_jalan/index_cetak_faktur_surat_jalan', $data);
		} else {
			$this->session->unset_userdata('myusername');
			$this->load->view('menu/login/login');
		}
	}

	public function faktur_pajak()
	{
		if (true == $this->session->has_userdata('myusername')) {
			$data['title'] = "Faktur Pajak";
			$data['icons'] = "<i class='links_icon far fa-receipt' style='font-size: 22px;'></i>";
			$data['script'] = "menu/sales/faktur_pajak/js";
			$this->template->view('menu/sales/faktur_pajak/index_faktur_pajak', $data);
		} else {
			$this->session->unset_userdata('myusername');
			$this->load->view('menu/login/login');
		}
	}
	// End of SALES

	// FINANCE

	public function kas_bank()
	{
		if (true == $this->session->has_userdata('myusername')) {
			$data['title'] = "Transaksi Kas/Bank";
			$data['icons'] = "<i class='links_icon fas fa-shopping-cart' style='font-size: 22px;'></i>";
			$data['script'] = "menu/finance/kas_bank/js";
			$this->template->view('menu/finance/kas_bank/index_kas_bank', $data);
		} else {
			$this->session->unset_userdata('myusername');
			$this->load->view('menu/login/login');
		}
	}

	public function pengeluaran_uang()
	{
		if (true == $this->session->has_userdata('myusername')) {
			$data['title'] = "Pengeluaran Uang";
			$data['icons'] = "<i class='links_icon fal fa-file-export' style='font-size: 22px;'></i>";
			$data['script'] = "menu/finance/pengeluaran_uang/js";
			$this->template->view('menu/finance/pengeluaran_uang/index_pengeluaran_uang', $data);
		} else {
			$this->session->unset_userdata('myusername');
			$this->load->view('menu/login/login');
		}
	}

	public function permohonan_uang()
	{
		if (true == $this->session->has_userdata('myusername')) {
			$data['title'] = "Permohonan Uang";
			$data['icons'] = "<i class='links_icon fal fa-file-invoice-dollar' style='font-size: 22px;'></i>";
			$data['script'] = "menu/finance/permohonan_uang/js";
			$this->template->view('menu/finance/permohonan_uang/index_permohonan_uang', $data);
		} else {
			$this->session->unset_userdata('myusername');
			$this->load->view('menu/login/login');
		}
	}

	public function penerimaan_uang()
	{
		if (true == $this->session->has_userdata('myusername')) {
			$data['title'] = "Penerimaan Uang";
			$data['icons'] = "<i class='links_icon fal fa-donate' style='font-size: 22px;'></i>";
			$data['script'] = "menu/finance/penerimaan_uang/js";
			$this->template->view('menu/finance/penerimaan_uang/index_penerimaan_uang', $data);
		} else {
			$this->session->unset_userdata('myusername');
			$this->load->view('menu/login/login');
		}
	}

	public function daftar_piutang()
	{
		if (true == $this->session->has_userdata('myusername')) {
			$data['title'] = "Daftar Piutang Customer";
			$data['icons'] = "<i class='links_icon fal fa-donate' style='font-size: 22px;'></i>";
			$data['script'] = "menu/finance/daftar_piutang/js";
			$this->template->view('menu/finance/daftar_piutang/index_piutang', $data);
		} else {
			$this->session->unset_userdata('myusername');
			$this->load->view('menu/login/login');
		}
	}

	// End of FINANCE

	// INVENTORY

	public function packing_barang()
	{
		if (true == $this->session->has_userdata('myusername')) {
			$data['title'] = "Packing Barang";
			$data['icons'] = "<i class='links_icon fal fa-boxes-alt' style='font-size: 22px;'></i>";
			$data['script'] = "menu/inventory/packing/js";
			$this->template->view('menu/inventory/packing/index_packing', $data);
		} else {
			$this->session->unset_userdata('myusername');
			$this->load->view('menu/login/login');
		}
	}

	// public function titipan_pelanggan()
	// {
	// 	$data['title'] = "Titipan Pelanggan";
	// 	$data['icons'] = "<i class='links_icon fal fa-boxes-alt' style='font-size: 22px;'></i>";
	// 	$data['script'] = "menu/inventory/titipan_pelanggan/js";
	// 	$this->template->view('menu/inventory/titipan_pelanggan/index_titipan_pelanggan', $data);
	// }

	public function inventory_stock()
	{
		if (true == $this->session->has_userdata('myusername')) {
			$data['title'] = "Inventory Stock";
			$data['icons'] = "<i class='links_icon fal fa-boxes-alt' style='font-size: 22px;'></i>";
			$data['script'] = "menu/inventory/inventory_stock/js";
			$this->template->view('menu/inventory/inventory_stock/index_inventory_stock', $data);
		} else {
			$this->session->unset_userdata('myusername');
			$this->load->view('menu/login/login');
		}
	}

	public function stock_opname()
	{
		if (true == $this->session->has_userdata('myusername')) {
			$data['title'] = "Stock Opname";
			$data['icons'] = "<i class='links_icon fal fa-boxes-alt' style='font-size: 22px;'></i>";
			$data['script'] = "menu/inventory/stock_opname/js";
			$this->template->view('menu/inventory/stock_opname/index_stock_opname', $data);
		} else {
			$this->session->unset_userdata('myusername');
			$this->load->view('menu/login/login');
		}
	}

	public function stock_opname_recount()
	{
		if (true == $this->session->has_userdata('myusername')) {
			$data['title'] = "Stock Opname Recount";
			$data['icons'] = "<i class='links_icon fal fa-boxes-alt' style='font-size: 22px;'></i>";
			$data['script'] = "menu/inventory/stock_opname_recount/js";
			$this->template->view('menu/inventory/stock_opname_recount/index_stock_opname_recount', $data);
		} else {
			$this->session->unset_userdata('myusername');
			$this->load->view('menu/login/login');
		}
	}

	public function list_data_approve()
	{
		if (true == $this->session->has_userdata('myusername')) {
			$data['title'] = "List Data Recount Approve";
			$data['icons'] = "<i class='links_icon fal fa-boxes-alt' style='font-size: 22px;'></i>";
			$data['script'] = "menu/inventory/list_approve/js";
			$this->template->view('menu/inventory/list_approve/index_listapprove_recount', $data);
		} else {
			$this->session->unset_userdata('myusername');
			$this->load->view('menu/login/login');
		}
	}

	public function bag()
	{
		if (true == $this->session->has_userdata('myusername')) {
			$data['title'] = "Berita Acara Gudang";
			$data['icons'] = "<i class='links_icon fas fa-clipboard-list' style='font-size: 22px;'></i>";
			$data['script'] = "menu/inventory/bag/js";
			$this->template->view('menu/inventory/bag/index_bag', $data);
		} else {
			$this->session->unset_userdata('myusername');
			$this->load->view('menu/login/login');
		}
	}

	public function surat_jalan_gudang()
	{
		if (true == $this->session->has_userdata('myusername')) {
			$data['title'] = "Surat Jalan Gudang";
			$data['icons'] = "<i class='links_icon fas fa-clipboard-list' style='font-size: 22px;'></i>";
			$data['script'] = "menu/inventory/surat_jalan_gudang/js";
			$this->template->view('menu/inventory/surat_jalan_gudang/index_sj_gudang', $data);
		} else {
			$this->session->unset_userdata('myusername');
			$this->load->view('menu/login/login');
		}
	}

	public function transfer_stock()
	{
		if (true == $this->session->has_userdata('myusername')) {
			$data['title'] = "Transfer Stock";
			$data['icons'] = "<i class='links_icon fal fa-boxes-alt' style='font-size: 22px;'></i>";
			$data['script'] = "menu/inventory/transfer_stock/js";
			$this->template->view('menu/inventory/transfer_stock/index_transfer_stock', $data);
		} else {
			$this->session->unset_userdata('myusername');
			$this->load->view('menu/login/login');
		}
	}

	public function penerimaan_transfer_stock()
	{
		if (true == $this->session->has_userdata('myusername')) {
			$data['title'] = "Penerimaan Transfer Stock";
			$data['icons'] = "<i class='links_icon fal fa-boxes-alt' style='font-size: 22px;'></i>";
			$data['script'] = "menu/inventory/penerimaan_transfer_stock/js";
			$this->template->view('menu/inventory/penerimaan_transfer_stock/index_penerimaan_transfer_stock', $data);
		} else {
			$this->session->unset_userdata('myusername');
			$this->load->view('menu/login/login');
		}
	}

	public function retur_barang()
	{
		if (true == $this->session->has_userdata('myusername')) {
			$data['title'] = "Retur Barang";
			$data['icons'] = "<i class='links_icon fal fa-boxes-alt' style='font-size: 22px;'></i>";
			$data['script'] = "menu/inventory/retur_barang/js";
			$this->template->view('menu/inventory/retur_barang/index_retur_barang', $data);
		} else {
			$this->session->unset_userdata('myusername');
			$this->load->view('menu/login/login');
		}
	}

	public function req_repack()
	{
		if (true == $this->session->has_userdata('myusername')) {
			$data['title'] = "Request Repack Stock";
			$data['icons'] = "<i class='links_icon fal fa-boxes-alt' style='font-size: 22px;'></i>";
			$data['script'] = "menu/inventory/req_repack_stock/js";
			$this->template->view('menu/inventory/req_repack_stock/index_req_repack_stock', $data);
		} else {
			$this->session->unset_userdata('myusername');
			$this->load->view('menu/login/login');
		}
	}

	public function penerimaan_repack()
	{
		if (true == $this->session->has_userdata('myusername')) {
			$data['title'] = "Penerimaan Repack Stock";
			$data['icons'] = "<i class='links_icon fal fa-boxes-alt' style='font-size: 22px;'></i>";
			$data['script'] = "menu/inventory/penerimaan_repack/js";
			$this->template->view('menu/inventory/penerimaan_repack/index_penerimaan_repack', $data);
		} else {
			$this->session->unset_userdata('myusername');
			$this->load->view('menu/login/login');
		}
	}

	public function proses_repack()
	{
		if (true == $this->session->has_userdata('myusername')) {
			$data['title'] = "Proses Repack Stock";
			$data['icons'] = "<i class='links_icon fal fa-boxes-alt' style='font-size: 22px;'></i>";
			$data['script'] = "menu/inventory/proses_repack_stock/js";
			$this->template->view('menu/inventory/proses_repack_stock/index_proses_repack_stock', $data);
		} else {
			$this->session->unset_userdata('myusername');
			$this->load->view('menu/login/login');
		}
	}
	// End of INVENTORY

	// PURCHASE ORDER
	public function penerimaan_barang()
	{
		if (true == $this->session->has_userdata('myusername')) {
			$data['title'] = "Penerimaan Barang";
			$data['icons'] = "<i class='links_icon fal fa-boxes-alt' style='font-size: 22px;'></i>";
			$data['script'] = "menu/purchase/penerimaan_barang/js";
			$this->template->view('menu/purchase/penerimaan_barang/index_penerimaan_barang', $data);
		} else {
			$this->session->unset_userdata('myusername');
			$this->load->view('menu/login/login');
		}
	}
	// End of PURCHASE ORDER

	// MASTER DATA

	public function supplier()
	{
		if (true == $this->session->has_userdata('myusername')) {
			$data['title'] = "Supplier";
			$data['icons'] = "<i class='links_icon fa fa-users' style='font-size: 22px;'></i>";
			$data['script'] = "menu/masterdata/supplier/js";
			$this->template->view('menu/masterdata/supplier/index_supplier', $data);
		} else {
			$this->session->unset_userdata('myusername');
			$this->load->view('menu/login/login');
		}
	}

	public function free_barang()
	{
		if (true == $this->session->has_userdata('myusername')) {
			$data['title'] = "Free Barang";
			$data['icons'] = "<i class='links_icon fa fa-users' style='font-size: 22px;'></i>";
			$data['script'] = "menu/masterdata/free_barang/js";
			$this->template->view('menu/masterdata/free_barang/index_freebarang', $data);
		} else {
			$this->session->unset_userdata('myusername');
			$this->load->view('menu/login/login');
		}
	}

	public function kelompok_toko()
	{
		if (true == $this->session->has_userdata('myusername')) {
			$data['title'] = "Master Kelompok Toko";
			$data['icons'] = "<i class='links_icon fa fa-users' style='font-size: 22px;'></i>";
			$data['script'] = "menu/masterdata/grup_toko/js";
			$this->template->view('menu/masterdata/grup_toko/index_group_toko', $data);
		} else {
			$this->session->unset_userdata('myusername');
			$this->load->view('menu/login/login');
		}
	}

	public function grup_pelanggan()
	{
		if (true == $this->session->has_userdata('myusername')) {
			$data['title'] = "Grup Pelanggan";
			$data['icons'] = "<i class='links_icon fa fa-users' style='font-size: 22px;'></i>";
			$data['script'] = "menu/masterdata/grup_pelanggan/js";
			$this->template->view('menu/masterdata/grup_pelanggan/index_grup_pelanggan', $data);
		} else {
			$this->session->unset_userdata('myusername');
			$this->load->view('menu/login/login');
		}
	}

	public function pelanggan()
	{
		if (true == $this->session->has_userdata('myusername')) {
			$data['title'] = "Pelanggan";
			$data['icons'] = "<i class='links_icon fa fa-users' style='font-size: 22px;'></i>";
			$data['script'] = "menu/masterdata/pelanggan/js";
			$this->template->view('menu/masterdata/pelanggan/index_pelanggan', $data);
		} else {
			$this->session->unset_userdata('myusername');
			$this->load->view('menu/login/login');
		}
	}

	public function merk()
	{
		if (true == $this->session->has_userdata('myusername')) {
			$data['title'] = "Merk";
			$data['icons'] = "<i class='links_icon fal fa-boxes-alt' style='font-size: 22px;'></i>";
			$data['script'] = "menu/masterdata/merk/js";
			$this->template->view('menu/masterdata/merk/index_merk', $data);
		} else {
			$this->session->unset_userdata('myusername');
			$this->load->view('menu/login/login');
		}
	}

	public function barang()
	{
		if (true == $this->session->has_userdata('myusername')) {
			$data['title'] = "Barang";
			$data['icons'] = "<i class='links_icon fal fa-boxes-alt' style='font-size: 22px;'></i>";
			$data['script'] = "menu/masterdata/barang/js";
			$this->template->view('menu/masterdata/barang/index_barang', $data);
		} else {
			$this->session->unset_userdata('myusername');
			$this->load->view('menu/login/login');
		}
	}

	public function kodepos()
	{
		if (true == $this->session->has_userdata('myusername')) {
			$data['title'] = "Kode Pos";
			$data['icons'] = "<i class='links_icon fa fa-envelope' style='font-size: 22px;'></i>";
			$data['script'] = "menu/masterdata/kode_pos/js";
			$this->template->view('menu/masterdata/kode_pos/index_kode_pos', $data);
		} else {
			$this->session->unset_userdata('myusername');
			$this->load->view('menu/login/login');
		}
	}

	public function cabang()
	{
		if (true == $this->session->has_userdata('myusername')) {
			$data['title'] = "Cabang";
			$data['icons'] = "<i class='links_icon fas fa-store' style='font-size: 22px;'></i>";
			$data['script'] = "menu/masterdata/cabang/js";
			$this->template->view('menu/masterdata/cabang/index_cabang', $data);
		} else {
			$this->session->unset_userdata('myusername');
			$this->load->view('menu/login/login');
		}
	}

	public function top()
	{
		if (true == $this->session->has_userdata('myusername')) {
			$data['title'] = "Term of Payment";
			$data['icons'] = "<i class='links_icon fa fa-calendar-o' style='font-size: 22px;'></i>";
			$data['script'] = "menu/masterdata/top/js";
			$this->template->view('menu/masterdata/top/index_top', $data);
		} else {
			$this->session->unset_userdata('myusername');
			$this->load->view('menu/login/login');
		}
	}

	public function rekening_transfer()
	{
		if (true == $this->session->has_userdata('myusername')) {
			$data['title'] = "Rekening Transfer";
			$data['icons'] = "<i class='links_icon fa fa-credit-card' style='font-size: 22px;'></i>";
			$data['script'] = "menu/masterdata/master_rekening/js";
			$this->template->view('menu/masterdata/master_rekening/index_master_rekening', $data);
		} else {
			$this->session->unset_userdata('myusername');
			$this->load->view('menu/login/login');
		}
	}

	public function salesman()
	{
		if (true == $this->session->has_userdata('myusername')) {
			$data['title'] = "Salesman";
			$data['icons'] = "<i class='links_icon fa fa-user-circle' style='font-size: 22px;'></i>";
			$data['script'] = "menu/masterdata/salesman/js";
			$this->template->view('menu/masterdata/salesman/index_salesman', $data);
		} else {
			$this->session->unset_userdata('myusername');
			$this->load->view('menu/login/login');
		}
	}

	public function satuan()
	{
		if (true == $this->session->has_userdata('myusername')) {
			$data['title'] = "Satuan";
			$data['icons'] = "<i class='links_icon fa fa-balance-scale' style='font-size: 22px;'></i>";
			$data['script'] = "menu/masterdata/satuan/js";
			$this->template->view('menu/masterdata/satuan/index_satuan', $data);
		} else {
			$this->session->unset_userdata('myusername');
			$this->load->view('menu/login/login');
		}
	}

	public function supervisor()
	{
		if (true == $this->session->has_userdata('myusername')) {
			$data['title'] = "Supervisor";
			$data['icons'] = "<i class='links_icon fa fa-user-circle' style='font-size: 22px;'></i>";
			$data['script'] = "menu/masterdata/supervisor/js";
			$this->template->view('menu/masterdata/supervisor/index_supervisor', $data);
		} else {
			$this->session->unset_userdata('myusername');
			$this->load->view('menu/login/login');
		}
	}

	public function gudang()
	{
		if (true == $this->session->has_userdata('myusername')) {
			$data['title'] = "Gudang";
			$data['icons'] = "<i class='links_icon fas fa-warehouse' style='font-size: 22px;'></i>";
			$data['script'] = "menu/masterdata/gudang/js";
			$this->template->view('menu/masterdata/gudang/index_gudang', $data);
		} else {
			$this->session->unset_userdata('myusername');
			$this->load->view('menu/login/login');
		}
	}

	public function coa()
	{
		if (true == $this->session->has_userdata('myusername')) {
			$data['title'] = "COA";
			$data['icons'] = "<i class='links_icon fas fa-shopping-cart' style='font-size: 22px;'></i>";
			$data['script'] = "menu/masterdata/coa/js";
			$this->template->view('menu/masterdata/coa/index_coa', $data);
		} else {
			$this->session->unset_userdata('myusername');
			$this->load->view('menu/login/login');
		}
	}

	public function diskon_pp()
	{
		if (true == $this->session->has_userdata('myusername')) {
			$data['title'] = "Promo";
			$data['icons'] = "<i class='links_icon fa fa-percent' style='font-size: 22px;'></i>";
			$data['script'] = "menu/masterdata/promo/js";
			$this->template->view('menu/masterdata/promo/index_promo', $data);
		} else {
			$this->session->unset_userdata('myusername');
			$this->load->view('menu/login/login');
		}
	}

	public function konfigurasi()
	{
		if (true == $this->session->has_userdata('myusername')) {
			$data['title'] = "Konfigurasi";
			$data['icons'] = "<i class='links_icon fas fa-cogs' style='font-size: 22px;'></i>";
			$data['script'] = "menu/masterdata/konfigurasi/js";
			$this->template->view('menu/masterdata/konfigurasi/index_konfigurasi', $data);
		} else {
			$this->session->unset_userdata('myusername');
			$this->load->view('menu/login/login');
		}
	}

	public function level_diskon()
	{
		if (true == $this->session->has_userdata('myusername')) {
			$data['title'] = "Level Diskon";
			$data['icons'] = "<i class='links_icon fa fa-percent' style='font-size: 22px;'></i>";
			$data['script'] = "menu/masterdata/level_diskon/js";
			$this->template->view('menu/masterdata/level_diskon/index_level_diskon', $data);
		} else {
			$this->session->unset_userdata('myusername');
			$this->load->view('menu/login/login');
		}
	}

	public function diskon_khusus_pelanggan()
	{
		if (true == $this->session->has_userdata('myusername')) {
			$data['title'] = "Diskon Pelanggan Khusus";
			$data['icons'] = "<i class='links_icon fa fa-percent' style='font-size: 22px;'></i>";
			$data['script'] = "menu/masterdata/diskon_khusus/js";
			$this->template->view('menu/masterdata/diskon_khusus/index_diskon_khusus', $data);
		} else {
			$this->session->unset_userdata('myusername');
			$this->load->view('menu/login/login');
		}
	}

	public function diskon_kelompokaj()
	{
		if (true == $this->session->has_userdata('myusername')) {
			$data['title'] = "Diskon Kelompok A - J";
			$data['icons'] = "<i class='links_icon fa fa-percent' style='font-size: 22px;'></i>";
			$data['script'] = "menu/masterdata/kelompokaj/js";
			$this->template->view('menu/masterdata/kelompokaj/index_kelompokaj', $data);
		} else {
			$this->session->unset_userdata('myusername');
			$this->load->view('menu/login/login');
		}
	}

	public function barang_detail()
	{
		if (true == $this->session->has_userdata('myusername')) {
			$data['title'] = "Barang Detail";
			$data['icons'] = "<i class='links_icon fal fa-boxes-alt' style='font-size: 22px;'></i>";
			$data['script'] = "menu/masterdata/barang_detail/js";
			$this->template->view('menu/masterdata/barang_detail/index_barang_detail', $data);
		} else {
			$this->session->unset_userdata('myusername');
			$this->load->view('menu/login/login');
		}
	}

	public function diskon_kategori_produk()
	{
		if (true == $this->session->has_userdata('myusername')) {
			$data['title'] = "Disc. Produk Kategori Tertentu";
			$data['icons'] = "<i class='links_icon fal fa-boxes-alt' style='font-size: 22px;'></i>";
			$data['script'] = "menu/masterdata/diskon_kategori_produk/js";
			$this->template->view('menu/masterdata/diskon_kategori_produk/index_diskon_kategori_produk', $data);
		} else {
			$this->session->unset_userdata('myusername');
			$this->load->view('menu/login/login');
		}
	}

	public function user()
	{
		if (true == $this->session->has_userdata('myusername')) {
			$data['title'] = "User";
			$data['icons'] = "<i class='links_icon fal fa-boxes-alt' style='font-size: 22px;'></i>";
			$data['script'] = "menu/masterdata/user/js";
			$this->template->view('menu/masterdata/user/index_user', $data);
		} else {
			$this->session->unset_userdata('myusername');
			$this->load->view('menu/login/login');
		}
	}

	public function registrasi_faktur_pajak()
	{
		if (true == $this->session->has_userdata('myusername')) {
			$data['title'] = "Registrasi Nomor Faktur Pajak";
			$data['icons'] = "<i class='links_icon far fa-receipt' style='font-size: 22px;'></i>";
			$data['script'] = "menu/masterdata/registrasi_faktur_pajak/js";
			$this->template->view('menu/masterdata/registrasi_faktur_pajak/index_registrasi_faktur_pajak', $data);
		} else {
			$this->session->unset_userdata('myusername');
			$this->load->view('menu/login/login');
		}
	}

	// End of MASTERDATA

	// AKUN
	public function akun_pengguna()
	{
		if (true == $this->session->has_userdata('myusername')) {
			$data['title'] = "Ganti Password";
			$data['icons'] = "<i class='links_icon fas fa-user' style='font-size: 22px;'></i>";
			$data['script'] = "menu/masterdata/akun_pengguna/js";
			$this->template->view('menu/masterdata/akun_pengguna/index_akun_pengguna', $data);
		} else {
			$this->session->unset_userdata('myusername');
			$this->load->view('menu/login/login');
		}
	}
	// End of AKUN
}
