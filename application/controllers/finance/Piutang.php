<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Piutang extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set("Asia/Jakarta");
        $this->load->model('finance/Piutang_model');
        $this->load->model('caridataaktif_model');
        $this->load->model('global/global_model');
        $this->load->library('form_validation');
        $this->load->library('session');
    }
    function CariDataCustomer()
    {
        $fetch_data = $this->caridataaktif_model->make_datatables($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value'));
        $data = array();
        foreach ($fetch_data as $row) {
            $sub_array = array();
            $i = 1;
            $count = count($this->input->post('field'));
            foreach ($this->input->post('field') as $key => $value) {
                if ($i <= $count) {
                    if ($i == 1) {
                        $msearch = $row->$value;
                        $sub_array[] = '<button class="btn btn-dark searchpiutang" data-id="' . $msearch . '"  data-dismiss="modal" style="font-size: 16px; margin-bottom: 0px; border-radius: 4px;"><i class="far fa-check-circle"></i></button> ';
                        $sub_array[] = $row->$value;
                    } else {
                        if ($i == $count) {
                            $sub_array[] = $row->$value;
                        } else {
                            $sub_array[] = $row->$value;
                        }
                    }
                }
                $i++;
            }
            $data[] = $sub_array;
        }
        $output = array(
            "draw"                    =>     intval($_POST["draw"]),
            "recordsTotal"          =>      $this->caridataaktif_model->get_all_data($this->input->post('nmtb')),
            "recordsFiltered"     =>     $this->caridataaktif_model->get_filtered_data($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value')),
            "data"                    =>     $data
        );
        echo json_encode($output);
    }

    function GetDataPiutang()
    {
        $result =  $this->Piutang_model->GetDataPiutang($this->input->post('nopelanggan'));
        echo json_encode($result);
    }

    function GetDataDaftarPiutang()
    {
        $result =  $this->Piutang_model->GetDataDaftarPiutang($this->input->post('nopelanggan'));
        echo json_encode($result);
    }
}