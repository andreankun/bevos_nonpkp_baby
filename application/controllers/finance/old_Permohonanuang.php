<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Permohonanuang extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set("Asia/Jakarta");
        $this->load->model('finance/Permohonanuang_model');
        $this->load->model('caridataaktif_model');
        $this->load->library('form_validation');
        $this->load->library('session');
    }

    function CariJenisPengeluaran()
    {
        $fetch_data = $this->caridataaktif_model->make_datatables($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value'));
        $data = array();
        foreach ($fetch_data as $row) {
            $sub_array = array();
            $i = 1;
            $count = count($this->input->post('field'));
            foreach ($this->input->post('field') as $key => $value) {
                if ($i <= $count) {
                    if ($i == 1) {
                        $msearch = $row->$value;
                        $sub_array[] = '<button class="btn btn-dark searchjenis" data-id="' . $msearch . '"  data-dismiss="modal" style="font-size: 16px; margin-bottom: 0px; border-radius: 4px;"><i class="far fa-check-circle"></i></button> ';
                        $sub_array[] = $row->$value;
                    } else {
                        if ($i == $count) {
                            $sub_array[] = $row->$value;
                        } else {
                            $sub_array[] = $row->$value;
                        }
                    }
                }
                $i++;
            }
            $data[] = $sub_array;
        }
        $output = array(
            "draw"                    =>     intval($_POST["draw"]),
            "recordsTotal"          =>      $this->caridataaktif_model->get_all_data($this->input->post('nmtb')),
            "recordsFiltered"     =>     $this->caridataaktif_model->get_filtered_data($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value')),
            "data"                    =>     $data
        );
        echo json_encode($output);
    }

    function DataJenisPengeluaran()
    {
        $result =  $this->Permohonanuang_model->DataJenisPengeluaran($this->input->post('kode'));
        echo json_encode($result);
    }

    function CariDepartement()
    {
        $fetch_data = $this->caridataaktif_model->make_datatables($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value'));
        $data = array();
        foreach ($fetch_data as $row) {
            $sub_array = array();
            $i = 1;
            $count = count($this->input->post('field'));
            foreach ($this->input->post('field') as $key => $value) {
                if ($i <= $count) {
                    if ($i == 1) {
                        $msearch = $row->$value;
                        $sub_array[] = '<button class="btn btn-dark searchdepartement" data-id="' . $msearch . '"  data-dismiss="modal" style="font-size: 16px; margin-bottom: 0px; border-radius: 4px;"><i class="far fa-check-circle"></i></button> ';
                        $sub_array[] = $row->$value;
                    } else {
                        if ($i == $count) {
                            $sub_array[] = $row->$value;
                        } else {
                            $sub_array[] = $row->$value;
                        }
                    }
                }
                $i++;
            }
            $data[] = $sub_array;
        }
        $output = array(
            "draw"                    =>     intval($_POST["draw"]),
            "recordsTotal"          =>      $this->caridataaktif_model->get_all_data($this->input->post('nmtb')),
            "recordsFiltered"     =>     $this->caridataaktif_model->get_filtered_data($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value')),
            "data"                    =>     $data
        );
        echo json_encode($output);
    }

    function DataDepartement()
    {
        $result =  $this->Permohonanuang_model->DataDepartemen($this->input->post('kode'));
        echo json_encode($result);
    }

    function CariDataHutang()
    {
        $fetch_data = $this->caridataaktif_model->make_datatables($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value'));
        $data = array();
        foreach ($fetch_data as $row) {
            $sub_array = array();
            $i = 1;
            $count = count($this->input->post('field'));
            foreach ($this->input->post('field') as $key => $value) {
                if ($i <= $count) {
                    if ($i == 1) {
                        $msearch = $row->$value;
                        $sub_array[] = '<button class="btn btn-dark searchhutang" data-id="' . $msearch . '"  data-dismiss="modal" style="font-size: 16px; margin-bottom: 0px; border-radius: 4px;"><i class="far fa-check-circle"></i></button> ';
                        $sub_array[] = $row->$value;
                    } else {
                        if ($i == $count) {
                            $sub_array[] = $row->$value;
                        } else {
                            $sub_array[] = $row->$value;
                        }
                    }
                }
                $i++;
            }
            $data[] = $sub_array;
        }
        $output = array(
            "draw"                    =>     intval($_POST["draw"]),
            "recordsTotal"          =>      $this->caridataaktif_model->get_all_data($this->input->post('nmtb')),
            "recordsFiltered"     =>     $this->caridataaktif_model->get_filtered_data($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value')),
            "data"                    =>     $data
        );
        echo json_encode($output);
    }

    public function DataHutang()
    {
        $result =  $this->Permohonanuang_model->DataHutang($this->input->post('nomor'));
        echo json_encode($result);
    }

    function CariDokumenLainLain()
    {
        $fetch_data = $this->caridataaktif_model->make_datatables($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value'));
        $data = array();
        foreach ($fetch_data as $row) {
            $sub_array = array();
            $i = 1;
            $count = count($this->input->post('field'));
            foreach ($this->input->post('field') as $key => $value) {
                if ($i <= $count) {
                    if ($i == 1) {
                        $msearch = $row->$value;
                        $sub_array[] = '<button class="btn btn-dark searchdoclain" data-id="' . $msearch . '"  data-dismiss="modal" style="font-size: 16px; margin-bottom: 0px; border-radius: 4px;"><i class="far fa-check-circle"></i></button> ';
                        $sub_array[] = $row->$value;
                    } else {
                        if ($i == $count) {
                            $sub_array[] = $row->$value;
                        } else {
                            $sub_array[] = $row->$value;
                        }
                    }
                }
                $i++;
            }
            $data[] = $sub_array;
        }
        $output = array(
            "draw"                    =>     intval($_POST["draw"]),
            "recordsTotal"          =>      $this->caridataaktif_model->get_all_data($this->input->post('nmtb')),
            "recordsFiltered"     =>     $this->caridataaktif_model->get_filtered_data($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value')),
            "data"                    =>     $data
        );
        echo json_encode($output);
    }

    function DataAccountLain()
    {
        $result =  $this->Permohonanuang_model->DataAccountlain($this->input->post('nomor'));
        echo json_encode($result);
    }

    function CariAccount()
    {
        $fetch_data = $this->caridataaktif_model->make_datatables($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value'));
        $data = array();
        foreach ($fetch_data as $row) {
            $sub_array = array();
            $i = 1;
            $count = count($this->input->post('field'));
            foreach ($this->input->post('field') as $key => $value) {
                if ($i <= $count) {
                    if ($i == 1) {
                        $msearch = $row->$value;
                        $sub_array[] = '<button class="btn btn-dark searchaccount" data-id="' . $msearch . '"  data-dismiss="modal" style="font-size: 16px; margin-bottom: 0px; border-radius: 4px;"><i class="far fa-check-circle"></i></button> ';
                        $sub_array[] = $row->$value;
                    } else {
                        if ($i == $count) {
                            $sub_array[] = $row->$value;
                        } else {
                            $sub_array[] = $row->$value;
                        }
                    }
                }
                $i++;
            }
            $data[] = $sub_array;
        }
        $output = array(
            "draw"                    =>     intval($_POST["draw"]),
            "recordsTotal"          =>      $this->caridataaktif_model->get_all_data($this->input->post('nmtb')),
            "recordsFiltered"     =>     $this->caridataaktif_model->get_filtered_data($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value')),
            "data"                    =>     $data
        );
        echo json_encode($output);
    }

    function DataAccount()
    {
        $result =  $this->Permohonanuang_model->DataAccount($this->input->post('nomor'));
        echo json_encode($result);
    }

    function CariPermohonanUang()
    {
        $fetch_data = $this->caridataaktif_model->make_datatables($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value'));
        $data = array();
        foreach ($fetch_data as $row) {
            $sub_array = array();
            $i = 1;
            $count = count($this->input->post('field'));
            foreach ($this->input->post('field') as $key => $value) {
                if ($i <= $count) {
                    if ($i == 1) {
                        $msearch = $row->$value;
                        $sub_array[] = '<button class="btn btn-dark searchpermohonan" data-id="' . $msearch . '"  data-dismiss="modal" style="font-size: 16px; margin-bottom: 0px; border-radius: 4px;"><i class="far fa-check-circle"></i></button> ';
                        $sub_array[] = $row->$value;
                    } else {
                        if ($i == $count) {
                            $sub_array[] = $row->$value;
                        } else {
                            $sub_array[] = $row->$value;
                        }
                    }
                }
                $i++;
            }
            $data[] = $sub_array;
        }
        $output = array(
            "draw"                    =>     intval($_POST["draw"]),
            "recordsTotal"          =>      $this->caridataaktif_model->get_all_data($this->input->post('nmtb')),
            "recordsFiltered"     =>     $this->caridataaktif_model->get_filtered_data($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value')),
            "data"                    =>     $data
        );
        echo json_encode($output);
    }

    function DataPermohonanUang()
    {
        $result =  $this->Permohonanuang_model->DataPermohonanUang($this->input->post('nomor'));
        echo json_encode($result);
    }

    function DataPermohonanUangDetail()
    {
        $result =  $this->Permohonanuang_model->DataPermohonanUangDetail($this->input->post('nomor'));
        echo json_encode($result);
    }


    public function ClearChr($param)
    {
        return $result = str_replace(array('\'', '"', ',', ';', '<', '>', '_', '-', ' ', '.', '!', '?', '/', '=', '+', ']', '[', '{', '}', '@', '#', '$', '%', '^', '&', "*", '(', ')'), '', $param);
    }

    function Save()
    {
        $nomor = "";
        $pemakai = $this->session->userdata('myusername');
        $tanggal = $this->input->post('tanggal');
        $kodedepartement = $this->input->post('kodedepartement');
        $keterangan = $this->input->post('keterangan');
        $namadepartement = $this->input->post('namadepartement');
        $kodepengeluaran = $this->input->post('kodepengeluaran');
        $namapengeluaran = $this->input->post('namapengeluaran');
        $kodecabang = $this->input->post('kodecabang');
        $datadetail = $this->input->post('datadetail');
        $errorvalidasi = false;

        $ambilnomor = "M" . $kodecabang . '-' . date("Y") . '-' . date("m");
        $get["np"] = $this->Permohonanuang_model->GetMaxNomor($ambilnomor);

        if (!$get["np"]->nomor) {
            $nomor = $ambilnomor . "-" . "00001";
        } else {
            $lastNomor = $get['np']->nomor;
            // print_r($lastNomor);
            // die();
            $lastNoUrut = substr($lastNomor, 11, 11);
            // nomor urut ditambah 1
            $nextNoUrut = $lastNoUrut + 1;
            $nomor = $ambilnomor . "-" . sprintf('%05s', $nextNoUrut);
        }
        if (empty($nomor) || $nomor == "") {
            $resultjson = array(
                'nomor' => "",
                'message' => "Data gagal disimpan, Error Generate Nomor."
            );
            $errorvalidasi = TRUE;
            echo json_encode($resultjson);
            return FALSE;
        }
        if (empty($datadetail)) {
            $resultjson = array(
                'nomor' => "",
                'message' => "Data gagal disimpan, Data Detail Kosong."
            );
            $errorvalidasi = TRUE;
            echo json_encode($resultjson);
            return FALSE;
        }
        if ($errorvalidasi  == false) {


            $this->db->trans_start(); # Starting Transaction
            $this->db->trans_strict(FALSE);

            if ($kodepengeluaran == 'KUF99') {
                $data = array(
                    'nomor' => $nomor,
                    'tanggal' => $tanggal,
                    'jenispengeluaran' => $kodepengeluaran,
                    'kodedepartement' => $kodedepartement,
                    'namadepartement' => $namadepartement,
                    'keterangan' => $keterangan,
                    'tglsimpan' => date("Y-m-d H:i:s"),
                    'pemakai' => $pemakai,
                    'kodecabang' => $kodecabang,
                );
                $this->Permohonanuang_model->savePermohonanUang($data);
            } else {
                $data = array(
                    'nomor' => $nomor,
                    'tanggal' => $tanggal,
                    'jenispengeluaran' => $kodepengeluaran,
                    'keterangan' => $keterangan,
                    'tglsimpan' => date("Y-m-d H:i:s"),
                    'pemakai' => $pemakai,
                    'kodcabang' => $kodecabang,
                );
                $this->Permohonanuang_model->savePermohonanUang($data);
            }

            if (!empty($datadetail)) {
                foreach ($datadetail as $key => $value) {

                    if ($kodepengeluaran == 'KUF01') {
                        $data = array(
                            'nomorpermohonan' => $nomor,
                            'noreferensi' => $value['DocInvoice'],
                            'kodesupplier' => $value['NoReferensi'],
                            'namasupplier' => $value['Referensi'],
                            'nilaipermohonan' => floatval(str_replace(",", "", $value['NilaiPengeluaran'])),
                            'kodeaccount' => $value['Account'],
                            'nilaialokasi' => floatval(str_replace(",", "", $value['NilaiAlokasi'])),
                            'accountalokasi' => $value['AccountAlokasi'],
                            'memo' => $value['Memo'],
                        );
                        $this->Permohonanuang_model->savePermohonanUangDetail($data);

                        $hutang = $this->Permohonanuang_model->DataHutangOnly($value['DocInvoice']);
                        $data = array(
                            'nilaipermohonan' => $hutang->nilaipermohonan + (floatval(str_replace(",", "", $value['NilaiPengeluaran'])) + floatval(str_replace(",", "", $value['NilaiAlokasi']))),
                        );
                        $this->Permohonanuang_model->updateHutang($data, $value['DocInvoice']);
                    } else if ($kodepengeluaran == 'KUF99') {
                        $data = array(
                            'nomorpermohonan' => $nomor,
                            'noreferensi' =>  $value['NoReferensi'],
                            'kodesupplier' => $value['NoReferensi'],
                            'namasupplier' => $value['Referensi'],
                            'nilaipermohonan' => floatval(str_replace(",", "", $value['NilaiPengeluaran'])),
                            'kodeaccount' => $value['Account'],
                            'nilaialokasi' => floatval(str_replace(",", "", $value['NilaiAlokasi'])),
                            'accountalokasi' => $value['AccountAlokasi'],
                            'memo' => $value['Memo'],
                        );
                        // print_r($data);
                        // die();
                        $this->Permohonanuang_model->savePermohonanUangDetail($data);
                    }
                }
            };

            $this->db->trans_complete();

            if ($this->db->trans_status() === FALSE) {
                $resultjson = array(
                    'nomor' => "",
                    'message' => "Data gagal disimpan, Nomor sudah pernah digunakan"
                );
                # Something went wrong.
                $this->db->trans_rollback();
            } else {
                $resultjson = array(
                    'nomor' => $nomor,
                    'message' => "Data berhasil disimpan"
                );
                # Everything is Perfect. 
                # Committing data to the database.
                $this->db->trans_commit();
            }
            echo json_encode($resultjson);
        }
    }

    function Cancel()
    {
        $pemakai = $this->session->userdata('myusername');

        $nomor = $this->input->post('nomor');
        $jenis = $this->input->post('kodepengeluaran');
        $datadetail = $this->input->post('datadetail');
        $validasicancel = FALSE;

        foreach ($datadetail as $key => $value) {
            if ($jenis == 'KUF01') {
                $cek['pengeluaran'] = $this->Permohonanuang_model->CekPengeluaran($value['DocInvoice']);
                if (!empty($cek['faktur'])) {
                    $resultjson = array(
                        'error' => true,
                        'message' => "Data gagal dibatalkan, Nomor " . $value['DocInvoice'] . " Telah Terbit Pengeleluaran"
                    );
                    $validasicancel = TRUE;
                    echo json_encode($resultjson);
                    return FALSE;
                }
            } else {
                $cek['pengeluaran'] = $this->Permohonanuang_model->CekPengeluaran($nomor);
                if (!empty($cek['faktur'])) {
                    $resultjson = array(
                        'error' => true,
                        'message' => "Data gagal dibatalkan, Nomor " . $nomor . " Telah Terbit Pengeleluaran"
                    );
                    $validasicancel = TRUE;
                    echo json_encode($resultjson);
                    return FALSE;
                }
            }
        }

        if ($validasicancel == FALSE) {

            $this->db->trans_start(); # Starting Transaction
            $this->db->trans_strict(FALSE);

            foreach ($datadetail as $key => $value) {
                if ($this->input->post('kodepengeluaran') == 'KUF01') {
                    #BATAL PERMOHONAN
                    if (!empty($this->input->post('datadetail'))) {
                        foreach ($this->input->post('datadetail') as $key => $value) {
                            $hutang = $this->Permohonanuang_model->DataHutangOnly($value['DocInvoice']);
                            $data = array(
                                'nilaipembayaran' => $value->nilaipembayaran - floatval(str_replace(",", "", $value['NilaiPengeluaran'])),
                            );
                            $this->Permohonanuang_model->updateBatalHutang($data, $value['DocInvoice']);
                        }
                    };
                }
            }

            $data = array(
                'keteranganbatal' => $this->input->post('keterangan'),
                'batal' => true,
                'tanggalbatal' => date("Y-m-d H:i:s"),
                'userbatal' => $pemakai
            );
            $this->Permohonanuang_model->CancelTransaksi($data, $nomor);
            $this->db->trans_complete();

            if ($this->db->trans_status() === FALSE) {
                $resultjson = array(
                    'error' => true,
                    'message' => "Data gagal disimpan, Silahkan Hub. Vendor"
                );
                # Something went wrong.
                $this->db->trans_rollback();
            } else {
                $resultjson = array(
                    'error' => false,
                    'message' => "Data berhasil dibatalkan"
                );
                # Everything is Perfect. 
                # Committing data to the database.
                $this->db->trans_commit();
            }
            echo json_encode($resultjson);
        }
    }
}
