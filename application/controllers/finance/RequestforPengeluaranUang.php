<?php

defined('BASEPATH') or exit('No direct script access allowed');

class RequestforPengeluaranUang extends CI_Controller
{

  public function __construct()
  {
    parent::__construct();
    date_default_timezone_set("Asia/Jakarta");
    $this->load->model('finance/Pengeluaranuang_model');
    $this->load->model('Caridataaktif_model');
    $this->load->library('form_validation');
    $this->load->library('session');
  }

  function DataPengeluaranUang()
  {
    $result =  $this->Pengeluaranuang_model->DataPengeluaranUang($this->input->post('nomor'));
    echo json_encode($result);
  }

  function DataPengeluaranUangDetail()
  {
    $result =  $this->Pengeluaranuang_model->DataPengeluaranUangDetail($this->input->post('nomor'));
    echo json_encode($result);
  }

  function DataJenisPengeluaran()
  {
    $result =  $this->Pengeluaranuang_model->DataJenisPengeluaran($this->input->post('kode'));
    echo json_encode($result);
  }

  function DataPermohonanUang()
  {
    $result =  $this->Pengeluaranuang_model->DataPermohonanUang($this->input->post('nomor'));
    echo json_encode($result);
  }

  function DataPermohonanUangDetail()
  {
    $result =  $this->Pengeluaranuang_model->DataPermohonanUangDetail($this->input->post('nomor'));
    echo json_encode($result);
  }

  function CariDataPengeluaranUang()
  {
    $fetch_data = $this->Caridataaktif_model->make_datatables($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'));
    $data = array();
    foreach ($fetch_data as $row) {
      $sub_array = array();
      $i = 1;
      $count = count($this->input->post('field'));
      foreach ($this->input->post('field') as $key => $value) {
        if ($i <= $count) {
          if ($i == 1) {
            $msearch = $row->$value;
            $sub_array[] = '<button class="btn btn-dark searchpengeluaranuang" data-dismiss="modal" style="margin: 0px;" data-id="' . $msearch . '"><i class="far fa-check-circle"></i></button>';
            $sub_array[] = $row->$value;
          } else {
            if ($i == $count) {
              $sub_array[] = $row->$value;
            } else {
              $sub_array[] = $row->$value;
            }
          }
          // $sub_array[] = $row->$value;
        }
        $i++;
      }
      $data[] = $sub_array;
    }
    $output = array(
      "draw"                    =>     intval($_POST["draw"]),
      "recordsTotal"          =>      $this->Caridataaktif_model->get_all_data($this->input->post('nmtb')),
      "recordsFiltered"     =>     $this->Caridataaktif_model->get_filtered_data($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where')),
      "data"                    =>     $data
    );
    echo json_encode($output);
  }

  function CariJenisPengeluaran()
  {
    $fetch_data = $this->Caridataaktif_model->make_datatables($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'));
    $data = array();
    foreach ($fetch_data as $row) {
      if ($row->jenis == 1) {
        $sub_array = array();
        $i = 1;
        $count = count($this->input->post('field'));
        foreach ($this->input->post('field') as $key => $value) {
          if ($i <= $count) {
            if ($i == 1) {
              $msearch = $row->$value;
              $sub_array[] = '<button class="btn btn-dark searchjenispengeluaran" data-dismiss="modal" style="margin: 0px;" data-id="' . $msearch . '"><i class="far fa-check-circle"></i></button>';
              $sub_array[] = $row->$value;
            } else {
              if ($i == $count) {
                $sub_array[] = $row->$value;
              } else {
                $sub_array[] = $row->$value;
              }
            }
            // $sub_array[] = $row->$value;
          }
          $i++;
        }
        $data[] = $sub_array;
      }
    }
    $output = array(
      "draw"                    =>     intval($_POST["draw"]),
      "recordsTotal"          =>      $this->Caridataaktif_model->get_all_data($this->input->post('nmtb')),
      "recordsFiltered"     =>     $this->Caridataaktif_model->get_filtered_data($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where')),
      "data"                    =>     $data
    );
    echo json_encode($output);
  }

  function CariDataPermohonanUang()
  {
    $fetch_data = $this->Caridataaktif_model->make_datatables($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'));
    $data = array();
    foreach ($fetch_data as $row) {
      $sub_array = array();
      $i = 1;
      $count = count($this->input->post('field'));
      foreach ($this->input->post('field') as $key => $value) {
        if ($i <= $count) {
          if ($i == 1) {
            $msearch = $row->$value;
            $sub_array[] = '<button class="btn btn-dark searchpermohonanuang" data-dismiss="modal" style="margin: 0px;" data-id="' . $msearch . '"><i class="far fa-check-circle"></i></button>';
            $sub_array[] = $row->$value;
          } else {
            if ($i == $count) {
              $sub_array[] = $row->$value;
            } else {
              $sub_array[] = $row->$value;
            }
          }
          // $sub_array[] = $row->$value;
        }
        $i++;
      }
      $data[] = $sub_array;
    }
    $output = array(
      "draw"                    =>     intval($_POST["draw"]),
      "recordsTotal"          =>      $this->Caridataaktif_model->get_all_data($this->input->post('nmtb')),
      "recordsFiltered"     =>     $this->Caridataaktif_model->get_filtered_data($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where')),
      "data"                    =>     $data
    );
    echo json_encode($output);
  }
  
  function submitPengeluaran()
  {
    $kodecabang = $this->session->userdata('mycabang');
    $pemakai = $this->session->userdata('myusername');
    
    $nomor = $this->input->post('nomor');
      // print_r($this->input->post()); die();
    if (empty($nomor)) {
      $this->db->trans_start(); # Starting Transaction
      $this->db->trans_strict(FALSE);

      $ambilnomor = "PU" . $kodecabang . date("y") . date("m");
      $get["pengeluaran"] = $this->Pengeluaranuang_model->GetMaxNomor($ambilnomor);
      if (!$get["pengeluaran"]) {
        $nomorpengeluaran = $ambilnomor . "0001";
      } else {
        $lastNomor = $get['pengeluaran']->nomor;
        $lastNoUrut = substr($lastNomor, 10, 14);

        // nomor urut ditambah 1
        $nextNoUrut = $lastNoUrut + 1;
        $nomorpengeluaran = $ambilnomor . sprintf('%04s', $nextNoUrut);;
      }

      $data = [
        'nomor' => $nomorpengeluaran,
        'tanggal' => $this->input->post('tanggal'),
        'jenispengeluaran' => $this->input->post('kodepengeluaran'),
        'nopermohonan' => $this->input->post('nomorpermohonan'),
        'kodedepartemen' => $this->input->post('kodedepartemen'),
        'namadepartement' => $this->input->post('namadepartement'),
        'nilaipengeluaran' => $this->input->post('nilaipengeluaran'),
        'jenisaccount' => $this->input->post('nomoracc'),
        'namaaccount' => $this->input->post('namaacc'),
        'keterangan' => $this->input->post('keterangan'),
        'batal' => false,
        'tglsimpan' => date("Y-m-d H:i:s"),
        'pemakai' => $pemakai,
        'kodecabang' => $kodecabang,
      ];

      $this->Pengeluaranuang_model->savePengeluaranUang($data);

      if (!empty($this->input->post('datadetail'))) {
        foreach ($this->input->post('datadetail') as $key => $value) {
          $data = [
            'nomorpengeluaran' => $nomorpengeluaran,
            'noreferensi' => $value['Invoice'],
            'kodesupplier' => $value['KodeSupplier'],
            'namasupplier' => $value['NamaSupplier'],
            'nilaipengeluaran' => $this->input->post('nilaipengeluaran'),
            'kodeaccount' => $value['KodeAccount'],
            'nilaialokasi' => $this->input->post('nilaipengeluaran'),
            'accountalokasi' => $value['NamaAccount'],
            'keterangan' => $value['Keterangan'],
          ];
          $this->Pengeluaranuang_model->savePengeluaranUangDetail($data);
        }

        foreach ($this->input->post('datadetail') as $key => $value) {
          $hutang = $this->Pengeluaranuang_model->DataHutang($value['Invoice']);

          $data = [
            'nilaipermohonan' => $hutang->nilaipermohonan - $value['NilaiPengeluaran'],
            'nilaipembayaran' => $hutang->nilaipembayaran + $value['NilaiPengeluaran'],
            'pemakai' => $pemakai,
            'kodecabang' => $kodecabang,
            'noreferensi' => $value['Invoice'],
          ];
          $this->Pengeluaranuang_model->updateHutang($data);
        }
      };

      $this->db->trans_complete();

      if ($this->db->trans_status() === FALSE) {
        $resultjson = array(
          'nomor' => "",
          'message' => "Data gagal disimpan, Nomor sudah pernah digunakan"
        );
        # Something went wrong.
        $this->db->trans_rollback();
      } else {
        $resultjson = array(
          'nomor' => $nomorpengeluaran,
          'message' => "Data berhasil disimpan"
        );
        # Everything is Perfect. 
        # Committing data to the database.
        $this->db->trans_commit();
      }
      echo json_encode($resultjson);
    } else {
      $this->db->trans_complete();
      $resultjson = array(
        'nomor' => "",
        'message' => "Data gagal disimpan, Data Sudah Digunakan"
      );
      # Something went wrong.
      $this->db->trans_rollback();

      echo json_encode($resultjson);
    }
  }
}
