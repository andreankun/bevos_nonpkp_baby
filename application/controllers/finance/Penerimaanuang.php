<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Penerimaanuang extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set("Asia/Jakarta");
		$this->load->model('finance/Penerimaanuang_model');
		$this->load->model('caridataaktif_model');
		$this->load->model('global/global_model');
		$this->load->library('form_validation');
		$this->load->library('session');
	}

	function CariJenisPenerimaan()
	{
		$fetch_data = $this->caridataaktif_model->make_datatables($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value'));
		$data = array();
		foreach ($fetch_data as $row) {
			$sub_array = array();
			$i = 1;
			$count = count($this->input->post('field'));
			foreach ($this->input->post('field') as $key => $value) {
				if ($i <= $count) {
					if ($i == 1) {
						$msearch = $row->$value;
						$sub_array[] = '<button class="btn btn-dark searchjenispenerimaan" data-id="' . $msearch . '"  data-dismiss="modal" style="font-size: 16px; margin-bottom: 0px; border-radius: 4px;"><i class="far fa-check-circle"></i></button> ';
						$sub_array[] = $row->$value;
					} else {
						if ($i == $count) {
							$sub_array[] = $row->$value;
						} else {
							$sub_array[] = $row->$value;
						}
					}
				}
				$i++;
			}
			$data[] = $sub_array;
		}
		$output = array(
			"draw"                    =>     intval($_POST["draw"]),
			"recordsTotal"          =>      $this->caridataaktif_model->get_all_data($this->input->post('nmtb')),
			"recordsFiltered"     =>     $this->caridataaktif_model->get_filtered_data($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value')),
			"data"                    =>     $data
		);
		echo json_encode($output);
	}

	function DataJenisPenerimaan()
	{
		$result =  $this->Penerimaanuang_model->DataJenisPenerimaan($this->input->post('kode'));
		echo json_encode($result);
	}

	function CariDepartement()
	{
		$fetch_data = $this->caridataaktif_model->make_datatables($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value'));
		$data = array();
		foreach ($fetch_data as $row) {
			$sub_array = array();
			$i = 1;
			$count = count($this->input->post('field'));
			foreach ($this->input->post('field') as $key => $value) {
				if ($i <= $count) {
					if ($i == 1) {
						$msearch = $row->$value;
						$sub_array[] = '<button class="btn btn-dark searchdepartement" data-id="' . $msearch . '"  data-dismiss="modal" style="font-size: 16px; margin-bottom: 0px; border-radius: 4px;"><i class="far fa-check-circle"></i></button> ';
						$sub_array[] = $row->$value;
					} else {
						if ($i == $count) {
							$sub_array[] = $row->$value;
						} else {
							$sub_array[] = $row->$value;
						}
					}
				}
				$i++;
			}
			$data[] = $sub_array;
		}
		$output = array(
			"draw"                    =>     intval($_POST["draw"]),
			"recordsTotal"          =>      $this->caridataaktif_model->get_all_data($this->input->post('nmtb')),
			"recordsFiltered"     =>     $this->caridataaktif_model->get_filtered_data($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value')),
			"data"                    =>     $data
		);
		echo json_encode($output);
	}

	function DataDepartement()
	{
		$result =  $this->Penerimaanuang_model->DataDepartement($this->input->post('kode'));
		echo json_encode($result);
	}

	function CariDataOngkir()
	{
		$fetch_data = $this->caridataaktif_model->make_datatables($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value'));
		$data = array();
		foreach ($fetch_data as $row) {
			$sub_array = array();
			$i = 1;
			$count = count($this->input->post('field'));
			foreach ($this->input->post('field') as $key => $value) {
				if ($i <= $count) {
					if ($i == 1) {
						$msearch = $row->$value;
						$sub_array[] = '<button class="btn btn-dark searchongkir" data-id="' . $msearch . '"  data-dismiss="modal" style="font-size: 16px; margin-bottom: 0px; border-radius: 4px;"><i class="far fa-check-circle"></i></button> ';
						$sub_array[] = $row->$value;
					} else {
						if ($i == $count) {
							$sub_array[] = $row->$value;
						} else {
							$sub_array[] = $row->$value;
						}
					}
				}
				$i++;
			}
			$data[] = $sub_array;
		}
		$output = array(
			"draw"                    =>     intval($_POST["draw"]),
			"recordsTotal"          =>      $this->caridataaktif_model->get_all_data($this->input->post('nmtb')),
			"recordsFiltered"     =>     $this->caridataaktif_model->get_filtered_data($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value')),
			"data"                    =>     $data
		);
		echo json_encode($output);
	}

	function DataOngkir()
	{
		$result = $this->Penerimaanuang_model->DataOngkir($this->input->post('nomor'));
		echo json_encode($result);
	}

	function CariDokumenLainLain()
	{
		$fetch_data = $this->caridataaktif_model->make_datatables($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value'));
		$data = array();
		foreach ($fetch_data as $row) {
			$sub_array = array();
			$i = 1;
			$count = count($this->input->post('field'));
			foreach ($this->input->post('field') as $key => $value) {
				if ($i <= $count) {
					if ($i == 1) {
						$msearch = $row->$value;
						$sub_array[] = '<button class="btn btn-dark searchdoclain" data-id="' . $msearch . '"  data-dismiss="modal" style="font-size: 16px; margin-bottom: 0px; border-radius: 4px;"><i class="far fa-check-circle"></i></button> ';
						$sub_array[] = $row->$value;
					} else {
						if ($i == $count) {
							$sub_array[] = $row->$value;
						} else {
							$sub_array[] = $row->$value;
						}
					}
				}
				$i++;
			}
			$data[] = $sub_array;
		}
		$output = array(
			"draw"                    =>     intval($_POST["draw"]),
			"recordsTotal"          =>      $this->caridataaktif_model->get_all_data($this->input->post('nmtb')),
			"recordsFiltered"     =>     $this->caridataaktif_model->get_filtered_data($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value')),
			"data"                    =>     $data
		);
		echo json_encode($output);
	}

	function CariAccount()
	{
		$fetch_data = $this->caridataaktif_model->make_datatables($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value'));
		$data = array();
		foreach ($fetch_data as $row) {
			$sub_array = array();
			$i = 1;
			$count = count($this->input->post('field'));
			foreach ($this->input->post('field') as $key => $value) {
				if ($i <= $count) {
					if ($i == 1) {
						$msearch = $row->$value;
						$sub_array[] = '<button class="btn btn-dark searchaccount" data-id="' . $msearch . '"  data-dismiss="modal" style="font-size: 16px; margin-bottom: 0px; border-radius: 4px;"><i class="far fa-check-circle"></i></button> ';
						$sub_array[] = $row->$value;
					} else {
						if ($i == $count) {
							$sub_array[] = $row->$value;
						} else {
							$sub_array[] = $row->$value;
						}
					}
				}
				$i++;
			}
			$data[] = $sub_array;
		}
		$output = array(
			"draw"                    =>     intval($_POST["draw"]),
			"recordsTotal"          =>      $this->caridataaktif_model->get_all_data($this->input->post('nmtb')),
			"recordsFiltered"     =>     $this->caridataaktif_model->get_filtered_data($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value')),
			"data"                    =>     $data
		);
		echo json_encode($output);
	}

	function DataAccount()
	{
		$result =  $this->Penerimaanuang_model->DataAccount($this->input->post('noaccount'));
		echo json_encode($result);
	}

	function DataAccountLain()
	{
		$result =  $this->Penerimaanuang_model->DataAccountlain($this->input->post('nomor'));
		echo json_encode($result);
	}

	function SisaDeposit()
	{
		$result =  $this->Penerimaanuang_model->SisaDeposit($this->input->post('nomor'));
		echo json_encode($result);
	}


	function CariDataCustomer()
	{
		$fetch_data = $this->caridataaktif_model->make_datatables($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value'));
		$data = array();
		foreach ($fetch_data as $row) {
			$sub_array = array();
			$i = 1;
			$count = count($this->input->post('field'));
			foreach ($this->input->post('field') as $key => $value) {
				if ($i <= $count) {
					if ($i == 1) {
						$msearch = $row->$value;
						$sub_array[] = '<button class="btn btn-dark searchcustomer" data-id="' . $msearch . '"  data-dismiss="modal" style="font-size: 16px; margin-bottom: 0px; border-radius: 4px;"><i class="far fa-check-circle"></i></button> ';
						$sub_array[] = $row->$value;
					} else {
						if ($i == $count) {
							$sub_array[] = $row->$value;
						} else {
							$sub_array[] = $row->$value;
						}
					}
				}
				$i++;
			}
			$data[] = $sub_array;
		}
		$output = array(
			"draw"                    =>     intval($_POST["draw"]),
			"recordsTotal"          =>      $this->caridataaktif_model->get_all_data($this->input->post('nmtb')),
			"recordsFiltered"     =>     $this->caridataaktif_model->get_filtered_data($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value')),
			"data"                    =>     $data
		);
		echo json_encode($output);
	}
	function CariDataSO()
	{
		$fetch_data = $this->caridataaktif_model->make_datatables($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value'));
		$data = array();
		foreach ($fetch_data as $row) {
			$sub_array = array();
			$i = 1;
			$count = count($this->input->post('field'));
			foreach ($this->input->post('field') as $key => $value) {
				if ($i <= $count) {
					if ($i == 1) {
						$msearch = $row->$value;
						$sub_array[] = '<button class="btn btn-dark searchso" data-id="' . $msearch . '"  data-dismiss="modal" style="font-size: 16px; margin-bottom: 0px; border-radius: 4px;"><i class="far fa-check-circle"></i></button> ';
						$sub_array[] = $row->$value;
					} else {
						if ($i == $count) {
							$sub_array[] = $row->$value;
						} else {
							$sub_array[] = $row->$value;
						}
					}
				}
				$i++;
			}
			$data[] = $sub_array;
		}
		$output = array(
			"draw"                    =>     intval($_POST["draw"]),
			"recordsTotal"          =>      $this->caridataaktif_model->get_all_data($this->input->post('nmtb')),
			"recordsFiltered"     =>     $this->caridataaktif_model->get_filtered_data($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value')),
			"data"                    =>     $data
		);
		echo json_encode($output);
	}

	function DataSO()
	{
		$result = $this->Penerimaanuang_model->DataSO($this->input->post('nomor'));
		echo json_encode($result);
	}

	function DataRekening()
	{
		$where = array(
			"aktif" => true,
			"nomor" => $this->input->post('nomor')
		);
		$result = $this->global_model->GetDataGlobal('glbm_customer', $where);
		echo json_encode($result);
	}
	function DataCoaDepositCabang()
	{
		$where = array(
			"aktif" => true,
			"kode" => $this->input->post('kodecabang')
		);
		$result = $this->global_model->GetDataGlobal('glbm_cabang', $where);
		echo json_encode($result);
	}



	function DataCustomer()
	{
		$where = array(
			"aktif" => true,
			"nomor" => $this->input->post('nomor')
		);
		$result = $this->global_model->GetDataGlobal('glbm_customer', $where);
		echo json_encode($result);
	}

	function CariDataPiutang()
	{
		$fetch_data = $this->caridataaktif_model->make_datatables($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value'));
		$data = array();
		foreach ($fetch_data as $row) {
			$sub_array = array();
			$i = 1;
			$count = count($this->input->post('field'));
			foreach ($this->input->post('field') as $key => $value) {
				if ($i <= $count) {
					if ($i == 1) {
						$msearch = $row->$value;
						$sub_array[] = '<button class="btn btn-dark searchpiutang" data-id="' . $msearch . '"  data-dismiss="modal" style="font-size: 16px; margin-bottom: 0px; border-radius: 4px;"><i class="far fa-check-circle"></i></button> ';
						$sub_array[] = $row->$value;
					} else {
						if ($i == $count) {
							$sub_array[] = $row->$value;
						} else {
							$sub_array[] = $row->$value;
						}
					}
				}
				$i++;
			}
			$data[] = $sub_array;
		}
		$output = array(
			"draw"                    =>     intval($_POST["draw"]),
			"recordsTotal"          =>      $this->caridataaktif_model->get_all_data($this->input->post('nmtb')),
			"recordsFiltered"     =>     $this->caridataaktif_model->get_filtered_data($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value')),
			"data"                    =>     $data
		);
		echo json_encode($output);
	}

	public function DataPiutang()
	{
		$result =  $this->Penerimaanuang_model->DataPiutang($this->input->post('nomor'));
		echo json_encode($result);
	}

	function CariDataPenerimaanUang()
	{
		$fetch_data = $this->caridataaktif_model->make_datatables($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value'));
		$data = array();
		foreach ($fetch_data as $row) {
			$sub_array = array();
			$i = 1;
			$count = count($this->input->post('field'));
			foreach ($this->input->post('field') as $key => $value) {
				if ($i <= $count) {
					if ($i == 1) {
						$msearch = $row->$value;
						$sub_array[] = '<button class="btn btn-dark searchpenerimaanuang" data-id="' . $msearch . '"  data-dismiss="modal" style="font-size: 16px; margin-bottom: 0px; border-radius: 4px;"><i class="far fa-check-circle"></i></button> ';
						$sub_array[] = $row->$value;
					} else {
						if ($i == $count) {
							$sub_array[] = $row->$value;
						} else {
							$sub_array[] = $row->$value;
						}
					}
				}
				$i++;
			}
			$data[] = $sub_array;
		}
		$output = array(
			"draw"                    =>     intval($_POST["draw"]),
			"recordsTotal"          =>      $this->caridataaktif_model->get_all_data($this->input->post('nmtb')),
			"recordsFiltered"     =>     $this->caridataaktif_model->get_filtered_data($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value')),
			"data"                    =>     $data
		);
		echo json_encode($output);
	}

	function DataPenerimaanUang()
	{
		$result =  $this->Penerimaanuang_model->DataPenerimaanUang($this->input->post('nomor'));
		echo json_encode($result);
	}

	function DataPenerimaanUangDetail()
	{
		$result =  $this->Penerimaanuang_model->DataPenerimaanUangDetail($this->input->post('nomor'));
		echo json_encode($result);
	}

	public function ClearChr($param)
	{
		return $result = str_replace(array('\'', '"', ',', ';', '<', '>', '_', '-', ' ', '.', '!', '?', '/', '=', '+', ']', '[', '{', '}', '@', '#', '$', '%', '^', '&', "*", '(', ')'), '', $param);
	}

	function Save()
	{
		$nomor = "";
		$pemakai = $this->session->userdata('myusername');
		$tanggal = $this->input->post('tanggal');
		$kodedepartement = $this->input->post('kodedepartement');
		$keterangan = $this->input->post('keterangan');
		$namadepartement = $this->input->post('namadepartement');
		$kodepenerimaan = $this->input->post('kodepenerimaan');
		$namapenerimaan = $this->input->post('namapenerimaan');
		$kodecabang = $this->input->post('kodecabang');
		$datadetail = $this->input->post('datadetail');
		$errorvalidasi = false;
		// $thn = date('Y-m-d', strtotime($tanggal));

		$ambilnomor = "P" . $kodecabang . '-' . $tanggal;
		$get["np"] = $this->Penerimaanuang_model->GetMaxNomor($ambilnomor);
		if (!$get["np"]) {
			$nomor = $ambilnomor . "-" . "00001";
			// print_r($nomor);
		} else {
			$lastNomor = $get['np']->nomor;
			$lastNoUrut = substr($lastNomor, 14, 14);

			// nomor urut ditambah 1
			$nextNoUrut = $lastNoUrut + 1;
			$nomor = $ambilnomor . "-" . sprintf('%05s', $nextNoUrut);
			// print_r($nomor);
		}

		if (empty($nomor) || $nomor == "") {
			$resultjson = array(
				'nomor' => "",
				'message' => "Data gagal disimpan, Error Generate Nomor."
			);
			$errorvalidasi = TRUE;
			echo json_encode($resultjson);
			return FALSE;
		}
		if (empty($datadetail)) {
			$resultjson = array(
				'nomor' => "",
				'message' => "Data gagal disimpan, Data Detail Kosong."
			);
			$errorvalidasi = TRUE;
			echo json_encode($resultjson);
			return FALSE;
		}
		foreach ($datadetail as $key => $value) {
			switch ($kodepenerimaan) {
				case 'TUF01': //Deposit Customer                        
					$where = array(
						'nomor' => $value['NoReferensi'],
						'aktif' => false
					);
					$getdata = $this->global_model->GetDataGlobal("glbm_customer", $where);
					if (!empty($getdata)) {
						$resultjson = array(
							'nomor' => "",
							'message' => "Data gagal disimpan, Data Customer " . $value['NoReferensi'] . " Tidak Aktif."
						);
						$errorvalidasi = TRUE;
						echo json_encode($resultjson);
						return FALSE;
					}
					break;

				case 'TUF02':
					$where = array(
						'nomor' => $value['DocInvoice'],
						'batal' => true
					);
					$getdata = $this->global_model->GetDataGlobal("srvt_salesorder", $where);
					if (!empty($getdata)) {
						$resultjson = array(
							'nomor' => "",
							'message' => "Data gagal disimpan, No Sales Order " . $value['DocInvoice'] . " Batal."
						);
						$errorvalidasi = TRUE;
						echo json_encode($resultjson);
						return FALSE;
					}
					break;
				case 'TUF03':
					$where = array(
						'nomor' => $value['DocInvoice'],
						'batal' => true
					);
					$getdata = $this->global_model->GetDataGlobal("srvt_piutang", $where);
					if (!empty($getdata)) {
						$resultjson = array(
							'nomor' => "",
							'message' => "Data gagal disimpan, No Invoice " . $value['DocInvoice'] . " Batal."
						);
						$errorvalidasi = TRUE;
						echo json_encode($resultjson);
						return FALSE;
					}
					break;

				case 'TUF0$':
					$where = array(
						'nomor' => $value['DocInvoice'],
						'batal' => true
					);
					$getdata = $this->global_model->GetDataGlobal("srvt_piutang", $where);
					if (!empty($getdata)) {
						$resultjson = array(
							'nomor' => "",
							'message' => "Data gagal disimpan, No Invoice " . $value['DocInvoice'] . " Batal."
						);
						$errorvalidasi = TRUE;
						echo json_encode($resultjson);
						return FALSE;
					}
					break;
			}
		}
		if ($errorvalidasi == FALSE) {

			$this->db->trans_start(); # Starting Transaction
			$this->db->trans_strict(FALSE);

			$data = array(
				'nomor' => $nomor,
				'tanggal' => $tanggal,
				'jenispenerimaan' => $kodepenerimaan,
				'kodedepartement' => $kodedepartement,
				'namadepartement' => $namadepartement,
				'keterangan' => $keterangan,
				'tglsimpan' => date("Y-m-d H:i:s"),
				'pemakai' => $pemakai,
				'kodecabang' => $kodecabang,
			);

			$this->Penerimaanuang_model->savePenerimaanUang($data);

			foreach ($datadetail as $key => $value) {
				$statusdeposit = ($value['Deposit'] == "YA") ? true : false;
				$datadetail = array(
					'nomorpenerimaan' => $nomor,
					'noreferensi' => $value['DocInvoice'],
					'nocustomer' => $value['NoReferensi'],
					'namacustomer' => $value['Referensi'],
					'totaltransaksi' => floatval(str_replace(",", "", $value['TotalTransaksi'])),
					'nilaipenerimaan' => floatval(str_replace(",", "", $value['NilaiPenerimaan'])),
					'kodeaccount' => $value['Account'],
					'nilaialokasi' => floatval(str_replace(",", "", $value['NilaiAlokasi'])),
					'nilaipembulatan' => floatval(str_replace(",", "", $value['NilaiPembulatan'])),
					'accountalokasi' => $value['AccountAlokasi'],
					'deposit' => $statusdeposit,
					'memo' => $value['Memo'],
				);
				$this->Penerimaanuang_model->savePenerimaanUangDetail($datadetail);
				switch ($kodepenerimaan) {
					case 'TUF01': //Deposit Customer                        
						$data = array(
							"nomorcustomer" => $value['NoReferensi'],
							"tanggal" => $tanggal,
							"kodestatus" => 1,
							"noreferensi" => $nomor,
							"keterangan" => "Penerimaan Uang Masuk Untuk Deposit Customer",
							"debit" => floatval(str_replace(",", "", $value['NilaiPenerimaan'])),
							"kredit" => 0,
						);
						$this->global_model->InsertDataGlobal("trnt_historydepositcustomer", $data);
						break;

					case 'TUF02':
						$where = array(
							'nomor' => $value['DocInvoice'],
							'batal' => false
						);
						$order = $this->global_model->GetDataGlobal("srvt_salesorder", $where);
						if (!empty($order)) {
							$dataupdate = array(
								'nilaiuangmuka' => $order[0]->nilaiuangmuka + floatval(str_replace(",", "", $value['NilaiPenerimaan'])) + floatval(str_replace(",", "", $value['NilaiAlokasi'])) + floatval(str_replace(",", "", $value['NilaiPembulatan'])),
							);
							$where = array(
								'nomor' => $value['DocInvoice'],
								'batal' => false
							);
							$this->global_model->UpdateDataGlobal("srvt_salesorder", $dataupdate, $where);
						}
						if ($value['NilaiAlokasi'] != "" || $value['NilaiAlokasi'] != 0) {
							if ($value['Deposit'] == "YA") {
								$data = array(
									"nomorcustomer" => $value['NoReferensi'],
									"tanggal" => $tanggal,
									"kodestatus" => 2,
									"noreferensi" => $nomor,
									"noreferensi2" => $value['DocInvoice'],
									"keterangan" => "Pembayaran Uang Muka Customer",
									"debit" => 0,
									"kredit" => floatval(str_replace(",", "", $value['NilaiAlokasi'])),
								);
								$this->global_model->InsertDataGlobal("trnt_historydepositcustomer", $data);
							} else {
								$data = array(
									"nomorcustomer" => $value['NoReferensi'],
									"tanggal" => $tanggal,
									"kodestatus" => 1,
									"noreferensi" => $nomor,
									"noreferensi2" => $value['DocInvoice'],
									"keterangan" => "Penerimaan Uang Masuk Untuk Deposit Customer",
									"debit" => floatval(str_replace(",", "", $value['NilaiAlokasi'])) * -1,
									"kredit" => 0
								);
								$this->global_model->InsertDataGlobal("trnt_historydepositcustomer", $data);
							}
						}
						break;
					case 'TUF03':
						$where = array(
							'nomor' => $value['DocInvoice'],
							'batal' => false
						);
						$piutang = $this->global_model->GetDataGlobal("srvt_piutang", $where);
						if (!empty($piutang)) {
							$dataupdate = array(
								'nilaipenerimaan' => $piutang[0]->nilaipenerimaan + floatval(str_replace(",", "", $value['NilaiPenerimaan'])) + floatval(str_replace(",", "", $value['NilaiAlokasi'])) + floatval(str_replace(",", "", $value['NilaiPembulatan'])),
							);
							$where = array(
								'nomor' => $value['DocInvoice'],
								'batal' => false
							);
							$this->global_model->UpdateDataGlobal("srvt_piutang", $dataupdate, $where);
						}
						if ($value['NilaiAlokasi'] != "" || $value['NilaiAlokasi'] != 0) {
							if ($value['Deposit'] == "YA") {
								$data = array(
									"nomorcustomer" => $value['NoReferensi'],
									"tanggal" => $tanggal,
									"kodestatus" => 3,
									"noreferensi" => $nomor,
									"noreferensi2" => $value['DocInvoice'],
									"keterangan" => "Pembayaran Pelunasan Customer",
									"debit" => 0,
									"kredit" => floatval(str_replace(",", "", $value['NilaiAlokasi'])),
								);
								$this->global_model->InsertDataGlobal("trnt_historydepositcustomer", $data);
							} else {
								$data = array(
									"nomorcustomer" => $value['NoReferensi'],
									"tanggal" => $tanggal,
									"kodestatus" => 1,
									"noreferensi" => $nomor,
									"noreferensi2" => $value['DocInvoice'],
									"keterangan" => "Penerimaan Uang Masuk Untuk Deposit Customer",
									"debit" => floatval(str_replace(",", "", $value['NilaiAlokasi'])) * -1,
									"kredit" => 0
								);
								$this->global_model->InsertDataGlobal("trnt_historydepositcustomer", $data);
							}
						}
						break;
					case 'TUF04':
						$where = array(
							'nomor' => $value['DocInvoice'],
							'batal' => false
						);
						$piutang = $this->global_model->GetDataGlobal("srvt_piutang", $where);
						if (!empty($piutang)) {
							$dataupdate = array(
								'nilaipenerimaan' => $piutang[0]->nilaipenerimaan + floatval(str_replace(",", "", $value['NilaiPenerimaan'])) + floatval(str_replace(",", "", $value['NilaiAlokasi'])) + floatval(str_replace(",", "", $value['NilaiPembulatan'])),
							);
							$where = array(
								'nomor' => $value['DocInvoice'],
								'batal' => false
							);
							$this->global_model->UpdateDataGlobal("srvt_piutang", $dataupdate, $where);
						}
						if ($value['NilaiAlokasi'] != "" || $value['NilaiAlokasi'] != 0) {
							if ($value['Deposit'] == "YA") {
								$data = array(
									"nomorcustomer" => $value['NoReferensi'],
									"tanggal" => $tanggal,
									"kodestatus" => 3,
									"noreferensi" => $nomor,
									"noreferensi2" => $value['DocInvoice'],
									"keterangan" => "Pembayaran Pelunasan Customer",
									"debit" => 0,
									"kredit" => floatval(str_replace(",", "", $value['NilaiAlokasi'])),
								);
								$this->global_model->InsertDataGlobal("trnt_historydepositcustomer", $data);
							} else {
								$data = array(
									"nomorcustomer" => $value['NoReferensi'],
									"tanggal" => $tanggal,
									"kodestatus" => 1,
									"noreferensi" => $nomor,
									"noreferensi2" => $value['DocInvoice'],
									"keterangan" => "Penerimaan Uang Masuk Untuk Deposit Customer",
									"debit" => floatval(str_replace(",", "", $value['NilaiAlokasi'])) * -1,
									"kredit" => 0
								);
								$this->global_model->InsertDataGlobal("trnt_historydepositcustomer", $data);
							}
						}
						break;
				}
			}

			// die();
			$this->db->trans_complete();

			if ($this->db->trans_status() === FALSE) {
				$resultjson = array(
					'nomor' => "",
					'message' => "Data gagal disimpan, Nomor sudah pernah digunakan"
				);
				# Something went wrong.
				$this->db->trans_rollback();
			} else {
				$resultjson = array(
					'nomor' => $nomor,
					'message' => "Data berhasil disimpan"
				);
				# Everything is Perfect. 
				# Committing data to the database.
				$this->db->trans_commit();
			}
			echo json_encode($resultjson);
		} else {
			$this->db->trans_complete();
			$resultjson = array(
				'nomor' => "",
				'message' => "Data gagal disimpan, Data Sudah Digunakan"
			);
			# Something went wrong.
			$this->db->trans_rollback();

			echo json_encode($resultjson);
		}
	}

	function Cancel()
	{
		$pemakai = $this->session->userdata('myusername');

		$nomor = $this->input->post('nomor');
		$tanggal = $this->input->post('tanggal');
		$kodepenerimaan = $this->input->post('kodepenerimaan');
		$datadetail = $this->input->post('datadetail');
		$validasicancel = FALSE;


		if (empty($datadetail)) {
			$resultjson = array(
				'error' => true,
				'message' => "Data gagal dibatalkan, Data Detail Tidak ada"
			);
			$validasicancel = TRUE;
			echo json_encode($resultjson);
			return FALSE;
		}
		foreach ($datadetail as $key => $value) {
			switch ($kodepenerimaan) {
				case 'TUF01': //Deposit Customer                        
					$where = array(
						'nomor' => $value['NoReferensi'],
						'aktif' => false
					);
					$getdata = $this->global_model->GetDataGlobal("glbm_customer", $where);
					if (!empty($getdata)) {
						$resultjson = array(
							'nomor' => "",
							'message' => "Data gagal disimpan, Data Customer " . $value['NoReferensi'] . " Tidak Aktif."
						);
						$errorvalidasi = TRUE;
						echo json_encode($resultjson);
						return FALSE;
					}
					break;

				case 'TUF02':
					$where = array(
						'nomor' => $value['DocInvoice'],
						'batal' => true
					);
					$getdata = $this->global_model->GetDataGlobal("srvt_salesorder", $where);
					if (!empty($getdata)) {
						$resultjson = array(
							'nomor' => "",
							'message' => "Data gagal disimpan, No Sales Order " . $value['DocInvoice'] . " Batal."
						);
						$errorvalidasi = TRUE;
						echo json_encode($resultjson);
						return FALSE;
					}
					break;
				case 'TUF03':
					$where = array(
						'nomor' => $value['DocInvoice'],
						'batal' => true
					);
					$getdata = $this->global_model->GetDataGlobal("srvt_piutang", $where);
					if (!empty($getdata)) {
						$resultjson = array(
							'nomor' => "",
							'message' => "Data gagal disimpan, No Invoice " . $value['DocInvoice'] . " Batal."
						);
						$errorvalidasi = TRUE;
						echo json_encode($resultjson);
						return FALSE;
					}
					break;
			}
		}

		if ($validasicancel == FALSE) {

			$this->db->trans_start(); # Starting Transaction
			$this->db->trans_strict(FALSE);

			foreach ($datadetail as $key => $value) {

				switch ($kodepenerimaan) {
					case 'TUF01': //Deposit Customer                        
						$data = array(
							"nomorcustomer" => $value['NoReferensi'],
							"tanggal" => date("Y-m-d H:i:s"),
							"kodestatus" => 51,
							"noreferensi" => $nomor,
							"keterangan" => "Batal Penerimaan Uang Masuk Untuk Deposit Customer",
							"debit" => 0,
							"kredit" => floatval(str_replace(",", "", $value['NilaiPenerimaan'])),
						);
						$this->global_model->InsertDataGlobal("trnt_historydepositcustomer", $data);
						break;

					case 'TUF02':
						$where = array(
							'nomor' => $value['DocInvoice'],
							'batal' => false
						);
						$order = $this->global_model->GetDataGlobal("srvt_salesorder", $where);
						if (!empty($order)) {
							$dataupdate = array(
								'nilaiuangmuka' => $order[0]->nilaiuangmuka - (floatval(str_replace(",", "", $value['NilaiPenerimaan'])) + floatval(str_replace(",", "", $value['NilaiAlokasi'])) + floatval(str_replace(",", "", $value['NilaiPembulatan']))),
							);
							$where = array(
								'nomor' => $value['DocInvoice'],
								'batal' => false
							);
							$this->global_model->UpdateDataGlobal("srvt_salesorder", $dataupdate, $where);
						}
						if ($value['NilaiAlokasi'] != "" || $value['NilaiAlokasi'] != 0) {
							if ($value['Deposit'] == "YA") {
								$data = array(
									"nomorcustomer" => $value['NoReferensi'],
									"tanggal" => date("Y-m-d H:i:s"),
									"kodestatus" => 52,
									"noreferensi" => $nomor,
									"noreferensi2" => $value['DocInvoice'],
									"keterangan" => "Batal Pembayaran Uang Muka Customer",
									"debit" => floatval(str_replace(",", "", $value['NilaiAlokasi'])),
									"kredit" => 0,
								);
								$this->global_model->InsertDataGlobal("trnt_historydepositcustomer", $data);
							} else {
								$data = array(
									"nomorcustomer" => $value['NoReferensi'],
									"tanggal" => date("Y-m-d H:i:s"),
									"kodestatus" => 51,
									"noreferensi" => $nomor,
									"noreferensi2" => $value['DocInvoice'],
									"keterangan" => "Batal Penerimaan Uang Masuk Untuk Deposit Customer",
									"debit" => 0,
									"kredit" => floatval(str_replace(",", "", $value['NilaiAlokasi'])) * -1
								);
								$this->global_model->InsertDataGlobal("trnt_historydepositcustomer", $data);
							}
						}
						break;
					case 'TUF03':
						$where = array(
							'nomor' => $value['DocInvoice'],
							'batal' => false
						);
						$piutang = $this->global_model->GetDataGlobal("srvt_piutang", $where);
						if (!empty($piutang)) {
							$dataupdate = array(
								'nilaipenerimaan' => $piutang[0]->nilaipenerimaan - (floatval(str_replace(",", "", $value['NilaiPenerimaan'])) + floatval(str_replace(",", "", $value['NilaiAlokasi'])) + floatval(str_replace(",", "", $value['NilaiPembulatan']))),
							);
							$where = array(
								'nomor' => $value['DocInvoice'],
								'batal' => false
							);
							$this->global_model->UpdateDataGlobal("srvt_piutang", $dataupdate, $where);
						}

						if ($value['NilaiAlokasi'] != "" || $value['NilaiAlokasi'] != 0) {
							if ($value['Deposit'] == "YA") {
								$data = array(
									"nomorcustomer" => $value['NoReferensi'],
									"tanggal" => date("Y-m-d H:i:s"),
									"kodestatus" => 53,
									"noreferensi" => $nomor,
									"noreferensi2" => $value['DocInvoice'],
									"keterangan" => "Batal Pembayaran Pelunasan Customer",
									"debit" => floatval(str_replace(",", "", $value['NilaiAlokasi'])),
									"kredit" => 0,
								);
								$this->global_model->InsertDataGlobal("trnt_historydepositcustomer", $data);
							} else {
								$data = array(
									"nomorcustomer" => $value['NoReferensi'],
									"tanggal" => date("Y-m-d H:i:s"),
									"kodestatus" => 51,
									"noreferensi" => $nomor,
									"noreferensi2" => $value['DocInvoice'],
									"keterangan" => "Batal Penerimaan Uang Masuk Untuk Deposit Customer",
									"debit" => 0,
									"kredit" => floatval(str_replace(",", "", $value['NilaiAlokasi'])) * -1
								);
								$this->global_model->InsertDataGlobal("trnt_historydepositcustomer", $data);
							}
						}
						break;
				}
			}

			$data = array(
				'keteranganbatal' => $this->input->post('keterangan'),
				'batal' => true,
				'tglbatal' => $tanggal,
				'userbatal' => $pemakai
			);
			$this->Penerimaanuang_model->CancelTransaksi($data, $nomor);

			$this->db->trans_complete();

			if ($this->db->trans_status() === FALSE) {
				$resultjson = array(
					'error' => true,
					'message' => "Data gagal disimpan, Silahkan Hub. Vendor"
				);
				# Something went wrong.
				$this->db->trans_rollback();
			} else {
				$resultjson = array(
					'error' => false,
					'message' => "Data berhasil dibatalkan"
				);
				# Everything is Perfect. 
				# Committing data to the database.
				$this->db->trans_commit();
			}
			echo json_encode($resultjson);
		}
	}

	function Update()
	{
		$nomor = $this->input->post('nomor');

		$this->db->trans_start(); # Starting Transaction
		$this->db->trans_strict(FALSE);

		$data = [
			'nomor' => $nomor,
			'keterangan' => $this->input->post('keterangan')
			// 'kodecabang' => $kodecabang,
		];

		$this->Penerimaanuang_model->UpdateData($data, $nomor);

		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE) {
			$resultjson = array(
				'nomor' => "",
				'message' => "Data gagal diperbarui, nomor '<b style='color: red'>" . $nomor . "</b>' sudah pernah digunakan."
			);
			# Something went wrong.
			$this->db->trans_rollback();
		} else {
			$resultjson = array(
				'nomor' => $nomor,
				'message' => "Data berhasil diperbarui."
			);
			# Everything is Perfect. 
			# Committing data to the database.
			$this->db->trans_commit();
		}
		echo json_encode($resultjson);
	}
}
