<?php

defined('BASEPATH') or exit('No direct script access allowed');

class FreeBarang extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('masterdata/Freebarang_model');
		$this->load->model('Caridataaktif_model');
		$this->load->library('form_validation');
		$this->load->library('session');
	}

	function CariDataFreeBarang()
	{
		$fetch_data = $this->Caridataaktif_model->make_datatables($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value'));
		$data = array();
		foreach ($fetch_data as $row) {
			$sub_array = array();
			$i = 1;
			$count = count($this->input->post('field'));
			foreach ($this->input->post('field') as $key => $value) {
				if ($i <= $count) {
					if ($i == 1) {
						$msearch = $row->$value;
						$sub_array[] = '<button class="btn btn-dark searchfree" data-dismiss="modal" style="margin: 0px;" data-id="' . $msearch . '"><i class="far fa-check-circle"></i></button>';
						$sub_array[] = $row->$value;
					} else {
						if ($i == $count) {
							$sub_array[] = $row->$value;
						} else {
							$sub_array[] = $row->$value;
						}
					}
					// $sub_array[] = $row->$value;
				}
				$i++;
			}
			$data[] = $sub_array;
		}
		$output = array(
			"draw"                    =>     intval($_POST["draw"]),
			"recordsTotal"          =>      $this->Caridataaktif_model->get_all_data($this->input->post('nmtb')),
			"recordsFiltered"     =>     $this->Caridataaktif_model->get_filtered_data($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value')),
			"data"                    =>     $data
		);
		echo json_encode($output);
	}

	function DataFreeBarang()
	{
		$result = $this->Freebarang_model->DataFreeBarang($this->input->post('kode'));

		echo json_encode($result);
	}

	function DataFreeBarangDetail()
	{
		$result = $this->Freebarang_model->DataFreeBarangdetail($this->input->post('kode'));
		// print_r($result);
		// die();
		echo json_encode($result);
	}
	function DataFreeBarangHeader(){
		$result = $this->Freebarang_model->DataFreeBarangHeader($this->input->post('kode'));
		// print_r($result);
		// die();
		echo json_encode($result);
	}

	function CariDataBarang()
	{
		$fetch_data = $this->Caridataaktif_model->make_datatables($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value'));
		$data = array();
		foreach ($fetch_data as $row) {
			$sub_array = array();
			$i = 1;
			$count = count($this->input->post('field'));
			foreach ($this->input->post('field') as $key => $value) {
				if ($i <= $count) {
					if ($i == 1) {
						$msearch = $row->$value;
						$sub_array[] = '<button class="btn btn-dark seacrhb" data-dismiss="modal" style="margin: 0px;" data-id="' . $msearch . '"><i class="far fa-check-circle"></i></button>';
						$sub_array[] = $row->$value;
					} else {
						if ($i == $count) {
							$sub_array[] = $row->$value;
						} else {
							$sub_array[] = $row->$value;
						}
					}
					// $sub_array[] = $row->$value;
				}
				$i++;
			}
			$data[] = $sub_array;
		}
		$output = array(
			"draw"                    =>     intval($_POST["draw"]),
			"recordsTotal"          =>      $this->Caridataaktif_model->get_all_data($this->input->post('nmtb')),
			"recordsFiltered"     =>     $this->Caridataaktif_model->get_filtered_data($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value')),
			"data"                    =>     $data
		);
		echo json_encode($output);
	}

	function DataBarang()
	{
		$result = $this->Freebarang_model->DataBarang($this->input->post('kode'));
		echo  json_encode($result);
	}

	public function ClearChr($param)
	{
		return $result = str_replace(array('\'', '"', ',', ';', '<', '>', '_', '-', ' ', '.', '!', '?', '/', '=', '+', ']', '[', '{', '}', '@', '#', '$', '%', '^', '&', "*", '(', ')'), '', $param);
	}

	function Save()
	{
		$kodebarang = $this->input->post('kodebarang');
		$kodepromo = $this->input->post('kode');
		$kodecabang = $this->input->post('kodecabang');
		$pemakai = $this->session->userdata('myusername');
		$cekkode = $this->Freebarang_model->CekFreeBarang($kodebarang, $kodecabang);

		$get["promo"] = $this->Freebarang_model->GetMaxNomor("PRM");
		if (!$get["promo"]) {
			$kode = "PRM000001";
		} else {
			$lastnomor = $get["promo"]->kode;
			$lastNoUrut = substr($lastnomor, 3, 9);

			$nextNourut = $lastNoUrut + 1;
			$kode = "PRM" . sprintf('%09s', $nextNourut);
			// print_r($kode);
			// die();
		}

		$data = [
			'kode' => $kode,
			'kodecabang' => $this->input->post('kodecabang'),
			'qty' => $this->input->post('qty'),
			'tglsimpan' => date('Y-m-d H:i:s'),
			'pemakai' => $this->session->userdata('myusername'),
		];
		// print_r($data);
		// die();
		$this->Freebarang_model->SaveData($data);

		foreach ($this->input->post('dataheader') as $key => $value) {
			$data = [
				'kode' => $kode,
				'kodebarang' => $value['KodeBrg'],
				'namabarang' => $value['NamaBrg'],
				// 'qty' => $value['Qty'],
				'kodecabang' => $kodecabang,
				'tglsimpan' => date('Y-m-d H:i:s'),
				'pemakai' => $pemakai
			];
			// print_r($data);
			// die();
			$this->Freebarang_model->SaveDataDetail2($data);
		}

		foreach ($this->input->post('datadetail') as $key => $value) {
			$cekdetail = $this->Freebarang_model->CekDatadetail($kodebarang, $kodecabang);
			if (empty($cekdetail)) {
				$datadetail = [
					'kode' => $kode,
					'kodebarang' => $value['Kode'],
					'namabarang' => $value['Nama'],
					'qty' => $value['Qty'],
					'kodecabang' => $this->input->post('kodecabang'),
					'tglsimpan' => date('Y-m-d H:i:s'),
				];
				$this->Freebarang_model->SaveDataDetail($datadetail);
			}
		}

		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$resultjson = array(
				'kode' => "",
				'message' => "Data gagal disimpan, Kode '<b style='color: red'>" . $kode . "</b>' dengan Cabang '<b style='color: red'>" . $kodecabang . "</b>' sudah pernah digunakan."
			);
			# Something went wrong.
			$this->db->trans_rollback();
		} else {
			$resultjson = array(
				'kode' => $kode,
				'message' => "Data berhasil disimpan."
			);
			# Everything is Perfect. 
			# Committing data to the database.
			$this->db->trans_commit();
		}
		echo json_encode($resultjson);
	}

	function Update()
	{
		$kode = $this->input->post('kode');
		// print_r($kode);
		// die();
		$kodecabang = $this->input->post('kodecabang');
		$pemakai = $this->session->userdata('myusername');
		$cekkode = $this->Freebarang_model->CekFreeBarang($kode, $kodecabang);

		

		if (!empty($this->input->post('datadetail'))) {
			$this->Freebarang_model->DelDetail($kode, $kodecabang);
			foreach ($this->input->post('datadetail') as $key => $value) {
				$datadetail = [
					'kode' => $kode,
					'kodebarang' => $value['Kode'],
					'namabarang' => $value['Nama'],
					'qty' => $value['Qty'],
					'tglsimpan' => date('Y-m-d H:i:s'),
					'pemakai' => $pemakai,
					'kodecabang' => $kodecabang
				];
				$this->Freebarang_model->SaveDataDetail($datadetail);
			}
		}

		if(!empty($this->input->post('dataheader'))){
			$this->Freebarang_model->DelHeader($kode, $kodecabang);
			foreach($this->input->post('dataheader') as $key => $val){
				$dataheader = [
					'kode' => $kode,
					'kodebarang' => $val['KodeBrg'],
					'namabarang' => $val['NamaBrg'],
					'tglsimpan' => date('Y-m-d H:i:s'),
					'pemakai' => $pemakai,
					'kodecabang' => $kodecabang
				];
				// print_r($dataheader);
				// die();
				$this->Freebarang_model->SaveDataDetail2($dataheader);
			}
		}

		$data = [
			'kode' => $kode,
			'kodecabang' => $this->input->post('kodecabang'),
			'qty' => $this->input->post('qty'),
			'tglsimpan' => date('Y-m-d H:i:s'),
			'pemakai' => $this->session->userdata('myusername'),
			'aktif' => $this->input->post('aktif'),
		];
		// print_r($data);
		// die();
		$this->Freebarang_model->UpdateData($data, $kode);

		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE) {
			$resultjson = array(
				'kode' => "",
				'message' => "Data gagal disimpan, Kode '<b style='color: red'>" . $kode . "</b>' dengan Cabang '<b style='color: red'>" . $kodecabang . "</b>' sudah pernah digunakan."
			);
			# Something went wrong.
			$this->db->trans_rollback();
		} else {
			$resultjson = array(
				'kode' => $kode,
				'message' => "Data berhasil disimpan."
			);
			# Everything is Perfect. 
			# Committing data to the database.
			$this->db->trans_commit();
		}
		echo json_encode($resultjson);
	}
}
