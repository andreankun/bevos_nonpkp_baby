<?php

defined('BASEPATH') or exit('No direct script access allowed');

class KelompokToko extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->load->model('masterdata/Kelompoktoko_model');
        $this->load->model('Caridataaktif_model');
        $this->load->library('form_validation');
        $this->load->library('session');
    }

    function CariDataKelompok()
    {
        $fetch_data = $this->Caridataaktif_model->make_datatables($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value'));
        $data = array();
        foreach ($fetch_data as $row) {
            $sub_array = array();
            $i = 1;
            $count = count($this->input->post('field'));
            foreach ($this->input->post('field') as $key => $value) {
                if ($i <= $count) {
                    if ($i == 1) {
                        $msearch = $row->$value;
                        $sub_array[] = '<button class="btn btn-dark searchkelompok" data-dismiss="modal" style="margin: 0px;" data-id="' . $msearch . '"><i class="far fa-check-circle"></i></button>';
                        $sub_array[] = $row->$value;
                    } else {
                        if ($i == $count) {
                            $sub_array[] = $row->$value;
                        } else {
                            $sub_array[] = $row->$value;
                        }
                    }
                    // $sub_array[] = $row->$value;
                }
                $i++;
            }
            $data[] = $sub_array;
        }
        $output = array(
            "draw"                    =>     intval($_POST["draw"]),
            "recordsTotal"          =>      $this->Caridataaktif_model->get_all_data($this->input->post('nmtb')),
            "recordsFiltered"     =>     $this->Caridataaktif_model->get_filtered_data($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value')),
            "data"                    =>     $data
        );
        echo json_encode($output);
    }

    function DataKelompok()
    {
        $result = $this->Kelompoktoko_model->DataKelompok($this->input->post('nomor'));
        echo json_encode($result);
    }
	function DataKelompokDetail()
    {
        $result = $this->Kelompoktoko_model->DataDetailKelompok($this->input->post('nomor'));
        echo json_encode($result);
    }

    public function ClearPercent($param)
    {
        return $result = str_replace(array('%'), '', $param);
    }

	public function ClearChr($param)
	{
		return $result = str_replace(array('\'', '"', ',', ';', '<', '>', '_', '-', ' ', '.', '!', '?', '/', '=', '+', ']', '[', '{', '}', '@', '#', '$', '%', '^', '&', "*", '(', ')'), '', $param);
	}


    function Save()
    {
        $pemakai = $this->session->userdata('myusername');
        $kode = $this->input->post('nomorpelanggan');
        $CekKode = $this->Kelompoktoko_model->CekKode($kode);
		$kodecabang = $this->session->userdata('mycabang');

        if (empty($CekKode)) {
            $this->db->trans_start();
            $this->db->trans_strict(FALSE);

			if(!empty($this->input->post('datadetail'))){
				 foreach($this->input->post('datadetail') as $key => $value){
					 $data = [
						 'nomor' => $kode,
						 'nopelanggan' => $value['Kode'],
						 'namapelanggan' => $value['Nama'],
						 'namajwb' => $value['NamaPenanggung'],
						 'tglsimpan' => date('Y-m-d H:m:s'),
						 'pemakai' => $pemakai
					 ];
					//  print_r($data);
					//  die();
					 $this->Kelompoktoko_model->SaveDataDetail($data);
				 }
			}

            $data = [
                // 'kode' => $kode,
                'nomor' => $this->input->post('nomorpelanggan'),
                'nama' => $this->input->post('namapelanggan'),
                'ktp' => $this->input->post('ktp'),
                'limitbln' => $this->ClearChr($this->input->post('limitbln')),
                'limitth' => $this->ClearChr($this->input->post('limitth')),
                'bank' => $this->input->post('bank'),
                'rekeningtf' => $this->input->post('rekening'),
                'userid' => $this->input->post('userid'),
                'password' => $this->input->post('passwordatm'),
                'passwordkey' => $this->input->post('passwordkey'),
                'passwordbanking' => $this->input->post('passwordbanking'),
                'tglsimpan' => date('Y-m-d H:i:s'),
				'kodecabang' => $kodecabang,
                'pemakai' => $pemakai
            ];
            // print_r($data);
            // die();
            $this->Kelompoktoko_model->SaveData($data);

            $this->db->trans_complete();

            if ($this->db->trans_status() == FALSE) {
                $resultjson = array(
                    'kode' => "",
                    'message' => "Data gagal disimpan, Nomor Pelanggan '<b style='color: red'>" . $kode . "</b>'  sudah pernah digunakan."
                );
                $this->db->trans_rollback();
            } else {
                $resultjson = array(
                    'kode' =>  $kode,
                    'message' => "Data berhasil disimpan"
                );

                $this->db->trans_commit();
            }
            echo json_encode($resultjson);
        } else {
            $this->db->trans_complete();
            $resultjson = array(
                'kode' => "",
                'message' => "Data gagal disimpan, Nomor Pelanggan '<b style='color: red'>" . $kode . "</b>' sudah pernah digunakan."
            );
            $this->db->trans_rollback();
            echo json_encode($resultjson);
        }
    }

    function Update()
    {
        $pemakai = $this->session->userdata('myusername');
        $kode = $this->input->post('nomorpelanggan');
		$kodecabang = $this->session->userdata('mycabang');


        $this->db->trans_start();
        $this->db->trans_strict(FALSE);

		
		if(!empty($this->input->post('datadetail'))){
			$this->Kelompoktoko_model->DeleteDetail($kode);
			foreach($this->input->post('datadetail') as $key => $value){
				$data = [
					'nomor' => $kode,
					'nopelanggan' => $value['Kode'],
					'namapelanggan' => $value['Nama'],
					'namajwb' => $value['NamaPenanggung'],
					'tglsimpan' => date('Y-m-d H:m:s'),
					'pemakai' => $pemakai
				];
			    // print_r($data);
			    // die();
				$this->Kelompoktoko_model->SaveDataDetail($data);
			}
	   }

        $data = [
           // 'kode' => $kode,
		   'nomor' => $this->input->post('nomorpelanggan'),
		   'nama' => $this->input->post('namapelanggan'),
		   'ktp' => $this->input->post('ktp'),
		   'limitbln' => $this->ClearChr($this->input->post('limitbln')),
		   'limitth' => $this->ClearChr($this->input->post('limitth')),
		   'bank' => $this->input->post('bank'),
		   'rekeningtf' => $this->input->post('rekening'),
		   'userid' => $this->input->post('userid'),
		   'password' => $this->input->post('passwordatm'),
		   'passwordkey' => $this->input->post('passwordkey'),
		   'passwordbanking' => $this->input->post('passwordbanking'),
		   'tglsimpan' => date('Y-m-d H:i:s'),
		   'kodecabang' => $kodecabang,
		   'pemakai' => $pemakai
        ];

        $this->Kelompoktoko_model->UpdateData($data, $kode);

        $this->db->trans_complete();

        if ($this->db->trans_status() == FALSE) {
            $resultjson = array(
                'kode' => "",
                'message' => "Data gagal diperbarui, Kode '<b style='color: red'>" . $kode . "</b>'  sudah pernah digunakan."
            );
            $this->db->trans_rollback();
        } else {
            $resultjson = array(
                'kode' =>  $kode,
                'message' => "Data berhasil diperbarui."
            );

            $this->db->trans_commit();
        }
        echo json_encode($resultjson);
    }
}
