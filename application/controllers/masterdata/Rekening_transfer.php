<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Rekening_transfer extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->load->model('masterdata/Rekening_transfer_model');
        $this->load->model('Caridataaktif_model');
        $this->load->library('form_validation');
        $this->load->library('session');
    }

    function CariDataCoa()
    {
        $fetch_data = $this->Caridataaktif_model->make_datatables($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value'));
        $data = array();
        foreach ($fetch_data as $row) {
            $sub_array = array();
            $i = 1;
            $count = count($this->input->post('field'));
            foreach ($this->input->post('field') as $key => $value) {
                if ($i <= $count) {
                    if ($i == 1) {
                        $msearch = $row->$value;
                        $sub_array[] = '<button class="btn btn-dark searchcoa" data-dismiss="modal" style="margin: 0px;" data-id="' . $msearch . '"><i class="far fa-check-circle"></i></button>';
                        $sub_array[] = $row->$value;
                    } else {
                        if ($i == $count) {
                            $sub_array[] = $row->$value;
                        } else {
                            $sub_array[] = $row->$value;
                        }
                    }
                    // $sub_array[] = $row->$value;
                }
                $i++;
            }
            $data[] = $sub_array;
        }
        $output = array(
            "draw"                    =>     intval($_POST["draw"]),
            "recordsTotal"          =>      $this->Caridataaktif_model->get_all_data($this->input->post('nmtb')),
            "recordsFiltered"     =>     $this->Caridataaktif_model->get_filtered_data($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value')),
            "data"                    =>     $data
        );
        echo json_encode($output);
    }

    function DataCoa()
    {
        $result = $this->Rekening_transfer_model->DataCoa($this->input->post('noaccount'));
        echo json_encode($result);
    }

    function CariDataRekening()
    {
        $fetch_data = $this->Caridataaktif_model->make_datatables($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value'));
        $data = array();
        foreach ($fetch_data as $row) {
            $sub_array = array();
            $i = 1;
            $count = count($this->input->post('field'));
            foreach ($this->input->post('field') as $key => $value) {
                if ($i <= $count) {
                    if ($i == 1) {
                        $msearch = $row->$value;
                        $sub_array[] = '<button class="btn btn-dark searchrekening" data-dismiss="modal" style="margin: 0px;" data-id="' . $msearch . '"><i class="far fa-check-circle"></i></button>';
                        $sub_array[] = $row->$value;
                    } else {
                        if ($i == $count) {
                            $sub_array[] = $row->$value;
                        } else {
                            $sub_array[] = $row->$value;
                        }
                    }
                    // $sub_array[] = $row->$value;
                }
                $i++;
            }
            $data[] = $sub_array;
        }
        $output = array(
            "draw"                    =>     intval($_POST["draw"]),
            "recordsTotal"          =>      $this->Caridataaktif_model->get_all_data($this->input->post('nmtb')),
            "recordsFiltered"     =>     $this->Caridataaktif_model->get_filtered_data($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value')),
            "data"                    =>     $data
        );
        echo json_encode($output);
    }

    function DataRekening()
    {
        $result = $this->Rekening_transfer_model->DataRekening($this->input->post('norek'));
        echo json_encode($result);
    }

    function Save()
    {
        $kodecabang = $this->session->userdata('mycabang');
        $pemakai = $this->session->userdata('myusername');
        // $kodecabang = "BEVOS";
        // $pemakai = "VSP";
		$kode = "";
		$kodeprefix = $this->input->post('jenisacc');
		if($kodeprefix == 0){
			$kode = "C";
		} else {
			$kode = "B";
		}

        $norek = $this->input->post('norek');
        $coa = $this->input->post('coa');
        $ceknorek = $this->Rekening_transfer_model->CekNorek($coa);

        if (empty($ceknorek)) {
            $this->db->trans_start(); # Starting Transaction
            $this->db->trans_strict(FALSE);

            $data = [
                'nomor' => $this->input->post('coa'),
                'nama' => $this->input->post('nama'),   
                'namabisnis' => $this->input->post('namabisnis'),   
                'norekening' => $this->input->post('norek'),
                'keterangan' => $this->input->post('keterangan'),
                'kodeprefix' => $kode,
                'jenisaccount' => $this->input->post('jenisacc'),
                'pemakai' => $pemakai,
                'tglsimpan' => date('Y-m-d H:i:s'),
                'kode_cabang' => $this->input->post('kodecabang'),
            ];

            $this->Rekening_transfer_model->SaveData($data);

            $this->db->trans_complete();

            if ($this->db->trans_status() === FALSE) {
                $resultjson = array(
                    'nomor' => "",
                    'message' => "Data gagal disimpan, COA '<b style='color: red'>" . $coa . "</b>' sudah pernah digunakan."
                );
                # Something went wrong.
                $this->db->trans_rollback();
            } else {
                $resultjson = array(
                    'nomor' => $coa,
                    'message' => "Data berhasil disimpan."
                );
                # Everything is Perfect. 
                # Committing data to the database.
                $this->db->trans_commit();
            }
            echo json_encode($resultjson);
        } else {
            $this->db->trans_complete();
            $resultjson = array(
                'nomor' => "",
                'message' => "Data gagal disimpan, COA '<b style='color: red'>" . $coa . "</b>' sudah pernah digunakan."
            );
            # Something went wrong.
            $this->db->trans_rollback();
            echo json_encode($resultjson);
        }
    }

    function Update()
    {
        $kodecabang = $this->session->userdata('mycabang');
        $pemakai = $this->session->userdata('myusername');

        $norek = $this->input->post('norek');
        $nomor = $this->input->post('coa');

		$kode = "";
		$kodeprefix = $this->input->post('jenisacc');
		if($kodeprefix == 0){
			$kode = "C";
		} else {
			$kode = "B";
		}

        $this->db->trans_start(); # Starting Transaction
        $this->db->trans_strict(FALSE);

		$data = [
            'nomor' => $this->input->post('coa'),
            'nama' => $this->input->post('nama'),   
            'namabisnis' => $this->input->post('namabisnis'),   
            'norekening' => $this->input->post('norek'),
            'keterangan' => $this->input->post('keterangan'),
            'kodeprefix' => $kode,
            'jenisaccount' => $this->input->post('jenisacc'),
            'pemakai' => $pemakai,
            'tglsimpan' => date('Y-m-d H:i:s'),
            'kode_cabang' => $this->input->post('kodecabang'),
        ];
		

        $this->Rekening_transfer_model->UpdateData($data, $nomor);

        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE) {
            $resultjson = array(
                'nomor' => "",
                'message' => "Data gagal diperbarui, COA '<b style='color: red'>" . $nomor . "</b>' sudah pernah digunakan."
            );
            # Something went wrong.
            $this->db->trans_rollback();
        } else {
            $resultjson = array(
                'nomor' => $nomor,
                'message' => "Data berhasil diperbarui."
            );
            # Everything is Perfect. 
            # Committing data to the database.
            $this->db->trans_commit();
        }
        echo json_encode($resultjson);
    }
}
