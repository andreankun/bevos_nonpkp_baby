<?php

defined('BASEPATH') or exit('No direct script access allowed');

class DiskonKhusus extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->load->model('masterdata/DiskonKhusus_model');
        $this->load->model('Caridataaktif_model');
        $this->load->library('form_validation');
        $this->load->library('session');
    }

    function CariDataDiskonKhusus()
    {
        $fetch_data = $this->Caridataaktif_model->make_datatables($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value'));
        $data = array();
        foreach ($fetch_data as $row) {
            $sub_array = array();
            $i = 1;
            $count = count($this->input->post('field'));
            foreach ($this->input->post('field') as $key => $value) {
                if ($i <= $count) {
                    if ($i == 1) {
                        $msearch = $row->$value;
                        $sub_array[] = '<button class="btn btn-dark searchdiskon" data-dismiss="modal" style="margin: 0px;" data-id="' . $msearch . '"><i class="far fa-check-circle"></i></button>';
                        $sub_array[] = $row->$value;
                    } else {
                        if ($i == $count) {
                            $sub_array[] = $row->$value;
                        } else {
                            $sub_array[] = $row->$value;
                        }
                    }
                    // $sub_array[] = $row->$value;
                }
                $i++;
            }
            $data[] = $sub_array;
        }
        $output = array(
            "draw"                    =>     intval($_POST["draw"]),
            "recordsTotal"          =>      $this->Caridataaktif_model->get_all_data($this->input->post('nmtb')),
            "recordsFiltered"     =>     $this->Caridataaktif_model->get_filtered_data($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value')),
            "data"                    =>     $data
        );
        echo json_encode($output);
    }

    function DataDiskonKhusus()
    {
        $result = $this->DiskonKhusus_model->DataDiskon($this->input->post('nomorpelanggan'));
        echo json_encode($result);
    }
	function DataDiskonKhususDetail()
    {
        $result = $this->DiskonKhusus_model->DataDetailDiskon($this->input->post('nomorpelanggan'));
        echo json_encode($result);
    }

    public function ClearPercent($param)
    {
        return $result = str_replace(array('%'), '', $param);
    }

    function Save()
    {
        $pemakai = $this->session->userdata('myusername');
        $kode = $this->input->post('nomorpelanggan');
        $CekKode = $this->DiskonKhusus_model->CekKode($kode);

        if (empty($CekKode)) {
            $this->db->trans_start();
            $this->db->trans_strict(FALSE);

			if(!empty($this->input->post('datadetail'))){
				 foreach($this->input->post('datadetail') as $key => $value){
					 $data = [
						 'nomorpelanggan' => $kode,
						 'kodebarang' => $value['Kode'],
						 'namabarang' => $value['Nama'],
						 'tglsimpan' => date('Y-m-d H:m:s'),
						 'pemakai' => $pemakai
					 ];
					//  print_r($data);
					//  die();
					 $this->DiskonKhusus_model->SaveDataDetail($data);
				 }
			}

            $data = [
                // 'kode' => $kode,
                'nomorpelanggan' => $this->input->post('nomorpelanggan'),
                'namapelanggan' => $this->input->post('namapelanggan'),
                // 'kodemerk' => $this->input->post('kodemerk'),
                'diskon' => $this->ClearPercent($this->input->post('diskon')),
                'tglsimpan' => date('Y-m-d H:i:s'),
                'pemakai' => $pemakai
            ];
            // print_r($data);
            // die();
            $this->DiskonKhusus_model->SaveData($data);

            $this->db->trans_complete();

            if ($this->db->trans_status() == FALSE) {
                $resultjson = array(
                    'kode' => "",
                    'message' => "Data gagal disimpan, Nomor Pelanggan '<b style='color: red'>" . $kode . "</b>'  sudah pernah digunakan."
                );
                $this->db->trans_rollback();
            } else {
                $resultjson = array(
                    'kode' =>  $kode,
                    'message' => "Data berhasil disimpan"
                );

                $this->db->trans_commit();
            }
            echo json_encode($resultjson);
        } else {
            $this->db->trans_complete();
            $resultjson = array(
                'kode' => "",
                'message' => "Data gagal disimpan, Nomor Pelanggan '<b style='color: red'>" . $kode . "</b>' sudah pernah digunakan."
            );
            $this->db->trans_rollback();
            echo json_encode($resultjson);
        }
    }

    function Update()
    {
        $pemakai = $this->session->userdata('myusername');
        $kode = $this->input->post('nomorpelanggan');


        $this->db->trans_start();
        $this->db->trans_strict(FALSE);

		
		if(!empty($this->input->post('datadetail'))){
			$this->DiskonKhusus_model->DeleteDetail($kode);
			foreach($this->input->post('datadetail') as $key => $value){
				$data = [
					'nomorpelanggan' => $kode,
					'kodebarang' => $value['Kode'],
					'namabarang' => $value['Nama'],
					'tglsimpan' => date('Y-m-d H:m:s'),
					'pemakai' => $pemakai
				];
			    // print_r($data);
			    // die();
				$this->DiskonKhusus_model->SaveDataDetail($data);
			}
	   }

        $data = [
            // 'kode' => $kode,
            'nomorpelanggan' => $this->input->post('nomorpelanggan'),
            'namapelanggan' => $this->input->post('namapelanggan'),
            // 'kodebarang' => $this->input->post('kodebarang'),
            // 'namabarang' => $this->input->post('namabarang'),
            'diskon' => $this->ClearPercent($this->input->post('diskon')),
            'aktif' => $this->input->post('aktif'),
            'tglsimpan' => date('Y-m-d H:i:s'),
            'pemakai' => $pemakai
        ];

        $this->DiskonKhusus_model->UpdateData($data, $kode);

        $this->db->trans_complete();

        if ($this->db->trans_status() == FALSE) {
            $resultjson = array(
                'kode' => "",
                'message' => "Data gagal diperbarui, Kode '<b style='color: red'>" . $kode . "</b>'  sudah pernah digunakan."
            );
            $this->db->trans_rollback();
        } else {
            $resultjson = array(
                'kode' =>  $kode,
                'message' => "Data berhasil diperbarui."
            );

            $this->db->trans_commit();
        }
        echo json_encode($resultjson);
    }
}
