<?php

defined('BASEPATH') or exit('No direct script access allowed');

class RegistrasiNomorFakturPajak extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->load->model('masterdata/RegistrasiNomorFakturPajak_model');
        $this->load->model('Caridataaktif_model');
        $this->load->library('form_validation');
        $this->load->library('session');
    }

    function CariDataRegistrasiNomorFakturPajak()
    {
        $fetch_data = $this->Caridataaktif_model->make_datatables($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value'));
        $data = array();
        foreach ($fetch_data as $row) {
            $sub_array = array();
            $i = 1;
            $count = count($this->input->post('field'));
            foreach ($this->input->post('field') as $key => $value) {
                if ($i <= $count) {
                    if ($i == 1) {
                        $msearch = $row->$value;
                        // $sub_array[] = '<button class="btn btn-dark searchnofp" data-dismiss="modal" style="margin: 0px;" data-id="' . $msearch . '"><i class="far fa-check-circle"></i></button>';
                        $sub_array[] = $row->$value;
                    } else {
                        if ($i == $count) {
                            $sub_array[] = $row->$value;
                        } else {
                            $sub_array[] = $row->$value;
                        }
                    }
                    // $sub_array[] = $row->$value;
                }
                $i++;
            }
            $data[] = $sub_array;
        }
        $output = array(
            "draw"                    =>     intval($_POST["draw"]),
            "recordsTotal"          =>      $this->Caridataaktif_model->get_all_data($this->input->post('nmtb')),
            "recordsFiltered"     =>     $this->Caridataaktif_model->get_filtered_data($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value')),
            "data"                    =>     $data
        );
        echo json_encode($output);
    }

    function Save()
    {
        $nomor1 = substr($this->input->post('nomor1'), 8);
        $nomor2 = substr($this->input->post('nomor2'), 8);
        $nomors = substr($this->input->post('nomor1'), 0, -5);
        // $nomor = "";
        $this->db->trans_start(); # Starting Transaction
        $this->db->trans_strict(FALSE);
        do {
            $nomor = $nomors . sprintf('%05s', $nomor1);
            $data = [
                'nomor' => $nomor,
                'tanggal' => date('Y-m-d H:i:s')
            ];
            // print_r($data);
            $this->RegistrasiNomorFakturPajak_model->SaveData($data);
            // echo $nomor1;
            // echo '<br>';
            $nomor1++;
        } while ($nomor1 <= $nomor2);

        // for ($nomor1 <= $nomor2; $nomor1++;) {
        //     print_r($nomor1);
        //     // $data = [
        //     //     'nomor' => $nomor . sprintf('%08s', $nomor1),
        //     //     'tanggal' => date('Y-m-d H:i:s')
        //     // ];

        //     // $this->RegistrasiNomorFakturPajak_model->SaveData($data);
        // }
        // die();
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE) {
            $resultjson = array(
                'nomor' => "",
                'message' => "Data gagal disimpan, Nomor '<b style='color: red'>" . $nomor . "</b>'  sudah pernah digunakan."
            );
            # Something went wrong.
            $this->db->trans_rollback();
        } else {
            $resultjson = array(
                'nomor' => $nomor,
                'message' => "Data berhasil disimpan."
            );
            # Everything is Perfect. 
            # Committing data to the database.
            $this->db->trans_commit();
        }
        echo json_encode($resultjson);
    }

    function LoadNomorFakturPajak()
	{
		$result = $this->RegistrasiNomorFakturPajak_model->LoadNomorFakturPajak();
		echo json_encode($result);
	}
}
