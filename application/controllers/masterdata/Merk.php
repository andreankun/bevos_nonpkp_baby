<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Merk extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->load->model('masterdata/Merk_model');
        $this->load->model('Caridataaktif_model');
        $this->load->library('form_validation');
        $this->load->library('session');
    }

    function CariDataMerk()
	{
		$fetch_data = $this->Caridataaktif_model->make_datatables($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value'));
		$data = array();
		foreach ($fetch_data as $row) {
			$sub_array = array();
			$i = 1;
			$count = count($this->input->post('field'));
			foreach ($this->input->post('field') as $key => $value) {
				if ($i <= $count) {
					if ($i == 1) {
						$msearch = $row->$value;
						$sub_array[] = '<button class="btn btn-dark searchmerk" data-dismiss="modal" style="margin: 0px;" data-id="' . $msearch . '"><i class="far fa-check-circle"></i></button>';
						$sub_array[] = $row->$value;
					} else {
						if ($i == $count) {
							$sub_array[] = $row->$value;
						} else {
							$sub_array[] = $row->$value;
						}
					}
					// $sub_array[] = $row->$value;
				}
				$i++;
			}
			$data[] = $sub_array;
		}
		$output = array(
			"draw"                    =>     intval($_POST["draw"]),
			"recordsTotal"          =>      $this->Caridataaktif_model->get_all_data($this->input->post('nmtb')),
			"recordsFiltered"     =>     $this->Caridataaktif_model->get_filtered_data($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value')),
			"data"                    =>     $data
		);
		echo json_encode($output);
	}

	function CariDataMerkAkhir()
	{
		$fetch_data = $this->Caridataaktif_model->make_datatables($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value'));
		$data = array();
		foreach ($fetch_data as $row) {
			$sub_array = array();
			$i = 1;
			$count = count($this->input->post('field'));
			foreach ($this->input->post('field') as $key => $value) {
				if ($i <= $count) {
					if ($i == 1) {
						$msearch = $row->$value;
						$sub_array[] = '<button class="btn btn-dark searchmerkakhir" data-dismiss="modal" style="margin: 0px;" data-id="' . $msearch . '"><i class="far fa-check-circle"></i></button>';
						$sub_array[] = $row->$value;
					} else {
						if ($i == $count) {
							$sub_array[] = $row->$value;
						} else {
							$sub_array[] = $row->$value;
						}
					}
					// $sub_array[] = $row->$value;
				}
				$i++;
			}
			$data[] = $sub_array;
		}
		$output = array(
			"draw"                    =>     intval($_POST["draw"]),
			"recordsTotal"          =>      $this->Caridataaktif_model->get_all_data($this->input->post('nmtb')),
			"recordsFiltered"     =>     $this->Caridataaktif_model->get_filtered_data($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value')),
			"data"                    =>     $data
		);
		echo json_encode($output);
	}

	function DataMerk()
	{
		$result = $this->Merk_model->DataMerk($this->input->post('kode'));
		echo json_encode($result);
	}

    function Save()
    {
        $kodecabang = $this->session->userdata('mycabang');
        $pemakai = $this->session->userdata('myusername');
        // $kodecabang = "BEVOS";
        // $pemakai = "VSP";

        $kode = $this->input->post('kode');
        $cekkode = $this->Merk_model->CekKode($kode);

        if (empty($cekkode)) {
            $this->db->trans_start(); # Starting Transaction
            $this->db->trans_strict(FALSE);

            $data = [
                'kode' => $this->input->post('kode'),
                'nama' => $this->input->post('nama'),
                'pemakai' => $pemakai,
                'tglsimpan' => date('Y-m-d H:i:s'),
                'kodecabang' => $kodecabang,
            ];

            $this->Merk_model->SaveData($data);

            $this->db->trans_complete();

            if ($this->db->trans_status() === FALSE) {
                $resultjson = array(
                    'kode' => "",
                    'message' => "Data gagal disimpan, Kode '<b style='color: red'>" . $kode . "</b>' sudah pernah digunakan."
                );
                # Something went wrong.
                $this->db->trans_rollback();
            } else {
                $resultjson = array(
                    'kode' => $kode,
                    'message' => "Data berhasil disimpan."
                );
                # Everything is Perfect. 
                # Committing data to the database.
                $this->db->trans_commit();
            }
            echo json_encode($resultjson);
        }
        else {
            $this->db->trans_complete();
            $resultjson = array(
                'kode' => "",
                'message' => "Data gagal disimpan, Kode '<b style='color: red'>" . $kode . "</b>' sudah pernah digunakan."
            );
            # Something went wrong.
            $this->db->trans_rollback();
            echo json_encode($resultjson);
        }
    }

    function Update()
	{
		// $kodecabang = $this->session->userdata('mycabang');
		$pemakai = $this->session->userdata('myusername');
		// $kodecabang = "BEVOS";
		// $pemakai = "VSP";

		$kode = $this->input->post('kode');

		$this->db->trans_start(); # Starting Transaction
		$this->db->trans_strict(FALSE);

		$data = [
			'kode' => $kode,
			'nama' => $this->input->post('nama'),
			'aktif' => $this->input->post('aktif'),
			'pemakai' => $pemakai,
			'tglsimpan' => date('Y-m-d H:i:s'),
			// 'kodecabang' => $kodecabang,
		];

		$this->Merk_model->UpdateData($data, $kode);

		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE) {
			$resultjson = array(
				'kode' => "",
				'message' => "Data gagal diperbarui, Kode '<b style='color: red'>" . $kode . "</b>' sudah pernah digunakan."
			);
			# Something went wrong.
			$this->db->trans_rollback();
		} else {
			$resultjson = array(
				'kode' => $kode,
				'message' => "Data berhasil diperbarui."
			);
			# Everything is Perfect. 
			# Committing data to the database.
			$this->db->trans_commit();
		}
		echo json_encode($resultjson);
	}

	function DataMerkSelect()
    {
        $response = $this->Merk_model->DataMerkSelect($this->input->post('searchTerm'));
        echo json_encode($response);
    }

	function DataMerkSelectAkhir()
    {
        $response = $this->Merk_model->DataMerkSelectAkhir($this->input->post('searchTerm'));
        echo json_encode($response);
    }
}
