<?php
defined('BASEPATH') or exit('No direct script access allowed');
class BarangDetail extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('masterdata/Barangdetail_model');
		$this->load->model('masterdata/Leveldiskon_model');
		$this->load->model('Caridataaktif_model');
		$this->load->library('form_validation');
		$this->load->library('session');
	}

	function CariDataBarangDetail()
	{
		$fetch_data = $this->Caridataaktif_model->make_datatables($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value'));
		$data = array();
		foreach ($fetch_data as $row) {
			$sub_array = array();
			$i = 1;
			$count = count($this->input->post('field'));
			foreach ($this->input->post('field') as $key => $value) {
				if ($i <= $count) {
					if ($i == 1) {
						$msearch = $row->$value;
						$sub_array[] = '<button class="btn btn-dark searchdetail" data-dismiss="modal" style="margin: 0px;" data-id="' . $msearch . '"><i class="far fa-check-circle"></i></button>';
						$sub_array[] = $row->$value;
					} else {
						if ($i == $count) {
							$sub_array[] = $row->$value;
						} else {
							$sub_array[] = $row->$value;
						}
					}
					// $sub_array[] = $row->$value;
				}
				$i++;
			}
			$data[] = $sub_array;
		}
		$output = array(
			"draw"                    =>     intval($_POST["draw"]),
			"recordsTotal"          =>      $this->Caridataaktif_model->get_all_data($this->input->post('nmtb')),
			"recordsFiltered"     =>     $this->Caridataaktif_model->get_filtered_data($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value')),
			"data"                    =>     $data
		);
		echo json_encode($output);
	}

	function DataBarangDetail()
	{
		$result = $this->Barangdetail_model->DataBarangDetail($this->input->post('kodebarang'), $this->input->post('cabang'));

		echo json_encode($result);
	}

	function DataHargaBarang()
	{
		$result = $this->Barangdetail_model->DataHargaDetail($this->input->post('kodebarang'), $this->input->post('cabang'));
		// print_r($result);
		// die();
		echo json_encode($result);
	}

	function DataSalesmanDetail()
	{
		$result = $this->Barangdetail_model->DataSalesmanDetail($this->input->post('kodebarang'), $this->input->post('cabang'));

		echo json_encode($result);
	}

	public function ClearChr($param)
	{
		return $result = str_replace(array('\'', '"', ',', ';', '<', '>', '_', '-', ' ', '.', '!', '?', '/', '=', '+', ']', '[', '{', '}', '@', '#', '$', '%', '^', '&', "*", '(', ')'), '', $param);
	}

	public function ClearPercent($param)
	{
		return $result = str_replace(array('%'), '', $param);
	}


	function Save()
	{
		$kodebarang = $this->input->post('kodebarang');
		$kodecabang = $this->input->post('kodecabang');
		$pemakai = $this->session->userdata('myusername');
		$periode = date("Y") . date("m");
		$gudang = $this->Barangdetail_model->DataGudang($kodecabang);

		$this->db->trans_start();
		$this->db->trans_strict(FALSE);

		$cekkode = $this->Barangdetail_model->CekKodeDetail($kodebarang, $kodecabang);

		if (empty($cekkode)) {
			foreach ($gudang as $key => $values) {
				$cekinventory = $this->Barangdetail_model->CekInventory($kodebarang, $values->kode, $kodecabang);
				if (empty($cekinventory)) {
					$datainventory = [
						'periode' => $periode,
						'kodecabang' => $kodecabang,
						'kodegudang' => $values->kode,
						'kodebarang' => $kodebarang,
						'tglsimpan' => date('Y-m-d H:m:s'),
						'pemakai' => $pemakai
					];

					$this->Barangdetail_model->SaveDataInv($datainventory);
				}
			}

			if (!empty($this->input->post('datadetail'))) {
				foreach ($this->input->post('datadetail') as $key => $value) {
					if ($value['Kondisi'] == 'true') {
						$datadetail = [
							'kode' => $kodebarang,
							'kodesalesman' => $value['Kode'],
							'namasalesman' => $value['Nama'],
							'tglsimpan' => date('Y-m-d H:i:s'),
							'pemakai' => $pemakai,
							'kodecabang' => $kodecabang,
							'checksales' => $value['Kondisi']
						];
						$this->Barangdetail_model->SaveDataDetail($datadetail);
					} else {
						$datadetail = [
							'kode' => $kodebarang,
							'kodesalesman' => $value['Kode'],
							'namasalesman' => $value['Nama'],
							'tglsimpan' => date('Y-m-d H:i:s'),
							'pemakai' => $pemakai,
							'kodecabang' => $kodecabang,
							'checksales' => $value['Kondisi']
						];
						$this->Barangdetail_model->SaveDataDetail($datadetail);
					}
				}
			}
			if (!empty($this->input->post('dataharga'))) {
				foreach ($this->input->post('dataharga') as $key => $value) {
					$dataharga = [
						'kode' => $kodebarang,
						'hargajual' => $this->ClearChr($value['HargaJual']),
						'hargabeli' => $this->ClearChr($value['HargaBeli']),
						'tglsimpan' => date('Y-m-d H:i:s'),
						'pemakai' => $pemakai,
						'kodecabang' => $kodecabang
					];
					// print_r($dataharga);
					// die();
					$this->Barangdetail_model->SaveDataHarga($dataharga);
				}
			}

			$data = [
				'kodecabang' => $kodecabang,
				'kodebarang' => $kodebarang,
				'namabarang' => $this->input->post('namabarang'),
				'hargajual' => $this->ClearChr($this->input->post('hargajual')),
				'hargabeli' => $this->ClearChr($this->input->post('hargabeli')),
				'komisi' => $this->ClearPercent($this->input->post('komisi')),
				'lokasi' => $this->input->post('lokasi'),
				'tglsimpan' => date('Y-m-d H:i:s'),
				'pemakai' => $pemakai
			];

			$this->Barangdetail_model->SaveData($data);
			$this->db->trans_complete();
			if ($this->db->trans_status() === FALSE) {
				$resultjson = array(
					'kode' => "",
					'kodecabang' => "",
					'message' => "Data gagal disimpan, Kode '<b style='color: red'>" . $kodebarang . "</b>' dengan Cabang '<b style='color: red'>" . $kodecabang . "</b>' sudah pernah digunakan."
				);
				# Something went wrong.
				$this->db->trans_rollback();
			} else {
				$resultjson = array(
					'kode' => $kodebarang,
					'kodebarang' => $kodebarang,
					'message' => "Data berhasil disimpan."
				);
				# Everything is Perfect. 
				# Committing data to the database.
				$this->db->trans_commit();
			}
			echo json_encode($resultjson);
		} else {
			$this->db->trans_complete();
			$resultjson = array(
				'kode' => "",
				'kodecabang' => "",
				'message' => "Data gagal disimpan, Kode '<b style='color: red'>" . $kodebarang . "</b>' dengan Cabang '<b style='color: red'>" . $kodecabang . "</b>' sudah pernah digunakan."
			);
			# Something went wrong.
			$this->db->trans_rollback();
			echo json_encode($resultjson);
		}
	}

	function Import()
	{
		$errorvalidasi = false;
		$rowCount = 0;
		$kode = "";
		if (isset($_FILES["file_csv"]["name"])) {
			$this->load->library('CSVReader');
			$csvData = $this->csvreader->parse_csv($_FILES['file_csv']['tmp_name'][0]);
			$this->db->trans_start(); # Starting Transaction
			$this->db->trans_strict(FALSE);
			$this->db->trans_complete();
			if (!empty($csvData)) {
				foreach ($csvData as $key => $value) {
					$kode = $value['kodebarang'];
					$kodecabang = $value['kodecabang'];
					$cekkode = $this->Barangdetail_model->CekBarang($kode);
					$cekdetail = $this->Barangdetail_model->CekKodeDetail($kode);
					if (!empty($cekdetail)) {
						$kodebarang = $value['kodebarang'];
						$kodecabang = $this->session->userdata('mycabang');
						// print_r($kodecabang);
						// die();
						$rowCount++;
						$data = array(
							'kodecabang' => $value['kodecabang'],
							'kodebarang' => $value['kodebarang'],
							'namabarang' => $value['namabarang'],
							'hargabeli' => $value['hargabeli'],
							'hargajual' => $value['hargajual'],
							'lokasi' => $value['lokasi'],
							'komisi' => $value['komisi'],
							'tglsimpan' => date('Y-m-d H:m:s'),
							'pemakai' => $this->session->userdata('myusername')
						);

						$this->Barangdetail_model->UpdateData($data, $kodebarang, $kodecabang);
					} else if (!empty($cekkode) && empty($cekdetail)) {
						$data = array(
							'kodecabang' => $value['kodecabang'],
							'kodebarang' => $value['kodebarang'],
							'namabarang' => $value['namabarang'],
							'hargabeli' => $value['hargabeli'],
							'hargajual' => $value['hargajual'],
							'lokasi' => $value['lokasi'],
							'komisi' => $value['komisi'],
							'tglsimpan' => date('Y-m-d H:m:s'),
							'pemakai' => $this->session->userdata('myusername')
						);
						// print_r($data);
						// die();
						$this->Barangdetail_model->SaveData($data);
					}
				}
			}
			if ($this->db->trans_status() === FALSE) {
				$resultjson = array(
					'kode' => true,
					'message' => "Data gagal diupload"
				);
				# Something went wrong.
				$this->db->trans_rollback();
			} else {
				$resultjson = array(
					'kode' => false,
					'message' => "Data berhasil diupload"
				);
				# Everything is Perfect. 
				# Committing data to the database.
				$this->db->trans_commit();
			}
			echo json_encode($resultjson);
		}
	}

	function Export($param = null)
	{
		$param = explode(":", urldecode($param));
		$data['databarang'] = $this->Barangdetail_model->GetExport($param[1], $param[0]);
		$this->load->view('menu/masterdata/barang_detail/v_excel', $data);
	}

	function Update()
	{
		$kodebarang = $this->input->post('kodebarang');
		$kodecabang = $this->input->post('kodecabang');
		$pemakai = $this->session->userdata('myusername');
		$kodelevel = $this->input->post('kodelevel');
		$periode = date('Y') . date('m');

		$this->db->trans_start();
		$this->db->trans_strict(FALSE);
		$gudang = $this->Barangdetail_model->DataGudang($kodecabang);
		// $cekkode = $this->Barangdetail_model->CekKodeDetail($kodebarang);

		foreach ($gudang as $key => $values) {
			$cekinventory = $this->Barangdetail_model->CekInventory($kodebarang, $values->kode, $kodecabang);
			if (empty($cekinventory)) {
				$datainventory = [
					'periode' => $periode,
					'kodecabang' => $kodecabang,
					'kodegudang' => $values->kode,
					'kodebarang' => $kodebarang,
					'tglsimpan' => date('Y-m-d H:m:s'),
					'pemakai' => $pemakai
				];
				$this->Barangdetail_model->SaveDataInv($datainventory);
			}
		}


		if (!empty($this->input->post('dataharga'))) {
			$this->Barangdetail_model->DeleteHarga($kodebarang, $kodecabang);
			foreach ($this->input->post('dataharga') as $key => $value) {
				$dataharga = [
					'kode' => $kodebarang,
					'hargajual' => $this->ClearChr($value['HargaJual']),
					'hargabeli' => $this->ClearChr($value['HargaBeli']),
					'tglsimpan' => date('Y-m-d H:i:s'),
					'pemakai' => $pemakai,
					'kodecabang' => $kodecabang
				];
				// print_r($dataharga);
				// die();
				$this->Barangdetail_model->SaveDataHarga($dataharga);
			}
		}


		if (!empty($this->input->post('datadetail'))) {
			$this->Barangdetail_model->DeleteDetail($kodebarang, $kodecabang);
			foreach ($this->input->post('datadetail') as $key => $value) {
				// if ($value['Kondisi'] == 'true') {
				$datadetail = [
					'kode' => $kodebarang,
					'kodesalesman' => $value['Kode'],
					'namasalesman' => $value['Nama'],
					'tglsimpan' => date('Y-m-d H:i:s'),
					'pemakai' => $pemakai,
					'kodecabang' => $kodecabang,
					'checksales' => $value['Kondisi']
				];
				$this->Barangdetail_model->SaveDataDetail($datadetail);
				// }
			}
		}

		// $data = [
		// 	'kode' => $kodelevel,
		// 	'kodecabang' => $kodecabang,
		// 	'kodebarang' => $kodebarang,
		// 	'diskon' => $this->ClearPercent($this->input->post('discount')),
		// 	'kondisi1' => $this->input->post('kondisi1'),
		// 	'kondisi2' => $this->input->post('kondisi2'),
		// 	'tglsimpan' => date('Y-m-d H:i:s'),
		// 	'pemakai' => $pemakai,
		// 	'aktif' => $this->input->post('aktif')
		// ];
		// $this->Leveldiskon_model->UpdateData($kodelevel, $data);

		$data = [
			'hargabeli' => $this->ClearChr($this->input->post('hargabeli')),
			'hargajual' => $this->ClearChr($this->input->post('hargajual')),
			'komisi' => $this->ClearPercent($this->input->post('komisi')),
			'lokasi' => $this->input->post('lokasi'),
			'tglsimpan' => date('Y-m-d H:i:s'),
			'pemakai' => $pemakai,
			'aktif' => $this->input->post('aktif')
		];

		$this->Barangdetail_model->UpdateData($data, $kodebarang, $kodecabang);

		$this->db->trans_complete();

		// print_r($cekkode);
		// die();
		if ($this->db->trans_status() === FALSE) {
			$resultjson = array(
				'kode' => "",
				'message' => "Data gagal diperbarui, Kode '<b style='color: red'>" . $kodebarang . "</b>' sudah pernah digunakan."
			);
			# Something went wrong.
			$this->db->trans_rollback();
		} else {
			$resultjson = array(
				'kode' => $kodebarang,
				'message' => "Data berhasil diperbarui."
			);
			# Everything is Perfect. 
			# Committing data to the database.
			$this->db->trans_commit();
		}
		echo json_encode($resultjson);
	}
}
