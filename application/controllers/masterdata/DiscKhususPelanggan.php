<?php

defined('BASEPATH') or exit('No direct script access allowed');

class DiscKhususPelanggan extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('masterdata/Disckhususpelanggan_model');
		$this->load->model('Caridataaktif_model');
		$this->load->library('form_validation');
		$this->load->library('session');
	}

	function CariDataDiscKhusus()
	{
		$fetch_data = $this->Caridataaktif_model->make_datatables($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value'));
		$data = array();
		foreach ($fetch_data as $row) {
			$sub_array = array();
			$i = 1;
			$count = count($this->input->post('field'));
			foreach ($this->input->post('field') as $key => $value) {
				if ($i <= $count) {
					if ($i == 1) {
						$msearch = $row->$value;
						$sub_array[] = '<button class="btn btn-dark searchdiscpelanggan" data-dismiss="modal" style="margin: 0px;" data-id="' . $msearch . '"><i class="far fa-check-circle"></i></button>';
						$sub_array[] = $row->$value;
					} else {
						if ($i == $count) {
							$sub_array[] = $row->$value;
						} else {
							$sub_array[] = $row->$value;
						}
					}
					// $sub_array[] = $row->$value;
				}
				$i++;
			}
			$data[] = $sub_array;
		}
		$output = array(
			"draw"                    =>     intval($_POST["draw"]),
			"recordsTotal"          =>      $this->Caridataaktif_model->get_all_data($this->input->post('nmtb')),
			"recordsFiltered"     =>     $this->Caridataaktif_model->get_filtered_data($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value')),
			"data"                    =>     $data
		);
		echo json_encode($output);
	}

	function DataDisckKhususPelanggan()
	{
		$result = $this->Disckhususpelanggan_model->DataDiscKhususPelanggan($this->input->post('nomorpelanggan'));
		// print_r($result);
		// die();
		echo json_encode($result);
	}

	function DataDiscKhususPelangganDetail()
	{
		$result = $this->Disckhususpelanggan_model->DataDiscKhususPelangganDetail($this->input->post('nomorpelanggan'));
		echo json_encode($result);
	}

	public function ClearPercent($param)
	{
		return $result = str_replace(array('%'), '', $param);
	}

	function Save()
	{
		// $kodecabang = $this->session->userdata('mycabang');
		$pemakai = $this->session->userdata('myusername');
		// $kodecabang = "BEVOS";
		// $pemakai = "VSP";
		// $data1 = $this->input->post('datadetail');
		// print_r($data1);
		// die();

		$nopelanggan = $this->input->post('nomorpelanggan');
		$ceknopelanggan = $this->Disckhususpelanggan_model->CekKode($nopelanggan);

		if (empty($ceknopelanggan)) {
			$this->db->trans_start(); # Starting Transaction
			$this->db->trans_strict(FALSE);
			

			if (!empty($this->input->post('datadetail'))) {
				foreach ($this->input->post('datadetail') as $key => $values) {
					$data = [
						'nopelanggan' => $nopelanggan,
						'kodemerk' => $values['Kode'],
						'namamerk' => $values['Nama'],
						'pemakai' => $pemakai,
						'tglsimpan' => date('Y-m-d H:i:s'),
					];
					// print_r($data);
					// die();
					$this->Disckhususpelanggan_model->SaveDataDetail($data);
				}
			}

			$data = [
				// 'kode' => $nopelanggan,
				'nopelanggan' => $this->input->post('nomorpelanggan'),
				'namapelanggan' => $this->input->post('namapelanggan'),
				// 'kodemerk' => $this->input->post('kodemerk'),
				'diskon' => $this->ClearPercent($this->input->post('diskon')),
				'pemakai' => $pemakai,
				'tglsimpan' => date('Y-m-d H:i:s'),
				// 'kodecabang' => $kodecabang,
			];
			// print_r($data);
			// die();

			$this->Disckhususpelanggan_model->SaveData($data);

			$this->db->trans_complete();

			if ($this->db->trans_status() === FALSE) {
				$resultjson = array(
					'kode' => "",
					'message' => "Data gagal disimpan, Nomor Pelanggan '<b style='color: red'>" . $nopelanggan . "</b>'  sudah pernah digunakan."
				);
				# Something went wrong.
				$this->db->trans_rollback();
			} else {
				$resultjson = array(
					'kode' => $nopelanggan,
					'message' => "Data berhasil disimpan."
				);
				# Everything is Perfect. 
				# Committing data to the database.
				$this->db->trans_commit();
			}
			echo json_encode($resultjson);
		} else {
			$this->db->trans_complete();
			$resultjson = array(
				'kode' => "",
				'message' => "Data gagal disimpan, Nomor Pelanggan '<b style='color: red'>" . $nopelanggan . "</b>' sudah pernah digunakan."
			);
			# Something went wrong.
			$this->db->trans_rollback();
			echo json_encode($resultjson);
		}
	}

	function Update()
	{
		$kodecabang = $this->session->userdata('mycabang');
		$pemakai = $this->session->userdata('myusername');
		// $kodecabang = "BEVOS";
		// $pemakai = "VSP";

		$nopelanggan = $this->input->post('nomorpelanggan');

		$this->db->trans_start(); # Starting Transaction
		$this->db->trans_strict(FALSE);

		if (!empty($this->input->post('datadetail'))) {
			$this->Disckhususpelanggan_model->DeleteDetail($nopelanggan);
			foreach ($this->input->post('datadetail') as $key => $values) {
				$data = [
					'nopelanggan' => $nopelanggan,
					'kodemerk' => $values['Kode'],
					'namamerk' => $values['Nama'],
					'pemakai' => $pemakai,
					'tglsimpan' => date('Y-m-d H:i:s'),
				];
				// print_r($data);
				// die();
				$this->Disckhususpelanggan_model->SaveDataDetail($data);
			}
		}

		$data = [
			// 'kode' => $this->input->post('kode'),
			'nopelanggan' => $this->input->post('nomorpelanggan'),
			'namapelanggan' => $this->input->post('namapelanggan'),
			// 'kodemerk' => $this->input->post('kodemerk'),
			'diskon' => $this->ClearPercent($this->input->post('diskon')),
			'pemakai' => $pemakai,
			'tglsimpan' => date('Y-m-d H:i:s'),
			'aktif' => $this->input->post('aktif'),
			// 'kodecabang' => $kodecabang,
		];
		// print_r($data);
		// die();

		$this->Disckhususpelanggan_model->UpdateData($data, $nopelanggan);

		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE) {
			$resultjson = array(
				'kode' => "",
				'message' => "Data gagal diperbarui, Nomor Pelanggan '<b style='color: red'>" . $nopelanggan . "</b>' sudah pernah digunakan."
			);
			# Something went wrong.
			$this->db->trans_rollback();
		} else {
			$resultjson = array(
				'kode' => $nopelanggan,
				'message' => "Data berhasil diperbarui."
			);
			# Everything is Perfect. 
			# Committing data to the database.
			$this->db->trans_commit();
		}
		echo json_encode($resultjson);
	}
}
