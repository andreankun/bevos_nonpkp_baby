<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Salesman extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('masterdata/Salesman_model');
		$this->load->model('Caridataaktif_model');
		$this->load->library('form_validation');
		$this->load->library('session');
	}

	function CariDataSalesman()
	{
		$fetch_data = $this->Caridataaktif_model->make_datatables($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value'));
		$data = array();
		foreach ($fetch_data as $row) {
			$sub_array = array();
			$i = 1;
			$count = count($this->input->post('field'));
			foreach ($this->input->post('field') as $key => $value) {
				if ($i <= $count) {
					if ($i == 1) {
						$msearch = $row->$value;
						$sub_array[] = '<button class="btn btn-dark searchsalesman" data-dismiss="modal" style="margin: 0px;" data-id="' . $msearch . '"><i class="far fa-check-circle"></i></button>';
						$sub_array[] = $row->$value;
					} else {
						if ($i == $count) {
							$sub_array[] = $row->$value;
						} else {
							$sub_array[] = $row->$value;
						}
					}
					// $sub_array[] = $row->$value;
				}
				$i++;
			}
			$data[] = $sub_array;
		}
		$output = array(
			"draw"                    =>     intval($_POST["draw"]),
			"recordsTotal"          =>      $this->Caridataaktif_model->get_all_data($this->input->post('nmtb')),
			"recordsFiltered"     =>     $this->Caridataaktif_model->get_filtered_data($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value')),
			"data"                    =>     $data
		);
		echo json_encode($output);
	}

	function DataSalesman()
	{
		$result = $this->Salesman_model->DataSalesman($this->input->post('kode'));
		echo json_encode($result);
	}

	function DataSalesmanDetail()
	{
		$result = $this->Salesman_model->DataSalesmanDetail($this->input->post('kode'));
		echo json_encode($result);
	}

	public function ClearChr($param)
	{
		return $result = str_replace(array('\'', '"', ',', ';', '<', '>', '_', '-', ' ', '.', '!', '?', '/', '=', '+', ']', '[', '{', '}', '@', '#', '$', '%', '^', '&', "*", '(', ')'), '', $param);
	}

	function Save()
	{

		// $kodecabang = "BEVOS";
		$kodecabang = $this->session->userdata('mycabang');
		$pemakai = $this->session->userdata('myusername');

		$kode = $this->input->post('kode');
		$cekkode = $this->Salesman_model->CekKode($kode);

		if (empty($cekkode)) {
			$this->db->trans_start(); # Starting Transaction
			$this->db->trans_strict(FALSE);

			$kodesalesman = $this->input->post('kode');
			$kodecabang = $this->input->post('kodecabang');

			$cekkode = $this->Salesman_model->cekKode($kodesalesman);
			$cekcabang = $this->Salesman_model->CekCabang($kodecabang);
			$databarang = $this->Salesman_model->GetDataBarangCabang($kodecabang);

			// $ambilnomor = "BS".$kodecabang;

			$get["salesman"] = $this->Salesman_model->GetMaxNomor($kodecabang);
			if (!$get["salesman"]) {
				$kode = $kodecabang . "0001";
			} else {
				$lastNomor = $get['salesman']->kode;
				$lastNoUrut = substr($lastNomor, 1, 5);

				// nomor urut ditambah 1
				$nextNoUrut = $lastNoUrut + 1;
				$kode = $kodecabang . sprintf('%04s', $nextNoUrut);;
				// print_r($kode);
				// die();
			}

			if (empty($cekkode)) {
				if (!empty($cekcabang)) {
					foreach ($databarang as $key => $value) {
						$datadetail = [
							'kode' => $value->kodebarang,
							'kodesalesman' => $kode,
							'namasalesman' => $this->input->post('nama'),
							'tglsimpan' => date('Y-m-d H:i:s'),
							'kodecabang' => $this->input->post('kodecabang'),
							'pemakai' => $pemakai,
							'checksales' => false
						];
						$this->Salesman_model->SaveDataDetail($datadetail);
					}
				}
				$data = [
					'kode' => $kode,
					'nama' => $this->input->post('nama'),
					'nohp' => $this->input->post('nohp'),
					'kodespv' => $this->input->post('kodespv'),
					'namaspv' => $this->input->post('namaspv'),
					'kodecabang' => $this->input->post('kodecabang'),
					'pemakai' => $pemakai,
					'tglsimpan' => date('Y-m-d H:i:s'),
					// 'kodecabang' => $kodecabang,
				];
				$this->Salesman_model->SaveData($data);
			}

			$this->db->trans_complete();

			if ($this->db->trans_status() === FALSE) {
				$resultjson = array(
					'kode' => "",
					'message' => "Data gagal disimpan, Kode '<b style='color: red'>" . $kode . "</b>' sudah pernah digunakan."
				);
				# Something went wrong.
				$this->db->trans_rollback();
			} else {
				$resultjson = array(
					'kode' => $kode,
					'message' => "Data berhasil disimpan."
				);
				# Everything is Perfect. 
				# Committing data to the database.
				$this->db->trans_commit();
			}
			echo json_encode($resultjson);
		} else {
			$this->db->trans_complete();
			$resultjson = array(
				'kode' => "",
				'message' => "Data gagal disimpan, Kode '<b style='color: red'>" . $kode . "</b>' sudah pernah digunakan."
			);
			# Something went wrong.
			$this->db->trans_rollback();
			echo json_encode($resultjson);
		}
	}

	function Update()
	{
		// $kodecabang = $this->session->userdata('mycabang');
		$pemakai = $this->session->userdata('myusername');
		// $kodecabang = "BEVOS";
		// $pemakai = "VSP";

		$kode = $this->input->post('kode');

		$this->db->trans_start(); # Starting Transaction
		$this->db->trans_strict(FALSE);

		$data = [
			'kode' => $kode,
			'nama' => $this->input->post('nama'),
			'nohp' => $this->input->post('nohp'),
			'kodespv' => $this->input->post('kodespv'),
			'namaspv' => $this->input->post('namaspv'),
			'kodecabang' => $this->input->post('kodecabang'),
			'aktif' => $this->input->post('aktif'),
			'pemakai' => $pemakai,
			'tglsimpan' => date('Y-m-d H:i:s'),
			// 'kodecabang' => $kodecabang,
		];
		// print_r($data);
		// die();

		$this->Salesman_model->UpdateData($data, $kode);

		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE) {
			$resultjson = array(
				'kode' => "",
				'message' => "Data gagal diperbarui, Kode '<b style='color: red'>" . $kode . "</b>' sudah pernah digunakan."
			);
			# Something went wrong.
			$this->db->trans_rollback();
		} else {
			$resultjson = array(
				'kode' => $kode,
				'message' => "Data berhasil diperbarui."
			);
			# Everything is Perfect. 
			# Committing data to the database.
			$this->db->trans_commit();
		}
		echo json_encode($resultjson);
	}

	function DataSlm()
	{
		$response = $this->Salesman_model->DataSlm($this->input->post('searchTerm'), $this->input->post('cabang'));
		echo json_encode($response);
	}
	
	function LoadSalesman(){
		$response = $this->Salesman_model->LoadSalesman($this->input->post('cabang'));
		echo json_encode($response);
	}
}
