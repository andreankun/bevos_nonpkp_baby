<?php
function rupiah($angka)
{

	$hasil_rupiah = number_format($angka);
	// $hasil_rupiah = "Rp. " . number_format($angka);
	return $hasil_rupiah;
}

function tanggal_indo($tanggal)
{
	$bulan = [
		1 => 'Januari',
		'Februari',
		'Maret',
		'April',
		'Mei',
		'Juni',
		'Juli',
		'Agustus',
		'September',
		'Oktober',
		'November',
		'Desember'
	];

	$pecahkan = explode('-', $tanggal);

	return $pecahkan[0] . ' ' . $bulan[(int)$pecahkan[1]] . ' ' . $pecahkan[2];
}

// $kodegudang = $this->session->userdata('mygudang');
?>

<style>
	* {
		font-family: 'Courier New', Courier, monospace;
		font-weight: bold;
	}

	.cf:before,
	.cf:after {
		content: " ";
		/* 1 */
		display: table;
		/* 2 */
	}

	.cf:after {
		clear: both;
	}

	/**
 * For IE 6/7 only
 * Include this rule to trigger hasLayout and contain floats.
 */
	.cf {
		*zoom: 1;
	}

	table {
		font-family: Verdana, Arial, Helvetica, sans-serif;
		border-collapse: collapse;
		width: 100%;
	}

	th {
		border: 1px solid #333;
		text-align: left;
		padding: 8px;
	}

	/* tr:nth-child(even) {
		background-color: #dddddd;
	} */

	div {
		font-size: 12;
		/* border: 1px solid #408080; */
	}
</style>
<title>Barang Rusak - <?= $bag->nomor ?></title>
<div class="row cf" style="margin-bottom: 10px;">
	<div class="col" style="float: left; width: 50%; text-align: left;">
		<div style="font-size: 16px;">
			<b>PT. BEVOS PRIMA CENTER</b>
		</div>
	</div>
	<div class="col" style="float: right; width: 50%; text-align: right;">
		<div style="font-size: 13px;">

		</div>
	</div>
</div>

<div class="row cf" style="margin-bottom: 10px;">
	<div class="col" style="float: left; width: 50%; font-weight: bold; text-align: left;">
		<div style="font-size: 16px;">
			BARANG RUSAK
		</div>
	</div>
	<div class="col" style="float: right; width: 50%; text-align: right;">
		<div style="font-size: 13px; text-transform: uppercase;">
			Jakarta, <?= tanggal_indo(date('d-m-Y')) ?>
		</div>
	</div>
</div>

<div class="row cf" style="margin-bottom: 2px;">
	<div class="col cf" style="float: left; width: 30%; text-align: left;">
		<div style="font-size: 13px; width: 30%; float: left;">
			Gudang
		</div>
		<div style="font-size: 13px; width: 70%; float: right;">
			: <?= $bag->lokasi ?>
		</div>
	</div>
	<div class="col" style="float: right; width: 70%; text-align: right;">
		<div style="font-size: 13px; text-transform: uppercase;">

		</div>
	</div>
</div>

<div class="row cf" style="margin-bottom: 2px;">
	<div class="col cf" style="float: left; width: 45%; text-align: left;">
		<div style="font-size: 13px; width: 30%; float: left;">
			No. BR
		</div>
		<div style="font-size: 13px; width: 80%; float: right;">
			: <?php echo $bag->nomor ?>
		</div>
	</div>
	<div class="col" style="float: right; width: 70%; text-align: right;">
		<div style="font-size: 13px; text-transform: uppercase;">

		</div>
	</div>
</div>

<div class="row cf" style="margin-bottom: 2px;">
	<div class="col cf" style="float: left; width: 30%; text-align: left;">
		<div style="font-size: 13px; width: 30%; float: left;">
			Tanggal
		</div>
		<div style="font-size: 13px; width: 70%; float: right;">
			: <?= tanggal_indo(date('d-n-y', strtotime($bag->tanggal)))  ?>
		</div>
	</div>
	<div class="col" style="float: right; width: 70%; text-align: right;">
		<div style="font-size: 13px; text-transform: uppercase;">

		</div>
	</div>
</div>

<div class="row cf" style="margin-bottom: 5px;">
	<div class="col" style="float: right; text-align: right; width: 100%;">
		<div style="font-size: 13px;">

		</div>
	</div>
</div>

<table style="margin-bottom: 35px;" cellpadding="7" cellspacing="0">
	<thead>
		<tr>
			<th style="text-align: left; font-size: 12px;">No</th>
			<th style="text-align: left; font-size: 12px;">Barcode</th>
			<th style="text-align: left; font-size: 12px;">Nama Produk</th>
			<th style="text-align: left; font-size: 12px;">Qty</th>
			<th style="text-align: left; font-size: 12px;">Satuan</th>
		</tr>
	</thead>
	<tbody>
		<?php
		$no = 1;
		foreach ($bagd as $val) : ?>
			<tr>
				<td style="font-size: 12px;"><?= $no++ ?></td>
				<td style="font-size: 12px;"><?= $val->kodebarang ?></td>
				<td style="font-size: 12px;"><?= $val->namabarang ?></td>
				<td style="font-size: 12px;"><?= $val->qty ?></td>
				<td style="font-size: 12px;"> <?= $val->kodesatuan ?></td>
			</tr>
		<?php endforeach; ?>
	</tbody>
</table>
<hr>
<div class="row">
	<div class="col-sm-8" style="float: right; width: 20%; font-size: 12px;">
		<?php $sum = 0;
		foreach ($bagd as $cell) : ?>
			<?php $sum += $cell->qty++ ?>
		<?php endforeach ?>
		<b style="float: left;">BARANG KOYAK : </b> <b style="float: right;"><?= rupiah($sum) ?></b>
	</div>
	<div class="col-sm-4" style="font-size: 12px; width: 70%; text-transform: uppercase;">

	</div>

</div>

<div class="row" style="margin-bottom: 15px;">
	<div class="col-sm-4" style="font-size: 12px; width: 40%; text-transform: uppercase;">

	</div>
</div>

<div style="float: left; font-size: 13px; width: 20%;">
	Mengetahui
</div>
<!-- <div style="float: left; font-size: 13px; width: 20%;">
	Sopir,
</div>
<div style="float: left; font-size: 13px; width: 20%;">
	Hormat Kami,
</div> -->