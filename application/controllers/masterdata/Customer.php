<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Customer extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('masterdata/Customer_model');
		$this->load->model('caridataaktif_model');
		$this->load->library('form_validation');
		$this->load->library('session');
	}

	function CariDataCustomer()
	{
		$fetch_data = $this->caridataaktif_model->make_datatables($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value'));
		$data = array();
		foreach ($fetch_data as $row) {
			$sub_array = array();
			$i = 1;
			$count = count($this->input->post('field'));
			foreach ($this->input->post('field') as $key => $value) {
				if ($i <= $count) {
					if ($i == 1) {
						$msearch = $row->$value;
						$sub_array[] = '<button class="btn btn-dark searchcustomer" data-dismiss="modal" style="margin: 0px;" data-id="' . $msearch . '"><i class="far fa-check-circle"></i></button>';
						$sub_array[] = $row->$value;
					} else {
						if ($i == $count) {
							$sub_array[] = $row->$value;
						} else {
							$sub_array[] = $row->$value;
						}
					}
					// $sub_array[] = $row->$value;
				}
				$i++;
			}
			$data[] = $sub_array;
		}
		$output = array(
			"draw"                    =>     intval($_POST["draw"]),
			"recordsTotal"          =>      $this->caridataaktif_model->get_all_data($this->input->post('nmtb')),
			"recordsFiltered"     =>     $this->caridataaktif_model->get_filtered_data($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value')),
			"data"                    =>     $data
		);
		echo json_encode($output);
	}

	function DataCustomer()
	{
		$result =  $this->Customer_model->DataCustomer($this->input->post('nomor'));
		echo json_encode($result);
	}

	function CariDataCustomer1()
	{
		$fetch_data = $this->caridataaktif_model->make_datatables($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value'));
		$data = array();
		foreach ($fetch_data as $row) {
			$sub_array = array();
			$i = 1;
			$count = count($this->input->post('field'));
			foreach ($this->input->post('field') as $key => $value) {
				if ($i <= $count) {
					if ($i == 1) {
						$msearch = $row->$value;
						$sub_array[] = '<button class="btn btn-dark searchcustomer1" data-dismiss="modal" style="margin: 0px;" data-id="' . $msearch . '"><i class="far fa-check-circle"></i></button>';
						$sub_array[] = $row->$value;
					} else {
						if ($i == $count) {
							$sub_array[] = $row->$value;
						} else {
							$sub_array[] = $row->$value;
						}
					}
					// $sub_array[] = $row->$value;
				}
				$i++;
			}
			$data[] = $sub_array;
		}
		$output = array(
			"draw"                    =>     intval($_POST["draw"]),
			"recordsTotal"          =>      $this->caridataaktif_model->get_all_data($this->input->post('nmtb')),
			"recordsFiltered"     =>     $this->caridataaktif_model->get_filtered_data($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value')),
			"data"                    =>     $data
		);
		echo json_encode($output);
	}

	function DataCustomer1()
	{
		$result =  $this->Customer_model->DataCustomer($this->input->post('nomor'));
		echo json_encode($result);
	}

	function DataCustomerDetail()
	{
		$result =  $this->Customer_model->DataDetailCustomer($this->input->post('nomor'));
		echo json_encode($result);
	}
	public function ClearChr($param)
	{
		return $result = str_replace(array('\'', '"', ',', ';', '<', '>', '_', '-', ' ', '.', '!', '?', '/', '=', '+', ']', '[', '{', '}', '@', '#', '$', '%', '^', '&', "*", '(', ')'), '', $param);
	}

	function Save()
	{
		$kodecabang = $this->input->post('cabang');
		$pemakai = $this->session->userdata('myusername');
		// $kodecabang = "BVS";
		$limit = $this->input->post('limit');

		$nomor = $this->input->post('nomor');

		if (empty($nomor)) {
			$this->db->trans_start(); # Starting Transaction
			$this->db->trans_strict(FALSE);

			// $nomor = $this->input->post('nomor');

			$get["customer"] = $this->Customer_model->GetMaxNomor($kodecabang);
			if (!$get["customer"]) {
				$nomor = $kodecabang . "000000001";
			} else {
				$lastNomor = $get['customer']->nomor;
				$lastNoUrut = substr($lastNomor, 2, 10);

				// nomor urut ditambah 1
				$nextNoUrut = $lastNoUrut + 1;
				$nomor = $kodecabang . sprintf('%09s', $nextNoUrut);
			}
			// print_r($nomor);
			// die();
			if(!empty($this->input->post('datadetail'))){
				foreach($this->input->post('datadetail') as $key => $value){
					$data = [
						'nomor' => $nomor,
						'kodesalesman' => $value['Kode'],
						'namasalesman' => $value['Nama'],
						'tglsimpan' => date('Y-m-d H:m:s'),
						'pemakai' => $pemakai
					];
					$this->Customer_model->SaveDataDetail($data);
				}
			}
			if ($this->input->post('statuspkp') == 'true' && $this->input->post('status') == 'VVIP') {
				$data = array(
					'nomor' => $nomor,
					'nama' => $this->input->post('nama'),
					'kodepos' => $this->input->post('kodepos'),
					'alamat' => $this->input->post('alamat'),
					'pengiriman' => $this->input->post('alamatpengiriman'),
					'nohp' => $this->input->post('nohp'),
					'notelp' => $this->input->post('notlp'),
					'statuspkp' => $this->input->post('statuspkp'),
					'statusppn' => $this->input->post('statusppn'),
					'npwp' => $this->input->post('npwp'),
					'ktp' => 0,
					'email' => $this->input->post('email'),
					'top' => $this->input->post('top'),
					'status' => $this->input->post('status'),
					'nama_toko' => $this->input->post('namatoko'),
					// 'limit' => $this->input->post('limit'),
					// 'kategori' => $this->input->post('kategori'),
					'gruppelanggan' => $this->input->post('gruppelanggan'),
					'rekeningtf' => $this->input->post('nocoa'),
					'tglsimpan' => date("Y-m-d H:i:s"),
					'pemakai' => $pemakai,
					'kodecabang' => $kodecabang
				);
				$this->Customer_model->SaveData($data);
			} else if ($this->input->post('statuspkp') == 'false' && $this->input->post('status') == 'VVIP') {
				$data = array(
					'nomor' => $nomor,
					'nama' => $this->input->post('nama'),
					'kodepos' => $this->input->post('kodepos'),
					'alamat' => $this->input->post('alamat'),
					'pengiriman' => $this->input->post('alamatpengiriman'),
					'nohp' => $this->input->post('nohp'),
					'notelp' => $this->input->post('notlp'),
					'nama_toko' => $this->input->post('namatoko'),
					'statuspkp' => $this->input->post('statuspkp'),
					'statusppn' => $this->input->post('statusppn'),
					'npwp' => '',
					'ktp' => $this->input->post('ktp'),
					'email' => $this->input->post('email'),
					'top' => $this->input->post('top'),
					'status' => $this->input->post('status'),
					// 'limit' => $this->input->post('limit'),
					// 'kategori' => $this->input->post('kategori'),
					'gruppelanggan' => $this->input->post('gruppelanggan'),
					'rekeningtf' => $this->input->post('nocoa'),
					'tglsimpan' => date("Y-m-d H:i:s"),
					'pemakai' => $pemakai,
					'kodecabang' => $kodecabang
				);
				// print_r($data);
				// die();
				$this->Customer_model->SaveData($data);
			} else if ($this->input->post('statuspkp') == 'true' && $this->input->post('status') == 'Regular') {
				$data = array(
					'nomor' => $nomor,
					'nama' => $this->input->post('nama'),
					'kodepos' => $this->input->post('kodepos'),
					'alamat' => $this->input->post('alamat'),
					'pengiriman' => $this->input->post('alamatpengiriman'),
					'nohp' => $this->input->post('nohp'),
					'notelp' => $this->input->post('notlp'),
					'nama_toko' => $this->input->post('namatoko'),
					'statuspkp' => $this->input->post('statuspkp'),
					'statusppn' => $this->input->post('statusppn'),
					'npwp' => $this->input->post('npwp'),
					'ktp' => 0,
					'email' => $this->input->post('email'),
					'top' => $this->input->post('top'),
					'status' => $this->input->post('status'),
					'limit' => $this->ClearChr($this->input->post('limit')),
					// 'kategori' => $this->input->post('kategori'),
					'gruppelanggan' => $this->input->post('gruppelanggan'),
					'rekeningtf' => $this->input->post('nocoa'),
					'tglsimpan' => date("Y-m-d H:i:s"),
					'pemakai' => $pemakai,
					'kodecabang' => $kodecabang
				);
				// print_r($data);
				// die();
				$this->Customer_model->SaveData($data);
			} else if ($this->input->post('statuspkp') == 'false' && $this->input->post('status') == 'Regular') {
				$data = array(
					'nomor' => $nomor,
					'nama' => $this->input->post('nama'),
					'kodepos' => $this->input->post('kodepos'),
					'alamat' => $this->input->post('alamat'),
					'pengiriman' => $this->input->post('alamatpengiriman'),
					'nohp' => $this->input->post('nohp'),
					'nama_toko' => $this->input->post('namatoko'),
					'notelp' => $this->input->post('notlp'),
					'statuspkp' => $this->input->post('statuspkp'),
					'statusppn' => $this->input->post('statusppn'),
					'npwp' => '',
					'ktp' => $this->input->post('ktp'),
					'email' => $this->input->post('email'),
					'top' => $this->input->post('top'),
					'status' => $this->input->post('status'),
					'limit' => $this->ClearChr($this->input->post('limit')),
					// 'kategori' => $this->input->post('kategori'),
					'gruppelanggan' => $this->input->post('gruppelanggan'),
					'rekeningtf' => $this->input->post('nocoa'),
					'tglsimpan' => date("Y-m-d H:i:s"),
					'pemakai' => $pemakai,
					'kodecabang' => $kodecabang
				);
				// print_r($data);
				// die();
				$this->Customer_model->SaveData($data);
			}
			
			$this->db->trans_complete();
			
			if ($this->db->trans_status() === FALSE) {
				$resultjson = array(
					'nomor' => "",
					'message' => "Data gagal disimpan, Nomor '<b style='color: red'>" . $nomor . "</b>' sudah pernah digunakan."
				);
				# Something went wrong.
				$this->db->trans_rollback();
			} else {
				$resultjson = array(
					'nomor' => $nomor,
					'message' => "Data berhasil disimpan."
				);
				# Everything is Perfect. 
				# Committing data to the database.
				$this->db->trans_commit();
			}
		} else {
			$this->db->trans_complete();
			$resultjson = array(
				'nomor' => "",
				'message' => "Data gagal disimpan, Limit tidak boleh '<b style='color: red'>" . $limit . "</b>'"
			);
			# Something went wrong.
			$this->db->trans_rollback();
		}
		echo json_encode($resultjson);
	}
	
	function Update()
	{
		$kodecabang = $this->input->post('cabang');
		$pemakai = $this->session->userdata('myusername');
		// $kodecabang = "BVS";
		
		$nomor = $this->input->post('nomor');
		
		$this->db->trans_start(); # Starting Transaction
		$this->db->trans_strict(FALSE);
		
		if(!empty($this->input->post('datadetail'))){
			$this->Customer_model->DeleteDetail($nomor);
			foreach($this->input->post('datadetail') as $key => $value){
				$data = [
					'nomor' => $nomor,
					'kodesalesman' => $value['Kode'],
					'namasalesman' => $value['Nama'],
					'tglsimpan' => date('Y-m-d H:m:s'),
					'pemakai' => $pemakai
				];
				$this->Customer_model->SaveDataDetail($data);
			}
		}

		if ($this->input->post('statuspkp') == 'true' && $this->input->post('status') == 'VVIP') {
			$data = array(
				'nomor' => $nomor,
				'nama' => $this->input->post('nama'),
				'kodepos' => $this->input->post('kodepos'),
				'alamat' => $this->input->post('alamat'),
				'pengiriman' => $this->input->post('alamatpengiriman'),
				'nohp' => $this->input->post('nohp'),
				'notelp' => $this->input->post('notlp'),
				'statuspkp' => $this->input->post('statuspkp'),
				'statusppn' => $this->input->post('statusppn'),
				'npwp' => $this->input->post('npwp'),
				'ktp' => 0,
				'email' => $this->input->post('email'),
				'top' => $this->input->post('top'),
				'status' => $this->input->post('status'),
				'nama_toko' => $this->input->post('namatoko'),
				// 'limit' => $this->input->post('limit'),
				// 'kategori' => $this->input->post('kategori'),
				'gruppelanggan' => $this->input->post('gruppelanggan'),
				'rekeningtf' => $this->input->post('nocoa'),
				'aktif' => $this->input->post('aktif'),
				'tglsimpan' => date("Y-m-d H:i:s"),
				'pemakai' => $pemakai,
				'kodecabang' => $kodecabang
			);
			$this->Customer_model->UpdateData($data, $nomor);
		} else if ($this->input->post('statuspkp') == 'false' && $this->input->post('status') == 'VVIP') {
			$data = array(
				'nomor' => $nomor,
				'nama' => $this->input->post('nama'),
				'kodepos' => $this->input->post('kodepos'),
				'alamat' => $this->input->post('alamat'),
				'pengiriman' => $this->input->post('alamatpengiriman'),
				'nohp' => $this->input->post('nohp'),
				'notelp' => $this->input->post('notlp'),
				'nama_toko' => $this->input->post('namatoko'),
				'statuspkp' => $this->input->post('statuspkp'),
				'statusppn' => $this->input->post('statusppn'),
				'npwp' => '',
				'ktp' => $this->input->post('ktp'),
				'email' => $this->input->post('email'),
				'top' => $this->input->post('top'),
				'status' => $this->input->post('status'),
				// 'limit' => $this->input->post('limit'),
				// 'kategori' => $this->input->post('kategori'),
				'gruppelanggan' => $this->input->post('gruppelanggan'),
				'rekeningtf' => $this->input->post('nocoa'),
				'aktif' => $this->input->post('aktif'),
				'tglsimpan' => date("Y-m-d H:i:s"),
				'pemakai' => $pemakai,
				'kodecabang' => $kodecabang
			);
			// print_r($data);
			// die();
			$this->Customer_model->UpdateData($data, $nomor);
		} else if ($this->input->post('statuspkp') == 'true' && $this->input->post('status') == 'Regular') {
			$data = array(
				'nomor' => $nomor,
				'nama' => $this->input->post('nama'),
				'kodepos' => $this->input->post('kodepos'),
				'alamat' => $this->input->post('alamat'),
				'pengiriman' => $this->input->post('alamatpengiriman'),
				'nohp' => $this->input->post('nohp'),
				'notelp' => $this->input->post('notlp'),
				'nama_toko' => $this->input->post('namatoko'),
				'statuspkp' => $this->input->post('statuspkp'),
				'statusppn' => $this->input->post('statusppn'),
				'npwp' => $this->input->post('npwp'),
				'ktp' => 0,
				'email' => $this->input->post('email'),
				'top' => $this->input->post('top'),
				'status' => $this->input->post('status'),
				'limit' => $this->ClearChr($this->input->post('limit')),
				// 'kategori' => $this->input->post('kategori'),
				'gruppelanggan' => $this->input->post('gruppelanggan'),
				'rekeningtf' => $this->input->post('nocoa'),
				'aktif' => $this->input->post('aktif'),
				'tglsimpan' => date("Y-m-d H:i:s"),
				'pemakai' => $pemakai,
				'kodecabang' => $kodecabang
			);
			// print_r($data);
			// die();
			$this->Customer_model->UpdateData($data, $nomor);
		} else if ($this->input->post('statuspkp') == 'false' && $this->input->post('status') == 'Regular') {
			$data = array(
				'nomor' => $nomor,
				'nama' => $this->input->post('nama'),
				'kodepos' => $this->input->post('kodepos'),
				'alamat' => $this->input->post('alamat'),
				'pengiriman' => $this->input->post('alamatpengiriman'),
				'nohp' => $this->input->post('nohp'),
				'nama_toko' => $this->input->post('namatoko'),
				'notelp' => $this->input->post('notlp'),
				'statuspkp' => $this->input->post('statuspkp'),
				'statusppn' => $this->input->post('statusppn'),
				'npwp' => '',
				'ktp' => $this->input->post('ktp'),
				'email' => $this->input->post('email'),
				'top' => $this->input->post('top'),
				'status' => $this->input->post('status'),
				'limit' => $this->ClearChr($this->input->post('limit')),
				// 'kategori' => $this->input->post('kategori'),
				'gruppelanggan' => $this->input->post('gruppelanggan'),
				'rekeningtf' => $this->input->post('nocoa'),
				'aktif' => $this->input->post('aktif'),
				'tglsimpan' => date("Y-m-d H:i:s"),
				'pemakai' => $pemakai,
				'kodecabang' => $kodecabang
			);
			// print_r($data);
			// die();
			$this->Customer_model->UpdateData($data, $nomor);
		}

		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE) {
			$resultjson = array(
				'nomor' => "",
				'message' => "Data gagal diperbarui, Nomor '<b style='color: red'>" . $nomor . "</b>' sudah pernah digunakan."
			);
			# Something went wrong.
			$this->db->trans_rollback();
		} else {
			$resultjson = array(
				'nomor' => $nomor,
				'message' => "Data berhasil diperbarui."
			);
			# Everything is Perfect. 
			# Committing data to the database.
			$this->db->trans_commit();
		}
		echo json_encode($resultjson);
	}

	function DataPelanggan()
    {
        $response = $this->Customer_model->DataPelanggan($this->input->post('searchTerm'), $this->input->post('cabang'));
        echo json_encode($response);
    }
}
