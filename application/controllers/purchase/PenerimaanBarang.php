<?php

defined('BASEPATH') or exit('No direct script access allowed');

class PenerimaanBarang extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set("Asia/Jakarta");
        $this->load->model('purchase/PenerimaanBarang_model');
        $this->load->model('global/global_model');
        $this->load->model('Caridataaktif_model');
        $this->load->library('form_validation');
        $this->load->library('session');
    }

    public function ClearChr($param)
    {
        return $result = str_replace(array('\'', '"', ',', ';', '<', '>', '_', '-', ' ', '.', '!', '?', '/', '=', '+', ']', '[', '{', '}', '@', '#', '$', '%', '^', '&', "*", '(', ')'), '', $param);
    }

    function LoadKonfigurasi()
    {
        $result = $this->global_model->GetDataGlobal("stpm_konfigurasi", array());
        echo json_encode($result);
    }

    function CariDataPenerimaanBarang()
    {
        $fetch_data = $this->Caridataaktif_model->make_datatables($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value'));
        $data = array();
        foreach ($fetch_data as $row) {
            $sub_array = array();
            $i = 1;
            $count = count($this->input->post('field'));
            foreach ($this->input->post('field') as $key => $value) {
                if ($i <= $count) {
                    if ($i == 1) {
                        $msearch = $row->$value;
                        $sub_array[] = '<button class="btn btn-dark searchpb" data-dismiss="modal" style="margin: 0px;" data-id="' . $msearch . '"><i class="far fa-check-circle"></i></button>';
                        $sub_array[] = $row->$value;
                    } else {
                        if ($i == $count) {
                            $sub_array[] = $row->$value;
                        } else {
                            $sub_array[] = $row->$value;
                        }
                    }
                    // $sub_array[] = $row->$value;
                }
                $i++;
            }
            $data[] = $sub_array;
        }
        $output = array(
            "draw"                    =>     intval($_POST["draw"]),
            "recordsTotal"          =>      $this->Caridataaktif_model->get_all_data($this->input->post('nmtb')),
            "recordsFiltered"     =>     $this->Caridataaktif_model->get_filtered_data($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value')),
            "data"                    =>     $data
        );
        echo json_encode($output);
    }

    function DataPenerimaanBarang()
    {
        $result = $this->PenerimaanBarang_model->DataPenerimaanBarang($this->input->post('nomor'));
        echo json_encode($result);
    }

    function DataPenerimaanBarangDetail()
    {
        $result = $this->PenerimaanBarang_model->DataPenerimaanBarangDetail($this->input->post('nomor'));
        echo json_encode($result);
    }

    function Save()
    {
        // $kodecabang = $this->session->userdata('mycabang');
        // $pemakai = $this->session->userdata('myusername');

        $pemakai =  $this->session->userdata('myusername');
        $nomor = $this->input->post('nomorpb');
        $cabang = $this->input->post('kodecabang');
        $kodegudang = $this->input->post('kodegudang');
        $namagudang = $this->input->post('namagudang');

        $periode = date('Y') . date('m');
        $errorvalidasi = FALSE;

        if (!empty($this->input->post('datadetail'))) {
            foreach ($this->input->post('datadetail') as $key => $value) {
                $where = array(
                    "periode" => $periode,
                    "kodecabang" => $cabang,
                    "kodebarang" => $value['Kode']
                );

                // $wherenotin = array("kodegudang" => $kodegudang);
                $datastock = $this->global_model->GetDataGlobalAndNotIN("srvt_inventorystok", $where, $kodegudang);
                if (!empty($datastock)) {
                    foreach ($datastock as $key => $values) {
                        if (($values->qtyawal + $values->qtymasuk - $values->qtykeluar) > 0) {
                            $resultjson = array(
                                'nomor' => "",
                                'message' => "Data gagal disimpan, Barang '<b style='color: red'>" . $value['Nama'] . "</b>' Stock Ada di Gudang '<b style='color: red'>" . $namagudang . "</b>' "
                            );
                            $errorvalidasi = TRUE;
                            echo json_encode($resultjson);
                            return FALSE;
                        }
                    }
                }
            }
        }

        if (empty($this->input->post('datadetail'))) {
            $resultjson = array(
                'nomor' => "",
                'message' => "Data gagal disimpan, Data Detail Tidak Ada."
            );
            $errorvalidasi = TRUE;
            echo json_encode($resultjson);
            return FALSE;
        }


        $ambilnomor = "P" . $cabang . '-' . date("Y") . '-' . date("m");
        $get["np"] = $this->PenerimaanBarang_model->GetMaxNomor($ambilnomor);

        if (!$get["np"]->nomor) {
            $nomor = $ambilnomor . "-" . "00001";
        } else {
            $lastNomor = $get['np']->nomor;
            // print_r($lastNomor);
            // die();
            $lastNoUrut = substr($lastNomor, 11, 11);
            // nomor urut ditambah 1
            $nextNoUrut = $lastNoUrut + 1;
            $nomor = $ambilnomor . "-" . sprintf('%05s', $nextNoUrut);
        }
        if (empty($nomor) || $nomor == "") {
            $resultjson = array(
                'nomor' => "",
                'message' => "Data gagal disimpan, Error Generate Nomor."
            );
            $errorvalidasi = TRUE;
            echo json_encode($resultjson);
            return FALSE;
        }

        if ($errorvalidasi == FALSE) {
            $this->db->trans_start(); # Starting Transaction
            $this->db->trans_strict(FALSE);

            foreach ($this->input->post('datadetail') as $key => $value) {
                // print_r($value);
                // die();
                $where = array(
                    "periode" => $periode,
                    "kodecabang" => $cabang,
                    "kodegudang" => $kodegudang,
                    "kodebarang" => $value['Kode']
                );
                $datastock = $this->global_model->GetDataGlobal("srvt_inventorystok", $where);
                if (!empty($datastock)) {
                    foreach ($datastock as $key => $value2) {
                        $datasodetail = array(
                            'nomor' => $nomor,
                            'kodebarang' => $value['Kode'],
                            'namabarang' => $value['Nama'],
                            'qty' => floatval(str_replace(",", "", $value['Qty'])),
                            'harga' => floatval(str_replace(",", "", $value['Price'])),
                            'disc' => $value['Disc'],
                            'discitem' => floatval(str_replace(",", "", $value['DiscPerItem'])),
                            'total' => floatval(str_replace(",", "", $value['Subtotal'])),
                            'pemakai' => $pemakai,
                            'tglsimpan' => date('Y-m-d H:i:s'),
                        );
                        $this->global_model->InsertDataGlobal("srvt_penerimaanbarangdetail", $datasodetail);
                        $qtymasukdb = $value2->qtymasuk + floatval(str_replace(",", "", $value['Qty']));
                        $setdata = "qtymasuk = '" . $qtymasukdb . "'";
                        $wheredata = "periode = '" . $periode . "' AND kodecabang = '" . $cabang . "' AND kodegudang = '" . $kodegudang . "' AND kodebarang = '" . $value['Kode'] . "'";
                        $this->PenerimaanBarang_model->UpdateDataInventory("srvt_inventorystok", $setdata, $wheredata);
                    }
                } else {
                    $datasodetail = array(
                        'nomor' => $nomor,
                        'kodebarang' => $value['Kode'],
                        'namabarang' => $value['Nama'],
                        'qty' => floatval(str_replace(",", "", $value['Qty'])),
                        'harga' => floatval(str_replace(",", "", $value['Price'])),
                        'disc' => $value['Disc'],
                        'discitem' => floatval(str_replace(",", "", $value['DiscPerItem'])),
                        'total' => floatval(str_replace(",", "", $value['Subtotal'])),
                        'pemakai' => $pemakai,
                        'tglsimpan' => date('Y-m-d H:i:s'),
                    );
                    $this->global_model->InsertDataGlobal("srvt_penerimaanbarangdetail", $datasodetail);
                    $datainventorystock = array(
                        "periode" => $periode,
                        "kodecabang" => $cabang,
                        "kodegudang" => $kodegudang,
                        "kodebarang" => $value['Kode'],
                        "qtymasuk" => floatval(str_replace(",", "", $value['Qty'])),
                        'pemakai' => $pemakai,
                        'tglsimpan' => date('Y-m-d H:i:s'),
                    );
                    $this->global_model->InsertDataGlobal("srvt_inventorystok", $datainventorystock);
                }
            }

            $dataheader = array(
                'nomor' => $nomor,
                'tanggal' => $this->input->post('tanggalpb'),
                'nomorpo' => $this->input->post('nomorpo'),
                'tglpo' => $this->input->post('tanggalpo'),
                'nomorpib' => $this->input->post('nomorpib'),
                // 'tglpib' => $this->input->post('tanggalpib'),
                'nosupplier' => $this->input->post('nomorsupplier'),
                'namasupplier' => $this->input->post('namasupplier'),
                'kodegudang' => $this->input->post('kodegudang'),
                // 'namagudang' => $this->input->post('namagudang'),
                'catatan' => $this->input->post('catatan'),

                'dpp' => floatval(str_replace(",", "", $this->input->post('dpp'))),
                'ppn' => floatval(str_replace(",", "", $this->input->post('ppn'))),
                'total' => floatval(str_replace(",", "", $this->input->post('grandtotal'))),
                'pemakai' => $pemakai,
                'tglsimpan' => date('Y-m-d H:i:s'),
                'kodecabang' => $cabang,
            );
            // print_r($dataheader);
            // die();
            $this->global_model->InsertDataGlobal("srvt_penerimaanbarang", $dataheader);

            $this->db->trans_complete();

            if ($this->db->trans_status() === FALSE) {
                $resultjson = array(
                    'nomor' => "",
                    'message' => "Data gagal disimpan, Nomor '<b style='color: red'>" . $nomor . "</b>' sudah pernah digunakan."
                );
                # Something went wrong.
                $this->db->trans_rollback();
            } else {
                $resultjson = array(
                    'nomor' => $nomor,
                    'message' => "Data berhasil disimpan."
                );
                # Everything is Perfect. 
                # Committing data to the database.
                $this->db->trans_commit();
            }

            echo json_encode($resultjson);
        }
    }

    function Update()
    {
        // $kodecabang = $this->session->userdata('mycabang');
        $pemakai = $this->session->userdata('myusername');
        // $kodecabang = "BEVOS";
        // $pemakai = "VSP";

        $nomorpb = $this->input->post('nomorpb');

        $this->db->trans_start(); # Starting Transaction
        $this->db->trans_strict(FALSE);

        $data = [
            'nomor' => $nomorpb,
            'nomorpib' => $this->input->post('nomorpib'),
        ];

        $this->PenerimaanBarang_model->UpdateData($nomorpb, $data);

        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE) {
            $resultjson = array(
                'nomor' => "",
                'message' => "Data gagal diperbarui, nomor '<b style='color: red'>" . $nomorpb . "</b>' sudah pernah digunakan."
            );
            # Something went wrong.
            $this->db->trans_rollback();
        } else {
            $resultjson = array(
                'nomor' => $nomorpb,
                'message' => "Data berhasil diperbarui."
            );
            # Everything is Perfect. 
            # Committing data to the database.
            $this->db->trans_commit();
        }
        echo json_encode($resultjson);
    }

    function Cancel()
    {

        $pemakai =  $this->session->userdata('myusername');
        $nomor = $this->input->post('nomorpb');
        $cabang = $this->input->post('kodecabang');
        $keterangan = $this->input->post('keterangan');
        $kodegudang = $this->input->post('kodegudang');
        $periode = date('Y') . date('m');
        $errorvalidasi = FALSE;

        if (!empty($this->input->post('datadetail'))) {
            foreach ($this->input->post('datadetail') as $key => $value) {
                // print_r($value);
                // die();
                $where = array(
                    "periode" => $periode,
                    "kodecabang" => $cabang,
                    "kodegudang" => $kodegudang,
                    "kodebarang" => $value['Kode']
                );
                $datastock = $this->global_model->GetDataGlobal("srvt_inventorystok", $where);
                if (empty($datastock)) {
                    $resultjson = array(
                        'nomor' => "",
                        'message' => "Data gagal dibatalkan, Barang '<b style='color: red'>" . $value['Nama'] . "</b>' Stock Tidak Ada."
                    );
                    $errorvalidasi = TRUE;
                    echo json_encode($resultjson);
                    return FALSE;
                } else {
                    foreach ($datastock as $key => $values) {
                        $stocksaatini = $values->qtyawal + $values->qtymasuk - $values->qtykeluar - $values->qtypick;
                        if (floatval(str_replace(",", "", $value['Qty'])) > $stocksaatini) {
                            $resultjson = array(
                                'nomor' => "",
                                'message' => "Data gagal dibatalkan, Barang '<b style='color: red'>" . $value['Nama'] . "</b>' Stock Tidak Cukup."
                            );
                            $errorvalidasi = TRUE;
                            echo json_encode($resultjson);
                            return FALSE;
                        }
                    }
                }
            }
        }


        if (empty($this->input->post('datadetail'))) {
            $resultjson = array(
                'nomor' => "",
                'message' => "Data gagal dibatalkan, Data Detail Tidak Ada."
            );
            $errorvalidasi = TRUE;
            echo json_encode($resultjson);
            return FALSE;
        }
        if ($this->input->post('kodecabang') == "") {
            $resultjson = array(
                'nomor' => "",
                'message' => "Data gagal dibatalkan, Cabang Kosong."
            );
            $errorvalidasi = TRUE;
            echo json_encode($resultjson);
            return FALSE;
        }
        if ($this->input->post('kodegudang') == "") {
            $resultjson = array(
                'nomor' => "",
                'message' => "Data gagal dibatalkan, Gudang Kosong."
            );
            $errorvalidasi = TRUE;
            echo json_encode($resultjson);
            return FALSE;
        }
        if ($errorvalidasi == FALSE) {
            $this->db->trans_start(); # Starting Transaction
            $this->db->trans_strict(FALSE);

            foreach ($this->input->post('datadetail') as $key => $value) {
                $where = array(
                    "periode" => $periode,
                    "kodecabang" => $cabang,
                    "kodegudang" => $kodegudang,
                    "kodebarang" => $value['Kode']
                );

                $datastock = $this->global_model->GetDataGlobal("srvt_inventorystok", $where);
                // print_r($datastock);
                // die();
                if (!empty($datastock)) {
                    foreach ($datastock as $key => $value2) {
                        $qty = $value2->qtykeluar + floatval(str_replace(",", "", $value['Qty']));
                        $setdata = "qtykeluar = '" . $qty . "'";
                        $wheredata = "periode = '" . $periode . "' AND kodecabang = '" . $cabang . "' AND kodegudang = '" . $kodegudang . "' AND kodebarang = '" . $value['Kode'] . "'";
                        $this->PenerimaanBarang_model->UpdateDataInventory("srvt_inventorystok", $setdata, $wheredata);
                    }
                }
            }

            $dataheader = array(
                'batal' => true,
                'userbatal' => $pemakai,
                'alasanbatal' => $keterangan,
                'tglbatal' => date('Y-m-d H:i:s'),
            );
            $wheredata = array("nomor" => $nomor);
            $this->global_model->UpdateDataGlobal("srvt_penerimaanbarang", $dataheader, $wheredata);
            $this->db->trans_complete();

            if ($this->db->trans_status() === FALSE) {
                $resultjson = array(
                    'nomor' => "",
                    'message' => "Data gagal dibatalkan, Nomor '<b style='color: red'>" . $nomor . "</b>' tidak ada."
                );
                # Something went wrong.
                $this->db->trans_rollback();
            } else {
                $resultjson = array(
                    'nomor' => $nomor,
                    'message' => "Data berhasil dibatalkan."
                );
                # Everything is Perfect. 
                # Committing data to the database.
                $this->db->trans_commit();
            }

            echo json_encode($resultjson);
            return FALSE;
        }
    }

    // function Update()
    // {
    //     // $kodecabang = $this->session->userdata('mycabang');
    //     // $pemakai = $this->session->userdata('myusername');

    //     $pemakai =  $this->session->userdata('myusername');
    //     $nomor = $this->input->post('nomorpb');
    //     $kodecabang = $this->session->userdata('mycabang');

    //     $this->db->trans_start(); # Starting Transaction
    //     $this->db->trans_strict(FALSE);

    //     if (!empty($this->input->post('datadetail'))) {
    //         $this->PenerimaanBarang_model->DeletePenerimaanBarang($nomor);
    //         foreach ($this->input->post('datadetail') as $key => $value) {
    //             $datadetail = [
    //                 'nomor' => $nomor,
    //                 'kodebarang' => $value['Kode'],
    //                 'namabarang' => $value['Nama'],
    //                 'qty' => $this->ClearChr($value['Qty']),
    //                 'harga' => $this->ClearChr($value['Price']),
    //                 'disc' => $this->ClearChr($value['Disc']),
    //                 'discitem' => $this->ClearChr($value['DiscPerItem']),
    //                 'total' => $this->ClearChr($value['Subtotal']),
    //                 'tglsimpan' => date('Y-m-d H:i:s'),
    //                 'pemakai' => $pemakai
    //             ];

    //             $this->PenerimaanBarang_model->SaveDataDetail($datadetail);
    //         }
    //         $data = [
    //             'nomor' => $nomor,
    //             'tanggal' => $this->input->post('tanggalpb'),
    //             'nomorpo' => $this->input->post('nomorpo'),
    //             'tglpo' => $this->input->post('tanggalpo'),
    //             'nosupplier' => $this->input->post('nomorsupplier'),
    //             'namasupplier' => $this->input->post('namasupplier'),
    //             'kodegudang' => $this->input->post('kodegudang'),
    //             'namagudang' => $this->input->post('namagudang'),
    //             'catatan' => $this->input->post('catatan'),
    //             'dpp' => $this->ClearChr($this->input->post('dpp')),
    //             'ppn' => $this->ClearChr($this->input->post('ppn')),
    //             'total' => $this->ClearChr($this->input->post('grandtotal')),
    //             'tglsimpan' => date('Y-m-d H:i:s'),
    //             'pemakai' => $pemakai
    //         ];

    //         $this->PenerimaanBarang_model->UpdateData($nomor, $data);

    //         $datahutang = [
    //             'nomor' => $nomor,
    //             'jenistransaksi' => 'KUF01',
    //             'tgltransaksi' => $this->input->post('tanggalpb'),
    //             'nomorsupplier' => $this->input->post('nomorsupplier'),
    //             'nilaihutang' => $this->ClearChr($this->input->post('grandtotal')),
    //             'kodecabang' => $kodecabang,
    //             'tglsimpan' => date('Y-m-d H:i:s'),
    //             'pemakai' => $pemakai
    //         ];

    //         $this->PenerimaanBarang_model->SaveDataHutang($nomor, $datahutang);
    //     }
    //     $this->db->trans_complete();

    //     if ($this->db->trans_status() === FALSE) {
    //         $resultjson = array(
    //             'nomor' => "",
    //             'message' => "Data gagal diperbarui, Nomor '<b style='color: red'>" . $nomor . "</b>' sudah pernah digunakan."
    //         );
    //         # Something went wrong.
    //         $this->db->trans_rollback();
    //     } else {
    //         $resultjson = array(
    //             'nomor' => $nomor,
    //             'message' => "Data berhasil diperbarui."
    //         );
    //         # Everything is Perfect. 
    //         # Committing data to the database.
    //         $this->db->trans_commit();
    //     }
    //     echo json_encode($resultjson);
    // }
}
