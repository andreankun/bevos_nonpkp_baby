<?php

defined('BASEPATH') or exit('No direct script access allowed');

class PackingBarang extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->load->model('inventory/PackingBarang_model');
        $this->load->model('global/global_model');
        $this->load->model('Caridataaktif_model');
        $this->load->library('form_validation');
        $this->load->library('session');
    }

    function CariDataPackingBarang()
    {
        $fetch_data = $this->Caridataaktif_model->make_datatables($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value'));
        $data = array();
        foreach ($fetch_data as $row) {
            $sub_array = array();
            $i = 1;
            $count = count($this->input->post('field'));
            foreach ($this->input->post('field') as $key => $value) {
                if ($i <= $count) {
                    if ($i == 1) {
                        $msearch = $row->$value;
                        $sub_array[] = '<button class="btn btn-dark searchpacking" data-dismiss="modal" style="margin: 0px;" data-id="' . $msearch . '"><i class="far fa-check-circle"></i></button>';
                        $sub_array[] = $row->$value;
                    } else {
                        if ($i == $count) {
                            $sub_array[] = $row->$value;
                        } else {
                            $sub_array[] = $row->$value;
                        }
                    }
                    // $sub_array[] = $row->$value;
                }
                $i++;
            }
            $data[] = $sub_array;
        }
        $output = array(
            "draw"                    =>     intval($_POST["draw"]),
            "recordsTotal"          =>      $this->Caridataaktif_model->get_all_data($this->input->post('nmtb')),
            "recordsFiltered"     =>     $this->Caridataaktif_model->get_filtered_data($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value')),
            "data"                    =>     $data
        );
        echo json_encode($output);
    }

    function DataPackingBarang()
    {
        $result = $this->PackingBarang_model->DataPackingBarang($this->input->post('nomor'));
        echo json_encode($result);
    }

    function DataPackingBarangDetail()
    {
        $result = $this->PackingBarang_model->DataPackingBarangDetail($this->input->post('nomor'));
        echo json_encode($result);
    }

    function CariDataSalesOrder()
    {
        $fetch_data = $this->Caridataaktif_model->make_datatables($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value'));
        $data = array();
        foreach ($fetch_data as $row) {
            $sub_array = array();
            $i = 1;
            $count = count($this->input->post('field'));
            foreach ($this->input->post('field') as $key => $value) {
                $keterangan = "";
                if ($i <= $count) {
                    if ($i == 1) {
                        $msearch = $row->$value;
                        $where = array(
                            "nomor" => $row->$value,
                            "batal" => false,
                            "kodestatus" => 0
                        );
                        $getdatastatusTombol = $this->global_model->GetDataGlobal("srvt_salesorder", $where);
                        if (!empty($getdatastatusTombol)) {
                            foreach ($getdatastatusTombol as $key => $valuetombol) {
                                if ($valuetombol->jenisjual == '1') {
                                    if ($valuetombol->grandtotal > $valuetombol->nilaiuangmuka) {
                                        if ($valuetombol->cetakproforma > 0) {
                                            $sub_array[] = '';
                                            $sub_array[1] = '<button class="btn btn-sm btn-dark cetakproforma" style="margin: 0px; background-image: linear-gradient(#000,#007bff); border-color: #000;" data-id="' . $msearch . '"><i class="fas fa-print"></i>&nbsp;Cetak Proforma</button>';
                                        } else {
                                            $keterangan = "Belum dilakukan penerimaan uang";
                                            $sub_array[] = "";
                                            $sub_array[1] = '<button class="btn btn-sm btn-dark cetakproforma" style="margin: 0px; background-color: #007bff; border-color: #007bff;" data-id="' . $msearch . '"><i class="fas fa-print"></i>&nbsp;Cetak Proforma</button>';
                                        }
                                    } else {
                                        if ($valuetombol->cetakproforma > 0) {
                                            $sub_array[] = '<button class="btn btn-dark searchso" data-dismiss="modal" style="margin: 0px;" data-id="' . $msearch . '"><i class="far fa-check-circle"></i></button>';
                                            $sub_array[1] = '<button class="btn btn-sm btn-dark cetakproforma" style="margin: 0px; background-image: linear-gradient(#000,#982374); border-color: #000;" data-id="' . $msearch . '"><i class="fas fa-print"></i>&nbsp;Cetak Proforma</button>';
                                        } else {
                                            $keterangan = "";
                                            $sub_array[] = '<button class="btn btn-dark searchso" data-dismiss="modal" style="margin: 0px;" data-id="' . $msearch . '"><i class="far fa-check-circle"></i></button>';
                                            $sub_array[1] = '<button class="btn btn-sm btn-dark cetakproforma" style="margin: 0px; background-color: #982374; border-color: #982374;" data-id="' . $msearch . '"><i class="fas fa-print"></i>&nbsp;Cetak Proforma</button>';
                                        }
                                    }
                                } else {
                                    if ($valuetombol->cetakproforma > 0) {
                                        $sub_array[] = '<button class="btn btn-dark searchso" data-dismiss="modal" style="margin: 0px;" data-id="' . $msearch . '"><i class="far fa-check-circle"></i></button>';
                                        $sub_array[1] = '<button class="btn btn-sm btn-dark cetakproforma" style="margin: 0px; background-image: linear-gradient(#000,#439856); border-color: #000;" data-id="' . $msearch . '"><i class="fas fa-print"></i>&nbsp;Cetak Proforma</button>';
                                    } else {
                                        $sub_array[] = '<button class="btn btn-dark searchso" data-dismiss="modal" style="margin: 0px;" data-id="' . $msearch . '"><i class="far fa-check-circle"></i></button>';
                                        $sub_array[1] = '<button class="btn btn-sm btn-dark cetakproforma" style="margin: 0px; background-color: #439856; border-color: #439856;" data-id="' . $msearch . '"><i class="fas fa-print"></i>&nbsp;Cetak Proforma</button>';
                                    }
                                }
                            }
                        }
                        $sub_array[] = $row->$value;
                    } else {
                        if ($i == $count) {
                            $sub_array[] = $row->$value;
                            $sub_array[] = $keterangan;
                        } else {
                            $sub_array[] = $row->$value;
                        }
                    }
                    // $sub_array[] = $row->$value;
                }
                $i++;
            }
            $data[] = $sub_array;
        }
        $output = array(
            "draw"                    =>     intval($_POST["draw"]),
            "recordsTotal"          =>      $this->Caridataaktif_model->get_all_data($this->input->post('nmtb')),
            "recordsFiltered"     =>     $this->Caridataaktif_model->get_filtered_data($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value')),
            "data"                    =>     $data
        );
        echo json_encode($output);
    }

    function DataSalesOrder()
    {
        $result = $this->PackingBarang_model->DataSalesOrder($this->input->post('nomor'));
        echo json_encode($result);
    }

    function DataSalesDetail()
    {
        $result = $this->PackingBarang_model->DataSalesDetail($this->input->post('nomor'));
        echo json_encode($result);
    }

    function CariDataSODetail()
    {
        $fetch_data = $this->Caridataaktif_model->make_datatables($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value'));
        $data = array();
        foreach ($fetch_data as $row) {
            $sub_array = array();
            $i = 1;
            $count = count($this->input->post('field'));
            foreach ($this->input->post('field') as $key => $value) {
                if ($i <= $count) {
                    if ($i == 1) {
                        $msearch = $row->$value;
                        $sub_array[] = '<button class="btn btn-dark searchsodetail" data-dismiss="modal" style="margin: 0px;" data-id="' . $msearch . '"><i class="far fa-check-circle"></i></button>';
                        $sub_array[] = $row->$value;
                    } else {
                        if ($i == $count) {
                            $sub_array[] = $row->$value;
                        } else {
                            $sub_array[] = $row->$value;
                        }
                    }
                    // $sub_array[] = $row->$value;
                }
                $i++;
            }
            $data[] = $sub_array;
        }
        $output = array(
            "draw"                    =>     intval($_POST["draw"]),
            "recordsTotal"          =>      $this->Caridataaktif_model->get_all_data($this->input->post('nmtb')),
            "recordsFiltered"     =>     $this->Caridataaktif_model->get_filtered_data($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value')),
            "data"                    =>     $data
        );
        echo json_encode($output);
    }

    function DataSODetail()
    {
        $result = $this->PackingBarang_model->DataSODetail($this->input->post('nomorso'), $this->input->post('kodebarang'));
        echo json_encode($result);
    }

    function Save()
    {
        $kodecabang = $this->session->userdata('mycabang');
        $pemakai = $this->session->userdata('myusername');

        $nomor = $this->input->post('nomor');
        $ceknomor = $this->PackingBarang_model->CekNomor($nomor);
        $this->db->trans_start(); # Starting Transaction
        $this->db->trans_strict(FALSE);

        $ambilnomor = "I" . $kodecabang . '-' . date("Y") . '-' . date("m");

        if (empty($nomor)) {
            $get["pb"] = $this->PackingBarang_model->GetMaxNomor($ambilnomor);
            if (!$get["pb"]) {
                $nomor = $ambilnomor . '-' . "00001";
            } else {
                $lastNomor = $get['pb']->nomor;
                $lastNoUrut = substr($lastNomor, 11, 11);

                // nomor urut ditambah 1
                $nextNoUrut = $lastNoUrut + 1;
                $nomor = $ambilnomor . '-' . sprintf('%05s', $nextNoUrut);
            }
            if (empty($ceknomor)) {
                if (!empty($this->input->post('datadetail'))) {
                    foreach ($this->input->post('datadetail') as $key => $value) {
                        $datadetail = [
                            'nomor' => $nomor,
                            'kodebarang' => $value['Kode'],
                            'namabarang' => $value['Nama'],
                            'qty' => floatval(str_replace(",", "", $value['Qty'])),
                            'pemakai' => $pemakai,
                            'tglsimpan' => date('Y-m-d H:i:s'),
                        ];
                        $this->PackingBarang_model->SaveDataDetail($datadetail);
                    }

                    $data = [
                        'nomor' => $nomor,
                        'tanggal' => $this->input->post('tanggal'),
                        'nomorso' => $this->input->post('nomorso'),
                        'tglso' => $this->input->post('tanggalso'),
                        'pemakai' => $pemakai,
                        'tglsimpan' => date('Y-m-d H:i:s'),
                        'kodecabang' => $kodecabang,
                    ];
                    $this->PackingBarang_model->SaveData($data);

                    $updateSO = [
                        'kodestatus' => 1
                    ];
                    $this->PackingBarang_model->UpdateStatusSO($this->input->post('nomorso'), $updateSO);
                }
            }
        }
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE) {
            $resultjson = array(
                'nomor' => "",
                'message' => "Data gagal disimpan, Nomor '<b style='color: red'>" . $nomor . "</b>' sudah pernah digunakan."
            );
            # Something went wrong.
            $this->db->trans_rollback();
        } else {
            $resultjson = array(
                'nomor' => $nomor,
                'message' => "Data berhasil disimpan."
            );
            # Everything is Perfect. 
            # Committing data to the database.
            $this->db->trans_commit();
        }
        echo json_encode($resultjson);
    }

    function Cancel()
    {

        $pemakai =  $this->session->userdata('myusername');
        $nomor = $this->input->post('nomor');
        $nomorso = $this->input->post('nomorso');
        $keterangan = $this->input->post('keterangan');
        $errorvalidasi = FALSE;
        $where = array(
            "nomor" => $nomor
        );
        $datapb = $this->global_model->GetDataGlobal("srvt_packingbarang", $where);
        if (empty($this->input->post('datadetail'))) {
            $resultjson = array(
                'nomor' => "",
                'message' => "Data gagal dibatalkan, Data Detail Tidak Ada."
            );
            $errorvalidasi = TRUE;
            echo json_encode($resultjson);
            return FALSE;
        }

        foreach ($datapb as $key => $val) {;
            if ($val->status == 't') {
                $resultjson = array(
                    'nomor' => "",
                    'message' => "Data gagal dibatalkan, Packing Barang '<b style='color: red'>" . $nomor . "</b>' Sudah Mencetak Surat Jalan."
                );
                $errorvalidasi = TRUE;
                echo json_encode($resultjson);
                return FALSE;
            }
        }

        if ($errorvalidasi == FALSE) {
            $this->db->trans_start(); # Starting Transaction
            $this->db->trans_strict(FALSE);

            $dataheader = array(
                'batal' => true,
                'userbatal' => $pemakai,
                'alasanbatal' => $keterangan,
                'tglbatal' => date('Y-m-d H:i:s'),
            );
            $wheredata = array("nomor" => $nomor);
            $this->global_model->UpdateDataGlobal("srvt_packingbarang", $dataheader, $wheredata);

            // $dataheader = array(
            //     'kodestatus' => 0
            // );
            // $wheredata = array("nomor" => $nomorso);
            $this->global_model->UpdateDataGlobal("srvt_salesorder", array('kodestatus' => 0), array("nomor" => $nomorso));

            // die();
            $this->db->trans_complete();

            if ($this->db->trans_status() === FALSE) {
                $resultjson = array(
                    'nomor' => "",
                    'message' => "Data gagal dibatalkan, Nomor '<b style='color: red'>" . $nomor . "</b>' tidak ada."
                );
                # Something went wrong.
                $this->db->trans_rollback();
            } else {
                $resultjson = array(
                    'nomor' => $nomor,
                    'message' => "Data berhasil dibatalkan."
                );
                # Everything is Perfect. 
                # Committing data to the database.
                $this->db->trans_commit();
            }

            echo json_encode($resultjson);
            return FALSE;
        }
    }
}
