<?php

defined('BASEPATH') or exit('No direct script access allowed');

class TransferStok extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set("Asia/Jakarta");
        $this->load->model('inventory/TransferStok_model');
        $this->load->model('global/global_model');
        $this->load->model('Caridataaktif_model');
        $this->load->library('form_validation');
        $this->load->library('session');
    }

    function CariDataTransferStok()
    {
        $fetch_data = $this->Caridataaktif_model->make_datatables($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value'));
        $data = array();
        foreach ($fetch_data as $row) {
            $sub_array = array();
            $i = 1;
            $count = count($this->input->post('field'));
            foreach ($this->input->post('field') as $key => $value) {
                if ($i <= $count) {
                    if ($i == 1) {
                        $msearch = $row->$value;
                        $where = array(
                            "nomor" => $row->$value,
                            "batal" => false,
                            "status" => false
                        );
                        $getdatastatusTombol = $this->global_model->GetDataGlobal("srvt_transferstock", $where);
                        if (!empty($getdatastatusTombol)) {
                            foreach ($getdatastatusTombol as $key => $vals) {
                                if ($vals->cetaktransferstok == 0) {
                                    $sub_array[] = '<button class="btn btn-dark searchts" data-dismiss="modal" style="margin: 0px;" data-id="' . $msearch . '"><i class="far fa-check-circle"></i></button>';
                                    $sub_array[1] = '<button class="btn btn-sm btn-dark cetaktransferstok" style="margin: 0px; background-color: #9A86A4; border-color: #9A86A4;" data-id="' . $msearch . '"><i class="fas fa-print"></i>&nbsp;Cetak Transfer Stok</button>';
                                } else {
                                    $sub_array[] = '<button class="btn btn-dark searchts" data-dismiss="modal" style="margin: 0px;" data-id="' . $msearch . '"><i class="far fa-check-circle"></i></button>';
                                    $sub_array[1] = '<button class="btn btn-sm btn-dark cetaktransferstok" style="margin: 0px; background-image: linear-gradient(#000,#9A86A4); border-color: #000;" data-id="' . $msearch . '"><i class="fas fa-print"></i>&nbsp;Cetak Transfer Stok</button>';
                                }
                            }
                        } else {
                            $sub_array[] = '<button class="btn btn-dark searchts" data-dismiss="modal" style="margin: 0px;" data-id="' . $msearch . '"><i class="far fa-check-circle"></i></button>';
                            $sub_array[1] = '<button class="btn btn-sm btn-dark cetaktransferstok" style="margin: 0px; background-color: #9A86A4; border-color: #9A86A4;" data-id="' . $msearch . '"><i class="fas fa-print"></i>&nbsp;Cetak Transfer Stok</button>';
                        }
                        $sub_array[] = $row->$value;
                    } else {
                        if ($i == $count) {
                            $sub_array[] = $row->$value;
                        } else {
                            $sub_array[] = $row->$value;
                        }
                    }
                    // $sub_array[] = $row->$value;
                }
                $i++;
            }
            $data[] = $sub_array;
        }
        $output = array(
            "draw"                    =>     intval($_POST["draw"]),
            "recordsTotal"          =>      $this->Caridataaktif_model->get_all_data($this->input->post('nmtb')),
            "recordsFiltered"     =>     $this->Caridataaktif_model->get_filtered_data($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value')),
            "data"                    =>     $data
        );
        echo json_encode($output);
    }

    function DataTransferStok()
    {
        $result = $this->TransferStok_model->DataTransferStok($this->input->post('nomor'));
        echo json_encode($result);
    }

    function DataTransferStokDetail()
    {
        $result = $this->TransferStok_model->DataTransferStokDetail($this->input->post('nomor'));
        echo json_encode($result);
    }

    function CariDataAsal()
    {
        $fetch_data = $this->Caridataaktif_model->make_datatables($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value'));
        $data = array();
        foreach ($fetch_data as $row) {
            $sub_array = array();
            $i = 1;
            $count = count($this->input->post('field'));
            foreach ($this->input->post('field') as $key => $value) {
                if ($i <= $count) {
                    if ($i == 1) {
                        $msearch = $row->$value;
                        $sub_array[] = '<button class="btn btn-dark searchasal" data-dismiss="modal" style="margin: 0px;" data-id="' . $msearch . '"><i class="far fa-check-circle"></i></button>';
                        $sub_array[] = $row->$value;
                    } else {
                        if ($i == $count) {
                            $sub_array[] = $row->$value;
                        } else {
                            $sub_array[] = $row->$value;
                        }
                    }
                    // $sub_array[] = $row->$value;
                }
                $i++;
            }
            $data[] = $sub_array;
        }
        $output = array(
            "draw"                    =>     intval($_POST["draw"]),
            "recordsTotal"          =>      $this->Caridataaktif_model->get_all_data($this->input->post('nmtb')),
            "recordsFiltered"     =>     $this->Caridataaktif_model->get_filtered_data($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value')),
            "data"                    =>     $data
        );
        echo json_encode($output);
    }

    function DataAsal()
    {
        $result = $this->TransferStok_model->DataAsal($this->input->post('kode'));
        echo json_encode($result);
    }

    function CariDataBarang()
    {
        $fetch_data = $this->Caridataaktif_model->make_datatables($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value'));
        $data = array();
        foreach ($fetch_data as $row) {
            $sub_array = array();
            $i = 1;
            $count = count($this->input->post('field'));
            foreach ($this->input->post('field') as $key => $value) {
                if ($i <= $count) {
                    if ($i == 1) {
                        $msearch = $row->$value;
                        $sub_array[] = '<button class="btn btn-dark searchbarang" data-dismiss="modal" style="margin: 0px;" data-id="' . $msearch . '"><i class="far fa-check-circle"></i></button>';
                        $sub_array[] = $row->$value;
                    } else {
                        if ($i == $count) {
                            $sub_array[] = $row->$value;
                        } else {
                            $sub_array[] = $row->$value;
                        }
                    }
                    // $sub_array[] = $row->$value;
                }
                $i++;
            }
            $data[] = $sub_array;
        }
        $output = array(
            "draw"                    =>     intval($_POST["draw"]),
            "recordsTotal"          =>      $this->Caridataaktif_model->get_all_data($this->input->post('nmtb')),
            "recordsFiltered"     =>     $this->Caridataaktif_model->get_filtered_data($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value')),
            "data"                    =>     $data
        );
        echo json_encode($output);
    }

    function DataBarang()
    {
        $result = $this->TransferStok_model->DataBarang($this->input->post('kode'), $this->input->post('kodecabang'), $this->input->post('kodegudang'));
        echo json_encode($result);
    }

    function CariDataCabangTujuan()
    {
        $fetch_data = $this->Caridataaktif_model->make_datatables($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value'));
        $data = array();
        foreach ($fetch_data as $row) {
            $sub_array = array();
            $i = 1;
            $count = count($this->input->post('field'));
            foreach ($this->input->post('field') as $key => $value) {
                if ($i <= $count) {
                    if ($i == 1) {
                        $msearch = $row->$value;
                        $sub_array[] = '<button class="btn btn-dark searchcbtujuan" data-dismiss="modal" style="margin: 0px;" data-id="' . $msearch . '"><i class="far fa-check-circle"></i></button>';
                        $sub_array[] = $row->$value;
                    } else {
                        if ($i == $count) {
                            $sub_array[] = $row->$value;
                        } else {
                            $sub_array[] = $row->$value;
                        }
                    }
                    // $sub_array[] = $row->$value;
                }
                $i++;
            }
            $data[] = $sub_array;
        }
        $output = array(
            "draw"                    =>     intval($_POST["draw"]),
            "recordsTotal"          =>      $this->Caridataaktif_model->get_all_data($this->input->post('nmtb')),
            "recordsFiltered"     =>     $this->Caridataaktif_model->get_filtered_data($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value')),
            "data"                    =>     $data
        );
        echo json_encode($output);
    }

    function DataCabangTujuan()
    {
        $result = $this->TransferStok_model->DataCabangTujuan($this->input->post('kode'));
        echo json_encode($result);
    }

    function CariDataGudangTujuan()
    {
        $fetch_data = $this->Caridataaktif_model->make_datatables($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value'));
        $data = array();
        foreach ($fetch_data as $row) {
            $sub_array = array();
            $i = 1;
            $count = count($this->input->post('field'));
            foreach ($this->input->post('field') as $key => $value) {
                if ($i <= $count) {
                    if ($i == 1) {
                        $msearch = $row->$value;
                        $sub_array[] = '<button class="btn btn-dark searchgdtujuan" data-dismiss="modal" style="margin: 0px;" data-id="' . $msearch . '"><i class="far fa-check-circle"></i></button>';
                        $sub_array[] = $row->$value;
                    } else {
                        if ($i == $count) {
                            $sub_array[] = $row->$value;
                        } else {
                            $sub_array[] = $row->$value;
                        }
                    }
                    // $sub_array[] = $row->$value;
                }
                $i++;
            }
            $data[] = $sub_array;
        }
        $output = array(
            "draw"                    =>     intval($_POST["draw"]),
            "recordsTotal"          =>      $this->Caridataaktif_model->get_all_data($this->input->post('nmtb')),
            "recordsFiltered"     =>     $this->Caridataaktif_model->get_filtered_data($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value')),
            "data"                    =>     $data
        );
        echo json_encode($output);
    }

    function DataGudangTujuan()
    {
        $result = $this->TransferStok_model->DataGudangTujuan($this->input->post('kode'));
        echo json_encode($result);
    }

    public function ClearChr($param)
    {
        return $result = str_replace(array('\'', '"', ',', ';', '<', '>', '_', '-', ' ', '.', '!', '?', '/', '=', '+', ']', '[', '{', '}', '@', '#', '$', '%', '^', '&', "*", '(', ')'), '', $param);
    }

    function Save()
    {
        $kodecabang = $this->session->userdata('mycabang');
        $pemakai = $this->session->userdata('myusername');
        $periode = date("Y") . date("m");
        $kodegudang = $this->input->post('kodegdasal');
        $nomor = $this->input->post('notransfer');

        $this->db->trans_start(); # Starting Transaction
        $this->db->trans_strict(FALSE);

        $ambilnomor = "L" . $kodecabang . "-" . date('Y') . "-" . date('m');

        if (empty($nomor)) {
            $get["ts"] = $this->TransferStok_model->GetMaxNomor($ambilnomor);
            if (!$get["ts"]) {
                $nomor = $ambilnomor . "-" . "00001";
            } else {
                $lastNomor = $get['ts']->nomor;
                $lastNoUrut = substr($lastNomor, 11, 11);

                // nomor urut ditambah 1
                $nextNoUrut = $lastNoUrut + 1;
                $nomor = $ambilnomor . "-" . sprintf('%05s', $nextNoUrut);
            }

            if (!empty($this->input->post('datadetail'))) {
                foreach ($this->input->post('datadetail') as $key => $value) {
                    $datadetail = [
                        'nomor' => $nomor,
                        'kodebarang' => $value['Kode'],
                        'namabarang' => $value['Nama'],
                        'qtysistem' => $this->ClearChr($value['QtySistem']),
                        'qty' => $this->ClearChr($value['Qty']),
                        'tglsimpan' => date('Y-m-d H:i:s'),
                        'pemakai' => $pemakai
                    ];
                    $this->TransferStok_model->SaveDataDetail($datadetail);

                    $getinv = $this->TransferStok_model->DataInv($periode, $value['Kode'], $kodecabang, $kodegudang);
                    foreach ($getinv as $key => $values) {
                        $datainv = array(
                            'qtypick' => $values->qtypick + $this->ClearChr($value['Qty'])
                        );
                        $this->TransferStok_model->UpdateStok($periode, $value['Kode'], $kodecabang, $kodegudang, $datainv);
                    }
                }
                $data = [
                    'nomor' => $nomor,
                    'tanggal' => $this->input->post('tanggal'),
                    'pilihan' => $this->input->post('jenistujuan'),
                    'kodecbasal' => $this->input->post('kodecbasal'),
                    'kodegdasal' => $this->input->post('kodegdasal'),
                    'kodecbtujuan' => $this->input->post('kodecbtujuan'),
                    'kodegdtujuan' => $this->input->post('kodegdtujuan'),
                    'keterangan' => $this->input->post('keterangan'),
                    'nosupplier' => $this->input->post('nomorsupplier'),
                    'namasupplier' => $this->input->post('namasupplier'),
                    'tglsimpan' => date('Y-m-d H:i:s'),
                    'pemakai' => $pemakai
                ];
                // print_r($data);
                $this->TransferStok_model->SaveData($data);
            }
            // die();
            $this->db->trans_complete();

            if ($this->db->trans_status() === FALSE) {
                $resultjson = array(
                    'nomor' => "",
                    'message' => "Data gagal disimpan, Nomor '<b style='color: red'>" . $nomor . "</b>' sudah pernah digunakan."
                );
                # Something went wrong.
                $this->db->trans_rollback();
            } else {
                $resultjson = array(
                    'nomor' => $nomor,
                    'message' => "Data berhasil disimpan."
                );
                # Everything is Perfect. 
                # Committing data to the database.
                $this->db->trans_commit();
            }
            echo json_encode($resultjson);
        } else {
            $resultjson = array(
                'nomor' => "",
                'message' => "Data gagal disimpan, Nomor '<b style='color: red'>" . $nomor . "</b>' sudah pernah digunakan."
            );
            # Something went wrong.
            $this->db->trans_rollback();
        }
    }

    function Update()
    {
        $kodecabang = $this->session->userdata('mycabang');
        $pemakai = $this->session->userdata('myusername');
        $nomor = $this->input->post('nomor');

        $this->db->trans_start(); # Starting Transaction
        $this->db->trans_strict(FALSE);

        if (!empty($this->input->post('datadetail'))) {
            $this->TransferStok_model->DeleteTransferStok($nomor);
            foreach ($this->input->post('datadetail') as $key => $value) {
                $datadetail = [
                    'nomor' => $nomor,
                    'kodebarang' => $value['Kode'],
                    'namabarang' => $value['Nama'],
                    'qtysistem' => $this->ClearChr($value['QtySistem']),
                    'qty' => $this->ClearChr($value['Qty']),
                    'tglsimpan' => date('Y-m-d H:i:s'),
                    'pemakai' => $pemakai
                ];
                $this->TransferStok_model->SaveDataDetail($datadetail);
            }
            $data = [
                'nomor' => $nomor,
                'tanggal' => $this->input->post('tanggal'),
                'pilihan' => $this->input->post('jenistujuan'),
                'kodecbasal' => $this->input->post('kodecbasal'),
                'kodegdasal' => $this->input->post('kodegdasal'),
                'kodecbtujuan' => $this->input->post('kodecbtujuan'),
                'kodegdtujuan' => $this->input->post('kodegdtujuan'),
                'keterangan' => $this->input->post('keterangan'),
                'nosupplier' => $this->input->post('nomorsupplier'),
                'namasupplier' => $this->input->post('namasupplier'),
                'tglsimpan' => date('Y-m-d H:i:s'),
                'pemakai' => $pemakai
            ];
            $this->TransferStok_model->UpdateData($nomor, $data);
        }
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE) {
            $resultjson = array(
                'nomor' => "",
                'message' => "Data gagal diperbarui, Nomor '<b style='color: red'>" . $nomor . "</b>' sudah pernah digunakan."
            );
            # Something went wrong.
            $this->db->trans_rollback();
        } else {
            $resultjson = array(
                'nomor' => $nomor,
                'message' => "Data berhasil diperbarui."
            );
            # Everything is Perfect. 
            # Committing data to the database.
            $this->db->trans_commit();
        }
        echo json_encode($resultjson);
    }

    function Cancel()
    {

        $kodecabang =  $this->session->userdata('mycabang');
        $pemakai =  $this->session->userdata('myusername');
        $periode = date('Y') . date('m');
        $kodegudang = $this->input->post('kodegdasal');
        $nomor = $this->input->post('nomor');
        $keterangan = $this->input->post('keterangan');
        $errorvalidasi = FALSE;
        $where = array(
            "nomor" => $nomor
        );
        $datatf = $this->global_model->GetDataGlobal("srvt_transferstock", $where);
        if (empty($this->input->post('datadetail'))) {
            $resultjson = array(
                'nomor' => "",
                'message' => "Data gagal dibatalkan, Data Detail Tidak Ada."
            );
            $errorvalidasi = TRUE;
            echo json_encode($resultjson);
            return FALSE;
        }

        foreach ($datatf as $key => $val) {;
            if ($val->kodestatus > 0) {
                $resultjson = array(
                    'nomor' => "",
                    'message' => "Data gagal dibatalkan, Transfer Stok Barang dengan Nomor '<b style='color: red'>" . $nomor . "</b>' Sudah Melakukan Pengiriman Barang."
                );
                $errorvalidasi = TRUE;
                echo json_encode($resultjson);
                return FALSE;
            }
        }

        if ($errorvalidasi == FALSE) {
            $this->db->trans_start(); # Starting Transaction
            $this->db->trans_strict(FALSE);

            foreach ($this->input->post('datadetail') as $key => $value) {
                $getinv = $this->TransferStok_model->DataInv($periode, $value['Kode'], $kodecabang, $kodegudang);
                foreach ($getinv as $val) {
                    $data = array(
                        'qtypick' => $val->qtypick - $this->ClearChr($value['Qty'])
                    );

                    $this->TransferStok_model->UpdateStok($periode, $value['Kode'], $kodecabang, $kodegudang, $data);
                }
            }

            $dataheader = array(
                'batal' => true,
                'userbatal' => $pemakai,
                'alasanbatal' => $keterangan,
                'tglbatal' => date('Y-m-d H:i:s')
            );
            $wheredata = array("nomor" => $nomor);
            $this->global_model->UpdateDataGlobal("srvt_transferstock", $dataheader, $wheredata);
            $this->db->trans_complete();

            if ($this->db->trans_status() === FALSE) {
                $resultjson = array(
                    'nomor' => "",
                    'message' => "Data gagal dibatalkan, Nomor '<b style='color: red'>" . $nomor . "</b>' tidak ada."
                );
                # Something went wrong.
                $this->db->trans_rollback();
            } else {
                $resultjson = array(
                    'nomor' => $nomor,
                    'message' => "Data berhasil dibatalkan."
                );
                # Everything is Perfect. 
                # Committing data to the database.
                $this->db->trans_commit();
            }

            echo json_encode($resultjson);
            return FALSE;
        }
    }

    function CetakTransferStokUpdate()
    {
        $kodecabang = $this->session->userdata('mycabang');
        $pemakai = $this->session->userdata('myusername');

        $nomor = $this->input->post('nomor');

        $this->db->trans_start(); # Starting Transaction
        $this->db->trans_strict(FALSE);

        $datatf = $this->TransferStok_model->DataTransferStok($nomor);

        foreach ($datatf as $ambildata) {
            $cetaktransferstok = $ambildata->cetaktransferstok;
            $data = [
                'cetaktransferstok' => $cetaktransferstok + 1
            ];
            $this->TransferStok_model->UpdateData($nomor, $data);
        }


        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE) {
            $resultjson = array(
                'nomor' => "",
                'message' => "Data gagal di update, Nomor '<b style='color: red'>" . $nomor . "</b>' sudah pernah digunakan."
            );
            # Something went wrong.
            $this->db->trans_rollback();
        } else {
            $resultjson = array(
                'nomor' => $nomor,
                'message' => "Data berhasil di update."
            );
            # Everything is Perfect. 
            # Committing data to the database.
            $this->db->trans_commit();
        }
        echo json_encode($resultjson);
    }
}
