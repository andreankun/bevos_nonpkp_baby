<?php

defined('BASEPATH') or exit('No direct script access allowed');

class ReqRepackStock extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('inventory/ReqRepackStock_model');
		$this->load->model('global/global_model');
		$this->load->model('Caridataaktif_model');
		$this->load->library('form_validation');
		$this->load->library('session');
	}

	function CariDataReq()
	{
		$fetch_data = $this->Caridataaktif_model->make_datatables($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value'));
		$data = array();
		foreach ($fetch_data as $row) {
			$sub_array = array();
			$i = 1;
			$count = count($this->input->post('field'));
			foreach ($this->input->post('field') as $key => $value) {
				if ($i <= $count) {
					if ($i == 1) {
						$msearch = $row->$value;
						$sub_array[] = '<button class="btn btn-dark searchreq" data-dismiss="modal" style="margin: 0px;" data-id="' . $msearch . '"><i class="far fa-check-circle"></i></button>';
						$sub_array[] = $row->$value;
					} else {
						if ($i == $count) {
							$sub_array[] = $row->$value;
						} else {
							$sub_array[] = $row->$value;
						}
					}
					// $sub_array[] = $row->$value;
				}
				$i++;
			}
			$data[] = $sub_array;
		}
		$output = array(
			"draw"                    =>     intval($_POST["draw"]),
			"recordsTotal"          =>      $this->Caridataaktif_model->get_all_data($this->input->post('nmtb')),
			"recordsFiltered"     =>     $this->Caridataaktif_model->get_filtered_data($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value')),
			"data"                    =>     $data
		);
		echo json_encode($output);
	}

	function DataReq()
	{
		$result = $this->ReqRepackStock_model->DataReq($this->input->post('nomor'));
		echo json_encode($result);
	}

	function DataReqDetail()
	{
		$result = $this->ReqRepackStock_model->DataReqDetail($this->input->post('nomor'));
		echo json_encode($result);
	}

	function CariDataBarang()
	{
		$fetch_data = $this->Caridataaktif_model->make_datatables($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value'));
		$data = array();
		foreach ($fetch_data as $row) {
			$sub_array = array();
			$i = 1;
			$count = count($this->input->post('field'));
			foreach ($this->input->post('field') as $key => $value) {
				if ($i <= $count) {
					if ($i == 1) {
						$msearch = $row->$value;
						$sub_array[] = '<button class="btn btn-dark searchbarang" data-dismiss="modal" style="margin: 0px;" data-id="' . $msearch . '"><i class="far fa-check-circle"></i></button>';
						$sub_array[] = $row->$value;
					} else {
						if ($i == $count) {
							$sub_array[] = $row->$value;
						} else {
							$sub_array[] = $row->$value;
						}
					}
					// $sub_array[] = $row->$value;
				}
				$i++;
			}
			$data[] = $sub_array;
		}
		$output = array(
			"draw"                    =>     intval($_POST["draw"]),
			"recordsTotal"          =>      $this->Caridataaktif_model->get_all_data($this->input->post('nmtb')),
			"recordsFiltered"     =>     $this->Caridataaktif_model->get_filtered_data($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value')),
			"data"                    =>     $data
		);
		echo json_encode($output);
	}

	function DataBarang()
	{
		$result = $this->ReqRepackStock_model->DataBarang($this->input->post('kode'), $this->input->post('kodegudang'));
		echo json_encode($result);
	}

	public function ClearChr($param)
	{
		return $result = str_replace(array('\'', '"', ',', ';', '<', '>', '_', '-', ' ', '.', '!', '?', '/', '=', '+', ']', '[', '{', '}', '@', '#', '$', '%', '^', '&', "*", '(', ')'), '', $param);
	}

	function Save()
	{
		$kodecabang = $this->session->userdata('mycabang');
		$kodegudang = $this->input->post('kodegudang');
		$periode = date('Y') . date('m');
		$pemakai = $this->session->userdata('myusername');

		$nomor = $this->input->post('nomor');
		$ceknomor = $this->ReqRepackStock_model->CekReq($nomor);

		$this->db->trans_start(); # Starting Transaction
		$this->db->trans_strict(FALSE);

		$ambilnomor = "K" . $kodecabang . "-" . date("Y") . "-" . date("m");

		if (empty($nomor)) {
			$get["rrs"] = $this->ReqRepackStock_model->GetMaxNomor($ambilnomor);
			if (!$get["rrs"]) {
				$nomor = $ambilnomor . "-" . "00001";
			} else {
				$lastNomor = $get['rrs']->nomor;
				$lastNoUrut = substr($lastNomor, 11, 11);

				// nomor urut ditambah 1
				$nextNoUrut = $lastNoUrut + 1;
				$nomor = $ambilnomor . "-" . sprintf('%05s', $nextNoUrut);
			}
			if (empty($ceknomor)) {
				if (!empty($this->input->post('datadetail'))) {
					foreach ($this->input->post('datadetail') as $key => $value) {
						$datadetail = [
							'nomor' => $nomor,
							'kodebarang' => $value['Kode'],
							'namabarang' => $value['Nama'],
							'qtysistem' => $this->ClearChr($value['QtySistem']),
							'qty' => $this->ClearChr($value['Qty']),
							'pemakai' => $pemakai,
							'tglsimpan' => date('Y-m-d H:i:s'),
						];
						$this->ReqRepackStock_model->SaveDataDetail($datadetail);

						$getdatainv = $this->ReqRepackStock_model->DataInv($periode, $value['Kode'], $kodecabang, $kodegudang);
						foreach ($getdatainv as $val) {
							$datainv = [
								'qtypick' => $val->qtypick + $this->ClearChr($value['Qty'])
							];

							$this->ReqRepackStock_model->UpdateStok($periode, $value['Kode'], $kodecabang, $kodegudang, $datainv);
						}
					}
					$data = [
						'nomor' => $nomor,
						'tanggal' => $this->input->post('tanggal'),
						'keterangan' => $this->input->post('keterangan'),
						'pemakai' => $pemakai,
						'tglsimpan' => date('Y-m-d H:i:s'),
						'kodecabang' => $kodecabang,
						'kodegudang' => $kodegudang,
					];
					$this->ReqRepackStock_model->SaveData($data);
				}
			}
		}
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE) {
			$resultjson = array(
				'nomor' => "",
				'message' => "Data gagal disimpan, Nomor '<b style='color: red'>" . $nomor . "</b>' sudah pernah digunakan."
			);
			# Something went wrong.
			$this->db->trans_rollback();
		} else {
			$resultjson = array(
				'nomor' => $nomor,
				'message' => "Data berhasil disimpan."
			);
			# Everything is Perfect. 
			# Committing data to the database.
			$this->db->trans_commit();
		}
		echo json_encode($resultjson);
	}

	function Update()
	{
		$kodecabang = $this->session->userdata('mycabang');
		$pemakai = $this->session->userdata('myusername');
		$kodegudang = $this->input->post('lokasigdg');
		$periode = date('Y') . date('m');

		$nomor = $this->input->post('nomor');

		$this->db->trans_start(); # Starting Transaction
		$this->db->trans_strict(FALSE);

		if (!empty($this->input->post('datadetail'))) {
			$datareq = $this->ReqRepackStock_model->DataReqDetail($nomor);
			foreach ($datareq as $key => $valupd) {
				$getdatainvupd = $this->ReqRepackStock_model->DataInv($periode, $valupd->kodebarang, $kodecabang, $kodegudang);
				foreach ($getdatainvupd as $val) {
					$datainvupd = [
						'qtypick' => $val->qtypick - $valupd->qty
					];
					// print_r($datainvupd);
					$this->ReqRepackStock_model->UpdateStok($periode, $valupd->kodebarang, $kodecabang, $kodegudang, $datainvupd);
				}
			}
			$this->ReqRepackStock_model->DeleteDetail($nomor);
			foreach ($this->input->post('datadetail') as $key => $value) {
				$datadetail = [
					'nomor' => $nomor,
					'kodebarang' => $value['Kode'],
					'namabarang' => $value['Nama'],
					'qtysistem' => $this->ClearChr($value['QtySistem']),
					'qty' => $this->ClearChr($value['Qty']),
					'pemakai' => $pemakai,
					'tglsimpan' => date('Y-m-d H:i:s'),
				];
				$this->ReqRepackStock_model->SaveDataDetail($datadetail);

				$getdatainv = $this->ReqRepackStock_model->DataInv($periode, $value['Kode'], $kodecabang, $kodegudang);
				foreach ($getdatainv as $val) {
					$datainv = [
						'qtypick' => $val->qtypick + $this->ClearChr($value['Qty'])
					];

					$this->ReqRepackStock_model->UpdateStok($periode, $value['Kode'], $kodecabang, $kodegudang, $datainv);
				}
			}
			$data = [
				'nomor' => $nomor,
				'tanggal' => $this->input->post('tanggal'),
				'keterangan' => $this->input->post('keterangan'),
				'pemakai' => $pemakai,
				'tglsimpan' => date('Y-m-d H:i:s')
			];
			$this->ReqRepackStock_model->UpdateData($nomor, $data);
		}
		// die();		
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE) {
			$resultjson = array(
				'nomor' => "",
				'message' => "Data gagal disimpan, Nomor '<b style='color: red'>" . $nomor . "</b>' sudah pernah digunakan."
			);
			# Something went wrong.
			$this->db->trans_rollback();
		} else {
			$resultjson = array(
				'nomor' => $nomor,
				'message' => "Data berhasil disimpan."
			);
			# Everything is Perfect. 
			# Committing data to the database.
			$this->db->trans_commit();
		}
		echo json_encode($resultjson);
	}

	function Cancel()
	{

		$kodecabang =  $this->session->userdata('mycabang');
		$pemakai =  $this->session->userdata('myusername');
		$nomor = $this->input->post('nomor');
		$keterangan = $this->input->post('keterangan');
		$errorvalidasi = FALSE;
		$where = array(
			"nomor" => $nomor
		);
		$dataproses = $this->global_model->GetDataGlobal("srvt_reqrepackstock", $where);
		if (empty($this->input->post('datadetail'))) {
			$resultjson = array(
				'nomor' => "",
				'message' => "Data gagal dibatalkan, Data Detail Tidak Ada."
			);
			$errorvalidasi = TRUE;
			echo json_encode($resultjson);
			return FALSE;
		}

		foreach ($dataproses as $key => $val) {;
			if ($val->proses == 't') {
				$resultjson = array(
					'nomor' => "",
					'message' => "Data gagal dibatalkan, Req. Repack Barang dengan Nomor '<b style='color: red'>" . $nomor . "</b>' Sudah Melakukan Proses Repack."
				);
				$errorvalidasi = TRUE;
				echo json_encode($resultjson);
				return FALSE;
			}
		}

		if ($errorvalidasi == FALSE) {
			$this->db->trans_start(); # Starting Transaction
			$this->db->trans_strict(FALSE);

			$dataheader = array(
				'batal' => true,
				'userbatal' => $pemakai,
				'alasanbatal' => $keterangan,
				'tglbatal' => date('Y-m-d H:i:s'),
				'kodecabang' => $kodecabang
			);
			$wheredata = array("nomor" => $nomor);
			$this->global_model->UpdateDataGlobal("srvt_reqrepackstock", $dataheader, $wheredata);
			$this->db->trans_complete();

			if ($this->db->trans_status() === FALSE) {
				$resultjson = array(
					'nomor' => "",
					'message' => "Data gagal dibatalkan, Nomor '<b style='color: red'>" . $nomor . "</b>' tidak ada."
				);
				# Something went wrong.
				$this->db->trans_rollback();
			} else {
				$resultjson = array(
					'nomor' => $nomor,
					'message' => "Data berhasil dibatalkan."
				);
				# Everything is Perfect. 
				# Committing data to the database.
				$this->db->trans_commit();
			}

			echo json_encode($resultjson);
			return FALSE;
		}
	}
}
