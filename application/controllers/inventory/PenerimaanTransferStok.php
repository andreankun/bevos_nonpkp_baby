<?php

defined('BASEPATH') or exit('No direct script access allowed');

class PenerimaanTransferStok extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set("Asia/Jakarta");
        $this->load->model('inventory/PenerimaanTransferStok_model');
        $this->load->model('global/global_model');
        $this->load->model('Caridataaktif_model');
        $this->load->library('form_validation');
        $this->load->library('session');
    }

    function CariDataBarang()
    {
        $fetch_data = $this->Caridataaktif_model->make_datatables($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value'));
        $data = array();
        foreach ($fetch_data as $row) {
            $sub_array = array();
            $i = 1;
            $count = count($this->input->post('field'));
            foreach ($this->input->post('field') as $key => $value) {
                if ($i <= $count) {
                    if ($i == 1) {
                        $msearch = $row->$value;
                        $sub_array[] = '<button class="btn btn-dark searchbarang" data-dismiss="modal" style="margin: 0px;" data-id="' . $msearch . '"><i class="far fa-check-circle"></i></button>';
                        $sub_array[] = $row->$value;
                    } else {
                        if ($i == $count) {
                            $sub_array[] = $row->$value;
                        } else {
                            $sub_array[] = $row->$value;
                        }
                    }
                    // $sub_array[] = $row->$value;
                }
                $i++;
            }
            $data[] = $sub_array;
        }
        $output = array(
            "draw"                    =>     intval($_POST["draw"]),
            "recordsTotal"          =>      $this->Caridataaktif_model->get_all_data($this->input->post('nmtb')),
            "recordsFiltered"     =>     $this->Caridataaktif_model->get_filtered_data($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value')),
            "data"                    =>     $data
        );
        echo json_encode($output);
    }

    function DataBarang()
    {
        $result = $this->PenerimaanTransferStok_model->DataBarang($this->input->post('nomor'), $this->input->post('kode'));
        echo json_encode($result);
    }

    function CariDataPenerimaanTransferStok()
    {
        $fetch_data = $this->Caridataaktif_model->make_datatables($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value'));
        $data = array();
        foreach ($fetch_data as $row) {
            $sub_array = array();
            $i = 1;
            $count = count($this->input->post('field'));
            foreach ($this->input->post('field') as $key => $value) {
                if ($i <= $count) {
                    if ($i == 1) {
                        $msearch = $row->$value;
                        $sub_array[] = '<button class="btn btn-dark searchpts" data-dismiss="modal" style="margin: 0px;" data-id="' . $msearch . '"><i class="far fa-check-circle"></i></button>';
                        $sub_array[] = $row->$value;
                    } else {
                        if ($i == $count) {
                            $sub_array[] = $row->$value;
                        } else {
                            $sub_array[] = $row->$value;
                        }
                    }
                    // $sub_array[] = $row->$value;
                }
                $i++;
            }
            $data[] = $sub_array;
        }
        $output = array(
            "draw"                    =>     intval($_POST["draw"]),
            "recordsTotal"          =>      $this->Caridataaktif_model->get_all_data($this->input->post('nmtb')),
            "recordsFiltered"     =>     $this->Caridataaktif_model->get_filtered_data($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value')),
            "data"                    =>     $data
        );
        echo json_encode($output);
    }

    function DataPenerimaanTransferStok()
    {
        $result = $this->PenerimaanTransferStok_model->DataPenerimaanStok($this->input->post('nomor'));
        echo json_encode($result);
    }

    function DataPenerimaanTransferStokDetail()
    {
        $result = $this->PenerimaanTransferStok_model->DataPenerimaanTransferStokDetail($this->input->post('nomor'));
        echo json_encode($result);
    }

    function CariDataSuratJalanGudang()
    {
        $fetch_data = $this->Caridataaktif_model->make_datatables($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value'));
        $data = array();
        foreach ($fetch_data as $row) {
            $sub_array = array();
            $i = 1;
            $count = count($this->input->post('field'));
            foreach ($this->input->post('field') as $key => $value) {
                if ($i <= $count) {
                    if ($i == 1) {
                        $msearch = $row->$value;
                        $sub_array[] = '<button class="btn btn-dark searchsj" data-dismiss="modal" style="margin: 0px;" data-id="' . $msearch . '"><i class="far fa-check-circle"></i></button>';
                        $sub_array[] = $row->$value;
                    } else {
                        if ($i == $count) {
                            $sub_array[] = $row->$value;
                        } else {
                            $sub_array[] = $row->$value;
                        }
                    }
                    // $sub_array[] = $row->$value;
                }
                $i++;
            }
            $data[] = $sub_array;
        }
        $output = array(
            "draw"                    =>     intval($_POST["draw"]),
            "recordsTotal"          =>      $this->Caridataaktif_model->get_all_data($this->input->post('nmtb')),
            "recordsFiltered"     =>     $this->Caridataaktif_model->get_filtered_data($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value')),
            "data"                    =>     $data
        );
        echo json_encode($output);
    }

    function DataSuratJalanGudang()
    {
        $result = $this->PenerimaanTransferStok_model->DataSuratJalanGudang($this->input->post('nomor'));
        echo json_encode($result);
    }

    function DataSuratJalanGudangDetail()
    {
        $result = $this->PenerimaanTransferStok_model->DataSuratJalanGudangDetail($this->input->post('nomor'));
        echo json_encode($result);
    }

    public function ClearChr($param)
    {
        return $result = str_replace(array('\'', '"', ',', ';', '<', '>', '_', '-', ' ', '.', '!', '?', '/', '=', '+', ']', '[', '{', '}', '@', '#', '$', '%', '^', '&', "*", '(', ')'), '', $param);
    }

    function Save()
    {
        $kodecabang = $this->session->userdata('mycabang');
        $pemakai = $this->session->userdata('myusername');
        $nomor = $this->input->post('nomor');
        $kodegdasal = $this->input->post('kodegdasal');
        $kodegdtujuan = $this->input->post('kodegdtujuan');
        $kodecbasal = $this->input->post('kodecbasal');
        $kodecbtujuan = $this->input->post('kodecbtujuan');
        $periode = date("Y") . date("m");

        $this->db->trans_start(); # Starting Transaction
        $this->db->trans_strict(FALSE);

        $ambilnomor = "A" . $kodecabang . "-" . date('Y') . "-" . date('m');

        if (empty($nomor)) {
            $get["pts"] = $this->PenerimaanTransferStok_model->GetMaxNomor($ambilnomor);
            if (!$get["pts"]) {
                $nomor = $ambilnomor . "-" . "00001";
            } else {
                $lastNomor = $get['pts']->nomor;
                $lastNoUrut = substr($lastNomor, 11, 11);

                // nomor urut ditambah 1
                $nextNoUrut = $lastNoUrut + 1;
                $nomor = $ambilnomor . "-" . sprintf('%05s', $nextNoUrut);
            }

            if (!empty($this->input->post('datadetail'))) {
                foreach ($this->input->post('datadetail') as $key => $value) {

                    $datadetail = [
                        'nomor' => $nomor,
                        'kodebarang' => $value['Kode'],
                        'namabarang' => $value['Nama'],
                        'qty' => $this->ClearChr($value['Qty']),
                        'tglsimpan' => date('Y-m-d H:i:s'),
                        'pemakai' => $pemakai
                    ];
                    $this->PenerimaanTransferStok_model->SaveDataDetail($datadetail);

                    #Gudang A
                    $getdatainvasal = $this->PenerimaanTransferStok_model->DataInv($periode, $value['Kode'], $kodecbasal, $kodegdasal);
                    foreach ($getdatainvasal as $key => $values) {

                        $datainvasal = [
                            'qtypick' => $values->qtypick - $this->ClearChr($value['Qty']),
                            'qtykeluar' => $values->qtykeluar + $this->ClearChr($value['Qty'])
                        ];
                        // print_r($datainvasal);
                        $this->PenerimaanTransferStok_model->UpdateStok($periode, $value['Kode'], $kodecbasal, $kodegdasal, $datainvasal);
                    }

                    $getdatainvtujuan = $this->PenerimaanTransferStok_model->DataInv($periode, $value['Kode'], $kodecbtujuan, $kodegdtujuan);
                    if (!empty($getdatainvtujuan)) {
                        foreach ($getdatainvtujuan as $key => $val) {
                            $datainvtujuan = [
                                // 'qtypick' => $val->qtypick - $this->ClearChr($value['Qty']),
                                'qtymasuk' => $val->qtymasuk + $this->ClearChr($value['Qty'])
                            ];
                            $this->PenerimaanTransferStok_model->UpdateStok($periode, $value['Kode'], $kodecbtujuan, $kodegdtujuan, $datainvtujuan);
                        }
                    } else {
                        $datainventorystock = array(
                            "periode" => $periode,
                            "kodecabang" => $kodecbtujuan,
                            "kodegudang" => $kodegdtujuan,
                            "kodebarang" => $value['Kode'],
                            "qtymasuk" => $this->ClearChr($value['Qty']),
                            'pemakai' => $pemakai,
                            'tglsimpan' => date('Y-m-d H:i:s'),
                        );
                        $this->global_model->InsertDataGlobal("srvt_inventorystok", $datainventorystock);
                    }

                    $datasjgd = [
                        'terima' => true,
                        'tglterima' => date('Y-m-d H:i:s'),
                        'userterima' => $pemakai
                    ];
                    $this->PenerimaanTransferStok_model->UpdateSuratJalanGudang($this->input->post('nomortransfer'), $datasjgd);

                    $getsjgudang = $this->PenerimaanTransferStok_model->DataSJG($this->input->post('nomortransfer'));
                    foreach ($getsjgudang as $key => $valsjgudang) {
                        $datats = [
                            'status' => true,
                            'kodestatus' => 2,
                            'tglterima' => date('Y-m-d H:i:s'),
                            'userterima' => $pemakai
                        ];
                        $this->PenerimaanTransferStok_model->UpdateTransferStok($valsjgudang->noproses, $datats);
                    }
                }
                $data = [
                    'nomor' => $nomor,
                    'tanggal' => $this->input->post('tanggal'),
                    'kodegdasal' => $this->input->post('kodegdasal'),
                    'kodegdtujuan' => $this->input->post('kodegdtujuan'),
                    'kodecbasal' => $this->input->post('kodecbasal'),
                    'kodecbtujuan' => $this->input->post('kodecbtujuan'),
                    'notransfer' => $this->input->post('nomortransfer'),
                    'tanggaltransfer' => $this->input->post('tanggaltransfer'),
                    'tglsimpan' => date('Y-m-d H:i:s'),
                    'pemakai' => $pemakai
                ];
                $this->PenerimaanTransferStok_model->SaveData($data);
            }

            // die();
            $this->db->trans_complete();

            if ($this->db->trans_status() === FALSE) {
                $resultjson = array(
                    'nomor' => "",
                    'message' => "Data gagal disimpan, Nomor '<b style='color: red'>" . $nomor . "</b>' sudah pernah digunakan."
                );
                # Something went wrong.
                $this->db->trans_rollback();
            } else {
                $resultjson = array(
                    'nomor' => $nomor,
                    'message' => "Data berhasil disimpan."
                );
                # Everything is Perfect. 
                # Committing data to the database.
                $this->db->trans_commit();
            }
            echo json_encode($resultjson);
        } else {
            $resultjson = array(
                'nomor' => "",
                'message' => "Data gagal disimpan, Nomor '<b style='color: red'>" . $nomor . "</b>' sudah pernah digunakan."
            );
            # Something went wrong.
            $this->db->trans_rollback();
        }
    }
    function Cancel()
    {
        $kodecabang =  $this->session->userdata('mycabang');
        $pemakai =  $this->session->userdata('myusername');
        $nomor = $this->input->post('nomor');
        $keterangan = $this->input->post('keterangan');
        $cabang = $this->input->post('kodecabang');
        $keterangan = $this->input->post('keterangan');
        $kodegudang = $this->input->post('kodegudang');
        $kodecbasal = $this->input->post('kodecbasal');
        $kodegdasal = $this->input->post('kodegdasal');
        $periode = date('Y') . date('m');
        $errorvalidasi = FALSE;
        $where = array(
            "nomor" => $this->input->post('notransfer'),
        );

        if (!empty($this->input->post('datadetail'))) {
            foreach ($this->input->post('datadetail') as $key => $value) {
                // print_r($value);
                // die();
                $where = array(
                    "periode" => $periode,
                    "kodecabang" => $cabang,
                    "kodegudang" => $kodegudang,
                    "kodebarang" => $value['Kode']
                );
                $datastock = $this->global_model->GetDataGlobal("srvt_inventorystok", $where);
                if (empty($datastock)) {
                    $resultjson = array(
                        'nomor' => "",
                        'message' => "Data gagal dibatalkan, Barang '<b style='color: red'>" . $value['Nama'] . "</b>' Stock Tidak Ada."
                    );
                    $errorvalidasi = TRUE;
                    echo json_encode($resultjson);
                    return FALSE;
                } else {
                    foreach ($datastock as $key => $values) {
                        $stocksaatini = $values->qtyawal + $values->qtymasuk - $values->qtykeluar - $values->qtypick;
                        if (floatval(str_replace(",", "", $value['Qty'])) > $stocksaatini) {
                            $resultjson = array(
                                'nomor' => "",
                                'message' => "Data gagal dibatalkan, Barang '<b style='color: red'>" . $value['Nama'] . "</b>' Stock Tidak Cukup."
                            );
                            $errorvalidasi = TRUE;
                            echo json_encode($resultjson);
                            return FALSE;
                        }
                    }
                }
            }
        }

        if (empty($this->input->post('datadetail'))) {
            $resultjson = array(
                'nomor' => "",
                'message' => "Data gagal dibatalkan, Data Detail Tidak Ada."
            );
            $errorvalidasi = TRUE;
            echo json_encode($resultjson);
            return FALSE;
        }
        if ($this->input->post('kodecabang') == "") {
            $resultjson = array(
                'nomor' => "",
                'message' => "Data gagal dibatalkan, Cabang Kosong."
            );
            $errorvalidasi = TRUE;
            echo json_encode($resultjson);
            return FALSE;
        }
        if ($this->input->post('kodegudang') == "") {
            $resultjson = array(
                'nomor' => "",
                'message' => "Data gagal dibatalkan, Gudang Kosong."
            );
            $errorvalidasi = TRUE;
            echo json_encode($resultjson);
            return FALSE;
        }

        if ($errorvalidasi == FALSE) {
            $this->db->trans_start(); # Starting Transaction
            $this->db->trans_strict(FALSE);

            if(!empty($this->input->post('datadetail'))){
                foreach($this->input->post('datadetail') as $key => $value){
                    $getdataterima = $this->PenerimaanTransferStok_model->DataInv($periode, $value['Kode'], $cabang, $kodegudang);
                    foreach ($getdataterima as $key => $val) {
                        $dataterima = [
                            'qtykeluar' => $val->qtykeluar + $this->ClearChr($value['Qty'])
                        ];
                        $this->PenerimaanTransferStok_model->UpdateStok($periode, $value['Kode'], $cabang, $kodegudang, $dataterima);
                    }

                    $getdataasal = $this->PenerimaanTransferStok_model->DataInv($periode, $value['Kode'], $kodecbasal, $kodegdasal);
                    foreach ($getdataasal as $key => $val) {
                        $dataasal = [
                            'qtypick' => $val->qtypick + $this->ClearChr($value['Qty'])
                        ];
                        $this->PenerimaanTransferStok_model->UpdateStok($periode, $value['Kode'], $kodecbasal, $kodegdasal, $dataasal);
                    }
                }
            }

            $dataheader = array(
                'batal' => true,
                'userbatal' => $pemakai,
                'alasanbatal' => $keterangan,
                'tglbatal' => date('Y-m-d H:i:s'),
                'kodecabang' => $kodecabang
            );
            $wheredata = array("nomor" => $nomor);
            $this->global_model->UpdateDataGlobal("srvt_penerimaantransferstock", $dataheader, $wheredata);
            $this->db->trans_complete();
            if ($this->db->trans_status() === FALSE) {
                $resultjson = array(
                    'nomor' => "",
                    'message' => "Data gagal dibatalkan, Nomor '<b style='color: red'>" . $nomor . "</b>' tidak ada."
                );
                # Something went wrong.
                $this->db->trans_rollback();
            } else {
                $resultjson = array(
                    'nomor' => $nomor,
                    'message' => "Data berhasil dibatalkan."
                );
                # Everything is Perfect. 
                # Committing data to the database.
                $this->db->trans_commit();
            }

            echo json_encode($resultjson);
            return FALSE;
        }
    }
}
