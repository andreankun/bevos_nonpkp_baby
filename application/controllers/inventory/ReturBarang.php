<?php

defined('BASEPATH') or exit('No direct script access allowed');

class ReturBarang extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('inventory/ReturBarang_model');
		$this->load->model('global/global_model');

		$this->load->model('Caridataaktif_model');
		$this->load->library('form_validation');
		$this->load->library('session');
	}

	function CariDataReturBarang()
	{
		$fetch_data = $this->Caridataaktif_model->make_datatables($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value'));
		$data = array();
		foreach ($fetch_data as $row) {
			$sub_array = array();
			$i = 1;
			$count = count($this->input->post('field'));
			foreach ($this->input->post('field') as $key => $value) {
				if ($i <= $count) {
					if ($i == 1) {
						$msearch = $row->$value;
						$sub_array[] = '<button class="btn btn-dark searchrb" data-dismiss="modal" style="margin: 0px;" data-id="' . $msearch . '"><i class="far fa-check-circle"></i></button>';
						$sub_array[1] = '<button class="btn btn-sm btn-dark cetakretur" style="margin: 0px; background-color: #827354; border-color: #827354;" data-id="' . $msearch . '"><i class="fas fa-print"></i>&nbsp;Cetak Retur</button>';
						$sub_array[] = $row->$value;
					} else {
						if ($i == $count) {
							$sub_array[] = $row->$value;
						} else {
							$sub_array[] = $row->$value;
						}
					}
					// $sub_array[] = $row->$value;
				}
				$i++;
			}
			$data[] = $sub_array;
		}
		$output = array(
			"draw"                    =>     intval($_POST["draw"]),
			"recordsTotal"          =>      $this->Caridataaktif_model->get_all_data($this->input->post('nmtb')),
			"recordsFiltered"     =>     $this->Caridataaktif_model->get_filtered_data($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value')),
			"data"                    =>     $data
		);
		echo json_encode($output);
	}

	function DataReturBarang()
	{
		$result = $this->ReturBarang_model->DataReturBarang($this->input->post('nomor'));
		echo json_encode($result);
	}

	function DataReturBarangDetail()
	{
		$result = $this->ReturBarang_model->DataReturBarangDetail($this->input->post('nomor'));
		echo json_encode($result);
	}

	function CariDataInvoice()
	{
		$fetch_data = $this->Caridataaktif_model->make_datatables($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value'));
		$data = array();
		foreach ($fetch_data as $row) {
			$sub_array = array();
			$i = 1;
			$count = count($this->input->post('field'));
			foreach ($this->input->post('field') as $key => $value) {
				if ($i <= $count) {
					if ($i == 1) {
						$msearch = $row->$value;
						$sub_array[] = '<button class="btn btn-dark searchinvoice" data-dismiss="modal" style="margin: 0px;" data-id="' . $msearch . '"><i class="far fa-check-circle"></i></button>';
						$sub_array[] = $row->$value;
					} else {
						if ($i == $count) {
							$sub_array[] = $row->$value;
						} else {
							$sub_array[] = $row->$value;
						}
					}
					// $sub_array[] = $row->$value;
				}
				$i++;
			}
			$data[] = $sub_array;
		}
		$output = array(
			"draw"                    =>     intval($_POST["draw"]),
			"recordsTotal"          =>      $this->Caridataaktif_model->get_all_data($this->input->post('nmtb')),
			"recordsFiltered"     =>     $this->Caridataaktif_model->get_filtered_data($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value')),
			"data"                    =>     $data
		);
		echo json_encode($output);
	}

	function DataInvoice()
	{
		$result = $this->ReturBarang_model->DataInvoice($this->input->post('nomor'));
		echo json_encode($result);
	}

	function CariDataInvoiceDetail()
	{
		$fetch_data = $this->Caridataaktif_model->make_datatables($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value'));
		$data = array();
		foreach ($fetch_data as $row) {
			$sub_array = array();
			$i = 1;
			$count = count($this->input->post('field'));
			foreach ($this->input->post('field') as $key => $value) {
				if ($i <= $count) {
					if ($i == 1) {
						$msearch = $row->$value;
						$sub_array[] = '<button class="btn btn-dark searchinvoicedetail" data-dismiss="modal" style="margin: 0px;" data-id="' . $msearch . '"><i class="far fa-check-circle"></i></button>';
						$sub_array[] = $row->$value;
					} else {
						if ($i == $count) {
							$sub_array[] = $row->$value;
						} else {
							$sub_array[] = $row->$value;
						}
					}
					// $sub_array[] = $row->$value;
				}
				$i++;
			}
			$data[] = $sub_array;
		}
		$output = array(
			"draw"                    =>     intval($_POST["draw"]),
			"recordsTotal"          =>      $this->Caridataaktif_model->get_all_data($this->input->post('nmtb')),
			"recordsFiltered"     =>     $this->Caridataaktif_model->get_filtered_data($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value')),
			"data"                    =>     $data
		);
		echo json_encode($output);
	}

	function DataInvoiceDetail()
	{
		$result = $this->ReturBarang_model->DataInvoiceDetail($this->input->post('kodebarang'), $this->input->post('kodecabang'));
		echo json_encode($result);
	}

	function LoadGudang()
	{
		$result = $this->global_model->GetDataGlobal("glbm_gudang", array('kodecabang' => $this->input->post('cabang')));
		echo json_encode($result);
	}


	public function ClearChr($param)
	{
		return $result = str_replace(array('\'', '"', ',', ';', '<', '>', '_', '-', ' ', '.', '!', '?', '/', '=', '+', ']', '[', '{', '}', '@', '#', '$', '%', '^', '&', "*", '(', ')'), '', $param);
	}

	function Save()
	{
		$kodecabang = $this->session->userdata('mycabang');
		// $kodegudang = $this->session->userdata('mygudang');
		$kodegudang = $this->input->post('lokasigdg');

		$pemakai = $this->session->userdata('myusername');
		$pengembalian = $this->input->post('pengembalian');

		$nomor = $this->input->post('noretur');
		$periode = date("Y") . date("m");
		$ceknomor = $this->ReturBarang_model->CekRB($nomor);
		$selisihbulan = 0;
		$errorvalidasi = FALSE;

		if (!empty($this->input->post('datadetail'))) {
			foreach ($this->input->post('datadetail') as $key => $value) {
				$where = array(
					"periode" => $periode,
					"kodecabang" => $kodecabang,
					"kodebarang" => $value['Kode']
				);

				// $wherenotin = array("kodegudang" => $kodegudang);
				$datastock = $this->global_model->GetDataGlobalAndNotIN("srvt_inventorystok", $where, $kodegudang);
				if (!empty($datastock)) {
					foreach ($datastock as $key => $values) {
						if (($values->qtyawal + $values->qtymasuk - $values->qtykeluar) > 0) {
							$resultjson = array(
								'nomor' => "",
								'message' => "Data gagal disimpan, Barang '<b style='color: red'>" . $value['Nama'] . "</b>' Stock Ada di Gudang Lain"
							);
							$errorvalidasi = TRUE;
							echo json_encode($resultjson);
							return FALSE;
						}
					}
				}
			}
		}
		if ($errorvalidasi == FALSE) {
			$this->db->trans_start(); # Starting Transaction
			$this->db->trans_strict(FALSE);

			$ambilnomor = "RJ" . "-" . date('Y-m', strtotime($this->input->post('tglretur')));

			$diff = abs(strtotime($this->input->post('tglretur')) - strtotime(date('Y-m-d')));
			$years = floor($diff / (365 * 60 * 60 * 24));
			$bulan = floor(($diff - $years * 365 * 60 * 60 * 24) / (30 * 60 * 60 * 24));
			if ((date('m') == date('m', strtotime($this->input->post('tglretur'))) && (date('Y') == date('Y', strtotime($this->input->post('tglretur')))))) {
				$selisihbulan = 0;
			} else if ((date('m') == date('m', strtotime($this->input->post('tglretur'))) && (date('Y') > date('Y', strtotime($this->input->post('tglretur')))))) {
				$selisihbulan = $bulan - 1 + ($years * 12);
			} else if ((date('m') != date('m', strtotime($this->input->post('tglretur'))) && (date('Y') == date('Y', strtotime($this->input->post('tglretur')))))) {
				$selisihbulan = $bulan + 1 + ($years * 12);
			} else {
				$selisihbulan = $bulan + ($years * 12);
			}

			if (empty($nomor)) {
				$get["rb"] = $this->ReturBarang_model->GetMaxNomor($ambilnomor);
				if (!$get["rb"]) {
					$nomor = $ambilnomor . "-" . "00001";
				} else {
					$lastNomor = $get['rb']->nomor;
					$lastNoUrut = substr($lastNomor, 11, 11);

					// nomor urut ditambah 1
					$nextNoUrut = $lastNoUrut + 1;
					$nomor = $ambilnomor . "-" . sprintf('%05s', $nextNoUrut);
				}
				if (!empty($this->input->post('datadetail'))) {
					foreach ($this->input->post('datadetail') as $key => $value) {

						$datadetail = [
							'nomor' => $nomor,
							'kodebarang' => $value['Kode'],
							'namabarang' => $value['Nama'],
							'qty' => $this->ClearChr($value['Qty']),
							'harga' => $this->ClearChr($value['Harga']),
							'discbarang' => $value['Disc'],
							'discbrgitem' => $this->ClearChr($value['DiscItem']),
							'discpaket' => $value['DiscPaket'],
							'discpaketitem' => $this->ClearChr($value['DiscPaketItem']),
							'total' => $this->ClearChr($value['Subtotal']),
							'pemakai' => $pemakai,
							'tglsimpan' => date('Y-m-d H:i:s'),
						];
						$this->ReturBarang_model->SaveDataDetail($datadetail);

						if ($selisihbulan > 0) {
							$saldoawal = 0;
							for ($i = 0; $i <= $selisihbulan; $i++) {
								$periodestock = date('Ym', strtotime('+' . $i . ' month', strtotime($this->input->post('tglretur'))));
								$where = array(
									"periode" => $periodestock,
									"kodecabang" => $kodecabang,
									"kodegudang" => $kodegudang,
									"kodebarang" => $value['Kode']
								);
								$datastock = $this->global_model->GetDataGlobal("srvt_inventorystok", $where);
								if (!empty($datastock)) {
									foreach ($datastock as $key => $value2) {
										if ($saldoawal == 0) {
											$qtymsk = ($value2->qtymasuk + $this->ClearChr($value['Qty']));
											$setdata = "qtymasuk = '" . $qtymsk . "'";
											$wheredata = "periode = '" . $periodestock . "' AND kodecabang = '" . $kodecabang . "' AND kodegudang = '" . $kodegudang . "' AND kodebarang = '" . $value['Kode'] . "'";
											$this->ReturBarang_model->UpdateDataInventory("srvt_inventorystok", $setdata, $wheredata);
										} else {
											$qtymsk = ($value2->qtymasuk + $this->ClearChr($value['Qty']));
											$setdata = "qtyawal = '" . $saldoawal . "'";
											$wheredata = "periode = '" . $periodestock . "' AND kodecabang = '" . $kodecabang . "' AND kodegudang = '" . $kodegudang . "' AND kodebarang = '" . $value['Kode'] . "'";
											$this->ReturBarang_model->UpdateDataInventory("srvt_inventorystok", $setdata, $wheredata);
										}
										$ceksaldoawal = $this->global_model->GetDataGlobal("srvt_inventorystok", $where);
										if (!empty($ceksaldoawal)) {
											foreach ($ceksaldoawal as $key => $valuesaldoawal) {
												$saldoawal = $valuesaldoawal->qtyawal + $valuesaldoawal->qtymasuk - $valuesaldoawal->qtykeluar;	# code...
											}
										}
									}
								} else {
									if ($saldoawal == 0) {
										$datainsert = array(
											"periode" => $periodestock,
											"kodecabang" => $kodecabang,
											"kodegudang" => $kodegudang,
											"kodebarang" => $value['Kode'],
											"qtyawal" => $saldoawal,
											"qtymasuk" =>  $this->ClearChr($value['Qty']),
											"qtykeluar" => 0,
											"qtypick" => 0,
											"cogs" => 0,
											'pemakai' => $pemakai,
											'tglsimpan' => date('Y-m-d H:i:s'),
										);
										$datastock = $this->global_model->InsertDataGlobal("srvt_inventorystok", $datainsert);
									} else {
										$datainsert = array(
											"periode" => $periodestock,
											"kodecabang" => $kodecabang,
											"kodegudang" => $kodegudang,
											"kodebarang" => $value['Kode'],
											"qtyawal" => $saldoawal,
											"qtymasuk" =>  0,
											"qtykeluar" => 0,
											"qtypick" => 0,
											"cogs" => 0,
											'pemakai' => $pemakai,
											'tglsimpan' => date('Y-m-d H:i:s'),
										);
										$datastock = $this->global_model->InsertDataGlobal("srvt_inventorystok", $datainsert);
									}
									$ceksaldoawal = $this->global_model->GetDataGlobal("srvt_inventorystok", $where);
									if (!empty($ceksaldoawal)) {
										foreach ($ceksaldoawal as $key => $valuesaldoawal) {
											$saldoawal = $valuesaldoawal->qtyawal + $valuesaldoawal->qtymasuk - $valuesaldoawal->qtykeluar;	# code...
										}
									}
								}
							}
						} else {
							$where = array(
								"periode" => $periode,
								"kodecabang" => $kodecabang,
								"kodegudang" => $kodegudang,
								"kodebarang" => $value['Kode']
							);
							$datastock = $this->global_model->GetDataGlobal("srvt_inventorystok", $where);
							if (!empty($datastock)) {
								foreach ($datastock as $key => $value2) {
									$qtymsk = ($value2->qtymasuk + $this->ClearChr($value['Qty']));
									$setdata = "qtymasuk = '" . $qtymsk . "'";
									$wheredata = "periode = '" . $periode . "' AND kodecabang = '" . $kodecabang . "' AND kodegudang = '" . $kodegudang . "' AND kodebarang = '" . $value['Kode'] . "'";
									$this->ReturBarang_model->UpdateDataInventory("srvt_inventorystok", $setdata, $wheredata);
								}
							}
						}
					}
				}
				if ($pengembalian == "true") {
					$data = array(
						"nomorcustomer" => $this->input->post('nopelanggan'),
						"tanggal" => $this->input->post('tglretur'),
						"kodestatus" => 4,
						"noreferensi" => $nomor,
						"keterangan" => "Retur Penjualan Customer",
						"debit" => floatval(str_replace(",", "", $this->input->post('grandtotal'))),
						"kredit" => 0,
					);
					$this->global_model->InsertDataGlobal("trnt_historydepositcustomer", $data);
					$data = [
						'nomor' => $nomor,
						'tanggal' => $this->input->post('tglretur'),
						'noinvoice' => $this->input->post('noinvoice'),
						'nopelanggan' => $this->input->post('nopelanggan'),
						'notaretur' => $this->input->post('notaretur'),
						'tglnotaretur' => $this->input->post('tglnotaretur'),
						'nofakturpajak' => $this->input->post('nofakturpajak'),
						'tglfakturpajak' => $this->input->post('tglfakturpajak'),
						'disccash' => $this->ClearChr($this->input->post('discash')),
						'disccustomer' => $this->ClearChr($this->input->post('discustomer')),
						'nilaicash' => $this->ClearChr($this->input->post('nilaicash')),
						'nilaicustomer' => $this->ClearChr($this->input->post('nilaicustomer')),
						'total' => $this->ClearChr($this->input->post('total')),
						'dpp' => $this->ClearChr($this->input->post('dpp')),
						'ppn' => $this->ClearChr($this->input->post('ppn')),
						'grandtotal' => $this->ClearChr($this->input->post('grandtotal')),
						'nilaipengembalian' => $this->ClearChr($this->input->post('grandtotal')),
						'pemakai' => $pemakai,
						'tglsimpan' => date('Y-m-d H:i:s'),
						'pengembalian' => $pengembalian,
						'kodecabang' => $kodecabang,
						'lokasigudang' => $kodegudang,
					];
					$this->ReturBarang_model->SaveData($data);
				} else {
					$datapiutang = $this->ReturBarang_model->DataPiutang($this->input->post('noinvoice'));
					$dataupdate = [
						'nilaipiutang' => $datapiutang->nilaipiutang - $this->ClearChr($this->input->post('grandtotal'))
					];
					$this->ReturBarang_model->UpdateDataPiutang($this->input->post('noinvoice'), $dataupdate);

					$data = [
						'nomor' => $nomor,
						'tanggal' => $this->input->post('tglretur'),
						'noinvoice' => $this->input->post('noinvoice'),
						'nopelanggan' => $this->input->post('nopelanggan'),
						'notaretur' => $this->input->post('notaretur'),
						'tglnotaretur' => $this->input->post('tglnotaretur'),
						'nofakturpajak' => $this->input->post('nofakturpajak'),
						'tglfakturpajak' => $this->input->post('tglfakturpajak'),
						'disccash' => $this->ClearChr($this->input->post('discash')),
						'disccustomer' => $this->ClearChr($this->input->post('discustomer')),
						'nilaicash' => $this->ClearChr($this->input->post('nilaicash')),
						'nilaicustomer' => $this->ClearChr($this->input->post('nilaicustomer')),
						'total' => $this->ClearChr($this->input->post('total')),
						'dpp' => $this->ClearChr($this->input->post('dpp')),
						'ppn' => $this->ClearChr($this->input->post('ppn')),
						'grandtotal' => $this->ClearChr($this->input->post('grandtotal')),
						'nilaipengembalian' => 0,
						'pemakai' => $pemakai,
						'tglsimpan' => date('Y-m-d H:i:s'),
						'pengembalian' => $pengembalian,
						'kodecabang' => $kodecabang,
						'lokasigudang' => $kodegudang,
					];
					$this->ReturBarang_model->SaveData($data);
				}

				$wheredata = array("nomor" => $this->input->post('noinvoice'));
				$this->global_model->UpdateDataGlobal("srvt_invoice", array('statusretur' => true), $wheredata);
			}

			// die();
			$this->db->trans_complete();

			if ($this->db->trans_status() === FALSE) {
				$resultjson = array(
					'nomor' => "",
					'message' => "Data gagal disimpan, Nomor '<b style='color: red'>" . $nomor . "</b>' sudah pernah digunakan."
				);
				# Something went wrong.
				$this->db->trans_rollback();
			} else {
				$resultjson = array(
					'nomor' => $nomor,
					'message' => "Data berhasil disimpan."
				);
				# Everything is Perfect. 
				# Committing data to the database.
				$this->db->trans_commit();
			}
			// print_r($resultjson);
			// die();
			echo json_encode($resultjson);
		}
	}


	function Update()
	{
		// $kodecabang = $this->session->userdata('mycabang');
		$pemakai = $this->session->userdata('myusername');

		$nomor = $this->input->post('noretur');

		$this->db->trans_start(); # Starting Transaction
		$this->db->trans_strict(FALSE);
		if (!empty($this->input->post('datadetail'))) {
			$this->ReturBarang_model->DeleteDetail($nomor);
			foreach ($this->input->post('datadetail') as $key => $value) {
				$datadetail = [
					'nomor' => $nomor,
					'kodebarang' => $value['Kode'],
					'namabarang' => $value['Nama'],
					'qty' => $this->ClearChr($value['Qty']),
					'harga' => $this->ClearChr($value['Harga']),
					'discbarang' => $value['Disc'],
					'discbrgitem' => $this->ClearChr($value['DiscItem']),
					'discpaket' => $value['DiscPaket'],
					'discpaketitem' => $this->ClearChr($value['DiscPaketItem']),
					'total' => $this->ClearChr($value['Subtotal']),
					'pemakai' => $pemakai,
					'tglsimpan' => date('Y-m-d H:i:s'),
				];
				$this->ReturBarang_model->SaveDataDetail($datadetail);
			}

			$data = [
				'nomor' => $nomor,
				'tanggal' => $this->input->post('tglretur'),
				'noinvoice' => $this->input->post('noinvoice'),
				'nopelanggan' => $this->input->post('nopelanggan'),
				'notaretur' => $this->input->post('notaretur'),
				'tglnotaretur' => $this->input->post('tglnotaretur'),
				'nofakturpajak' => $this->input->post('nofakturpajak'),
				'tglfakturpajak' => $this->input->post('tglfakturpajak'),
				'disccash' => floatval(str_replace(",", "", $this->input->post('discash'))),
				'disccustomer' => floatval(str_replace(",", "", $this->input->post('discustomer'))),
				'nilaicash' => floatval(str_replace(",", "", $this->input->post('nilaicash'))),
				'nilaicustomer' => floatval(str_replace(",", "", $this->input->post('nilaicustomer'))),
				'total' => floatval(str_replace(",", "", $this->input->post('total'))),
				'dpp' => floatval(str_replace(",", "", $this->input->post('dpp'))),
				'ppn' => floatval(str_replace(",", "", $this->input->post('ppn'))),
				'grandtotal' => floatval(str_replace(",", "", $this->input->post('grandtotal'))),
				// 'lokasigudang' =>
				'pemakai' => $pemakai,
				'tglsimpan' => date('Y-m-d H:i:s'),
				// 'kodecabang' => $kodecabang,
			];
			$this->ReturBarang_model->UpdateData($nomor, $data);
		}
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE) {
			$resultjson = array(
				'nomor' => "",
				'message' => "Data gagal disimpan, Nomor '<b style='color: red'>" . $nomor . "</b>' sudah pernah digunakan."
			);
			# Something went wrong.
			$this->db->trans_rollback();
		} else {
			$resultjson = array(
				'nomor' => $nomor,
				'message' => "Data berhasil disimpan."
			);
			# Everything is Perfect. 
			# Committing data to the database.
			$this->db->trans_commit();
		}
		echo json_encode($resultjson);
	}



	function Cancel()
	{


		$noretur = $this->input->post('noretur');
		$kodegudang = $this->input->post('lokasigdg');
		$tglretur = $this->input->post('tglretur');
		$noinvoice = $this->input->post('noinvoice');
		$nopelanggan = $this->input->post('nopelanggan');
		$keteranganbatal = $this->input->post('keteranganbatal');
		$datadetail = $this->input->post('datadetail');
		$kodecabang = $this->session->userdata('mycabang');
		$pemakai = $this->session->userdata('myusername');
		$pengembalian = $this->input->post('pengembalian');
		$grandtotal = $this->input->post('grandtotal');


		$periode = date("Y") . date("m");
		$selisihbulan = 0;
		$errorvalidasi = FALSE;

		$diff = abs(strtotime($this->input->post('tglretur')) - strtotime(date('Y-m-d')));
		$years = floor($diff / (365 * 60 * 60 * 24));
		$bulan = floor(($diff - $years * 365 * 60 * 60 * 24) / (30 * 60 * 60 * 24));
		if ((date('m') == date('m', strtotime($this->input->post('tglretur'))) && (date('Y') == date('Y', strtotime($this->input->post('tglretur')))))) {
			$selisihbulan = 0;
		} else if ((date('m') == date('m', strtotime($this->input->post('tglretur'))) && (date('Y') > date('Y', strtotime($this->input->post('tglretur')))))) {
			$selisihbulan = $bulan - 1 + ($years * 12);
		} else if ((date('m') != date('m', strtotime($this->input->post('tglretur'))) && (date('Y') == date('Y', strtotime($this->input->post('tglretur')))))) {
			$selisihbulan = $bulan + 1 + ($years * 12);
		} else {
			$selisihbulan = $bulan + ($years * 12);
		}

		if (!empty($datadetail)) {
			foreach ($datadetail  as $key => $value) {
				if ($selisihbulan > 0) {
					for ($i = 0; $i <= $selisihbulan; $i++) {
						$periodestock = date('Ym', strtotime('+' . $i . ' month', strtotime($this->input->post('tglretur'))));
						$where = array(
							"periode" => $periodestock,
							"kodecabang" => $kodecabang,
							"kodegudang" => $kodegudang,
							"kodebarang" => $value['Kode']
						);
						$datastock = $this->global_model->GetDataGlobal("srvt_inventorystok", $where);
						if (empty($datastock)) {
							$resultjson = array(
								'nomor' => "",
								'message' => "Data gagal disimpan, Barang '<b style='color: red'>" . $value['Nama'] . "</b>' Stock Tidak Ada."
							);
							$errorvalidasi = TRUE;
							echo json_encode($resultjson);
							return FALSE;
						} else {
							foreach ($datastock as $key => $values) {
								$stocksaatini = $values->qtyawal + $values->qtymasuk - $values->qtykeluar - $values->qtypick;
								if (floatval(str_replace(",", "", $value['Qty'])) > $stocksaatini) {
									$resultjson = array(
										'nomor' => "",
										'message' => "Data gagal disimpan, Barang '<b style='color: red'>" . $value['Nama'] . "</b>' Stock Tidak Cukup."
									);
									$errorvalidasi = TRUE;
									echo json_encode($resultjson);
									return FALSE;
								}
							}
						}
					}
				} else {
					$where = array(
						"periode" => $periode,
						"kodecabang" => $kodecabang,
						"kodegudang" => $kodegudang,
						"kodebarang" => $value['Kode']
					);
					$datastock = $this->global_model->GetDataGlobal("srvt_inventorystok", $where);
					if (empty($datastock)) {
						$resultjson = array(
							'nomor' => "",
							'message' => "Data gagal disimpan, Barang '<b style='color: red'>" . $value['Nama'] . "</b>' Stock Tidak Ada."
						);
						$errorvalidasi = TRUE;
						echo json_encode($resultjson);
						return FALSE;
					} else {
						foreach ($datastock as $key => $values) {
							$stocksaatini = $values->qtyawal + $values->qtymasuk - $values->qtykeluar - $values->qtypick;
							if (floatval(str_replace(",", "", $value['Qty'])) > $stocksaatini) {
								$resultjson = array(
									'nomor' => "",
									'message' => "Data gagal disimpan, Barang '<b style='color: red'>" . $value['Nama'] . "</b>' Stock Tidak Cukup."
								);
								$errorvalidasi = TRUE;
								echo json_encode($resultjson);
								return FALSE;
							}
						}
					}
				}
			}
		}

		if (!empty($datadetail)) {
			foreach ($datadetail  as $key => $value) {
				$where = array(
					"periode" => $periode,
					"kodecabang" => $kodecabang,
					"kodebarang" => $value['Kode']
				);

				// $wherenotin = array("kodegudang" => $kodegudang);
				$datastock = $this->global_model->GetDataGlobalAndNotIN("srvt_inventorystok", $where, $kodegudang);
				if (!empty($datastock)) {
					foreach ($datastock as $key => $values) {
						if (($values->qtyawal + $values->qtymasuk - $values->qtykeluar) > 0) {
							$resultjson = array(
								'nomor' => "",
								'message' => "Data gagal dibatalkan, Barang '<b style='color: red'>" . $value['Nama'] . "</b>' Stock Ada di Gudang Lain"
							);
							$errorvalidasi = TRUE;
							echo json_encode($resultjson);
							return FALSE;
						}
					}
				}
			}
		}

		if ($errorvalidasi == FALSE) {
			$this->db->trans_start(); # Starting Transaction
			$this->db->trans_strict(FALSE);

			if (!empty($datadetail)) {
				foreach ($datadetail  as $key => $value) {

					if ($selisihbulan > 0) {
						$saldoawal = 0;
						for ($i = 0; $i <= $selisihbulan; $i++) {
							$periodestock = date('Ym', strtotime('+' . $i . ' month', strtotime($this->input->post('tglretur'))));
							$where = array(
								"periode" => $periodestock,
								"kodecabang" => $kodecabang,
								"kodegudang" => $kodegudang,
								"kodebarang" => $value['Kode']
							);
							$datastock = $this->global_model->GetDataGlobal("srvt_inventorystok", $where);
							if (!empty($datastock)) {
								foreach ($datastock as $key => $value2) {
									if ($saldoawal == 0) {
										$qtyklr = ($value2->qtykeluar + $this->ClearChr($value['Qty']));
										$setdata = "qtykeluar = '" . $qtyklr . "'";
										$wheredata = "periode = '" . $periodestock . "' AND kodecabang = '" . $kodecabang . "' AND kodegudang = '" . $kodegudang . "' AND kodebarang = '" . $value['Kode'] . "'";
										$this->ReturBarang_model->UpdateDataInventory("srvt_inventorystok", $setdata, $wheredata);
									} else {
										$qtyklr = ($value2->qtykeluar + $this->ClearChr($value['Qty']));
										$setdata = "qtyawal = '" . $saldoawal . "'";
										$wheredata = "periode = '" . $periodestock . "' AND kodecabang = '" . $kodecabang . "' AND kodegudang = '" . $kodegudang . "' AND kodebarang = '" . $value['Kode'] . "'";
										$this->ReturBarang_model->UpdateDataInventory("srvt_inventorystok", $setdata, $wheredata);
									}
									$ceksaldoawal = $this->global_model->GetDataGlobal("srvt_inventorystok", $where);
									if (!empty($ceksaldoawal)) {
										foreach ($ceksaldoawal as $key => $valuesaldoawal) {
											$saldoawal = $valuesaldoawal->qtyawal + $valuesaldoawal->qtymasuk - $valuesaldoawal->qtykeluar;	# code...
										}
									}
								}
							}
						}
					} else {
						$where = array(
							"periode" => $periode,
							"kodecabang" => $kodecabang,
							"kodegudang" => $kodegudang,
							"kodebarang" => $value['Kode']
						);
						$datastock = $this->global_model->GetDataGlobal("srvt_inventorystok", $where);
						if (!empty($datastock)) {
							foreach ($datastock as $key => $value2) {
								$qtyklr = ($value2->qtykeluar + $this->ClearChr($value['Qty']));
								$setdata = "qtykeluar = '" . $qtyklr . "'";
								$wheredata = "periode = '" . $periode . "' AND kodecabang = '" . $kodecabang . "' AND kodegudang = '" . $kodegudang . "' AND kodebarang = '" . $value['Kode'] . "'";
								$this->ReturBarang_model->UpdateDataInventory("srvt_inventorystok", $setdata, $wheredata);
							}
						}
					}
				}
			}
			if ($pengembalian == "true") {
				$data = array(
					"nomorcustomer" => $nopelanggan,
					"tanggal" => $tglretur,
					"kodestatus" => 5,
					"noreferensi" => $noretur,
					"keterangan" => "Batal Retur Penjualan Customer",
					"debit" => 0,
					"kredit" => floatval(str_replace(",", "", $grandtotal)),
				);
				$this->global_model->InsertDataGlobal("trnt_historydepositcustomer", $data);
				$wheredata = array("nomor" => $noretur);
				$updatedata = array(
					'nilaipengembalian' => 0,
					'batal' => true,
					'userbatal' => $pemakai,
					'tglbatal' => date('Y-m-d H:i:s'),
					'alasanbatal' => $keteranganbatal
				);
				$this->global_model->UpdateDataGlobal("srvt_returbarang", $updatedata, $wheredata);
			}


			$wheredata = array("nomor" => $noinvoice);
			$this->global_model->UpdateDataGlobal("srvt_invoice", array('statusretur' => false), $wheredata);


			// die();
			$this->db->trans_complete();

			if ($this->db->trans_status() === FALSE) {
				$resultjson = array(
					'nomor' => "",
					'message' => "Data gagal dibatalkan, Nomor '<b style='color: red'>" . $noretur . "</b>' sudah pernah digunakan."
				);
				# Something went wrong.
				$this->db->trans_rollback();
			} else {
				$resultjson = array(
					'nomor' => $noretur,
					'message' => "Data berhasil dibatalkan."
				);
				# Everything is Perfect. 
				# Committing data to the database.
				$this->db->trans_commit();
			}
			// print_r($resultjson);
			// die();
			echo json_encode($resultjson);
		}
	}
}
