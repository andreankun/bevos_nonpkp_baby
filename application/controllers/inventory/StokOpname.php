<?php
defined('BASEPATH') or exit('No direct script access allowed');
class StokOpname extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('inventory/Stokopname_model');
		$this->load->model('inventory/Stockrecount_model');
		$this->load->model('caridataaktif_model');
		$this->load->library('form_validation');
		$this->load->library('session');
	}

	function CariDataStockOpname()
	{
		$fetch_data = $this->caridataaktif_model->make_datatables($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value'));
		$data = array();
		foreach ($fetch_data as $row) {
			$sub_array = array();
			$i = 1;
			$count = count($this->input->post('field'));
			foreach ($this->input->post('field') as $key => $value) {
				if ($i <= $count) {
					if ($i == 1) {
						$msearch = $row->$value;
						$sub_array[] = '<button class="btn btn-dark searchopname" data-dismiss="modal" style="margin: 0px;" data-id="' . $msearch . '"><i class="far fa-check-circle"></i></button>';
						$sub_array[] = $row->$value;
					} else {
						if ($i == $count) {
							$sub_array[] = $row->$value;
						} else {
							$sub_array[] = $row->$value;
						}
					}
					// $sub_array[] = $row->$value;
				}
				$i++;
			}
			$data[] = $sub_array;
		}
		$output = array(
			"draw"                    =>     intval($_POST["draw"]),
			"recordsTotal"          =>      $this->caridataaktif_model->get_all_data($this->input->post('nmtb')),
			"recordsFiltered"     =>     $this->caridataaktif_model->get_filtered_data($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value')),
			"data"                    =>     $data
		);
		echo json_encode($output);
	}

	
	function CariDataStockOpnameBRecount()
	{
		$fetch_data = $this->caridataaktif_model->make_datatables($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value'));
		$data = array();
		foreach ($fetch_data as $row) {
			$sub_array = array();
			$i = 1;
			$count = count($this->input->post('field'));
			foreach ($this->input->post('field') as $key => $value) {
				if ($i <= $count) {
					if ($i == 1) {
						$msearch = $row->$value;
						$sub_array[] = '<button class="btn btn-dark searchopnamebrecount" data-dismiss="modal" style="margin: 0px;" data-id="' . $msearch . '"><i class="far fa-check-circle"></i></button>';
						$sub_array[] = $row->$value;
					} else {
						if ($i == $count) {
							$sub_array[] = $row->$value;
						} else {
							$sub_array[] = $row->$value;
						}
					}
					// $sub_array[] = $row->$value;
				}
				$i++;
			}
			$data[] = $sub_array;
		}
		$output = array(
			"draw"                    =>     intval($_POST["draw"]),
			"recordsTotal"          =>      $this->caridataaktif_model->get_all_data($this->input->post('nmtb')),
			"recordsFiltered"     =>     $this->caridataaktif_model->get_filtered_data($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value')),
			"data"                    =>     $data
		);
		echo json_encode($output);
	}

	function CariDataStockOpnameTanggal()
	{
		$fetch_data = $this->caridataaktif_model->make_datatables($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value'));
		$data = array();
		foreach ($fetch_data as $row) {
			$sub_array = array();
			$i = 1;
			$count = count($this->input->post('field'));
			foreach ($this->input->post('field') as $key => $value) {
				if ($i <= $count) {
					if ($i == 1) {
						$msearch = $row->$value;
						// $sub_array[] = '<button class="btn btn-dark serachall" data-dismiss="modal" style="margin: 0px;" data-id="' . $msearch . '"><i class="far fa-check-circle"></i></button>';
						$sub_array[] = $row->$value;
					} else {
						if ($i == $count) {
							$sub_array[] = $row->$value;
						} else {
							$sub_array[] = $row->$value;
						}
					}
					// $sub_array[] = $row->$value;
				}
				$i++;
			}
			$data[] = $sub_array;
		}
		$output = array(
			"draw"                    =>     intval($_POST["draw"]),
			"recordsTotal"          =>      $this->caridataaktif_model->get_all_data($this->input->post('nmtb')),
			"recordsFiltered"     =>     $this->caridataaktif_model->get_filtered_data($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value')),
			"data"                    =>     $data
		);
		echo json_encode($output);
	}

	function CariNomorStockOpname()
	{
		$fetch_data = $this->caridataaktif_model->make_datatables($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value'));
		$data = array();
		foreach ($fetch_data as $row) {
			$sub_array = array();
			$i = 1;
			$count = count($this->input->post('field'));
			foreach ($this->input->post('field') as $key => $value) {
				if ($i <= $count) {
					if ($i == 1) {
						$msearch = $row->$value;
						$sub_array[] = '<button class="btn btn-dark searchnomor" data-dismiss="modal" style="margin: 0px;" data-id="' . $msearch . '"><i class="far fa-check-circle"></i></button>';
						$sub_array[] = $row->$value;
					} else {
						if ($i == $count) {
							$sub_array[] = $row->$value;
						} else {
							$sub_array[] = $row->$value;
						}
					}
					// $sub_array[] = $row->$value;
				}
				$i++;
			}
			$data[] = $sub_array;
		}
		$output = array(
			"draw"                    =>     intval($_POST["draw"]),
			"recordsTotal"          =>      $this->caridataaktif_model->get_all_data($this->input->post('nmtb')),
			"recordsFiltered"     =>     $this->caridataaktif_model->get_filtered_data($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value')),
			"data"                    =>     $data
		);
		echo json_encode($output);
	}

	function find(){
		$tglawal = $this->input->post('tglawal');
		$tglakhir = $this->input->post('tglakhir');
		$kodegudang = $this->input->post('kodegudang');

		$result = $this->Stokopname_model->DataStockOpnameCari($tglawal, $tglakhir, $kodegudang);
		echo json_encode($result);
	}

	function DataStokOpname()
	{
		$result = $this->Stokopname_model->DataStockOpname($this->input->post('nomor'));
		echo json_encode($result);
	}
	function DataStockOpnameDetail()
	{
		$result = $this->Stokopname_model->DataStockOpnameDetail($this->input->post('nomor'));
		echo json_encode($result);
	}

	function DataInventoryStock()
	{
		$result = $this->Stokopname_model->DataInventory($this->input->post('kodebarang'), $this->input->post('kodegudang'));
		echo json_encode($result);
	}

	function DataStockOpnameAll()
	{
		$result = $this->Stokopname_model->DataStockOpnameAll($this->input->post('nomor'));
		echo json_encode($result);
	}

	public function ClearChr($param)
	{
		return $result = str_replace(array('\'', '"', ',', ';', '<', '>', '_', '-', ' ', '.', '!', '?', '/', '=', '+', ']', '[', '{', '}', '@', '#', '$', '%', '^', '&', "*", '(', ')'), '', $param);
	}

	function Save()
	{
		$kodecabang =  $this->session->userdata('mycabang');
		$pemakai =  $this->session->userdata('myusername');

		$nomorawal = $this->input->post('nomor');
		$nomor = $this->input->post('nomor');
		
		$this->db->trans_start();
		$this->db->trans_strict(FALSE);
		$cekOpname = $this->Stokopname_model->CekOpname($nomor);

		$ambilnomor = "T" . $kodecabang . "-" . date("Y") . "-" . date("m");

		if (empty($cekOpname)) {
			$get["sto"] = $this->Stokopname_model->GetMaxNomer($ambilnomor);

			if (!$get["sto"]) {
				$nomor = $ambilnomor. "-" . "00001";
			} else {
				$lastNomor = $get['sto']->nomor;
				$lastNoUrut = substr($lastNomor, 11, 11);

				$nextNoUrut = $lastNoUrut + 1;
				$nomor = $ambilnomor. "-" . sprintf('%05s', $nextNoUrut);
				// print_r($nomor);
				// die();
			}

			if (!empty($this->input->post('datadetail'))) {
				foreach ($this->input->post('datadetail') as $key => $value) {
					$data = array(
						'nomor' => $nomor,
						'kodebarang' => $value['Kode'],
						'namabarang' => $value['Nama'],
						'qty' => $this->ClearChr($value['Qty']),
						'qtysistem' => $this->ClearChr($value['QtySistem']),
						'expired' => $value['Expired'],
						'keterangan' => $value['Keterangan'],
						'pemakai' => $pemakai,
						'tglsimpan' => date('Y-m-d H:m:s')
					);
					$this->Stokopname_model->SaveDataDetail($data);
					
				}
			};

			

			$data = array(
				'nomor' => $nomor,
				'tanggal' => $this->input->post('tanggalopname'),
				'keterangan' => $this->input->post('keterangan'),
				'pemakai' => $pemakai,
				'kodecabang' => $kodecabang,
				'kodegudang' => $this->input->post('kodegudang'),
				'tglsimpan' => date('Y-m-d H:m:s')
			);
			// print_r($data);
			// die();
			$this->Stokopname_model->SaveData($data);
			$this->db->trans_complete();
			if ($this->db->trans_status() == FALSE) {
				$resultjson = array(
					'nomor' => "",
					'message' => "Data gagal, Data sudah digunakan"
				);
			} else {
				$resultjson = array(
					'nomor' => $nomor,
					'message' => "Data berhasil disimpan"
				);
			}
		} else if(!empty($nomorawal))  {
			if (!empty($this->input->post('datadetail'))) {
				foreach ($this->input->post('datadetail') as $key => $value) {
					$data = array(
						'nomor' => substr($nomorawal,0,13),
						'kodebarang' => $value['Kode'],
						'namabarang' => $value['Nama'],
						'qty' => $this->ClearChr($value['Qty']),
						'qtysistem' => $this->ClearChr($value['QtySistem']),
						'expired' => $value['Expired'],
						'keterangan' => $value['Keterangan'],
						'pemakai' => $pemakai,
						'tglsimpan' => date('Y-m-d H:m:s')
					);
					$this->Stokopname_model->SaveDataDetail($data);
					
				}
			};

			$data = array(
				'nomor' => substr($nomorawal,0,13),
				'tanggal' => $this->input->post('tanggalopname'),
				'keterangan' => $this->input->post('keterangan'),
				'pemakai' => $pemakai,
				'kodecabang' => $kodecabang,
				'kodegudang' => $this->input->post('kodegudang'),
				'tglsimpan' => date('Y-m-d H:m:s')
			);
			// print_r($data);
			// die();
			$this->Stokopname_model->SaveData($data);

			$this->db->trans_complete();
			if ($this->db->trans_status() == FALSE) {
				$resultjson = array(
					'nomor' => "",
					'message' => "Data gagal, Data sudah digunakan"
				);
			} else {
				$resultjson = array(
					'nomor' => $nomorawal,
					'message' => "Data berhasil disimpan"
				);
			}
		} else {
			$this->db->trans_complete();
			$resultjson = array(
				'nomor' => "",
				'message' => "Data gagal disimpan, Nomor '<b style='color: red'>" . $nomorawal . "</b>' dengan dokumen '<b style='color: red'>" . $nomorawal . "</b>' sudah pernah digunakan."
			);
			$this->db->trans_rollback();
		}
		echo json_encode($resultjson);
	}

	function SaveRecount()
	{
		$kodecabang =  $this->session->userdata('mycabang');
		$pemakai =  $this->session->userdata('myusername');

		$nomorawal = $this->input->post('nomor');
		$nomor = $this->input->post('nomor');
		$nomorref = $this->input->post('norefrensi');
		
		$this->db->trans_start();
		$this->db->trans_strict(FALSE);
		$cekOpname = $this->Stokopname_model->CekOpname($nomor);

		$ambilnomor = "T" . $kodecabang . "-". date("Y"). "-" . date("m");

		if (empty($cekOpname)) {
			$get["sto"] = $this->Stokopname_model->GetMaxNomer($ambilnomor);

			if (!$get["sto"]) {
				$nomor = $ambilnomor. "-" . "00001";
			} else {
				$lastNomor = $get['sto']->nomor;
				$lastNoUrut = substr($lastNomor, 11, 11);

				$nextNoUrut = $lastNoUrut + 1;
				$nomor = $ambilnomor. "-" . sprintf('%05s', $nextNoUrut);
				// print_r($nomor);
				// die();
			}

			if (!empty($this->input->post('datadetail'))) {
				foreach ($this->input->post('datadetail') as $key => $value) {
					$data = array(
						'nomor' => $nomor,
						'kodebarang' => $value['Kode'],
						'namabarang' => $value['Nama'],
						'qty' => $this->ClearChr($value['Qty']),
						'qtysistem' => $this->ClearChr($value['QtySistem']),
						'expired' => $value['Expired'],
						'keterangan' => $value['Keterangan'],
						'pemakai' => $pemakai,
						'tglsimpan' => date('Y-m-d H:m:s')
					);
					$this->Stokopname_model->SaveDataDetail($data);
					
				}
			};

			$data = [
				'status_recount' => false,
			];
			$this->Stokrecount_model->UpdateRecount($nomorref, $data);

			$data = array(
				'nomor' => $nomor,
				'tanggal' => $this->input->post('tanggalopname'),
				'keterangan' => $this->input->post('keterangan'),
				'noreferensi' => $this->input->post('norefrensi'),
				'pemakai' => $pemakai,
				'kodecabang' => $kodecabang,
				'kodegudang' => $this->input->post('kodegudang'),
				'tglsimpan' => date('Y-m-d H:m:s')
			);
			// print_r($data);
			// die();
			$this->Stokopname_model->SaveData($data);
			$this->db->trans_complete();
			if ($this->db->trans_status() == FALSE) {
				$resultjson = array(
					'nomor' => "",
					'message' => "Data gagal, Data sudah digunakan"
				);
			} else {
				$resultjson = array(
					'nomor' => $nomor,
					'message' => "Data berhasil disimpan"
				);
			}
		} else if(!empty($nomorawal))  {
			if (!empty($this->input->post('datadetail'))) {
				foreach ($this->input->post('datadetail') as $key => $value) {
					$data = array(
						'nomor' => substr($nomorawal,0,13),
						'kodebarang' => $value['Kode'],
						'namabarang' => $value['Nama'],
						'qty' => $this->ClearChr($value['Qty']),
						'qtysistem' => $this->ClearChr($value['QtySistem']),
						'expired' => $value['Expired'],
						'keterangan' => $value['Keterangan'],
						'pemakai' => $pemakai,
						'tglsimpan' => date('Y-m-d H:m:s')
					);
					$this->Stokopname_model->SaveDataDetail($data);
					
				}
			};

			$data = array(
				'nomor' => substr($nomorawal,0,13),
				'tanggal' => $this->input->post('tanggalopname'),
				'keterangan' => $this->input->post('keterangan'),
				'pemakai' => $pemakai,
				'kodecabang' => $kodecabang,
				'kodegudang' => $this->input->post('kodegudang'),
				'tglsimpan' => date('Y-m-d H:m:s')
			);
			// print_r($data);
			// die();
			$this->Stokopname_model->SaveData($data);

			$this->db->trans_complete();
			if ($this->db->trans_status() == FALSE) {
				$resultjson = array(
					'nomor' => "",
					'message' => "Data gagal, Data sudah digunakan"
				);
			} else {
				$resultjson = array(
					'nomor' => $nomorawal,
					'message' => "Data berhasil disimpan"
				);
			}
		} else {
			$this->db->trans_complete();
			$resultjson = array(
				'nomor' => "",
				'message' => "Data gagal disimpan, Nomor '<b style='color: red'>" . $nomorawal . "</b>' dengan dokumen '<b style='color: red'>" . $nomorawal . "</b>' sudah pernah digunakan."
			);
			$this->db->trans_rollback();
		}
		echo json_encode($resultjson);
	}

	function CancelRecount(){
		$nomor = $this->input->post('nomor');
		$pemakai =  $this->session->userdata('myusername');
		
		$this->db->trans_start();
		$this->db->trans_strict(FALSE);

		if(!empty($this->input->post('datadetail'))){
			$this->Stokopname_model->DeleteDetail($nomor);
			foreach($this->input->post('datadetail') as $key => $value){
				$data = [
					'nomor' => $nomor,
					'kodebarang' => $value['Kode'],
					'namabarang' => $value['Nama'],
					'qty' => $this->ClearChr($value['Qty']),
					'qtysistem' => $this->ClearChr($value['QtySistem']),
					'expired' => $value['Expired'],
					'keterangan' => $value['Keterangan'],
					'pemakai' => $pemakai,
					'tglsimpan' => date('Y-m-d H:m:s')
				];
				$this->Stokopname_model->SaveDataDetail($data);
			}
		}

		$data = [
			"batal" => true,
			"tglbatal" => date('Y-m-d H:m:s'),
		];
	
		$this->Stokopname_model->CancelRecount($nomor, $data);

		// $data = [
		// 	"nocancel" => $nomor,
		// 	"nodokumen" => $nomor,
		// 	"jenisdokumen" => "STO",
		// 	"tglsimpan" => date('Y-m-d H:m:s'),
		// 	"pemakai" => $pemakai
		// ];
		// $this->Stokopname_model->SaveHistCancel($data);

		$this->db->trans_complete();
		if($this->db->trans_status() == FALSE){
			$resultjson = [
				'nomor' => "",
				'message' => "Data gagal dicancel",
			];
		} else {
			$resultjson = [ 
				'nomor' => $nomor,
				'message' => "Data berhasil dicancel",
			];
		}
		echo json_encode($resultjson);
	}

	function Update()
	{
		$pemakai =  $this->session->userdata('myusername');
		$kodecabang =  $this->session->userdata('mycabang');

		$nomor = $this->input->post('nomor');
		// print_r($nomor);
		// die();

		$this->db->trans_start();
		$this->db->trans_strict(FALSE);

		if (!empty($this->input->post('datadetail'))) {
			$this->Stokopname_model->DeleteDetail($nomor);
			foreach ($this->input->post('datadetail') as $key => $value) {
				$data = [
					'nomor' => $nomor,
					'kodebarang' => $value['Kode'],
					'namabarang' => $value['Nama'],
					'qty' => $this->ClearChr($value['Qty']),
					'qtysistem' => $this->ClearChr($value['QtySistem']),
					'expired' => $value['Expired'],
					'keterangan' => $value['Keterangan'],
					'pemakai' => $pemakai,
					'tglsimpan' => date('Y-m-d H:m:s')
				];
				$this->Stokopname_model->SaveDataDetail($data);
			}
		}

		$data = [
			'nomor' => $nomor,
			'tanggal' => $this->input->post('tanggalopname'),
			'keterangan' => $this->input->post('keterangan'),
			'kodegudang' => $this->input->post('kodegudang'),
			'kodecabang' => $kodecabang,
			'pemakai' => $pemakai,
			'tglsimpan' => date('Y-m-d H:m:s')
		];
		$this->Stokopname_model->UpdateData($nomor, $data);

		$this->db->trans_complete();
		if ($this->db->trans_status() == FALSE) {
			$resultjson = array(
				'nomor' => "",
				'message' => "Data gagal diperbarui, Kode '<b style='color: red'>" . $nomor . "</b>' sudah pernah digunakan."
			);
			$this->db->trans_rollback();
		} else {
			$resultjson = array(
				'nomor' => $nomor,
				'message' => "Data berhasil di perbarui"
			);
			$this->db->trans_commit();
		}
		echo json_encode($resultjson);
	}
}
