<?php

defined('BASEPATH') or exit('No direct script access allowed');

class PenerimaanRepack extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set("Asia/Jakarta");
        $this->load->model('inventory/PenerimaanRepack_model');
        $this->load->model('global/global_model');
        $this->load->model('Caridataaktif_model');
        $this->load->library('form_validation');
        $this->load->library('session');
    }

    function CariDataPenerimaan()
    {
        $fetch_data = $this->Caridataaktif_model->make_datatables($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value'));
        $data = array();
        foreach ($fetch_data as $row) {
            $sub_array = array();
            $i = 1;
            $count = count($this->input->post('field'));
            foreach ($this->input->post('field') as $key => $value) {
                if ($i <= $count) {
                    if ($i == 1) {
                        $msearch = $row->$value;
                        $sub_array[] = '<button class="btn btn-dark searchpenerimaan" data-dismiss="modal" style="margin: 0px;" data-id="' . $msearch . '"><i class="far fa-check-circle"></i></button>';
                        $sub_array[] = $row->$value;
                    } else {
                        if ($i == $count) {
                            $sub_array[] = $row->$value;
                        } else {
                            $sub_array[] = $row->$value;
                        }
                    }
                    // $sub_array[] = $row->$value;
                }
                $i++;
            }
            $data[] = $sub_array;
        }
        $output = array(
            "draw"                    =>     intval($_POST["draw"]),
            "recordsTotal"          =>      $this->Caridataaktif_model->get_all_data($this->input->post('nmtb')),
            "recordsFiltered"     =>     $this->Caridataaktif_model->get_filtered_data($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value')),
            "data"                    =>     $data
        );
        echo json_encode($output);
    }

    function DataPenerimaan()
    {
        $result = $this->PenerimaanRepack_model->DataPenerimaan($this->input->post('nomor'));
        echo json_encode($result);
    }

    function DataPenerimaanDetail()
    {
        $result = $this->PenerimaanRepack_model->DataPenerimaanDetail($this->input->post('nomor'));
        echo json_encode($result);
    }

    function CariDataSJGudang()
    {
        $fetch_data = $this->Caridataaktif_model->make_datatables($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value'));
        $data = array();
        foreach ($fetch_data as $row) {
            $sub_array = array();
            $i = 1;
            $count = count($this->input->post('field'));
            foreach ($this->input->post('field') as $key => $value) {
                if ($i <= $count) {
                    if ($i == 1) {
                        $msearch = $row->$value;
                        $sub_array[] = '<button class="btn btn-dark searchsjgd" data-dismiss="modal" style="margin: 0px;" data-id="' . $msearch . '"><i class="far fa-check-circle"></i></button>';
                        $sub_array[] = $row->$value;
                    } else {
                        if ($i == $count) {
                            $sub_array[] = $row->$value;
                        } else {
                            $sub_array[] = $row->$value;
                        }
                    }
                    // $sub_array[] = $row->$value;
                }
                $i++;
            }
            $data[] = $sub_array;
        }
        $output = array(
            "draw"                    =>     intval($_POST["draw"]),
            "recordsTotal"          =>      $this->Caridataaktif_model->get_all_data($this->input->post('nmtb')),
            "recordsFiltered"     =>     $this->Caridataaktif_model->get_filtered_data($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value')),
            "data"                    =>     $data
        );
        echo json_encode($output);
    }

    function DataSJGudang()
    {
        $result = $this->PenerimaanRepack_model->DataSJGudang($this->input->post('nomor'));
        echo json_encode($result);
    }

    function DataSJGudangDetail()
    {
        $result = $this->PenerimaanRepack_model->DataSJGudangDetail($this->input->post('nomor'));
        echo json_encode($result);
    }

    public function ClearChr($param)
    {
        return $result = str_replace(array('\'', '"', ',', ';', '<', '>', '_', '-', ' ', '.', '!', '?', '/', '=', '+', ']', '[', '{', '}', '@', '#', '$', '%', '^', '&', "*", '(', ')'), '', $param);
    }

    function Save()
    {
        $kodecabang = $this->session->userdata('mycabang');
        $kodegudang = $this->session->userdata('mygudang');
        $pemakai = $this->session->userdata('myusername');
        $periode = date("Y") . date("m");

        $nomor = $this->input->post('nomor');
        $ceknomor = $this->PenerimaanRepack_model->CekPenerimaan($nomor);

        $this->db->trans_start(); # Starting Transaction
        $this->db->trans_strict(FALSE);

        $ambilnomor = "P" . $kodecabang . "-" . date("Y") . "-" . date("m");

        if (empty($nomor)) {
            $get["prs"] = $this->PenerimaanRepack_model->GetMaxNomor($ambilnomor);
            if (!$get["prs"]) {
                $nomor = $ambilnomor . "-" . "00001";
            } else {
                $lastNomor = $get['prs']->nomor;
                $lastNoUrut = substr($lastNomor, 11, 11);

                // nomor urut ditambah 1
                $nextNoUrut = $lastNoUrut + 1;
                $nomor = $ambilnomor . "-" . sprintf('%05s', $nextNoUrut);
            }
            if (empty($ceknomor)) {
                if (!empty($this->input->post('datadetail'))) {
                    foreach ($this->input->post('datadetail') as $key => $value) {

                        # Data Detail Penerimaan #
                        $datadetail = [
                            'nomor' => $nomor,
                            'kodebarang' => $value['Kode'],
                            'namabarang' => $value['Nama'],
                            'qty' => $this->ClearChr($value['Qty']),
                            'pemakai' => $pemakai,
                            'tglsimpan' => date('Y-m-d H:i:s'),
                        ];
                        $this->PenerimaanRepack_model->SaveDataDetail($datadetail);

                        $datasjgudang = $this->PenerimaanRepack_model->DataSJGudang($this->input->post('nosjgudang'));
                        foreach ($datasjgudang as $datasjgudang) {
                            $cabang = $datasjgudang->cabangtujuan;
                            $gudang = $datasjgudang->lokasi;
                        }

                        # Data Surat Jalan Gudang #
                        $datainvsjgudang = $this->PenerimaanRepack_model->DataInv($periode, $value['Kode'], $cabang, $gudang);
                        foreach ($datainvsjgudang as $key => $valprod) {
                            $dataprod = [
                                'qtypick' => $valprod->qtypick - $this->ClearChr($value['Qty']),
                                'qtymasuk' => $valprod->qtymasuk + $this->ClearChr($value['Qty']),
                            ];
                            // print_r($dataprod);
                            $this->PenerimaanRepack_model->UpdateStok($periode, $value['Kode'], $cabang, $gudang, $dataprod);
                        }

                        if ($this->input->post('jenisdokumen') == '1') {

                            #Data Req Repack#

                            $datanoreq = $this->PenerimaanRepack_model->DataSJGudang($this->input->post('nosjgudang'));
                            foreach ($datanoreq as $key => $valreq) {
                                $cabangreq = $valreq->kodecabang;
                                $gudangreq = $valreq->asal;
                                $nomorreq = $valreq->noproses;

                                $datainvreq = $this->PenerimaanRepack_model->DataInv($periode, $value['Kode'], $cabangreq, $gudangreq);
                                foreach ($datainvreq as $key => $valasal) {
                                    $datainvasal = [
                                        'qtypick' => $valasal->qtypick - $this->ClearChr($value['Qty']),
                                        'qtykeluar' => $valasal->qtykeluar + $this->ClearChr($value['Qty']),
                                    ];
                                    $this->PenerimaanRepack_model->UpdateStok($periode, $value['Kode'], $cabangreq, $gudangreq, $datainvasal);
                                }

                                $datareq = [
                                    "terima" => true,
                                    "tglterima" => date("Y-m-d H:i:s"),
                                    "userterima" => $pemakai
                                ];
                                $this->PenerimaanRepack_model->UpdateReq($nomorreq, $datareq);
                            }
                        } else {
                            $datapros = $this->PenerimaanRepack_model->DataSJGudang($this->input->post('nosjgudang'));
                            foreach ($datapros as $key => $valpros) {
                                $cabangpros = $valpros->kodecabang;
                                $gudangpros = $valpros->asal;

                                $datainvpros = $this->PenerimaanRepack_model->DataInv($periode, $value['Kode'], $cabangpros, $gudangpros);
                                foreach ($datainvpros as $key => $valasal) {
                                    $datainvasal = [
                                        'qtypick' => $valasal->qtypick - $this->ClearChr($value['Qty']),
                                        'qtykeluar' => $valasal->qtykeluar + $this->ClearChr($value['Qty']),
                                    ];
                                    $this->PenerimaanRepack_model->UpdateStok($periode, $value['Kode'], $cabangpros, $gudangpros, $datainvasal);
                                    // print_r($value['Kode']);
                                }
                            }
                        }
                    }
                }
                $datanoreq = $this->PenerimaanRepack_model->DataSJGudang($this->input->post('nosjgudang'));
                foreach ($datanoreq as $key => $valreq) {
                    $cabangtj = $valreq->cabangtujuan;
                    $gudangtj = $valreq->lokasi;
                }
                if ($this->input->post('jenisdokumen') == '1') {
                    $data = [
                        'nomor' => $nomor,
                        'tanggal' => $this->input->post('tanggal'),
                        'nosjgudang' => $this->input->post('nosjgudang'),
                        'jenis' => $this->input->post('jenisdokumen'),
                        'pemakai' => $pemakai,
                        'tglsimpan' => date('Y-m-d H:i:s'),
                        'kodecabang' => $cabangtj,
                        'kodegudang' => $gudangtj
                    ];
                    $this->PenerimaanRepack_model->SaveData($data);
                } else {
                    $data = [
                        'nomor' => $nomor,
                        'tanggal' => $this->input->post('tanggal'),
                        'nosjgudang' => $this->input->post('nosjgudang'),
                        'jenis' => $this->input->post('jenisdokumen'),
                        'proses' => true,
                        'tglproses' => date('Y-m-d H:i:s'),
                        'userproses' => $pemakai,
                        'pemakai' => $pemakai,
                        'tglsimpan' => date('Y-m-d H:i:s'),
                        'kodecabang' => $cabangtj,
                        'kodegudang' => $gudangtj
                    ];
                    $this->PenerimaanRepack_model->SaveData($data);
                }

                $datasjgudang = [
                    "terima" => true,
                    "tglterima" => date("Y-m-d H:i:s"),
                    "userterima" => $pemakai
                ];
                // print_r($datasjgudang);
                $this->PenerimaanRepack_model->UpdateSJGudang($this->input->post('nosjgudang'), $datasjgudang);
            }
        }
        // die();
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE) {
            $resultjson = array(
                'nomor' => "",
                'message' => "Data gagal disimpan, Nomor '<b style='color: red'>" . $nomor . "</b>' sudah pernah digunakan."
            );
            # Something went wrong.
            $this->db->trans_rollback();
        } else {
            $resultjson = array(
                'nomor' => $nomor,
                'message' => "Data berhasil disimpan."
            );
            # Everything is Perfect. 
            # Committing data to the database.
            $this->db->trans_commit();
        }
        echo json_encode($resultjson);
    }

    function Update()
    {
        $kodecabang = $this->session->userdata('mycabang');
        $pemakai = $this->session->userdata('myusername');

        $nomor = $this->input->post('nomor');

        $this->db->trans_start(); # Starting Transaction
        $this->db->trans_strict(FALSE);

        if (!empty($this->input->post('datadetail'))) {
            $this->PenerimaanRepack_model->DeleteDetail($nomor);
            foreach ($this->input->post('datadetail') as $key => $value) {
                $datadetail = [
                    'nomor' => $nomor,
                    'kodebarang' => $value['Kode'],
                    'namabarang' => $value['Nama'],
                    'qty' => $this->ClearChr($value['Qty']),
                    'pemakai' => $pemakai,
                    'tglsimpan' => date('Y-m-d H:i:s'),
                ];
                $this->PenerimaanRepack_model->SaveDataDetail($datadetail);
            }
            $data = [
                'nomor' => $nomor,
                'tanggal' => $this->input->post('tanggal'),
                'nosjgudang' => $this->input->post('nosjgudang'),
                'pemakai' => $pemakai,
                'tglsimpan' => date('Y-m-d H:i:s')
            ];
            $this->PenerimaanRepack_model->UpdateData($nomor, $data);
        }
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE) {
            $resultjson = array(
                'nomor' => "",
                'message' => "Data gagal disimpan, Nomor '<b style='color: red'>" . $nomor . "</b>' sudah pernah digunakan."
            );
            # Something went wrong.
            $this->db->trans_rollback();
        } else {
            $resultjson = array(
                'nomor' => $nomor,
                'message' => "Data berhasil disimpan."
            );
            # Everything is Perfect. 
            # Committing data to the database.
            $this->db->trans_commit();
        }
        echo json_encode($resultjson);
    }

    function Cancel()
    {

        $kodecabang =  $this->session->userdata('mycabang');
        $pemakai =  $this->session->userdata('myusername');
        $nomor = $this->input->post('nomor');
        $keterangan = $this->input->post('keterangan');
        $errorvalidasi = FALSE;
        $where = array(
            "nomor" => $nomor
        );
        $dataproses = $this->global_model->GetDataGlobal("srvt_prosesrepact", $where);
        if (empty($this->input->post('datadetail'))) {
            $resultjson = array(
                'nomor' => "",
                'message' => "Data gagal dibatalkan, Data Detail Tidak Ada."
            );
            $errorvalidasi = TRUE;
            echo json_encode($resultjson);
            return FALSE;
        }

        foreach ($dataproses as $key => $val) {;
            if ($val->kirim == 't') {
                $resultjson = array(
                    'nomor' => "",
                    'message' => "Data gagal dibatalkan, Penerimaan Repack Barang dengan Nomor '<b style='color: red'>" . $nomor . "</b>' Sudah Melakukan Penerimaan Barang."
                );
                $errorvalidasi = TRUE;
                echo json_encode($resultjson);
                return FALSE;
            }
        }

        if ($errorvalidasi == FALSE) {
            $this->db->trans_start(); # Starting Transaction
            $this->db->trans_strict(FALSE);

            $dataheader = array(
                'batal' => true,
                'userbatal' => $pemakai,
                'alasanbatal' => $keterangan,
                'tglbatal' => date('Y-m-d H:i:s'),
                'kodecabang' => $kodecabang
            );
            $wheredata = array("nomor" => $nomor);
            $this->global_model->UpdateDataGlobal("srvt_penerimaanrepack", $dataheader, $wheredata);
            $this->db->trans_complete();

            if ($this->db->trans_status() === FALSE) {
                $resultjson = array(
                    'nomor' => "",
                    'message' => "Data gagal dibatalkan, Nomor '<b style='color: red'>" . $nomor . "</b>' tidak ada."
                );
                # Something went wrong.
                $this->db->trans_rollback();
            } else {
                $resultjson = array(
                    'nomor' => $nomor,
                    'message' => "Data berhasil dibatalkan."
                );
                # Everything is Perfect. 
                # Committing data to the database.
                $this->db->trans_commit();
            }

            echo json_encode($resultjson);
            return FALSE;
        }
    }
}
