<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Bag extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('inventory/Inventorybag_model');
		$this->load->model('caridataaktif_model');
		$this->load->model('global/global_model');
		$this->load->library('form_validation');
		$this->load->library('session');
	}

	function CariDataBag()
	{
		$fetch_data = $this->caridataaktif_model->make_datatables($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value'));
		$data = array();
		foreach ($fetch_data as $row) {
			$sub_array = array();
			$i = 1;
			$count = count($this->input->post('field'));
			foreach ($this->input->post('field') as $key => $value) {
				if ($i <= $count) {
					if ($i == 1) {
						$msearch = $row->$value;
						$sub_array[] = '<button class="btn btn-dark searchbag" data-dismiss="modal" style="margin: 0px;" data-id="' . $msearch . '"><i class="far fa-check-circle"></i></button>';
						$sub_array[1] = '<button class="btn btn-sm btn-dark printbag" style="margin: 0px; background-color: #007bff; border-color: #007bff;" data-id="' . $msearch . '"><i class="fas fa-print"></i>&nbsp;Cetak BAG</button>';
						$sub_array[] = $row->$value;
					} else {
						if ($i == $count) {
							$sub_array[] = $row->$value;
						} else {
							$sub_array[] = $row->$value;
						}
					}
					// $sub_array[] = $row->$value;
				}
				$i++;
			}
			$data[] = $sub_array;
		}
		$output = array(
			"draw"                    =>     intval($_POST["draw"]),
			"recordsTotal"          =>      $this->caridataaktif_model->get_all_data($this->input->post('nmtb')),
			"recordsFiltered"     =>     $this->caridataaktif_model->get_filtered_data($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value')),
			"data"                    =>     $data
		);
		echo json_encode($output);
	}

	function CariDataListBag()
	{
		$fetch_data = $this->caridataaktif_model->make_datatables($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value'));
		$data = array();
		foreach ($fetch_data as $row) {
			$sub_array = array();
			$i = 1;
			$count = count($this->input->post('field'));
			foreach ($this->input->post('field') as $key => $value) {
				if ($i <= $count) {
					if ($i == 1) {
						$msearch = $row->$value;
						// $sub_array[] = '<button class="btn btn-dark searchbag" data-dismiss="modal" style="margin: 0px;" data-id="' . $msearch . '"><i class="far fa-check-circle"></i></button>';
						$sub_array[] = $row->$value;
					} else {
						if ($i == $count) {
							$sub_array[] = $row->$value;
						} else {
							$sub_array[] = $row->$value;
						}
					}
					// $sub_array[] = $row->$value;
				}
				$i++;
			}
			$data[] = $sub_array;
		}
		$output = array(
			"draw"                    =>     intval($_POST["draw"]),
			"recordsTotal"          =>      $this->caridataaktif_model->get_all_data($this->input->post('nmtb')),
			"recordsFiltered"     =>     $this->caridataaktif_model->get_filtered_data($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value')),
			"data"                    =>     $data
		);
		echo json_encode($output);
	}

	function DataBag()
	{
		$result = $this->Inventorybag_model->DataBag($this->input->post('nomor'));
		echo json_encode($result);
	}

	function DataBagDetail()
	{
		$result = $this->Inventorybag_model->DataBagDetail($this->input->post('nomor'));
		echo json_encode($result);
	}

	public function ClearChr($param)
	{
		return $result = str_replace(array('\'', '"', ',', ';', '<', '>', '_', '-', ' ', '.', '!', '?', '/', '=', '+', ']', '[', '{', '}', '@', '#', '$', '%', '^', '&', "*", '(', ')'), '', $param);
	}

	function Save()
	{
		$kodecabang =  $this->session->userdata('mycabang');
		$pemakai =  $this->session->userdata('myusername');
		$periode = date('Y') . date('m');
		$kodegudang = $this->input->post('lokasi');

		$nomor = $this->input->post('nobag');

		$cekbag = $this->Inventorybag_model->CekNoBag($nomor);

		if (empty($cekbag)) {
			$this->db->trans_start();
			$this->db->trans_strict(FALSE);

			$ambilnomor = "U" . $kodecabang . "-" . date("Y") . "-" . date("m");
			$get["ba"] = $this->Inventorybag_model->GetMaxNomor($ambilnomor);

			if (!$get["ba"]) {
				$nomor = $ambilnomor . "-" . "00001";
			} else {
				$lastNomor = $get['ba']->nomor;
				$lastNoUrut = substr($lastNomor, 11, 11);

				$nextNoUrut = $lastNoUrut + 1;
				$nomor = $ambilnomor . "-" . sprintf('%05s', $nextNoUrut);
			}

			if (!empty($this->input->post('datadetail'))) {
				foreach ($this->input->post('datadetail') as $key => $value) {
					$data = array(
						'nomor' => $nomor,
						'kodebarang' => $value['Kode'],
						'namabarang' => $value['Nama'],
						'qty' => $this->ClearChr($value['Qty']),
						'tglsimpan' => date('Y-m-d H:i:s'),
						'pemakai' => $pemakai
					);
					$this->Inventorybag_model->SaveBagDetail($data);

					// $getinv = $this->Inventorybag_model->DataInv($periode, $value['Kode'], $kodecabang, $kodegudang);
					// foreach ($getinv as $key => $values) {
					// 	$datainv = array(
					// 		'qtypick' => $values->qtypick + $this->ClearChr($value['Qty'])
					// 	);

					// 	$this->Inventorybag_model->UpdateStok($periode, $value['Kode'], $kodecabang, $kodegudang, $datainv);
					// }
				}
			};

			$data = array(
				'nomor' => $nomor,
				'tanggal' => $this->input->post('tanggalbag'),
				'jenis' => $this->input->post('jenis'),
				'lokasi' => $this->input->post('lokasi'),
				'keterangan' => $this->input->post('keterangan'),
				'tglsimpan' => date('Y-m-d H:i:s'),
				'pemakai' => $pemakai,
				'kodecabang' => $kodecabang
			);
			// print_r($data);
			// die();
			$this->Inventorybag_model->SaveData($data);

			$this->db->trans_complete();
			if ($this->db->trans_status() == FALSE) {
				$resultjson = array(
					'nomor' => "",
					'message' => "Data gagal disimpan, Data Sudah digunakan"
				);
				$this->db->trans_rollback();
			} else {
				$resultjson = array(
					'nomor' => $nomor,
					'message' => "Data berhasil disimpan"
				);
			}
		} else {
			$this->db->trans_complete();
			$resultjson = array(
				'kode' => "",
				'kodebarang' => "",
				'message' => "Data gagal disimpan, Kode '<b style='color: red'>" . $nomor . "</b>' dengan dokumen '<b style='color: red'>" . $nomor . "</b>' sudah pernah digunakan."
			);
			$this->db->trans_rollback();
		}
		echo json_encode($resultjson);
	}

	function Approve()
	{

		$kodecabang =  $this->session->userdata('mycabang');
		$pemakai =  $this->session->userdata('myusername');

		$nomor = $this->input->post('nomorbag');
		$periode = date('Y') . date('m');
		$kodegudang = $this->input->post('lokasi');


		$this->db->trans_start();
		$this->db->trans_strict(FALSE);


		if ($this->input->post('jenis') == '1' || $this->input->post('jenis') == '3' || $this->input->post('jenis') == '4' || $this->input->post('jenis') == '5' || $this->input->post('jenis') == '6') {

			$data = array(
				'approve' => true,
				'tglapprove' => date('Y-m-d H:i:s'),
				'userapprove' => $pemakai
			);
			$this->Inventorybag_model->ApproveData($nomor, $data);

			$data = array(
				'nodokumen' => $nomor,
				'jenisdokumen' => 'BAG',
				'tglsimpan' => date('Y-m-d H:i:s'),
				'pemakai' => $pemakai
			);
			$this->Inventorybag_model->SaveHistoriApprove($data);

			foreach ($this->input->post('datadetail') as $key => $value) {
				$getinv = $this->Inventorybag_model->DataInv($periode, $value['Kode'], $kodecabang, $kodegudang);
				foreach ($getinv as $key => $values) {
					$data = array(
						// 'qtypick' => $values->qtypick - $this->ClearChr($value['Qty']),
						'qtykeluar' => $values->qtykeluar + $this->ClearChr($value['Qty'])
					);

					$this->Inventorybag_model->UpdateStok($periode, $value['Kode'], $kodecabang, $kodegudang, $data);
				}
			}
		} else {
			$data = array(
				'approve' => true,
				'tglapprove' => date('Y-m-d H:i:s'),
				'userapprove' => $pemakai
			);
			$this->Inventorybag_model->ApproveData($nomor, $data);

			$data = array(
				'nodokumen' => $nomor,
				'jenisdokumen' => 'BAG',
				'tglsimpan' => date('Y-m-d H:i:s'),
				'pemakai' => $pemakai
			);
			$this->Inventorybag_model->SaveHistoriApprove($data);

			foreach ($this->input->post('datadetail') as $key => $value) {
				$getinv = $this->Inventorybag_model->DataInv($periode, $value['Kode'], $kodecabang, $kodegudang);
				foreach ($getinv as $key => $values1) {
					$data = array(
						// 'qtypick' => $values1->qtypick - $this->ClearChr($value['Qty']),
						'qtymasuk' => $values1->qtymasuk + $this->ClearChr($value['Qty'])
					);

					$this->Inventorybag_model->UpdateStok($periode, $value['Kode'], $kodecabang, $kodegudang, $data);
				}
			}
		}

		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE) {
			$resultjson = array(
				'nomor' => "",
				'message' => "Data gagal Approve."
			);
			$this->db->trans_rollback();
		} else {
			$resultjson = array(
				'nomor' => $nomor,
				'message' => "Data berhasil diapprove."
			);
			$this->db->trans_commit();
		}
		echo json_encode($resultjson);
	}

	function Cancel()
	{
		$kodecabang =  $this->session->userdata('mycabang');
		$pemakai =  $this->session->userdata('myusername');
		$nomor = $this->input->post('nomor');
		$periode = date('Y') . date('m');
		$kodegudang = $this->input->post('lokasi');
		$ValidasiCancel = FALSE;

		$this->db->trans_start();
		$this->db->trans_strict(FALSE);

		$cekdokumen = $this->Inventorybag_model->CekHist($nomor);
		if (!empty($cekdokumen)) {
			$resultjson = array(
				'error' => true,
				'message' => "Data gagal dibatalkan, Nomor " . $nomor . " BAG Sudah di approve"
			);
			$ValidasiCancel = true;
			echo json_encode($resultjson);
		};

		if ($ValidasiCancel == FALSE) {
			if ($this->input->post('jenis') == '1' || $this->input->post('jenis') == '3' || $this->input->post('jenis') == '4') {
				foreach ($this->input->post('datadetail') as $key => $value) {
					$getinv = $this->Inventorybag_model->DataInv($periode, $value['Kode'], $kodecabang, $kodegudang);
					foreach ($getinv as $val) {
						$data = array(
							'qtymasuk' => $val->qtymasuk - $this->ClearChr($value['Qty'])
						);
						
						$this->Inventorybag_model->UpdateStok($periode, $value['Kode'], $kodecabang, $kodegudang, $data);
					}
				}
			} else {
				foreach ($this->input->post('datadetail') as $key => $value) {
					$getinv = $this->Inventorybag_model->DataInv($periode, $value['Kode'], $kodecabang, $kodegudang);
					foreach ($getinv as $val) {
						$data = array(
							'qtykeluar' => $val->qtykeluar - $this->ClearChr($value['Qty'])
						);

						$this->Inventorybag_model->UpdateStok($periode, $value['Kode'], $kodecabang, $kodegudang, $data);
					}
				}
			}

			$data = array(
				'keteranganbatal' => $this->input->post('keterangan'),
				'batal' => true,
				'tglbatal' => date('Y-m-d H:i:s'),
				'userbatal' => $pemakai
			);

			// print_r($data);
			// die();
			$this->Inventorybag_model->CancelData($nomor, $data);

			$this->db->trans_complete();

			if ($this->db->trans_status() == FALSE) {
				$resultjson = array(
					'nomor' => "",
					'message' => "Data gagal dibatalkan"
				);
				$this->db->trans_rollback();
			} else {
				$resultjson = array(
					'nomor' => $nomor,
					'message' => "Data berhasil di batalkan"
				);
				$this->db->trans_rollback();
			}
			echo json_encode($resultjson);
		}
	}

	function Update()
	{
		$kodecabang =  $this->session->userdata('mycabang');
		$pemakai =  $this->session->userdata('myusername');
		$nomor = $this->input->post('nomorbag');
		$periode = date('Y') . date('m');
		$kodegudang = $this->input->post('lokasi');

		$this->db->trans_start();
		$this->db->trans_strict(FALSE);


		if (!empty($this->input->post('datadetail'))) {
			$this->Inventorybag_model->DeleteDetail($nomor);
			foreach ($this->input->post('datadetail') as $key => $value) {

				$datadetail = [
					'nomor' => $nomor,
					'kodebarang' => $value['Kode'],
					'namabarang' => $value['Nama'],
					'qty' => $this->ClearChr($value['Qty']),
					'tglsimpan' => date('Y-m-d H:i:s'),
					'pemakai' => $pemakai
				];
				$this->Inventorybag_model->SaveBagDetail($datadetail);

				// $getinv = $this->Inventorybag_model->DataInv($periode, $value['Kode'], $kodecabang, $kodegudang);

				// foreach ($getinv as $val) {
				// 	$nilaiinv = $val->qtypick;
				// 	$datainv = array(
				// 		'qtypick' => $nilaiinv + $nilaibag + $this->ClearChr($value['Qty'])
				// 	);
				// 	$this->Inventorybag_model->UpdateStok($periode, $value['Kode'], $kodecabang, $kodegudang, $datainv);
				// }

			}
		}

		$data = [
			'nomor' => $nomor,
			'tanggal' => $this->input->post('tanggalbag'),
			'jenis' => $this->input->post('jenis'),
			'lokasi' => $this->input->post('lokasi'),
			'keterangan' => $this->input->post('keterangan'),
			'pemakai' => $pemakai,
			'tglsimpan' => date('Y-m-d H:m:s'),
			'kodecabang' => $kodecabang
		];
		$this->Inventorybag_model->UpdateData($data, $nomor);
		$this->db->trans_complete();

		if ($this->db->trans_status() == FALSE) {
			$resultjson = array(
				'nomor' => "",
				'message' => "Data gagal diperbarui, Kode '<b style='color: red'>" . $nomor . "</b>' sudah pernah digunakan."
			);
			$this->db->trans_rollback();
		} else {
			$resultjson = array(
				'nomor' => $nomor,
				'message' => "Data berhasil di perbarui"
			);
			$this->db->trans_commit();
		}
		echo json_encode($resultjson);
	}

	function LoadGudang()
	{
		$result = $this->global_model->GetDataGlobal("glbm_gudang", array("kodecabang" => $this->input->post('cabang')));
		echo json_encode($result);
	}
}
