<?php
defined('BASEPATH') or exit('No direct script access allowed');

class StockOpnameRecount extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('inventory/Stockrecount_model');
		$this->load->model('inventory/Stokopname_model');
		$this->load->model('inventory/Inventorybag_model');
		$this->load->model('caridataaktif_model');
		$this->load->library('form_validation');
		$this->load->library('session');
	}

	function CariDataStockRecount()
	{
		$fetch_data = $this->caridataaktif_model->make_datatables($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value'));
		$data = array();
		foreach ($fetch_data as $row) {
			$sub_array = array();
			$i = 1;
			$count = count($this->input->post('field'));
			foreach ($this->input->post('field') as $key => $value) {
				if ($i <= $count) {
					if ($i == 1) {
						$msearch = $row->$value;
						$sub_array[] = '<button class="btn btn-dark searchstockrecount" data-dismiss="modal" style="margin: 0px;" data-id="' . $msearch . '"><i class="far fa-check-circle"></i></button>';
						$sub_array[] = $row->$value;
					} else {
						if ($i == $count) {
							$sub_array[] = $row->$value;
						} else {
							$sub_array[] = $row->$value;
						}
					}
					// $sub_array[] = $row->$value;
				}
				$i++;
			}
			$data[] = $sub_array;
		}
		$output = array(
			"draw"                    =>     intval($_POST["draw"]),
			"recordsTotal"          =>      $this->caridataaktif_model->get_all_data($this->input->post('nmtb')),
			"recordsFiltered"     =>     $this->caridataaktif_model->get_filtered_data($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value')),
			"data"                    =>     $data
		);
		echo json_encode($output);
	}

	function CariDataStockRecountAfterConfirm()
	{
		$fetch_data = $this->caridataaktif_model->make_datatables($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value'));
		$data = array();
		foreach ($fetch_data as $row) {
			$sub_array = array();
			$i = 1;
			$count = count($this->input->post('field'));
			foreach ($this->input->post('field') as $key => $value) {
				if ($i <= $count) {
					if ($i == 1) {
						$msearch = $row->$value;
						$sub_array[] = '<button class="btn btn-dark viewdata" data-toggle="modal" data-target="#datatarget" style="margin: 0px;" data-id="' . $msearch . '"><i class="far fa-check-circle"></i></button>';
						$sub_array[] = $row->$value;
					} else {
						if ($i == $count) {
							$sub_array[] = $row->$value;
						} else {
							$sub_array[] = $row->$value;
						}
					}
					// $sub_array[] = $row->$value;
				}
				$i++;
			}
			$data[] = $sub_array;
		}
		$output = array(
			"draw"                    =>     intval($_POST["draw"]),
			"recordsTotal"          =>      $this->caridataaktif_model->get_all_data($this->input->post('nmtb')),
			"recordsFiltered"     =>     $this->caridataaktif_model->get_filtered_data($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value')),
			"data"                    =>     $data
		);
		echo json_encode($output);
	}

	public function ClearChr($param)
	{
		return $result = str_replace(array('\'', '"', ',', ';', '<', '>', '_', '-', ' ', '.', '!', '?', '/', '=', '+', ']', '[', '{', '}', '@', '#', '$', '%', '^', '&', "*", '(', ')'), '', $param);
	}

	function DataStockRecount()
	{
		$result = $this->Stockrecount_model->DataStockRecount($this->input->post('nomor'));
		echo json_encode($result);
	}

	function DataInventoryStock()
	{
		$result = $this->Stockrecount_model->DataInventory($this->input->post('kodebarang'));
		echo json_encode($result);
	}

	function DataStockRecountDetail()
	{
		$result = $this->Stockrecount_model->DataStockRecountDetail($this->input->post('nomor'));
		echo json_encode($result);
	}

	function DataStockRecountSelisih()
	{
		$result = $this->Stockrecount_model->DataStockRecountSelisih($this->input->post('nomor'));
		echo json_encode($result);
	}

	function Proses()
	{
		$kodecabang =  $this->session->userdata('mycabang');
		$pemakai =  $this->session->userdata('myusername');
		$nomor = $this->input->post('nomor');

		$this->db->trans_start();
		$this->db->trans_strict(FALSE);


		$CekRecount = $this->Stockrecount_model->CekNoStockRecount($nomor);
		$ambilnomor = "R" . $kodecabang . "-" . date("Y") . "-" . date("m");
		if (empty($CekRecount)) {

			$get["str"] = $this->Stockrecount_model->GetMaxNomor($ambilnomor);

			if (!$get["str"]) {
				$nomor = $ambilnomor . "-" . "00001";
			} else {
				$lastNomor = $get['str']->nomor;
				$lastNoUrut = substr($lastNomor, 11, 11);

				$nextNoUrut = $lastNoUrut + 1;
				$nomor = $ambilnomor . "-" . sprintf('%05s', $nextNoUrut);
				// print_r($nomor);
				// die();
			}


			if (!empty($this->input->post('datadetail'))) {
				foreach ($this->input->post('datadetail') as $key => $value) {
					$data = array(
						'nomor' => $nomor,
						'nomorsto' => $value['Nomor'],
						'tglsto' => $value['Tanggal'],
						'kodebarang' => $value['Kode'],
						'namabarang' => $value['Nama'],
						'qty' => $this->ClearChr($value['Qty']),
						'qtysistem' => $this->ClearChr($value['QtySistem']),
						'tglexpired' => $value['Expired'],
						'keterangan' => $value['Keterangan'],
						'tglsimpan' => date('Y-m-d H:m:s'),
						'pemakai' => $pemakai
					);
					$this->Stockrecount_model->SaveDataDetail($data);

					if ($this->ClearChr($value['Qty']) < $this->ClearChr($value['QtySistem'])) {
						$selisih = $this->ClearChr($value['Qty']) - $this->ClearChr($value['QtySistem']);
						$data = [
							'nomor' => $nomor,
							'tanggal' => $this->input->post('tanggal'),
							'nomorsto' => $value['Nomor'],
							'tglsto' => $value['Tanggal'],
							'kodebarang' => $value['Kode'],
							'namabarang' => $value['Nama'],
							'qty' => $this->ClearChr($value['Qty']),
							'qtysistem' => $this->ClearChr($value['QtySistem']),
							'selisih' => $selisih,
							'keterangan' => $value['Keterangan'],
							'tglexpired' => $value['Expired'],
							'tglsimpan' => date('Y-m-d H:m:s'),
							'pemakai' => $pemakai
						];
						
						$this->Stockrecount_model->SaveDataMinus($data);
					} else if ($this->ClearChr($value['Qty']) >  $this->ClearChr($value['QtySistem'])) {
						$selisih = $this->ClearChr($value['Qty']) - $this->ClearChr($value['QtySistem']);
						$data = [
							'nomor' => $nomor,
							'tanggal' => $this->input->post('tanggal'),
							'nomorsto' => $value['Nomor'],
							'tglsto' => $value['Tanggal'],
							'kodebarang' => $value['Kode'],
							'namabarang' => $value['Nama'],
							'qty' => $this->ClearChr($value['Qty']),
							'qtysistem' => $this->ClearChr($value['QtySistem']),
							'selisih' => $selisih,
							'keterangan' => $value['Keterangan'],
							'tglexpired' => $value['Expired'],
							'tglsimpan' => date('Y-m-d H:m:s'),
							'pemakai' => $pemakai
						];
						
						$this->Stockrecount_model->SaveDataMinus($data);
					} else {
						$selisih = $this->ClearChr($value['Qty']) - $this->ClearChr($value['QtySistem']);
						$data = [
							'nomor' => $nomor,
							'tanggal' => $this->input->post('tanggal'),
							'nomorsto' => $value['Nomor'],
							'tglsto' => $value['Tanggal'],
							'kodebarang' => $value['Kode'],
							'namabarang' => $value['Nama'],
							'qty' => $this->ClearChr($value['Qty']),
							'qtysistem' => $this->ClearChr($value['QtySistem']),
							'selisih' => $selisih,
							'keterangan' => $value['Keterangan'],
							'tglexpired' => $value['Expired'],
							'tglsimpan' => date('Y-m-d H:m:s'),
							'pemakai' => $pemakai
						];
						
						$this->Stockrecount_model->SaveDataMinus($data);
					}


					$data = [
						'norecount' => $nomor,
						'status_proses' => true,
					];
					$this->Stockrecount_model->UpdateStatusProses($data, $value['Nomor']);
				}
			};

			$data = array(
				'nomor' => $nomor,
				'tanggal' => $this->input->post('tanggal'),
				'keterangan' => $this->input->post('keterangan'),
				'tglsimpan' => date('Y-m-d H:m:s'),
				'kodegudang' => $this->input->post('kodegudang'),
				'pemakai' => $pemakai,
				'kodecabang' => $kodecabang
			);

			$this->Stockrecount_model->SaveData($data);

			$data = [
				'nomor' => $nomor,
				'noreferensi' => $value['Nomor']
			];
			$this->Stockrecount_model->SaveDataDetail2($data);

			$this->db->trans_complete();
			if ($this->db->trans_status() == FALSE) {
				$resultjson = array(
					'nomor' => "",
					'message' => "Data gagal disimpan, Data Sudah digunakan"
				);
				$this->db->trans_rollback();
			} else {
				$resultjson = array(
					'nomor' => $nomor,
					'message' => "Data berhasil disimpan"
				);
			}
		} else {
			$this->db->trans_complete();
			$resultjson = array(
				'nomor' => "",
				'message' => "Data gagal disimpan, Kode '<b style='color: red'>" . $nomor . "</b>' dengan dokumen '<b style='color: red'>" . $nomor . "</b>' sudah pernah digunakan."
			);
			$this->db->trans_rollback();
		}
		echo json_encode($resultjson);
	}

	// Recount function
	function Recount()
	{
		$nomor = $this->input->post('nomor');
		$kodecabang =  $this->session->userdata('mycabang');
		$nomorso = $this->input->post('nomorsto');
		$pemakai = $this->session->userdata('myusername');


		$this->db->trans_start();
		$this->db->trans_strict(FALSE);

		$ambilnomor = "T" . $kodecabang . "-" . date("Y") . "-" . date("m");
		$nomorsto = "";

		$data = [
			'status_recount' => true,
		];
		$this->Stockrecount_model->UpdateData($data, $nomor);

		$this->db->trans_complete();
		if ($this->db->trans_status() == FALSE) {
			$resultjson = array(
				'nomor' => "",
				'message' => "Data gagal, Data sudah digunakan"
			);
			$this->db->trans_rollback();
		} else {
			$resultjson = array(
				'nomor' => $nomor,
				'message' => "Data berhasil disimpan"
			);
			$this->db->trans_commit();
		}
		echo json_encode($resultjson);
	}

	function Confirm()
	{
		$nomor = $this->input->post('nomor');
		$kodecabang =  $this->session->userdata('mycabang');
		$pemakai =  $this->session->userdata('myusername');

		$this->db->trans_start();
		$this->db->trans_strict(false);

		$data = [
			'status' => true,
			'tglproses' => date('Y-m-d H:m:s'),
			'userapprove' => $pemakai
		];
		$this->Stockrecount_model->ApproveData($data, $nomor);

		// $data = [
		// 	'nodokumen' => $nomor,
		// 	'jenisdokumen' => "STO",
		// 	'tglsimpan' => date('Y-m-d H:m:s'),
		// 	'pemakai' => $pemakai
		// ];
		// $this->Stockrecount_model->SaveDataHist($data);

		$this->db->trans_complete();
		if ($this->db->trans_status() == FALSE) {
			$resultjson = [
				'nomor' => "",
				'message' => "Data gagal di approve"
			];
			$this->db->trans_rollback();
		} else {
			$resultjson = [
				'nomor' => $nomor,
				'message' => "Data berhasil di Confirm"
			];
			$this->db->trans_commit();
		}
		echo json_encode($resultjson);
	}

	function Denda()
	{
		$pemakai =  $this->session->userdata('myusername');
		$nomor = $this->input->post('nomor');
		// print_r($nomor);
		// die();



		$this->db->trans_start();
		$this->db->trans_strict(FALSE);

		$data = [
			'status' => true,
			'tglproses' => date('Y-m-d H:m:s'),
			'userapprove' => $pemakai
		];
		$this->Stockrecount_model->ApproveData($data, $nomor);

		$this->db->trans_complete();
		if ($this->db->trans_status() == FALSE) {
			$resultjson = array(
				'nomor' => "",
				'message' => "Data gagal Approve."
			);
			$this->db->trans_rollback();
		} else {
			$resultjson = array(
				'nomor' => $nomor,
				'message' => "Data berhasil diapprove"
			);
			$this->db->trans_commit();
		}
		echo json_encode($resultjson);
	}

	function Adjust()
	{
		$pemakai =  $this->session->userdata('myusername');

		$nomor = $this->input->post('nomor');
		// print_r($nomor);
		// 	die();
		$kodecabang =  $this->session->userdata('mycabang');
		$nomorba = "";

		$qty = $this->input->post('qty');
		$qtysistem = $this->input->post('qtysistem');

		$dataselisih = $this->Stockrecount_model->DataStockRecountSelisih($nomor);

		$this->db->trans_start();
		$this->db->trans_strict(FALSE);


		if (!empty($this->input->post('datamin'))) {
			foreach ($this->input->post('datamin') as $key => $values) {

				$ambilnomor = "U" . $kodecabang . "-" . date("Y") . "-" . date("m");
				$get["ba"] = $this->Inventorybag_model->GetMaxNomor($ambilnomor);

				if (!$get["ba"]) {
					$nomorba = $ambilnomor . "-" . "0001";
				} else {
					$lastNomor = $get['ba']->nomor;
					$lastNoUrut = substr($lastNomor, 11, 11);

					$nextNoUrut = $lastNoUrut + 1;
					$nomorba = $ambilnomor . "-" . sprintf('%05s', $nextNoUrut);
				}

				$data = [
					'nomor' => $nomorba,
					'tanggal' => date('Y-m-d H:m:s'),
					'jenis' => 1,
					'keterangan' => $this->input->post('keterangan'),
					'tglapprove' => date('Y-m-d H:m:s'),
					'userapprove' => $pemakai,
					'approve' => true,
					'pemakai' => $pemakai,
					'tglsimpan' => date('Y-m-d H:m:s'),
				];
				$this->Inventorybag_model->SaveData($data);

				$data = [
					'nomor' => $nomorba,
					'kodebarang' => $values['Kode'],
					'namabarang' => $values['Nama'],
					'qty' => $this->ClearChr($values['Selisih']),
					'tglsimpan' => date('Y-m-d H:m:s'),
					'pemakai' => $pemakai
				];

				$this->Inventorybag_model->SaveBagDetail($data);

				$data = [
					'nodokumen' => $nomorba,
					'jenisdokumen' => "BAG",
					'tglsimpan' => date('Y-m-d H:m:s'),
					'pemakai' => $pemakai
				];
				$this->Inventorybag_model->SaveHistoriApprove($data);
			}
		} else {
			foreach ($this->input->post('datapass') as $key => $values) {
				$ambilnomor = "U" . $kodecabang . "-" . date("Y") . "-" . date("m");
				$get["ba"] = $this->Inventorybag_model->GetMaxNomor($ambilnomor);

				if (!$get["ba"]) {
					$nomorba = $ambilnomor . "-" . "00001";
				} else {
					$lastNomor = $get['ba']->nomor;
					$lastNoUrut = substr($lastNomor, 11, 11);

					$nextNoUrut = $lastNoUrut + 1;
					$nomorbag = $ambilnomor . "-" . sprintf('%05s', $nextNoUrut);
				}

				$data = [
					'nomor' => $nomorbag,
					'tanggal' => date('Y-m-d H:m:s'),
					'jenis' => 2,
					'keterangan' => $this->input->post('keterangan'),
					'tglapprove' => date('Y-m-d H:m:s'),
					'userapprove' => $pemakai,
					'approve' => true,
					'pemakai' => $pemakai,
					'tglsimpan' => date('Y-m-d H:m:s'),
				];
				$this->Inventorybag_model->SaveData($data);

				$data = [
					'nomor' => $nomorbag,
					'kodebarang' => $values['Kode'],
					'namabarang' => $values['Nama'],
					'qty' => $this->ClearChr($values['Selisih']),
					'tglsimpan' => date('Y-m-d H:m:s'),
					'pemakai' => $pemakai
				];

				$this->Inventorybag_model->SaveBagDetail($data);

				$data = [
					'nodokumen' => $nomorbag,
					'jenisdokumen' => "BAG",
					'tglsimpan' => date('Y-m-d H:m:s'),
					'pemakai' => $pemakai
				];
				$this->Inventorybag_model->SaveHistoriApprove($data);
			}
		}

		if (!empty($this->input->post('datapass'))) {
			foreach ($this->input->post('datapass') as $key => $values) {
				$ambilnomor = "U" . $kodecabang . "-" . date("Y") . "-" . date("m");
				$get["ba"] = $this->Inventorybag_model->GetMaxNomor($ambilnomor);

				if (!$get["ba"]) {
					$nomorba = $ambilnomor . "-" . "00001";
				} else {
					$lastNomor = $get['ba']->nomor;
					$lastNoUrut = substr($lastNomor, 11, 11);

					$nextNoUrut = $lastNoUrut + 1;
					$nomorbag = $ambilnomor . "-" . sprintf('%05s', $nextNoUrut);
				}

				$data = [
					'nomor' => $nomorbag,
					'tanggal' => date('Y-m-d H:m:s'),
					'jenis' => 2,
					'keterangan' => $this->input->post('keterangan'),
					'tglapprove' => date('Y-m-d H:m:s'),
					'userapprove' => $pemakai,
					'approve' => true,
					'pemakai' => $pemakai,
					'tglsimpan' => date('Y-m-d H:m:s'),
				];
				$this->Inventorybag_model->SaveData($data);

				$data = [
					'nomor' => $nomorbag,
					'kodebarang' => $values['Kode'],
					'namabarang' => $values['Nama'],
					'qty' => $this->ClearChr($values['Selisih']),
					'tglsimpan' => date('Y-m-d H:m:s'),
					'pemakai' => $pemakai
				];

				$this->Inventorybag_model->SaveBagDetail($data);

				$data = [
					'nodokumen' => $nomorbag,
					'jenisdokumen' => "BAG",
					'tglsimpan' => date('Y-m-d H:m:s'),
					'pemakai' => $pemakai
				];
				$this->Inventorybag_model->SaveHistoriApprove($data);
			}
		} else {
			foreach ($this->input->post('datamin') as $key => $values) {

				$ambilnomor = "U" . $kodecabang . "-" . date("Y") . "-" . date("m");
				$get["ba"] = $this->Inventorybag_model->GetMaxNomor($ambilnomor);

				if (!$get["ba"]) {
					$nomorba = $ambilnomor . "-" . "0001";
				} else {
					$lastNomor = $get['ba']->nomor;
					$lastNoUrut = substr($lastNomor, 11, 11);

					$nextNoUrut = $lastNoUrut + 1;
					$nomorba = $ambilnomor . "-" . sprintf('%05s', $nextNoUrut);
				}

				$data = [
					'nomor' => $nomorba,
					'tanggal' => date('Y-m-d H:m:s'),
					'jenis' => 1,
					'keterangan' => $this->input->post('keterangan'),
					'tglapprove' => date('Y-m-d H:m:s'),
					'userapprove' => $pemakai,
					'approve' => true,
					'pemakai' => $pemakai,
					'tglsimpan' => date('Y-m-d H:m:s'),
				];
				$this->Inventorybag_model->SaveData($data);

				$data = [
					'nomor' => $nomorba,
					'kodebarang' => $values['Kode'],
					'namabarang' => $values['Nama'],
					'qty' => $this->ClearChr($values['Selisih']),
					'tglsimpan' => date('Y-m-d H:m:s'),
					'pemakai' => $pemakai
				];

				$this->Inventorybag_model->SaveBagDetail($data);

				$data = [
					'nodokumen' => $nomorba,
					'jenisdokumen' => "BAG",
					'tglsimpan' => date('Y-m-d H:m:s'),
					'pemakai' => $pemakai
				];
				$this->Inventorybag_model->SaveHistoriApprove($data);
			}
		}

		foreach ($this->input->post('dataselisih') as $key => $values) {
			$data = [
				'status' => true,
				'tglrecount' => date('Y-m-d H:m:s')
			];
			$this->Stockrecount_model->UpdateStatusProses($data, $values['Nomor']);
		}




		$data = [
			'status_management' => true,
			'tglapprove' => date('Y-m-d H:m:s'),
			'management_approve' => $pemakai,
			'jenissistem' => $this->input->post('jenissistem'),
			'keteranganbag' => $this->input->post('keterangan')
		];
		// print_r($data);
		// die();
		$this->Stockrecount_model->ApproveData($data, $nomor);

		$data = [
			'nodokumen' => $nomor,
			'jenisdokumen' => "STO",
			'tglsimpan' => date('Y-m-d H:m:s'),
			'pemakai' => $pemakai
		];
		// print_r($data);
		// die();
		$this->Stockrecount_model->SaveDataHist($data);

		$this->db->trans_complete();
		if ($this->db->trans_status() == FALSE) {
			$resultjson = [
				'nomor' => "",
				'message' => "Data gagal Approve"
			];
			$this->db->trans_rollback();
		} else {
			$resultjson = [
				'nomor' => $nomor,
				'message' => "Data berhasil diapprove"
			];
			$this->db->trans_commit();
		}
		echo json_encode($resultjson);
	}

	function Update()
	{
		$kodecabang =  $this->session->userdata('mycabang');
		$pemakai =  $this->session->userdata('myusername');
		$nomor = $this->input->post('nomor');

		$this->db->trans_start();
		$this->db->trans_strict(FALSE);
		if (!empty($this->input->post('datadetail'))) {
			$this->Stockrecount_model->DeleteDetail($nomor);
			$this->Stockrecount_model->DeleteDetail2($nomor);
			foreach ($this->input->post('datadetail') as $key => $value) {
				$data = [
					'nomor' => $nomor,
					'nomorsto' => $value['Nomor'],
					'tglsto' => $value['Tanggal'],
					'kodebarang' => $value['Kode'],
					'namabarang' => $value['Nama'],
					'qty' => $value['Qty'],
					'qtysistem' => $value['QtySistem'],
					'tglexpired' => $value['Expired'],
					'keterangan' => $value['Keterangan'],
					'tglsimpan' => date('Y-m-d H:m:s'),
					'pemakai' => $pemakai
				];
				$this->Stockrecount_model->SaveDataDetail($data);
			}
		}
		$data = [
			'nomor' => $nomor,
			'tanggal' => $this->input->post('tanggal'),
			'keterangan' => $this->input->post('keterangan'),
		];
		$this->Stockrecount_model->UpdateData($data, $nomor);

		$this->db->trans_complete();

		if ($this->db->trans_status() == FALSE) {
			$resultjson = array(
				'nomor' => "",
				'message' => "Data gagal diperbarui, nomor '<b style='color: red'>" . $nomor . "</b>' sudah pernah digunakan."
			);
			$this->db->trans_rollback();
		} else {
			$resultjson = array(
				'nomor' => $nomor,
				'message' => "Data berhasil di perbarui."
			);
			$this->db->trans_commit();
		}
		echo json_encode($resultjson);
	}
}
