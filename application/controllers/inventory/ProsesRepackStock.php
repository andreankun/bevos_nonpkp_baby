<?php

defined('BASEPATH') or exit('No direct script access allowed');

class ProsesRepackStock extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->load->model('inventory/ProsesRepackStock_model');
        $this->load->model('global/global_model');
        $this->load->model('Caridataaktif_model');
        $this->load->library('form_validation');
        $this->load->library('session');
    }

    function CariDataProses()
    {
        $fetch_data = $this->Caridataaktif_model->make_datatables($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value'));
        $data = array();
        foreach ($fetch_data as $row) {
            $sub_array = array();
            $i = 1;
            $count = count($this->input->post('field'));
            foreach ($this->input->post('field') as $key => $value) {
                if ($i <= $count) {
                    if ($i == 1) {
                        $msearch = $row->$value;
                        $sub_array[] = '<button class="btn btn-dark searchproses" data-dismiss="modal" style="margin: 0px;" data-id="' . $msearch . '"><i class="far fa-check-circle"></i></button>';
                        $sub_array[] = $row->$value;
                    } else {
                        if ($i == $count) {
                            $sub_array[] = $row->$value;
                        } else {
                            $sub_array[] = $row->$value;
                        }
                    }
                    // $sub_array[] = $row->$value;
                }
                $i++;
            }
            $data[] = $sub_array;
        }
        $output = array(
            "draw"                    =>     intval($_POST["draw"]),
            "recordsTotal"          =>      $this->Caridataaktif_model->get_all_data($this->input->post('nmtb')),
            "recordsFiltered"     =>     $this->Caridataaktif_model->get_filtered_data($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value')),
            "data"                    =>     $data
        );
        echo json_encode($output);
    }

    function DataProses()
    {
        $result = $this->ProsesRepackStock_model->DataProses($this->input->post('nomor'));
        echo json_encode($result);
    }

    function DataProsesDetail()
    {
        $result = $this->ProsesRepackStock_model->DataProsesDetail($this->input->post('nomor'));
        echo json_encode($result);
    }

    function DataHasilDetail()
    {
        $result = $this->ProsesRepackStock_model->DataHasilDetail($this->input->post('nomor'));
        echo json_encode($result);
    }

    function CariDataPenerimaan()
    {
        $fetch_data = $this->Caridataaktif_model->make_datatables($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value'));
        $data = array();
        foreach ($fetch_data as $row) {
            $sub_array = array();
            $i = 1;
            $count = count($this->input->post('field'));
            foreach ($this->input->post('field') as $key => $value) {
                if ($i <= $count) {
                    if ($i == 1) {
                        $msearch = $row->$value;
                        $sub_array[] = '<button class="btn btn-dark searchprs" data-dismiss="modal" style="margin: 0px;" data-id="' . $msearch . '"><i class="far fa-check-circle"></i></button>';
                        $sub_array[] = $row->$value;
                    } else {
                        if ($i == $count) {
                            $sub_array[] = $row->$value;
                        } else {
                            $sub_array[] = $row->$value;
                        }
                    }
                    // $sub_array[] = $row->$value;
                }
                $i++;
            }
            $data[] = $sub_array;
        }
        $output = array(
            "draw"                    =>     intval($_POST["draw"]),
            "recordsTotal"          =>      $this->Caridataaktif_model->get_all_data($this->input->post('nmtb')),
            "recordsFiltered"     =>     $this->Caridataaktif_model->get_filtered_data($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value')),
            "data"                    =>     $data
        );
        echo json_encode($output);
    }

    function DataPenerimaan()
    {
        $result = $this->ProsesRepackStock_model->DataPenerimaan($this->input->post('nomor'));
        echo json_encode($result);
    }

    function DataPenerimaanDetail()
    {
        $result = $this->ProsesRepackStock_model->DataPenerimaanDetail($this->input->post('nomor'));
        echo json_encode($result);
    }

    public function ClearChr($param)
    {
        return $result = str_replace(array('\'', '"', ',', ';', '<', '>', '_', '-', ' ', '.', '!', '?', '/', '=', '+', ']', '[', '{', '}', '@', '#', '$', '%', '^', '&', "*", '(', ')'), '', $param);
    }

    function Save()
    {
        $kodecabang = $this->session->userdata('mycabang');
        $kodegudang = $this->session->userdata('mygudang');
        $cabang = $this->input->post('cabang');
        $gudang = $this->input->post('gudang');
        $periode = date('Y') . date('m');
        $pemakai = $this->session->userdata('myusername');

        $nomor = $this->input->post('nomor');
        $ceknomor = $this->ProsesRepackStock_model->CekProses($nomor);

        $this->db->trans_start(); # Starting Transaction
        $this->db->trans_strict(FALSE);

        $ambilnomor = "E" . $kodecabang . "-" . date("Y") . "-" . date("m");

        if (empty($nomor)) {
            $get["rrs"] = $this->ProsesRepackStock_model->GetMaxNomor($ambilnomor);
            if (!$get["rrs"]) {
                $nomor = $ambilnomor . "-" . "00001";
            } else {
                $lastNomor = $get['rrs']->nomor;
                $lastNoUrut = substr($lastNomor, 11, 11);

                // nomor urut ditambah 1
                $nextNoUrut = $lastNoUrut + 1;
                $nomor = $ambilnomor . "-" . sprintf('%05s', $nextNoUrut);
            }
            if (empty($ceknomor)) {
                if (!empty($this->input->post('datadetail'))) {
                    foreach ($this->input->post('datadetail') as $key => $value) {

                        #Data Barang Produksi#
                        $datadetail = [
                            'nomor' => $nomor,
                            'kodebarang' => $value['Kode'],
                            'namabarang' => $value['Nama'],
                            'qty' => $this->ClearChr($value['Qty']),
                            'pemakai' => $pemakai,
                            'tglsimpan' => date('Y-m-d H:i:s'),
                        ];
                        $this->ProsesRepackStock_model->SaveDataDetail($datadetail);

                        // $getdataproses = $this->ProsesRepackStock_model->DataInv($periode, $value['Kode'], $kodecabang, $kodegudang);
                        // foreach ($getdataproses as $val) {
                        //     $datainv = [
                        //         'qtykeluar' => $val->qtykeluar + $this->ClearChr($value['Qty'])
                        //     ];
                        //     print_r($value['Kode']);
                        //     $this->ProsesRepackStock_model->UpdateStok($periode, $value['Kode'], $kodecabang, $kodegudang, $datainv);
                        // }
                    }
                }
                if (!empty($this->input->post('datahasildetail'))) {
                    foreach ($this->input->post('datahasildetail') as $key => $value) {
                        #Data Hasil Barang#
                        $datahasildetail = [
                            'nomor' => $nomor,
                            'kodebarang' => $value['Kode'],
                            'namabarang' => $value['Nama'],
                            'qty' => $this->ClearChr($value['Qty']),
                            'pemakai' => $pemakai,
                            'tglsimpan' => date('Y-m-d H:i:s'),
                        ];
                        // print_r($datahasildetail);
                        $this->ProsesRepackStock_model->SaveDataHasilDetail($datahasildetail);

                        $getdatainv = $this->ProsesRepackStock_model->DataInv($periode, $value['Kode'], $cabang, $gudang);
                        foreach ($getdatainv as $val) {
                            $datainv = [
                                'qtypick' => $val->qtypick + $this->ClearChr($value['Qty'])
                            ];
                            $this->ProsesRepackStock_model->UpdateStok($periode, $value['Kode'], $cabang, $gudang, $datainv);
                        }

                        $datareq = $this->ProsesRepackStock_model->DataReq($this->input->post('nopenerimaan'));
                        foreach ($datareq as $key => $valreq) {
                            $nomorreq = $valreq->nomorreq;

                            $dataupdreq = [
                                'proses' => true,
                                'tglproses' => date('Y-m-d H:i:s'),
                                'userproses' => $pemakai,
                                'status' => 2
                            ];
                            // print_r($dataupdreq);
                            $this->ProsesRepackStock_model->UpdateReq($nomorreq, $dataupdreq);
                        }
                    }
                }
                $data = [
                    'nomor' => $nomor,
                    'tanggal' => $this->input->post('tanggal'),
                    'nopenerimaan' => $this->input->post('nopenerimaan'),
                    'pemakai' => $pemakai,
                    'kodecabang' => $cabang,
                    'kodegudang' => $gudang,
                    'tglsimpan' => date('Y-m-d H:i:s')
                ];
                $this->ProsesRepackStock_model->SaveData($data);

                $dataupdprs = [
                    'proses' => true,
                    'tglproses' => date('Y-m-d H:i:s'),
                    'userproses' => $pemakai
                ];
                $this->ProsesRepackStock_model->UpdatePenerimaan($this->input->post('nopenerimaan'), $dataupdprs);
            }
        }
        // die();
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE) {
            $resultjson = array(
                'nomor' => "",
                'message' => "Data gagal disimpan, Nomor '<b style='color: red'>" . $nomor . "</b>' sudah pernah digunakan."
            );
            # Something went wrong.
            $this->db->trans_rollback();
        } else {
            $resultjson = array(
                'nomor' => $nomor,
                'message' => "Data berhasil disimpan."
            );
            # Everything is Perfect. 
            # Committing data to the database.
            $this->db->trans_commit();
        }
        echo json_encode($resultjson);
    }

    function Update()
    {
        $kodecabang = $this->session->userdata('mycabang');
        $pemakai = $this->session->userdata('myusername');
        $kodegudang = $this->session->userdata('mygudang');
        $cabang = $this->input->post('cabang');
        $gudang = $this->input->post('gudang');
        $periode = date('Y') . date('m');

        $nomor = $this->input->post('nomor');

        $this->db->trans_start(); # Starting Transaction
        $this->db->trans_strict(FALSE);

        if (!empty($this->input->post('datadetail'))) {
            $dataproses = $this->ProsesRepackStock_model->DataProsesDetail($nomor);
            foreach ($dataproses as $key => $valupd) {
                $getdataproses = $this->ProsesRepackStock_model->DataInv($periode, $valupd->kodebarang, $kodecabang, $kodegudang);
                foreach ($getdataproses as $val) {
                    $datainv = [
                        'qtykeluar' => $val->qtykeluar - $valupd->qty
                    ];

                    $this->ProsesRepackStock_model->UpdateStok($periode, $valupd->kodebarang, $kodecabang, $kodegudang, $datainv);
                }
            }
            $this->ProsesRepackStock_model->DeleteDetail($nomor);
            foreach ($this->input->post('datadetail') as $key => $value) {
                $datadetail = [
                    'nomor' => $nomor,
                    'kodebarang' => $value['Kode'],
                    'namabarang' => $value['Nama'],
                    'qty' => $this->ClearChr($value['Qty']),
                    'pemakai' => $pemakai,
                    'tglsimpan' => date('Y-m-d H:i:s'),
                ];
                $this->ProsesRepackStock_model->SaveDataDetail($datadetail);

                $getdataproses = $this->ProsesRepackStock_model->DataInv($periode, $value['Kode'], $kodecabang, $kodegudang);
                foreach ($getdataproses as $val) {
                    $datainv = [
                        'qtykeluar' => $val->qtykeluar + $this->ClearChr($value['Qty'])
                    ];

                    $this->ProsesRepackStock_model->UpdateStok($periode, $value['Kode'], $kodecabang, $kodegudang, $datainv);
                }
            }
            $data = [
                'nomor' => $nomor,
                'tanggal' => $this->input->post('tanggal'),
                'nopenerimaan' => $this->input->post('nopenerimaan'),
                'pemakai' => $pemakai,
                'tglsimpan' => date('Y-m-d H:i:s')
            ];
            $this->ProsesRepackStock_model->UpdateData($nomor, $data);
        }

        if (!empty($this->input->post('datahasildetail'))) {
            $datahasil = $this->ProsesRepackStock_model->DataHasilDetail($nomor);
            foreach ($datahasil as $key => $valupd) {
                $getdatainvupd = $this->ProsesRepackStock_model->DataInv($periode, $valupd->kodebarang, $cabang, $gudang);
                foreach ($getdatainvupd as $val) {
                    $datainvupd = [
                        'qtypick' => $val->qtypick - $valupd->qty
                    ];

                    $this->ProsesRepackStock_model->UpdateStok($periode, $valupd->kodebarang, $cabang, $gudang, $datainvupd);
                }
            }
            $this->ProsesRepackStock_model->DeleteHasilDetail($nomor);
            foreach ($this->input->post('datahasildetail') as $key => $value) {
                #Data Hasil Barang#
                $datahasildetail = [
                    'nomor' => $nomor,
                    'kodebarang' => $value['Kode'],
                    'namabarang' => $value['Nama'],
                    'qty' => $this->ClearChr($value['Qty']),
                    'pemakai' => $pemakai,
                    'tglsimpan' => date('Y-m-d H:i:s'),
                ];
                $this->ProsesRepackStock_model->SaveDataHasilDetail($datahasildetail);

                $getdatainv = $this->ProsesRepackStock_model->DataInv($periode, $value['Kode'], $cabang, $gudang);
                foreach ($getdatainv as $val) {
                    $datainv = [
                        'qtypick' => $val->qtypick + $this->ClearChr($value['Qty'])
                    ];

                    $this->ProsesRepackStock_model->UpdateStok($periode, $value['Kode'], $cabang, $gudang, $datainv);
                }
            }
        }
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE) {
            $resultjson = array(
                'nomor' => "",
                'message' => "Data gagal disimpan, Nomor '<b style='color: red'>" . $nomor . "</b>' sudah pernah digunakan."
            );
            # Something went wrong.
            $this->db->trans_rollback();
        } else {
            $resultjson = array(
                'nomor' => $nomor,
                'message' => "Data berhasil disimpan."
            );
            # Everything is Perfect. 
            # Committing data to the database.
            $this->db->trans_commit();
        }
        echo json_encode($resultjson);
    }

    function Cancel()
    {

        $kodecabang =  $this->session->userdata('mycabang');
        $pemakai =  $this->session->userdata('myusername');
        $nomor = $this->input->post('nomor');
        $keterangan = $this->input->post('keterangan');
        $errorvalidasi = FALSE;
        $where = array(
            "nomor" => $nomor
        );
        $dataproses = $this->global_model->GetDataGlobal("srvt_prosesrepact", $where);
        if (empty($this->input->post('datadetail'))) {
            $resultjson = array(
                'nomor' => "",
                'message' => "Data gagal dibatalkan, Data Detail Tidak Ada."
            );
            $errorvalidasi = TRUE;
            echo json_encode($resultjson);
            return FALSE;
        }

        foreach ($dataproses as $key => $val) {;
            if ($val->kirim == 't') {
                $resultjson = array(
                    'nomor' => "",
                    'message' => "Data gagal dibatalkan, Proses Repack Barang dengan Nomor '<b style='color: red'>" . $nomor . "</b>' Sudah Melakukan Pengiriman Barang."
                );
                $errorvalidasi = TRUE;
                echo json_encode($resultjson);
                return FALSE;
            }
        }

        if ($errorvalidasi == FALSE) {
            $this->db->trans_start(); # Starting Transaction
            $this->db->trans_strict(FALSE);

            $dataheader = array(
                'batal' => true,
                'userbatal' => $pemakai,
                'alasanbatal' => $keterangan,
                'tglbatal' => date('Y-m-d H:i:s'),
                'kodecabang' => $kodecabang
            );
            $wheredata = array("nomor" => $nomor);
            $this->global_model->UpdateDataGlobal("srvt_prosesrepact", $dataheader, $wheredata);
            $this->db->trans_complete();

            if ($this->db->trans_status() === FALSE) {
                $resultjson = array(
                    'nomor' => "",
                    'message' => "Data gagal dibatalkan, Nomor '<b style='color: red'>" . $nomor . "</b>' tidak ada."
                );
                # Something went wrong.
                $this->db->trans_rollback();
            } else {
                $resultjson = array(
                    'nomor' => $nomor,
                    'message' => "Data berhasil dibatalkan."
                );
                # Everything is Perfect. 
                # Committing data to the database.
                $this->db->trans_commit();
            }

            echo json_encode($resultjson);
            return FALSE;
        }
    }
}
