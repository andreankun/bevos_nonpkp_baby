<?php
defined('BASEPATH') or exit('No direct script access allowed');
class SuratJalanGudang extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('inventory/Suratjalangudang_model');
		$this->load->model('global/global_model');
		$this->load->model('Caridataaktif_model');
		$this->load->library('form_validation');
		$this->load->library('session');
	}

	function CariDataSuratGudang()
	{

		$fetch_data = $this->Caridataaktif_model->make_datatables($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value'));
		$data = array();
		foreach ($fetch_data as $row) {
			$sub_array = array();
			$i = 1;
			$count = count($this->input->post('field'));
			foreach ($this->input->post('field') as $key => $value) {
				if ($i <= $count) {
					if ($i == 1) {
						$msearch = $row->$value;
						$where = array(
							"nomor" => $row->$value,
							"batal" => false,
							"terima" => false
						);
						$getdatastatusTombol = $this->global_model->GetDataGlobal("srvt_suratjalangudang", $where);
						if (!empty($getdatastatusTombol)) {
							foreach ($getdatastatusTombol as $key => $vals) {
								if ($vals->cetaksuratjalan == 0) {
									$sub_array[] = '<button class="btn btn-dark searchsjgudang" data-dismiss="modal" style="margin: 0px;" data-id="' . $msearch . '"><i class="far fa-check-circle"></i></button>';
									$sub_array[1] = '<button class="btn btn-sm btn-dark cetaksuratjalangudang" style="margin: 0px; background-color: #9A86A4; border-color: #9A86A4;" data-id="' . $msearch . '"><i class="fas fa-print"></i>&nbsp;Cetak Surat Jalan</button>';
								} else {
									$sub_array[] = '<button class="btn btn-dark searchsjgudang" data-dismiss="modal" style="margin: 0px;" data-id="' . $msearch . '"><i class="far fa-check-circle"></i></button>';
									$sub_array[1] = '<button class="btn btn-sm btn-dark cetaksuratjalangudang" style="margin: 0px; background-image: linear-gradient(#000,#9A86A4); border-color: #000;" data-id="' . $msearch . '"><i class="fas fa-print"></i>&nbsp;Cetak Surat Jalan</button>';
								}
							}
						} else {
							$sub_array[] = '<button class="btn btn-dark searchsjgudang" data-dismiss="modal" style="margin: 0px;" data-id="' . $msearch . '"><i class="far fa-check-circle"></i></button>';
							$sub_array[1] = '<button class="btn btn-sm btn-dark cetaksuratjalangudang" style="margin: 0px; background-color: #9A86A4; border-color: #9A86A4;" data-id="' . $msearch . '"><i class="fas fa-print"></i>&nbsp;Cetak Surat Jalan</button>';
						}
						$sub_array[] = $row->$value;
					} else {
						if ($i == $count) {
							$sub_array[] = $row->$value;
						} else {
							$sub_array[] = $row->$value;
						}
					}
					// $sub_array[] = $row->$value;
				}
				$i++;
			}
			$data[] = $sub_array;
		}
		$output = array(
			"draw"                    =>     intval($_POST["draw"]),
			"recordsTotal"          =>      $this->Caridataaktif_model->get_all_data($this->input->post('nmtb')),
			"recordsFiltered"     =>     $this->Caridataaktif_model->get_filtered_data($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value')),
			"data"                    =>     $data
		);
		echo json_encode($output);
	}

	function DataSuratJalanGudang()
	{
		$result = $this->Suratjalangudang_model->DataSuratJalanGudang($this->input->post('nomor'));
		echo json_encode($result);
	}

	function DataSuratJalanGudangDetail()
	{
		$result = $this->Suratjalangudang_model->DataSuratJalanGudangDetail($this->input->post('nomor'));
		echo json_encode($result);
	}

	function DataProses()
	{
		$result = $this->Suratjalangudang_model->DataProses($this->input->post('nomor'));
		echo json_encode($result);
	}

	function CariDataTransferStok()
	{
		$fetch_data = $this->Caridataaktif_model->make_datatables($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value'));
		$data = array();
		foreach ($fetch_data as $row) {
			$sub_array = array();
			$i = 1;
			$count = count($this->input->post('field'));
			foreach ($this->input->post('field') as $key => $value) {
				if ($i <= $count) {
					if ($i == 1) {
						$msearch = $row->$value;
						$where = array(
							"nomor" => $row->$value,
							"batal" => false,
							"status" => false
						);
						$getdatastatusTombol = $this->global_model->GetDataGlobal("srvt_transferstock", $where);
						if (!empty($getdatastatusTombol)) {
							foreach ($getdatastatusTombol as $key => $vals) {
								if ($vals->cetaktransferstok == 0) {
									$sub_array[] = '<button class="btn btn-dark searchts" data-dismiss="modal" style="margin: 0px;" data-id="' . $msearch . '"><i class="far fa-check-circle"></i></button>';
									$sub_array[1] = '<button class="btn btn-sm btn-dark cetaktransferstok" style="margin: 0px; background-color: #9A86A4; border-color: #9A86A4;" data-id="' . $msearch . '"><i class="fas fa-print"></i>&nbsp;Cetak Transfer Stok</button>';
								} else {
									$sub_array[] = '<button class="btn btn-dark searchts" data-dismiss="modal" style="margin: 0px;" data-id="' . $msearch . '"><i class="far fa-check-circle"></i></button>';
									$sub_array[1] = '<button class="btn btn-sm btn-dark cetaktransferstok" style="margin: 0px; background-image: linear-gradient(#000,#9A86A4); border-color: #000;" data-id="' . $msearch . '"><i class="fas fa-print"></i>&nbsp;Cetak Transfer Stok</button>';
								}
							}
						} else {
							$sub_array[] = '<button class="btn btn-dark searchts" data-dismiss="modal" style="margin: 0px;" data-id="' . $msearch . '"><i class="far fa-check-circle"></i></button>';
							$sub_array[1] = '<button class="btn btn-sm btn-dark cetaktransferstok" style="margin: 0px; background-color: #9A86A4; border-color: #9A86A4;" data-id="' . $msearch . '"><i class="fas fa-print"></i>&nbsp;Cetak Transfer Stok</button>';
						}
						$sub_array[] = $row->$value;
					} else {
						if ($i == $count) {
							$sub_array[] = $row->$value;
						} else {
							$sub_array[] = $row->$value;
						}
					}
					// $sub_array[] = $row->$value;
				}
				$i++;
			}
			$data[] = $sub_array;
		}
		$output = array(
			"draw"                    =>     intval($_POST["draw"]),
			"recordsTotal"          =>      $this->Caridataaktif_model->get_all_data($this->input->post('nmtb')),
			"recordsFiltered"     =>     $this->Caridataaktif_model->get_filtered_data($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value')),
			"data"                    =>     $data
		);
		echo json_encode($output);
	}

	function DataTransferStok()
	{
		$result = $this->Suratjalangudang_model->DataTransferStok($this->input->post('nomor'));
		echo json_encode($result);
	}

	function DataTransferStokDetail()
	{
		$result = $this->Suratjalangudang_model->DataTransferStokDetail($this->input->post('nomor'));
		echo json_encode($result);
	}

	function DataProsesDetail()
	{
		$result = $this->Suratjalangudang_model->DataProsesDetail($this->input->post('nomor'));
		echo json_encode($result);
	}

	public function ClearChr($param)
	{
		return $result = str_replace(array('\'', '"', ',', ';', '<', '>', '_', '-', ' ', '.', '!', '?', '/', '=', '+', ']', '[', '{', '}', '@', '#', '$', '%', '^', '&', "*", '(', ')'), '', $param);
	}

	function Save()
	{
		$nomor = $this->input->post('nomor');
		$kodecabang = $this->session->userdata('mycabang');
		$asal = $this->session->userdata('mygudang');
		$pemakai = $this->session->userdata('myusername');
		$kodegudang = $this->input->post('lokasi');
		$periode = date('Y') . date('m');

		$this->db->trans_start();
		$this->db->trans_strict(FALSE);

		$cekNomor = $this->Suratjalangudang_model->CekNoSuratJalanGudang($nomor);
		if (empty($cekNomor)) {
			$ambilnomor = "J" . $kodecabang . "-" . date("Y") . "-" . date("m");
			$get["sj"] = $this->Suratjalangudang_model->GetMaxNomor($ambilnomor);

			if (!$get["sj"]) {
				$nomor = $ambilnomor . "-" . "00001";
			} else {
				$lastNomor = $get['sj']->nomor;
				$lastNoUrut = substr($lastNomor, 11, 11);

				$nextNoUrut = $lastNoUrut + 1;
				$nomor = $ambilnomor . "-" . sprintf('%05s', $nextNoUrut);
			}
			if (!empty($this->input->post('datadetail'))) {
				foreach ($this->input->post('datadetail') as $key => $value) {
					$data = array(
						'nomor' => $nomor,
						'kodebarang' => $value['Kode'],
						'namabarang' => $value['Nama'],
						'qty' => $this->ClearChr($value['Qty']),
						'tglsimpan' => date('Y-m-d H:i:s'),
						'pemakai' => $pemakai
					);
					$this->Suratjalangudang_model->SaveSjGudangDetail($data);

					$datareqrepack = $this->Suratjalangudang_model->DataReqrepack($this->input->post('nodokumen'));
					if ($this->input->post('jenisdokumen') == '1') {
						foreach ($datareqrepack as $datareq) {
							$cabangasalr = $datareq->kodecabang;
							$gudangasalr = $datareq->kodegudang;
						}
						$datainv = $this->Suratjalangudang_model->DataInv($periode, $value['Kode'], $this->input->post('lokasi'), $this->input->post('cabang'));
						if (!empty($datainv)) {
							foreach ($datainv as $key => $val) {
								$qtypick = $val->qtypick;
								$data = array(
									'qtypick' => $qtypick + $this->ClearChr($value['Qty'])
								);

								$this->Suratjalangudang_model->UpdateStok($periode, $value['Kode'], $this->input->post('cabang'), $this->input->post('lokasi'), $data);
							}
						} else {
							// foreach ($datainv as $key => $val) {
							// print_r($kodecabang);
							$data = array(
								'periode' => $periode,
								'kodecabang' => $this->input->post('cabang'),
								'kodegudang' => $this->input->post('lokasi'),
								'kodebarang' => $value['Kode'],
								'qtyawal' => 0,
								'qtymasuk' => 0,
								'qtykeluar' => 0,
								'qtypick' => $this->ClearChr($value['Qty']),
								'cogs' => 0,
								'tglsimpan' => date('Y-m-d H:i:s'),
								'pemakai' => $pemakai
							);
							// print_r($nomor);
							$this->Suratjalangudang_model->SaveDataStok($data);
							// }
						}
					} else if ($this->input->post('jenisdokumen') == '2') {
						$dataproses = $this->Suratjalangudang_model->DataProses($this->input->post('nodokumen'));
						foreach ($dataproses as $dataproes) {
							$cabangasalpros = $dataproes->kodecabang;
							$gudangasalpros = $dataproes->kodegudang;
						}
						$datainv = $this->Suratjalangudang_model->DataInv($periode, $value['Kode'], $this->input->post('lokasi'), $this->input->post('cabang'));
						if (!empty($datainv)) {
							foreach ($datainv as $key => $val) {
								$qtypick = $val->qtypick;
								$data = array(
									'qtypick' => $qtypick + $this->ClearChr($value['Qty'])
								);

								$this->Suratjalangudang_model->UpdateStok($periode, $value['Kode'], $this->input->post('cabang'), $this->input->post('lokasi'), $data);
							}
						} else {
							// foreach ($datainv as $key => $val) {
							// print_r($kodecabang);
							$data = array(
								'periode' => $periode,
								'kodecabang' => $this->input->post('cabang'),
								'kodegudang' => $this->input->post('lokasi'),
								'kodebarang' => $value['Kode'],
								'qtyawal' => 0,
								'qtymasuk' => 0,
								'qtykeluar' => 0,
								'qtypick' => $this->ClearChr($value['Qty']),
								'cogs' => 0,
								'tglsimpan' => date('Y-m-d H:i:s'),
								'pemakai' => $pemakai
							);
							// print_r($data);
							$this->Suratjalangudang_model->SaveDataStok($data);
							// }
						}
					}
					// else {
					// 	$datatransferstok = $this->Suratjalangudang_model->DataTransferStok($this->input->post('nodokumen'));
					// 	foreach ($datatransferstok as $datatf) {
					// 		$cabangasal = $datatf->kodecbasal;
					// 		$gudangasal = $datatf->kodegdasal;
					// 		$cabangtujuan = $datatf->kodecbtujuan;
					// 		$gudangtujuan = $datatf->kodegdtujuan;
					// 	}

					// 	$datainv = $this->Suratjalangudang_model->DataInv($periode, $value['Kode'], $gudangtujuan, $cabangtujuan);
					// 	if (!empty($datainv)) {
					// 		foreach ($datainv as $key => $val) {
					// 			$qtypick = $val->qtypick;
					// 			$data = array(
					// 				'qtypick' => $qtypick + $this->ClearChr($value['Qty'])
					// 			);

					// 			$this->Suratjalangudang_model->UpdateStok($periode, $value['Kode'], $cabangtujuan, $gudangtujuan, $data);
					// 		}
					// 	} else {
					// 		// foreach ($datainv as $key => $val) {
					// 		// print_r($cabangtujuan);
					// 		$data = array(
					// 			'periode' => $periode,
					// 			'kodecabang' => $cabangtujuan,
					// 			'kodegudang' => $gudangtujuan,
					// 			'kodebarang' => $value['Kode'],
					// 			'qtyawal' => 0,
					// 			'qtymasuk' => 0,
					// 			'qtykeluar' => 0,
					// 			'qtypick' => $this->ClearChr($value['Qty']),
					// 			'cogs' => 0,
					// 			'tglsimpan' => date('Y-m-d H:i:s'),
					// 			'pemakai' => $pemakai
					// 		);
					// 		$this->Suratjalangudang_model->SaveDataStok($data);
					// 		// }
					// 	}
					// }
				}
				if ($this->input->post('jenisdokumen') == '1') {
					$data = array(
						'nomor' => $nomor,
						'tanggal' => $this->input->post('tanggal'),
						'noproses' => $this->input->post('nodokumen'),
						'keterangan' => $this->input->post('keterangan'),
						'jenisdokumen' => $this->input->post('jenisdokumen'),
						'lokasi' => $this->input->post('lokasi'),
						'asal' => $gudangasalr,
						'cabangtujuan' => $this->input->post('cabang'),
						'tglsimpan' => date('Y-m-d H:i:s'),
						'tglpengiriman' => $this->input->post('tglpengiriman'),
						'pemakai' => $pemakai,
						'kodecabang' => $kodecabang,
					);
					// print_r($data);
					$this->Suratjalangudang_model->SaveData($data);

					$datareq = [
						'status' => 1
					];
					$this->Suratjalangudang_model->UpdateReq($datareq, $this->input->post('nodokumen'));
				} else if ($this->input->post('jenisdokumen') == '2') {
					$data = array(
						'nomor' => $nomor,
						'tanggal' => $this->input->post('tanggal'),
						'noproses' => $this->input->post('nodokumen'),
						'keterangan' => $this->input->post('keterangan'),
						'jenisdokumen' => $this->input->post('jenisdokumen'),
						'lokasi' => $this->input->post('lokasi'),
						'asal' => $gudangasalpros,
						'cabangtujuan' => $this->input->post('cabang'),
						'tglsimpan' => date('Y-m-d H:i:s'),
						'tglpengiriman' => $this->input->post('tglpengiriman'),
						'pemakai' => $pemakai,
						'kodecabang' => $kodecabang,
					);
					// print_r($data);
					$this->Suratjalangudang_model->SaveData($data);

					$datapros = [
						'kirim' => true,
						'tglkirim' => date('Y-m-d H:i:s'),
						'userkirim' => $pemakai
					];
					$this->Suratjalangudang_model->UpdatePros($datapros, $this->input->post('nodokumen'));
				} else {
					$datatransferstok = $this->Suratjalangudang_model->DataTransferStok($this->input->post('nodokumen'));
					foreach ($datatransferstok as $datatf) {
						$cabangasal = $datatf->kodecbasal;
						$gudangasal = $datatf->kodegdasal;
						$cabangtujuan = $datatf->kodecbtujuan;
						$gudangtujuan = $datatf->kodegdtujuan;
					}

					$data = array(
						'nomor' => $nomor,
						'tanggal' => $this->input->post('tanggal'),
						'noproses' => $this->input->post('nodokumen'),
						'keterangan' => $this->input->post('keterangan'),
						'jenisdokumen' => $this->input->post('jenisdokumen'),
						'lokasi' => $this->input->post('lokasi'),
						'asal' => $gudangasal,
						'cabangtujuan' => $this->input->post('cabang'),
						'tglsimpan' => date('Y-m-d H:i:s'),
						'tglpengiriman' => $this->input->post('tglpengiriman'),
						'pemakai' => $pemakai,
						'kodecabang' => $kodecabang,
						'cetaksuratjalan' => 1
					);
					// print_r($data);
					$this->Suratjalangudang_model->SaveData($data);

					$datatf = array(
						'kodestatus' => 1
					);
					$whereupdate = array(
						'nomor' => $this->input->post('nodokumen')
					);
					$this->global_model->UpdateDataGlobal("srvt_transferstock", $datatf, $whereupdate);
				}
			}
			// die();
			$this->db->trans_complete();
			if ($this->db->trans_status() == FALSE) {
				$resultjson = array(
					'nomor' => "",
					'message' => "Data gagal disimpan, Data Sudah digunakan"
				);
				$this->db->trans_rollback();
			} else {
				$resultjson = array(
					'nomor' => $nomor,
					'message' => "Data berhasil disimpan"
				);
			}
		} else {
			$this->db->trans_complete();
			$resultjson = array(
				'nomor' => "",
				'message' => "Data gagal disimpan, Kode '<b style='color: red'>" . $nomor . "</b>' dengan dokumen '<b style='color: red'>" . $nomor . "</b>' sudah pernah digunakan."
			);
			$this->db->trans_rollback();
		}
		echo json_encode($resultjson);
	}


	function Update()
	{
		$kodecabang = $this->session->userdata('mycabang');
		$asal = $this->session->userdata('mygudang');
		$pemakai = $this->session->userdata('myusername');
		$nomor = $this->input->post('nomor');

		$this->db->trans_start();
		$this->db->trans_strict(FALSE);

		if (!empty($this->input->post('datadetail'))) {
			$this->Suratjalangudang_model->DeleteDetail($nomor);
			foreach ($this->input->post('datadetail') as $key => $value) {
				$datadetail = [
					'nomor' => $nomor,
					'kodebarang' => $value['Kode'],
					'namabarang' => $value['Nama'],
					'qty' => $this->ClearChr($value['Qty']),
					'tglsimpan' => date('Y-m-d H:i:s'),
					'pemakai' => $pemakai
				];
				// print_r($datadetail);
				// die();
				$this->Suratjalangudang_model->SaveSjGudangDetail($datadetail);
			}
		}

		$datasj = $this->Suratjalangudang_model->DataSuratJalanGudang($nomor);
		foreach ($datasj as $sj) {
			$gudang = $sj->kodegudang;
		}
		$data = [
			'nomor' => $nomor,
			'tanggal' => $this->input->post('tanggal'),
			'noproses' => $this->input->post('nodokumen'),
			'keterangan' => $this->input->post('keterangan'),
			'jenisdokumen' => $this->input->post('jenisdokumen'),
			'lokasi' => $this->input->post('lokasi'),
			'asal' => $gudang,
			'cabangtujuan' => $this->input->post('cabang'),
			'pemakai' => $pemakai,
			'kodecabang' => $kodecabang,
			'tglsimpan' => date('Y-m-d H:i:s')
		];
		$this->Suratjalangudang_model->UpdateData($data, $nomor);

		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE) {
			$resultjson = array(
				'nomor' => "",
				'message' => "Data gagal diperbarui, Nomor '<b style='color: red'>" . $nomor . "</b>' sudah pernah digunakan."
			);
			$this->db->trans_rollback();
		} else {
			$resultjson = array(
				'nomor' => $nomor,
				'message' => "Data berhasil di perbarui"
			);
			$this->db->trans_commit();
		}
		echo json_encode($resultjson);
	}

	function Cancel()
	{

		$kodecabang =  $this->session->userdata('mycabang');
		$pemakai =  $this->session->userdata('myusername');
		$nomor = $this->input->post('nomor');
		$keterangan = $this->input->post('keterangan');
		$errorvalidasi = FALSE;
		$periode = date('Y') . date('m');
		$where = array(
			"nomor" => $this->input->post('nodokumen')
		);

		$datatf = $this->global_model->GetDataGlobal("srvt_transferstock", $where);
		// print_r($datatf);


		// if (!empty($this->input->post('datadetail'))) {
		// 	foreach ($this->input->post('datadetail') as $key => $value) {
		// 		
		// 		$where = array(
		// 			"periode" => $periode,
		// 			"kodecabang" => $cabang,
		// 			"kodegudang" => $kodegudang,
		// 			"kodebarang" => $value['Kode']
		// 		);

		if ($this->input->post('jenisdokumen') == '3') {
			$wheresj = array(
				"notransfer" => $nomor
			);
			$datasj = $this->global_model->GetDataGlobal("srvt_penerimaantransferstock", $wheresj);
			foreach ($datasj as $key => $valdatasj) {
				// print_r($valdatasj->nomor);
				if ($valdatasj->batal == 'f') {
					$resultjson = array(
						'nomor' => "",
						'message' => "Data gagal dibatalkan, Batalkan Terlebih Dahulu Barang dengan Nomor Penerimaan '<b style='color: red'>" . $valdatasj->nomor . "</b>'."
					);
					$errorvalidasi = TRUE;
					echo json_encode($resultjson);
					return FALSE;
				}
			}

			$whrtf = array(
				"nomor" => $this->input->post('nodokumen')
			);
			$datatransfer = $this->global_model->GetDataGlobal("srvt_transferstock", $whrtf);
			foreach ($datatransfer as $rowtf) {
				$datatfcancel = array(
					'kodestatus' => 0
				);
				$wherecancel = array("nomor" => $rowtf->nomor);
				$this->global_model->UpdateDataGlobal("srvt_transferstock", $datatfcancel, $wherecancel);
			}
		}

		// 	}
		// } 
		if (empty($this->input->post('datadetail'))) {
			$resultjson = array(
				'nomor' => "",
				'message' => "Data gagal dibatalkan, Data Detail Tidak Ada."
			);
			$errorvalidasi = TRUE;
			echo json_encode($resultjson);
			return FALSE;
		}
		foreach ($datatf as $key => $valdatatf) {
			$cabang = $valdatatf->kodecbasal;
			$kodegudang = $valdatatf->kodegdasal;
			// print_r($kodegudang);
			if ($cabang == "") {
				$resultjson = array(
					'nomor' => "",
					'message' => "Data gagal dibatalkan, Cabang Kosong."
				);
				$errorvalidasi = TRUE;
				echo json_encode($resultjson);
				return FALSE;
			}
			if ($kodegudang == "") {
				$resultjson = array(
					'nomor' => "",
					'message' => "Data gagal dibatalkan, Gudang Kosong."
				);
				$errorvalidasi = TRUE;
				echo json_encode($resultjson);
				return FALSE;
			}
		}

		if ($errorvalidasi == FALSE) {
			$this->db->trans_start(); # Starting Transaction
			$this->db->trans_strict(FALSE);

			// if ($this->input->post('jenisdokumen') == '3') {
			// 	if (!empty($this->input->post('datadetail'))) {
			// 		foreach ($this->input->post('datadetail') as $key => $value) {
			// 			// foreach ($datatf as $key => $valdatatf) {
			// 			// 	$kodecbasal = $valdatatf->kodecbasal;
			// 			// 	$kodegdasal = $valdatatf->kodegdasal;
			// 			// }

			// 			$getdataasal = $this->Suratjalangudang_model->DataInv($periode, $value['Kode'], $kodegudang, $cabang);
			// 			foreach ($getdataasal as $key => $valasal) {
			// 				$dataasal = [
			// 					'qtypick' => $valasal->qtypick - $this->ClearChr($value['Qty']),
			// 					'qtymasuk' => $valasal->qtymasuk + $this->ClearChr($value['Qty'])
			// 				];
			// 				// print_r($dataasal);
			// 				$this->Suratjalangudang_model->UpdateStok($periode, $value['Kode'], $cabang, $kodegudang, $dataasal);
			// 			}
			// 		}
			// 	}
			// }

			$dataheader = array(
				'batal' => true,
				'userbatal' => $pemakai,
				'alasanbatal' => $keterangan,
				'tglbatal' => date('Y-m-d H:i:s')
			);
			$wheredata = array("nomor" => $nomor);
			$this->global_model->UpdateDataGlobal("srvt_suratjalangudang", $dataheader, $wheredata);
			// die();
			$this->db->trans_complete();

			if ($this->db->trans_status() === FALSE) {
				$resultjson = array(
					'nomor' => "",
					'message' => "Data gagal dibatalkan, Nomor '<b style='color: red'>" . $nomor . "</b>' tidak ada."
				);
				# Something went wrong.
				$this->db->trans_rollback();
			} else {
				$resultjson = array(
					'nomor' => $nomor,
					'message' => "Data berhasil dibatalkan."
				);
				# Everything is Perfect. 
				# Committing data to the database.
				$this->db->trans_commit();
			}

			echo json_encode($resultjson);
			return FALSE;
		}
	}

	function LoadGudang()
	{
		$result = $this->global_model->GetDataGlobal("glbm_gudang", array());
		echo json_encode($result);
	}
	function LoadCabang()
	{
		$result = $this->global_model->GetDataGlobal("glbm_cabang", array());
		echo json_encode($result);
	}

	function CetakSuratJalanGudangUpdate()
	{
		$kodecabang = $this->session->userdata('mycabang');
		$pemakai = $this->session->userdata('myusername');

		$nomor = $this->input->post('nomor');

		$this->db->trans_start(); # Starting Transaction
		$this->db->trans_strict(FALSE);

		$datasj = $this->Suratjalangudang_model->CekNoSuratJalanGudang($nomor);

		foreach ($datasj as $ambildata) {
			$cetaksuratjalan = $ambildata->cetaksuratjalan;
			$data = [
				'cetaksuratjalan' => $cetaksuratjalan + 1
			];
			$this->Suratjalangudang_model->UpdateData($data, $nomor);
		}


		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE) {
			$resultjson = array(
				'nomor' => "",
				'message' => "Data gagal di update, Nomor '<b style='color: red'>" . $nomor . "</b>' sudah pernah digunakan."
			);
			# Something went wrong.
			$this->db->trans_rollback();
		} else {
			$resultjson = array(
				'nomor' => $nomor,
				'message' => "Data berhasil di update."
			);
			# Everything is Perfect. 
			# Committing data to the database.
			$this->db->trans_commit();
		}
		echo json_encode($resultjson);
	}
}
