<?php
defined('BASEPATH') or exit('No direct script access allowed');

class BarangRusak extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('caridataaktif_model');
		$this->load->library('form_validation');
		$this->load->library('session');
	}

	// public function Print($nomor = "")
	// {
	// 	$paper = 'A4';
	// 	// $data = 'asd';
	// 	$data['a'] = $this->db->query("SELECT * FROM srvt_invoice WHERE nomor = '" . $nomor . "'")->row();

	// 	$data['invoiced'] = $this->db->query("SELECT invoice.*, invoiced.*, brg.kodesatuan FROM srvt_invoicedetail invoiced LEFT JOIN srvt_invoice invoice on invoice.nomor = invoiced.nomor LEFT JOIN glbm_barang brg on brg.kode = invoiced.kodebarang WHERE invoiced.nomor = '" . $nomor . "'")->result();
	// 	// print_r($data);
	// 	// die();

	// 	$orientation = 'portrait';
	// 	// $filename = 'Surat Jalan - ' . $data['sj']->nomor;
	// 	$filename = 'Invoice -';

	// 	$this->load->library('pdf');
	// 	$this->pdf->load_view('menu/cetakan_pdf/invoice', $data, $filename, $paper, $orientation);

	// 	$this->pdf->setPaper('F4', 'landscape');
	// 	$this->pdf->rander();
	// 	$this->pdf->stream("welcome.pdf", array("Attachment" => 0));

	// 	exit(0);
	// }

	public function Print($nomor = "")
	{
		// $nomor = "UB-2022-02-00001";
		$paper = array(0, 0, 612, 396);
		$data['bag'] = $this->db->query("SELECT * FROM srvt_bag WHERE nomor = '" . $nomor . "'")->row();

		// $gudang = "SD";		

		$data['bagd'] = $this->db->query("SELECT 
		bad.nomor,
		bad.kodebarang,
		bad.namabarang,
		bad.qty,
		b.kodesatuan
		FROM srvt_bagdetail bad
		LEFT JOIN glbm_barang b on b.kode = bad.kodebarang
		WHERE bad.nomor = '" . $nomor . "'
		ORDER BY bad.namabarang ASC")->result();

		// $data['vbag'] = $this->db->query(" SELECT
		// sj.nomor AS nomorsj,
		// so.nomor AS nomorso,
		// inv.nomor AS noinvoice,
		// so.kodesalesman,
		// so.lokasi,
		// c.nama AS namacustomer,
		// c.pengiriman,
		// c.kodepos as kode_pos,
		// pos.kodepos,
		// c.nohp,
		// c.notelp,
		// pos.kota
		// FROM srvt_suratjalan sj
		// LEFT JOIN srvt_invoice inv ON inv.nomor = sj.noinvoice
		// LEFT JOIN srvt_salesorder so ON so.nomor = inv.nomorso
		// LEFT JOIN glbm_customer c ON c.nomor = so.nopelanggan
		// LEFT JOIN glbm_kodepos pos ON pos.kode = c.kodepos
		// WHERE sj.nomor = '" . $nomor . "'")->result();

		// $data['vsjd'] = $this->db->query("SELECT
		// sj.nomor AS nomorsj,
		// sjd.namabarang,
		// sjd.qty,
		// brg.kodesatuan
		// FROM srvt_suratjalandetail sjd
		// LEFT JOIN srvt_suratjalan sj ON sj.nomor = sjd.nomor
		// LEFT JOIN glbm_barang brg ON brg.kode = sjd.kodebarang
		// WHERE sj.nomor = '" . $nomor . "'")->result();

		$orientation = 'portrait';
		$filename = 'Barang Rusak - ' . $data['bag']->nomor;

		$this->load->library('pdf');
		$this->pdf->load_view('menu/cetakan_pdf/barang_rusak', $data, $filename, $paper, $orientation);

		$this->pdf->setPaper($paper, 'portrait');
		$this->pdf->rander();
		$this->pdf->stream("welcome.pdf", array("Attachment" => 0));

		exit(0);
	}
}
