<?php
defined('BASEPATH') or exit('No direct script access allowed');

class SuratJalanOngkir extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('caridataaktif_model');
		$this->load->library('form_validation');
		$this->load->library('session');
	}

	public function Print($nomor = "")
	{
		$paper = array(0, 0, 612, 396);
		$data['sj'] = $this->db->query("SELECT * FROM srvt_suratjalan WHERE nomor = '" . $nomor . "'")->row();
		$data['konfigurasi'] = $this->db->query("SELECT* FROM stpm_konfigurasi")->row();

		$data['vsj'] = $this->db->query("SELECT
		sj.nomor AS nomorsj,
		inv.nomor AS noinvoice,
		c.nama AS namacustomer,
		c.nama_toko,
		c.alamat,
		c.pengiriman,
		c.kodepos as kode_pos,
		pos.kodepos,
		c.nohp,
		c.notelp,
		c.rekeningtf,
		pos.kota,
		hist.revisike,
		inv.tanggalongkir as tanggal,
		inv.kodecabang,
		a.namabisnis
	FROM
		srvt_suratjalan sj
		LEFT JOIN srvt_ongkir inv ON inv.nomor = sj.noinvoice
		LEFT JOIN hist_cancel hist ON hist.noinvoice = inv.nomor
		LEFT JOIN glbm_customer c ON c.nomor = inv.nopelanggan
		LEFT JOIN glbm_kodepos pos ON pos.kodepos = c.kodepos
		left join glbm_account a on a.nomor = c.rekeningtf
	WHERE sj.nomor = '" . $nomor . "'
		LIMIT 1")->row();

		$data['vsjd'] = $this->db->query("SELECT
		sjd.nomor AS nomorsj,
		s.keterangan
	FROM
		srvt_suratjalan sjd
		LEFT JOIN srvt_ongkir s ON s.nomor = sjd.noinvoice
	WHERE
		sjd.nomor ='" . $nomor . "'")->result();

		$orientation = 'portrait';
		$filename = 'Surat Jalan - ' . $data['sj']->nomor;

		$this->load->library('pdf');
		$this->pdf->load_view('menu/cetakan_pdf/surat_jalanongkir', $data, $filename, $paper, $orientation);

		$this->pdf->setPaper($paper, 'portrait');
		$this->pdf->rander();
		$this->pdf->stream("welcome.pdf", array("Attachment" => 0));

		exit(0);
	}
}
