<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Reprint_Invoice extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('caridataaktif_model');
		$this->load->library('form_validation');
		$this->load->library('session');
	}

	public function Print($param = "")
	{
		$params = explode(":", urldecode($param));
		// print_r($params);
		// die();
		$paper = array(0, 0, 612, 396);
		$data['inv'] = $this->db->query("SELECT nomor, nomorso FROM srvt_invoice WHERE nomorgb = '" . $params[0] . "'")->row();

		$data['vinv'] = $this->db->query("SELECT
		inv.nomor,
		inv.nomorgb,
		inv.tanggal,
		inv.tgljthtempo,
		inv.kodecabang,
		so.kodesalesman,
		inv.lokasi,
		c.nama as namapelanggan,
		c.nama_toko,
		c.pengiriman,
		c.notelp,
		c.nohp,
		c.rekeningtf,
		c.alamat,
		pos.kota,
		pos.kodepos,
		so.jenisdisc,
		so.jenisjual,
		pro.nama as namapromo,
		a.nama as namaaccount,
		a.norekening,
		a.keterangan,
		a.namabisnis,
		a.nomor as nomorbisnis,
		inv.total,
		inv.dpp,
		inv.ppn,
		inv.grandtotal,
		inv.reprint,
		inv.nilaicustomer,
		inv.nilaicash,
		hist.revisike,
		kc.nama as namacabang
		FROM
		srvt_invoice inv
		LEFT JOIN srvt_salesorder so ON so.nomor = inv.nomorso
		LEFT JOIN hist_cancel hist ON hist.noinvoice = inv.nomor
		LEFT JOIN glbm_promo pro ON pro.kode = so.jenisdisc
		LEFT JOIN glbm_gudang g ON g.kode = inv.lokasi
		LEFT JOIN glbm_customer c ON c.nomor = inv.nopelanggan
		LEFT JOIN glbm_account a ON a.nomor = c.rekeningtf
		LEFT JOIN glbm_kodepos pos ON pos.kodepos = c.kodepos
		LEFT JOIN glbm_cabang kc on kc.kode = inv.kodecabang
		WHERE inv.nomorgb = '" . $params[0] . "'
		ORDER BY hist.revisike DESC
		LIMIT 1")->row();

		$data['vinvd'] = $this->db->query("SELECT
		invd.namabarang,
		s.nama as namasatuan,
		invd.qty,
		invd.harga,
		invd.discpaket,
		invd.discpaketitem,
		invd.discbarang,
		invd.discbrgitem,
		inv.nilaicustomer,
		inv.nilaicash,
		invd.total
		FROM
		srvt_invoice inv
		LEFT JOIN srvt_invoicedetail invd ON invd.nomor = inv.nomor
		LEFT JOIN glbm_barang b ON b.kode = invd.kodebarang
		LEFT JOIN glbm_satuan s ON s.kode = b.kodesatuan
		WHERE inv.nomorgb = '" . $params[0] . "'
		ORDER BY invd.namabarang ASC")->result();

		$orientation = 'portrait';
		$filename = 'Invoice - ' . $data['inv']->nomor;

		$this->load->library('pdf');
		$this->pdf->load_view('menu/cetakan_pdf/reprint_invoice', $data, $filename, $paper, $orientation);

		$this->pdf->setPaper($paper, 'portrait');
		$this->pdf->rander();
		$this->pdf->stream("welcome.pdf", array("Attachment" => 0));

		exit(0);
	}
}
