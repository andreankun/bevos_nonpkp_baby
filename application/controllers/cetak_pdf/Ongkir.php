<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Ongkir extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('caridataaktif_model');
		$this->load->library('form_validation');
		$this->load->library('session');
	}

	public function Print($nomor = "")
	{
		// $paper = 'A4';
		$paper = array(0, 0, 612, 396);
		$data['ongk'] = $this->db->query("SELECT nomor FROM srvt_ongkir WHERE nomor = '" . $nomor . "'")->row();
		$data['konfigurasi'] = $this->db->query("SELECT* FROM stpm_konfigurasi")->row();

		$data['vinv'] = $this->db->query("SELECT
		inv.nomor,
		inv.keterangan,
		inv.nopelanggan,
		inv.namapelanggan,
		inv.nilaiongkir,
		inv.nilainominal,
		inv.keterangan,
		inv.kodecabang,
		inv.tanggalongkir as tanggal,
		c.alamat,
		c.nama_toko,
		c.pengiriman,
		c.rekeningtf,
		c.kodepos,
		c.nohp,
		c.notelp,
		a.nama,
		a.namabisnis,
		a.norekening,
		p.tgljthtempo
	FROM
		srvt_ongkir inv
		LEFT JOIN glbm_customer c on c.nomor = inv.nopelanggan
		LEFT JOIN glbm_account a on a.nomor = c.rekeningtf
		LEFT JOIN srvt_piutang p on p.nomor = inv.nomor 
	WHERE inv.nomor = '" . $nomor . "'
		LIMIT 1")->row();

		$data['vinvd'] = $this->db->query("SELECT
		inv.keterangan,
		inv.nilaiongkir
	FROM
		srvt_ongkir inv
	WHERE
	 inv.nomor = '" . $nomor . "'")->result();

		$orientation = 'portrait';
		$filename = 'Invoice - ' . $data['ongk']->nomor;

		$this->load->library('pdf');
		$this->pdf->load_view('menu/cetakan_pdf/ongkir', $data, $filename, $paper, $orientation);

		$this->pdf->setPaper($paper, 'portrait');
		$this->pdf->rander();
		$this->pdf->stream("welcome.pdf", array("Attachment" => 0));

		exit(0);
	}
}
