<?php
defined('BASEPATH') or exit('No direct script access allowed');

class NotaRetur extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('caridataaktif_model');
		$this->load->library('form_validation');
		$this->load->library('session');
	}

	// public function Print($nomor = "")
	// {
	// 	$paper = 'A4';
	// 	// $data = 'asd';
	// 	$data['a'] = $this->db->query("SELECT * FROM srvt_invoice WHERE nomor = '" . $nomor . "'")->row();

	// 	$data['invoiced'] = $this->db->query("SELECT invoice.*, invoiced.*, brg.kodesatuan FROM srvt_invoicedetail invoiced LEFT JOIN srvt_invoice invoice on invoice.nomor = invoiced.nomor LEFT JOIN glbm_barang brg on brg.kode = invoiced.kodebarang WHERE invoiced.nomor = '" . $nomor . "'")->result();
	// 	// print_r($data);
	// 	// die();

	// 	$orientation = 'portrait';
	// 	// $filename = 'Surat Jalan - ' . $data['sj']->nomor;
	// 	$filename = 'Invoice -';

	// 	$this->load->library('pdf');
	// 	$this->pdf->load_view('menu/cetakan_pdf/invoice', $data, $filename, $paper, $orientation);

	// 	$this->pdf->setPaper('F4', 'landscape');
	// 	$this->pdf->rander();
	// 	$this->pdf->stream("welcome.pdf", array("Attachment" => 0));

	// 	exit(0);
	// }

	public function Print($nomor = "")
	{
		// $nomor = "RS-2022-02-00001";
		// $kode = "BVS";
		$paper = array(0, 0, 612, 396);
		$data['rp'] = $this->db->query("SELECT * FROM srvt_returbarang WHERE nomor = '" . $nomor . "'")->row();

		$data['vrp'] = $this->db->query("SELECT
			rp.*,
			invoice.nopelanggan,
			-- invoice.namapelanggan,
			c.alamat,
			c.npwp,
			c.nama,
			c.nama_toko
			FROM srvt_returbarang rp
			LEFT JOIN srvt_invoice invoice on invoice.nomor = rp.noinvoice
			LEFT JOIN glbm_customer c on c.nomor = rp.nopelanggan
			WHERE rp.nomor = '" . $nomor . "'
		")->row();

		$data['config'] = $this->db->query("SELECT *FROM stpm_konfigurasi")->row();

		$data['vrpd'] = $this->db->query("SELECT
			rpd.*
			FROM srvt_returbarangdetail rpd
			WHERE rpd.nomor = '" . $nomor . "'
		")->result();

		$orientation = 'portrait';
		$filename = 'Nota Retur - ' . $data['rp']->nomor;

		$this->load->library('pdf');
		$this->pdf->load_view('menu/cetakan_pdf/retur_penjualan_fp', $data, $filename, $paper, $orientation);

		$this->pdf->setPaper($paper, 'portrait');
		$this->pdf->rander();
		$this->pdf->stream("welcome.pdf", array("Attachment" => 0));

		exit(0);
	}
}
