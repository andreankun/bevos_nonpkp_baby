<?php
defined('BASEPATH') or exit('No direct script access allowed');

class TransferStok extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('caridataaktif_model');
		$this->load->library('form_validation');
		$this->load->library('session');
	}

	public function Print($nomor = "")
	{
		$paper = array(0, 0, 612, 396);
		$data['tf'] = $this->db->query("SELECT * FROM srvt_transferstock WHERE nomor = '" . $nomor . "'")->row();
		// $data['sj'] = 'Test';

		$data['vtf'] = $this->db->query("SELECT * FROM srvt_transferstock
		WHERE batal = false AND nomor = '" . $nomor . "'
		LIMIT 1")->row();

		$data['asal'] = $this->db->query("SELECT
		c.nama AS namacabangasal,
		g.nama AS namagudangasal
		FROM srvt_transferstock tf
		LEFT JOIN glbm_cabang c ON c.kode = tf.kodecbasal
		LEFT JOIN glbm_gudang g ON g.kode = tf.kodegdasal
		WHERE tf.batal = false AND tf.nomor = '" . $nomor . "'")->row();

		$data['tujuan'] = $this->db->query("SELECT
		c.nama AS namacabangtujuan,
		g.nama AS namagudangtujuan
		FROM srvt_transferstock tf
		LEFT JOIN glbm_cabang c ON c.kode = tf.kodecbtujuan
		LEFT JOIN glbm_gudang g ON g.kode = tf.kodegdtujuan
		WHERE tf.batal = false AND tf.nomor = '" . $nomor . "'")->row();

		$data['vtfd'] = $this->db->query("SELECT
        tf.nomor,
        tf.tanggal,
        tfd.kodebarang,
        tfd.namabarang,
        tfd.qty,
        s.nama AS namasatuan
    FROM
        srvt_transferstock tf
        LEFT JOIN srvt_transferstockdetail tfd ON tfd.nomor = tf.nomor
        LEFT JOIN glbm_gudang g ON g.kode = tf.kodegdasal
        LEFT JOIN glbm_cabang c ON c.kode = tf.kodecbasal
        LEFT JOIN glbm_barang b ON b.kode = tfd.kodebarang
        LEFT JOIN glbm_satuan s ON s.kode = b.kodesatuan
		WHERE tf.batal = false AND tf.nomor = '" . $nomor . "' AND tfd.qty > 0
		ORDER BY tfd.namabarang ASC")->result();

		$orientation = 'portrait';
		// $filename = 'Surat Jalan - ' . $data['sj']->nomor;
		$filename = 'Transfer Stok - ';

		$this->load->library('pdf');
		$this->pdf->load_view('menu/cetakan_pdf/transfer_stok', $data, $filename, $paper, $orientation);

		$this->pdf->setPaper($paper, 'portrait');
		$this->pdf->rander();
		$this->pdf->stream("welcome.pdf", array("Attachment" => 0));

		exit(0);
	}
}
