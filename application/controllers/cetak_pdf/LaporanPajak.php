<?php
defined('BASEPATH') or exit('No direct script access allowed');

class LaporanPajak extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('laporan/LaporanPajak_model');
		$this->load->model('caridataaktif_model');
		$this->load->library('form_validation');
		$this->load->library('session');
	}

	public function RekapPPnPenjualan($param = "")
	{
		$params = explode(":", urldecode($param));

		$filename = 'Laporan Rekap PPn Penjualan' . '_' . date('d M Y', strtotime($params[0])) . ' - ' . date('d M Y', strtotime($params[1]));

		// if (!empty($tanggal)){

		$paper = 'A4';
		$orientation = 'landscape';

		// menggunakan class dompdf
		$this->load->library('pdf');

		$data['report'] = $this->LaporanPajak_model->GetRekapPPnPenjualan($params[0], $params[1]);
		$data['ppn'] = $this->LaporanPajak_model->Konfigurasi();
		// $data['list'] = $this->LaporanPajak_model->GetPenjualanSemuaPelangganList($params[0], $params[1], $params[2]);
		// $data['detail'] = $this->LaporanPajak_model->GetPenjualanSemuaPelangganDetail($params[0], $params[1], $params[2]);
		// $data['row'] = $this->LaporanPajak_model->Cabang($params[2]);
		$data['tglawal'] = date('d-m-Y', strtotime($params[0]));
		$data['tglakhir'] = date('d-m-Y', strtotime($params[1]));
		// $data['cabang'] = $params[2];

		$this->pdf->load_view('menu/cetakan_pdf/r_rekap_ppn_penjualan', $data, $filename, $paper, $orientation);

		// (Opsional) Mengatur ukuran kertas dan orientasi kertas
		$this->pdf->setPaper('F4', 'landscape');

		// Menjadikan HTML sebagai PDF
		$this->pdf->render();

		// Output akan menghasilkan PDF (1 = download dan 0 = preview)
		$this->pdf->stream("welcome.pdf", array("Attachment" => 0));

		exit(0);
		// }
	}

	public function ExportRekapPPnPenjualan($param = "")
	{
		$params = explode(":", urldecode($param));

		$data['report'] = $this->LaporanPajak_model->GetRekapPPnPenjualan($params[0], $params[1]);
		$data['ppn'] = $this->LaporanPajak_model->Konfigurasi();
		$data['tglawal'] = date('d-m-Y', strtotime($params[0]));
		$data['tglakhir'] = date('d-m-Y', strtotime($params[1]));
		// $data['cabang'] = $params[2];
		$this->load->view('menu/reporting/pajak/excel_rekap_ppn_penjualan', $data);
	}

	public function ExportPenjualan($param = "")
	{
		$params = explode(":", urldecode($param));

		$data['report'] = $this->LaporanPajak_model->GetDataPenjualan($params[0], $params[1], $params[2]);
		$data['ppn'] = $this->LaporanPajak_model->Konfigurasi();
		$data['tglawal'] = date('d-m-Y', strtotime($params[0]));
		$data['tglakhir'] = date('d-m-Y', strtotime($params[1]));
		// $data['cabang'] = $params[2];
		$this->load->view('menu/reporting/pajak/excel_penjualan', $data);
	}

	public function ExportEfaktur($param = "")
	{
		$params = explode(":", urldecode($param));

		$data['reportrow'] = $this->LaporanPajak_model->GetDataEFakturHeader($params[0], $params[1]);
		$data['report'] = $this->LaporanPajak_model->GetDataEFakturList($params[0], $params[1]);
		$data['detail'] = $this->LaporanPajak_model->GetDataEFakturDetail($params[0], $params[1]);
		$data['konfigurasi'] = $this->LaporanPajak_model->Konfigurasi();
		$data['tglawal'] = date('Y-m-d', strtotime($params[0]));
		$data['tglakhir'] = date('Y-m-d', strtotime($params[1]));
		$this->load->view('menu/reporting/pajak/excel_efaktur', $data);
	}

	public function ExportCustomerAllCabang()
	{
		// $params = explode(":", urldecode($param));

		$data['report'] = $this->LaporanPajak_model->GetExportCustomerAllCabang();
		$this->load->view('menu/reporting/pajak/excel_customer_all_cabang', $data);
	}

	public function ExportInvoicePajak($param = "")
	{
		$params = explode(":", urldecode($param));

		$data['report'] = $this->LaporanPajak_model->GetDataInvoiceOutstanding($params[0],$params[1],$params[2]);
		$data['tglawal'] = date('d-m-Y', strtotime($params[0]));
		$data['tglakhir'] = date('d-m-Y', strtotime($params[1]));
		$data['kodecabang'] = $params[2];
		$this->load->view('menu/reporting/pajak/excel_invoice_outstanding', $data);
	}

	function ExportPembayaranPiutang($param = ""){
		$params = explode(":", urldecode($param));

		$data['report'] = $this->LaporanPajak_model->GetDataPenerimaan($params[0],$params[1],$params[2]);
		$data['tglawal'] = date('d-m-Y', strtotime($params[0]));
		$data['tglakhir'] = date('d-m-Y', strtotime($params[1]));
		$data['kodecabang'] = $params[2];
		$this->load->view('menu/reporting/pajak/excel_pembayaran', $data);
	}

	public function ExportLaporanCustomer()
	{
		// $params = explode(":", urldecode($param));

		$data['report'] = $this->LaporanPajak_model->GetExportCustomer();
		$this->load->view('menu/reporting/pajak/excel_customer_all_cabang', $data);
	}

	public function ExportDeposit($param = "")
	{
		$params = explode(":", urldecode($param));

		$data['header'] = $this->LaporanPajak_model->GetDataDepositHeader($params[0], $params[1], $params[2]);
		$data['report'] = $this->LaporanPajak_model->GetDataDepositList($params[0], $params[1], $params[2]);
		$data['detail'] = $this->LaporanPajak_model->GetDataDepositDetail($params[0], $params[1], $params[2]);
		$data['tglawal'] = date('d-m-Y', strtotime($params[0]));
		$data['tglakhir'] = date('d-m-Y', strtotime($params[1]));
		$data['kodecabang'] = $params[2];
		$this->load->view('menu/reporting/pajak/excel_deposit', $data);
	}

	public function ExportPenerimaanUang($param = "")
	{
		$params = explode(":", urldecode($param));

		$data['report'] = $this->LaporanPajak_model->GetDataPenerimaanUang($params[0], $params[1], $params[2]);
		$data['tglawal'] = date('d-m-Y', strtotime($params[0]));
		$data['tglakhir'] = date('d-m-Y', strtotime($params[1]));
		$data['kodecabang'] = $params[2];
		$this->load->view('menu/reporting/pajak/excel_penerimaan', $data);
	}

	public function ExportPemasok($param = "")
	{
		$params = explode(":", urldecode($param));

		$data['report'] = $this->LaporanPajak_model->GetDataPemasok($params[0]);
		$data['kodecabang'] = $params[0];
		$this->load->view('menu/reporting/pajak/excel_pemasok', $data);
	}

	public function ExportPenerimaan($param = "")
	{
		$params = explode(":", urldecode($param));
		// print_r($params);
		// die();

		$data['report'] = $this->LaporanPajak_model->GetDataPenerimaanBarang($params[0],$params[1], $params[2]);
		$data['tglawal'] = date('d-m-Y', strtotime($params[0]));
		$data['tglakhir'] = date('d-m-Y', strtotime($params[1]));
		$data['kodegudang'] = $this->LaporanPajak_model->Gudang($params[2]);
		// print_r($data);
		// die();
		$this->load->view('menu/reporting/pajak/excel_penerimaanbarang', $data);
	}

	public function ExportBarangKeluar($param = "")
	{
		$params = explode(":", urldecode($param));
		// print_r($params);
		// die();

		$data['report'] = $this->LaporanPajak_model->GetDataBarangKeluar($params[0],$params[1], $params[2]);
		$data['tglawal'] = date('d-m-Y', strtotime($params[0]));
		$data['tglakhir'] = date('d-m-Y', strtotime($params[1]));
		$data['kodegudang'] = $this->LaporanPajak_model->Gudang($params[2]);
		// print_r($data);
		// die();
		$this->load->view('menu/reporting/pajak/excel_barangkeluar', $data);
	}

	public function ExportMasterBarang($param = "")
	{
		$params = explode(":", urldecode($param));
		// print_r($params);
		// die();

		$data['report'] = $this->LaporanPajak_model->GetExportMasterBarang();
		$data['merk'] = $this->LaporanPajak_model->GetMasterMerk();
		// $data['tglawal'] = date('d-m-Y', strtotime());
		// $data['tglakhir'] = date('d-m-Y', strtotime($params[1]));
		$data['cabang'] = $this->LaporanPajak_model->Cabang();
		// print_r($data);
		// die();
		$this->load->view('menu/reporting/pajak/excel_master_barang', $data);
	}

	public function ExportBarangKetuker($param = "")
	{
		$params = explode(":", urldecode($param));
		// print_r($params);
		// die();

		$data['report'] = $this->LaporanPajak_model->GetDataBarangKetuker($params[0],$params[1], $params[2]);
		$data['tglawal'] = date('d-m-Y', strtotime($params[0]));
		$data['tglakhir'] = date('d-m-Y', strtotime($params[1]));
		$data['kodecabang'] = $this->LaporanPajak_model->Cabang($params[2]);
		// print_r($data);
		// die();
		$this->load->view('menu/reporting/pajak/excel_barangketuker', $data);
	}

	public function ExporBarangPacking($param = "")
	{
		$params = explode(":", urldecode($param));
		// print_r($params);
		// die();

		$data['report'] = $this->LaporanPajak_model->GetDataBarangPacking($params[0],$params[1], $params[2]);
		$data['tglawal'] = date('d-m-Y', strtotime($params[0]));
		$data['tglakhir'] = date('d-m-Y', strtotime($params[1]));
		$data['kodecabang'] = $this->LaporanPajak_model->Cabang($params[2]);
		// print_r($data);
		// die();
		$this->load->view('menu/reporting/pajak/excel_barangpacking', $data);
	}

	public function ExporBarangRusak($param = "")
	{
		$params = explode(":", urldecode($param));
		// print_r($params);
		// die();

		$data['report'] = $this->LaporanPajak_model->GetDataBarangRusak($params[0],$params[1], $params[2]);
		$data['row'] = $this->LaporanPajak_model->GetDataBarangRusakRow($params[0],$params[1], $params[2]);
		$data['tglawal'] = date('d-m-Y', strtotime($params[0]));
		$data['tglakhir'] = date('d-m-Y', strtotime($params[1]));
		$data['kodecabang'] = $this->LaporanPajak_model->Cabang($params[2]);
		// print_r($data);
		// die();
		$this->load->view('menu/reporting/pajak/excel_barangrusak', $data);
	}

	public function PreviewRekapPPnPenjualan($param = "")
	{
		$params = explode(":", urldecode($param));

		$filename = 'Laporan Rekap PPn Penjualan' . '_' . date('d M Y', strtotime($params[0])) . ' - ' . date('d M Y', strtotime($params[1]));

		// if (!empty($tanggal)){

		$paper = 'A4';
		$orientation = 'landscape';

		// menggunakan class dompdf
		// $this->load->library('pdf');

		$data['report'] = $this->LaporanPajak_model->GetRekapPPnPenjualan($params[0], $params[1]);
		$data['ppn'] = $this->LaporanPajak_model->Konfigurasi();
		// $data['list'] = $this->LaporanPajak_model->GetPenjualanSemuaPelangganList($params[0], $params[1], $params[2]);
		// $data['detail'] = $this->LaporanPajak_model->GetPenjualanSemuaPelangganDetail($params[0], $params[1], $params[2]);
		// $data['row'] = $this->LaporanPajak_model->Cabang($params[2]);
		$data['tglawal'] = date('d-m-Y', strtotime($params[0]));
		$data['tglakhir'] = date('d-m-Y', strtotime($params[1]));
		// $data['cabang'] = $params[2];

		set_time_limit(300);

		$this->load->view('menu/cetakan_pdf/r_rekap_ppn_penjualan', $data, $filename, $paper, $orientation);

		// (Opsional) Mengatur ukuran kertas dan orientasi kertas
		// $this->pdf->setPaper('F4', 'landscape');

		// Menjadikan HTML sebagai PDF
		// $this->pdf->render();

		// Output akan menghasilkan PDF (1 = download dan 0 = preview)
		// $this->pdf->stream("welcome.pdf", array("Attachment" => 0));

		// exit(0);
		// }
	}
}
