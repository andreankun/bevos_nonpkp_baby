<?php
defined('BASEPATH') or exit('No direct script access allowed');

class LaporanPersediaan extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('laporan/LaporanPersediaan_model');
		$this->load->model('caridataaktif_model');
		$this->load->library('form_validation');
		$this->load->library('session');
	}

	function PersediaanBarangAllgudangPerkategori($param = "")
	{
		$params = explode(":", urldecode($param));
		// $tglawal = $params[0];
		// $tglakhir = $params[1];
		// $gudang = $params[2];
		// $merkawal = $params[3];
		// $merkakhir = $params[4];

		$filename = 'Laporan Persediaan Barang ' . '_' . date('d M Y', strtotime($params[0])) . ' - ' . date('d M Y', strtotime($params[1]));


		$paper = 'A4';
		$orientation = 'portrait';

		// menggunakan class dompdf
		$this->load->library('pdf');

		$data['report'] = $this->LaporanPersediaan_model->GetdataPersedianPerKategori($params[0], $params[1], $params[2], $params[3], $params[4]);
		$data['reportrow'] = $this->LaporanPersediaan_model->GetdataPersedianPerKategoriRow($params[0], $params[1], $params[2], $params[3], $params[4]);
		$data['persediaan'] = $this->LaporanPersediaan_model->GetdataPersedianPerKategorList($params[0], $params[1], $params[2], $params[3], $params[4]);
		// $data['persediaanlist'] = $this->LaporanPersediaan_model->PersediaanAllgudangPerkategoriList($params[0], $params[1], $params[2],$params[3]);
		$data['tglawal'] = date('Y-m-d', strtotime($params[0]));
		$data['tglakhir'] = date('Y-m-d', strtotime($params[1]));
		$data['merkawal'] = $this->LaporanPersediaan_model->GetCabang($params[2]);
		$data['merkakhir'] = $this->LaporanPersediaan_model->GetCabangAkhir($params[3]);
		// var_dump($data);
		// die();

		$this->pdf->load_view('menu/cetakan_pdf/laporanpersediaanallgudangperkategori', $data, $filename, $paper, $orientation);

		// (Opsional) Mengatur ukuran kertas dan orientasi kertas
		$this->pdf->setPaper('F4', 'portrait');

		// Menjadikan HTML sebagai PDF
		$this->pdf->render();

		// Output akan menghasilkan PDF (1 = download dan 0 = preview)
		$this->pdf->stream("welcome.pdf", array("Attachment" => 0));

		exit(0);
	}

	function PersediaanBarangPergudangAllKategori($param)
	{
		$params = explode(":", urldecode($param));
		// $tglawal = $params[0];
		// $tglakhir = $params[1];
		// $gudang = $params[2];
		// $merkawal = $params[3];
		// $merkakhir = $params[4];

		$filename = 'Laporan Persediaan Barang ' . '_' . date('d M Y', strtotime($params[0])) . ' - ' . date('d M Y', strtotime($params[1]));


		$paper = 'A4';
		$orientation = 'portrait';

		// menggunakan class dompdf
		$this->load->library('pdf');

		$data['report'] = $this->LaporanPersediaan_model->GetDataPersedianPerGudang($params[0], $params[1], $params[2], $params[3], $params[4], $params[5]);
		// $data['reportrow'] = $this->LaporanPersediaan_model->GetdataPersedianPerKategoriRow($params[0], $params[1], $params[2], $params[3], $params[4], $params[5]);
		$data['persediaan'] = $this->LaporanPersediaan_model->GetDataPersedianPerGudangList($params[0], $params[1], $params[2], $params[3], $params[4], $params[5]);
		// $data['persediaanlist'] = $this->LaporanPersediaan_model->PersediaanAllgudangPerkategoriList($params[0], $params[1], $params[2],$params[3]);
		$data['tglawal'] = date('Y-m-d', strtotime($params[0]));
		$data['tglakhir'] = date('Y-m-d', strtotime($params[1]));
		$data['merkawal'] = $this->LaporanPersediaan_model->GetCabang($params[2]);
		$data['merkakhir'] = $this->LaporanPersediaan_model->GetCabangAkhir($params[3]);
		$data['gudang'] = $params[4];
		// var_dump($data);
		// die();

		$this->pdf->load_view('menu/cetakan_pdf/laporanpersedianpergudangsemuakategori', $data, $filename, $paper, $orientation);

		// (Opsional) Mengatur ukuran kertas dan orientasi kertas
		$this->pdf->setPaper('F4', 'portrait');

		// Menjadikan HTML sebagai PDF
		$this->pdf->render();

		// Output akan menghasilkan PDF (1 = download dan 0 = preview)
		$this->pdf->stream("welcome.pdf", array("Attachment" => 0));

		exit(0);
	}

	function PersedianBarangSemuaGudangSemuaKategori($param = "")
	{
		$params = explode(":", urldecode($param));
		// $tglawal = $params[0];
		// $tglakhir = $params[1];
		// $gudang = $params[2];
		// $merkawal = $params[3];
		// $merkakhir = $params[4];

		$filename = 'Laporan Persediaan Barang ' . '_' . date('d M Y', strtotime($params[0])) . ' - ' . date('d M Y', strtotime($params[1]));


		$paper = 'A4';
		$orientation = 'portrait';

		// menggunakan class dompdf
		$this->load->library('pdf');

		$data['report'] = $this->LaporanPersediaan_model->GetdataPersedianPerKategori($params[0], $params[1], $params[2], $params[3], $params[4]);
		$data['reportrow'] = $this->LaporanPersediaan_model->GetdataPersedianPerKategoriRow($params[0], $params[1], $params[2], $params[3], $params[4]);
		$data['persediaan'] = $this->LaporanPersediaan_model->GetdataPersedianPerKategorList($params[0], $params[1], $params[2], $params[3], $params[4]);
		$data['tglawal'] = date('Y-m-d', strtotime($params[0]));
		$data['tglakhir'] = date('Y-m-d', strtotime($params[1]));
		$data['merkawal'] = $this->LaporanPersediaan_model->GetCabang($params[2]);
		$data['merkakhir'] = $this->LaporanPersediaan_model->GetCabangAkhir($params[3]);
		$data['cabang'] = $params[4];

		// var_dump($data);
		// die();

		$this->pdf->load_view('menu/cetakan_pdf/laporanpersediansemuagudangsemuakategori', $data, $filename, $paper, $orientation);

		// (Opsional) Mengatur ukuran kertas dan orientasi kertas
		$this->pdf->setPaper('F4', 'portrait');

		// Menjadikan HTML sebagai PDF
		$this->pdf->render();

		// Output akan menghasilkan PDF (1 = download dan 0 = preview)
		$this->pdf->stream("welcome.pdf", array("Attachment" => 0));

		exit(0);
	}

	function PersedianBarangPergudangSemuaKategori($param)
	{
		$params = explode(":", urldecode($param));
		// $tglawal = $params[0];
		// $tglakhir = $params[1];
		// $gudang = $params[2];
		// $merkawal = $params[3];
		// $merkakhir = $params[4];

		$filename = 'Laporan Persediaan Barang ' . '_' . date('d M Y', strtotime($params[0])) . ' - ' . date('d M Y', strtotime($params[1]));


		$paper = 'A4';
		$orientation = 'portrait';

		// menggunakan class dompdf
		$this->load->library('pdf');

		$data['report'] = $this->LaporanPersediaan_model->GetdataPersedianPerKategori($params[0], $params[1], $params[2], $params[3]);
		$data['row'] = $this->LaporanPersediaan_model->GetdataPersedianPerKategoriRow($params[0], $params[1], $params[2], $params[3]);
		$data['persediaan'] = $this->LaporanPersediaan_model->GetdataPersedianPerKategorList($params[0], $params[1], $params[2], $params[3]);
		$data['tglawal'] = date('Y-m-d', strtotime($params[0]));
		$data['tglakhir'] = date('Y-m-d', strtotime($params[1]));
		$data['merkawal'] = $this->LaporanPersediaan_model->GetCabang($params[2]);
		$data['merkakhir'] = $this->LaporanPersediaan_model->GetCabangAkhir($params[3]);
		$data['gudang'] = $params[5];

		$this->pdf->load_view('menu/cetakan_pdf/laporanpersedianpergudangsemuakategori', $data, $filename, $paper, $orientation);

		// (Opsional) Mengatur ukuran kertas dan orientasi kertas
		$this->pdf->setPaper('F4', 'portrait');

		// Menjadikan HTML sebagai PDF
		$this->pdf->render();

		// Output akan menghasilkan PDF (1 = download dan 0 = preview)
		$this->pdf->stream("welcome.pdf", array("Attachment" => 0));

		exit(0);
	}

	function LaporanBarangKeluar($param = "")
	{
		$params = explode(":", urldecode($param));
		// $tglawal = $params[0];
		// $tglakhir = $params[1];
		// $gudang = $params[2];
		// $merkawal = $params[3];
		// $merkakhir = $params[4];

		$filename = 'Laporan Barang Keluar Per-gudang ' . '_' . date('d M Y', strtotime($params[0])) . ' - ' . date('d M Y', strtotime($params[1]));


		$paper = 'A4';
		$orientation = 'portrait';

		// menggunakan class dompdf
		$this->load->library('pdf');

		$data['report'] = $this->LaporanPersediaan_model->GetDataBarangKeluar($params[0], $params[1], $params[2]);
		$data['row'] = $this->LaporanPersediaan_model->GetDataBarangKeluarRow($params[0], $params[1], $params[2]);
		$data['persediaan'] = $this->LaporanPersediaan_model->GetDataBarangKeluarList($params[0], $params[1], $params[2]);
		$data['tglawal'] = date('Y-m-d', strtotime($params[0]));
		$data['tglakhir'] = date('Y-m-d', strtotime($params[1]));
		$data['gudang'] = $this->LaporanPersediaan_model->Gudang($params[2]);


		$this->pdf->load_view('menu/cetakan_pdf/laporanbarangkeluarpergudang', $data, $filename, $paper, $orientation);

		// (Opsional) Mengatur ukuran kertas dan orientasi kertas
		$this->pdf->setPaper('F4', 'portrait');

		// Menjadikan HTML sebagai PDF
		$this->pdf->render();

		// Output akan menghasilkan PDF (1 = download dan 0 = preview)
		$this->pdf->stream("welcome.pdf", array("Attachment" => 0));

		exit(0);
	}

	function LaporanBarangKeluarSemuaGudang($param = "")
	{
		$params = explode(":", urldecode($param));
		// $tglawal = $params[0];
		// $tglakhir = $params[1];
		// $gudang = $params[2];
		// $merkawal = $params[3];
		// $merkakhir = $params[4];

		$filename = 'Laporan Barang Keluar Semua Gudang ' . '_' . date('d M Y', strtotime($params[0])) . ' - ' . date('d M Y', strtotime($params[1]));


		$paper = 'A4';
		$orientation = 'portrait';

		// menggunakan class dompdf
		$this->load->library('pdf');

		$data['report'] = $this->LaporanPersediaan_model->GetDataBarangKeluarSemuaGudang($params[0], $params[1], $params[2]);
		$data['row'] = $this->LaporanPersediaan_model->GetDataBarangKeluarSemuaRow($params[0], $params[1], $params[2]);
		$data['persediaan'] = $this->LaporanPersediaan_model->GetDataBarangKeluarSemuaGudangList($params[0], $params[1], $params[2]);
		$data['tglawal'] = date('Y-m-d', strtotime($params[0]));
		$data['tglakhir'] = date('Y-m-d', strtotime($params[1]));
		$data['cabang'] = $this->LaporanPersediaan_model->Cabang($params[2]);


		$this->pdf->load_view('menu/cetakan_pdf/laporanbarangkeluarsemuagudang', $data, $filename, $paper, $orientation);

		// (Opsional) Mengatur ukuran kertas dan orientasi kertas
		$this->pdf->setPaper('F4', 'portrait');

		// Menjadikan HTML sebagai PDF
		$this->pdf->render();

		// Output akan menghasilkan PDF (1 = download dan 0 = preview)
		$this->pdf->stream("welcome.pdf", array("Attachment" => 0));

		exit(0);
	}

	function CetakKartuStokPerbarang($param = "")
	{
		$params = explode(":", urldecode($param));
		// $barang = $params[2];
		// $cabang = $params[3];

		$filename = 'Laporan Kartu Stok Per Barang ' . '_' . date('d M Y', strtotime($params[0])) . ' - ' . date('d M Y', strtotime($params[1]));


		$paper = 'A4';
		$orientation = 'portrait';

		// menggunakan class dompdf
		$this->load->library('pdf');

		// $data['reportrow'] = $this->LaporanPersediaan_model->GetDataKartuInvoice($params[0], $params[1], $params[2], $params[3], $params[4]);
		$data['report'] = $this->LaporanPersediaan_model->LaporanKartuStok($params[0], $params[1], $params[2], $params[3]);
		$data['kartustok'] = $this->LaporanPersediaan_model->GetDataKartuStok($params[0], $params[1], $params[2], $params[3]);
		// $data['persediaan'] = $this->LaporanPersediaan_model->GetDataKartuPeneriman($params[0], $params[1], $params[2], $params[3], $params[4]);
		$data['tglawal'] = date('Y-m-d', strtotime($params[0]));
		$data['tglakhir'] = date('Y-m-d', strtotime($params[1]));
		// $data['barang'] = $this->LaporanPersediaan_model->Barang($params[2]);
		$data['barang'] = $this->LaporanPersediaan_model->Barang($params[2]);
		$data['gudang'] = $this->LaporanPersediaan_model->Gudang($params[3]);
		// $data['cabang'] = $params[3];
		// var_dump($data);
		// die();


		$this->pdf->load_view('menu/cetakan_pdf/laporankartustokbarang', $data, $filename, $paper, $orientation);

		// (Opsional) Mengatur ukuran kertas dan orientasi kertas
		$this->pdf->setPaper('F4', 'portrait');

		// Menjadikan HTML sebagai PDF
		$this->pdf->render();

		// Output akan menghasilkan PDF (1 = download dan 0 = preview)
		$this->pdf->stream("welcome.pdf", array("Attachment" => 0));

		exit(0);
	}

	function CetakKartuStokPerbarangPajak($param = "")
	{
		$params = explode(":", urldecode($param));
		// $barang = $params[2];
		// $cabang = $params[3];

		$filename = 'Laporan Kartu Stok Per Barang ' . '_' . date('d M Y', strtotime($params[0])) . ' - ' . date('d M Y', strtotime($params[1]));


		$paper = 'A4';
		$orientation = 'portrait';

		// menggunakan class dompdf
		$this->load->library('pdf');

		// $data['reportrow'] = $this->LaporanPersediaan_model->GetDataKartuInvoice($params[0], $params[1], $params[2], $params[3], $params[4]);
		$data['report'] = $this->LaporanPersediaan_model->LaporanKartuStokPajak($params[0], $params[1], $params[2], $params[3]);
		$data['kartustok'] = $this->LaporanPersediaan_model->GetDataKartuStok($params[0], $params[1], $params[2], $params[3]);
		// $data['persediaan'] = $this->LaporanPersediaan_model->GetDataKartuPeneriman($params[0], $params[1], $params[2], $params[3], $params[4]);
		$data['tglawal'] = date('Y-m-d', strtotime($params[0]));
		$data['tglakhir'] = date('Y-m-d', strtotime($params[1]));
		$data['barang'] = $this->LaporanPersediaan_model->Barang($params[2]);
		$data['gudang'] = $this->LaporanPersediaan_model->Gudang($params[3]);
		// $data['cabang'] = $params[3];
		// var_dump($data);
		// die();


		$this->pdf->load_view('menu/cetakan_pdf/laporankartustokbarang', $data, $filename, $paper, $orientation);

		// (Opsional) Mengatur ukuran kertas dan orientasi kertas
		$this->pdf->setPaper('F4', 'portrait');

		// Menjadikan HTML sebagai PDF
		$this->pdf->render();

		// Output akan menghasilkan PDF (1 = download dan 0 = preview)
		$this->pdf->stream("welcome.pdf", array("Attachment" => 0));

		exit(0);
	}

	function CetakKartuStokSemuaBarang($param = "")
	{
		$params = explode(":", urldecode($param));
		// $barang = $params[2];
		// $cabang = $params[3];

		$filename = 'Laporan Kartu Stok Semua Barang ' . '_' . date('d M Y', strtotime($params[0])) . ' - ' . date('d M Y', strtotime($params[1]));


		$paper = 'A4';
		$orientation = 'portrait';

		// menggunakan class dompdf
		$this->load->library('pdf');

		// $data['reportrow'] = $this->LaporanPersediaan_model->GetDataKartuInvoice($params[0], $params[1], $params[2], $params[3], $params[4]);
		$data['report'] = $this->LaporanPersediaan_model->GetDataKartuStokMerk1($params[0], $params[1], $params[2], $params[3], $params[4]);
		$data['kartulist'] = $this->LaporanPersediaan_model->GetDataKartuStokMerk2($params[0], $params[1], $params[2], $params[3], $params[4]);
		$data['Kartulist2'] = $this->LaporanPersediaan_model->GetDataKartuStokMerk3($params[0], $params[1], $params[2], $params[3], $params[4]);
		$data['kartustok'] = $this->LaporanPersediaan_model->GetDataKartuStokMerk($params[0], $params[1], $params[2], $params[3], $params[4]);
		// $data['persediaan'] = $this->LaporanPersediaan_model->GetDataKartuPeneriman($params[0], $params[1], $params[2], $params[3], $params[4]);
		$data['tglawal'] = date('Y-m-d', strtotime($params[0]));
		$data['tglakhir'] = date('Y-m-d', strtotime($params[1]));
		$data['merkawal'] = $this->LaporanPersediaan_model->GetCabang($params[2]);
		$data['merkakhir'] = $this->LaporanPersediaan_model->GetCabangAkhir($params[3]);
		$data['cabang'] = $this->LaporanPersediaan_model->Cabang($params[4]);
		// $data['cabang'] = $params[3];
		// var_dump($data);
		// die();


		$this->pdf->load_view('menu/cetakan_pdf/laporankartustoksemua', $data, $filename, $paper, $orientation);

		// (Opsional) Mengatur ukuran kertas dan orientasi kertas
		$this->pdf->setPaper('F4', 'portrait');

		// Menjadikan HTML sebagai PDF
		$this->pdf->render();

		// Output akan menghasilkan PDF (1 = download dan 0 = preview)
		$this->pdf->stream("welcome.pdf", array("Attachment" => 0));

		exit(0);
	}



	function ExportBarangKeluarPergudang($param = "")
	{
		$params = explode(":", urldecode($param));


		$data['report'] = $this->LaporanPersediaan_model->GetDataBarangKeluar($params[0], $params[1], $params[2]);
		// $data['row'] = $this->LaporanPersediaan_model->GetdataPersedianPerKategoriRow($params[0], $params[1], $params[2]);
		$data['persediaan'] = $this->LaporanPersediaan_model->GetDataBarangKeluarList($params[0], $params[1], $params[2]);
		$data['tglawal'] = date('Y-m-d', strtotime($params[0]));
		$data['tglakhir'] = date('Y-m-d', strtotime($params[1]));
		$data['gudang'] = $this->LaporanPersediaan_model->Gudang($params[2]);
		// print_r($data);
		// die();
		$this->load->view('menu/reporting/persedian/excelbarangkeluarpergudang', $data);
	}

	function ExportBarangKeluarSemuaGudang($param = "")
	{
		$params = explode(":", urldecode($param));


		$data['report'] = $this->LaporanPersediaan_model->GetDataBarangKeluarSemuaGudang($params[0], $params[1], $params[2]);
		// $data['row'] = $this->LaporanPersediaan_model->GetdataPersedianPerKategoriRow($params[0], $params[1], $params[2]);
		$data['persediaan'] = $this->LaporanPersediaan_model->GetDataBarangKeluarSemuaGudangList($params[0], $params[1], $params[2]);
		$data['tglawal'] = date('Y-m-d', strtotime($params[0]));
		$data['tglakhir'] = date('Y-m-d', strtotime($params[1]));
		$data['cabang'] = $this->LaporanPersediaan_model->Cabang($params[2]);
		// print_r($data);
		// die();
		$this->load->view('menu/reporting/persedian/excelbarangkeluarsemuagudang', $data);
	}

	function ExportPersedianSemuaGudangSemuaBarang($param = "")
	{
		$params = explode(":", urldecode($param));



		$data['report'] = $this->LaporanPersediaan_model->GetdataPersedianPerKategori($params[0], $params[1], $params[2], $params[3], $params[4]);
		$data['reportrow'] = $this->LaporanPersediaan_model->GetdataPersedianPerKategoriRow($params[0], $params[1], $params[2], $params[3], $params[4]);
		$data['persediaan'] = $this->LaporanPersediaan_model->GetdataPersedianPerKategorList($params[0], $params[1], $params[2], $params[3], $params[4]);
		$data['tglawal'] = date('Y-m-d', strtotime($params[0]));
		$data['tglakhir'] = date('Y-m-d', strtotime($params[1]));
		$data['merkawal'] = $this->LaporanPersediaan_model->GetCabang($params[2]);
		$data['merkakhir'] = $this->LaporanPersediaan_model->GetCabangAkhir($params[3]);
		$data['cabang'] = $this->LaporanPersediaan_model->Cabang($params[4]);

		// $data['gudang'] = $params[2];
		// print_r($data);
		// die();
		$this->load->view('menu/reporting/persedian/excelpersediansemuagudangsemuabarang', $data);
	}

	function ExportPersedianSemuaGudangPerkategori($param = "")
	{
		$params = explode(":", urldecode($param));

		$data['report'] = $this->LaporanPersediaan_model->GetdataPersedianPerKategori($params[0], $params[1], $params[2], $params[3], $params[4]);
		$data['reportrow'] = $this->LaporanPersediaan_model->GetdataPersedianPerKategoriRow($params[0], $params[1], $params[2], $params[3], $params[4]);
		$data['persediaan'] = $this->LaporanPersediaan_model->GetdataPersedianPerKategorList($params[0], $params[1], $params[2], $params[3], $params[4]);
		// $data['persediaanlist'] = $this->LaporanPersediaan_model->PersediaanAllgudangPerkategoriList($params[0], $params[1], $params[2],$params[3]);
		$data['tglawal'] = date('Y-m-d', strtotime($params[0]));
		$data['tglakhir'] = date('Y-m-d', strtotime($params[1]));
		$data['merkawal'] = $this->LaporanPersediaan_model->GetCabang($params[2]);
		$data['merkakhir'] = $this->LaporanPersediaan_model->GetCabangAkhir($params[3]);

		$this->load->view('menu/reporting/persedian/excelpersedianbarangsemuagudangperkategori', $data);
	}

	function ExportPersedianPerGudangPerkategori($param = "")
	{
		$params = explode(":", urldecode($param));

		$data['report'] = $this->LaporanPersediaan_model->GetDataPersedianPerGudang($params[0], $params[1], $params[2], $params[3], $params[4], $params[5]);
		$data['reportrow'] = $this->LaporanPersediaan_model->GetdataPersedianPerKategoriRow($params[0], $params[1], $params[2], $params[3], $params[4]);
		$data['persediaan'] = $this->LaporanPersediaan_model->GetdataPersedianPerKategorList($params[0], $params[1], $params[2], $params[3], $params[4]);
		// $data['persediaanlist'] = $this->LaporanPersediaan_model->PersediaanAllgudangPerkategoriList($params[0], $params[1], $params[2],$params[3]);
		$data['tglawal'] = date('Y-m-d', strtotime($params[0]));
		$data['tglakhir'] = date('Y-m-d', strtotime($params[1]));
		$data['merkawal'] = $this->LaporanPersediaan_model->GetCabang($params[2]);
		$data['merkakhir'] = $this->LaporanPersediaan_model->GetCabangAkhir($params[3]);

		$this->load->view('menu/reporting/persedian/excelpersedianpergudangallkategori', $data);
	}

	function ExportLaporanKartuStokPerbarang($param = "")
	{
		$params = explode(":", urldecode($param));

		// $data['reportrow'] = $this->LaporanPersediaan_model->GetDataKartuInvoice($params[0], $params[1], $params[2], $params[3], $params[4]);
		$data['report'] = $this->LaporanPersediaan_model->LaporanKartuStok($params[0], $params[1], $params[2], $params[3]);
		$data['kartustok'] = $this->LaporanPersediaan_model->GetDataKartuStok($params[0], $params[1], $params[2], $params[3]);
		// $data['persediaan'] = $this->LaporanPersediaan_model->GetDataKartuPeneriman($params[0], $params[1], $params[2], $params[3], $params[4]);
		$data['tglawal'] = date('Y-m-d', strtotime($params[0]));
		$data['tglakhir'] = date('Y-m-d', strtotime($params[1]));
		$data['barang'] = $params[2];
		$data['gudang'] = $this->LaporanPersediaan_model->Gudang($params[3]);
		// $data['cabang'] = $params[3];
		// var_dump($data);
		// die();

		$this->load->view('menu/reporting/persedian/excelkartustokperbaarang', $data);
	}

	function ExportLaporanKartuStokPerbarangPajak($param = "")
	{
		$params = explode(":", urldecode($param));

		// $data['reportrow'] = $this->LaporanPersediaan_model->GetDataKartuInvoice($params[0], $params[1], $params[2], $params[3], $params[4]);
		$data['report'] = $this->LaporanPersediaan_model->LaporanKartuStokPajak($params[0], $params[1], $params[2], $params[3]);
		$data['kartustok'] = $this->LaporanPersediaan_model->GetDataKartuStok($params[0], $params[1], $params[2], $params[3]);
		// $data['persediaan'] = $this->LaporanPersediaan_model->GetDataKartuPeneriman($params[0], $params[1], $params[2], $params[3], $params[4]);
		$data['tglawal'] = date('Y-m-d', strtotime($params[0]));
		$data['tglakhir'] = date('Y-m-d', strtotime($params[1]));
		$data['barang'] = $params[2];
		$data['gudang'] = $this->LaporanPersediaan_model->Gudang($params[3]);
		// $data['cabang'] = $params[3];
		// var_dump($data);
		// die();

		$this->load->view('menu/reporting/persedian/excelkartustokperbaarang', $data);
	}

	function ExportLaporanKartuStokMerk($param = "")
	{
		$params = explode(":", urldecode($param));

		// $data['reportrow'] = $this->LaporanPersediaan_model->GetDataKartuInvoice($params[0], $params[1], $params[2], $params[3], $params[4]);
		// $data['report'] = $this->LaporanPersediaan_model->GetDataKartuStokMerk1($params[0], $params[1], $params[2], $params[3], $params[4]);
		// $data['kartulist'] = $this->LaporanPersediaan_model->GetDataKartuStokMerk2($params[0], $params[1], $params[2], $params[3], $params[4]);
		// $data['Kartulist2'] = $this->LaporanPersediaan_model->GetDataKartuStokMerk3($params[0], $params[1], $params[2], $params[3], $params[4]);
		$data['kartustok'] = $this->LaporanPersediaan_model->GetDataKartuStokMerk($params[0], $params[1], $params[2], $params[3], $params[4]);
		// $data['persediaan'] = $this->LaporanPersediaan_model->GetDataKartuPeneriman($params[0], $params[1], $params[2], $params[3], $params[4]);
		$data['tglawal'] = date('Y-m-d', strtotime($params[0]));
		$data['tglakhir'] = date('Y-m-d', strtotime($params[1]));
		$data['merkawal'] = $this->LaporanPersediaan_model->GetCabang($params[2]);
		$data['merkakhir'] = $this->LaporanPersediaan_model->GetCabangAkhir($params[3]);
		$data['cabang'] = $this->LaporanPersediaan_model->Cabang($params[4]);
		// $data['cabang'] = $params[3];
		// var_dump($data);
		// die();
		$this->load->view('menu/reporting/persedian/excelkartustokmerk', $data);
	}

	function ExportLaporanKartuStokMerkPajak($param = "")
	{
		$params = explode(":", urldecode($param));

		// $data['reportrow'] = $this->LaporanPersediaan_model->GetDataKartuInvoice($params[0], $params[1], $params[2], $params[3], $params[4]);
		// $data['report'] = $this->LaporanPersediaan_model->GetDataKartuStokMerk1($params[0], $params[1], $params[2], $params[3], $params[4]);
		// $data['kartulist'] = $this->LaporanPersediaan_model->GetDataKartuStokMerk2($params[0], $params[1], $params[2], $params[3], $params[4]);
		// $data['Kartulist2'] = $this->LaporanPersediaan_model->GetDataKartuStokMerk3($params[0], $params[1], $params[2], $params[3], $params[4]);
		$data['kartustok'] = $this->LaporanPersediaan_model->GetDataKartuStokMerkPajak($params[0], $params[1], $params[2], $params[3], $params[4]);
		// $data['persediaan'] = $this->LaporanPersediaan_model->GetDataKartuPeneriman($params[0], $params[1], $params[2], $params[3], $params[4]);
		$data['tglawal'] = date('Y-m-d', strtotime($params[0]));
		$data['tglakhir'] = date('Y-m-d', strtotime($params[1]));
		$data['merkawal'] = $this->LaporanPersediaan_model->GetCabang($params[2]);
		$data['merkakhir'] = $this->LaporanPersediaan_model->GetCabangAkhir($params[3]);
		$data['cabang'] = $this->LaporanPersediaan_model->Cabang($params[4]);
		// $data['cabang'] = $params[3];
		// var_dump($data);
		// die();
		$this->load->view('menu/reporting/persedian/excelkartustokmerkpajak', $data);
	} 
	function ExportLaporanKartuStokMerkPegudang($param = "")
	{
		$params = explode(":", urldecode($param));

		// $data['reportrow'] = $this->LaporanPersediaan_model->GetDataKartuInvoice($params[0], $params[1], $params[2], $params[3], $params[4]);
		// $data['report'] = $this->LaporanPersediaan_model->GetDataKartuStokMerk1($params[0], $params[1], $params[2], $params[3], $params[4]);
		// $data['kartulist'] = $this->LaporanPersediaan_model->GetDataKartuStokMerk2($params[0], $params[1], $params[2], $params[3], $params[4]);
		// $data['Kartulist2'] = $this->LaporanPersediaan_model->GetDataKartuStokMerk3($params[0], $params[1], $params[2], $params[3], $params[4]);
		$data['kartustok'] = $this->LaporanPersediaan_model->GetDataKartuStokMerkPerGudang($params[0], $params[1], $params[2], $params[3], $params[4], $params[5]);
		// $data['persediaan'] = $this->LaporanPersediaan_model->GetDataKartuPeneriman($params[0], $params[1], $params[2], $params[3], $params[4]);
		$data['tglawal'] = date('Y-m-d', strtotime($params[0]));
		$data['tglakhir'] = date('Y-m-d', strtotime($params[1]));
		$data['merkawal'] = $this->LaporanPersediaan_model->GetCabang($params[2]);
		$data['merkakhir'] = $this->LaporanPersediaan_model->GetCabangAkhir($params[3]);
		$data['gudang'] = $this->LaporanPersediaan_model->Gudang($params[4]);
		// $data['cabang'] = $params[3];
		// var_dump($data);
		// die();
		$this->load->view('menu/reporting/persedian/excelkartustokmerkpergudang', $data);
	}



	function PreviewPersediaanBarangAllgudangPerkategori($param = "")
	{
		$params = explode(":", urldecode($param));
		// $tglawal = $params[0];
		// $tglakhir = $params[1];
		// $gudang = $params[2];
		// $merkawal = $params[3];
		// $merkakhir = $params[4];

		$filename = 'Laporan Persediaan Barang ' . '_' . date('d M Y', strtotime($params[0])) . ' - ' . date('d M Y', strtotime($params[1]));


		$paper = 'A4';
		$orientation = 'portrait';

		// menggunakan class dompdf
		// $this->load->library('pdf');

		$data['report'] = $this->LaporanPersediaan_model->GetdataPersedianPerKategori($params[0], $params[1], $params[2], $params[3], $params[4]);
		$data['reportrow'] = $this->LaporanPersediaan_model->GetdataPersedianPerKategoriRow($params[0], $params[1], $params[2], $params[3], $params[4]);
		$data['persediaan'] = $this->LaporanPersediaan_model->GetdataPersedianPerKategorList($params[0], $params[1], $params[2], $params[3], $params[4]);
		// $data['persediaanlist'] = $this->LaporanPersediaan_model->PersediaanAllgudangPerkategoriList($params[0], $params[1], $params[2],$params[3]);
		$data['tglawal'] = date('Y-m-d', strtotime($params[0]));
		$data['tglakhir'] = date('Y-m-d', strtotime($params[1]));
		$data['merkawal'] = $this->LaporanPersediaan_model->GetCabang($params[2]);
		$data['merkakhir'] = $this->LaporanPersediaan_model->GetCabangAkhir($params[3]);
		// var_dump($data);
		// die();

		set_time_limit(300);

		$this->load->view('menu/cetakan_pdf/laporanpersediaanallgudangperkategori', $data, $filename, $paper, $orientation);
	}

	function PreviewPersediaanBarangPergudangAllKategori($param)
	{
		$params = explode(":", urldecode($param));
		// $tglawal = $params[0];
		// $tglakhir = $params[1];
		// $gudang = $params[2];
		// $merkawal = $params[3];
		// $merkakhir = $params[4];

		$filename = 'Laporan Persediaan Barang ' . '_' . date('d M Y', strtotime($params[0])) . ' - ' . date('d M Y', strtotime($params[1]));


		$paper = 'A4';
		$orientation = 'portrait';

		// menggunakan class dompdf
		// $this->load->library('pdf');

		$data['report'] = $this->LaporanPersediaan_model->GetDataPersedianPerGudang($params[0], $params[1], $params[2], $params[3], $params[4], $params[5]);
		// $data['reportrow'] = $this->LaporanPersediaan_model->GetdataPersedianPerKategoriRow($params[0], $params[1], $params[2], $params[3], $params[4], $params[5]);
		$data['persediaan'] = $this->LaporanPersediaan_model->GetDataPersedianPerGudangList($params[0], $params[1], $params[2], $params[3], $params[4], $params[5]);
		// $data['persediaanlist'] = $this->LaporanPersediaan_model->PersediaanAllgudangPerkategoriList($params[0], $params[1], $params[2],$params[3]);
		$data['tglawal'] = date('Y-m-d', strtotime($params[0]));
		$data['tglakhir'] = date('Y-m-d', strtotime($params[1]));
		$data['merkawal'] = $this->LaporanPersediaan_model->GetCabang($params[2]);
		$data['merkakhir'] = $this->LaporanPersediaan_model->GetCabangAkhir($params[3]);
		$data['gudang'] = $params[4];
		// var_dump($data);
		// die();

		set_time_limit(300);

		$this->load->view('menu/cetakan_pdf/laporanpersedianpergudangsemuakategori', $data, $filename, $paper, $orientation);
	}

	function PreviewPersedianBarangSemuaGudangSemuaKategori($param = "")
	{
		$params = explode(":", urldecode($param));
		// $tglawal = $params[0];
		// $tglakhir = $params[1];
		// $gudang = $params[2];
		// $merkawal = $params[3];
		// $merkakhir = $params[4];

		$filename = 'Laporan Persediaan Barang ' . '_' . date('d M Y', strtotime($params[0])) . ' - ' . date('d M Y', strtotime($params[1]));


		$paper = 'A4';
		$orientation = 'portrait';

		// menggunakan class dompdf
		$this->load->library('pdf');

		$data['report'] = $this->LaporanPersediaan_model->GetdataPersedianPerKategori($params[0], $params[1], $params[2], $params[3], $params[4]);
		$data['reportrow'] = $this->LaporanPersediaan_model->GetdataPersedianPerKategoriRow($params[0], $params[1], $params[2], $params[3], $params[4]);
		$data['persediaan'] = $this->LaporanPersediaan_model->GetdataPersedianPerKategorList($params[0], $params[1], $params[2], $params[3], $params[4]);
		$data['tglawal'] = date('Y-m-d', strtotime($params[0]));
		$data['tglakhir'] = date('Y-m-d', strtotime($params[1]));
		$data['merkawal'] = $this->LaporanPersediaan_model->GetCabang($params[2]);
		$data['merkakhir'] = $this->LaporanPersediaan_model->GetCabangAkhir($params[3]);
		$data['cabang'] = $params[4];

		// var_dump($data);
		// die();
		set_time_limit(300);
		$this->load->view('menu/cetakan_pdf/laporanpersediansemuagudangsemuakategori', $data, $filename, $paper, $orientation);
	}

	function PreviewPersedianBarangPergudangSemuaKategori($param)
	{
		$params = explode(":", urldecode($param));
		// $tglawal = $params[0];
		// $tglakhir = $params[1];
		// $gudang = $params[2];
		// $merkawal = $params[3];
		// $merkakhir = $params[4];

		$filename = 'Laporan Persediaan Barang ' . '_' . date('d M Y', strtotime($params[0])) . ' - ' . date('d M Y', strtotime($params[1]));


		$paper = 'A4';
		$orientation = 'portrait';

		// menggunakan class dompdf
		// $this->load->library('pdf');

		$data['report'] = $this->LaporanPersediaan_model->GetdataPersedianPerKategori($params[0], $params[1], $params[2], $params[3]);
		$data['row'] = $this->LaporanPersediaan_model->GetdataPersedianPerKategoriRow($params[0], $params[1], $params[2], $params[3]);
		$data['persediaan'] = $this->LaporanPersediaan_model->GetdataPersedianPerKategorList($params[0], $params[1], $params[2], $params[3]);
		$data['tglawal'] = date('Y-m-d', strtotime($params[0]));
		$data['tglakhir'] = date('Y-m-d', strtotime($params[1]));
		$data['merkawal'] = $this->LaporanPersediaan_model->GetCabang($params[2]);
		$data['merkakhir'] = $this->LaporanPersediaan_model->GetCabangAkhir($params[3]);
		$data['gudang'] = $params[5];
		set_time_limit(300);
		$this->load->view('menu/cetakan_pdf/laporanpersedianpergudangsemuakategori', $data, $filename, $paper, $orientation);
	}
	
	function PreviewCetakKartuStokSemuaBarang($param = "")
	{
		$params = explode(":", urldecode($param));
		// $barang = $params[2];
		// $cabang = $params[3];

		$filename = 'Laporan Kartu Stok Semua Barang ' . '_' . date('d M Y', strtotime($params[0])) . ' - ' . date('d M Y', strtotime($params[1]));


		$paper = 'A4';
		$orientation = 'portrait';

		// menggunakan class dompdf
		// $this->load->library('pdf');

		// $data['reportrow'] = $this->LaporanPersediaan_model->GetDataKartuInvoice($params[0], $params[1], $params[2], $params[3], $params[4]);
		// $data['report'] = $this->LaporanPersediaan_model->GetDataKartuStokMerk1($params[0], $params[1], $params[2], $params[3], $params[4]);
		// $data['kartulist'] = $this->LaporanPersediaan_model->GetDataKartuStokMerk2($params[0], $params[1], $params[2], $params[3], $params[4]);
		// $data['Kartulist2'] = $this->LaporanPersediaan_model->GetDataKartuStokMerk3($params[0], $params[1], $params[2], $params[3], $params[4]);
		$data['kartustok'] = $this->LaporanPersediaan_model->GetDataKartuStokMerk($params[0], $params[1], $params[2], $params[3], $params[4]);
		// $data['persediaan'] = $this->LaporanPersediaan_model->GetDataKartuPeneriman($params[0], $params[1], $params[2], $params[3], $params[4]);
		$data['tglawal'] = date('Y-m-d', strtotime($params[0]));
		$data['tglakhir'] = date('Y-m-d', strtotime($params[1]));
		$data['merkawal'] = $this->LaporanPersediaan_model->GetCabang($params[2]);
		$data['merkakhir'] = $this->LaporanPersediaan_model->GetCabangAkhir($params[3]);
		$data['cabang'] = $this->LaporanPersediaan_model->Cabang($params[4]);
		// $data['cabang'] = $params[3];
		// var_dump($data);
		// die();

		set_time_limit(300);
		$this->load->view('menu/cetakan_pdf/laporankartustoksemua', $data, $filename, $paper, $orientation);
	}

	function PreviewCetakKartuStokSemuaBarangPajak($param = "")
	{
		$params = explode(":", urldecode($param));
		// $barang = $params[2];
		// $cabang = $params[3];

		$filename = 'Laporan Kartu Stok Semua Barang ' . '_' . date('d M Y', strtotime($params[0])) . ' - ' . date('d M Y', strtotime($params[1]));


		$paper = 'A4';
		$orientation = 'portrait';

		// menggunakan class dompdf
		// $this->load->library('pdf');

		// $data['reportrow'] = $this->LaporanPersediaan_model->GetDataKartuInvoice($params[0], $params[1], $params[2], $params[3], $params[4]);
		// $data['report'] = $this->LaporanPersediaan_model->GetDataKartuStokMerk1($params[0], $params[1], $params[2], $params[3], $params[4]);
		// $data['kartulist'] = $this->LaporanPersediaan_model->GetDataKartuStokMerk2($params[0], $params[1], $params[2], $params[3], $params[4]);
		// $data['Kartulist2'] = $this->LaporanPersediaan_model->GetDataKartuStokMerk3($params[0], $params[1], $params[2], $params[3], $params[4]);
		$data['kartustok'] = $this->LaporanPersediaan_model->GetDataKartuStokMerkPajak($params[0], $params[1], $params[2], $params[3], $params[4]);
		// $data['persediaan'] = $this->LaporanPersediaan_model->GetDataKartuPeneriman($params[0], $params[1], $params[2], $params[3], $params[4]);
		$data['tglawal'] = date('Y-m-d', strtotime($params[0]));
		$data['tglakhir'] = date('Y-m-d', strtotime($params[1]));
		$data['merkawal'] = $this->LaporanPersediaan_model->GetCabang($params[2]);
		$data['merkakhir'] = $this->LaporanPersediaan_model->GetCabangAkhir($params[3]);
		$data['cabang'] = $this->LaporanPersediaan_model->Cabang($params[4]);
		// $data['cabang'] = $params[3];
		// var_dump($data);
		// die();

		set_time_limit(300);
		$this->load->view('menu/cetakan_pdf/laporankartustoksemuapajak', $data, $filename, $paper, $orientation);
	}

	function LaporanTransferStokPerGudang($param = "")
	{
		$params = explode(":", urldecode($param));
		// $tglawal = $params[0];
		// $tglakhir = $params[1];
		// $gudang = $params[2];
		// $merkawal = $params[3];
		// $merkakhir = $params[4];

		$filename = 'Laporan Transfer Stok ' . '_' . date('d M Y', strtotime($params[0])) . ' - ' . date('d M Y', strtotime($params[1]));


		$paper = 'A4';
		$orientation = 'portrait';

		// menggunakan class dompdf
		$this->load->library('pdf');

		$data['report'] = $this->LaporanPersediaan_model->LaporanTransferStokPergudang($params[0], $params[1], $params[2]);
		$data['row'] = $this->LaporanPersediaan_model->LaporanTransferStokPergudangRow($params[0], $params[1], $params[2]);
		$data['reportlist'] = $this->LaporanPersediaan_model->LaporanTransferStokPergudangList($params[0], $params[1], $params[2]);
		$data['tglawal'] = date('Y-m-d', strtotime($params[0]));
		$data['tglakhir'] = date('Y-m-d', strtotime($params[1]));
		$data['gudang'] = $params[2];

		$this->pdf->load_view('menu/cetakan_pdf/r_transfer_stok', $data, $filename, $paper, $orientation);

		// (Opsional) Mengatur ukuran kertas dan orientasi kertas
		$this->pdf->setPaper('F4', 'portrait');

		// Menjadikan HTML sebagai PDF
		$this->pdf->render();

		// Output akan menghasilkan PDF (1 = download dan 0 = preview)
		$this->pdf->stream("welcome.pdf", array("Attachment" => 0));

		exit(0);
	}

	function ExportLaporanTransferStokPerGudang($param = "")
	{
		$params = explode(":", urldecode($param));
		$data['report'] = $this->LaporanPersediaan_model->LaporanTransferStokPergudang($params[0], $params[1], $params[2]);
		$data['row'] = $this->LaporanPersediaan_model->LaporanTransferStokPergudangRow($params[0], $params[1], $params[2]);
		$data['reportlist'] = $this->LaporanPersediaan_model->LaporanTransferStokPergudangList($params[0], $params[1], $params[2]);
		$data['tglawal'] = date('Y-m-d', strtotime($params[0]));
		$data['tglakhir'] = date('Y-m-d', strtotime($params[1]));
		$data['gudang'] = $params[2];
		// $data['cabang'] = $params[3];
		// var_dump($data);
		// die();
		$this->load->view('menu/cetakan_excel/excel_transfer_stok', $data);
	}

	function LaporanPenerimaanTransferStokPerGudang($param = "")
	{
		$params = explode(":", urldecode($param));
		// $tglawal = $params[0];
		// $tglakhir = $params[1];
		// $gudang = $params[2];
		// $merkawal = $params[3];
		// $merkakhir = $params[4];

		$filename = 'Laporan Transfer Stok ' . '_' . date('d M Y', strtotime($params[0])) . ' - ' . date('d M Y', strtotime($params[1]));


		$paper = 'A4';
		$orientation = 'portrait';

		// menggunakan class dompdf
		$this->load->library('pdf');

		$data['report'] = $this->LaporanPersediaan_model->LaporanPenerimaanTransferStokPergudang($params[0], $params[1], $params[2]);
		$data['row'] = $this->LaporanPersediaan_model->LaporanPenerimaanTransferStokPergudangRow($params[0], $params[1], $params[2]);
		$data['reportlist'] = $this->LaporanPersediaan_model->LaporanPenerimaanTransferStokPergudangList($params[0], $params[1], $params[2]);
		$data['tglawal'] = date('Y-m-d', strtotime($params[0]));
		$data['tglakhir'] = date('Y-m-d', strtotime($params[1]));
		$data['gudang'] = $params[2];

		$this->pdf->load_view('menu/cetakan_pdf/r_penerimaan_transfer_stok', $data, $filename, $paper, $orientation);

		// (Opsional) Mengatur ukuran kertas dan orientasi kertas
		$this->pdf->setPaper('F4', 'portrait');

		// Menjadikan HTML sebagai PDF
		$this->pdf->render();

		// Output akan menghasilkan PDF (1 = download dan 0 = preview)
		$this->pdf->stream("welcome.pdf", array("Attachment" => 0));

		exit(0);
	}

	function ExportLaporanPenerimaanTransferStokPergudang($param = "")
	{
		$params = explode(":", urldecode($param));
		$data['report'] = $this->LaporanPersediaan_model->LaporanPenerimaanTransferStokPergudang($params[0], $params[1], $params[2]);
		$data['row'] = $this->LaporanPersediaan_model->LaporanPenerimaanTransferStokPergudangRow($params[0], $params[1], $params[2]);
		$data['reportlist'] = $this->LaporanPersediaan_model->LaporanPenerimaanTransferStokPergudangList($params[0], $params[1], $params[2]);
		$data['tglawal'] = date('Y-m-d', strtotime($params[0]));
		$data['tglakhir'] = date('Y-m-d', strtotime($params[1]));
		$data['gudang'] = $params[2];
		// $data['cabang'] = $params[3];
		// var_dump($data);
		// die();
		$this->load->view('menu/cetakan_excel/excel_penerimaan_transfer_stok', $data);
	}
}
