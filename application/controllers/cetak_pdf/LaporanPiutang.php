<?php

class LaporanPiutang extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('laporan/LaporanKeuangan_model');
		$this->load->model('laporan/Laporan_model');
		$this->load->model('Caridataaktif_model');
		$this->load->library('form_validation');
		$this->load->library('session');
	}

	function LaporanPiutangJatuhTempo($param = "")
	{
		$params = explode(":", urldecode($param));
		$tglawal = $params[0];
		$tglakhir = $params[1];
		$cabang = $params[2];

		$filename = "Laporan Piutang Jatuh Tempo" . '_' . date('d M Y', strtotime($params[0])) . ' - ' . date('d M Y', strtotime($params[1]));

		$paper = "A4";
		$orientation = 'portrait';

		// menggunakan class dompdf
		$this->load->library('pdf');

		$data['row'] = $this->LaporanKeuangan_model->LaporanPiutangJatuhTempo($params[0], $params[1], $params[2]);
		$data['report'] = $this->LaporanKeuangan_model->LaporanPiutangJatuhTempoList($params[0], $params[1], $params[2]);
		$data['tglawal'] = date('d-m-Y', strtotime($params[0]));
		$data['tglakhir'] = date('d-m-Y', strtotime($params[1]));
		$data['cabang'] = $params[2];

		$this->pdf->load_view('menu/cetakan_pdf/laporanjatuhtempo', $data, $filename, $paper, $orientation);

		// (Opsional) Mengatur ukuran kertas dan orientasi kertas
		$this->pdf->setPaper('F4', 'portrait');

		// Menjadikan HTML sebagai PDF
		$this->pdf->render();

		// Output akan menghasilkan PDF (1 = download dan 0 = preview)
		$this->pdf->stream("welcome.pdf", array("Attachment" => 0));

		exit(0);
	}

	function LaporanDepositPelanggan($param = "")
	{
		$params = explode(":", urldecode($param));


		$filename = "Laporan Deposit Pelanggan" . '_' . $params[0];

		$paper = "A4";
		$orientation = 'portrait';

		// menggunakan class dompdf
		$this->load->library('pdf');

		$data['report'] = $this->LaporanKeuangan_model->LaporanDepositPelanggan($params[0], $params[1], $params[2]);
		// $data['row'] = $this->LaporanKeuangan_model->GetDataLaporanPiutangRow($params[0], $params[1], $params[2]);
		// $data['row'] = $this->Laporan_model->Cabang($params[2]);
		// $data['row'] = $this->LaporanKeuangan_model->LaporanDepositPelangganRow($params[0]);
		// $data['laporandeposit'] = $this->LaporanKeuangan_model->LaporanDepositPelangganList($params[0]);
		$data['tglawal'] = date('d-m-Y', strtotime($params[1]));
		$data['tglakhir'] = date('d-m-Y', strtotime($params[2]));
		$data['cabang'] = $params[0];

		$this->pdf->load_view('menu/cetakan_pdf/laporandepositpelanggan', $data, $filename, $paper, $orientation);

		// (Opsional) Mengatur ukuran kertas dan orientasi kertas
		$this->pdf->setPaper('F4', 'portrait');

		// Menjadikan HTML sebagai PDF
		$this->pdf->render();

		// Output akan menghasilkan PDF (1 = download dan 0 = preview)
		$this->pdf->stream("welcome.pdf", array("Attachment" => 0));

		exit(0);
	}

	function LaporanPelangganTidakAktif($param = "")
	{
		$params = explode(":", urldecode($param));


		$filename = "Laporan Pelanggan Tidak Aktif" . '_' . $params[0];

		$paper = "A4";
		$orientation = 'portrait';

		// menggunakan class dompdf
		$this->load->library('pdf');

		$data['report'] = $this->LaporanKeuangan_model->LaporanPelangganTidakAktif($params[0]);
		// $data['row'] = $this->LaporanKeuangan_model->GetDataLaporanPiutangRow($params[0], $params[1], $params[2]);
		// $data['row'] = $this->Laporan_model->Cabang($params[2]);
		$data['row'] = $this->LaporanKeuangan_model->LaporanPelangganTidakAktifRow($params[0]);
		// $data['laporandeposit'] = $this->LaporanKeuangan_model->LaporanDepositPelangganList($params[0]);
		// $data['tglawal'] = date('d-m-Y', strtotime($params[0]));
		// $data['tglakhir'] = date('d-m-Y', strtotime($params[1]));
		$data['cabang'] = $params[0];

		$this->pdf->load_view('menu/cetakan_pdf/laporanpelanggantidakaktif', $data, $filename, $paper, $orientation);

		// (Opsional) Mengatur ukuran kertas dan orientasi kertas
		$this->pdf->setPaper('F4', 'portrait');

		// Menjadikan HTML sebagai PDF
		$this->pdf->render();

		// Output akan menghasilkan PDF (1 = download dan 0 = preview)
		$this->pdf->stream("welcome.pdf", array("Attachment" => 0));

		exit(0);
	}

	function LaporanInvoiceOutstanding($param = "")
	{
		$params = explode(":", urldecode($param));
		$filename = "Laporan Invoice Outstanding Periode"  . '_' . date('d M Y', strtotime($params[0])) . ' - ' . date('d M Y', strtotime($params[1]));

		$paper = "A4";
		$orientation = "portrait";

		// Manggunakan Class dompdf
		$this->load->library('pdf');

		$data['report'] = $this->LaporanKeuangan_model->LaporanInvoiceOutstandingList($params[0], $params[1], $params[2]);
		$data['row'] = $this->LaporanKeuangan_model->LaporanInvoiceOutstanding($params[0], $params[1], $params[2]);
		// $data['outstandinglist'] = $this->LaporanKeuangan_model->LaporanOutstandingInvoiceList($params[0], $params[1], $params[2]);
		$data['tglawal'] = date('d-m-Y', strtotime($params[0]));
		$data['tglakhir'] = date('d-m-Y', strtotime($params[1]));
		$data['cabang'] = $params[2];


		$this->pdf->load_view('menu/cetakan_pdf/laporanoutstandinginvoice', $data, $filename, $paper, $orientation);

		// (Opsional) mengatur ukuran kertas dan orientasi kertas
		$this->pdf->setPaper('F4', 'portrait');

		// Menjadikan HTML sebagai PDF
		$this->pdf->render();

		// output akan menghasilkan PDF (1 = download dan 0 = preview)
		$this->pdf->stream("welcome.pdf", array("Attachment" => 0));

		exit(0);
	}

	public function ExportOustandingInvoice($param = "")
	{
		$params = explode(":", urldecode($param));

		$data['report'] = $this->LaporanKeuangan_model->LaporanInvoiceOutstandingList($params[0], $params[1], $params[2]);
		$data['row'] = $this->LaporanKeuangan_model->LaporanInvoiceOutstanding($params[0], $params[1], $params[2]);
		$data['tglawal'] = date('d-m-Y', strtotime($params[0]));
		$data['tglakhir'] = date('d-m-Y', strtotime($params[1]));
		$data['cabang'] = $this->Laporan_model->Cabang($params[2]);
		$this->load->view('menu/cetakan_excel/excel_outstanding_invoice', $data);
	}

	function LaporanJenisPembayaran($param = ""){
		$params = explode(":", urldecode($param));
		$filename = "Laporan Invoice Outstanding Periode"  . '_' . date('d M Y', strtotime($params[0])) . ' - ' . date('d M Y', strtotime($params[1]));

		$paper = "A4";
		$orientation = "portrait";

		// Manggunakan Class dompdf
		$this->load->library('pdf');

		$data['report'] = $this->LaporanKeuangan_model->LaporanJenisPembayaran($params[0], $params[1], $params[2]);
		$data['row'] = $this->LaporanKeuangan_model->LaporanJenisPemabayarKredit($params[0], $params[1], $params[2]);
		// $data['outstandinglist'] = $this->LaporanKeuangan_model->LaporanOutstandingInvoiceList($params[0], $params[1], $params[2]);
		$data['tglawal'] = date('d-m-Y', strtotime($params[0]));
		$data['tglakhir'] = date('d-m-Y', strtotime($params[1]));
		$data['cabang'] = $params[2];
		// print_r($data);
		// die();

		$this->pdf->load_view('menu/cetakan_pdf/laporanjenispembayaran', $data, $filename, $paper, $orientation);

		// (Opsional) mengatur ukuran kertas dan orientasi kertas
		$this->pdf->setPaper('F4', 'portrait');

		// Menjadikan HTML sebagai PDF
		$this->pdf->render();

		// output akan menghasilkan PDF (1 = download dan 0 = preview)
		$this->pdf->stream("welcome.pdf", array("Attachment" => 0));

		exit(0);
	}

	function LaporanKartuPiutangUsaha($param = "")
	{
		$params = explode(":", urldecode($param));
		$filename = "Laporan Kartu Piutang Usaha"  . '_' . date('d M Y', strtotime($params[0])) . ' - ' . date('d M Y', strtotime($params[1]));

		$paper = "A4";
		$orientation = "landscape";

		// Manggunakan Class dompdf
		// $this->load->library('pdf');

		$data['report'] = $this->LaporanKeuangan_model->GetDataKartuPiutangUsaha($params[0], $params[1], $params[2]);
		$data['list'] = $this->LaporanKeuangan_model->GetDataKartuPiutangUsahaList($params[0], $params[1], $params[2]);
		$data['tglawal'] = date('d-m-Y', strtotime($params[0]));
		$data['tglakhir'] = date('d-m-Y', strtotime($params[1]));
		$data['cabang'] = $params[2];
		$data['row'] = $this->Laporan_model->Cabang($params[2]);

		$this->load->view('menu/cetakan_pdf/r_kartu_piutang_usaha', $data, $filename, $paper, $orientation);

		// (Opsional) mengatur ukuran kertas dan orientasi kertas
		// $this->pdf->setPaper('F4', 'landscape');

		// // Menjadikan HTML sebagai PDF
		// $this->pdf->render();

		// // output akan menghasilkan PDF (1 = download dan 0 = preview)
		// $this->pdf->stream("welcome.pdf", array("Attachment" => 0));

		// exit(0);
	}

	function LaporanPiutangPerPelanggan($param = "")
	{
		$params = explode(":", urldecode($param));
		$filename = "Laporan Piutang Per Pelanggan"  . '_' . date('d M Y', strtotime($params[0])) . ' - ' . date('d M Y', strtotime($params[1]));

		$paper = "A4";
		$orientation = "landscape";

		// Manggunakan Class dompdf
		$this->load->library('pdf');

		$data['report'] = $this->LaporanKeuangan_model->GetDataPiutangPerPelanggan($params[0], $params[1], $params[2], $params[3]);
		$data['reportrow'] = $this->LaporanKeuangan_model->GetDataPiutangPerPelangganRow($params[0], $params[1], $params[2], $params[3]);
		$data['tglawal'] = date('d-m-Y', strtotime($params[0]));
		$data['tglakhir'] = date('d-m-Y', strtotime($params[1]));
		$data['cabang'] = $params[2];
		$data['row'] = $this->Laporan_model->Cabang($params[2]);
		$data['pelanggan'] = $this->LaporanKeuangan_model->Pelanggan($params[3]);

		$this->pdf->load_view('menu/cetakan_pdf/r_piutang_per_pelanggan', $data, $filename, $paper, $orientation);

		// (Opsional) mengatur ukuran kertas dan orientasi kertas
		$this->pdf->setPaper('F4', 'landscape');

		// Menjadikan HTML sebagai PDF
		$this->pdf->render();

		// output akan menghasilkan PDF (1 = download dan 0 = preview)
		$this->pdf->stream("welcome.pdf", array("Attachment" => 0));

		exit(0);
	}

	function LaporanDaftarPiutang($param = "")
	{
		$params = explode(":", urldecode($param));
		$filename = "Laporan Daftar Piutang"  . '_' . date('d M Y', strtotime($params[0])) . ' - ' . date('d M Y', strtotime($params[1]));

		$paper = "A4";
		$orientation = "landscape";

		// Manggunakan Class dompdf
		$this->load->library('pdf');

		$data['report'] = $this->LaporanKeuangan_model->GetGataDaftarPiutang($params[0], $params[1], $params[2]);
		$data['reportrow'] = $this->LaporanKeuangan_model->GetGataDaftarPiutangRow($params[0], $params[1], $params[2]);
		$data['tglawal'] = date('d-m-Y', strtotime($params[0]));
		$data['tglakhir'] = date('d-m-Y', strtotime($params[1]));
		$data['cabang'] = $params[2];
		$data['row'] = $this->Laporan_model->Cabang($params[2]);

		$this->pdf->load_view('menu/cetakan_pdf/r_daftar_piutang', $data, $filename, $paper, $orientation);

		// (Opsional) mengatur ukuran kertas dan orientasi kertas
		$this->pdf->setPaper('F4', 'landscape');

		// Menjadikan HTML sebagai PDF
		$this->pdf->render();

		// output akan menghasilkan PDF (1 = download dan 0 = preview)
		$this->pdf->stream("welcome.pdf", array("Attachment" => 0));

		exit(0);
	}

	function LaporanRekapPembayaran($param = "")
	{
		$params = explode(":", urldecode($param));
		$filename = "Laporan Rekap Pembayaran"  . '_' . date('d M Y', strtotime($params[0])) . ' - ' . date('d M Y', strtotime($params[1]));

		$paper = "A4";
		$orientation = "landscape";

		// Manggunakan Class dompdf
		$this->load->library('pdf');

		$data['report'] = $this->LaporanKeuangan_model->GetDatRekapPembayaran($params[0], $params[1], $params[2]);
		$data['list'] = $this->LaporanKeuangan_model->GetDatRekapPembayaranList($params[0], $params[1], $params[2]);
		$data['reportrow'] = $this->LaporanKeuangan_model->GetDatRekapPembayaranRow($params[0], $params[1], $params[2]);
		$data['tglawal'] = date('d-m-Y', strtotime($params[0]));
		$data['tglakhir'] = date('d-m-Y', strtotime($params[1]));
		$data['cabang'] = $params[2];
		$data['row'] = $this->Laporan_model->Cabang($params[2]);

		$this->pdf->load_view('menu/cetakan_pdf/r_rekap_pemabayaran', $data, $filename, $paper, $orientation);

		// (Opsional) mengatur ukuran kertas dan orientasi kertas
		$this->pdf->setPaper('F4', 'landscape');

		// Menjadikan HTML sebagai PDF
		$this->pdf->render();

		// output akan menghasilkan PDF (1 = download dan 0 = preview)
		$this->pdf->stream("welcome.pdf", array("Attachment" => 0));

		exit(0);
	}

	function LaporanRincianPembayaranPerPelanggan($param = "")
	{
		$params = explode(":", urldecode($param));
		$filename = "Laporan Rincian Pembayaran Per Pelanggan"  . '_' . date('d M Y', strtotime($params[0])) . ' - ' . date('d M Y', strtotime($params[1]));

		$paper = "A4";
		$orientation = "landscape";

		// Manggunakan Class dompdf
		$this->load->library('pdf');

		$data['report'] = $this->LaporanKeuangan_model->GetDatPembayaranPerPelanggan($params[0], $params[1], $params[2], $params[3]);
		$data['list'] = $this->LaporanKeuangan_model->GetDatPembayaranPerPelangganList($params[0], $params[1], $params[2], $params[3]);
		$data['reportrow'] = $this->LaporanKeuangan_model->GetDatPembayaranPerPelangganRow($params[0], $params[1], $params[2], $params[3]);
		$data['tglawal'] = date('d-m-Y', strtotime($params[0]));
		$data['tglakhir'] = date('d-m-Y', strtotime($params[1]));
		$data['cabang'] = $params[2];
		$data['row'] = $this->Laporan_model->Cabang($params[2]);
		$data['pelanggan'] = $this->LaporanKeuangan_model->Pelanggan($params[3]);


		$this->pdf->load_view('menu/cetakan_pdf/r_rincian_pembayaran_per_pelanggan', $data, $filename, $paper, $orientation);

		// (Opsional) mengatur ukuran kertas dan orientasi kertas
		$this->pdf->setPaper('F4', 'landscape');

		// Menjadikan HTML sebagai PDF
		$this->pdf->render();

		// output akan menghasilkan PDF (1 = download dan 0 = preview)
		$this->pdf->stream("welcome.pdf", array("Attachment" => 0));

		exit(0);
	}

	function LaporanRincianPembayaran($param = "")
	{
		$params = explode(":", urldecode($param));
		$filename = "Laporan Rincian Pembayaran "  . '_' . date('d M Y', strtotime($params[0])) . ' - ' . date('d M Y', strtotime($params[1]));

		$paper = "A4";
		$orientation = "landscape";

		// Manggunakan Class dompdf
		$this->load->library('pdf');

		$data['report'] = $this->LaporanKeuangan_model->GetDatPembayaran($params[0], $params[1], $params[2]);
		$data['list'] = $this->LaporanKeuangan_model->GetDatPembayaranList($params[0], $params[1], $params[2]);
		$data['reportrow'] = $this->LaporanKeuangan_model->GetDatPembayaranRow($params[0], $params[1], $params[2]);
		$data['tglawal'] = date('d-m-Y', strtotime($params[0]));
		$data['tglakhir'] = date('d-m-Y', strtotime($params[1]));
		$data['cabang'] = $params[2];
		$data['row'] = $this->Laporan_model->Cabang($params[2]);

		$this->pdf->load_view('menu/cetakan_pdf/r_rincian_pembayaran', $data, $filename, $paper, $orientation);

		// (Opsional) mengatur ukuran kertas dan orientasi kertas
		$this->pdf->setPaper('F4', 'landscape');

		// Menjadikan HTML sebagai PDF
		$this->pdf->render();

		// output akan menghasilkan PDF (1 = download dan 0 = preview)
		$this->pdf->stream("welcome.pdf", array("Attachment" => 0));

		exit(0);
	}

	function ExportPiutangJatuhTempo($param = ""){
		$params = explode(":", urldecode($param));

		// $data['report'] = $this->LaporanKeuangan_model->GetDataLaporanPiutang($params[0], $params[1], $params[2]);
		// $data['row'] = $this->LaporanKeuangan_model->GetDataLaporanPiutangRow($params[0], $params[1], $params[2]);
		// $data['row'] = $this->Laporan_model->Cabang($params[2]);
		$data['row'] = $this->LaporanKeuangan_model->LaporanPiutangJatuhTempo($params[0], $params[1], $params[2]);
		$data['report'] = $this->LaporanKeuangan_model->LaporanPiutangJatuhTempoList($params[0], $params[1], $params[2]);
		$data['tglawal'] = date('d-m-Y', strtotime($params[0]));
		$data['tglakhir'] = date('d-m-Y', strtotime($params[1]));
		$data['cabang'] = $params[2];

		$this->load->view('menu/reporting/keuangan/excelpiutangjatuhtempo', $data);
	}

	public function ExportKartuPiutangUsaha($param = "")
	{
		$params = explode(":", urldecode($param));

		$data['report'] = $this->LaporanKeuangan_model->GetDataKartuPiutangUsaha($params[0], $params[1], $params[2]);
		$data['list'] = $this->LaporanKeuangan_model->GetDataKartuPiutangUsahaList($params[0], $params[1], $params[2]);
		$data['tglawal'] = date('d-m-Y', strtotime($params[0]));
		$data['tglakhir'] = date('d-m-Y', strtotime($params[1]));
		$data['cabang'] = $params[2];
		$data['row'] = $this->Laporan_model->Cabang($params[2]);
		$this->load->view('menu/cetakan_excel/excel_kartu_piutang_usaha', $data);
	}

	public function ExportPiutangPerPelanggan($param = "")
	{
		$params = explode(":", urldecode($param));

		$data['report'] = $this->LaporanKeuangan_model->GetDataPiutangPerPelanggan($params[0], $params[1], $params[2], $params[3]);
		$data['reportrow'] = $this->LaporanKeuangan_model->GetDataPiutangPerPelangganRow($params[0], $params[1], $params[2], $params[3]);
		$data['tglawal'] = date('d-m-Y', strtotime($params[0]));
		$data['tglakhir'] = date('d-m-Y', strtotime($params[1]));
		$data['cabang'] = $params[2];
		$data['row'] = $this->Laporan_model->Cabang($params[2]);
		$this->load->view('menu/cetakan_excel/excel_piutang_per_pelanggan', $data);
	}

	public function ExportDaftarPiutang($param = "")
	{
		$params = explode(":", urldecode($param));

		$data['report'] = $this->LaporanKeuangan_model->GetGataDaftarPiutang($params[0], $params[1], $params[2]);
		$data['reportrow'] = $this->LaporanKeuangan_model->GetGataDaftarPiutangRow($params[0], $params[1], $params[2]);
		$data['tglawal'] = date('d-m-Y', strtotime($params[0]));
		$data['tglakhir'] = date('d-m-Y', strtotime($params[1]));
		$data['cabang'] = $params[2];
		$data['row'] = $this->Laporan_model->Cabang($params[2]);
		$this->load->view('menu/cetakan_excel/excel_daftar_piutang', $data);
	}

	public function ExportDepositPelanggan($param = "")
	{
		$params = explode(":", urldecode($param));

		$data['report'] = $this->LaporanKeuangan_model->LaporanDepositPelanggan($params[0], $params[1], $params[2]);
		// $data['reportrow'] = $this->LaporanKeuangan_model->GetGataDaftarPiutangRow($params[0], $params[1], $params[2]);
		$data['tglawal'] = date('d-m-Y', strtotime($params[1]));
		$data['tglakhir'] = date('d-m-Y', strtotime($params[2]));
		$data['cabang'] = $params[0];
		$data['row'] = $this->Laporan_model->Cabang($params[0]);
		$this->load->view('menu/cetakan_excel/excel_deposit_pelanggan', $data);
	}

	public function ExportRekapPembayaran($param = "")
	{
		$params = explode(":", urldecode($param));

		$data['report'] = $this->LaporanKeuangan_model->GetDatRekapPembayaran($params[0], $params[1], $params[2]);
		$data['list'] = $this->LaporanKeuangan_model->GetDatRekapPembayaranList($params[0], $params[1], $params[2]);
		$data['reportrow'] = $this->LaporanKeuangan_model->GetDatRekapPembayaranRow($params[0], $params[1], $params[2]);
		$data['tglawal'] = date('d-m-Y', strtotime($params[0]));
		$data['tglakhir'] = date('d-m-Y', strtotime($params[1]));
		$data['cabang'] = $params[2];
		$data['row'] = $this->Laporan_model->Cabang($params[2]);
		$this->load->view('menu/cetakan_excel/excel_rekap_pembayaran', $data);
	}

	public function ExportRincianPembayaranPerPelanggan($param = "")
	{
		$params = explode(":", urldecode($param));

		$data['report'] = $this->LaporanKeuangan_model->GetDatPembayaranPerPelanggan($params[0], $params[1], $params[2], $params[3]);
		$data['list'] = $this->LaporanKeuangan_model->GetDatPembayaranPerPelangganList($params[0], $params[1], $params[2], $params[3]);
		$data['reportrow'] = $this->LaporanKeuangan_model->GetDatPembayaranPerPelangganRow($params[0], $params[1], $params[2], $params[3]);
		$data['tglawal'] = date('d-m-Y', strtotime($params[0]));
		$data['tglakhir'] = date('d-m-Y', strtotime($params[1]));
		$data['cabang'] = $params[2];
		$data['row'] = $this->Laporan_model->Cabang($params[2]);
		$this->load->view('menu/cetakan_excel/excel_rincian_pembayaran_per_pelanggan', $data);
	}
	public function ExportRincianPembayaran($param = "")
	{
		$params = explode(":", urldecode($param));

		$data['report'] = $this->LaporanKeuangan_model->GetDatPembayaran($params[0], $params[1], $params[2]);
		$data['list'] = $this->LaporanKeuangan_model->GetDatPembayaranList($params[0], $params[1], $params[2]);
		$data['reportrow'] = $this->LaporanKeuangan_model->GetDatPembayaranRow($params[0], $params[1], $params[2]);
		$data['tglawal'] = date('d-m-Y', strtotime($params[0]));
		$data['tglakhir'] = date('d-m-Y', strtotime($params[1]));
		$data['cabang'] = $params[2];
		$data['row'] = $this->Laporan_model->Cabang($params[2]);
		$this->load->view('menu/cetakan_excel/excel_rincian_pembayaran', $data);
	}
	function PreviewLaporanPiutangJatuhTempo($param = "")
	{
		$params = explode(":", urldecode($param));
		$tglawal = $params[0];
		$tglakhir = $params[1];
		$cabang = $params[2];

		$filename = "Laporan Piutang Jatuh Tempo" . '_' . date('d M Y', strtotime($params[0])) . ' - ' . date('d M Y', strtotime($params[1]));

		$paper = "A4";
		$orientation = 'portrait';

		// menggunakan class dompdf
		// $this->load->library('pdf');

		// $data['report'] = $this->LaporanKeuangan_model->GetDataLaporanPiutang($params[0], $params[1], $params[2]);
		// $data['row'] = $this->LaporanKeuangan_model->GetDataLaporanPiutangRow($params[0], $params[1], $params[2]);
		// $data['row'] = $this->Laporan_model->Cabang($params[2]);
		$data['row'] = $this->LaporanKeuangan_model->LaporanPiutangJatuhTempo($params[0], $params[1], $params[2]);
		$data['report'] = $this->LaporanKeuangan_model->LaporanPiutangJatuhTempoList($params[0], $params[1], $params[2]);
		$data['tglawal'] = date('d-m-Y', strtotime($params[0]));
		$data['tglakhir'] = date('d-m-Y', strtotime($params[1]));
		$data['cabang'] = $params[2];

		$this->load->view('menu/cetakan_pdf/laporanjatuhtempo', $data, $filename, $paper, $orientation);

		// 
	}

	function PreviewLaporanDepositPelanggan($param = ""){
		$params = explode(":", urldecode($param));
		

		$filename = "Laporan Deposit Pelanggan" . '_' . $params[0];

		$paper = "A4";
		$orientation = 'portrait';

		// menggunakan class dompdf
		// $this->load->library('pdf');

		$data['report'] = $this->LaporanKeuangan_model->LaporanDepositPelanggan($params[0], $params[1], $params[2]);
		// $data['row'] = $this->LaporanKeuangan_model->GetDataLaporanPiutangRow($params[0], $params[1], $params[2]);
		// $data['row'] = $this->Laporan_model->Cabang($params[2]);
		$data['row'] = $this->LaporanKeuangan_model->LaporanDepositPelanggan($params[0], $params[1], $params[2]);
		$data['laporandeposit'] = $this->LaporanKeuangan_model->LaporanDepositPelanggan($params[0], $params[1], $params[2]);
		// $data['tglawal'] = date('d-m-Y', strtotime($params[0]));
		// $data['tglakhir'] = date('d-m-Y', strtotime($params[1]));
		$data['cabang'] = $params[0];

		$this->load->view('menu/cetakan_pdf/laporandepositpelanggan', $data, $filename, $paper, $orientation);
	}

	function PreviewLaporanInvoiceOutstanding($param = ""){
		$params = explode(":", urldecode($param));
		$filename = "Laporan Invoice Outstanding Periode"  . '_' . date('d M Y', strtotime($params[0])) . ' - ' . date('d M Y', strtotime($params[1]));

		$paper = "A4";
		$orientation = "portrait";

		// Manggunakan Class dompdf
		// $this->load->library('pdf');

		$data['report'] = $this->LaporanKeuangan_model->LaporanInvoiceOutstandingList($params[0], $params[1], $params[2]);
		$data['row'] = $this->LaporanKeuangan_model->LaporanInvoiceOutstanding($params[0], $params[1], $params[2]);
		// $data['outstandinglist'] = $this->LaporanKeuangan_model->LaporanOutstandingInvoiceList($params[0], $params[1], $params[2]);
		$data['tglawal'] = date('d-m-Y', strtotime($params[0]));
		$data['tglakhir'] = date('d-m-Y', strtotime($params[1]));
		$data['cabang'] = $params[2];

		set_time_limit(300);

		$this->load->view('menu/cetakan_pdf/laporanoutstandinginvoice', $data, $filename, $paper, $orientation);
	}
}
