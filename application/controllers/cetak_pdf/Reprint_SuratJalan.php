<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Reprint_SuratJalan extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('caridataaktif_model');
		$this->load->library('form_validation');
		$this->load->library('session');
	}

	public function Print($param = "")
	{
		$params = explode(":", urldecode($param));
		// print_r($params);
		// die();
		$paper = array(0, 0, 612, 396);
		$data['sj'] = $this->db->query("SELECT * FROM srvt_suratjalan WHERE nomor = '" . $params[0] . "'")->row();

		$data['vsj'] = $this->db->query("SELECT
		sj.nomor AS nomorsj,
		so.nomor AS nomorso,
		inv.nomor AS noinvoice,
		sj.kodecabang,
		inv.nomorgb,
		so.kodesalesman,
		so.lokasi,
		c.nama AS namacustomer,
		c.nama_toko,
		c.alamat,
		c.pengiriman,
		c.kodepos as kode_pos,
		c.rekeningtf,
		pos.kodepos,
		c.nohp,
		c.notelp,
		pos.kota,
		hist.revisike,
		inv.tanggal,
		inv.reprint,
		a.namabisnis,
		kc.nama as namacabang
		FROM srvt_suratjalan sj
		LEFT JOIN srvt_invoice inv ON inv.nomor = sj.noinvoice
		LEFT JOIN hist_cancel hist ON hist.noinvoice = inv.nomor
		LEFT JOIN srvt_salesorder so ON so.nomor = inv.nomorso
		LEFT JOIN glbm_customer c ON c.nomor = inv.nopelanggan
		LEFT JOIN glbm_kodepos pos ON pos.kodepos = c.kodepos
		left join glbm_account a on a.nomor = c.rekeningtf
		LEFT JOIN glbm_cabang kc on kc.kode = sj.kodecabang
		WHERE sj.nomor = '" . $params[0] . "' AND sj.nomorgb = '".$params[1]."'
		ORDER BY hist.revisike DESC
		LIMIT 1")->row();

		$data['vsjd'] = $this->db->query("SELECT
		sj.nomor AS nomorsj,
		sjd.namabarang,
		sjd.qty,
		s.nama as namasatuan
		FROM srvt_suratjalandetail sjd
		LEFT JOIN srvt_suratjalan sj ON sj.nomor = sjd.nomor AND sj.noinvoice = sjd.noreferensi
		LEFT JOIN glbm_barang brg ON brg.kode = sjd.kodebarang
		LEFT JOIN glbm_satuan s ON s.kode = brg.kodesatuan
		WHERE sjd.nomor = '" . $params[0] . "' AND sjd.nomorgb = '".$params[1]."'
		ORDER BY sjd.namabarang ASC")->result();

		$orientation = 'portrait';
		$filename = 'Surat Jalan - ' . $data['sj']->nomor;

		$this->load->library('pdf');
		$this->pdf->load_view('menu/cetakan_pdf/reprint_surat_jalan', $data, $filename, $paper, $orientation);

		$this->pdf->setPaper($paper, 'portrait');
		$this->pdf->rander();
		$this->pdf->stream("welcome.pdf", array("Attachment" => 0));

		exit(0);
	}
}
