<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Laporan extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('laporan/Laporan_model');
		$this->load->model('Caridataaktif_model');
		$this->load->library('form_validation');
		$this->load->library('session');
	}

	public function ReturPerTanggal($param = "")
	{
		$params = explode(":", urldecode($param));

		$filename = 'Laporan Retur Penjualan Per Tanggal' . '_' . date('d M Y', strtotime($params[0])) . ' - ' . date('d M Y', strtotime($params[1]));

		// if (!empty($tanggal)){

		$paper = 'A4';
		$orientation = 'landscape';

		// menggunakan class dompdf
		$this->load->library('pdf');

		$data['report'] = $this->Laporan_model->GetDataReturPenjualanPerTanggal($params[0], $params[1], $params[2]);
		$data['row'] = $this->Laporan_model->Cabang($params[2]);
		$data['reportlist'] = $this->Laporan_model->GetDataReturPenjualanPerTanggalList($params[0], $params[1], $params[2]);
		$data['tglawal'] = date('d-m-Y', strtotime($params[0]));
		$data['tglakhir'] = date('d-m-Y', strtotime($params[1]));
		$data['cabang'] = $params[2];

		$this->pdf->load_view('menu/cetakan_pdf/penjualan', $data, $filename, $paper, $orientation);

		// (Opsional) Mengatur ukuran kertas dan orientasi kertas
		$this->pdf->setPaper('F4', 'landscape');

		// Menjadikan HTML sebagai PDF
		$this->pdf->render();

		// Output akan menghasilkan PDF (1 = download dan 0 = preview)
		$this->pdf->stream("welcome.pdf", array("Attachment" => 0));

		exit(0);
		// }
	}

	public function LaporanRekapPenjualan($param = "")
	{
		$params = explode(":", urldecode($param));

		$filename = 'Laporan Rekap Penjualan' . '_' . date('d M Y', strtotime($params[0])) . ' - ' . date('d M Y', strtotime($params[1]));

		// if (!empty($tanggal)){

		$paper = 'A4';
		$orientation = 'landscape';

		// menggunakan class dompdf
		$this->load->library('pdf');

		// $data['report'] = $this->Laporan_model->LaporanPenjualanDetail($params[0], $params[1], $params[2]);
		$data['reportrow'] = $this->Laporan_model->LaporanRekapPenjualan($params[0], $params[1], $params[2]);
		$data['reportlist'] = $this->Laporan_model->LaporanRekapPenjualanList($params[0], $params[1], $params[2]);
		$data['konfigurasi'] = $this->Laporan_model->Konfigurasi();
		$data['row'] = $this->Laporan_model->Cabang($params[2]);
		$data['tglawal'] = date('d-m-Y', strtotime($params[0]));
		$data['tglakhir'] = date('d-m-Y', strtotime($params[1]));
		$data['cabang'] = $params[2];

		$this->pdf->load_view('menu/cetakan_pdf/r_rekap_penjualan', $data, $filename, $paper, $orientation);

		// (Opsional) Mengatur ukuran kertas dan orientasi kertas
		$this->pdf->setPaper('F4', 'landscape');

		// Menjadikan HTML sebagai PDF
		$this->pdf->render();

		// Output akan menghasilkan PDF (1 = download dan 0 = preview)
		$this->pdf->stream("welcome.pdf", array("Attachment" => 0));

		exit(0);
		// }
	}

	public function PenjualanSemuaSalesman($param = "")
	{
		$params = explode(":", urldecode($param));

		$filename = 'Laporan Penjualan Semua Salesman' . '_' . date('d M Y', strtotime($params[0])) . ' - ' . date('d M Y', strtotime($params[1]));

		// if (!empty($tanggal)){

		$paper = 'A4';
		$orientation = 'landscape';

		// menggunakan class dompdf
		$this->load->library('pdf');

		$data['report'] = $this->Laporan_model->PenjualanSemuaSalesman($params[0], $params[1], $params[2]);
		$data['row'] = $this->Laporan_model->Cabang($params[2]);
		$data['reportlist'] = $this->Laporan_model->PenjualanSemuaSalesmanList($params[0], $params[1], $params[2]);
		$data['tglawal'] = date('d-m-Y', strtotime($params[0]));
		$data['tglakhir'] = date('d-m-Y', strtotime($params[1]));
		$data['cabang'] = $params[2];

		$this->pdf->load_view('menu/cetakan_pdf/r_penjualan_semua_salesman', $data, $filename, $paper, $orientation);

		// (Opsional) Mengatur ukuran kertas dan orientasi kertas
		$this->pdf->setPaper('F4', 'landscape');

		// Menjadikan HTML sebagai PDF
		$this->pdf->render();

		// Output akan menghasilkan PDF (1 = download dan 0 = preview)
		$this->pdf->stream("welcome.pdf", array("Attachment" => 0));

		exit(0);
		// }
	}

	public function PenjualanPerSalesman($param = "")
	{
		$params = explode(":", urldecode($param));

		$filename = 'Laporan Penjualan Per Salesman' . '_' . date('d M Y', strtotime($params[0])) . ' - ' . date('d M Y', strtotime($params[1]));

		// if (!empty($tanggal)){

		$paper = 'A4';
		$orientation = 'landscape';

		// menggunakan class dompdf
		$this->load->library('pdf');

		$data['report'] = $this->Laporan_model->PenjualanPerSalesman($params[0], $params[1], $params[2], $params[3]);
		$data['reportrow'] = $this->Laporan_model->PenjualanPerSalesmanRow($params[0], $params[1], $params[2], $params[3]);
		$data['row'] = $this->Laporan_model->Cabang($params[2]);
		$data['reportlist'] = $this->Laporan_model->PenjualanPerSalesmanList($params[0], $params[1], $params[2], $params[3]);
		$data['tglawal'] = date('d-m-Y', strtotime($params[0]));
		$data['tglakhir'] = date('d-m-Y', strtotime($params[1]));
		$data['cabang'] = $params[2];

		$this->pdf->load_view('menu/cetakan_pdf/r_penjualan_per_salesman', $data, $filename, $paper, $orientation);

		// (Opsional) Mengatur ukuran kertas dan orientasi kertas
		$this->pdf->setPaper('F4', 'landscape');

		// Menjadikan HTML sebagai PDF
		$this->pdf->render();

		// Output akan menghasilkan PDF (1 = download dan 0 = preview)
		$this->pdf->stream("welcome.pdf", array("Attachment" => 0));

		exit(0);
		// }
	}

	public function RekapPenjualanPerPelanggan($param = "")
	{
		$params = explode(":", urldecode($param));

		$filename = 'Laporan Penjualan Per Pelanggan' . '_' . date('d M Y', strtotime($params[0])) . ' - ' . date('d M Y', strtotime($params[1]));

		// if (!empty($tanggal)){

		$paper = 'A4';
		$orientation = 'portrait';

		// menggunakan class dompdf
		$this->load->library('pdf');

		$data['rekappenjualanperpelanggan'] = $this->Laporan_model->RekapPenjualanPerPelanggan($params[0], $params[1], $params[2], $params[3]);
		$data['rekappenjualanperpelangganlist'] = $this->Laporan_model->RekapPenjualanPerPelangganListPdf($params[0], $params[1], $params[2], $params[3]);
		$data['row'] = $this->Laporan_model->Cabang($params[3]);
		$data['rekappenjualanperpelangganrow'] = $this->Laporan_model->RekapPenjualanPerPelangganRow($params[0], $params[1], $params[2], $params[3]);
		$data['tglawal'] = date('d-m-Y', strtotime($params[0]));
		$data['ppn'] = $this->Laporan_model->Konfigurasi();
		$data['tglakhir'] = date('d-m-Y', strtotime($params[1]));

		$this->pdf->load_view('menu/cetakan_pdf/r_rekap_penjualan_per_pelanggan', $data, $filename, $paper, $orientation);

		// (Opsional) Mengatur ukuran kertas dan orientasi kertas
		$this->pdf->setPaper('F4', 'portrait');

		// Menjadikan HTML sebagai PDF
		$this->pdf->render();

		// Output akan menghasilkan PDF (1 = download dan 0 = preview)
		$this->pdf->stream("welcome.pdf", array("Attachment" => 0));

		exit(0);
		// }
	}

	public function ExportPenjualanPerPelanggan($param = "")
	{
		$params = explode(":", urldecode($param));
		$tglawal = $params[0];
		$tglakhir = $params[1];
		$nopelanggan = $params[2];
		$cabang = $params[3];

		$data['penjualanperpelanggan'] = $this->Laporan_model->GetDataPenjualanPerPelanggan($params[0], $params[1], $params[2], $params[3]);
		$data['penjualanperpelangganrow'] = $this->Laporan_model->GetDataPenjualanPerPelangganRow($params[0], $params[1], $params[2], $params[3]);
		$data['tglawal'] = date('d-m-Y', strtotime($params[0]));
		$data['tglakhir'] = date('d-m-Y', strtotime($params[1]));
		$this->load->view('menu/reporting/penjualan/excel_penjualan_pelanggan', $data);
	}

	public function ExportRekapPenjualanPerPelanggan($param = "")
	{
		$params = explode(":", urldecode($param));
		$tglawal = $params[0];
		$tglakhir = $params[1];
		$nopelanggan = $params[2];
		$cabang = $params[3];

		$data['rekappenjualanperpelanggan'] = $this->Laporan_model->RekapPenjualanPerPelanggan($params[0], $params[1], $params[2], $params[3]);
		$data['rekappenjualanperpelangganlist'] = $this->Laporan_model->RekapPenjualanPerPelangganList($params[0], $params[1], $params[2], $params[3]);
		$data['rekappenjualanperpelangganrow'] = $this->Laporan_model->RekapPenjualanPerPelangganRow($params[0], $params[1], $params[2], $params[3]);
		$data['tglawal'] = date('d-m-Y', strtotime($params[0]));
		$data['ppn'] = $this->Laporan_model->Konfigurasi();
		$data['tglakhir'] = date('d-m-Y', strtotime($params[1]));
		$this->load->view('menu/reporting/penjualan/excel_rekap_penjualan_per_pelanggan', $data);
	}

	public function PenjualanPerPelanggan($param = "")
	{
		$params = explode(":", urldecode($param));
		$tglawal = $params[0];
		$tglakhir = $params[1];
		$nopelanggan = $params[2];
		$cabang = $params[3];

		$filename = 'Laporan Rekap Penjualan Per Pelanggan' . '_' . date('d M Y', strtotime($params[0])) . ' - ' . date('d M Y', strtotime($params[1]));

		// if (!empty($tanggal)){

		$paper = 'A4';
		$orientation = 'landscape';

		// menggunakan class dompdf
		$this->load->library('pdf');

		$data['penjualanperpelanggan'] = $this->Laporan_model->GetDataPenjualanPerPelanggan($params[0], $params[1], $params[2], $params[3]);
		$data['penjualanperpelangganrow'] = $this->Laporan_model->GetDataPenjualanPerPelangganRow($params[0], $params[1], $params[2], $params[3]);
		$data['penjualanperpelangganlist'] = $this->Laporan_model->GetDataPenjualanPerPelangganList($params[0], $params[1], $params[2], $params[3]);
		$data['cabang'] = $this->Laporan_model->Cabang($params[3]);
		$data['tglawal'] = date('d-m-Y', strtotime($params[0]));
		$data['tglakhir'] = date('d-m-Y', strtotime($params[1]));
		$data['cabang'] = $this->Laporan_model->Cabang($params[3]);
		$data['nopelanggan'] = $this->Laporan_model->Pelanggan($params[2]);

		$this->pdf->load_view('menu/cetakan_pdf/r_penjualan_per_pelanggan', $data, $filename, $paper, $orientation);

		// (Opsional) Mengatur ukuran kertas dan orientasi kertas
		$this->pdf->setPaper('F4', 'landscape');

		// Menjadikan HTML sebagai PDF
		$this->pdf->render();

		// Output akan menghasilkan PDF (1 = download dan 0 = preview)
		$this->pdf->stream("welcome.pdf", array("Attachment" => 0));

		exit(0);
		// }
	}

	public function RekapPenjualanPelanggan($param = "")
	{
		$params = explode(":", urldecode($param));

		$filename = 'Laporan Rekap Penjualan Pelanggan' . '_' . date('d M Y', strtotime($params[0])) . ' - ' . date('d M Y', strtotime($params[1]));

		// if (!empty($tanggal)){

		$paper = 'A4';
		$orientation = 'landscape';

		// menggunakan class dompdf
		$this->load->library('pdf');

		$data['report'] = $this->Laporan_model->RekapPenjualanPelanggan($params[0], $params[1], $params[2]);
		$data['ppn'] = $this->Laporan_model->Konfigurasi();
		// $data['penjualanperpelangganrow'] = $this->Laporan_model->GetDataPenjualanPerPelangganRow($params[0], $params[1], $params[2], $params[3]);
		// $data['penjualanperpelangganlist'] = $this->Laporan_model->GetDataPenjualanPerPelangganList($params[0], $params[1], $params[2], $params[3]);
		$data['cabang'] = $this->Laporan_model->Cabang($params[2]);
		$data['tglawal'] = date('d-m-Y', strtotime($params[0]));
		$data['tglakhir'] = date('d-m-Y', strtotime($params[1]));

		$this->pdf->load_view('menu/cetakan_pdf/r_rekap_penjualan_pelanggan', $data, $filename, $paper, $orientation);

		// (Opsional) Mengatur ukuran kertas dan orientasi kertas
		$this->pdf->setPaper('F4', 'landscape');

		// Menjadikan HTML sebagai PDF
		$this->pdf->render();

		// Output akan menghasilkan PDF (1 = download dan 0 = preview)
		$this->pdf->stream("welcome.pdf", array("Attachment" => 0));

		exit(0);
		// }
	}

	public function SalesOrder($param = "")
	{
		$params = explode(":", urldecode($param));

		$filename = 'Laporan Sales Order' . '_' . date('d M Y', strtotime($params[0])) . ' - ' . date('d M Y', strtotime($params[1]));

		// if (!empty($tanggal)){

		$paper = 'A4';
		$orientation = 'landscape';

		// menggunakan class dompdf
		$this->load->library('pdf');

		$data['report'] = $this->Laporan_model->GetDataSalesOrder($params[0], $params[1], $params[2]);
		$data['row'] = $this->Laporan_model->Cabang($params[2]);
		$data['tglawal'] = date('d-m-Y', strtotime($params[0]));
		$data['tglakhir'] = date('d-m-Y', strtotime($params[1]));
		$data['cabang'] = $params[2];

		$this->pdf->load_view('menu/cetakan_pdf/r_sales_order', $data, $filename, $paper, $orientation);

		// (Opsional) Mengatur ukuran kertas dan orientasi kertas
		$this->pdf->setPaper('F4', 'landscape');

		// Menjadikan HTML sebagai PDF
		$this->pdf->render();

		// Output akan menghasilkan PDF (1 = download dan 0 = preview)
		$this->pdf->stream("welcome.pdf", array("Attachment" => 0));

		exit(0);
		// }
	}

	public function PreviewSalesOrder($param = "")
	{
		$params = explode(":", urldecode($param));

		$filename = 'Laporan Sales Order' . '_' . date('d M Y', strtotime($params[0])) . ' - ' . date('d M Y', strtotime($params[1]));

		// if (!empty($tanggal)){

		$paper = 'A4';
		$orientation = 'landscape';

		// menggunakan class dompdf
		// $this->load->library('pdf');

		$data['report'] = $this->Laporan_model->GetDataSalesOrder($params[0], $params[1], $params[2]);
		$data['row'] = $this->Laporan_model->Cabang($params[2]);
		$data['tglawal'] = date('d-m-Y', strtotime($params[0]));
		$data['tglakhir'] = date('d-m-Y', strtotime($params[1]));
		$data['cabang'] = $params[2];

		$this->load->view('menu/cetakan_pdf/r_sales_order', $data, $filename, $paper, $orientation);

		// // (Opsional) Mengatur ukuran kertas dan orientasi kertas
		// $this->pdf->setPaper('F4', 'landscape');

		// // Menjadikan HTML sebagai PDF
		// $this->pdf->render();

		// // Output akan menghasilkan PDF (1 = download dan 0 = preview)
		// $this->pdf->stream("welcome.pdf", array("Attachment" => 0));

		// exit(0);
		// }
	}

	public function SalesOrderDetail($param = "")
	{
		$params = explode(":", urldecode($param));

		$filename = 'Laporan Sales Order Detail' . '_' . date('d M Y', strtotime($params[0])) . ' - ' . date('d M Y', strtotime($params[1]));

		// if (!empty($tanggal)){

		$paper = 'A4';
		$orientation = 'landscape';

		// menggunakan class dompdf
		$this->load->library('pdf');

		$data['reportrow'] = $this->Laporan_model->GetDataSalesOrderHeader($params[0], $params[1], $params[2]);
		$data['report'] = $this->Laporan_model->GetDataSalesOrderList($params[0], $params[1], $params[2]);
		$data['detail'] = $this->Laporan_model->GetDataSalesOrderDetail($params[0], $params[1], $params[2]);
		$data['konfigurasi'] = $this->Laporan_model->Konfigurasi();
		$data['row'] = $this->Laporan_model->Cabang($params[2]);
		$data['tglawal'] = date('d-m-Y', strtotime($params[0]));
		$data['tglakhir'] = date('d-m-Y', strtotime($params[1]));
		$data['cabang'] = $params[2];

		set_time_limit(300);

		$this->pdf->load_view('menu/cetakan_pdf/r_sales_order_detail', $data, $filename, $paper, $orientation);

		// (Opsional) Mengatur ukuran kertas dan orientasi kertas
		$this->pdf->setPaper('F4', 'landscape');

		// Menjadikan HTML sebagai PDF
		$this->pdf->render();

		// Output akan menghasilkan PDF (1 = download dan 0 = preview)
		$this->pdf->stream("welcome.pdf", array("Attachment" => 1));

		exit(0);
		// }
	}

	public function PreviewSalesOrderDetail($param = "")
	{
		$params = explode(":", urldecode($param));

		$filename = 'Laporan Sales Order Detail' . '_' . date('d M Y', strtotime($params[0])) . ' - ' . date('d M Y', strtotime($params[1]));

		// if (!empty($tanggal)){

		$paper = 'A4';
		$orientation = 'landscape';

		// menggunakan class dompdf
		// $this->load->library('pdf');

		$data['reportrow'] = $this->Laporan_model->GetDataSalesOrderHeader($params[0], $params[1], $params[2]);
		$data['report'] = $this->Laporan_model->GetDataSalesOrderList($params[0], $params[1], $params[2]);
		$data['detail'] = $this->Laporan_model->GetDataSalesOrderDetail($params[0], $params[1], $params[2]);
		$data['konfigurasi'] = $this->Laporan_model->Konfigurasi();
		$data['row'] = $this->Laporan_model->Cabang($params[2]);
		$data['tglawal'] = date('d-m-Y', strtotime($params[0]));
		$data['tglakhir'] = date('d-m-Y', strtotime($params[1]));
		$data['cabang'] = $params[2];

		set_time_limit(300);

		$this->load->view('menu/cetakan_pdf/r_sales_order_detail', $data, $filename, $paper, $orientation);

		// (Opsional) Mengatur ukuran kertas dan orientasi kertas
		// $this->pdf->setPaper('F4', 'landscape');

		// Menjadikan HTML sebagai PDF
		// $this->pdf->render();

		// Output akan menghasilkan PDF (1 = download dan 0 = preview)
		// $this->pdf->stream("welcome.pdf", array("Attachment" => 0));

		// exit(0);
		// }
	}

	public function ReturDetail($param = "")
	{
		$params = explode(":", urldecode($param));

		$filename = 'Laporan Retur Detail' . '_' . date('d M Y', strtotime($params[0])) . ' - ' . date('d M Y', strtotime($params[1]));

		// if (!empty($tanggal)){

		$paper = 'A4';
		$orientation = 'landscape';

		// menggunakan class dompdf
		$this->load->library('pdf');

		$data['reportrow'] = $this->Laporan_model->GetDataReturHeader($params[0], $params[1], $params[2]);
		$data['report'] = $this->Laporan_model->GetDataReturList($params[0], $params[1], $params[2]);
		$data['detail'] = $this->Laporan_model->GetDataReturDetail($params[0], $params[1], $params[2]);
		$data['konfigurasi'] = $this->Laporan_model->Konfigurasi();
		$data['row'] = $this->Laporan_model->Cabang($params[2]);
		$data['tglawal'] = date('d-m-Y', strtotime($params[0]));
		$data['tglakhir'] = date('d-m-Y', strtotime($params[1]));
		$data['cabang'] = $params[2];

		set_time_limit(300);

		$this->pdf->load_view('menu/cetakan_pdf/r_retur_detail', $data, $filename, $paper, $orientation);

		// (Opsional) Mengatur ukuran kertas dan orientasi kertas
		$this->pdf->setPaper('F4', 'landscape');

		// Menjadikan HTML sebagai PDF
		$this->pdf->render();

		// Output akan menghasilkan PDF (1 = download dan 0 = preview)
		$this->pdf->stream("welcome.pdf", array("Attachment" => 0));

		exit(0);
		// }
	}

	public function PenjualanDetilPerPelanggan($param = "")
	{
		$params = explode(":", urldecode($param));

		$filename = 'Laporan Penjualan Detail Per Pelanggan' . '_' . date('d M Y', strtotime($params[0])) . ' - ' . date('d M Y', strtotime($params[1]));

		// if (!empty($tanggal)){

		$paper = 'A4';
		$orientation = 'landscape';

		// menggunakan class dompdf
		// $this->load->library('pdf');

		$data['reportrow'] = $this->Laporan_model->GetPenjualanDetailPerPelangganHeader($params[0], $params[1], $params[2], $params[3]);
		$data['report'] = $this->Laporan_model->GetPenjualanDetailPerPelangganList($params[0], $params[1], $params[2], $params[3]);
		$data['detail'] = $this->Laporan_model->GetPenjualanDetailPerPelangganDetail($params[0], $params[1], $params[2], $params[3]);
		$data['konfigurasi'] = $this->Laporan_model->Konfigurasi();
		$data['row'] = $this->Laporan_model->Cabang($params[2]);
		$data['tglawal'] = date('d-m-Y', strtotime($params[0]));
		$data['tglakhir'] = date('d-m-Y', strtotime($params[1]));
		$data['cabang'] = $params[2];

		$this->load->view('menu/cetakan_pdf/r_penjualan_detail_per_pelanggan', $data, $filename, $paper, $orientation);

		// (Opsional) Mengatur ukuran kertas dan orientasi kertas
		// $this->pdf->setPaper('F4', 'landscape');

		// Menjadikan HTML sebagai PDF
		// $this->pdf->render();

		// Output akan menghasilkan PDF (1 = download dan 0 = preview)
		// $this->pdf->stream("welcome.pdf", array("Attachment" => 0));

		// exit(0);
		// }
	}

	public function PenjualanPromo($param = "")
	{
		$params = explode(":", urldecode($param));

		$filename = 'Laporan Penjualan Promo' . '_' . date('d M Y', strtotime($params[0])) . ' - ' . date('d M Y', strtotime($params[1]));

		// if (!empty($tanggal)){

		$paper = 'A4';
		$orientation = 'landscape';

		// menggunakan class dompdf
		$this->load->library('pdf');

		$data['reportrow'] = $this->Laporan_model->GetPenjualanPromoHeader($params[0], $params[1], $params[2]);
		$data['report'] = $this->Laporan_model->GetPenjualanPromoList($params[0], $params[1], $params[2]);
		$data['detail'] = $this->Laporan_model->GetPenjualanPromoDetail($params[0], $params[1], $params[2]);
		$data['konfigurasi'] = $this->Laporan_model->Konfigurasi();
		$data['row'] = $this->Laporan_model->Cabang($params[2]);
		$data['tglawal'] = date('d-m-Y', strtotime($params[0]));
		$data['tglakhir'] = date('d-m-Y', strtotime($params[1]));
		$data['cabang'] = $params[2];

		$this->pdf->load_view('menu/cetakan_pdf/r_penjualan_promo', $data, $filename, $paper, $orientation);

		// (Opsional) Mengatur ukuran kertas dan orientasi kertas
		$this->pdf->setPaper('F4', 'landscape');

		// Menjadikan HTML sebagai PDF
		$this->pdf->render();

		// Output akan menghasilkan PDF (1 = download dan 0 = preview)
		$this->pdf->stream("welcome.pdf", array("Attachment" => 0));

		exit(0);
		// }
	}

	public function KomisiSalesNett($param = "")
	{
		$params = explode(":", urldecode($param));

		$filename = 'Laporan Komisi Sales - NETT' . '_' . date('d M Y', strtotime($params[0])) . ' - ' . date('d M Y', strtotime($params[1]));

		// if (!empty($tanggal)){

		$paper = 'A4';
		$orientation = 'landscape';

		// menggunakan class dompdf
		$this->load->library('pdf');

		$data['reportrow'] = $this->Laporan_model->GetDataKomisiSalesNettRow($params[0], $params[1], $params[2], $params[3]);
		$data['report'] = $this->Laporan_model->GetDataKomisiSalesNettList($params[0], $params[1], $params[2], $params[3]);
		$data['detail'] = $this->Laporan_model->GetDataKomisiSalesNett($params[0], $params[1], $params[2], $params[3]);
		$data['konfigurasi'] = $this->Laporan_model->Konfigurasi();
		$data['row'] = $this->Laporan_model->Cabang($params[2]);
		$data['tglawal'] = date('d-m-Y', strtotime($params[0]));
		$data['tglakhir'] = date('d-m-Y', strtotime($params[1]));
		$data['cabang'] = $params[2];

		$this->pdf->load_view('menu/cetakan_pdf/r_komisi_sales_nett', $data, $filename, $paper, $orientation);

		// (Opsional) Mengatur ukuran kertas dan orientasi kertas
		$this->pdf->setPaper('F4', 'landscape');

		// Menjadikan HTML sebagai PDF
		$this->pdf->render();

		// Output akan menghasilkan PDF (1 = download dan 0 = preview)
		$this->pdf->stream("welcome.pdf", array("Attachment" => 0));

		exit(0);
		// }
	}

	public function KomisiSalesHangus($param = "")
	{
		$params = explode(":", urldecode($param));

		$filename = 'Laporan Komisi Sales Hangus' . '_' . date('d M Y', strtotime($params[0])) . ' - ' . date('d M Y', strtotime($params[1]));

		// if (!empty($tanggal)){

		$paper = 'A4';
		$orientation = 'landscape';

		// menggunakan class dompdf
		$this->load->library('pdf');

		$data['reportrow'] = $this->Laporan_model->GetDataKomisiSalesHangusRow($params[0], $params[1], $params[2], $params[3]);
		$data['report'] = $this->Laporan_model->GetDataKomisiSalesHangusList($params[0], $params[1], $params[2], $params[3]);
		$data['detail'] = $this->Laporan_model->GetDataKomisiSalesHangus($params[0], $params[1], $params[2], $params[3]);
		$data['konfigurasi'] = $this->Laporan_model->Konfigurasi();
		$data['row'] = $this->Laporan_model->Cabang($params[2]);
		$data['tglawal'] = date('d-m-Y', strtotime($params[0]));
		$data['tglakhir'] = date('d-m-Y', strtotime($params[1]));
		$data['cabang'] = $params[2];

		$this->pdf->load_view('menu/cetakan_pdf/r_komisi_sales_hangus', $data, $filename, $paper, $orientation);

		// (Opsional) Mengatur ukuran kertas dan orientasi kertas
		$this->pdf->setPaper('F4', 'landscape');

		// Menjadikan HTML sebagai PDF
		$this->pdf->render();

		// Output akan menghasilkan PDF (1 = download dan 0 = preview)
		$this->pdf->stream("welcome.pdf", array("Attachment" => 0));

		exit(0);
		// }
	}

	public function KomisiSalesProyeksi($param = "")
	{
		$params = explode(":", urldecode($param));

		$filename = 'Laporan Komisi Sales Proyeksi' . '_' . date('d M Y', strtotime($params[0])) . ' - ' . date('d M Y', strtotime($params[1]));

		// if (!empty($tanggal)){

		$paper = 'A4';
		$orientation = 'landscape';

		// menggunakan class dompdf
		$this->load->library('pdf');

		$data['reportrow'] = $this->Laporan_model->GetDataKomisiSalesProyeksiRow($params[0], $params[1], $params[2], $params[3]);
		$data['report'] = $this->Laporan_model->GetDataKomisiSalesProyeksiList($params[0], $params[1], $params[2], $params[3]);
		$data['detail'] = $this->Laporan_model->GetDataKomisiSalesProyeksi($params[0], $params[1], $params[2], $params[3]);
		$data['konfigurasi'] = $this->Laporan_model->Konfigurasi();
		$data['row'] = $this->Laporan_model->Cabang($params[2]);
		$data['tglawal'] = date('d-m-Y', strtotime($params[0]));
		$data['tglakhir'] = date('d-m-Y', strtotime($params[1]));
		$data['cabang'] = $params[2];

		$this->pdf->load_view('menu/cetakan_pdf/r_komisi_sales_proyeksi', $data, $filename, $paper, $orientation);

		// (Opsional) Mengatur ukuran kertas dan orientasi kertas
		$this->pdf->setPaper('F4', 'landscape');

		// Menjadikan HTML sebagai PDF
		$this->pdf->render();

		// Output akan menghasilkan PDF (1 = download dan 0 = preview)
		$this->pdf->stream("welcome.pdf", array("Attachment" => 0));

		exit(0);
		// }
	}

	public function KomisiSalesPerPelanggan($param = "")
	{
		$params = explode(":", urldecode($param));

		$filename = 'Laporan Komisi Sales Per Pelanggan' . '_' . date('d M Y', strtotime($params[0])) . ' - ' . date('d M Y', strtotime($params[1]));

		// if (!empty($tanggal)){

		$paper = 'A4';
		$orientation = 'landscape';

		// menggunakan class dompdf
		$this->load->library('pdf');

		$data['reportrow'] = $this->Laporan_model->GetDataKomisiSalesNettRow($params[0], $params[1], $params[2], $params[3]);
		$data['report'] = $this->Laporan_model->GetDataKomisiSalesNettList($params[0], $params[1], $params[2], $params[3]);
		$data['detail'] = $this->Laporan_model->GetDataKomisiSalesNett($params[0], $params[1], $params[2], $params[3]);
		$data['konfigurasi'] = $this->Laporan_model->Konfigurasi();
		$data['row'] = $this->Laporan_model->Cabang($params[2]);
		$data['tglawal'] = date('d-m-Y', strtotime($params[0]));
		$data['tglakhir'] = date('d-m-Y', strtotime($params[1]));
		$data['cabang'] = $params[2];

		$this->pdf->load_view('menu/cetakan_pdf/r_komisi_sales_per_pelanggann', $data, $filename, $paper, $orientation);

		// (Opsional) Mengatur ukuran kertas dan orientasi kertas
		$this->pdf->setPaper('F4', 'landscape');

		// Menjadikan HTML sebagai PDF
		$this->pdf->render();

		// Output akan menghasilkan PDF (1 = download dan 0 = preview)
		$this->pdf->stream("welcome.pdf", array("Attachment" => 0));

		exit(0);
		// }
	}

	public function PenjualanSemuaPelanggan($param = "")
	{
		$params = explode(":", urldecode($param));

		$filename = 'Laporan Rekap Penjualan Semua Pelanggan' . '_' . date('d M Y', strtotime($params[0])) . ' - ' . date('d M Y', strtotime($params[1]));

		// if (!empty($tanggal)){

		$paper = 'A4';
		$orientation = 'landscape';

		// menggunakan class dompdf
		$this->load->library('pdf');

		$data['report'] = $this->Laporan_model->GetPenjualanSemuaPelanggan($params[0], $params[1], $params[2]);
		$data['ppn'] = $this->Laporan_model->Konfigurasi();
		// $data['list'] = $this->Laporan_model->GetPenjualanSemuaPelangganList($params[0], $params[1], $params[2]);
		// $data['detail'] = $this->Laporan_model->GetPenjualanSemuaPelangganDetail($params[0], $params[1], $params[2]);
		$data['row'] = $this->Laporan_model->Cabang($params[2]);
		$data['tglawal'] = date('d-m-Y', strtotime($params[0]));
		$data['tglakhir'] = date('d-m-Y', strtotime($params[1]));
		$data['cabang'] = $params[2];

		$this->pdf->load_view('menu/cetakan_pdf/r_penjualan_semua_pelanggan', $data, $filename, $paper, $orientation);

		// (Opsional) Mengatur ukuran kertas dan orientasi kertas
		$this->pdf->setPaper('F4', 'landscape');

		// Menjadikan HTML sebagai PDF
		$this->pdf->render();

		// Output akan menghasilkan PDF (1 = download dan 0 = preview)
		$this->pdf->stream("welcome.pdf", array("Attachment" => 0));

		exit(0);
		// }
	}

	public function PreviewPenjualanSemuaPelanggan($param = "")
	{
		$params = explode(":", urldecode($param));

		$filename = 'Laporan Rekap Penjualan Semua Pelanggan' . '_' . date('d M Y', strtotime($params[0])) . ' - ' . date('d M Y', strtotime($params[1]));

		// if (!empty($tanggal)){

		$paper = 'A4';
		$orientation = 'landscape';

		// menggunakan class dompdf
		// $this->load->library('pdf');

		$data['report'] = $this->Laporan_model->GetPenjualanSemuaPelanggan($params[0], $params[1], $params[2]);
		$data['ppn'] = $this->Laporan_model->Konfigurasi();
		// $data['list'] = $this->Laporan_model->GetPenjualanSemuaPelangganList($params[0], $params[1], $params[2]);
		// $data['detail'] = $this->Laporan_model->GetPenjualanSemuaPelangganDetail($params[0], $params[1], $params[2]);
		$data['row'] = $this->Laporan_model->Cabang($params[2]);
		$data['tglawal'] = date('d-m-Y', strtotime($params[0]));
		$data['tglakhir'] = date('d-m-Y', strtotime($params[1]));
		$data['cabang'] = $params[2];

		$this->load->view('menu/cetakan_pdf/r_penjualan_semua_pelanggan', $data, $filename, $paper, $orientation);
		// $this->load->view('menu/cetakan_pdf/r_penjualan_semua_pelanggan', $data);

		// (Opsional) Mengatur ukuran kertas dan orientasi kertas
		// $this->pdf->setPaper('F4', 'landscape');

		// Menjadikan HTML sebagai PDF
		// $this->pdf->render();

		// Output akan menghasilkan PDF (1 = download dan 0 = preview)
		// $this->pdf->stream("welcome.pdf", array("Attachment" => 0));

		// exit(0);
		// }
	}

	public function RekapRetur($param = "")
	{
		$params = explode(":", urldecode($param));

		$filename = 'Laporan Rekap Retur' . '_' . date('d M Y', strtotime($params[0])) . ' - ' . date('d M Y', strtotime($params[1]));

		// if (!empty($tanggal)){

		$paper = 'A4';
		$orientation = 'landscape';

		// menggunakan class dompdf
		$this->load->library('pdf');

		$data['report'] = $this->Laporan_model->GetRekapRetur($params[0], $params[1], $params[2]);
		$data['ppn'] = $this->Laporan_model->Konfigurasi();
		// $data['list'] = $this->Laporan_model->GetPenjualanSemuaPelangganList($params[0], $params[1], $params[2]);
		// $data['detail'] = $this->Laporan_model->GetPenjualanSemuaPelangganDetail($params[0], $params[1], $params[2]);
		$data['row'] = $this->Laporan_model->Cabang($params[2]);
		$data['tglawal'] = date('d-m-Y', strtotime($params[0]));
		$data['tglakhir'] = date('d-m-Y', strtotime($params[1]));
		$data['cabang'] = $params[2];

		$this->pdf->load_view('menu/cetakan_pdf/r_rekap_retur', $data, $filename, $paper, $orientation);

		// (Opsional) Mengatur ukuran kertas dan orientasi kertas
		$this->pdf->setPaper('F4', 'landscape');

		// Menjadikan HTML sebagai PDF
		$this->pdf->render();

		// Output akan menghasilkan PDF (1 = download dan 0 = preview)
		$this->pdf->stream("welcome.pdf", array("Attachment" => 0));

		exit(0);
		// }
	}

	public function RekapPenjualanPerGudang($param = "")
	{
		$params = explode(":", urldecode($param));

		$filename = 'Laporan Rekap Penjualan Per Gudang' . '_' . date('d M Y', strtotime($params[0])) . ' - ' . date('d M Y', strtotime($params[1]));

		// if (!empty($tanggal)){

		$paper = 'A4';
		$orientation = 'landscape';

		// menggunakan class dompdf
		$this->load->library('pdf');

		$data['report'] = $this->Laporan_model->GetRekapPenjualanPerGudang($params[0], $params[1], $params[2]);
		$data['row'] = $this->Laporan_model->Gudang($params[2]);
		$data['tglawal'] = date('d-m-Y', strtotime($params[0]));
		$data['tglakhir'] = date('d-m-Y', strtotime($params[1]));

		$this->pdf->load_view('menu/cetakan_pdf/r_penjualan_per_gudang', $data, $filename, $paper, $orientation);

		// (Opsional) Mengatur ukuran kertas dan orientasi kertas
		$this->pdf->setPaper('F4', 'landscape');

		// Menjadikan HTML sebagai PDF
		$this->pdf->render();

		// Output akan menghasilkan PDF (1 = download dan 0 = preview)
		$this->pdf->stream("welcome.pdf", array("Attachment" => 0));

		exit(0);
		// }
	}

	function PenjualanPerPelangganPerBarang($param = "")
	{
		$params = explode(":", urldecode($param));

		$filename = 'Laporan Rekap Penjualan Per Gudang' . '_' . date('d M Y', strtotime($params[0])) . ' - ' . date('d M Y', strtotime($params[1]));

		// if (!empty($tanggal)){

		$paper = 'A4';
		$orientation = 'landscape';

		// menggunakan class dompdf
		$this->load->library('pdf');

		$data['penjualanperpelangganperbarang'] = $this->Laporan_model->GetDataPenjualanPerPelangganPerBarang($params[0], $params[1], $params[2], $params[3], $params[4]);
		$data['penjualanperpelangganperbarangdetail'] = $this->Laporan_model->GetDataPenjualanPerPelangganPerBarangDetail($params[0], $params[1], $params[2], $params[3], $params[4]);
		$data['penjualanperpelangganperbarangrow'] = $this->Laporan_model->GetDataPenjualanPerPelangganPerBarangRow($params[0], $params[1], $params[2], $params[3], $params[4]);
		$data['tglawal'] = date('d-m-Y', strtotime($params[0]));
		$data['tglakhir'] = date('d-m-Y', strtotime($params[1]));
		$data['pelanggan'] = $this->Laporan_model->Pelanggan($params[2]);
		$data['barang'] = $this->Laporan_model->Barang($params[4]);
		// $this->load->view('menu/cetakan_pdf/r_penjualan_perpelanggan_perbarang', $data);
		$this->pdf->load_view('menu/cetakan_pdf/r_penjualan_perpelanggan_perbarang', $data, $filename, $paper, $orientation);

		// (Opsional) Mengatur ukuran kertas dan orientasi kertas
		$this->pdf->setPaper('F4', 'landscape');

		// Menjadikan HTML sebagai PDF
		$this->pdf->render();

		// Output akan menghasilkan PDF (1 = download dan 0 = preview)
		$this->pdf->stream("welcome.pdf", array("Attachment" => 0));

		exit(0);
	}

	public function PreviewLaporanPenjualanDetail($param = "")
	{
		$params = explode(":", urldecode($param));
		$tglawal = $params[0];
		$tglakhir = $params[1];
		$cabang = $params[2];
		// $pelanggan = $params[3];

		$filename = 'Laporan Penjualan Detail Per Pelanggan' . '_' . date('d M Y', strtotime($params[0])) . ' - ' . date('d M Y', strtotime($params[1]));

		// if (!empty($tanggal)){

		$paper = 'A4';
		$orientation = 'portrait';

		// menggunakan class dompdf
		// $this->load->library('pdf');

		$data['report'] = $this->Laporan_model->LaporanPenjualanDetail($params[0], $params[1], $params[2]);
		$data['reportrow'] = $this->Laporan_model->LaporanPenjualanDetailRow($params[0], $params[1], $params[2]);
		$data['reportlist'] = $this->Laporan_model->LaporanPenjualanDetailList($params[0], $params[1], $params[2]);
		$data['konfigurasi'] = $this->Laporan_model->Konfigurasi();
		$data['row'] = $this->Laporan_model->Cabang($params[2]);
		$data['tglawal'] = date('d-m-Y', strtotime($params[0]));
		$data['tglakhir'] = date('d-m-Y', strtotime($params[1]));
		$data['cabang'] = $params[2];
		// var_dump($data);
		// die();
		set_time_limit(300);
		$this->load->view('menu/cetakan_pdf/laporanpenjualandetail', $data, $filename, $paper, $orientation);
	}

	public function PreviewLaporanPenjualanDetailAllCabang($param = "")
	{
		$params = explode(":", urldecode($param));
		$tglawal = $params[0];
		$tglakhir = $params[1];
		// $cabang = $params[2];
		// $pelanggan = $params[3];

		$filename = 'Laporan Penjualan Detail All Cabang' . '_' . date('d M Y', strtotime($params[0])) . ' - ' . date('d M Y', strtotime($params[1]));

		// if (!empty($tanggal)){

		$paper = 'A4';
		$orientation = 'portrait';

		// menggunakan class dompdf
		// $this->load->library('pdf');

		$data['report'] = $this->Laporan_model->LaporanPenjualanDetailAllCabang($params[0], $params[1]);
		$data['reportrow'] = $this->Laporan_model->LaporanPenjualanDetailAllCabangRow($params[0], $params[1]);
		$data['reportlist'] = $this->Laporan_model->LaporanPenjualanDetailAllCabangList($params[0], $params[1]);
		$data['konfigurasi'] = $this->Laporan_model->Konfigurasi();
		// $data['row'] = $this->Laporan_model->Cabang($params[2]);
		$data['tglawal'] = date('d-m-Y', strtotime($params[0]));
		$data['tglakhir'] = date('d-m-Y', strtotime($params[1]));
		// $data['cabang'] = $params[2];
		// var_dump($data);
		// die();
		set_time_limit(300);
		$this->load->view('menu/cetakan_pdf/laporanpenjualandetailallcabang', $data, $filename, $paper, $orientation);
	}

	public function LaporanPenjualanDetailAllCabang($param = "")
	{
		$params = explode(":", urldecode($param));
		$tglawal = $params[0];
		$tglakhir = $params[1];
		// $cabang = $params[2];
		// $pelanggan = $params[3];

		$filename = 'Laporan Penjualan Detail All Cabang' . '_' . date('d M Y', strtotime($params[0])) . ' - ' . date('d M Y', strtotime($params[1]));

		// if (!empty($tanggal)){

		$paper = 'A4';
		$orientation = 'portrait';

		// menggunakan class dompdf
		$this->load->library('pdf');

		$data['report'] = $this->Laporan_model->LaporanPenjualanDetailAllCabang($params[0], $params[1]);
		$data['reportrow'] = $this->Laporan_model->LaporanPenjualanDetailAllCabangRow($params[0], $params[1]);
		$data['reportlist'] = $this->Laporan_model->LaporanPenjualanDetailAllCabangList($params[0], $params[1]);
		$data['konfigurasi'] = $this->Laporan_model->Konfigurasi();
		// $data['row'] = $this->Laporan_model->Cabang($params[2]);
		$data['tglawal'] = date('d-m-Y', strtotime($params[0]));
		$data['tglakhir'] = date('d-m-Y', strtotime($params[1]));
		// $data['cabang'] = $params[2];
		// var_dump($data);
		// die();
		set_time_limit(300);
		$this->pdf->load_view('menu/cetakan_pdf/laporanpenjualandetailallcabang', $data, $filename, $paper, $orientation);

		// (Opsional) Mengatur ukuran kertas dan orientasi kertas
		$this->pdf->setPaper('F4', 'landscape');

		// Menjadikan HTML sebagai PDF
		$this->pdf->render();

		// Output akan menghasilkan PDF (1 = download dan 0 = preview)
		$this->pdf->stream("welcome.pdf", array("Attachment" => 0));

		exit(0);
	}

	public function PreviewLaporanRekapPenjualan($param = "")
	{
		$params = explode(":", urldecode($param));
		$tglawal = $params[0];
		$tglakhir = $params[1];
		$cabang = $params[2];
		// $pelanggan = $params[3];

		$filename = 'Laporan Penjualan Detail Per Pelanggan' . '_' . date('d M Y', strtotime($params[0])) . ' - ' . date('d M Y', strtotime($params[1]));

		// if (!empty($tanggal)){

		$paper = 'A4';
		$orientation = 'portrait';

		// menggunakan class dompdf
		// $this->load->library('pdf');

		// $data['report'] = $this->Laporan_model->LaporanPenjualanDetail($params[0], $params[1], $params[2]);
		$data['reportrow'] = $this->Laporan_model->LaporanRekapPenjualan($params[0], $params[1], $params[2]);
		$data['reportlist'] = $this->Laporan_model->LaporanRekapPenjualanList($params[0], $params[1], $params[2]);
		$data['konfigurasi'] = $this->Laporan_model->Konfigurasi();
		$data['row'] = $this->Laporan_model->Cabang($params[2]);
		$data['tglawal'] = date('d-m-Y', strtotime($params[0]));
		$data['tglakhir'] = date('d-m-Y', strtotime($params[1]));
		$data['cabang'] = $params[2];
		// var_dump($data);
		// die();
		set_time_limit(300);
		$this->load->view('menu/cetakan_pdf/r_rekap_penjualan', $data, $filename, $paper, $orientation);
	}

	public function PreviewLaporanRekapPenjualanAllCabang($param = "")
	{
		$params = explode(":", urldecode($param));
		// $pelanggan = $params[3];

		$filename = 'Laporan Rekap Penjualan All Cabang' . '_' . date('d M Y', strtotime($params[0])) . ' - ' . date('d M Y', strtotime($params[1]));

		// if (!empty($tanggal)){

		$paper = 'A4';
		$orientation = 'landscape';

		// menggunakan class dompdf
		// $this->load->library('pdf');

		// $data['report'] = $this->Laporan_model->LaporanPenjualanDetail($params[0], $params[1], $params[2]);
		$data['reportrow'] = $this->Laporan_model->LaporanRekapPenjualanAllCabang($params[0], $params[1]);
		$data['reportlist'] = $this->Laporan_model->LaporanRekapPenjualanAllCabangList($params[0], $params[1]);
		$data['konfigurasi'] = $this->Laporan_model->Konfigurasi();
		// $data['row'] = $this->Laporan_model->Cabang($params[2]);
		$data['tglawal'] = date('d-m-Y', strtotime($params[0]));
		$data['tglakhir'] = date('d-m-Y', strtotime($params[1]));
		// $data['cabang'] = $params[2];
		// var_dump($data);
		// die();
		set_time_limit(300);
		$this->load->view('menu/cetakan_pdf/r_rekap_penjualan_all_cabang', $data, $filename, $paper, $orientation);
	}

	public function LaporanRekapPenjualanAllCabang($param = "")
	{
		$params = explode(":", urldecode($param));
		// $pelanggan = $params[3];

		$filename = 'Laporan Rekap Penjualan All Cabang' . '_' . date('d M Y', strtotime($params[0])) . ' - ' . date('d M Y', strtotime($params[1]));

		// if (!empty($tanggal)){

		$paper = 'A4';
		$orientation = 'landscape';

		// menggunakan class dompdf
		$this->load->library('pdf');

		// $data['report'] = $this->Laporan_model->LaporanPenjualanDetail($params[0], $params[1], $params[2]);
		$data['reportrow'] = $this->Laporan_model->LaporanRekapPenjualanAllCabang($params[0], $params[1]);
		$data['reportlist'] = $this->Laporan_model->LaporanRekapPenjualanAllCabangList($params[0], $params[1]);
		$data['konfigurasi'] = $this->Laporan_model->Konfigurasi();
		// $data['row'] = $this->Laporan_model->Cabang($params[2]);
		$data['tglawal'] = date('d-m-Y', strtotime($params[0]));
		$data['tglakhir'] = date('d-m-Y', strtotime($params[1]));
		// $data['cabang'] = $params[2];
		// var_dump($data);
		// die();
		// set_time_limit(300);
		// $this->load->view('menu/cetakan_pdf/r_rekap_penjualan_all_cabang', $data, $filename, $paper, $orientation);
		$this->pdf->load_view('menu/cetakan_pdf/r_rekap_penjualan_all_cabang', $data, $filename, $paper, $orientation);

		// (Opsional) Mengatur ukuran kertas dan orientasi kertas
		$this->pdf->setPaper('F4', 'landscape');

		// Menjadikan HTML sebagai PDF
		$this->pdf->render();

		// Output akan menghasilkan PDF (1 = download dan 0 = preview)
		$this->pdf->stream("welcome.pdf", array("Attachment" => 0));

		exit(0);
	}

	public function PreviewLaporanPenjualanPertanggal($param = "")
	{
		$params = explode(":", urldecode($param));
		$tglawal = $params[0];
		$tglakhir = $params[1];
		$cabang = $params[2];
		// $pelanggan = $params[3];

		$filename = 'Laporan Penjualan Pertangga;' . '_' . date('d M Y', strtotime($params[0])) . ' - ' . date('d M Y', strtotime($params[1]));

		// if (!empty($tanggal)){

		$paper = 'A4';
		$orientation = 'portrait';

		// menggunakan class dompdf
		// $this->load->library('pdf');

		// $data['report'] = $this->Laporan_model->LaporanPenjualanDetail($params[0], $params[1], $params[2]);
		$data['reportrow'] = $this->Laporan_model->LaporanPenjualanPertanggal($params[0], $params[1], $params[2]);
		$data['reportlist'] = $this->Laporan_model->LaporanPenjualanPertanggalList($params[0], $params[1], $params[2]);
		$data['konfigurasi'] = $this->Laporan_model->Konfigurasi();
		$data['row'] = $this->Laporan_model->Cabang($params[2]);
		$data['tglawal'] = date('d-m-Y', strtotime($params[0]));
		$data['tglakhir'] = date('d-m-Y', strtotime($params[1]));
		$data['cabang'] = $params[2];
		// var_dump($data);
		// die();
		set_time_limit(300);
		$this->load->view('menu/cetakan_pdf/r_penjualan_pertanggal', $data, $filename, $paper, $orientation);
	}

	public function PreviewLaporanPenjualanPerkategori($param = "")
	{
		$params = explode(":", urldecode($param));
		$tglawal = $params[0];
		$tglakhir = $params[1];
		$cabang = $params[2];
		// $pelanggan = $params[3];

		$filename = 'Laporan Penjualan Perkategori' . '_' . date('d M Y', strtotime($params[0])) . ' - ' . date('d M Y', strtotime($params[1]));

		// if (!empty($tanggal)){

		$paper = 'A4';
		$orientation = 'portrait';

		// menggunakan class dompdf
		// $this->load->library('pdf');

		// $data['report'] = $this->Laporan_model->LaporanPenjualanDetail($params[0], $params[1], $params[2]);
		$data['reportrow'] = $this->Laporan_model->LaporanPenjualanPerkategori($params[0], $params[1], $params[2], $params[3], $params[4]);
		$data['reportlist'] = $this->Laporan_model->LaporanPenjualanPerkategoriList($params[0], $params[1], $params[2], $params[3], $params[4]);
		$data['konfigurasi'] = $this->Laporan_model->Konfigurasi();
		$data['row'] = $this->Laporan_model->Cabang($params[2]);
		$data['tglawal'] = date('d-m-Y', strtotime($params[0]));
		$data['tglakhir'] = date('d-m-Y', strtotime($params[1]));
		$data['cabang'] = $params[2];
		$data['merkawal'] = $this->Laporan_model->GetMerk($params[3]);
		$data['merkakhir'] = $this->Laporan_model->GetMerkAkhir($params[4]);

		// var_dump($data);
		// die();
		set_time_limit(300);
		$this->load->view('menu/cetakan_pdf/r_penjualan_perkategori', $data, $filename, $paper, $orientation);
	}

	public function ExportPenjualanPerPelangganPerBarang($param = "")
	{
		$params = explode(":", urldecode($param));

		// $data['penjualanperpelangganperbarang'] = $this->Laporan_model->GetDataPenjualanPerPelangganPerBarang($params[0], $params[1], $params[2], $params[3], $params[4]);
		$data['penjualanperpelangganperbarang'] = $this->Laporan_model->GetDataPenjualanPerPelangganPerBarangDetail($params[0], $params[1], $params[2], $params[3], $params[4]);
		$data['penjualanperpelangganperbarangrow'] = $this->Laporan_model->GetDataPenjualanPerPelangganPerBarangRow($params[0], $params[1], $params[2], $params[3], $params[4]);
		$data['tglawal'] = date('d-m-Y', strtotime($params[0]));
		$data['tglakhir'] = date('d-m-Y', strtotime($params[1]));
		$data['cabang'] = $this->Laporan_model->Cabang($params[3]);
		$this->load->view('menu/reporting/penjualan/excel_penjualan_perpelanggan_perbarang', $data);
	}

	public function ExportRekapPenjualanPerGudang($param = "")
	{
		$params = explode(":", urldecode($param));

		$data['excel'] = $this->Laporan_model->GetRekapPenjualanPerGudang($params[0], $params[1], $params[2]);
		$data['row'] = $this->Laporan_model->Gudang($params[2]);
		$data['tglawal'] = date('d-m-Y', strtotime($params[0]));
		$data['tglakhir'] = date('d-m-Y', strtotime($params[1]));
		$this->load->view('menu/reporting/penjualan/excel_penjualan_per_gudang', $data);
	}

	public function ExportSalesOrder($param = "")
	{
		$params = explode(":", urldecode($param));

		$data['excel'] = $this->Laporan_model->GetDataSalesOrder($params[0], $params[1], $params[2]);
		$data['row'] = $this->Laporan_model->Cabang($params[2]);
		$data['tglawal'] = date('d-m-Y', strtotime($params[0]));
		$data['tglakhir'] = date('d-m-Y', strtotime($params[1]));
		$this->load->view('menu/reporting/penjualan/excel_sales_order', $data);
	}

	public function ExportRekapPenjualanAllCabang($param = "")
	{
		$params = explode(":", urldecode($param));

		$data['reportrow'] = $this->Laporan_model->LaporanRekapPenjualanAllCabang($params[0], $params[1]);
		$data['reportlist'] = $this->Laporan_model->LaporanRekapPenjualanAllCabangList($params[0], $params[1]);
		$data['konfigurasi'] = $this->Laporan_model->Konfigurasi();
		// $data['row'] = $this->Laporan_model->Cabang($params[2]);
		$data['tglawal'] = date('d-m-Y', strtotime($params[0]));
		$data['tglakhir'] = date('d-m-Y', strtotime($params[1]));
		$this->load->view('menu/reporting/penjualan/excel_rekap_penjualan_all_cabang', $data);
	}

	public function ExportKomisiSalesNett($param = "")
	{
		$params = explode(":", urldecode($param));

		$data['reportrow'] = $this->Laporan_model->GetDataKomisiSalesNettRow($params[0], $params[1], $params[2], $params[3]);
		$data['report'] = $this->Laporan_model->GetDataKomisiSalesNettList($params[0], $params[1], $params[2], $params[3]);
		$data['detail'] = $this->Laporan_model->GetDataKomisiSalesNett($params[0], $params[1], $params[2], $params[3]);
		$data['konfigurasi'] = $this->Laporan_model->Konfigurasi();
		$data['row'] = $this->Laporan_model->Cabang($params[2]);
		$data['tglawal'] = date('d-m-Y', strtotime($params[0]));
		$data['tglakhir'] = date('d-m-Y', strtotime($params[1]));
		$data['cabang'] = $params[2];
		$this->load->view('menu/reporting/penjualan/excel_komisi_sales_nett', $data);
	}

	public function ExportKomisiSalesProyeksi($param = "")
	{
		$params = explode(":", urldecode($param));

		$data['reportrow'] = $this->Laporan_model->GetDataKomisiSalesProyeksiRow($params[0], $params[1], $params[2], $params[3]);
		$data['report'] = $this->Laporan_model->GetDataKomisiSalesProyeksiList($params[0], $params[1], $params[2], $params[3]);
		$data['detail'] = $this->Laporan_model->GetDataKomisiSalesProyeksi($params[0], $params[1], $params[2], $params[3]);
		$data['konfigurasi'] = $this->Laporan_model->Konfigurasi();
		$data['row'] = $this->Laporan_model->Cabang($params[2]);
		$data['tglawal'] = date('d-m-Y', strtotime($params[0]));
		$data['tglakhir'] = date('d-m-Y', strtotime($params[1]));
		$data['cabang'] = $params[2];
		$this->load->view('menu/reporting/penjualan/excel_komisi_sales_proyeksi', $data);
	}

	public function ExportKomisiSalesHangus($param = "")
	{
		$params = explode(":", urldecode($param));

		$data['reportrow'] = $this->Laporan_model->GetDataKomisiSalesHangusRow($params[0], $params[1], $params[2], $params[3]);
		$data['report'] = $this->Laporan_model->GetDataKomisiSalesHangusList($params[0], $params[1], $params[2], $params[3]);
		$data['detail'] = $this->Laporan_model->GetDataKomisiSalesHangus($params[0], $params[1], $params[2], $params[3]);
		$data['konfigurasi'] = $this->Laporan_model->Konfigurasi();
		$data['row'] = $this->Laporan_model->Cabang($params[2]);
		$data['tglawal'] = date('d-m-Y', strtotime($params[0]));
		$data['tglakhir'] = date('d-m-Y', strtotime($params[1]));
		$data['cabang'] = $params[2];
		$this->load->view('menu/reporting/penjualan/excel_komisi_sales_hangus', $data);
	}

	public function ExportReturPerTanggal($param = "")
	{
		$params = explode(":", urldecode($param));

		$data['report'] = $this->Laporan_model->GetDataReturPenjualanPerTanggal($params[0], $params[1], $params[2]);
		$data['row'] = $this->Laporan_model->Cabang($params[2]);
		$data['reportlist'] = $this->Laporan_model->GetDataReturPenjualanPerTanggalList($params[0], $params[1], $params[2]);
		$data['tglawal'] = date('d-m-Y', strtotime($params[0]));
		$data['tglakhir'] = date('d-m-Y', strtotime($params[1]));
		$data['cabang'] = $params[2];
		$this->load->view('menu/reporting/penjualan/excel_retur_per_tanggal', $data);
	}

	public function ExportPenjualanSemuaPelanggan($param = "")
	{
		$params = explode(":", urldecode($param));

		$data['report'] = $this->Laporan_model->GetPenjualanSemuaPelanggan($params[0], $params[1], $params[2]);
		$data['ppn'] = $this->Laporan_model->Konfigurasi();
		$data['row'] = $this->Laporan_model->Cabang($params[2]);
		$data['tglawal'] = date('d-m-Y', strtotime($params[0]));
		$data['tglakhir'] = date('d-m-Y', strtotime($params[1]));
		$data['cabang'] = $params[2];
		$this->load->view('menu/reporting/penjualan/excel_rekap_penjualan_semua_pelanggan', $data);
	}

	public function ExportRekapRetur($param = "")
	{
		$params = explode(":", urldecode($param));


		$data['report'] = $this->Laporan_model->GetRekapRetur($params[0], $params[1], $params[2]);
		$data['ppn'] = $this->Laporan_model->Konfigurasi();
		$data['row'] = $this->Laporan_model->Cabang($params[2]);
		$data['tglawal'] = date('d-m-Y', strtotime($params[0]));
		$data['tglakhir'] = date('d-m-Y', strtotime($params[1]));
		$data['cabang'] = $params[2];
		$this->load->view('menu/reporting/penjualan/excel_rekap_retur', $data);
	}

	public function ExportPenjualanSemuaSalesman($param = "")
	{
		$params = explode(":", urldecode($param));

		$data['report'] = $this->Laporan_model->PenjualanSemuaSalesman($params[0], $params[1], $params[2]);
		$data['row'] = $this->Laporan_model->Cabang($params[2]);
		$data['reportlist'] = $this->Laporan_model->PenjualanSemuaSalesmanList($params[0], $params[1], $params[2]);
		$data['tglawal'] = date('d-m-Y', strtotime($params[0]));
		$data['tglakhir'] = date('d-m-Y', strtotime($params[1]));
		$data['cabang'] = $params[2];
		$this->load->view('menu/reporting/penjualan/excel_penjualan_semua_salesman', $data);
	}

	public function ExportPenjualanPerSalesman($param = "")
	{
		$params = explode(":", urldecode($param));

		$data['report'] = $this->Laporan_model->PenjualanPerSalesman($params[0], $params[1], $params[2], $params[3]);
		$data['reportrow'] = $this->Laporan_model->PenjualanPerSalesmanRow($params[0], $params[1], $params[2], $params[3]);
		$data['row'] = $this->Laporan_model->Cabang($params[2]);
		$data['reportlist'] = $this->Laporan_model->PenjualanPerSalesmanList($params[0], $params[1], $params[2], $params[3]);
		$data['tglawal'] = date('d-m-Y', strtotime($params[0]));
		$data['tglakhir'] = date('d-m-Y', strtotime($params[1]));
		$data['cabang'] = $params[2];
		$this->load->view('menu/reporting/penjualan/excel_penjualan_per_salesman', $data);
	}

	public function ExportSalesOrderDetail($param = "")
	{
		$params = explode(":", urldecode($param));

		$data['reportrow'] = $this->Laporan_model->GetDataSalesOrderHeader($params[0], $params[1], $params[2]);
		$data['report'] = $this->Laporan_model->GetDataSalesOrderList($params[0], $params[1], $params[2]);
		$data['detail'] = $this->Laporan_model->GetDataSalesOrderDetail($params[0], $params[1], $params[2]);
		$data['konfigurasi'] = $this->Laporan_model->Konfigurasi();
		$data['row'] = $this->Laporan_model->Cabang($params[2]);
		$data['tglawal'] = date('d-m-Y', strtotime($params[0]));
		$data['tglakhir'] = date('d-m-Y', strtotime($params[1]));
		$data['cabang'] = $params[2];
		$this->load->view('menu/reporting/penjualan/excel_sales_order_detail', $data);
	}

	public function ExportReturDetail($param = "")
	{
		$params = explode(":", urldecode($param));

		$data['reportrow'] = $this->Laporan_model->GetDataReturHeader($params[0], $params[1], $params[2]);
		$data['report'] = $this->Laporan_model->GetDataReturList($params[0], $params[1], $params[2]);
		$data['detail'] = $this->Laporan_model->GetDataReturDetail($params[0], $params[1], $params[2]);
		$data['konfigurasi'] = $this->Laporan_model->Konfigurasi();
		$data['row'] = $this->Laporan_model->Cabang($params[2]);
		$data['tglawal'] = date('d-m-Y', strtotime($params[0]));
		$data['tglakhir'] = date('d-m-Y', strtotime($params[1]));
		$data['cabang'] = $params[2];
		$this->load->view('menu/reporting/penjualan/excel_retur_detail', $data);
	}

	public function ExportPenjualanDetilPerPelanggan($param = "")
	{
		$params = explode(":", urldecode($param));

		$data['reportrow'] = $this->Laporan_model->GetPenjualanDetailPerPelangganHeader($params[0], $params[1], $params[2], $params[3]);
		$data['report'] = $this->Laporan_model->GetPenjualanDetailPerPelangganList($params[0], $params[1], $params[2], $params[3]);
		$data['detail'] = $this->Laporan_model->GetPenjualanDetailPerPelangganDetail($params[0], $params[1], $params[2], $params[3]);
		$data['konfigurasi'] = $this->Laporan_model->Konfigurasi();
		$data['row'] = $this->Laporan_model->Cabang($params[2]);
		$data['tglawal'] = date('d-m-Y', strtotime($params[0]));
		$data['tglakhir'] = date('d-m-Y', strtotime($params[1]));
		$data['cabang'] = $params[2];
		$this->load->view('menu/reporting/penjualan/excel_penjualan_detail_per_pelanggan', $data);
	}

	public function ExportPenjualanPromo($param = "")
	{
		$params = explode(":", urldecode($param));

		$data['reportrow'] = $this->Laporan_model->GetPenjualanPromoHeader($params[0], $params[1], $params[2]);
		$data['report'] = $this->Laporan_model->GetPenjualanPromoList($params[0], $params[1], $params[2]);
		$data['detail'] = $this->Laporan_model->GetPenjualanPromoDetail($params[0], $params[1], $params[2]);
		$data['konfigurasi'] = $this->Laporan_model->Konfigurasi();
		$data['row'] = $this->Laporan_model->Cabang($params[2]);
		$data['tglawal'] = date('d-m-Y', strtotime($params[0]));
		$data['tglakhir'] = date('d-m-Y', strtotime($params[1]));
		$data['cabang'] = $params[2];
		$this->load->view('menu/reporting/penjualan/excel_penjualan_promo', $data);
	}

	public function ExportRekapPenjualanPelanggan($param = "")
	{
		$params = explode(":", urldecode($param));

		$data['report'] = $this->Laporan_model->RekapPenjualanPelanggan($params[0], $params[1], $params[2]);
		$data['ppn'] = $this->Laporan_model->Konfigurasi();
		// $data['penjualanperpelangganrow'] = $this->Laporan_model->GetDataPenjualanPerPelangganRow($params[0], $params[1], $params[2], $params[3]);
		// $data['penjualanperpelangganlist'] = $this->Laporan_model->GetDataPenjualanPerPelangganList($params[0], $params[1], $params[2], $params[3]);
		$data['cabang'] = $this->Laporan_model->Cabang($params[2]);
		$data['tglawal'] = date('d-m-Y', strtotime($params[0]));
		$data['tglakhir'] = date('d-m-Y', strtotime($params[1]));
		$this->load->view('menu/reporting/penjualan/excel_rekap_penjualan_pelanggan', $data);
	}

	public function ExportPenjualanDetail($param = "")
	{
		$params = explode(":", urldecode($param));
		$tglawal = $params[0];
		$tglakhir = $params[1];
		$cabang = $params[2];

		$data['report'] = $this->Laporan_model->LaporanPenjualanDetail($params[0], $params[1], $params[2]);
		$data['reportrow'] = $this->Laporan_model->LaporanPenjualanDetailRow($params[0], $params[1], $params[2]);
		$data['reportlist'] = $this->Laporan_model->LaporanPenjualanDetailList($params[0], $params[1], $params[2]);
		$data['konfigurasi'] = $this->Laporan_model->Konfigurasi();
		$data['row'] = $this->Laporan_model->Cabang($params[2]);
		$data['tglawal'] = date('d-m-Y', strtotime($params[0]));
		$data['tglakhir'] = date('d-m-Y', strtotime($params[1]));
		$data['cabang'] = $params[2];

		$this->load->view('menu/reporting/penjualan/excel_penjualan_detail', $data);
	}

	public function ExportLaporanPenjualanDetailAllCabang($param = "")
	{
		$params = explode(":", urldecode($param));
		$tglawal = $params[0];
		$tglakhir = $params[1];
		// $cabang = $params[2];

		$data['report'] = $this->Laporan_model->LaporanPenjualanDetailAllCabang($params[0], $params[1]);
		$data['reportrow'] = $this->Laporan_model->LaporanPenjualanDetailAllCabangRow($params[0], $params[1]);
		$data['reportlist'] = $this->Laporan_model->LaporanPenjualanDetailAllCabangList($params[0], $params[1]);
		$data['konfigurasi'] = $this->Laporan_model->Konfigurasi();
		// $data['row'] = $this->Laporan_model->Cabang($params[2]);
		$data['tglawal'] = date('d-m-Y', strtotime($params[0]));
		$data['tglakhir'] = date('d-m-Y', strtotime($params[1]));
		// $data['cabang'] = $params[2];

		$this->load->view('menu/reporting/penjualan/excel_penjualan_detail_all_cabang', $data);
	}

	public function ExportRekapPenjualan($param = "")
	{
		$params = explode(":", urldecode($param));
		$tglawal = $params[0];
		$tglakhir = $params[1];
		$cabang = $params[2];

		// $data['report'] = $this->Laporan_model->LaporanPenjualanDetail($params[0], $params[1], $params[2]);
		$data['reportrow'] = $this->Laporan_model->LaporanRekapPenjualan($params[0], $params[1], $params[2]);
		$data['reportlist'] = $this->Laporan_model->LaporanRekapPenjualanList($params[0], $params[1], $params[2]);
		$data['konfigurasi'] = $this->Laporan_model->Konfigurasi();
		$data['row'] = $this->Laporan_model->Cabang($params[2]);
		$data['tglawal'] = date('d-m-Y', strtotime($params[0]));
		$data['tglakhir'] = date('d-m-Y', strtotime($params[1]));
		$data['cabang'] = $params[2];

		$this->load->view('menu/reporting/penjualan/excel_rekap_penjualan', $data);
	}

	public function ExportPenjualanPertanggal($param = "")
	{
		$params = explode(":", urldecode($param));
		$tglawal = $params[0];
		$tglakhir = $params[1];
		$cabang = $params[2];

		$data['reportrow'] = $this->Laporan_model->LaporanPenjualanPertanggal($params[0], $params[1], $params[2]);
		$data['reportlist'] = $this->Laporan_model->LaporanPenjualanPertanggalList($params[0], $params[1], $params[2]);
		$data['konfigurasi'] = $this->Laporan_model->Konfigurasi();
		$data['row'] = $this->Laporan_model->Cabang($params[2]);
		$data['tglawal'] = date('d-m-Y', strtotime($params[0]));
		$data['tglakhir'] = date('d-m-Y', strtotime($params[1]));
		$data['cabang'] = $params[2];

		$this->load->view('menu/reporting/penjualan/excel_penjuala_pertanggal', $data);
	}

	public function ExportPenjualanPerkategori($param = "")
	{
		$params = explode(":", urldecode($param));
		$tglawal = $params[0];
		$tglakhir = $params[1];
		$cabang = $params[2];

		// $data['report'] = $this->Laporan_model->LaporanPenjualanDetail($params[0], $params[1], $params[2]);
		$data['reportrow'] = $this->Laporan_model->LaporanPenjualanPerkategori($params[0], $params[1], $params[2], $params[3], $params[4]);
		$data['reportlist'] = $this->Laporan_model->LaporanPenjualanPerkategoriList($params[0], $params[1], $params[2], $params[3], $params[4]);
		$data['konfigurasi'] = $this->Laporan_model->Konfigurasi();
		$data['row'] = $this->Laporan_model->Cabang($params[2]);
		$data['tglawal'] = date('d-m-Y', strtotime($params[0]));
		$data['tglakhir'] = date('d-m-Y', strtotime($params[1]));
		$data['cabang'] = $params[2];
		$data['merkawal'] = $this->Laporan_model->GetMerk($params[3]);
		$data['merkakhir'] = $this->Laporan_model->GetMerkAkhir($params[4]);

		$this->load->view('menu/reporting/penjualan/excel_penjuala_perkategori', $data);
	}

	public function KomisiSales($param = "")
	{
		$params = explode(":", urldecode($param));

		$filename = 'Laporan Komisi Sales' . '_' . date('d M Y', strtotime($params[0])) . ' - ' . date('d M Y', strtotime($params[1]));

		// if (!empty($tanggal)){

		$paper = 'A4';
		$orientation = 'landscape';

		// menggunakan class dompdf
		$this->load->library('pdf');

		$data['report'] = $this->Laporan_model->GetDataKomisiSales($params[0], $params[1], $params[2], $params[3]);
		$data['reportrow'] = $this->Laporan_model->GetDataKomisiSalesRow($params[0], $params[1], $params[2], $params[3]);
		$data['ppn'] = $this->Laporan_model->Konfigurasi();
		// $data['list'] = $this->Laporan_model->GetPenjualanSemuaPelangganList($params[0], $params[1], $params[2]);
		// $data['detail'] = $this->Laporan_model->GetPenjualanSemuaPelangganDetail($params[0], $params[1], $params[2]);
		$data['row'] = $this->Laporan_model->Cabang($params[2]);
		$data['tglawal'] = date('d-m-Y', strtotime($params[0]));
		$data['tglakhir'] = date('d-m-Y', strtotime($params[1]));
		$data['cabang'] = $params[2];

		$this->pdf->load_view('menu/cetakan_pdf/r_komisi_sales', $data, $filename, $paper, $orientation);

		// (Opsional) Mengatur ukuran kertas dan orientasi kertas
		$this->pdf->setPaper('F4', 'landscape');

		// Menjadikan HTML sebagai PDF
		$this->pdf->render();

		// Output akan menghasilkan PDF (1 = download dan 0 = preview)
		$this->pdf->stream("welcome.pdf", array("Attachment" => 0));

		exit(0);
		// }
	}

	public function ExportKomisiSales($param = "")
	{
		$params = explode(":", urldecode($param));

		$data['report'] = $this->Laporan_model->GetDataKomisiSales($params[0], $params[1], $params[2], $params[3]);
		$data['reportrow'] = $this->Laporan_model->GetDataKomisiSalesRow($params[0], $params[1], $params[2], $params[3]);
		$data['ppn'] = $this->Laporan_model->Konfigurasi();
		$data['row'] = $this->Laporan_model->Cabang($params[2]);
		$data['tglawal'] = date('d-m-Y', strtotime($params[0]));
		$data['tglakhir'] = date('d-m-Y', strtotime($params[1]));
		$data['cabang'] = $params[2];
		$this->load->view('menu/reporting/penjualan/komisi_sales', $data);
	}

	public function PreviewOutstandingSalesOrder($param = "")
	{
		$params = explode(":", urldecode($param));

		$filename = 'Laporan Outstanding Sales Order';

		// if (!empty($tanggal)){

		$paper = 'A4';
		$orientation = 'landscape';

		// menggunakan class dompdf
		// $this->load->library('pdf');

		$data['report'] = $this->Laporan_model->GetOutstandingSalesOrder($params[0]);
		$data['row'] = $this->Laporan_model->Cabang($params[0]);

		set_time_limit(300);

		$this->load->view('menu/cetakan_pdf/r_outstanding_sales_order', $data, $filename, $paper, $orientation);

		// (Opsional) Mengatur ukuran kertas dan orientasi kertas
		// $this->pdf->setPaper('F4', 'landscape');

		// Menjadikan HTML sebagai PDF
		// $this->pdf->render();

		// Output akan menghasilkan PDF (1 = download dan 0 = preview)
		// $this->pdf->stream("welcome.pdf", array("Attachment" => 0));

		// exit(0);
		// }
	}
	public function OutstandingSalesOrder($param = "")
	{
		// print_r($param);
		// die();
		$params = explode(":", urldecode($param));
		$filename = 'Laporan Outstanding Sales Order';

		// if (!empty($tanggal)){

		$paper = 'A4';
		$orientation = 'landscape';

		// menggunakan class dompdf
		$this->load->library('pdf');

		// if($params[0] == '1' && $params[1] == null){
		$data['report'] = $this->Laporan_model->GetOutstandingSalesOrder($params[0]);
		// }else{
		// 	$data['report'] = $this->Laporan_model->GetOutstandingSalesOrderPerCabang($params[0], $params[1]);
		// }
		// $data['ppn'] = $this->Laporan_model->Konfigurasi();
		// $data['list'] = $this->Laporan_model->GetPenjualanSemuaPelangganList($params[0], $params[1], $params[2]);
		// $data['detail'] = $this->Laporan_model->GetPenjualanSemuaPelangganDetail($params[0], $params[1], $params[2]);
		$data['row'] = $this->Laporan_model->Cabang($params[0]);
		// $data['tglawal'] = date('d-m-Y', strtotime($params[0]));
		// $data['tglakhir'] = date('d-m-Y', strtotime($params[1]));
		// $data['cabang'] = $params[2];

		$this->pdf->load_view('menu/cetakan_pdf/r_outstanding_sales_order', $data, $filename, $paper, $orientation);

		// (Opsional) Mengatur ukuran kertas dan orientasi kertas
		$this->pdf->setPaper('F4', 'landscape');

		// Menjadikan HTML sebagai PDF
		$this->pdf->render();

		// Output akan menghasilkan PDF (1 = download dan 0 = preview)
		$this->pdf->stream("welcome.pdf", array("Attachment" => 0));

		exit(0);
		// }
	}
	public function ExportOutstandingSalesOrder($param = "")
	{
		$params = explode(":", urldecode($param));

		$data['report'] = $this->Laporan_model->GetOutstandingSalesOrder($params[0]);
		$data['row'] = $this->Laporan_model->Cabang($params[0]);
		$this->load->view('menu/reporting/penjualan/excel_outstanding_so', $data);
	}
}
