<?php
defined('BASEPATH') or exit('No direct script access allowed');

class SuratJalanGudang extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('caridataaktif_model');
		$this->load->library('form_validation');
		$this->load->library('session');
	}

	public function Print($nomor = "")
	{
		$paper = array(0, 0, 612, 396);
		$data['sj'] = $this->db->query("SELECT * FROM srvt_suratjalangudang WHERE nomor = '" . $nomor . "'")->row();
		// $data['sj'] = 'Test';

		$data['vsj'] = $this->db->query("SELECT
		g.nama AS namagudang,
		c.nama AS namacabang,
		sjgd.cabangtujuan,
		sjgd.nomor,
		sjgd.tanggal,
		sjgdtl.kodebarang,
		sjgdtl.namabarang,
		sjgdtl.qty,
		sjgd.jenisdokumen,
		tf.nomor AS nomortf,
		s.nama AS namasatuan,
		tf.keterangan
		FROM srvt_suratjalangudang sjgd
		LEFT JOIN srvt_suratjalangudangdetail sjgdtl ON sjgdtl.nomor = sjgd.nomor
		LEFT JOIN srvt_transferstock tf ON tf.nomor = sjgd.noproses
		LEFT JOIN glbm_gudang g ON g.kode = sjgd.lokasi
		LEFT JOIN glbm_cabang c ON c.kode = sjgd.kodecabang
		LEFT JOIN glbm_barang b ON b.kode = sjgdtl.kodebarang
		LEFT JOIN glbm_satuan s ON s.kode = b.kodesatuan
		WHERE sjgd.batal = false AND sjgd.nomor = '" . $nomor . "'
		LIMIT 1")->row();

		$data['asal'] = $this->db->query("SELECT
		c.nama AS namacabangasal,
		g.nama AS namagudangasal
		FROM srvt_transferstock tf
		LEFT JOIN glbm_cabang c ON c.kode = tf.kodecbasal
		LEFT JOIN glbm_gudang g ON g.kode = tf.kodegdasal
		LEFT JOIN srvt_suratjalangudang sjgd ON sjgd.noproses = tf.nomor
		WHERE tf.batal = false AND sjgd.nomor = '" . $nomor . "'")->row();

		$data['tujuan'] = $this->db->query("SELECT
		c.nama AS namacabangtujuan,
		g.nama AS namagudangtujuan
		FROM srvt_transferstock tf
		LEFT JOIN glbm_cabang c ON c.kode = tf.kodecbtujuan
		LEFT JOIN glbm_gudang g ON g.kode = tf.kodegdtujuan
		LEFT JOIN srvt_suratjalangudang sjgd ON sjgd.noproses = tf.nomor
		WHERE tf.batal = false AND sjgd.nomor = '" . $nomor . "'")->row();

		$data['vsjd'] = $this->db->query("SELECT
		g.nama AS namagudang,
		c.nama AS namacabang,
		sjgd.cabangtujuan,
		sjgd.nomor,
		sjgd.tanggal,
		sjgdtl.kodebarang,
		sjgdtl.namabarang,
		sjgdtl.qty,
		tf.nomor AS nomortf,
		s.nama AS namasatuan
		FROM srvt_suratjalangudang sjgd
		LEFT JOIN srvt_suratjalangudangdetail sjgdtl ON sjgdtl.nomor = sjgd.nomor
		LEFT JOIN srvt_transferstock tf ON tf.nomor = sjgd.noproses
		LEFT JOIN glbm_gudang g ON g.kode = sjgd.lokasi
		LEFT JOIN glbm_cabang c ON c.kode = sjgd.kodecabang
		LEFT JOIN glbm_barang b ON b.kode = sjgdtl.kodebarang
		LEFT JOIN glbm_satuan s ON s.kode = b.kodesatuan
		WHERE sjgd.batal = false AND sjgd.nomor = '" . $nomor . "' AND sjgdtl.qty > 0
		ORDER BY sjgdtl.namabarang ASC")->result();

		$orientation = 'portrait';
		// $filename = 'Surat Jalan - ' . $data['sj']->nomor;
		$filename = 'Surat Jalan - ';

		$this->load->library('pdf');
		$this->pdf->load_view('menu/cetakan_pdf/suratjalan_gudang', $data, $filename, $paper, $orientation);

		$this->pdf->setPaper($paper, 'portrait');
		$this->pdf->rander();
		$this->pdf->stream("welcome.pdf", array("Attachment" => 0));

		exit(0);
	}
}
