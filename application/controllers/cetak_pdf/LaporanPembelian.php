<?php
defined('BASEPATH') or exit('No direct script access allowed');

class LaporanPembelian extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('laporan/LaporanPembelian_model');
		$this->load->model('caridataaktif_model');
		$this->load->library('form_validation');
		$this->load->library('session');
	}

	function PrintPembelianPdf($param = "")
	{
		$params = explode(":",urldecode($param));
		// $tglawal = $params[0];
		// $tglakhir = $params[1];
		// $gudang = $params[2];
		// $merkawal = $params[3];
		// $merkakhir = $params[4];

		$filename = 'Laporan Peneriman Barang '. '_' . date('d M Y', strtotime($params[0])) . ' - ' . date('d M Y', strtotime($params[1]));

		
		$paper = 'A4';
		$orientation = 'portrait';
		
		// menggunakan class dompdf
		$this->load->library('pdf');

		$data['report'] = $this->LaporanPembelian_model->LaporanPenerimaanBarangPergudang($params[0], $params[1], $params[2]);
		$data['row'] = $this->LaporanPembelian_model->LaporanPenerimaanBarangPergudangRow($params[0], $params[1], $params[2]);
		$data['reportlist'] = $this->LaporanPembelian_model->LaporanPenerimaanBarangPergudangList($params[0], $params[1], $params[2]);
		$data['tglawal'] = date('Y-m-d',strtotime($params[0]));
		$data['tglakhir'] = date('Y-m-d',strtotime($params[1]));
		$data['gudang'] = $this->LaporanPembelian_model->Gudang($params[2]);

		$this->pdf->load_view('menu/cetakan_pdf/pembelian', $data, $filename, $paper, $orientation);
				
		// (Opsional) Mengatur ukuran kertas dan orientasi kertas
		$this->pdf->setPaper('F4', 'portrait');

		// Menjadikan HTML sebagai PDF
		$this->pdf->render();

		// Output akan menghasilkan PDF (1 = download dan 0 = preview)
		$this->pdf->stream("welcome.pdf", array("Attachment"=>0));

		exit(0);

	
	}

	function PembelianAllgudang($param = "")
	{
		$params = explode(":",urldecode($param));
		// $tglawal = $params[0];
		// $tglakhir = $params[1];
		// $gudang = $params[2];
		// $merkawal = $params[3];
		// $merkakhir = $params[4];

		$filename = 'Laporan Peneriman Barang '. '_' . date('d M Y', strtotime($params[0])) . ' - ' . date('d M Y', strtotime($params[1]));

		
		$paper = 'A4';
		$orientation = 'portrait';
		
		// menggunakan class dompdf
		$this->load->library('pdf');

		$data['report'] = $this->LaporanPembelian_model->LaporanPenerimaanBarangAll($params[0], $params[1], $params[2]);
		$data['row'] = $this->LaporanPembelian_model->LaporanPenerimaanBarangAllRow($params[0], $params[1], $params[2]);
		$data['reportlist'] = $this->LaporanPembelian_model->LaporanPenerimaanBarangAllList($params[0], $params[1], $params[2]);
		$data['tglawal'] = date('Y-m-d',strtotime($params[0]));
		$data['tglakhir'] = date('Y-m-d',strtotime($params[1]));
		$data['cabang'] = $this->LaporanPembelian_model->Cabang($params[2]);

		$this->pdf->load_view('menu/cetakan_pdf/laporanpenerimaanbarangallgudang', $data, $filename, $paper, $orientation);
				
		// (Opsional) Mengatur ukuran kertas dan orientasi kertas
		$this->pdf->setPaper('F4', 'portrait');

		// Menjadikan HTML sebagai PDF
		$this->pdf->render();

		// Output akan menghasilkan PDF (1 = download dan 0 = preview)
		$this->pdf->stream("welcome.pdf", array("Attachment"=>0));

		exit(0);

	
	}

	function LaporanBarangImport($param = "")
	{
		$params = explode(":",urldecode($param));
		// $tglawal = $params[0];
		// $tglakhir = $params[1];
		// $gudang = $params[2];
		// $merkawal = $params[3];
		// $merkakhir = $params[4];

		$filename = 'Laporan Peneriman Barang '. '_' . date('d M Y', strtotime($params[0])) . ' - ' . date('d M Y', strtotime($params[1]));

		
		$paper = 'A4';
		$orientation = 'portrait';
		
		// menggunakan class dompdf
		// $this->load->library('pdf');

		$data['report'] = $this->LaporanPembelian_model->LaporanImport($params[0], $params[1], $params[2]);
		$data['row'] = $this->LaporanPembelian_model->LaporanImportRow($params[0], $params[1], $params[2]);
		$data['reportlist'] = $this->LaporanPembelian_model->LaporanImportList($params[0], $params[1], $params[2]);
		$data['tglawal'] = date('Y-m-d',strtotime($params[0]));
		$data['tglakhir'] = date('Y-m-d',strtotime($params[1]));
		$data['gudang'] = $params[2];

		$this->load->view('menu/cetakan_pdf/laporanimportbarang', $data, $filename, $paper, $orientation);
				
		// (Opsional) Mengatur ukuran kertas dan orientasi kertas
		// $this->pdf->setPaper('F4', 'portrait');

		// Menjadikan HTML sebagai PDF
		// $this->pdf->render();

		// Output akan menghasilkan PDF (1 = download dan 0 = preview)
		// $this->pdf->stream("welcome.pdf", array("Attachment"=>0));

		// exit(0);

	
	}

	// function PrintPembelianExcel($param = "")
	// {
	// 	$params = explode(":",urldecode($param));
	// 	// $tglawal = $params[0];
	// 	// $tglakhir = $params[1];
	// 	// $gudang = $params[2];
	// 	// $merkawal = $params[3];
	// 	// $merkakhir = $params[4];

	// 	$filename = 'Laporan Peneriman Barang '. '_' . date('d M Y', strtotime($params[0])) . ' - ' . date('d M Y', strtotime($params[1]));

		
	// 	$paper = 'A4';
	// 	$orientation = 'portrait';
		
	// 	// menggunakan class dompdf
	// 	$this->load->library('pdf');

	// 	$data['report'] = $this->LaporanPembelian_model->LaporanPenerimaanBarangPergudang($params[0], $params[1], $params[2]);
	// 	$data['row'] = $this->LaporanPembelian_model->GetDataPenerimaanBarangRow($params[0], $params[1], $params[2]);
	// 	$data['tglawal'] = date('Y-m-d',strtotime($params[0]));
	// 	$data['tglakhir'] = date('Y-m-d',strtotime($params[1]));
	// 	$data['gudang'] = $params[2];

	// 	$this->pdf->load_view('menu/cetakan_excel/pembelian', $data, $filename, $paper, $orientation);
				
	// 	// (Opsional) Mengatur ukuran kertas dan orientasi kertas
	// 	$this->pdf->setPaper('F4', 'portrait');

	// 	// Menjadikan HTML sebagai PDF
	// 	$this->pdf->render();

	// 	// Output akan menghasilkan PDF (1 = download dan 0 = preview)
	// 	$this->pdf->stream("welcome.pdf", array("Attachment"=>0));

	// 	exit(0);
	// }

	

	function PrintPembelianExcel($param = "")
	{
		$params = explode(":", urldecode($param));
		$data['report'] = $this->LaporanPembelian_model->LaporanPenerimaanBarangPergudang($params[0], $params[1], $params[2]);
		$data['row'] = $this->LaporanPembelian_model->LaporanPenerimaanBarangPergudangRow($params[0], $params[1], $params[2]);
		$data['reportlist'] = $this->LaporanPembelian_model->LaporanPenerimaanBarangPergudangList($params[0], $params[1], $params[2]);
		$data['tglawal'] = date('Y-m-d',strtotime($params[0]));
		$data['tglakhir'] = date('Y-m-d',strtotime($params[1]));
		$data['gudang'] = $this->LaporanPembelian_model->Gudang($params[2]);
		// $data['cabang'] = $params[3];
		// var_dump($data);
		// die();
		$this->load->view('menu/cetakan_excel/excel_pembelian', $data);
	}

	function PrintPembelianAllGudangExcel($param = "")
	{
		$params = explode(":", urldecode($param));
		$data['report'] = $this->LaporanPembelian_model->LaporanPenerimaanBarangAll($params[0], $params[1], $params[2]);
		$data['row'] = $this->LaporanPembelian_model->LaporanPenerimaanBarangAllRow($params[0], $params[1], $params[2]);
		$data['reportlist'] = $this->LaporanPembelian_model->LaporanPenerimaanBarangAllList($params[0], $params[1], $params[2]);
		$data['tglawal'] = date('Y-m-d',strtotime($params[0]));
		$data['tglakhir'] = date('Y-m-d',strtotime($params[1]));
		$data['cabang'] = $this->LaporanPembelian_model->Cabang($params[2]);
		// $data['cabang'] = $params[3];
		// var_dump($data);
		// die();
		$this->load->view('menu/cetakan_excel/excel_pembelianallgudang', $data);
	}
}
