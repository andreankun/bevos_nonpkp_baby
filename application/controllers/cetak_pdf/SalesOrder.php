<?php
defined('BASEPATH') or exit('No direct script access allowed');

class SalesOrder extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('caridataaktif_model');
		$this->load->library('form_validation');
		$this->load->library('session');
	}

	public function Print($nomorso = "")
	{
		$paper = array(0, 0, 612, 396);
		// $paper = 'A4';

		$data['konfigurasi'] = $this->db->query("SELECT kode FROM stpm_konfigurasi")->row();
		$data['so'] = $this->db->query("SELECT
		so.nomor,
		so.jenisjual,
		so.namapelanggan,
		c.alamat,
		c.nama_toko,
		c.pengiriman,
		c.notelp,
		c.nohp,
		c.nama,
		so.keterangan,
		pos.kodepos,
		pos.kota,
		g.nama as namagudang,
		pro.nama AS namapromo
		FROM
		srvt_salesorder so
		LEFT JOIN glbm_customer c on c.nomor = so.nopelanggan
		LEFT JOIN glbm_kodepos pos on pos.kodepos = c.kodepos
		LEFT JOIN glbm_gudang g on g.kode = so.lokasi
		LEFT JOIN glbm_promo pro ON pro.kode = so.jenisdisc
		WHERE
		so.nomor = '" . $nomorso . "' ")->row();

		$data['sod'] = $this->db->query("SELECT
		sod.namabarang,
		sod.qty,
		s.nama as namasatuan
		FROM
		srvt_sodetail sod
		LEFT JOIN glbm_barang brg on brg.kode = sod.kodebarang
		LEFT JOIN glbm_satuan s on s.kode = brg.kodesatuan
		WHERE sod.nomor = '" . $nomorso . "' AND sod.qty > 0
		ORDER BY sod.namabarang ASC")->result();

		$orientation = 'portrait';
		$filename = 'Sales Order - "' . $data['so']->nomor . '"';

		$this->load->library('pdf');
		$this->pdf->load_view('menu/cetakan_pdf/so', $data, $filename, $paper, $orientation);

		$this->pdf->setPaper($paper, 'portrait');
		$this->pdf->rander();
		$this->pdf->stream("welcome.pdf", array("Attachment" => 0));

		exit(0);
	}
}
