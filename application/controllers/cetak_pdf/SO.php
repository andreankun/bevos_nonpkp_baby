<?php
defined('BASEPATH') or exit('No direct script access allowed');

class SO extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->load->model('caridataaktif_model');
        $this->load->library('form_validation');
        $this->load->library('session');
    }

    public function Print($nomor = "")
    {
        // $paper = 'A4';
        $paper = array(0, 0, 612, 396);
        $data['so'] = $this->db->query("SELECT * FROM srvt_salesorder WHERE nomor = '" . $nomor . "'")->row();
        $data['konfigurasi'] = $this->db->query("SELECT* FROM stpm_konfigurasi")->row();

        $data['vso'] = $this->db->query("SELECT
        so.nomor,
        so.kodesalesman,
        so.lokasi,
        c.nama AS namapelanggan,
        c.nama_toko,
        c.pengiriman,
        c.notelp,
        c.nohp,
        c.alamat,
        pos.kota,
        so.jenisdisc,
        so.jenisjual,
        p.nama AS namapromo,
        a.nama AS namaaccount,
        a.norekening,
        a.namabisnis,
        al.nama AS namaaccountlainlain,
        so.total,
        so.dpp,
        so.ppn,
        so.grandtotal,
        pro.nama AS namapromo
        FROM srvt_salesorder so
        LEFT JOIN glbm_promo p ON p.kode = so.jenisdisc
        LEFT JOIN glbm_gudang g ON g.kode = so.lokasi
        LEFT JOIN glbm_customer c ON c.nomor = so.nopelanggan
        LEFT JOIN glbm_account a ON a.nomor = c.rekeningtf
        LEFT JOIN glbm_kodepos pos ON pos.kodepos = c.kodepos
        LEFT JOIN glbm_accountlainlain al ON al.nomor = a.nomor
        LEFT JOIN glbm_promo pro ON pro.kode = so.jenisdisc        
		WHERE so.nomor = '" . $nomor . "'")->row();

        $data['vsod'] = $this->db->query("SELECT
        so.nomor,
        so.nilaicash,
        so.nilaicustomer,
        sod.namabarang,
        s.nama AS namasatuan,
        so.disccash,
        so.disccustomer,
        sod.qty,
        sod.harga,
        sod.discbarang,
        sod.discbrgitem,
        sod.discpromo,
        sod.discpaketpromo,
        sod.total
        FROM srvt_salesorder so
        LEFT JOIN srvt_sodetail sod ON sod.nomor = so.nomor
        LEFT JOIN glbm_barang b ON b.kode = sod.kodebarang
        LEFT JOIN glbm_satuan s ON s.kode = b.kodesatuan
		WHERE so.nomor = '" . $nomor . "' AND sod.qty > 0
        ORDER BY sod.namabarang ASC")->result();

        $orientation = 'portrait';
        $filename = 'SO - ' . $data['so']->nomor;

        $this->load->library('pdf');
        $this->pdf->load_view('menu/cetakan_pdf/sales_order_nominal', $data, $filename, $paper, $orientation);

        $this->pdf->setPaper($paper, 'portrait');
        $this->pdf->rander();
        $this->pdf->stream("welcome.pdf", array("Attachment" => 0));

        exit(0);
    }
}
