<?php

class Inventorybag_model extends CI_Model
{
	public function GetMaxNomor($nomor = "")
	{
		$this->db->select_max('nomor');
		$this->db->where("left(nomor,10)", $nomor);
		return $this->db->get("srvt_bag")->row();
	}

	function CekNoBag($nomor = "")
	{
		return $this->db->query("SELECT *FROM srvt_bag WHERE nomor = '" . $nomor . "'")->result();
	}

	function CekHist($nomor = "")
	{
		return $this->db->query("SELECT *FROM hist_approve WHERE nodokumen = '" . $nomor . "'")->result();
	}

	function DataBag($nomor = "")
	{
		return $this->db->query("SELECT *FROM srvt_bag WHERE nomor = '" . $nomor . "'")->result();
	}

	function DataBagDetail($nomor = "")
	{
		return $this->db->query("SELECT *FROM srvt_bagdetail WHERE nomor = '" . $nomor . "'")->result();
	}

	function CekBagDetail($nomor = "", $kodebarang = "")
	{
		return $this->db->query("SELECT *FROM srvt_bagdetail WHERE nomor = '" . $nomor . "' AND kodebarang = '" . $kodebarang . "'")->row();
	}

	function SaveData($data = "")
	{
		return $this->db->insert('srvt_bag', $data);
	}

	function SaveBagDetail($data = "")
	{
		return $this->db->insert('srvt_bagdetail', $data);
	}

	function UpdateData($data = "", $nomor = "")
	{
		$this->db->where('nomor', $nomor);
		return $this->db->update('srvt_bag', $data);
	}

	function UpdatedDataDetail($data = "", $nomor = "")
	{
		$this->db->where('nomor', $nomor);
		return $this->db->update('srvt_bagdetail', $data);
	}

	function DeleteDetail($nomor = "", $data = "")
	{
		$this->db->where('nomor', $nomor);
		return $this->db->delete('srvt_bagdetail');
	}

	function ApproveData($nomor = "", $data = "")
	{
		$this->db->where('nomor', $nomor);
		return $this->db->update('srvt_bag', $data);
	}

	function SaveHistoriApprove($data = "")
	{
		return $this->db->insert('hist_approve', $data);
	}

	function CancelData($nomor = "", $data = "")
	{
		$this->db->where('nomor', $nomor);
		return $this->db->update('srvt_bag', $data);
	}
	function DataInv($periode = "", $kodebarang = "", $kodecabang = "", $kodegudang = "")
	{
		return $this->db->query("SELECT * FROM srvt_inventorystok WHERE periode = '" . $periode . "' AND kodebarang = '" . $kodebarang . "' AND kodecabang = '" . $kodecabang . "' AND kodegudang = '" . $kodegudang . "' ")->result();
	}

	function UpdateStok($periode = "", $kodebarang = "", $kodecabang = "", $kodegudang = "", $data = "")
	{
		$this->db->where('periode', $periode);
		$this->db->where('kodebarang', $kodebarang);
		$this->db->where('kodecabang', $kodecabang);
		$this->db->where('kodegudang', $kodegudang);
		return $this->db->update('srvt_inventorystok', $data);
	}
}
