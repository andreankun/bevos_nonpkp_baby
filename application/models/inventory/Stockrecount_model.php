<?php

class Stockrecount_model extends CI_Model{
	function GetMaxNomor($nomor = "")
	{
		$this->db->select_max('nomor');
		$this->db->where("left(nomor,10)", $nomor);
		return $this->db->get('srvt_stockopnamerecount')->row();
	}

	function DataInventory($kodebarang = "")
	{
		return $this->db->query("SELECT *FROM cari_inventorystock WHERE kodebarang = '".$kodebarang."'")->result();
	}

	function CekNoStockRecount($nomor = "")
	{
		return $this->db->query("SELECT *FROM srvt_stockopnamerecount WHERE nomor = '".$nomor."'")->result();
	}
	function DataStockRecount($nomor = "")
	{
		return $this->db->query("SELECT sor.*, sord.* FROM srvt_stockopnamerecount sor left join srvt_stockopnamerecountdetail sord on sord.nomor = sor.nomor WHERE sor.nomor = '".$nomor."'")->result();
	}
	function DataStockRecountDetail($nomor)
	{
		return $this->db->query("SELECT *FROM srvt_stockopnamerecountdetail WHERE nomor = '".$nomor."'")->result();
	}
	function DataStockRecountSelisih($nomor = "") {
		return $this->db->query("SELECT *FROM srvt_stockopnamerecountmin WHERE nomor = '".$nomor."'")->result();
	}
	function SaveData($data = ""){
		return $this->db->insert('srvt_stockopnamerecount', $data);
	}
	function SaveDataDetail($data = "")
	{
		return $this->db->insert('srvt_stockopnamerecountdetail', $data);
	}
	function SaveDataDetail2($data = ""){
		return $this->db->insert('srvt_stockopnamerecountdetail2', $data);
	}

	function UpdateStatusProses($data = "", $nomorsto = ""){
		$this->db->where('nomor', $nomorsto);
		return $this->db->update('srvt_stockopname', $data);
	}

	function SaveDataMinus($data = ""){
		return $this->db->insert('srvt_stockopnamerecountmin', $data);
	}

	function SaveDataRecountHist($data = ""){
		return $this->db->insert('srvt_stockopnamerecounthist', $data);
	}

	function DataStockOpnameRecountMinus($nomor = "")
	{
		return $this->db->query("SELECT *FROM srvt_stockopnamerecountmin WHERE nomor = '".$nomor."'")->result();
	}

	function SaveDataRecountPass($data = ""){
		return $this->db->insert('srvt_stockopnamerecountpass', $data);
	}

	function SaveDataHist($data = ""){
		return $this->db->insert('hist_approve', $data);
	}

	function ApproveData($data = "", $nomor = "")
	{
		$this->db->where('nomor', $nomor);
		return $this->db->update('srvt_stockopnamerecount', $data);
	}

	function UpdateData($data = "", $nomor = "")
	{
		$this->db->where('nomor', $nomor);
		return $this->db->update('srvt_stockopnamerecount', $data);
	}

	function UpdateRecount($nomorsto = "", $data){
		$this->db->where('nomor', $nomorsto);
		return $this->db->update('srvt_stockopname', $data);
	}

	function DeleteDetail($nomor = "")
	{
		$this->db->where('nomor', $nomor);
		return $this->db->delete('srvt_stockopnamerecountdetail');
	}

	function DeleteDetail2($nomor = "")
	{
		$this->db->where('nomor', $nomor);
		return $this->db->delete('srvt_stockopnamerecountdetail2');
	}
}
