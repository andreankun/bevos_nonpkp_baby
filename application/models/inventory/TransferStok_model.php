<?php

class TransferStok_model extends CI_Model
{
    public function GetMaxNomor($nomor = "")
    {
        $this->db->select_max('nomor');
        $this->db->where("left(nomor,10)", $nomor);
        return $this->db->get("srvt_transferstock")->row();
    }

    function DataBarang($kode = "", $kodecabang = "", $kodegudang = "")
    {
        $periode = date('Y') . date('m');
        return $this->db->query("SELECT * FROM cari_barang WHERE kodebarang = '" . $kode . "' AND kodecabang = '" . $kodecabang . "' AND kodegudang = '" . $kodegudang . "' AND periode = '" . $periode . "'")->result();
    }

    function DataAsal($kode = "")
    {
        return $this->db->query("SELECT * FROM glbm_gudang WHERE kode = '" . $kode . "' ")->result();
    }

    function DataCabangTujuan($kode = "")
    {
        return $this->db->query("SELECT * FROM glbm_cabang WHERE kode = '" . $kode . "' ")->result();
    }

    function DataGudangTujuan($kode = "")
    {
        return $this->db->query("SELECT * FROM glbm_gudang WHERE kode = '" . $kode . "' ")->result();
    }

    function DataTransferStok($nomor = "")
    {
        return $this->db->query("SELECT * FROM cari_transferstock WHERE nomor = '" . $nomor . "' ")->result();
    }

    function DataTransferStokDetail($nomor = "")
    {
        return $this->db->query("SELECT TFSD.*, TF.status, TF.kodestatus FROM srvt_transferstockdetail TFSD
        LEFT JOIN cari_transferstock TF ON TF.nomor = TFSD.nomor
        WHERE TFSD.nomor = '" . $nomor . "' ")->result();
    }

    function SaveData($data = "")
    {
        return $this->db->insert("srvt_transferstock", $data);
    }

    function SaveDataDetail($data = "")
    {
        return $this->db->insert("srvt_transferstockdetail", $data);
    }

    function UpdateData($nomor = "", $data = "")
    {
        $this->db->where("nomor", $nomor);
        return $this->db->update("srvt_transferstock", $data);
    }

    function UpdateDataDetail($nomor = "", $data = "")
    {
        $this->db->where("nomor", $nomor);
        return $this->db->update("srvt_transferstockdetail", $data);
    }

    function DataInv($periode = "", $kodebarang = "", $kodecabang = "", $kodegudang = "")
    {
        return $this->db->query("SELECT * FROM srvt_inventorystok WHERE periode = '" . $periode . "' AND kodebarang = '" . $kodebarang . "' AND kodecabang = '" . $kodecabang . "' AND kodegudang = '" . $kodegudang . "' ")->result();
    }

    function UpdateStok($periode = "", $kodebarang = "", $kodecabang = "", $kodegudang = "", $data = "")
    {
        $this->db->where('periode', $periode);
        $this->db->where('kodebarang', $kodebarang);
        $this->db->where('kodecabang', $kodecabang);
        $this->db->where('kodegudang', $kodegudang);
        return $this->db->update('srvt_inventorystok', $data);
    }

    function DeleteTransferStok($nomor = "")
    {
        $this->db->where('nomor', $nomor);
        return $this->db->delete('srvt_transferstockdetail');
    }
}
