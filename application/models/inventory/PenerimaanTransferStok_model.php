<?php

class PenerimaanTransferStok_model extends CI_Model
{
    public function GetMaxNomor($nomor = "")
    {
        $this->db->select_max('nomor');
        $this->db->where("left(nomor,10)", $nomor);
        return $this->db->get("srvt_penerimaantransferstock")->row();
    }

    // function DataInv($periode = "", $kodegdasal = "", $kodebarang = "")
    // {
    //     return $this->db->query("SELECT * FROM cari_inventorystock WHERE periode = '" . $periode . "' AND kodegudang = '" . $kodegdasal . "' AND kodebarang = '" . $kodebarang . "' ")->result();
    // }

    function DataSuratJalanGudang($nomor = "")
    {
        return $this->db->query("SELECT * FROM cari_suratjalangudang WHERE nomor = '" . $nomor . "' ")->result();
    }

    function DataSJG($nomor = "")
    {
        return $this->db->query("SELECT * FROM srvt_suratjalangudang WHERE nomor = '" . $nomor . "' ")->result();
    }

    function DataSuratJalanGudangDetail($nomor = "")
    {
        return $this->db->query("SELECT * FROM srvt_suratjalangudangdetail WHERE nomor = '" . $nomor . "' ")->result();
    }

    function DataPenerimaanStok($nomor = "")
    {
        return $this->db->query("SELECT pts.*, ts.keterangan
        FROM srvt_penerimaantransferstock pts
        LEFT JOIN srvt_suratjalangudang sjgd ON sjgd.nomor = pts.notransfer
        LEFT JOIN srvt_transferstock ts ON ts.nomor = sjgd.noproses
        WHERE pts.nomor = '" . $nomor . "' ")->result();
    }

    function DataBarang($nomor = "", $kode = "")
    {
        return $this->db->query("SELECT * FROM srvt_transferstockdetail WHERE nomor = '" . $nomor . "' AND kodebarang = '" . $kode . "' ")->result();
    }

    function DataPenerimaanTransferStokDetail($nomor = "")
    {
        return $this->db->query("SELECT * FROM srvt_penerimaantransferstockdetail WHERE nomor = '" . $nomor . "' ")->result();
    }

    function SaveData($data = "")
    {
        return $this->db->insert("srvt_penerimaantransferstock", $data);
    }

    function SaveDataDetail($data = "")
    {
        return $this->db->insert("srvt_penerimaantransferstockdetail", $data);
    }

    function DataInv($periode = "", $kodebarang = "", $kodecabang = "", $kodegudang = "")
    {
        return $this->db->query("SELECT * FROM srvt_inventorystok WHERE periode = '" . $periode . "' AND kodebarang = '" . $kodebarang . "' AND kodecabang = '" . $kodecabang . "' AND kodegudang = '" . $kodegudang . "' ")->result();
    }

    function UpdateStok($periode = "", $kodebarang = "", $kodecabang = "", $kodegudang = "", $data = "")
    {
        $this->db->where('periode', $periode);
        $this->db->where('kodebarang', $kodebarang);
        $this->db->where('kodecabang', $kodecabang);
        $this->db->where('kodegudang', $kodegudang);
        return $this->db->update('srvt_inventorystok', $data);
    }

    function UpdateSuratJalanGudang($nomor = "", $data = "")
    {
        $this->db->where("nomor", $nomor);
        return $this->db->update("srvt_suratjalangudang", $data);
    }
    
    function UpdateTransferStok($nomor = "", $data = "")
    {
        $this->db->where("nomor", $nomor);
        return $this->db->update("srvt_transferstock", $data);
    }
}
