<?php

class ProsesRepackStock_model extends CI_Model
{
    public function GetMaxNomor($nomor = "")
    {
        $this->db->select_max('nomor');
        $this->db->where("left(nomor,10)", $nomor);
        return $this->db->get("srvt_prosesrepact")->row();
    }

    function CekProses($nomor = "")
    {
        return $this->db->query("SELECT * FROM srvt_prosesrepact  WHERE nomor = '" . $nomor . "'")->result();
    }

    function DataProses($nomor = "")
    {
        return $this->db->query("SELECT * FROM srvt_prosesrepact WHERE nomor = '" . $nomor . "'")->result();
    }

    function DataReq($nomor = "")
    {
        return $this->db->query("SELECT prs.nosjgudang, sjgd.noproses as nomorreq, sjgd.lokasi, sjgd.asal
        FROM srvt_penerimaanrepack prs
        LEFT JOIN srvt_suratjalangudang sjgd ON sjgd.nomor = prs.nosjgudang
        WHERE prs.nomor = '" . $nomor . "'")->result();
    }

    function DataProsesDetail($nomor = "")
    {
        return $this->db->query("SELECT * FROM srvt_prosesrepactdetail WHERE nomor = '" . $nomor . "'")->result();
    }

    function DataHasilDetail($nomor = "")
    {
        return $this->db->query("SELECT * FROM srvt_prosesrepactdetail2 WHERE nomor = '" . $nomor . "'")->result();
    }

    function DataPenerimaan($nomor = "")
    {
        return $this->db->query("SELECT * FROM srvt_penerimaanrepack WHERE nomor = '" . $nomor . "'")->result();
    }

    function DataPenerimaanDetail($nomor = "")
    {
        return $this->db->query("SELECT * FROM srvt_penerimaanrepackdetail WHERE nomor = '" . $nomor . "'")->result();
    }

    function SaveData($data = "")
    {
        return $this->db->insert('srvt_prosesrepact', $data);
    }

    function SaveDataDetail($data = "")
    {
        return $this->db->insert('srvt_prosesrepactdetail', $data);
    }

    function SaveDataHasilDetail($data = "")
    {
        return $this->db->insert('srvt_prosesrepactdetail2', $data);
    }

    function DataInv($periode = "", $kodebarang = "", $kodecabang = "", $kodegudang = "")
    {
        return $this->db->query("SELECT * FROM srvt_inventorystok WHERE periode = '" . $periode . "' AND kodebarang = '" . $kodebarang . "' AND kodecabang = '" . $kodecabang . "' AND kodegudang = '" . $kodegudang . "' ")->result();
    }

    function UpdateStok($periode = "", $kodebarang = "", $kodecabang = "", $kodegudang = "", $data = "")
    {
        $this->db->where('periode', $periode);
        $this->db->where('kodebarang', $kodebarang);
        $this->db->where('kodecabang', $kodecabang);
        $this->db->where('kodegudang', $kodegudang);
        return $this->db->update('srvt_inventorystok', $data);
    }

    function UpdateReq($nomor = "", $data = "")
    {
        $this->db->where('nomor', $nomor);
        return $this->db->update('srvt_reqrepackstock', $data);
    }

    function UpdatePenerimaan($nomor = "", $data = "")
    {
        $this->db->where('nomor', $nomor);
        return $this->db->update('srvt_penerimaanrepack', $data);
    }

    function UpdateData($nomor = "", $data = "")
    {
        $this->db->where('nomor', $nomor);
        return $this->db->update('srvt_prosesrepact', $data);
    }

    function UpdateDataDetail($nomor = "", $data = "")
    {
        $this->db->where('nomor', $nomor);
        return $this->db->update('srvt_prosesrepactdetail', $data);
    }

    function DeleteDetail($nomor = "")
    {
        $this->db->where('nomor', $nomor);
        return $this->db->delete('srvt_prosesrepactdetail');
    }

    function DeleteHasilDetail($nomor = "")
    {
        $this->db->where('nomor', $nomor);
        return $this->db->delete('srvt_prosesrepactdetail2');
    }
}