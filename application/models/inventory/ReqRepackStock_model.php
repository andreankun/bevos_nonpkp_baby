<?php

class ReqRepackStock_model extends CI_Model
{
    public function GetMaxNomor($nomor = "")
    {
        $this->db->select_max('nomor');
        $this->db->where("left(nomor,10)", $nomor);
        return $this->db->get("srvt_reqrepackstock")->row();
    }

    function CekReq($nomor = "")
    {
        return $this->db->query("SELECT * FROM srvt_reqrepackstock  WHERE nomor = '" . $nomor . "'")->result();
    }

    function DataReq($nomor = "")
    {
        return $this->db->query("SELECT * FROM srvt_reqrepackstock WHERE nomor = '" . $nomor . "'")->result();
    }

    function DataReqDetail($nomor = "")
    {
        return $this->db->query("SELECT * FROM srvt_reqrepackstockdetail WHERE nomor = '" . $nomor . "'")->result();
    }

    function DataBarang($kode = "", $gudang = "")
    {
        $periode = date('Y') . date('m');
        return $this->db->query("SELECT * FROM cari_barang WHERE kodebarang = '" . $kode . "' AND kodegudang = '" . $gudang . "' AND periode = '" . $periode . "' ")->result();
    }

    function SaveData($data = "")
    {
        return $this->db->insert('srvt_reqrepackstock', $data);
    }

    function SaveDataDetail($data = "")
    {
        return $this->db->insert('srvt_reqrepackstockdetail', $data);
    }

    function UpdateData($nomor = "", $data = "")
    {
        $this->db->where('nomor', $nomor);
        return $this->db->update('srvt_reqrepackstock', $data);
    }

    function UpdateDataDetail($nomor = "", $data = "")
    {
        $this->db->where('nomor', $nomor);
        return $this->db->update('srvt_reqrepackstockdetail', $data);
    }

    function DataInv($periode = "", $kodebarang = "", $kodecabang = "", $kodegudang = "")
    {
        return $this->db->query("SELECT * FROM srvt_inventorystok WHERE periode = '" . $periode . "' AND kodebarang = '" . $kodebarang . "' AND kodecabang = '" . $kodecabang . "' AND kodegudang = '" . $kodegudang . "' ")->result();
    }

    function UpdateStok($periode = "", $kodebarang = "", $kodecabang = "", $kodegudang = "", $data = "")
    {
        $this->db->where('periode', $periode);
        $this->db->where('kodebarang', $kodebarang);
        $this->db->where('kodecabang', $kodecabang);
        $this->db->where('kodegudang', $kodegudang);
        return $this->db->update('srvt_inventorystok', $data);
    }

    function DeleteDetail($nomor = "")
    {
        $this->db->where('nomor', $nomor);
        return $this->db->delete('srvt_reqrepackstockdetail');
    }
}
