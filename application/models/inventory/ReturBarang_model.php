<?php

class ReturBarang_model extends CI_Model
{
    public function GetMaxNomor($nomor = "")
    {
        $this->db->select_max('nomor');
        $this->db->where("left(nomor,10)", $nomor);
        return $this->db->get("srvt_returbarang")->row();
    }

    function CekRB($nomor = "")
    {
        return $this->db->query("SELECT * FROM srvt_invoice WHERE nomor = '" . $nomor . "' ")->result();
    }

    function DataReturBarang($nomor = "")
    {
        return $this->db->query("SELECT * FROM srvt_returbarang WHERE nomor = '" . $nomor . "'")->result();
    }

    function DataReturBarangDetail($nomor = "")
    {
        return $this->db->query("SELECT * FROM srvt_returbarangdetail WHERE nomor = '" . $nomor . "' ")->result();
    }

    function DataInvoice($nomor = "")
    {
        return $this->db->query("SELECT * FROM srvt_invoice WHERE nomor = '" . $nomor . "'")->result();
    }

    function DataPiutang($nomor = "")
    {
        return $this->db->query("SELECT * FROM srvt_piutang WHERE nomor = '" . $nomor . "'")->row();
    }

    function DataInvoiceDetail($kodebarang = "", $kodecabang = "")
    {
        return $this->db->query("SELECT * FROM glbm_barangdetail WHERE kodecabang = '" . $kodecabang . "' AND kodebarang = '" . $kodebarang . "' AND aktif = true")->result();
    }

    function DataInventory($periode = "", $kodegudang = "", $kodebarang = "")
    {
        return $this->db->query("SELECT * FROM srvt_inventorystok WHERE periode = '" . $periode . "' AND kodegudang = '" . $kodegudang . "' AND kodebarang = '" . $kodebarang . "'")->result();
    }

    function SaveData($data = "")
    {
        return $this->db->insert('srvt_returbarang', $data);
    }

    function SaveDataDetail($data = "")
    {
        return $this->db->insert('srvt_returbarangdetail', $data);
    }

    function UpdateData($nomor = "", $data = "")
    {
        $this->db->where('nomor', $nomor);
        return $this->db->update('srvt_returbarang', $data);
    }

    function UpdateDataPiutang($nomor = "", $data = "")
    {
        $this->db->where('nomor', $nomor);
        return $this->db->update('srvt_piutang', $data);
    }

    function UpdateDataDetail($nomor = "", $data = "")
    {
        $this->db->where('nomor', $nomor);
        return $this->db->update('srvt_returbarangdetail', $data);
    }

    function UpdateDataInventory($table = "", $setdata = "", $where = "")
    {
        // return $this->db->set($dataupdate);
        // $this->db->where($where);
        // $this->db->update($table);

        return $this->db->query("UPDATE " . $table . " SET " . $setdata . " WHERE " . $where);
    }
    // function UpdateDataInventory($periode = "", $kodegudang = "", $kodebarang = "", $data = "")
    // {
    //     $this->db->where('periode', $periode);
    //     $this->db->where('kodegudang', $kodegudang);
    //     $this->db->where('kodebarang', $kodebarang);
    //     return $this->db->update('srvt_inventorystok', $data);
    // }

    function DeleteDetail($nomor = "")
    {
        $this->db->where('nomor', $nomor);
        return $this->db->delete('srvt_returbarangdetail');
    }
}
