<?php

class PenerimaanRepack_model extends CI_Model
{
    public function GetMaxNomor($nomor = "")
    {
        $this->db->select_max('nomor');
        $this->db->where("left(nomor,10)", $nomor);
        return $this->db->get("srvt_penerimaanrepack")->row();
    }

    function CekPenerimaan($nomor = "")
    {
        return $this->db->query("SELECT * FROM srvt_penerimaanrepack  WHERE nomor = '" . $nomor . "'")->result();
    }

    function DataPenerimaan($nomor = "")
    {
        return $this->db->query("SELECT * FROM srvt_penerimaanrepack WHERE nomor = '" . $nomor . "'")->result();
    }

    function DataPenerimaanDetail($nomor = "")
    {
        return $this->db->query("SELECT * FROM srvt_penerimaanrepackdetail WHERE nomor = '" . $nomor . "'")->result();
    }

    function DataSJGudang($nomor = "")
    {
        return $this->db->query("SELECT * FROM srvt_suratjalangudang WHERE nomor = '" . $nomor . "'")->result();
    }

    function DataSJGudangDetail($nomor = "")
    {
        return $this->db->query("SELECT * FROM srvt_suratjalangudangdetail WHERE nomor = '" . $nomor . "'")->result();
    }

    function SaveData($data = "")
    {
        return $this->db->insert('srvt_penerimaanrepack', $data);
    }

    function SaveDataDetail($data = "")
    {
        return $this->db->insert('srvt_penerimaanrepackdetail', $data);
    }

    function DataInv($periode = "", $kodebarang = "", $kodecabang = "", $kodegudang = "")
    {
        return $this->db->query("SELECT * FROM srvt_inventorystok WHERE periode = '" . $periode . "' AND kodebarang = '" . $kodebarang . "' AND kodecabang = '" . $kodecabang . "' AND kodegudang = '" . $kodegudang . "' ")->result();
    }

    function UpdateStok($periode = "", $kodebarang = "", $kodecabang = "", $kodegudang = "", $data = "")
    {
        $this->db->where('periode', $periode);
        $this->db->where('kodebarang', $kodebarang);
        $this->db->where('kodecabang', $kodecabang);
        $this->db->where('kodegudang', $kodegudang);
        return $this->db->update('srvt_inventorystok', $data);
    }

    function UpdateReq($nomor = "", $data = "")
    {
        $this->db->where('nomor', $nomor);
        return $this->db->update('srvt_reqrepackstock', $data);
    }

    function UpdateSJGudang($nomor = "", $data = "")
    {
        $this->db->where('nomor', $nomor);
        return $this->db->update('srvt_suratjalangudang', $data);
    }
    
    function UpdateData($nomor = "", $data = "")
    {
        $this->db->where('nomor', $nomor);
        return $this->db->update('srvt_penerimaanrepack', $data);
    }

    function UpdateDataDetail($nomor = "", $data = "")
    {
        $this->db->where('nomor', $nomor);
        return $this->db->update('srvt_penerimaanrepackdetail', $data);
    }

    function DeleteDetail($nomor = "")
    {
        $this->db->where('nomor', $nomor);
        return $this->db->delete('srvt_penerimaanrepackdetail');
    }
}
