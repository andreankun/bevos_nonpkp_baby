<?php
class Suratjalangudang_model extends CI_Model
{
	public function GetMaxNomor($nomor = "")
	{
		$this->db->select_max('nomor');
		$this->db->where("left(nomor,10)", $nomor);
		return $this->db->get('srvt_suratjalangudang')->row();
	}

	function CekNoSuratJalanGudang($nomor = "")
	{
		return $this->db->query("SELECT *FROM srvt_suratjalangudang WHERE nomor = '" . $nomor . "'")->result();
	}

	function DataSuratJalanGudang($nomor = "")
	{
		return $this->db->query("SELECT *FROM srvt_suratjalangudang WHERE nomor = '" . $nomor . "'")->result();
	}

	function DataSuratJalanGudangDetail($nomor = "")
	{
		return $this->db->query("SELECT *FROM srvt_suratjalangudangdetail WHERE nomor = '" . $nomor . "'")->result();
	}

	function DataTransferStok($nomor = "")
    {
        return $this->db->query("SELECT * FROM srvt_transferstock WHERE nomor = '" . $nomor . "' ")->result();
    }

	function DataReqrepack($nomor = "")
    {
        return $this->db->query("SELECT * FROM srvt_reqrepackstock WHERE nomor = '" . $nomor . "' ")->result();
    }

	function DataTransferStokDetail($nomor = "")
	{
		return $this->db->query("SELECT *FROM srvt_transferstockdetail WHERE nomor = '" . $nomor . "'")->result();
	}

	function DataProsesDetail($nomor = "")
	{
		return $this->db->query("SELECT *FROM srvt_prosesrepactdetail2 WHERE nomor = '" . $nomor . "'")->result();
	}
	
	function CekSuratJalanGudangDetail($nomor = "")
	{
		return $this->db->query("SELECT *FROM srvt_suratjalangudangdetail WHERE nomor = '" . $nomor . "'")->result();
	}

	function DataProses($nomor = "")
	{
		return $this->db->query("SELECT *FROM  srvt_prosesrepact WHERE nomor = '" . $nomor . "'")->result();
	}

	function SaveData($data = "")
	{
		return $this->db->insert('srvt_suratjalangudang', $data);
	}
	function SaveDataStok($data = "")
	{
		return $this->db->insert('srvt_inventorystok', $data);
	}

	function SaveSjGudangDetail($data = "")
	{
		return $this->db->insert('srvt_suratjalangudangdetail', $data);
	}

	function UpdateData($data = "", $nomor = "")
	{
		$this->db->where('nomor', $nomor);
		return $this->db->update('srvt_suratjalangudang', $data);
	}

	function UpdateReq($data = "", $nomor = "")
	{
		$this->db->where('nomor', $nomor);
		return $this->db->update('srvt_reqrepackstock', $data);
	}

	function UpdatePros($data = "", $nomor = "")
	{
		$this->db->where('nomor', $nomor);
		return $this->db->update('srvt_prosesrepact', $data);
	}

	function DeleteDetail($nomor = "")
	{
		$this->db->where('nomor', $nomor);
		return $this->db->delete('srvt_suratjalangudangdetail');
	}
	
	function DataInv($periode = "", $kode = "", $kodegudang = "", $kodecabang)
	{
		return $this->db->query("SELECT *FROM srvt_inventorystok WHERE periode = '" . $periode . "' AND kodebarang = '" . $kode . "' AND kodegudang = '" . $kodegudang . "' AND kodecabang = '" . $kodecabang . "'")->result();
	}

	function UpdateStok($periode = "", $kode = "", $kodecabang = "", $kodegudang = "", $data = "")
	{
		$this->db->where('periode', $periode);
		$this->db->where('kodebarang', $kode);
		$this->db->where('kodecabang', $kodecabang);
		$this->db->where('kodegudang', $kodegudang);
		return $this->db->update('srvt_inventorystok', $data);
	}
}
