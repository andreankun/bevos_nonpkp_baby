<?php

class PackingBarang_model extends CI_Model
{
    public function GetMaxNomor($nomor = "")
    {
        $this->db->select_max('nomor');
        $this->db->where("left(nomor,10)", $nomor);
        return $this->db->get("srvt_packingbarang")->row();
    }

    function CekNomor($nomor = "")
    {
        return $this->db->query("SELECT nomor FROM srvt_packingbarang WHERE nomor = '" . $nomor . "' ")->result();
    }

    function DataPackingBarang($nomor = "")
    {
        return $this->db->query("SELECT * FROM srvt_packingbarang WHERE nomor = '" . $nomor . "' ")->result();
    }

    function DataPackingBarangDetail($nomor = "")
    {
        return $this->db->query("SELECT * FROM srvt_packingbarangdetail WHERE nomor = '" . $nomor . "' ")->result();
    }

    function DataSalesOrder($nomor = "")
    {
        return $this->db->query("SELECT * FROM srvt_salesorder WHERE nomor = '" . $nomor . "' ")->result();
    }

    function DataSODetail($nomor = "", $kode = "")
    {
        return $this->db->query("SELECT * FROM srvt_sodetail WHERE nomor = '" . $nomor . "' AND kodebarang = '" . $kode . "' ")->result();
    }

    function DataSalesDetail($nomor = "")
    {
        return $this->db->query("SELECT * FROM srvt_sodetail WHERE nomor = '" . $nomor . "' ")->result();
    }

    function SaveData($data = "")
    {
        return $this->db->insert('srvt_packingbarang', $data);
    }

    function SaveDataDetail($data = "")
    {
        return $this->db->insert('srvt_packingbarangdetail', $data);
    }

    function UpdateStatusSO($nomor = "", $data = "")
    {
        $this->db->where('nomor', $nomor);
        return $this->db->update('srvt_salesorder', $data);
    }
}
