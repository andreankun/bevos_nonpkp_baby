<?php

class Stokopname_model extends CI_Model
{
	public function GetMaxNomer($nomor = "")
	{
		$this->db->select_max('nomor');
		$this->db->where("left(nomor,10)", $nomor);
		return $this->db->get("srvt_stockopname")->row();
	}
	function CekOpName($nomor = "")
	{
		return $this->db->query("SELECT *FROM srvt_stockopname WHERE nomor = '" . $nomor . "'")->result();
	}
	function DataStockOpname($nomor = "")
	{
		return $this->db->query("SELECT *FROM srvt_stockopname WHERE nomor = '" . $nomor . "'")->result();
	}

	function DataStockOpnameAll($nomor = "")
	{
		return $this->db->query("SELECT *FROM cari_stockopname WHERE nomor = '" . $nomor . "'")->result();
	}

	function DataStockOpnameCari($tglawal = "", $tglakhir = "", $kodegudang = "")
	{
		if ($kodegudang == "") {
			return $this->db->query("SELECT 
		so.nomor,
		so.tanggal,
		so.keterangan,
		so.kodegudang,
		so.status,
		so.tglrecount,
		sod.namabarang,
		sod.kodebarang,
		sod.qtysistem,
		sod.qty,
		sod.keterangan as ketdetail 
		from srvt_stockopname so 
		LEFT JOIN srvt_stockopnamedetail sod on sod.nomor = so.nomor
		WHERE to_char(tanggal, 'YYYYMMDD') >= '" . str_replace('/', '-', $tglawal) . "'AND to_char(tanggal, 'YYYYMMDD') <= '" . str_replace('/', '-', $tglakhir) . "'")->result();
		} else {
			return $this->db->query("SELECT 
		so.nomor,
		so.tanggal,
		so.keterangan,
		so.kodegudang,
		so.status,
		so.tglrecount,
		sod.namabarang,
		sod.kodebarang,
		sod.qtysistem,
		sod.qty,
		sod.keterangan as ketdetail 
		from srvt_stockopname so 
		LEFT JOIN srvt_stockopnamedetail sod on sod.nomor = so.nomor
		WHERE to_char(tanggal, 'YYYYMMDD') >= '" . str_replace('/', '-', $tglawal) . "'AND to_char(tanggal, 'YYYYMMDD') <= '" . str_replace('/', '-', $tglakhir) . "' AND kodegudang = '".$kodegudang."'")->result();
		}
	}

	function DataInventory($kodebarang = "", $kodegudang = "")
	{
		return $this->db->query("SELECT *FROM cari_inventorystock WHERE kodebarang = '" . $kodebarang . "' AND kodegudang = '" . $kodegudang . "'")->result();
	}

	function CekStockOpnameDetail($nomor = "")
	{
		return $this->db->query("SELECT *FROM srvt_stockopnamedetail WHERE nomor = '" . $nomor . "'")->result();
	}
	function DataStockOpnameDetail($nomor = "")
	{
		return $this->db->query("SELECT *FROM srvt_stockopnamedetail WHERE nomor = '" . $nomor . "'")->result();
	}
	function SaveData($data = "")
	{
		return $this->db->insert('srvt_stockopname', $data);
	}
	function SaveDataDetail($data = "")
	{
		return $this->db->insert('srvt_stockopnamedetail', $data);
	}
	function UpdateData($nomor = "", $data = "")
	{
		$this->db->where('nomor', $nomor);
		return $this->db->update('srvt_stockopname', $data);
	}
	function CancelRecount($nomor = "", $data = ""){
		$this->db->where('nomor', $nomor);
		return $this->db->update('srvt_stockopname', $data);
	}
	function SaveHistCancel($data){
		return $this->db->insert('hist_cancel', $data);
	}
	function UpdateDataDetail($nomor = "", $data = "")
	{
		$this->db->where('nomor', $nomor);
		return $this->db->update('srvt_stockopnamedetail', $data);
	}
	function DeleteDetail($nomor = "")
	{
		$this->db->where('nomor', $nomor);
		return $this->db->delete('srvt_stockopnamedetail');
	}
}
