<?php

class Promo_model extends CI_Model
{
    function CekPromo($kode = "")
    {
        return $this->db->query("SELECT * FROM glbm_promo WHERE kode = '" . $kode . "' ")->result();
    }

    // function CekPromoDetail($kode = "")
    // {
    //     return $this->db->query("SELECT * FROM glbm_promodetail WHERE kode = '" . $kode . "'")->result();
    // }
	public function GetMaxNomor($kode= "")
    {
        $this->db->select_max('kode');
        $this->db->where("left(kode,7)",$kode);
        return $this->db->get("glbm_promo")->row();
    }
    function DataPromo($kode = "")
    {
        return $this->db->query("SELECT * FROM glbm_promo WHERE kode = '" . $kode . "' ")->result();
    }

    function DataPromoDetail($kode = "")
    {
        return $this->db->query("SELECT * FROM glbm_promodetail WHERE kode = '" . $kode . "' ")->result();
    }

    function SaveData($data = "")
    {
        return $this->db->insert('glbm_promo', $data);
    }
    function SaveDataDetail($data = "")
    {
        return $this->db->insert('glbm_promodetail', $data);
    }

    function UpdateData($data = "", $kode = "")
    {
        $this->db->where('kode', $kode);
        return $this->db->update('glbm_promo', $data);
    }

    function UpdateDataDetail($kode = "", $data = "")
    {
        $this->db->where('kode', $kode);
        return $this->db->update('glbm_promodetail', $data);
    }

    function DeleteDetail($kode = "")
    {
        $this->db->where('kode', $kode);
        return $this->db->delete('glbm_promodetail');
    }
}
