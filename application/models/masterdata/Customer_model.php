<?php

class Customer_model extends CI_Model
{

    public function GetMaxNomor($nomor = "")
    {
        $this->db->select_max('nomor');
        $this->db->where("left(nomor,1)", $nomor);
        return $this->db->get("glbm_customer")->row();
    }

    function DataCustomer($nomor = "")
    {
        return $this->db->query("SELECT * FROM cari_pelanggan WHERE nomor = '" . $nomor . "' ")->result();
    }

    function DataDetailCustomer($nomor = "")
    {
        return $this->db->query("SELECT * FROM glbm_customerdetail WHERE nomor = '" . $nomor . "' ")->result();
    }

    function SaveData($data = "")
    {
        return $this->db->insert('glbm_customer', $data);
    }

    function SaveDataDetail($data = "")
    {
        return $this->db->insert('glbm_customerdetail', $data);
    }

    function DeleteDetail($nomor = "")
    {
        $this->db->where('nomor', $nomor);
        return $this->db->delete('glbm_customerdetail');
    }


    function UpdateData($data = "", $nomor = "")
    {
        $this->db->where('nomor', $nomor);
        return $this->db->update('glbm_customer', $data);
    }

    function DataPelanggan($searchTerm = "", $cabang = "")
    {
        $this->db->select("*");
        $this->db->where("nama_toko like '%" . $searchTerm . "%' AND aktif = true AND kodecabang = '" . $cabang . "'");
        $fetched_records = $this->db->get("glbm_customer");
        $datapelanggan = $fetched_records->result_array();


        $data = array();
        foreach ($datapelanggan as $val) {

            $data[] = array(
                "id" => $val['nomor'],
                "text" => $val['nama_toko'],
                "nama" => $val['nama']
            );
        }

        return $data;
    }
}
