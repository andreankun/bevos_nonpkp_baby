<?php

class Kodepos_model extends CI_Model
{
    public function GetMaxNomor($kode = "")
    {
        $this->db->select_max('kode');
        $this->db->where("left(kode,3)", $kode);
        return $this->db->get("glbm_kodepos")->row();
    }

    function CekKode($kode = "")
    {
        return $this->db->query("SELECT kode FROM glbm_kodepos WHERE kode = '" . $kode . "' ")->result();
    }

    function DataKodePos($kode = "")
    {
        return $this->db->query("SELECT * FROM glbm_kodepos WHERE kode = '" . $kode . "' ")->result();
    }

    function SaveData($data = "")
    {
        return $this->db->insert('glbm_kodepos', $data);
    }

    function UpdateData($data = "", $kode = "")
    {
        $this->db->where('kode', $kode);
        // $this->db->where('kelurahan', $kelurahan);
        return $this->db->update('glbm_kodepos', $data);
    }
}
