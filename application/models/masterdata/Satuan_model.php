<?php

class Satuan_model extends CI_Model
{
    function CekKode($kode = "")
    {
        return $this->db->query("SELECT * FROM glbm_satuan WHERE kode = '" . $kode . "' ")->result();
    }
	public function GetMaxNomor($kode= "")
    {
        $this->db->select_max('kode');
        $this->db->where("left(kode,2)",$kode);
        return $this->db->get("glbm_satuan")->row();
    }
    function DataSatuan($kode = "")
    {
        return $this->db->query("SELECT * FROM glbm_satuan WHERE kode = '" . $kode . "' ")->result();
    }

    function SaveData($data = "")
    {
        return $this->db->insert('glbm_satuan', $data);
    }

    function UpdateData($data = "", $kode = "")
	{
		$this->db->where('kode', $kode);
		return $this->db->update('glbm_satuan', $data);
	}
}
