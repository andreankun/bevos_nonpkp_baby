<?php

class Disckhususpelanggan_model extends CI_Model
{
    function CekKode($nomorpelanggan = "")
    {
        return $this->db->query("SELECT * FROM glbm_discpelanggankategori WHERE nopelanggan = '" . $nomorpelanggan . "' ")->result();
    }
	// public function GetMaxNomor($kode= "")
    // {
    //     $this->db->select_max('kode');
    //     $this->db->where("left(kode,4)",$kode);
    //     return $this->db->get("glbm_discpelanggankategori")->row();
    // }
    function DataDiscKhususPelanggan($nomorpelanggan = "")
    {
        return $this->db->query("SELECT * FROM glbm_discpelanggankategori WHERE nopelanggan = '" . $nomorpelanggan . "' ")->result();
    }

	function DataDiscKhususPelangganDetail($nomorpelanggan = "")
    {
        return $this->db->query("SELECT * FROM glbm_discpelanggankategoridetail WHERE nopelanggan = '" . $nomorpelanggan . "' ")->result();
    }

    function SaveData($data = "")
    {
        return $this->db->insert('glbm_discpelanggankategori', $data);
    }

	function SaveDataDetail($data = "")
    {
        return $this->db->insert('glbm_discpelanggankategoridetail', $data);
    }

	function DeleteDetail($nomorpelanggan = "")
    {
        $this->db->where('nopelanggan', $nomorpelanggan);
        return $this->db->delete('glbm_discpelanggankategoridetail');
    }

    function UpdateData($data = "", $nomorpelanggan = "")
    {
        $this->db->where('nopelanggan', $nomorpelanggan);
        return $this->db->update('glbm_discpelanggankategori', $data);
    }
}
