<?php

class Merk_model extends CI_Model
{
    function CekKode($kode = "")
    {
        return $this->db->query("SELECT * FROM glbm_merk WHERE kode = '" . $kode . "' ")->result();
    }

    function DataMerk($kode = "")
    {
        return $this->db->query("SELECT * FROM glbm_merk WHERE kode = '" . $kode . "' ")->result();
    }

    function SaveData($data = "")
    {
        return $this->db->insert('glbm_merk', $data);
    }

    function UpdateData($data = "", $kode = "")
	{
		$this->db->where('kode', $kode);
		return $this->db->update('glbm_merk', $data);
	}

	function DataMerkSelect($searchTerm = "", $cabang = "")
    {
        $this->db->select("*");
        $this->db->where("kode like '%" . $searchTerm . "%' AND aktif = true");
		$this->db->order_by("kode", "asc");
        $fetched_records = $this->db->get("glbm_merk");
        $datamerk = $fetched_records->result_array();


        $data = array();
        foreach ($datamerk as $val) {

            $data[] = array(
                "id" => $val['kode'],
                "nama" => $val['nama']
            );
        }

        return $data;
    }

	function DataMerkSelectAkhir($searchTerm = "", $cabang = "")
    {
        $this->db->select("*");
        $this->db->where("kode like '%" . $searchTerm . "%' AND aktif = true");
		$this->db->order_by("kode", "desc");
        $fetched_records = $this->db->get("glbm_merk");
        $datamerk = $fetched_records->result_array();


        $data = array();
        foreach ($datamerk as $val) {

            $data[] = array(
                "id" => $val['kode'],
                "nama" => $val['nama']
            );
        }

        return $data;
    }
}
