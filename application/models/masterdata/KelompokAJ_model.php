<?php

class KelompokAJ_model extends CI_Model
{
    function CekKelompokAJ($kode = "")
    {
        return $this->db->query("SELECT * FROM glbm_kelompokaj WHERE kode = '" . $kode . "' ")->result();
    }

    // function CekKelompokAJDetail($kode = "")
    // {
    //     return $this->db->query("SELECT * FROM glbm_kelompokajdetail WHERE kode = '" . $kode . "'")->result();
    // }
	public function GetMaxNomor($kode= "")
    {
        $this->db->select_max('kode');
        $this->db->where("left(kode,6)",$kode);
        return $this->db->get("glbm_kelompokaj")->row();
    }
    function DataKelompokAJ($kode = "")
    {
        return $this->db->query("SELECT * FROM glbm_kelompokaj WHERE kode = '" . $kode . "' ")->result();
    }

    function DataKelompokAJDetail($kode = "")
    {
        return $this->db->query("SELECT b.kode as kodebarang, b.nama as namabarang FROM glbm_kelompokajdetail kaj LEFT JOIN glbm_barang b ON b.kode = kaj.kodebarang WHERE kaj.kode = '" . $kode . "' ")->result();
    }

    function SaveData($data = "")
    {
        return $this->db->insert('glbm_kelompokaj', $data);
    }
    function SaveDataDetail($data = "")
    {
        return $this->db->insert('glbm_kelompokajdetail', $data);
    }

    function UpdateData($data = "", $kode = "")
    {
        $this->db->where('kode', $kode);
        return $this->db->update('glbm_kelompokaj', $data);
    }

    function UpdateDataDetail($kode = "", $data = "")
    {
        $this->db->where('kode', $kode);
        return $this->db->update('glbm_kelompokajdetail', $data);
    }

    function DeleteDetail($kode = "")
    {
        $this->db->where('kode', $kode);
        return $this->db->delete('glbm_kelompokajdetail');
    }
}
