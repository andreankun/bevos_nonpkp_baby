<?php
class DiskonKhusus_model extends CI_Model
{
    function CekKode($nomorpelanggan = "")
    {
        return $this->db->query("SELECT *FROM glbm_diskonkhusus WHERE nomorpelanggan = '" . $nomorpelanggan . "'")->result();
    }
	
    function DataDiskon($nomorpelanggan = "")
    {
        return $this->db->query("SELECT *FROM glbm_diskonkhusus WHERE nomorpelanggan = '" . $nomorpelanggan . "'")->result();
    }

    function SaveData($data = "")
    {
        return $this->db->insert('glbm_diskonkhusus', $data);
    }

	function SaveDataDetail($data = "")
    {
        return $this->db->insert('glbm_diskonkhsusdetail', $data);
    }

	function DataDetailDiskon($nomorpelanggan = "")
    {
		return $this->db->query("SELECT *FROM glbm_diskonkhsusdetail WHERE nomorpelanggan = '" . $nomorpelanggan . "'")->result();
    }

	function DeleteDetail($nomorpelanggan = "")
    {
        $this->db->where('nomorpelanggan', $nomorpelanggan);
        return $this->db->delete('glbm_diskonkhsusdetail');
    }

    function UpdateData($data = "", $nomorpelanggan)
    {
        $this->db->where('nomorpelanggan', $nomorpelanggan);
        return $this->db->update('glbm_diskonkhusus', $data);
    }
}
