<?php

class Barang_model extends CI_Model
{
    function CekBarang($kode = "")
    {
        return $this->db->query("SELECT * FROM glbm_barang WHERE kode = '" . $kode . "' ")->result();
    }

    function CekInventory($kodegudang = "", $kodebarang = "")
    {
        return $this->db->query("SELECT * FROM srvt_inventorystok WHERE kodegudang = '" . $kodegudang . "' AND kodebarang = '" . $kodebarang . "'")->result();
    }

    function CekBarangDetail($kodebarang = "")
    {
        return $this->db->query("SELECT * FROM glbm_barangdetail WHERE kodebarang = '" . $kodebarang . "'")->result();
    }

    function DataBarang($kode = "")
    {
        return $this->db->query("SELECT
        b.*,
        bd.kodecabang,
        COALESCE((((inv.qtyawal + inv.qtymasuk) - inv.qtykeluar) - inv.qtypick)) AS stock
        FROM glbm_barang b
        LEFT JOIN srvt_inventorystok inv ON inv.kodebarang = b.kode
        LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode
        WHERE b.kode = '" . $kode . "' ")->result();
    }

    function DataBarangDetail($kode = "")
    {
        return $this->db->query("SELECT * FROM glbm_barangdetail WHERE kodebarang = '" . $kode . "' ")->result();
    }

    function SaveData($data = "")
    {
        return $this->db->insert('glbm_barang', $data);
    }

    function SaveDataDetail($data = "")
    {
        return $this->db->insert('glbm_barangdetail', $data);
    }

    function SaveDataInv($data = "")
    {
        return $this->db->insert('srvt_inventorystok', $data);
    }

    function UpdateData($data = "", $kode = "")
    {
        $this->db->where('kode', $kode);
        return $this->db->update('glbm_barang', $data);
    }

	function UpdateBarangDetail($data = "", $kode = ""){
		$this->db->where('kodebarang', $kode);
		return $this->db->update('glbm_barangdetail', $data);
	}

    function UpdateDataDetail($kode = "", $data = "")
    {
        $this->db->where('kode', $kode);
        return $this->db->update('glbm_barangdetail', $data);
    }

    function DeleteDetail($kode = "")
    {
        $this->db->where('kode', $kode);
        return $this->db->delete('glbm_barangdetail');
    }

    function DeleteInventory($periode = "")
    {
        $this->db->where('periode', $periode);
        return $this->db->delete('srvt_inventorystok');
    }

    function DataBrg($searchTerm = "")
    {
        $this->db->select("*");
        $this->db->where("nama like '%" . $searchTerm . "%' AND aktif = true");
        $this->db->order_by("nama", "asc");
        $fetched_records = $this->db->get("glbm_barang");
        $databarang = $fetched_records->result_array();


        $data = array();
        foreach ($databarang as $val) {

            $data[] = array(
                "id" => $val['kode'],
                "text" => $val['nama']
            );
        }

        return $data;
    }
}
