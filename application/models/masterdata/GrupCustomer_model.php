<?php

class GrupCustomer_model extends CI_Model
{
    function CekKode($kode = "")
    {
        return $this->db->query("SELECT * FROM glbm_grupcustomer WHERE kode = '" . $kode . "' ")->result();
    }

	public function GetMaxNomor($kode= "")
    {
        $this->db->select_max('kode');
        $this->db->where("left(kode,4)",$kode);
        return $this->db->get("glbm_grupcustomer")->row();
    }

    function DataGrupCustomer($kode = "")
    {
        return $this->db->query("SELECT * FROM glbm_grupcustomer WHERE kode = '" . $kode . "' ")->result();
    }

    function SaveData($data = "")
    {
        return $this->db->insert('glbm_grupcustomer', $data);
    }

    function UpdateData($data = "", $kode = "")
	{
		$this->db->where('kode', $kode);
		return $this->db->update('glbm_grupcustomer', $data);
	}
}
