<?php
class Kelompoktoko_model extends CI_Model
{
    function CekKode($nomorpelanggan = "")
    {
        return $this->db->query("SELECT *FROM glbm_kelompoktoko WHERE nomor = '" . $nomorpelanggan . "'")->result();
    }
	
    function DataKelompok($nomorpelanggan = "")
    {
        return $this->db->query("SELECT *FROM glbm_kelompoktoko WHERE nomor = '" . $nomorpelanggan . "'")->result();
    }

    function SaveData($data = "")
    {
        return $this->db->insert('glbm_kelompoktoko', $data);
    }

	function SaveDataDetail($data = "")
    {
        return $this->db->insert('glbm_kelompoktokodetail', $data);
    }

	function DataDetailKelompok($nomorpelanggan = "")
    {
		return $this->db->query("SELECT *FROM glbm_kelompoktokodetail WHERE nomor = '" . $nomorpelanggan . "'")->result();
    }

	function DeleteDetail($nomorpelanggan = "")
    {
        $this->db->where('nomor', $nomorpelanggan);
        return $this->db->delete('glbm_kelompoktokodetail');
    }

    function UpdateData($data = "", $nomorpelanggan)
    {
        $this->db->where('nomor', $nomorpelanggan);
        return $this->db->update('glbm_kelompoktoko', $data);
    }
}
