<?php

class Salesman_model extends CI_Model
{
	function CekKode($kode = "")
	{
		return $this->db->query("SELECT * FROM glbm_salesman WHERE kode = '" . $kode . "' ")->result();
	}

	public function GetMaxNomor($kode = "")
	{
		$this->db->select_max('kode');
		$this->db->where("left(kode,1)", $kode);
		return $this->db->get("glbm_salesman")->row();
	}

	// function CekBarangDetail($kodebarang = "", $kode = "")
	// {
	// 	return $this->db->query("SELECT * FROM glbm_salesmandetail WHERE kodebarang = '" . $kodebarang . "' AND kode = '" . $kode . "' ")->result();
	// }

	function DataSalesman($kode = "")
	{
		return $this->db->query("SELECT * FROM glbm_salesman WHERE kode = '" . $kode . "' ")->result();
	}

	function LoadSalesman($cabang = "")
	{
		return $this->db->query("SELECT * FROM glbm_salesman WHERE kodecabang = '" . $cabang . "' AND aktif = true ORDEr BY nama ASC")->result();
	}

	public function CekCabang($kode = ""){
		return $this->db->query("SELECT *FROM glbm_salesmandetail WHERE kodecabang = '".$kode."'")->result();
	}

	PUBLIC function GetDataBarangCabang($kode = ""){
		return $this->db->query("SELECT *FROM glbm_barangdetail WHERE kodecabang = '".$kode."'")->result();
	}
	// function DataSalesmanDetail($kode = "")
	// {
	// 	return $this->db->query("SELECT * FROM glbm_salesmandetail WHERE kode = '" . $kode . "' ")->result();
	// }

	function SaveData($data = "")
	{
		return $this->db->insert('glbm_salesman', $data);
	}

	function SaveDataDetail($data = "")
	{
		return $this->db->insert('glbm_salesmandetail', $data);
	}

	function UpdateData($data = "", $kode = "")
	{
		$this->db->where('kode', $kode);
		return $this->db->update('glbm_salesman', $data);
	}

	function UpdateDataDetail($kode = "", $data = "")
	{
		$this->db->where('kode', $kode);
		return $this->db->update('glbm_salesmandetail', $data);
	}

	function DataSlm($searchTerm = "", $cabang = "")
	{
		$this->db->select("*");
		$this->db->where("nama like '%" . $searchTerm . "%' AND aktif = true AND kodecabang = '" . $cabang . "'");
		$this->db->order_by("nama", "ASC");
		$fetched_records = $this->db->get("glbm_salesman");
		$datasales = $fetched_records->result_array();


		$data = array();
		foreach ($datasales as $val) {

			$data[] = array(
				"id" => $val['kode'],
				"text" => $val['nama']
			);
		}

		return $data;
	}

	// function DeleteDetail($kode = "")
	// {
	// 	$this->db->where('kode', $kode);
	// 	return $this->db->delete('glbm_salesmandetail');
	// }
}
