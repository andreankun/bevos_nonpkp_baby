<?php
class RegistrasiNomorFakturPajak_model extends CI_Model
{
    public function GetMaxNomor($nomor = "")
    {
        $this->db->select_max('nomor');
        $this->db->where("left(nomor,8)", $nomor);
        return $this->db->get("srvt_registrasinomorfakturpajak")->row();
    }

    function SaveData($data = "")
    {
        return $this->db->insert('srvt_registrasinomorfakturpajak', $data);
    }

    function LoadNomorFakturPajak()
	{
		return $this->db->query("SELECT * FROM srvt_registrasinomorfakturpajak WHERE status = false ORDER BY nomor DESC")->result();
	}
}
