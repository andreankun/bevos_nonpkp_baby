<?php

class Cabang_model extends CI_Model
{
    function CekKode($kode = "")
    {
        return $this->db->query("SELECT * FROM glbm_cabang WHERE kode = '" . $kode . "' ")->result();
    }

    function DataKodePos($kode = "")
    {
        return $this->db->query("SELECT kode AS kodepos FROM glbm_kodepos WHERE kode = '" . $kode . "' ")->result();
    }

    function DataCabang($kode = "")
    {
        return $this->db->query("SELECT * FROM glbm_cabang WHERE kode = '" . $kode . "' ")->result();
    }

    function DataCoa($nomor = "")
    {
        return $this->db->query("SELECT * FROM glbm_accountlainlain WHERE nomor = '" . $nomor . "' ")->result();
    }

    function SaveData($data = "")
    {
        return $this->db->insert('glbm_cabang', $data);
    }

    function UpdateData($data = "", $kode = "")
    {
        $this->db->where('kode', $kode);
        return $this->db->update('glbm_cabang', $data);
    }
}
