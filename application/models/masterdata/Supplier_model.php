<?php

class Supplier_model extends CI_Model{

    public function GetMaxNomor($nomor= "")
    {
        $this->db->select_max('nomor');
        $this->db->where("left(nomor,1)",$nomor);
        return $this->db->get("glbm_supplier")->row();
    }

    function DataSupplier($nomor = "")
    {
        return $this->db->query("SELECT * FROM glbm_supplier WHERE nomor = '".$nomor."' ")->result();
    }

    function SaveData($data = "")
    {
        return $this->db->insert('glbm_supplier', $data);
    }

    function UpdateData($data ="",$nomor = "")
    {
		$this->db->where('nomor', $nomor);
        return $this->db->update('glbm_supplier', $data);
	}

}