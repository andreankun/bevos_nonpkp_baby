<?php

class Freebarang_model extends CI_Model
{
	function CekFreeBarang($kodebarang = "", $kodecabang = "")
	{
		return $this->db->query("SELECT *FROM glbm_freebarang WHERE kode = '" . $kodebarang . "'")->result();
	}

	public function GetMaxNomor($kode = "")
	{
		$this->db->select_max('kode');
		$this->db->where("left(kode,3)", $kode);
		return $this->db->get("glbm_freebarang")->row();
	}

	function CekDatadetail($kodebarang = "", $kodecabang = "")
	{
		return $this->db->query("SELECT *FROM glbm_freebarangdetail where kodebarang = '" . $kodebarang . "'")->result();
	}

	function DataFreeBarang($kode = "", $kodecabang = "")
	{
		return $this->db->query("SELECT *FROM glbm_freebarang WHERE kode = '" . $kode . "'")->result();
	}

	function DataFreeBarangdetail($kode = "", $kodecabang = "")
	{
		return $this->db->query("SELECT *FROM glbm_freebarangdetail where kode = '" . $kode . "'")->result();
	}

	function DataFreeBarangHeader($kode = "", $kodecabang = ""){
		return $this->db->query("SELECT *FROM glbm_freebarangdetail2 where kode = '" . $kode . "'")->result();
	}

	function DataBarang($kode)
	{
		return $this->db->query("SELECT *FROM glbm_barang WHERE kode = '" . $kode . "'")->result();
	}

	function SaveData($data = "")
	{
		return $this->db->insert("glbm_freebarang", $data);
	}

	function SaveDataDetail($datadeatil = "")
	{
		return $this->db->insert("glbm_freebarangdetail", $datadeatil);
	}

	function SaveDataDetail2($datadeatil2 = "")
	{
		return $this->db->insert("glbm_freebarangdetail2", $datadeatil2);
	}

	function UpdateData($data = "", $kode = "")
	{
		$this->db->where('kode', $kode);
		// $this->db->where('kodecabang', $kodecabang);
		return $this->db->update('glbm_freebarang', $data);
	}

	function DelDetail($kode = "", $kodecabang)
	{
		$this->db->where('kode', $kode);
		// $this->db->where('kodecabang', $kodecabang);
		return $this->db->delete('glbm_freebarangdetail');
	}

	function DelHeader($kode = "", $kodecabang)
	{
		$this->db->where('kode', $kode);
		// $this->db->where('kodecabang', $kodecabang);
		return $this->db->delete('glbm_freebarangdetail2');
	}
}
