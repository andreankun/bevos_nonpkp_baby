<?php

class Barangdetail_model extends CI_Model
{
	function CekKodeDetail($kodebarang = "")
	{
		return $this->db->query("SELECT *FROM glbm_barangdetail WHERE kodebarang = '" . $kodebarang . "'")->result();
	}
	function CekBarang($kode = "")
	{
		return $this->db->query("SELECT *FROM glbm_barang where kode = '" . $kode . "'")->result();
	}
	function DataBarangDetail($kodebarang = "", $cabang = "")
	{
		return $this->db->query("SELECT *FROM cari_barang WHERE kodebarang = '" . $kodebarang . "' AND kodecabang = '" . $cabang . "'")->result();
	}
	function CekSalesmanDetail($kodesalesman = "", $kode = "")
	{
		return $this->db->query("SELECT * FROM glbm_salesmandetail WHERE kodesalesman = '" . $kodesalesman . "' AND kode = '" . $kode . "' ")->result();
	}
	function DataGudang($kodecabang = "")
	{
		return $this->db->query("SELECT *FROM glbm_gudang WHERE kodecabang = '" . $kodecabang . "'")->result();
	}
	function CekInventory($kodebarang = "", $kodegudang = "")
	{
		return $this->db->query("SELECT * FROM srvt_inventorystok WHERE kodebarang = '" . $kodebarang . "' AND kodegudang = '" . $kodegudang . "'")->result();
	}
	function SaveData($data = "")
	{
		return $this->db->insert('glbm_barangdetail', $data);
	}
	function SaveDataInv($data = "")
	{
		return $this->db->insert('srvt_inventorystok', $data);
	}
	function DataSalesmanDetail($kode = "")
	{
		return $this->db->query("SELECT * FROM glbm_salesmandetail WHERE kode = '" . $kode . "' ")->result();
	}
	function SaveDataDetail($data = "")
	{
		return $this->db->insert('glbm_salesmandetail', $data);
	}
	function DeleteDetail($kode = "")
	{
		$this->db->where('kode', $kode);
		return $this->db->delete('glbm_salesmandetail');
	}
	function UpdateData($data = "", $kodebarang = "", $kodecabang = "")
	{
		$this->db->where('kodebarang', $kodebarang);
		$this->db->where('kodecabang', $kodecabang);
		return $this->db->update('glbm_barangdetail', $data);
	}

	function GetExport($kodecabang = "", $kodemerk = "")
	{
		if ($kodemerk == "") {
			return $this->db->query("SELECT bd.*, b.kodemerk FROM glbm_barangdetail bd LEFT JOIN glbm_barang b on b.kode = bd.kodebarang WHERE kodecabang = '" . $kodecabang . "'")->result();
		} else {
			return $this->db->query("SELECT bd.*, b.kodemerk FROM glbm_barangdetail bd LEFT JOIN glbm_barang b on b.kode = bd.kodebarang WHERE kodecabang = '" . $kodecabang . "' AND b.kodemerk = '" . $kodemerk . "'")->result();
		}
	}
}
