<?php

class LaporanPajak_model extends CI_Model
{
	function LaporanPelanggan($cabang = ""){
		$this->db->where("kodecabang", $cabang);

		return $this->db->get("glbm_customer")->result();
	}

	function LaporanInvoicePajak($tglawal = "", $tglakhir = "", $cabang = ""){
		$this->db->where("tanggal >=", date('Y-m-d', strtotime($tglawal)));
		$this->db->where("tanggal <=", date('Y-m-d', strtotime($tglakhir)));
		$this->db->where("kodecabang", $cabang);

		return $this->db->get("rpt_invoicepajak")->result();
	}

	function LaporanPembayaranPiutang($tglawal = "", $tglakhir = "", $cabang = ""){
		$this->db->where("tanggal >=", date('Y-m-d', strtotime($tglawal)));
		$this->db->where("tanggal <=", date('Y-m-d', strtotime($tglakhir)));
		$this->db->where("kodecabang", $cabang);

		return $this->db->get("rpt_pembayaranpiutangpajak")->result();
	} 
	function LaporanMasterBarang($cabang = ""){
		$this->db->where("kodecabang", $cabang);

		return $this->db->get("rpt_masterbarang")->result();
	}

	function Cabang($cabang = "")
    {
        return $this->db->query("SELECT * FROM glbm_cabang WHERE kode = '" . $cabang . "'")->row();
    }

    function gudang($gudang = "")
    {
        return $this->db->query("SELECT * FROM glbm_gudang WHERE kode = '" . $gudang . "'")->row();
    }

    function Konfigurasi()
    {
        return $this->db->query("SELECT * FROM stpm_konfigurasi")->row();
    }

    function GetRekapPPnPenjualan($tglawal = "", $tglakhir = "")
    {
        $this->db->where("tanggal >=", date('Y-m-d', strtotime($tglawal)));
        $this->db->where("tanggal <=", date('Y-m-d', strtotime($tglakhir)));

        return $this->db->get("rpt_rekapppn")->result();
    }

    function GetDataEFakturHeader($tglawal = "", $tglakhir = "")
    {
        $this->db->where("tanggal >=",$tglawal);
        $this->db->where("tanggal <=",$tglakhir);

        return $this->db->get("rpt_efaktur")->result();
    }

    function GetDataEFakturDetail($tglawal = "", $tglakhir = "")
    {
        $this->db->where("tanggal >=",$tglawal);
        $this->db->where("tanggal <=",$tglakhir);

        return $this->db->get("rpt_efakturdetail")->result();
    }

    function GetDataEFakturList($tglawal = "", $tglakhir = "")
    {
        $this->db->select("nomor");
        $this->db->where("tanggal >=",$tglawal);
        $this->db->where("tanggal <=",$tglakhir);
        $this->db->group_by("nomor");

        return $this->db->get("rpt_efakturdetail")->result();
    }

    function GetDataDepositDetail($tglawal = "", $tglakhir = "", $cabang = "")
    {
        $this->db->where("tanggal >=", $tglawal);
        $this->db->where("tanggal <=", $tglakhir);
        $this->db->where("kodecabang", $cabang);
        
        return $this->db->get("rpt_depositpajak")->result();
    }

    function GetDataDepositHeader($tglawal = "", $tglakhir = "", $cabang = "")
    {
        $this->db->where("tanggal >=", $tglawal);
        $this->db->where("tanggal <=", $tglakhir);
        $this->db->where("kodecabang", $cabang);
        
        return $this->db->get("rpt_depositpajakheader")->result();
    }
    
    function GetDataDepositList($tglawal = "", $tglakhir = "", $cabang = "")
    {
        $this->db->where("tanggal >=", $tglawal);
        $this->db->where("tanggal <=", $tglakhir);
        $this->db->where("kodecabang", $cabang);

        return $this->db->get("rpt_depositpajakheader")->result();
    }

	function GetDataPenjualan($tglawal = "", $tglakhir = "", $cabang = ""){
		$this->db->where("tanggal >=", date('Y-m-d', strtotime($tglawal)));
        $this->db->where("tanggal <=", date('Y-m-d', strtotime($tglakhir)));
        $this->db->where("kodecabang", $cabang);

		return $this->db->get("rpt_penjualanpajak")->result();
	}

    function GetDataInvoiceOutstanding($tglawal = "", $tglakhir = "", $cabang = ""){
		$this->db->where("tanggal >=", date('Y-m-d', strtotime($tglawal)));
        $this->db->where("tanggal <=", date('Y-m-d', strtotime($tglakhir)));
        $this->db->where("kodecabang", $cabang);

		return $this->db->get("rpt_invoicepajak")->result();
	}

	function GetDataPenerimaan($tglawal = "", $tglakhir = "", $cabang = ""){
		$this->db->where("tanggal >=", date('Y-m-d', strtotime($tglawal)));
        $this->db->where("tanggal <=", date('Y-m-d', strtotime($tglakhir)));
        $this->db->where("kodecabang", $cabang);

		return $this->db->get("rpt_pembayaranpiutangpajak")->result();
	}

    function GetDataPenerimaanBarang($tglawal = "", $tglakhir = "", $gudang = "")
    {
        $this->db->where("tanggal >=", $tglawal);
        $this->db->where("tanggal <=", $tglakhir);
        $this->db->where("kodegudang", $gudang);

        return $this->db->get("rpt_penerimaanbarangunioin")->result();
    }

    function GetExportCustomer(){
		return $this->db->query("SELECT 
        c.*,
        cb.nama as namacabang
        FROM glbm_customer c
        LEFT JOIN glbm_cabang cb on cb.kode = c.kodecabang
        ")->result();
	}

    function GetExportMasterBarang()
    {
        return $this->db->query("SELECT
        i.kodebarang,
        b.nama AS namabarang,
        i.kodegudang,
        g.nama AS namagudang,
        m.kode AS kodekategori,
        m.nama AS namakategori,
        s.kode AS kodesatuan,
        s.nama AS namasatuan,
        SUM(i.qtyawal) AS qtyawal,
        SUM(i.qtymasuk) AS qtymasuk,
        SUM(i.qtyawal) + SUM(i.qtymasuk) AS saldo
    FROM
        srvt_inventorystok i
        LEFT JOIN glbm_barang b ON b.kode = i.kodebarang
        LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
        LEFT JOIN glbm_satuan s ON s.kode = b.kodesatuan
        LEFT JOIN glbm_gudang g ON g.kode = i.kodegudang
        LEFT JOIN glbm_cabang c ON c.kode = i.kodecabang
    WHERE
        i.periode >= '202205'
        AND i.periode <= '" . date('Ym') . "'
        AND i.qtyawal + i.qtymasuk <> 0
    GROUP BY
        i.kodebarang,
        b.nama,
        i.kodegudang,
        g.nama,
        m.kode,
        m.nama,
        s.kode,
        s.nama
    ORDER BY
        i.kodegudang")->result();
    }

    function GetMasterMerk()
    {
        return $this->db->query("SELECT * FROM glbm_merk ORDER BY nama")->result();
    }
}
