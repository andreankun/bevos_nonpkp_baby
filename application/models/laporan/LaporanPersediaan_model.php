<?php

class LaporanPersediaan_model extends CI_Model
{

	function GetCabang($merkawal)
	{
		return $this->db->query("SELECT *FROM glbm_merk where kode = '" . $merkawal . "'")->row();
	}

	function GetCabangAkhir($merkakhir)
	{
		return $this->db->query("SELECT *FROM glbm_merk where kode = '" . $merkakhir . "'")->row();
	}

	function Gudang($gudang)
	{
		return $this->db->query("SELECT *FROM glbm_gudang where kode = '" . $gudang . "'")->row();
	}

	function Cabang($cabang)
	{
		return $this->db->query("SELECT *FROM glbm_cabang where kode = '" . $cabang . "'")->row();
	}

	function Barang($barang)
	{
		return $this->db->query("SELECT *FROM glbm_barang where kode = '" . $barang . "'")->row();
	}

	function GetdataPersedianPerKategori($tglawal = "", $tglakhir = "", $merkawal = "", $merkakhir = "", $cabang = "")
	{
		return $this->db->query("SELECT
			hasil_data.kodebarang,
			hasil_data.namabarang,
			hasil_data.kodecabang,
			hasil_data.kodemerk,
			hasil_data.namamerk,
			sum(hasil_data.qtyawalsblm) + SUM(hasil_data.qtymasuksblm) - SUM(hasil_data.qtykeluarsblm) - SUM(hasil_data.qtypicksblm) as qtyawal,
			sum(hasil_data.qtymasuk) as qtymasuk,
			sum(hasil_data.qtykeluar) as qtykeluar,
			sum(hasil_data.qtypick) as qtypick,
			(
        sum(hasil_data.qtyawalsblm) + SUM(hasil_data.qtymasuksblm) - SUM(hasil_data.qtykeluarsblm) - SUM(hasil_data.qtypicksblm) + sum(hasil_data.qtymasuk)
    	) - (
        ABS(
        SUM(hasil_data.qtykeluar) + SUM(hasil_data.qtypick)
        )
    	) AS stok,
			hasil_data.satuan,
			hasil_data.hargajual,
			hasil_data.komisi
		FROM(
					SELECT
						i.kodebarang,
						b.nama as namabarang,
						i.kodecabang,
						b.kodemerk,
						m.nama AS namamerk,
						i.qtyawal as qtyawalsblm,
						0 AS qtymasuksblm,
						0 AS qtykeluarsblm,
						0 AS qtypicksblm,
						0 as qtyawal,
						0 as qtymasuk,
						0 as qtykeluar,
						0 as qtypick,
						b.kodesatuan AS satuan,
						bd.hargajual,
						bd.komisi
					FROM
						srvt_inventorystok i
						LEFT JOIN glbm_barang b ON b.kode = i.kodebarang
						LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
						LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = i.kodecabang
						LEFT JOIN glbm_gudang g ON g.kode = i.kodegudang
					WHERE
						periode = '202208'
						AND b.kodemerk >= '" . $merkawal . "'
						AND b.kodemerk <= '" . $merkakhir . "'
						AND i.kodecabang = '" . $cabang . "'
					UNION ALL
					SELECT
						pbd.kodebarang,
						b.nama as namabarang,
						pb.kodecabang,
						b.kodemerk,
						m.nama AS namamerk,
						0 AS qtyawalsblm,
						pbd.qty AS qtymasuksblm,
						0 AS qtykeluarsblm,
						0 AS qtypicksblm,
						0 as qtyawal,
						0 as qtymasuk,
						0 as qtykeluar,
						0 as qtypick,
						b.kodesatuan AS satuan,
						bd.hargajual,
						bd.komisi
					FROM
						srvt_penerimaanbarang pb
						LEFT JOIN srvt_penerimaanbarangdetail pbd on pb.nomor = pbd.nomor
						LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
						LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
						LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecabang
						LEFT JOIN glbm_gudang g ON g.kode = pb.kodegudang
					WHERE
						pb.batal = false
						AND to_char(pb.tanggal, 'YYYYMMDD') < '" . date('Ymd', strtotime($tglawal)) . "'
						AND b.kodemerk >= '" . $merkawal . "'
						AND b.kodemerk <= '" . $merkakhir . "'
						AND pb.kodecabang = '" . $cabang . "'
					UNION ALL
					SELECT
						pbd.kodebarang,
						b.nama as namabarang,
						pb.kodecabang,
						b.kodemerk,
						m.nama AS namamerk,
						0 AS qtyawalsblm,
						pbd.qty AS qtymasuksblm,
						0 AS qtykeluarsblm,
						0 AS qtypicksblm,
						0 as qtyawal,
						0 as qtymasuk,
						0 as qtykeluar,
						0 as qtypick,
						b.kodesatuan AS satuan,
						bd.hargajual,
						bd.komisi
					FROM
						srvt_penerimaanbarang pb
						LEFT JOIN srvt_penerimaanbarangdetail pbd on pb.nomor = pbd.nomor
						LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
						LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
						LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecabang
						LEFT JOIN glbm_gudang g ON g.kode = pb.kodegudang
					WHERE
						pb.batal = true
						AND to_char(pb.tglbatal, 'YYYYMMDD') < '" . date('Ymd', strtotime($tglawal)) . "'
						AND b.kodemerk >= '" . $merkawal . "'
						AND b.kodemerk <= '" . $merkakhir . "'
						AND pb.kodecabang = '" . $cabang . "'
					UNION ALL
					SELECT
						pbd.kodebarang,
						b.nama as namabarang,
						pb.kodecabang,
						b.kodemerk,
						m.nama AS namamerk,
						0 AS qtyawalsblm,
						pbd.qty AS qtymasuksblm,
						0 AS qtykeluarsblm,
						0 AS qtypicksblm,
						0 as qtyawal,
						0 as qtymasuk,
						0 as qtykeluar,
						0 as qtypick,
						b.kodesatuan AS satuan,
						bd.hargajual,
						bd.komisi
					FROM
						srvt_returbarang pb
						LEFT JOIN srvt_returbarangdetail pbd on pb.nomor = pbd.nomor
						LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
						LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
						LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecabang
						LEFT JOIN glbm_gudang g ON g.kode = pb.lokasigudang
					WHERE
						to_char(pb.tanggal, 'YYYYMMDD') < '" . date('Ymd', strtotime($tglawal)) . "'
						AND b.kodemerk >= '" . $merkawal . "'
						AND b.kodemerk <= '" . $merkakhir . "'
						AND pb.kodecabang = '" . $cabang . "'
					UNION ALL
					SELECT
						pbd.kodebarang,
						b.nama as namabarang,
						pb.kodecabang,
						b.kodemerk,
						m.nama AS namamerk,
						0 AS qtyawalsblm,
						0 AS qtymasuksblm,
						pbd.qty AS qtykeluarsblm,
						0 AS qtypicksblm,
						0 as qtyawal,
						0 as qtymasuk,
						0 as qtykeluar,
						0 as qtypick,
						b.kodesatuan AS satuan,
						bd.hargajual,
						bd.komisi
					FROM
						srvt_penerimaanbarang pb
						LEFT JOIN srvt_penerimaanbarangdetail pbd on pb.nomor = pbd.nomor
						LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
						LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
						LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecabang
						LEFT JOIN glbm_gudang g ON g.kode = pb.kodegudang
					WHERE
						pb.batal = true
						AND to_char(pb.tglbatal, 'YYYYMMDD') < '" . date('Ymd', strtotime($tglawal)) . "'
						AND b.kodemerk >= '" . $merkawal . "'
						AND b.kodemerk <= '" . $merkakhir . "'
						AND pb.kodecabang = '" . $cabang . "'
					UNION ALL
					SELECT
						pbd.kodebarang,
						b.nama as namabarang,
						pb.kodecabang,
						b.kodemerk,
						m.nama AS namamerk,
						0 AS qtyawalsblm,
						0 AS qtymasuksblm,
						0 AS qtykeluarsblm,
						CASE
							WHEN pb.kodestatus <> 2 THEN pbd.qty
							ELSE 0
						END AS qtypicksblm,
						0 as qtyawal,
						0 as qtymasuk,
						0 as qtykeluar,
						0 as qtypick,
						b.kodesatuan AS satuan,
						bd.hargajual,
						bd.komisi
					FROM
						srvt_salesorder pb
						LEFT JOIN srvt_sodetail pbd on pb.nomor = pbd.nomor
						LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
						LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
						LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecabang
						LEFT JOIN glbm_gudang g ON g.kode = pb.lokasi
					WHERE
						pb.batal = false
						AND to_char(pb.tanggal, 'YYYYMMDD') < '" . date('Ymd', strtotime($tglawal)) . "'
						AND b.kodemerk >= '" . $merkawal . "'
						AND b.kodemerk <= '" . $merkakhir . "'
						AND pb.kodecabang = '" . $cabang . "'
					UNION ALL
					SELECT
						pbd.kodebarang,
						b.nama as namabarang,
						pb.kodecabang,
						b.kodemerk,
						m.nama AS namamerk,
						0 AS qtyawalsblm,
						0 AS qtymasuksblm,
						pbd.qty AS qtykeluarsblm,
						0 AS qtypicksblm,
						0 as qtyawal,
						0 as qtymasuk,
						0 as qtykeluar,
						0 as qtypick,
						b.kodesatuan AS satuan,
						bd.hargajual,
						bd.komisi
					FROM
						srvt_invoice pb
						LEFT JOIN srvt_invoicedetail pbd ON pb.nomor = pbd.nomor
						LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
						LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
						LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecabang
						LEFT JOIN glbm_gudang g ON g.kode = pb.lokasi
					WHERE
						pb.batal = false
						AND to_char(pb.tanggal, 'YYYYMMDD') < '" . date('Ymd', strtotime($tglawal)) . "'
						AND b.kodemerk >= '" . $merkawal . "'
						AND b.kodemerk <= '" . $merkakhir . "'
						AND pb.kodecabang = '" . $cabang . "'
					UNION ALL
					SELECT
						pbd.kodebarang,
						b.nama as namabarang,
						pb.kodecabang,
						b.kodemerk,
						m.nama AS namamerk,
						0 AS qtyawalsblm,
						CASE
							WHEN pb.jenis = '2' THEN pbd.qty
							ELSE 0
						END AS qtymasuksblm,
						CASE
							WHEN pb.jenis <> '2' THEN pbd.qty
							ELSE 0
						END AS qtykeluarsblm,
						0 AS qtypicksblm,
						0 as qtyawal,
						0 as qtymasuk,
						0 as qtykeluar,
						0 as qtypick,
						b.kodesatuan AS satuan,
						bd.hargajual,
						bd.komisi
					FROM
						srvt_bag pb
						LEFT JOIN srvt_bagdetail pbd on pb.nomor = pbd.nomor
						LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
						LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
						LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecabang
						LEFT JOIN glbm_gudang g ON g.kode = pb.lokasi
					WHERE
						pb.batal = false
						AND approve = true
						AND to_char(pb.tanggal, 'YYYYMMDD') < '" . date('Ymd', strtotime($tglawal)) . "'
						AND b.kodemerk >= '" . $merkawal . "'
						AND b.kodemerk <= '" . $merkakhir . "'
						AND pb.kodecabang = '" . $cabang . "'
					UNION ALL
					SELECT
						pbd.kodebarang,
						b.nama as namabarang,
						pb.kodecbasal as kodecabang,
						b.kodemerk,
						m.nama AS namamerk,
						0 AS qtyawalsblm,
						0 AS qtymasuksblm,
						CASE WHEN pb.kodestatus = 2 THEN pbd.qty ELSE 0 END AS qtykeluarsblm,
						CASE WHEN pb.kodestatus <> 2 THEN pbd.qty ELSE 0 END AS qtypicksblm,
						0 as qtyawal,
						0 as qtymasuk,
						0 as qtykeluar,
						0 as qtypick,
						b.kodesatuan AS satuan,
						bd.hargajual,
						bd.komisi
					FROM
						srvt_transferstock pb
						LEFT JOIN srvt_transferstockdetail pbd on pb.nomor = pbd.nomor
						LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
						LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
						LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecbasal
						LEFT JOIN glbm_gudang g ON g.kode = pb.kodegdasal
					WHERE
						pb.batal = false
						AND to_char(pb.tanggal, 'YYYYMMDD') < '" . date('Ymd', strtotime($tglawal)) . "'
						AND b.kodemerk >= '" . $merkawal . "'
						AND b.kodemerk <= '" . $merkakhir . "'
						AND pb.kodecbasal = '" . $cabang . "'
					UNION ALL
					SELECT
						pbd.kodebarang,
						b.nama as namabarang,
						pb.kodecbtujuan as kodecabang,
						b.kodemerk,
						m.nama AS namamerk,
						0 AS qtyawalsblm,
						pbd.qty AS qtymasuksblm,
						0 AS qtykeluarsblm,
						0 AS qtypicksblm,
						0 as qtyawal,
						0 as qtymasuk,
						0 as qtykeluar,
						0 as qtypick,
						b.kodesatuan AS satuan,
						bd.hargajual,
						bd.komisi
					FROM
						srvt_penerimaantransferstock pb
						LEFT JOIN srvt_penerimaantransferstockdetail pbd on pb.nomor = pbd.nomor
						LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
						LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
						LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecbtujuan
						LEFT JOIN glbm_gudang g ON g.kode = pb.kodegdtujuan
					WHERE
						pb.batal = false
						AND to_char(pb.tanggal, 'YYYYMMDD') < '" . date('Ymd', strtotime($tglawal)) . "'
						AND b.kodemerk >= '" . $merkawal . "'
						AND b.kodemerk <= '" . $merkakhir . "'
						AND pb.kodecbtujuan = '" . $cabang . "'
				UNION ALL
				SELECT
					pbd.kodebarang,
					b.nama as namabarang,
					pb.kodecabang,
					b.kodemerk,
					m.nama AS namamerk,
					0 as qtyawalsblm,
					0 as qtymasuksblm,
					0 as qtykeluarsblm,
					0 as qtypicksblm,
					0 AS qtyawal,
					pbd.qty AS qtymasuk,
					0 AS qtykeluar,
					0 AS qtypick,
					b.kodesatuan AS satuan,
					bd.hargajual,
					bd.komisi
				FROM
					srvt_penerimaanbarang pb
					LEFT JOIN srvt_penerimaanbarangdetail pbd on pb.nomor = pbd.nomor
					LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
					LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
					LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecabang
					LEFT JOIN glbm_gudang g ON g.kode = pb.kodegudang
				WHERE
					pb.batal = false
					AND to_char(pb.tanggal, 'YYYYMMDD') >= '" . date('Ymd', strtotime($tglawal)) . "'
					AND to_char(pb.tanggal, 'YYYYMMDD') <= '" . date('Ymd', strtotime($tglakhir)) . "'
					AND b.kodemerk >= '" . $merkawal . "'
					AND b.kodemerk <= '" . $merkakhir . "'
					AND pb.kodecabang = '" . $cabang . "'
				UNION ALL
				SELECT
					pbd.kodebarang,
					b.nama as namabarang,
					pb.kodecabang,
					b.kodemerk,
					m.nama AS namamerk,
					0 as qtyawalsblm,
					0 as qtymasuksblm,
					0 as qtykeluarsblm,
					0 as qtypicksblm,
					0 AS qtyawal,
					pbd.qty AS qtymasuk,
					0 AS qtykeluar,
					0 AS qtypick,
					b.kodesatuan AS satuan,
					bd.hargajual,
					bd.komisi
				FROM
					srvt_penerimaanbarang pb
					LEFT JOIN srvt_penerimaanbarangdetail pbd on pb.nomor = pbd.nomor
					LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
					LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
					LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecabang
					LEFT JOIN glbm_gudang g ON g.kode = pb.kodegudang
				WHERE
					pb.batal = true
					AND to_char(pb.tglbatal, 'YYYYMMDD') >= '" . date('Ymd', strtotime($tglawal)) . "'
					AND to_char(pb.tglbatal, 'YYYYMMDD') <= '" . date('Ymd', strtotime($tglakhir)) . "'
					AND b.kodemerk >= '" . $merkawal . "'
					AND b.kodemerk <= '" . $merkakhir . "'
					AND pb.kodecabang = '" . $cabang . "'
				UNION ALL
				SELECT
					pbd.kodebarang,
					b.nama as namabarang,
					pb.kodecabang,
					b.kodemerk,
					m.nama AS namamerk,
					0 as qtyawalsblm,
					0 as qtymasuksblm,
					0 as qtykeluarsblm,
					0 as qtypicksblm,
					0 AS qtyawal,
					pbd.qty AS qtymasuk,
					0 AS qtykeluar,
					0 AS qtypick,
					b.kodesatuan AS satuan,
					bd.hargajual,
					bd.komisi
				FROM
					srvt_returbarang pb
					LEFT JOIN srvt_returbarangdetail pbd on pb.nomor = pbd.nomor
					LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
					LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
					LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecabang
					LEFT JOIN glbm_gudang g ON g.kode = pb.lokasigudang
				WHERE
					to_char(pb.tanggal, 'YYYYMMDD') >= '" . date('Ymd', strtotime($tglawal)) . "'
					AND to_char(pb.tanggal, 'YYYYMMDD') <= '" . date('Ymd', strtotime($tglakhir)) . "'
					AND b.kodemerk >= '" . $merkawal . "'
					AND b.kodemerk <= '" . $merkakhir . "'
					AND pb.kodecabang = '" . $cabang . "'
				UNION ALL
				SELECT
					pbd.kodebarang,
					b.nama as namabarang,
					pb.kodecabang,
					b.kodemerk,
					m.nama AS namamerk,
					0 as qtyawalsblm,
					0 as qtymasuksblm,
					0 as qtykeluarsblm,
					0 as qtypicksblm,
					0 AS qtyawal,
					0 AS qtymasuk,
					pbd.qty AS qtykeluar,
					0 AS qtypick,
					b.kodesatuan AS satuan,
					bd.hargajual,
					bd.komisi
				FROM
					srvt_penerimaanbarang pb
					LEFT JOIN srvt_penerimaanbarangdetail pbd on pb.nomor = pbd.nomor
					LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
					LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
					LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecabang
					LEFT JOIN glbm_gudang g ON g.kode = pb.kodegudang
				WHERE
					pb.batal = true
					AND to_char(pb.tglbatal, 'YYYYMMDD') >= '" . date('Ymd', strtotime($tglawal)) . "'
					AND to_char(pb.tglbatal, 'YYYYMMDD') <= '" . date('Ymd', strtotime($tglakhir)) . "'
					AND b.kodemerk >= '" . $merkawal . "'
					AND b.kodemerk <= '" . $merkakhir . "'
					AND pb.kodecabang = '" . $cabang . "'
				UNION ALL
				SELECT
					pbd.kodebarang,
					b.nama as namabarang,
					pb.kodecabang,
					b.kodemerk,
					m.nama AS namamerk,
					0 as qtyawalsblm,
					0 as qtymasuksblm,
					0 as qtykeluarsblm,
					0 as qtypicksblm,
					0 AS qtyawal,
					0 AS qtymasuk,
					0 AS qtykeluar,
					CASE
						WHEN pb.kodestatus <> 2 THEN pbd.qty
						ELSE 0
					END AS qtypick,
					b.kodesatuan AS satuan,
					bd.hargajual,
					bd.komisi
				FROM
					srvt_salesorder pb
					LEFT JOIN srvt_sodetail pbd on pb.nomor = pbd.nomor
					LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
					LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
					LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecabang
					LEFT JOIN glbm_gudang g ON g.kode = pb.lokasi
				WHERE
					pb.batal = false
					AND to_char(pb.tanggal, 'YYYYMMDD') >= '" . date('Ymd', strtotime($tglawal)) . "'
					AND to_char(pb.tanggal, 'YYYYMMDD') <= '" . date('Ymd', strtotime($tglakhir)) . "'
					AND b.kodemerk >= '" . $merkawal . "'
					AND b.kodemerk <= '" . $merkakhir . "'
					AND pb.kodecabang = '" . $cabang . "'
				UNION ALL
				SELECT
					pbd.kodebarang,
					b.nama as namabarang,
					pb.kodecabang,
					b.kodemerk,
					m.nama AS namamerk,
					0 AS qtyawalsblm,
					0 AS qtymasuksblm,
					0 AS qtykeluarsblm,
					0 AS qtypicksblm,
					0 as qtyawal,
					0 as qtymasuk,
					pbd.qty as qtykeluar,
					0 as qtypick,
					b.kodesatuan AS satuan,
					bd.hargajual,
					bd.komisi
				FROM
					srvt_invoice pb
					LEFT JOIN srvt_invoicedetail pbd ON pb.nomor = pbd.nomor
					LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
					LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
					LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecabang
					LEFT JOIN glbm_gudang g ON g.kode = pb.lokasi
				WHERE
					pb.batal = false
					AND to_char(pb.tanggal, 'YYYYMMDD') >= '" . date('Ymd', strtotime($tglawal)) . "'
					AND to_char(pb.tanggal, 'YYYYMMDD') <= '" . date('Ymd', strtotime($tglakhir)) . "'
					AND b.kodemerk >= '" . $merkawal . "'
					AND b.kodemerk <= '" . $merkakhir . "'
					AND pb.kodecabang = '" . $cabang . "'
				UNION ALL
				SELECT
					pbd.kodebarang,
					b.nama as namabarang,
					pb.kodecabang,
					b.kodemerk,
					m.nama AS namamerk,
					0 as qtyawalsblm,
					0 as qtymasuksblm,
					0 as qtykeluarsblm,
					0 as qtypicksblm,
					0 AS qtyawal,
					CASE
						WHEN pb.jenis = '2' THEN pbd.qty
						ELSE 0
					END AS qtymasuk,
					CASE
						WHEN pb.jenis <> '2' THEN pbd.qty
						ELSE 0
					END AS qtykeluar,
					0 AS qtypick,
					b.kodesatuan AS satuan,
					bd.hargajual,
					bd.komisi
				FROM
					srvt_bag pb
					LEFT JOIN srvt_bagdetail pbd on pb.nomor = pbd.nomor
					LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
					LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
					LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecabang
					LEFT JOIN glbm_gudang g ON g.kode = pb.lokasi
				WHERE
					pb.batal = false
					AND approve = true
					AND to_char(pb.tanggal, 'YYYYMMDD') >= '" . date('Ymd', strtotime($tglawal)) . "'
					AND to_char(pb.tanggal, 'YYYYMMDD') <= '" . date('Ymd', strtotime($tglakhir)) . "'
					AND b.kodemerk >= '" . $merkawal . "'
					AND b.kodemerk <= '" . $merkakhir . "'
					AND pb.kodecabang = '" . $cabang . "'
				UNION ALL
				SELECT
					pbd.kodebarang,
					b.nama as namabarang,
					pb.kodecbasal as kodecabang,
					b.kodemerk,
					m.nama AS namamerk,
					0 as qtyawalsblm,
					0 as qtymasuksblm,
					0 as qtykeluarsblm,
					0 as qtypicksblm,
					0 AS qtyawal,
					0 AS qtymasuk,
					CASE WHEN pb.kodestatus = 2 THEN pbd.qty ELSE 0 END AS qtykeluar,
            		CASE WHEN pb.kodestatus <> 2 THEN pbd.qty ELSE 0 END AS qtypick,
					b.kodesatuan AS satuan,
					bd.hargajual,
					bd.komisi
				FROM
					srvt_transferstock pb
					LEFT JOIN srvt_transferstockdetail pbd on pb.nomor = pbd.nomor
					LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
					LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
					LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecbasal
					LEFT JOIN glbm_gudang g ON g.kode = pb.kodegdasal
				WHERE
					pb.batal = false
					AND to_char(pb.tanggal, 'YYYYMMDD') >= '" . date('Ymd', strtotime($tglawal)) . "'
					AND to_char(pb.tanggal, 'YYYYMMDD') <= '" . date('Ymd', strtotime($tglakhir)) . "'
					AND b.kodemerk >= '" . $merkawal . "'
					AND b.kodemerk <= '" . $merkakhir . "'
					AND pb.kodecbasal = '" . $cabang . "'
				UNION ALL
				SELECT
					pbd.kodebarang,
					b.nama as namabarang,
					pb.kodecbtujuan as kodecabang,
					b.kodemerk,
					m.nama AS namamerk,
					0 as qtyawalsblm,
					0 as qtymasuksblm,
					0 as qtykeluarsblm,
					0 as qtypicksblm,
					0 AS qtyawal,
					pbd.qty AS qtymasuk,
					0 AS qtykeluar,
					0 AS qtypick,
					b.kodesatuan AS satuan,
					bd.hargajual,
					bd.komisi
				FROM
					srvt_penerimaantransferstock pb
					LEFT JOIN srvt_penerimaantransferstockdetail pbd on pb.nomor = pbd.nomor
					LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
					LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
					LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecbtujuan
					LEFT JOIN glbm_gudang g ON g.kode = pb.kodegdtujuan
				WHERE
					pb.batal = false
					AND to_char(pb.tanggal, 'YYYYMMDD') >= '" . date('Ymd', strtotime($tglawal)) . "'
					AND to_char(pb.tanggal, 'YYYYMMDD') <= '" . date('Ymd', strtotime($tglakhir)) . "'
					AND b.kodemerk >= '" . $merkawal . "'
					AND b.kodemerk <= '" . $merkakhir . "'
					AND pb.kodecbtujuan = '" . $cabang . "'
			) hasil_data
		GROUP BY
			hasil_data.kodebarang,
			hasil_data.namabarang,
			hasil_data.kodecabang,
			hasil_data.kodemerk,
			hasil_data.satuan,
			hasil_data.namamerk,
			hasil_data.hargajual,
			hasil_data.komisi
		ORDER BY
			namamerk ASC,
			namabarang ASC")->result();
	}

	function GetdataPersedianPerKategoriRow($tglawal = "", $tglakhir = "", $merkawal = "", $merkakhir = "", $cabang = "")
	{
		return $this->db->query("SELECT
		hasil_data.kodebarang,
		hasil_data.namabarang,
		hasil_data.kodecabang,
		hasil_data.kodemerk,
		hasil_data.namamerk,
		sum(hasil_data.qtyawalsblm) + SUM(hasil_data.qtymasuksblm) - SUM(hasil_data.qtykeluarsblm) - SUM(hasil_data.qtypicksblm) as qtyawal,
		sum(hasil_data.qtymasuk) as qtymasuk,
		sum(hasil_data.qtykeluar) as qtykeluar,
		sum(hasil_data.qtypick) as qtypick,
		(
	sum(hasil_data.qtyawalsblm) + SUM(hasil_data.qtymasuksblm) - SUM(hasil_data.qtykeluarsblm) - SUM(hasil_data.qtypicksblm) + sum(hasil_data.qtymasuk)
	) - (
	ABS(
	SUM(hasil_data.qtykeluar) + SUM(hasil_data.qtypick)
	)
	) AS stok,
		hasil_data.satuan,
		hasil_data.hargajual,
		hasil_data.komisi
	FROM(
				SELECT
					i.kodebarang,
					b.nama as namabarang,
					i.kodecabang,
					b.kodemerk,
					m.nama AS namamerk,
					i.qtyawal as qtyawalsblm,
					0 AS qtymasuksblm,
					0 AS qtykeluarsblm,
					0 AS qtypicksblm,
					0 as qtyawal,
					0 as qtymasuk,
					0 as qtykeluar,
					0 as qtypick,
					b.kodesatuan AS satuan,
					bd.hargajual,
					bd.komisi
				FROM
					srvt_inventorystok i
					LEFT JOIN glbm_barang b ON b.kode = i.kodebarang
					LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
					LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = i.kodecabang
					LEFT JOIN glbm_gudang g ON g.kode = i.kodegudang
				WHERE
					periode = '202208'
					AND b.kodemerk >= '" . $merkawal . "'
					AND b.kodemerk <= '" . $merkakhir . "'
					AND i.kodecabang = '" . $cabang . "'
				UNION ALL
				SELECT
					pbd.kodebarang,
					b.nama as namabarang,
					pb.kodecabang,
					b.kodemerk,
					m.nama AS namamerk,
					0 AS qtyawalsblm,
					pbd.qty AS qtymasuksblm,
					0 AS qtykeluarsblm,
					0 AS qtypicksblm,
					0 as qtyawal,
					0 as qtymasuk,
					0 as qtykeluar,
					0 as qtypick,
					b.kodesatuan AS satuan,
					bd.hargajual,
					bd.komisi
				FROM
					srvt_penerimaanbarang pb
					LEFT JOIN srvt_penerimaanbarangdetail pbd on pb.nomor = pbd.nomor
					LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
					LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
					LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecabang
					LEFT JOIN glbm_gudang g ON g.kode = pb.kodegudang
				WHERE
					pb.batal = false
					AND to_char(pb.tanggal, 'YYYYMMDD') < '" . date('Ymd', strtotime($tglawal)) . "'
					AND b.kodemerk >= '" . $merkawal . "'
					AND b.kodemerk <= '" . $merkakhir . "'
					AND pb.kodecabang = '" . $cabang . "'
				UNION ALL
				SELECT
					pbd.kodebarang,
					b.nama as namabarang,
					pb.kodecabang,
					b.kodemerk,
					m.nama AS namamerk,
					0 AS qtyawalsblm,
					pbd.qty AS qtymasuksblm,
					0 AS qtykeluarsblm,
					0 AS qtypicksblm,
					0 as qtyawal,
					0 as qtymasuk,
					0 as qtykeluar,
					0 as qtypick,
					b.kodesatuan AS satuan,
					bd.hargajual,
					bd.komisi
				FROM
					srvt_penerimaanbarang pb
					LEFT JOIN srvt_penerimaanbarangdetail pbd on pb.nomor = pbd.nomor
					LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
					LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
					LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecabang
					LEFT JOIN glbm_gudang g ON g.kode = pb.kodegudang
				WHERE
					pb.batal = true
					AND to_char(pb.tglbatal, 'YYYYMMDD') < '" . date('Ymd', strtotime($tglawal)) . "'
					AND b.kodemerk >= '" . $merkawal . "'
					AND b.kodemerk <= '" . $merkakhir . "'
					AND pb.kodecabang = '" . $cabang . "'
				UNION ALL
				SELECT
					pbd.kodebarang,
					b.nama as namabarang,
					pb.kodecabang,
					b.kodemerk,
					m.nama AS namamerk,
					0 AS qtyawalsblm,
					pbd.qty AS qtymasuksblm,
					0 AS qtykeluarsblm,
					0 AS qtypicksblm,
					0 as qtyawal,
					0 as qtymasuk,
					0 as qtykeluar,
					0 as qtypick,
					b.kodesatuan AS satuan,
					bd.hargajual,
					bd.komisi
				FROM
					srvt_returbarang pb
					LEFT JOIN srvt_returbarangdetail pbd on pb.nomor = pbd.nomor
					LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
					LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
					LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecabang
					LEFT JOIN glbm_gudang g ON g.kode = pb.lokasigudang
				WHERE
					to_char(pb.tanggal, 'YYYYMMDD') < '" . date('Ymd', strtotime($tglawal)) . "'
					AND b.kodemerk >= '" . $merkawal . "'
					AND b.kodemerk <= '" . $merkakhir . "'
					AND pb.kodecabang = '" . $cabang . "'
				UNION ALL
				SELECT
					pbd.kodebarang,
					b.nama as namabarang,
					pb.kodecabang,
					b.kodemerk,
					m.nama AS namamerk,
					0 AS qtyawalsblm,
					0 AS qtymasuksblm,
					pbd.qty AS qtykeluarsblm,
					0 AS qtypicksblm,
					0 as qtyawal,
					0 as qtymasuk,
					0 as qtykeluar,
					0 as qtypick,
					b.kodesatuan AS satuan,
					bd.hargajual,
					bd.komisi
				FROM
					srvt_penerimaanbarang pb
					LEFT JOIN srvt_penerimaanbarangdetail pbd on pb.nomor = pbd.nomor
					LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
					LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
					LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecabang
					LEFT JOIN glbm_gudang g ON g.kode = pb.kodegudang
				WHERE
					pb.batal = true
					AND to_char(pb.tglbatal, 'YYYYMMDD') < '" . date('Ymd', strtotime($tglawal)) . "'
					AND b.kodemerk >= '" . $merkawal . "'
					AND b.kodemerk <= '" . $merkakhir . "'
					AND pb.kodecabang = '" . $cabang . "'
				UNION ALL
				SELECT
					pbd.kodebarang,
					b.nama as namabarang,
					pb.kodecabang,
					b.kodemerk,
					m.nama AS namamerk,
					0 AS qtyawalsblm,
					0 AS qtymasuksblm,
					0 AS qtykeluarsblm,
					CASE
						WHEN pb.kodestatus <> 2 THEN pbd.qty
						ELSE 0
					END AS qtypicksblm,
					0 as qtyawal,
					0 as qtymasuk,
					0 as qtykeluar,
					0 as qtypick,
					b.kodesatuan AS satuan,
					bd.hargajual,
					bd.komisi
				FROM
					srvt_salesorder pb
					LEFT JOIN srvt_sodetail pbd on pb.nomor = pbd.nomor
					LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
					LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
					LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecabang
					LEFT JOIN glbm_gudang g ON g.kode = pb.lokasi
				WHERE
					pb.batal = false
					AND to_char(pb.tanggal, 'YYYYMMDD') < '" . date('Ymd', strtotime($tglawal)) . "'
					AND b.kodemerk >= '" . $merkawal . "'
					AND b.kodemerk <= '" . $merkakhir . "'
					AND pb.kodecabang = '" . $cabang . "'
				UNION ALL
				SELECT
					pbd.kodebarang,
					b.nama as namabarang,
					pb.kodecabang,
					b.kodemerk,
					m.nama AS namamerk,
					0 AS qtyawalsblm,
					0 AS qtymasuksblm,
					pbd.qty AS qtykeluarsblm,
					0 AS qtypicksblm,
					0 as qtyawal,
					0 as qtymasuk,
					0 as qtykeluar,
					0 as qtypick,
					b.kodesatuan AS satuan,
					bd.hargajual,
					bd.komisi
				FROM
					srvt_invoice pb
					LEFT JOIN srvt_invoicedetail pbd ON pb.nomor = pbd.nomor
					LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
					LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
					LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecabang
					LEFT JOIN glbm_gudang g ON g.kode = pb.lokasi
				WHERE
					pb.batal = false
					AND to_char(pb.tanggal, 'YYYYMMDD') < '" . date('Ymd', strtotime($tglawal)) . "'
					AND b.kodemerk >= '" . $merkawal . "'
					AND b.kodemerk <= '" . $merkakhir . "'
					AND pb.kodecabang = '" . $cabang . "'
				UNION ALL
				SELECT
					pbd.kodebarang,
					b.nama as namabarang,
					pb.kodecabang,
					b.kodemerk,
					m.nama AS namamerk,
					0 AS qtyawalsblm,
					CASE
						WHEN pb.jenis = '2' THEN pbd.qty
						ELSE 0
					END AS qtymasuksblm,
					CASE
						WHEN pb.jenis <> '2' THEN pbd.qty
						ELSE 0
					END AS qtykeluarsblm,
					0 AS qtypicksblm,
					0 as qtyawal,
					0 as qtymasuk,
					0 as qtykeluar,
					0 as qtypick,
					b.kodesatuan AS satuan,
					bd.hargajual,
					bd.komisi
				FROM
					srvt_bag pb
					LEFT JOIN srvt_bagdetail pbd on pb.nomor = pbd.nomor
					LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
					LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
					LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecabang
					LEFT JOIN glbm_gudang g ON g.kode = pb.lokasi
				WHERE
					pb.batal = false
					AND approve = true
					AND to_char(pb.tanggal, 'YYYYMMDD') < '" . date('Ymd', strtotime($tglawal)) . "'
					AND b.kodemerk >= '" . $merkawal . "'
					AND b.kodemerk <= '" . $merkakhir . "'
					AND pb.kodecabang = '" . $cabang . "'
				UNION ALL
				SELECT
					pbd.kodebarang,
					b.nama as namabarang,
					pb.kodecbasal as kodecabang,
					b.kodemerk,
					m.nama AS namamerk,
					0 AS qtyawalsblm,
					0 AS qtymasuksblm,
					CASE WHEN pb.kodestatus = 2 THEN pbd.qty ELSE 0 END AS qtykeluarsblm,
					CASE WHEN pb.kodestatus <> 2 THEN pbd.qty ELSE 0 END AS qtypicksblm,
					0 as qtyawal,
					0 as qtymasuk,
					0 as qtykeluar,
					0 as qtypick,
					b.kodesatuan AS satuan,
					bd.hargajual,
					bd.komisi
				FROM
					srvt_transferstock pb
					LEFT JOIN srvt_transferstockdetail pbd on pb.nomor = pbd.nomor
					LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
					LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
					LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecbasal
					LEFT JOIN glbm_gudang g ON g.kode = pb.kodegdasal
				WHERE
					pb.batal = false
					AND to_char(pb.tanggal, 'YYYYMMDD') < '" . date('Ymd', strtotime($tglawal)) . "'
					AND b.kodemerk >= '" . $merkawal . "'
					AND b.kodemerk <= '" . $merkakhir . "'
					AND pb.kodecbasal = '" . $cabang . "'
				UNION ALL
				SELECT
					pbd.kodebarang,
					b.nama as namabarang,
					pb.kodecbtujuan as kodecabang,
					b.kodemerk,
					m.nama AS namamerk,
					0 AS qtyawalsblm,
					pbd.qty AS qtymasuksblm,
					0 AS qtykeluarsblm,
					0 AS qtypicksblm,
					0 as qtyawal,
					0 as qtymasuk,
					0 as qtykeluar,
					0 as qtypick,
					b.kodesatuan AS satuan,
					bd.hargajual,
					bd.komisi
				FROM
					srvt_penerimaantransferstock pb
					LEFT JOIN srvt_penerimaantransferstockdetail pbd on pb.nomor = pbd.nomor
					LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
					LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
					LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecbtujuan
					LEFT JOIN glbm_gudang g ON g.kode = pb.kodegdtujuan
				WHERE
					pb.batal = false
					AND to_char(pb.tanggal, 'YYYYMMDD') < '" . date('Ymd', strtotime($tglawal)) . "'
					AND b.kodemerk >= '" . $merkawal . "'
					AND b.kodemerk <= '" . $merkakhir . "'
					AND pb.kodecbtujuan = '" . $cabang . "'
			UNION ALL
			SELECT
				pbd.kodebarang,
				b.nama as namabarang,
				pb.kodecabang,
				b.kodemerk,
				m.nama AS namamerk,
				0 as qtyawalsblm,
				0 as qtymasuksblm,
				0 as qtykeluarsblm,
				0 as qtypicksblm,
				0 AS qtyawal,
				pbd.qty AS qtymasuk,
				0 AS qtykeluar,
				0 AS qtypick,
				b.kodesatuan AS satuan,
				bd.hargajual,
				bd.komisi
			FROM
				srvt_penerimaanbarang pb
				LEFT JOIN srvt_penerimaanbarangdetail pbd on pb.nomor = pbd.nomor
				LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
				LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
				LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecabang
				LEFT JOIN glbm_gudang g ON g.kode = pb.kodegudang
			WHERE
				pb.batal = false
				AND to_char(pb.tanggal, 'YYYYMMDD') >= '" . date('Ymd', strtotime($tglawal)) . "'
				AND to_char(pb.tanggal, 'YYYYMMDD') <= '" . date('Ymd', strtotime($tglakhir)) . "'
				AND b.kodemerk >= '" . $merkawal . "'
				AND b.kodemerk <= '" . $merkakhir . "'
				AND pb.kodecabang = '" . $cabang . "'
			UNION ALL
			SELECT
				pbd.kodebarang,
				b.nama as namabarang,
				pb.kodecabang,
				b.kodemerk,
				m.nama AS namamerk,
				0 as qtyawalsblm,
				0 as qtymasuksblm,
				0 as qtykeluarsblm,
				0 as qtypicksblm,
				0 AS qtyawal,
				pbd.qty AS qtymasuk,
				0 AS qtykeluar,
				0 AS qtypick,
				b.kodesatuan AS satuan,
				bd.hargajual,
				bd.komisi
			FROM
				srvt_penerimaanbarang pb
				LEFT JOIN srvt_penerimaanbarangdetail pbd on pb.nomor = pbd.nomor
				LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
				LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
				LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecabang
				LEFT JOIN glbm_gudang g ON g.kode = pb.kodegudang
			WHERE
				pb.batal = true
				AND to_char(pb.tglbatal, 'YYYYMMDD') >= '" . date('Ymd', strtotime($tglawal)) . "'
				AND to_char(pb.tglbatal, 'YYYYMMDD') <= '" . date('Ymd', strtotime($tglakhir)) . "'
				AND b.kodemerk >= '" . $merkawal . "'
				AND b.kodemerk <= '" . $merkakhir . "'
				AND pb.kodecabang = '" . $cabang . "'
			UNION ALL
			SELECT
				pbd.kodebarang,
				b.nama as namabarang,
				pb.kodecabang,
				b.kodemerk,
				m.nama AS namamerk,
				0 as qtyawalsblm,
				0 as qtymasuksblm,
				0 as qtykeluarsblm,
				0 as qtypicksblm,
				0 AS qtyawal,
				pbd.qty AS qtymasuk,
				0 AS qtykeluar,
				0 AS qtypick,
				b.kodesatuan AS satuan,
				bd.hargajual,
				bd.komisi
			FROM
				srvt_returbarang pb
				LEFT JOIN srvt_returbarangdetail pbd on pb.nomor = pbd.nomor
				LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
				LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
				LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecabang
				LEFT JOIN glbm_gudang g ON g.kode = pb.lokasigudang
			WHERE
				to_char(pb.tanggal, 'YYYYMMDD') >= '" . date('Ymd', strtotime($tglawal)) . "'
				AND to_char(pb.tanggal, 'YYYYMMDD') <= '" . date('Ymd', strtotime($tglakhir)) . "'
				AND b.kodemerk >= '" . $merkawal . "'
				AND b.kodemerk <= '" . $merkakhir . "'
				AND pb.kodecabang = '" . $cabang . "'
			UNION ALL
			SELECT
				pbd.kodebarang,
				b.nama as namabarang,
				pb.kodecabang,
				b.kodemerk,
				m.nama AS namamerk,
				0 as qtyawalsblm,
				0 as qtymasuksblm,
				0 as qtykeluarsblm,
				0 as qtypicksblm,
				0 AS qtyawal,
				0 AS qtymasuk,
				pbd.qty AS qtykeluar,
				0 AS qtypick,
				b.kodesatuan AS satuan,
				bd.hargajual,
				bd.komisi
			FROM
				srvt_penerimaanbarang pb
				LEFT JOIN srvt_penerimaanbarangdetail pbd on pb.nomor = pbd.nomor
				LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
				LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
				LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecabang
				LEFT JOIN glbm_gudang g ON g.kode = pb.kodegudang
			WHERE
				pb.batal = true
				AND to_char(pb.tglbatal, 'YYYYMMDD') >= '" . date('Ymd', strtotime($tglawal)) . "'
				AND to_char(pb.tglbatal, 'YYYYMMDD') <= '" . date('Ymd', strtotime($tglakhir)) . "'
				AND b.kodemerk >= '" . $merkawal . "'
				AND b.kodemerk <= '" . $merkakhir . "'
				AND pb.kodecabang = '" . $cabang . "'
			UNION ALL
			SELECT
				pbd.kodebarang,
				b.nama as namabarang,
				pb.kodecabang,
				b.kodemerk,
				m.nama AS namamerk,
				0 as qtyawalsblm,
				0 as qtymasuksblm,
				0 as qtykeluarsblm,
				0 as qtypicksblm,
				0 AS qtyawal,
				0 AS qtymasuk,
				0 AS qtykeluar,
				CASE
					WHEN pb.kodestatus <> 2 THEN pbd.qty
					ELSE 0
				END AS qtypick,
				b.kodesatuan AS satuan,
				bd.hargajual,
				bd.komisi
			FROM
				srvt_salesorder pb
				LEFT JOIN srvt_sodetail pbd on pb.nomor = pbd.nomor
				LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
				LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
				LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecabang
				LEFT JOIN glbm_gudang g ON g.kode = pb.lokasi
			WHERE
				pb.batal = false
				AND to_char(pb.tanggal, 'YYYYMMDD') >= '" . date('Ymd', strtotime($tglawal)) . "'
				AND to_char(pb.tanggal, 'YYYYMMDD') <= '" . date('Ymd', strtotime($tglakhir)) . "'
				AND b.kodemerk >= '" . $merkawal . "'
				AND b.kodemerk <= '" . $merkakhir . "'
				AND pb.kodecabang = '" . $cabang . "'
			UNION ALL
			SELECT
				pbd.kodebarang,
				b.nama as namabarang,
				pb.kodecabang,
				b.kodemerk,
				m.nama AS namamerk,
				0 AS qtyawalsblm,
				0 AS qtymasuksblm,
				0 AS qtykeluarsblm,
				0 AS qtypicksblm,
				0 as qtyawal,
				0 as qtymasuk,
				pbd.qty as qtykeluar,
				0 as qtypick,
				b.kodesatuan AS satuan,
				bd.hargajual,
				bd.komisi
			FROM
				srvt_invoice pb
				LEFT JOIN srvt_invoicedetail pbd ON pb.nomor = pbd.nomor
				LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
				LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
				LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecabang
				LEFT JOIN glbm_gudang g ON g.kode = pb.lokasi
			WHERE
				pb.batal = false
				AND to_char(pb.tanggal, 'YYYYMMDD') >= '" . date('Ymd', strtotime($tglawal)) . "'
				AND to_char(pb.tanggal, 'YYYYMMDD') <= '" . date('Ymd', strtotime($tglakhir)) . "'
				AND b.kodemerk >= '" . $merkawal . "'
				AND b.kodemerk <= '" . $merkakhir . "'
				AND pb.kodecabang = '" . $cabang . "'
			UNION ALL
			SELECT
				pbd.kodebarang,
				b.nama as namabarang,
				pb.kodecabang,
				b.kodemerk,
				m.nama AS namamerk,
				0 as qtyawalsblm,
				0 as qtymasuksblm,
				0 as qtykeluarsblm,
				0 as qtypicksblm,
				0 AS qtyawal,
				CASE
					WHEN pb.jenis = '2' THEN pbd.qty
					ELSE 0
				END AS qtymasuk,
				CASE
					WHEN pb.jenis <> '2' THEN pbd.qty
					ELSE 0
				END AS qtykeluar,
				0 AS qtypick,
				b.kodesatuan AS satuan,
				bd.hargajual,
				bd.komisi
			FROM
				srvt_bag pb
				LEFT JOIN srvt_bagdetail pbd on pb.nomor = pbd.nomor
				LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
				LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
				LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecabang
				LEFT JOIN glbm_gudang g ON g.kode = pb.lokasi
			WHERE
				pb.batal = false
				AND approve = true
				AND to_char(pb.tanggal, 'YYYYMMDD') >= '" . date('Ymd', strtotime($tglawal)) . "'
				AND to_char(pb.tanggal, 'YYYYMMDD') <= '" . date('Ymd', strtotime($tglakhir)) . "'
				AND b.kodemerk >= '" . $merkawal . "'
				AND b.kodemerk <= '" . $merkakhir . "'
				AND pb.kodecabang = '" . $cabang . "'
			UNION ALL
			SELECT
				pbd.kodebarang,
				b.nama as namabarang,
				pb.kodecbasal as kodecabang,
				b.kodemerk,
				m.nama AS namamerk,
				0 as qtyawalsblm,
				0 as qtymasuksblm,
				0 as qtykeluarsblm,
				0 as qtypicksblm,
				0 AS qtyawal,
				0 AS qtymasuk,
				CASE WHEN pb.kodestatus = 2 THEN pbd.qty ELSE 0 END AS qtykeluar,
            	CASE WHEN pb.kodestatus <> 2 THEN pbd.qty ELSE 0 END AS qtypick,
				b.kodesatuan AS satuan,
				bd.hargajual,
				bd.komisi
			FROM
				srvt_transferstock pb
				LEFT JOIN srvt_transferstockdetail pbd on pb.nomor = pbd.nomor
				LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
				LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
				LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecbasal
				LEFT JOIN glbm_gudang g ON g.kode = pb.kodegdasal
			WHERE
				pb.batal = false
				AND to_char(pb.tanggal, 'YYYYMMDD') >= '" . date('Ymd', strtotime($tglawal)) . "'
				AND to_char(pb.tanggal, 'YYYYMMDD') <= '" . date('Ymd', strtotime($tglakhir)) . "'
				AND b.kodemerk >= '" . $merkawal . "'
				AND b.kodemerk <= '" . $merkakhir . "'
				AND pb.kodecbasal = '" . $cabang . "'
			UNION ALL
			SELECT
				pbd.kodebarang,
				b.nama as namabarang,
				pb.kodecbasal as kodecabang,
				b.kodemerk,
				m.nama AS namamerk,
				0 as qtyawalsblm,
				0 as qtymasuksblm,
				0 as qtykeluarsblm,
				0 as qtypicksblm,
				0 AS qtyawal,
				0 AS qtymasuk,
				pbd.qty AS qtykeluar,
				0 AS qtypick,
				b.kodesatuan AS satuan,
				bd.hargajual,
				bd.komisi
			FROM
				srvt_penerimaantransferstock pb
				LEFT JOIN srvt_penerimaantransferstockdetail pbd on pb.nomor = pbd.nomor
				LEFT JOIN srvt_suratjalangudang sjgd on sjgd.nomor = pb.notransfer
				LEFT JOIN srvt_transferstock tf on tf.nomor = sjgd.noproses
				LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
				LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
				LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecbasal
				LEFT JOIN glbm_gudang g ON g.kode = pb.kodegdasal
			WHERE
				pb.batal = false
				AND to_char(tf.tanggal, 'YYYYMMDD') >= '" . date('Ymd', strtotime($tglawal)) . "'
				AND to_char(tf.tanggal, 'YYYYMMDD') <= '" . date('Ymd', strtotime($tglakhir)) . "'
				AND b.kodemerk >= '" . $merkawal . "'
				AND b.kodemerk <= '" . $merkakhir . "'
				AND pb.kodecbasal = '" . $cabang . "'
			UNION ALL
			SELECT
				pbd.kodebarang,
				b.nama as namabarang,
				pb.kodecbtujuan as kodecabang,
				b.kodemerk,
				m.nama AS namamerk,
				0 as qtyawalsblm,
				0 as qtymasuksblm,
				0 as qtykeluarsblm,
				0 as qtypicksblm,
				0 AS qtyawal,
				pbd.qty AS qtymasuk,
				0 AS qtykeluar,
				0 AS qtypick,
				b.kodesatuan AS satuan,
				bd.hargajual,
				bd.komisi
			FROM
				srvt_penerimaantransferstock pb
				LEFT JOIN srvt_penerimaantransferstockdetail pbd on pb.nomor = pbd.nomor
				LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
				LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
				LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecbtujuan
				LEFT JOIN glbm_gudang g ON g.kode = pb.kodegdtujuan
			WHERE
				pb.batal = false
				AND to_char(pb.tanggal, 'YYYYMMDD') >= '" . date('Ymd', strtotime($tglawal)) . "'
				AND to_char(pb.tanggal, 'YYYYMMDD') <= '" . date('Ymd', strtotime($tglakhir)) . "'
				AND b.kodemerk >= '" . $merkawal . "'
				AND b.kodemerk <= '" . $merkakhir . "'
				AND pb.kodecbtujuan = '" . $cabang . "'
		) hasil_data
	GROUP BY
		hasil_data.kodebarang,
		hasil_data.namabarang,
		hasil_data.kodecabang,
		hasil_data.kodemerk,
		hasil_data.satuan,
		hasil_data.namamerk,
		hasil_data.hargajual,
		hasil_data.komisi
	ORDER BY
		namamerk ASC,
		namabarang ASC")->result();
	}



	function GetdataPersedianPerKategorList($tglawal = "", $tglakhir = "", $merkawal = "", $merkakhir = "", $cabang = "")
	{
		return $this->db->query("SELECT
		-- hasil_data.kodebarang,
		-- hasil_data.namabarang,
		-- hasil_data.kodecabang,
		hasil_data.kodemerk,
		hasil_data.namamerk
	-- 	sum(hasil_data.qtyawalsblm) + SUM(hasil_data.qtymasuksblm) - SUM(hasil_data.qtykeluarsblm) - SUM(hasil_data.qtypicksblm) as qtyawal,
	-- 	sum(hasil_data.qtymasuk) as qtymasuk,
	-- 	sum(hasil_data.qtykeluar) as qtykeluar,
	-- 	sum(hasil_data.qtypick) as qtypick,
	-- 	(
	-- sum(hasil_data.qtyawalsblm) + SUM(hasil_data.qtymasuksblm) - SUM(hasil_data.qtykeluarsblm) - SUM(hasil_data.qtypicksblm) + sum(hasil_data.qtymasuk)
	-- ) - (
	-- ABS(
	-- SUM(hasil_data.qtykeluar) + SUM(hasil_data.qtypick)
	-- )
	-- ) AS stok,
	-- 	hasil_data.satuan,
	-- 	hasil_data.hargajual,
	-- 	hasil_data.komisi
	FROM(
				SELECT
					i.kodebarang,
					b.nama as namabarang,
					i.kodecabang,
					b.kodemerk,
					m.nama AS namamerk,
					i.qtyawal as qtyawalsblm,
					0 AS qtymasuksblm,
					0 AS qtykeluarsblm,
					0 AS qtypicksblm,
					0 as qtyawal,
					0 as qtymasuk,
					0 as qtykeluar,
					0 as qtypick,
					b.kodesatuan AS satuan,
					bd.hargajual,
					bd.komisi
				FROM
					srvt_inventorystok i
					LEFT JOIN glbm_barang b ON b.kode = i.kodebarang
					LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
					LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = i.kodecabang
					LEFT JOIN glbm_gudang g ON g.kode = i.kodegudang
				WHERE
					periode = '202208'
					AND b.kodemerk >= '" . $merkawal . "'
					AND b.kodemerk <= '" . $merkakhir . "'
					AND i.kodecabang = '" . $cabang . "'
				UNION ALL
				SELECT
					pbd.kodebarang,
					b.nama as namabarang,
					pb.kodecabang,
					b.kodemerk,
					m.nama AS namamerk,
					0 AS qtyawalsblm,
					pbd.qty AS qtymasuksblm,
					0 AS qtykeluarsblm,
					0 AS qtypicksblm,
					0 as qtyawal,
					0 as qtymasuk,
					0 as qtykeluar,
					0 as qtypick,
					b.kodesatuan AS satuan,
					bd.hargajual,
					bd.komisi
				FROM
					srvt_penerimaanbarang pb
					LEFT JOIN srvt_penerimaanbarangdetail pbd on pb.nomor = pbd.nomor
					LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
					LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
					LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecabang
					LEFT JOIN glbm_gudang g ON g.kode = pb.kodegudang
				WHERE
					pb.batal = false
					AND to_char(pb.tanggal, 'YYYYMMDD') < '" . date('Ymd', strtotime($tglawal)) . "'
					AND b.kodemerk >= '" . $merkawal . "'
					AND b.kodemerk <= '" . $merkakhir . "'
					AND pb.kodecabang = '" . $cabang . "'
				UNION ALL
				SELECT
					pbd.kodebarang,
					b.nama as namabarang,
					pb.kodecabang,
					b.kodemerk,
					m.nama AS namamerk,
					0 AS qtyawalsblm,
					pbd.qty AS qtymasuksblm,
					0 AS qtykeluarsblm,
					0 AS qtypicksblm,
					0 as qtyawal,
					0 as qtymasuk,
					0 as qtykeluar,
					0 as qtypick,
					b.kodesatuan AS satuan,
					bd.hargajual,
					bd.komisi
				FROM
					srvt_penerimaanbarang pb
					LEFT JOIN srvt_penerimaanbarangdetail pbd on pb.nomor = pbd.nomor
					LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
					LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
					LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecabang
					LEFT JOIN glbm_gudang g ON g.kode = pb.kodegudang
				WHERE
					pb.batal = true
					AND to_char(pb.tglbatal, 'YYYYMMDD') < '" . date('Ymd', strtotime($tglawal)) . "'
					AND b.kodemerk >= '" . $merkawal . "'
					AND b.kodemerk <= '" . $merkakhir . "'
					AND pb.kodecabang = '" . $cabang . "'
				UNION ALL
				SELECT
					pbd.kodebarang,
					b.nama as namabarang,
					pb.kodecabang,
					b.kodemerk,
					m.nama AS namamerk,
					0 AS qtyawalsblm,
					pbd.qty AS qtymasuksblm,
					0 AS qtykeluarsblm,
					0 AS qtypicksblm,
					0 as qtyawal,
					0 as qtymasuk,
					0 as qtykeluar,
					0 as qtypick,
					b.kodesatuan AS satuan,
					bd.hargajual,
					bd.komisi
				FROM
					srvt_returbarang pb
					LEFT JOIN srvt_returbarangdetail pbd on pb.nomor = pbd.nomor
					LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
					LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
					LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecabang
					LEFT JOIN glbm_gudang g ON g.kode = pb.lokasigudang
				WHERE
					to_char(pb.tanggal, 'YYYYMMDD') < '" . date('Ymd', strtotime($tglawal)) . "'
					AND b.kodemerk >= '" . $merkawal . "'
					AND b.kodemerk <= '" . $merkakhir . "'
					AND pb.kodecabang = '" . $cabang . "'
				UNION ALL
				SELECT
					pbd.kodebarang,
					b.nama as namabarang,
					pb.kodecabang,
					b.kodemerk,
					m.nama AS namamerk,
					0 AS qtyawalsblm,
					0 AS qtymasuksblm,
					pbd.qty AS qtykeluarsblm,
					0 AS qtypicksblm,
					0 as qtyawal,
					0 as qtymasuk,
					0 as qtykeluar,
					0 as qtypick,
					b.kodesatuan AS satuan,
					bd.hargajual,
					bd.komisi
				FROM
					srvt_penerimaanbarang pb
					LEFT JOIN srvt_penerimaanbarangdetail pbd on pb.nomor = pbd.nomor
					LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
					LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
					LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecabang
					LEFT JOIN glbm_gudang g ON g.kode = pb.kodegudang
				WHERE
					pb.batal = true
					AND to_char(pb.tglbatal, 'YYYYMMDD') < '" . date('Ymd', strtotime($tglawal)) . "'
					AND b.kodemerk >= '" . $merkawal . "'
					AND b.kodemerk <= '" . $merkakhir . "'
					AND pb.kodecabang = '" . $cabang . "'
				UNION ALL
				SELECT
					pbd.kodebarang,
					b.nama as namabarang,
					pb.kodecabang,
					b.kodemerk,
					m.nama AS namamerk,
					0 AS qtyawalsblm,
					0 AS qtymasuksblm,
					0 AS qtykeluarsblm,
					CASE
						WHEN pb.kodestatus <> 2 THEN pbd.qty
						ELSE 0
					END AS qtypicksblm,
					0 as qtyawal,
					0 as qtymasuk,
					0 as qtykeluar,
					0 as qtypick,
					b.kodesatuan AS satuan,
					bd.hargajual,
					bd.komisi
				FROM
					srvt_salesorder pb
					LEFT JOIN srvt_sodetail pbd on pb.nomor = pbd.nomor
					LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
					LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
					LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecabang
					LEFT JOIN glbm_gudang g ON g.kode = pb.lokasi
				WHERE
					pb.batal = false
					AND to_char(pb.tanggal, 'YYYYMMDD') < '" . date('Ymd', strtotime($tglawal)) . "'
					AND b.kodemerk >= '" . $merkawal . "'
					AND b.kodemerk <= '" . $merkakhir . "'
					AND pb.kodecabang = '" . $cabang . "'
				UNION ALL
				SELECT
					pbd.kodebarang,
					b.nama as namabarang,
					pb.kodecabang,
					b.kodemerk,
					m.nama AS namamerk,
					0 AS qtyawalsblm,
					0 AS qtymasuksblm,
					pbd.qty AS qtykeluarsblm,
					0 AS qtypicksblm,
					0 as qtyawal,
					0 as qtymasuk,
					0 as qtykeluar,
					0 as qtypick,
					b.kodesatuan AS satuan,
					bd.hargajual,
					bd.komisi
				FROM
					srvt_invoice pb
					LEFT JOIN srvt_invoicedetail pbd ON pb.nomor = pbd.nomor
					LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
					LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
					LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecabang
					LEFT JOIN glbm_gudang g ON g.kode = pb.lokasi
				WHERE
					pb.batal = false
					AND to_char(pb.tanggal, 'YYYYMMDD') < '" . date('Ymd', strtotime($tglawal)) . "'
					AND b.kodemerk >= '" . $merkawal . "'
					AND b.kodemerk <= '" . $merkakhir . "'
					AND pb.kodecabang = '" . $cabang . "'
				UNION ALL
				SELECT
					pbd.kodebarang,
					b.nama as namabarang,
					pb.kodecabang,
					b.kodemerk,
					m.nama AS namamerk,
					0 AS qtyawalsblm,
					CASE
						WHEN pb.jenis = '2' THEN pbd.qty
						ELSE 0
					END AS qtymasuksblm,
					CASE
						WHEN pb.jenis <> '2' THEN pbd.qty
						ELSE 0
					END AS qtykeluarsblm,
					0 AS qtypicksblm,
					0 as qtyawal,
					0 as qtymasuk,
					0 as qtykeluar,
					0 as qtypick,
					b.kodesatuan AS satuan,
					bd.hargajual,
					bd.komisi
				FROM
					srvt_bag pb
					LEFT JOIN srvt_bagdetail pbd on pb.nomor = pbd.nomor
					LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
					LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
					LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecabang
					LEFT JOIN glbm_gudang g ON g.kode = pb.lokasi
				WHERE
					pb.batal = false
					AND approve = true
					AND to_char(pb.tanggal, 'YYYYMMDD') < '" . date('Ymd', strtotime($tglawal)) . "'
					AND b.kodemerk >= '" . $merkawal . "'
					AND b.kodemerk <= '" . $merkakhir . "'
					AND pb.kodecabang = '" . $cabang . "'
				UNION ALL
				SELECT
					pbd.kodebarang,
					b.nama as namabarang,
					pb.kodecbasal as kodecabang,
					b.kodemerk,
					m.nama AS namamerk,
					0 AS qtyawalsblm,
					0 AS qtymasuksblm,
					CASE WHEN pb.kodestatus = 2 THEN pbd.qty ELSE 0 END AS qtykeluarsblm,
					CASE WHEN pb.kodestatus <> 2 THEN pbd.qty ELSE 0 END AS qtypicksblm,
					0 as qtyawal,
					0 as qtymasuk,
					0 as qtykeluar,
					0 as qtypick,
					b.kodesatuan AS satuan,
					bd.hargajual,
					bd.komisi
				FROM
					srvt_transferstock pb
					LEFT JOIN srvt_transferstockdetail pbd on pb.nomor = pbd.nomor
					LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
					LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
					LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecbasal
					LEFT JOIN glbm_gudang g ON g.kode = pb.kodegdasal
				WHERE
					pb.batal = false
					AND to_char(pb.tanggal, 'YYYYMMDD') < '" . date('Ymd', strtotime($tglawal)) . "'
					AND b.kodemerk >= '" . $merkawal . "'
					AND b.kodemerk <= '" . $merkakhir . "'
					AND pb.kodecbasal = '" . $cabang . "'
				UNION ALL
				SELECT
					pbd.kodebarang,
					b.nama as namabarang,
					pb.kodecbtujuan as kodecabang,
					b.kodemerk,
					m.nama AS namamerk,
					0 AS qtyawalsblm,
					pbd.qty AS qtymasuksblm,
					0 AS qtykeluarsblm,
					0 AS qtypicksblm,
					0 as qtyawal,
					0 as qtymasuk,
					0 as qtykeluar,
					0 as qtypick,
					b.kodesatuan AS satuan,
					bd.hargajual,
					bd.komisi
				FROM
					srvt_penerimaantransferstock pb
					LEFT JOIN srvt_penerimaantransferstockdetail pbd on pb.nomor = pbd.nomor
					LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
					LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
					LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecbtujuan
					LEFT JOIN glbm_gudang g ON g.kode = pb.kodegdtujuan
				WHERE
					pb.batal = false
					AND to_char(pb.tanggal, 'YYYYMMDD') < '" . date('Ymd', strtotime($tglawal)) . "'
					AND b.kodemerk >= '" . $merkawal . "'
					AND b.kodemerk <= '" . $merkakhir . "'
					AND pb.kodecbtujuan = '" . $cabang . "'
			UNION ALL
			SELECT
				pbd.kodebarang,
				b.nama as namabarang,
				pb.kodecabang,
				b.kodemerk,
				m.nama AS namamerk,
				0 as qtyawalsblm,
				0 as qtymasuksblm,
				0 as qtykeluarsblm,
				0 as qtypicksblm,
				0 AS qtyawal,
				pbd.qty AS qtymasuk,
				0 AS qtykeluar,
				0 AS qtypick,
				b.kodesatuan AS satuan,
				bd.hargajual,
				bd.komisi
			FROM
				srvt_penerimaanbarang pb
				LEFT JOIN srvt_penerimaanbarangdetail pbd on pb.nomor = pbd.nomor
				LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
				LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
				LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecabang
				LEFT JOIN glbm_gudang g ON g.kode = pb.kodegudang
			WHERE
				pb.batal = false
				AND to_char(pb.tanggal, 'YYYYMMDD') >= '" . date('Ymd', strtotime($tglawal)) . "'
				AND to_char(pb.tanggal, 'YYYYMMDD') <= '" . date('Ymd', strtotime($tglakhir)) . "'
				AND b.kodemerk >= '" . $merkawal . "'
				AND b.kodemerk <= '" . $merkakhir . "'
				AND pb.kodecabang = '" . $cabang . "'
			UNION ALL
			SELECT
				pbd.kodebarang,
				b.nama as namabarang,
				pb.kodecabang,
				b.kodemerk,
				m.nama AS namamerk,
				0 as qtyawalsblm,
				0 as qtymasuksblm,
				0 as qtykeluarsblm,
				0 as qtypicksblm,
				0 AS qtyawal,
				pbd.qty AS qtymasuk,
				0 AS qtykeluar,
				0 AS qtypick,
				b.kodesatuan AS satuan,
				bd.hargajual,
				bd.komisi
			FROM
				srvt_penerimaanbarang pb
				LEFT JOIN srvt_penerimaanbarangdetail pbd on pb.nomor = pbd.nomor
				LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
				LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
				LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecabang
				LEFT JOIN glbm_gudang g ON g.kode = pb.kodegudang
			WHERE
				pb.batal = true
				AND to_char(pb.tglbatal, 'YYYYMMDD') >= '" . date('Ymd', strtotime($tglawal)) . "'
				AND to_char(pb.tglbatal, 'YYYYMMDD') <= '" . date('Ymd', strtotime($tglakhir)) . "'
				AND b.kodemerk >= '" . $merkawal . "'
				AND b.kodemerk <= '" . $merkakhir . "'
				AND pb.kodecabang = '" . $cabang . "'
			UNION ALL
			SELECT
				pbd.kodebarang,
				b.nama as namabarang,
				pb.kodecabang,
				b.kodemerk,
				m.nama AS namamerk,
				0 as qtyawalsblm,
				0 as qtymasuksblm,
				0 as qtykeluarsblm,
				0 as qtypicksblm,
				0 AS qtyawal,
				pbd.qty AS qtymasuk,
				0 AS qtykeluar,
				0 AS qtypick,
				b.kodesatuan AS satuan,
				bd.hargajual,
				bd.komisi
			FROM
				srvt_returbarang pb
				LEFT JOIN srvt_returbarangdetail pbd on pb.nomor = pbd.nomor
				LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
				LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
				LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecabang
				LEFT JOIN glbm_gudang g ON g.kode = pb.lokasigudang
			WHERE
				to_char(pb.tanggal, 'YYYYMMDD') >= '" . date('Ymd', strtotime($tglawal)) . "'
				AND to_char(pb.tanggal, 'YYYYMMDD') <= '" . date('Ymd', strtotime($tglakhir)) . "'
				AND b.kodemerk >= '" . $merkawal . "'
				AND b.kodemerk <= '" . $merkakhir . "'
				AND pb.kodecabang = '" . $cabang . "'
			UNION ALL
			SELECT
				pbd.kodebarang,
				b.nama as namabarang,
				pb.kodecabang,
				b.kodemerk,
				m.nama AS namamerk,
				0 as qtyawalsblm,
				0 as qtymasuksblm,
				0 as qtykeluarsblm,
				0 as qtypicksblm,
				0 AS qtyawal,
				0 AS qtymasuk,
				pbd.qty AS qtykeluar,
				0 AS qtypick,
				b.kodesatuan AS satuan,
				bd.hargajual,
				bd.komisi
			FROM
				srvt_penerimaanbarang pb
				LEFT JOIN srvt_penerimaanbarangdetail pbd on pb.nomor = pbd.nomor
				LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
				LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
				LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecabang
				LEFT JOIN glbm_gudang g ON g.kode = pb.kodegudang
			WHERE
				pb.batal = true
				AND to_char(pb.tglbatal, 'YYYYMMDD') >= '" . date('Ymd', strtotime($tglawal)) . "'
				AND to_char(pb.tglbatal, 'YYYYMMDD') <= '" . date('Ymd', strtotime($tglakhir)) . "'
				AND b.kodemerk >= '" . $merkawal . "'
				AND b.kodemerk <= '" . $merkakhir . "'
				AND pb.kodecabang = '" . $cabang . "'
			UNION ALL
			SELECT
				pbd.kodebarang,
				b.nama as namabarang,
				pb.kodecabang,
				b.kodemerk,
				m.nama AS namamerk,
				0 as qtyawalsblm,
				0 as qtymasuksblm,
				0 as qtykeluarsblm,
				0 as qtypicksblm,
				0 AS qtyawal,
				0 AS qtymasuk,
				0 AS qtykeluar,
				CASE
					WHEN pb.kodestatus <> 2 THEN pbd.qty
					ELSE 0
				END AS qtypick,
				b.kodesatuan AS satuan,
				bd.hargajual,
				bd.komisi
			FROM
				srvt_salesorder pb
				LEFT JOIN srvt_sodetail pbd on pb.nomor = pbd.nomor
				LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
				LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
				LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecabang
				LEFT JOIN glbm_gudang g ON g.kode = pb.lokasi
			WHERE
				pb.batal = false
				AND to_char(pb.tanggal, 'YYYYMMDD') >= '" . date('Ymd', strtotime($tglawal)) . "'
				AND to_char(pb.tanggal, 'YYYYMMDD') <= '" . date('Ymd', strtotime($tglakhir)) . "'
				AND b.kodemerk >= '" . $merkawal . "'
				AND b.kodemerk <= '" . $merkakhir . "'
				AND pb.kodecabang = '" . $cabang . "'
			UNION ALL
			SELECT
				pbd.kodebarang,
				b.nama as namabarang,
				pb.kodecabang,
				b.kodemerk,
				m.nama AS namamerk,
				0 AS qtyawalsblm,
				0 AS qtymasuksblm,
				0 AS qtykeluarsblm,
				0 AS qtypicksblm,
				0 as qtyawal,
				0 as qtymasuk,
				pbd.qty as qtykeluar,
				0 as qtypick,
				b.kodesatuan AS satuan,
				bd.hargajual,
				bd.komisi
			FROM
				srvt_invoice pb
				LEFT JOIN srvt_invoicedetail pbd ON pb.nomor = pbd.nomor
				LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
				LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
				LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecabang
				LEFT JOIN glbm_gudang g ON g.kode = pb.lokasi
			WHERE
				pb.batal = false
				AND to_char(pb.tanggal, 'YYYYMMDD') >= '" . date('Ymd', strtotime($tglawal)) . "'
				AND to_char(pb.tanggal, 'YYYYMMDD') <= '" . date('Ymd', strtotime($tglakhir)) . "'
				AND b.kodemerk >= '" . $merkawal . "'
				AND b.kodemerk <= '" . $merkakhir . "'
				AND pb.kodecabang = '" . $cabang . "'
			UNION ALL
			SELECT
				pbd.kodebarang,
				b.nama as namabarang,
				pb.kodecabang,
				b.kodemerk,
				m.nama AS namamerk,
				0 as qtyawalsblm,
				0 as qtymasuksblm,
				0 as qtykeluarsblm,
				0 as qtypicksblm,
				0 AS qtyawal,
				CASE
					WHEN pb.jenis = '2' THEN pbd.qty
					ELSE 0
				END AS qtymasuk,
				CASE
					WHEN pb.jenis <> '2' THEN pbd.qty
					ELSE 0
				END AS qtykeluar,
				0 AS qtypick,
				b.kodesatuan AS satuan,
				bd.hargajual,
				bd.komisi
			FROM
				srvt_bag pb
				LEFT JOIN srvt_bagdetail pbd on pb.nomor = pbd.nomor
				LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
				LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
				LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecabang
				LEFT JOIN glbm_gudang g ON g.kode = pb.lokasi
			WHERE
				pb.batal = false
				AND approve = true
				AND to_char(pb.tanggal, 'YYYYMMDD') >= '" . date('Ymd', strtotime($tglawal)) . "'
				AND to_char(pb.tanggal, 'YYYYMMDD') <= '" . date('Ymd', strtotime($tglakhir)) . "'
				AND b.kodemerk >= '" . $merkawal . "'
				AND b.kodemerk <= '" . $merkakhir . "'
				AND pb.kodecabang = '" . $cabang . "'
			UNION ALL
			SELECT
				pbd.kodebarang,
				b.nama as namabarang,
				pb.kodecbasal as kodecabang,
				b.kodemerk,
				m.nama AS namamerk,
				0 as qtyawalsblm,
				0 as qtymasuksblm,
				0 as qtykeluarsblm,
				0 as qtypicksblm,
				0 AS qtyawal,
				0 AS qtymasuk,
				CASE WHEN pb.kodestatus = 2 THEN pbd.qty ELSE 0 END AS qtykeluar,
            	CASE WHEN pb.kodestatus <> 2 THEN pbd.qty ELSE 0 END AS qtypick,
				b.kodesatuan AS satuan,
				bd.hargajual,
				bd.komisi
			FROM
				srvt_transferstock pb
				LEFT JOIN srvt_transferstockdetail pbd on pb.nomor = pbd.nomor
				LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
				LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
				LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecbasal
				LEFT JOIN glbm_gudang g ON g.kode = pb.kodegdasal
			WHERE
				pb.batal = false
				AND to_char(pb.tanggal, 'YYYYMMDD') >= '" . date('Ymd', strtotime($tglawal)) . "'
				AND to_char(pb.tanggal, 'YYYYMMDD') <= '" . date('Ymd', strtotime($tglakhir)) . "'
				AND b.kodemerk >= '" . $merkawal . "'
				AND b.kodemerk <= '" . $merkakhir . "'
				AND pb.kodecbasal = '" . $cabang . "'
			UNION ALL
			SELECT
				pbd.kodebarang,
				b.nama as namabarang,
				pb.kodecbtujuan as kodecabang,
				b.kodemerk,
				m.nama AS namamerk,
				0 as qtyawalsblm,
				0 as qtymasuksblm,
				0 as qtykeluarsblm,
				0 as qtypicksblm,
				0 AS qtyawal,
				pbd.qty AS qtymasuk,
				0 AS qtykeluar,
				0 AS qtypick,
				b.kodesatuan AS satuan,
				bd.hargajual,
				bd.komisi
			FROM
				srvt_penerimaantransferstock pb
				LEFT JOIN srvt_penerimaantransferstockdetail pbd on pb.nomor = pbd.nomor
				LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
				LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
				LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecbtujuan
				LEFT JOIN glbm_gudang g ON g.kode = pb.kodegdtujuan
			WHERE
				pb.batal = false
				AND to_char(pb.tanggal, 'YYYYMMDD') >= '" . date('Ymd', strtotime($tglawal)) . "'
				AND to_char(pb.tanggal, 'YYYYMMDD') <= '" . date('Ymd', strtotime($tglakhir)) . "'
				AND b.kodemerk >= '" . $merkawal . "'
				AND b.kodemerk <= '" . $merkakhir . "'
				AND pb.kodecbtujuan = '" . $cabang . "'
		) hasil_data
	GROUP BY
	hasil_data.kodemerk,
	hasil_data.namamerk
	ORDER BY
		namamerk ASC")->result();
	}

	function GetDataPersedianPerGudang($tglawal = "", $tglakhir = "", $merkawal = "", $merkakhir = "", $gudang = "", $cabang = "")
	{
		return $this->db->query("SELECT
		hasil_data.kodebarang,
		hasil_data.namabarang,
		hasil_data.kodecabang,
		hasil_data.namagudang,
		hasil_data.kodemerk,
		hasil_data.namamerk,
		sum(hasil_data.qtyawalsblm) + SUM(hasil_data.qtymasuksblm) - SUM(hasil_data.qtykeluarsblm) - SUM(hasil_data.qtypicksblm) as qtyawal,
		sum(hasil_data.qtymasuk) as qtymasuk,
		sum(hasil_data.qtykeluar) as qtykeluar,
		sum(hasil_data.qtypick) as qtypick,
		(
	sum(hasil_data.qtyawalsblm) + SUM(hasil_data.qtymasuksblm) - SUM(hasil_data.qtykeluarsblm) - SUM(hasil_data.qtypicksblm) + sum(hasil_data.qtymasuk)
	) - (
	ABS(
	SUM(hasil_data.qtykeluar) + SUM(hasil_data.qtypick)
	)
	) AS stok,
		hasil_data.satuan,
		hasil_data.hargajual,
		hasil_data.komisi
	FROM(
				SELECT
					i.kodebarang,
					b.nama as namabarang,
					i.kodecabang,
					g.nama AS namagudang,
					b.kodemerk,
					m.nama AS namamerk,
					i.qtyawal as qtyawalsblm,
					0 AS qtymasuksblm,
					0 AS qtykeluarsblm,
					0 AS qtypicksblm,
					0 as qtyawal,
					0 as qtymasuk,
					0 as qtykeluar,
					0 as qtypick,
					b.kodesatuan AS satuan,
					bd.hargajual,
					bd.komisi
				FROM
					srvt_inventorystok i
					LEFT JOIN glbm_barang b ON b.kode = i.kodebarang
					LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
					LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = i.kodecabang
					LEFT JOIN glbm_gudang g ON g.kode = i.kodegudang
				WHERE
					periode = '202208'
					AND b.kodemerk >= '" . $merkawal . "'
					AND b.kodemerk <= '" . $merkakhir . "'
					AND i.kodecabang = '" . $cabang . "'
					AND i.kodegudang = '" . $gudang . "'
				UNION ALL
				SELECT
					pbd.kodebarang,
					b.nama as namabarang,
					pb.kodecabang,
					g.nama AS namagudang,
					b.kodemerk,
					m.nama AS namamerk,
					0 AS qtyawalsblm,
					pbd.qty AS qtymasuksblm,
					0 AS qtykeluarsblm,
					0 AS qtypicksblm,
					0 as qtyawal,
					0 as qtymasuk,
					0 as qtykeluar,
					0 as qtypick,
					b.kodesatuan AS satuan,
					bd.hargajual,
					bd.komisi
				FROM
					srvt_penerimaanbarang pb
					LEFT JOIN srvt_penerimaanbarangdetail pbd on pb.nomor = pbd.nomor
					LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
					LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
					LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecabang
					LEFT JOIN glbm_gudang g ON g.kode = pb.kodegudang
				WHERE
					pb.batal = false
					AND to_char(pb.tanggal, 'YYYYMMDD') < '" . date('Ymd', strtotime($tglawal)) . "'
					AND b.kodemerk >= '" . $merkawal . "'
					AND b.kodemerk <= '" . $merkakhir . "'
					AND pb.kodecabang = '" . $cabang . "'
					AND pb.kodegudang = '" . $gudang . "'
				UNION ALL
				SELECT
					pbd.kodebarang,
					b.nama as namabarang,
					pb.kodecabang,
					g.nama AS namagudang,
					b.kodemerk,
					m.nama AS namamerk,
					0 AS qtyawalsblm,
					pbd.qty AS qtymasuksblm,
					0 AS qtykeluarsblm,
					0 AS qtypicksblm,
					0 as qtyawal,
					0 as qtymasuk,
					0 as qtykeluar,
					0 as qtypick,
					b.kodesatuan AS satuan,
					bd.hargajual,
					bd.komisi
				FROM
					srvt_penerimaanbarang pb
					LEFT JOIN srvt_penerimaanbarangdetail pbd on pb.nomor = pbd.nomor
					LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
					LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
					LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecabang
					LEFT JOIN glbm_gudang g ON g.kode = pb.kodegudang
				WHERE
					pb.batal = true
					AND to_char(pb.tglbatal, 'YYYYMMDD') < '" . date('Ymd', strtotime($tglawal)) . "'
					AND b.kodemerk >= '" . $merkawal . "'
					AND b.kodemerk <= '" . $merkakhir . "'
					AND pb.kodecabang = '" . $cabang . "'
					AND pb.kodegudang = '" . $gudang . "'
				UNION ALL
				SELECT
					pbd.kodebarang,
					b.nama as namabarang,
					pb.kodecabang,
					g.nama AS namagudang,
					b.kodemerk,
					m.nama AS namamerk,
					0 AS qtyawalsblm,
					pbd.qty AS qtymasuksblm,
					0 AS qtykeluarsblm,
					0 AS qtypicksblm,
					0 as qtyawal,
					0 as qtymasuk,
					0 as qtykeluar,
					0 as qtypick,
					b.kodesatuan AS satuan,
					bd.hargajual,
					bd.komisi
				FROM
					srvt_returbarang pb
					LEFT JOIN srvt_returbarangdetail pbd on pb.nomor = pbd.nomor
					LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
					LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
					LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecabang
					LEFT JOIN glbm_gudang g ON g.kode = pb.lokasigudang
				WHERE
					to_char(pb.tanggal, 'YYYYMMDD') < '" . date('Ymd', strtotime($tglawal)) . "'
					AND b.kodemerk >= '" . $merkawal . "'
					AND b.kodemerk <= '" . $merkakhir . "'
					AND pb.kodecabang = '" . $cabang . "'
					AND pb.lokasigudang = '" . $gudang . "'
				UNION ALL
				SELECT
					pbd.kodebarang,
					b.nama as namabarang,
					pb.kodecabang,
					g.nama AS namagudang,
					b.kodemerk,
					m.nama AS namamerk,
					0 AS qtyawalsblm,
					0 AS qtymasuksblm,
					pbd.qty AS qtykeluarsblm,
					0 AS qtypicksblm,
					0 as qtyawal,
					0 as qtymasuk,
					0 as qtykeluar,
					0 as qtypick,
					b.kodesatuan AS satuan,
					bd.hargajual,
					bd.komisi
				FROM
					srvt_penerimaanbarang pb
					LEFT JOIN srvt_penerimaanbarangdetail pbd on pb.nomor = pbd.nomor
					LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
					LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
					LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecabang
					LEFT JOIN glbm_gudang g ON g.kode = pb.kodegudang
				WHERE
					pb.batal = true
					AND to_char(pb.tglbatal, 'YYYYMMDD') < '" . date('Ymd', strtotime($tglawal)) . "'
					AND b.kodemerk >= '" . $merkawal . "'
					AND b.kodemerk <= '" . $merkakhir . "'
					AND pb.kodecabang = '" . $cabang . "'
					AND pb.kodegudang = '" . $gudang . "'
				UNION ALL
				SELECT
					pbd.kodebarang,
					b.nama as namabarang,
					pb.kodecabang,
					g.nama AS namagudang,
					b.kodemerk,
					m.nama AS namamerk,
					0 AS qtyawalsblm,
					0 AS qtymasuksblm,
					0 AS qtykeluarsblm,
					CASE
						WHEN pb.kodestatus <> 2 THEN pbd.qty
						ELSE 0
					END AS qtypicksblm,
					0 as qtyawal,
					0 as qtymasuk,
					0 as qtykeluar,
					0 as qtypick,
					b.kodesatuan AS satuan,
					bd.hargajual,
					bd.komisi
				FROM
					srvt_salesorder pb
					LEFT JOIN srvt_sodetail pbd on pb.nomor = pbd.nomor
					LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
					LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
					LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecabang
					LEFT JOIN glbm_gudang g ON g.kode = pb.lokasi
				WHERE
					pb.batal = false
					AND to_char(pb.tanggal, 'YYYYMMDD') < '" . date('Ymd', strtotime($tglawal)) . "'
					AND b.kodemerk >= '" . $merkawal . "'
					AND b.kodemerk <= '" . $merkakhir . "'
					AND pb.kodecabang = '" . $cabang . "'
					AND pb.lokasi = '" . $gudang . "'
				UNION ALL
				SELECT
					pbd.kodebarang,
					b.nama as namabarang,
					pb.kodecabang,
					g.nama AS namagudang,
					b.kodemerk,
					m.nama AS namamerk,
					0 AS qtyawalsblm,
					0 AS qtymasuksblm,
					pbd.qty AS qtykeluarsblm,
					0 AS qtypicksblm,
					0 as qtyawal,
					0 as qtymasuk,
					0 as qtykeluar,
					0 as qtypick,
					b.kodesatuan AS satuan,
					bd.hargajual,
					bd.komisi
				FROM
					srvt_invoice pb
					LEFT JOIN srvt_invoicedetail pbd ON pb.nomor = pbd.nomor
					LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
					LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
					LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecabang
					LEFT JOIN glbm_gudang g ON g.kode = pb.lokasi
				WHERE
					pb.batal = false
					AND to_char(pb.tanggal, 'YYYYMMDD') < '" . date('Ymd', strtotime($tglawal)) . "'
					AND b.kodemerk >= '" . $merkawal . "'
					AND b.kodemerk <= '" . $merkakhir . "'
					AND pb.kodecabang = '" . $cabang . "'
					AND pb.lokasi = '" . $gudang . "'
				UNION ALL
				SELECT
					pbd.kodebarang,
					b.nama as namabarang,
					pb.kodecabang,
					g.nama AS namagudang,
					b.kodemerk,
					m.nama AS namamerk,
					0 AS qtyawalsblm,
					CASE
						WHEN pb.jenis = '2' THEN pbd.qty
						ELSE 0
					END AS qtymasuksblm,
					CASE
						WHEN pb.jenis <> '2' THEN pbd.qty
						ELSE 0
					END AS qtykeluarsblm,
					0 AS qtypicksblm,
					0 as qtyawal,
					0 as qtymasuk,
					0 as qtykeluar,
					0 as qtypick,
					b.kodesatuan AS satuan,
					bd.hargajual,
					bd.komisi
				FROM
					srvt_bag pb
					LEFT JOIN srvt_bagdetail pbd on pb.nomor = pbd.nomor
					LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
					LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
					LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecabang
					LEFT JOIN glbm_gudang g ON g.kode = pb.lokasi
				WHERE
					pb.batal = false
					AND approve = true
					AND to_char(pb.tanggal, 'YYYYMMDD') < '" . date('Ymd', strtotime($tglawal)) . "'
					AND b.kodemerk >= '" . $merkawal . "'
					AND b.kodemerk <= '" . $merkakhir . "'
					AND pb.kodecabang = '" . $cabang . "'
					AND pb.lokasi = '" . $gudang . "'
				UNION ALL
				SELECT
					pbd.kodebarang,
					b.nama as namabarang,
					pb.kodecbasal as kodecabang,
					g.nama AS namagudang,
					b.kodemerk,
					m.nama AS namamerk,
					0 AS qtyawalsblm,
					0 AS qtymasuksblm,
					CASE WHEN pb.kodestatus = 2 THEN pbd.qty ELSE 0 END AS qtykeluarsblm,
					CASE WHEN pb.kodestatus <> 2 THEN pbd.qty ELSE 0 END AS qtypicksblm,
					0 as qtyawal,
					0 as qtymasuk,
					0 as qtykeluar,
					0 as qtypick,
					b.kodesatuan AS satuan,
					bd.hargajual,
					bd.komisi
				FROM
					srvt_transferstock pb
					LEFT JOIN srvt_transferstockdetail pbd on pb.nomor = pbd.nomor
					LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
					LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
					LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecbasal
					LEFT JOIN glbm_gudang g ON g.kode = pb.kodegdasal
				WHERE
					pb.batal = false
					AND to_char(pb.tanggal, 'YYYYMMDD') < '" . date('Ymd', strtotime($tglawal)) . "'
					AND b.kodemerk >= '" . $merkawal . "'
					AND b.kodemerk <= '" . $merkakhir . "'
					AND pb.kodecbasal = '" . $cabang . "'
					AND pb.kodegdasal = '" . $gudang . "'
				UNION ALL
				SELECT
					pbd.kodebarang,
					b.nama as namabarang,
					pb.kodecbtujuan as kodecabang,
					g.nama AS namagudang,
					b.kodemerk,
					m.nama AS namamerk,
					0 AS qtyawalsblm,
					pbd.qty AS qtymasuksblm,
					0 AS qtykeluarsblm,
					0 AS qtypicksblm,
					0 as qtyawal,
					0 as qtymasuk,
					0 as qtykeluar,
					0 as qtypick,
					b.kodesatuan AS satuan,
					bd.hargajual,
					bd.komisi
				FROM
					srvt_penerimaantransferstock pb
					LEFT JOIN srvt_penerimaantransferstockdetail pbd on pb.nomor = pbd.nomor
					LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
					LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
					LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecbtujuan
					LEFT JOIN glbm_gudang g ON g.kode = pb.kodegdtujuan
				WHERE
					pb.batal = false
					AND to_char(pb.tanggal, 'YYYYMMDD') < '" . date('Ymd', strtotime($tglawal)) . "'
					AND b.kodemerk >= '" . $merkawal . "'
					AND b.kodemerk <= '" . $merkakhir . "'
					AND pb.kodecbtujuan = '" . $cabang . "'
					AND pb.kodegdtujuan = '" . $gudang . "'
			UNION ALL
			SELECT
				pbd.kodebarang,
				b.nama as namabarang,
				pb.kodecabang,
				g.nama AS namagudang,
				b.kodemerk,
				m.nama AS namamerk,
				0 as qtyawalsblm,
				0 as qtymasuksblm,
				0 as qtykeluarsblm,
				0 as qtypicksblm,
				0 AS qtyawal,
				pbd.qty AS qtymasuk,
				0 AS qtykeluar,
				0 AS qtypick,
				b.kodesatuan AS satuan,
				bd.hargajual,
				bd.komisi
			FROM
				srvt_penerimaanbarang pb
				LEFT JOIN srvt_penerimaanbarangdetail pbd on pb.nomor = pbd.nomor
				LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
				LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
				LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecabang
				LEFT JOIN glbm_gudang g ON g.kode = pb.kodegudang
			WHERE
				pb.batal = false
				AND to_char(pb.tanggal, 'YYYYMMDD') >= '" . date('Ymd', strtotime($tglawal)) . "'
				AND to_char(pb.tanggal, 'YYYYMMDD') <= '" . date('Ymd', strtotime($tglakhir)) . "'
				AND b.kodemerk >= '" . $merkawal . "'
				AND b.kodemerk <= '" . $merkakhir . "'
				AND pb.kodecabang = '" . $cabang . "'
				AND pb.kodegudang = '" . $gudang . "'
			UNION ALL
			SELECT
				pbd.kodebarang,
				b.nama as namabarang,
				pb.kodecabang,
				g.nama AS namagudang,
				b.kodemerk,
				m.nama AS namamerk,
				0 as qtyawalsblm,
				0 as qtymasuksblm,
				0 as qtykeluarsblm,
				0 as qtypicksblm,
				0 AS qtyawal,
				pbd.qty AS qtymasuk,
				0 AS qtykeluar,
				0 AS qtypick,
				b.kodesatuan AS satuan,
				bd.hargajual,
				bd.komisi
			FROM
				srvt_penerimaanbarang pb
				LEFT JOIN srvt_penerimaanbarangdetail pbd on pb.nomor = pbd.nomor
				LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
				LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
				LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecabang
				LEFT JOIN glbm_gudang g ON g.kode = pb.kodegudang
			WHERE
				pb.batal = true
				AND to_char(pb.tglbatal, 'YYYYMMDD') >= '" . date('Ymd', strtotime($tglawal)) . "'
				AND to_char(pb.tglbatal, 'YYYYMMDD') <= '" . date('Ymd', strtotime($tglakhir)) . "'
				AND b.kodemerk >= '" . $merkawal . "'
				AND b.kodemerk <= '" . $merkakhir . "'
				AND pb.kodecabang = '" . $cabang . "'
				AND pb.kodegudang = '" . $gudang . "'
			UNION ALL
			SELECT
				pbd.kodebarang,
				b.nama as namabarang,
				pb.kodecabang,
				g.nama AS namagudang,
				b.kodemerk,
				m.nama AS namamerk,
				0 as qtyawalsblm,
				0 as qtymasuksblm,
				0 as qtykeluarsblm,
				0 as qtypicksblm,
				0 AS qtyawal,
				pbd.qty AS qtymasuk,
				0 AS qtykeluar,
				0 AS qtypick,
				b.kodesatuan AS satuan,
				bd.hargajual,
				bd.komisi
			FROM
				srvt_returbarang pb
				LEFT JOIN srvt_returbarangdetail pbd on pb.nomor = pbd.nomor
				LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
				LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
				LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecabang
				LEFT JOIN glbm_gudang g ON g.kode = pb.lokasigudang
			WHERE
				to_char(pb.tanggal, 'YYYYMMDD') >= '" . date('Ymd', strtotime($tglawal)) . "'
				AND to_char(pb.tanggal, 'YYYYMMDD') <= '" . date('Ymd', strtotime($tglakhir)) . "'
				AND b.kodemerk >= '" . $merkawal . "'
				AND b.kodemerk <= '" . $merkakhir . "'
				AND pb.kodecabang = '" . $cabang . "'
				AND pb.lokasigudang = '" . $gudang . "'
			UNION ALL
			SELECT
				pbd.kodebarang,
				b.nama as namabarang,
				pb.kodecabang,
				g.nama AS namagudang,
				b.kodemerk,
				m.nama AS namamerk,
				0 as qtyawalsblm,
				0 as qtymasuksblm,
				0 as qtykeluarsblm,
				0 as qtypicksblm,
				0 AS qtyawal,
				0 AS qtymasuk,
				pbd.qty AS qtykeluar,
				0 AS qtypick,
				b.kodesatuan AS satuan,
				bd.hargajual,
				bd.komisi
			FROM
				srvt_penerimaanbarang pb
				LEFT JOIN srvt_penerimaanbarangdetail pbd on pb.nomor = pbd.nomor
				LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
				LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
				LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecabang
				LEFT JOIN glbm_gudang g ON g.kode = pb.kodegudang
			WHERE
				pb.batal = true
				AND to_char(pb.tglbatal, 'YYYYMMDD') >= '" . date('Ymd', strtotime($tglawal)) . "'
				AND to_char(pb.tglbatal, 'YYYYMMDD') <= '" . date('Ymd', strtotime($tglakhir)) . "'
				AND b.kodemerk >= '" . $merkawal . "'
				AND b.kodemerk <= '" . $merkakhir . "'
				AND pb.kodecabang = '" . $cabang . "'
				AND pb.kodegudang = '" . $gudang . "'
			UNION ALL
			SELECT
				pbd.kodebarang,
				b.nama as namabarang,
				pb.kodecabang,
				g.nama AS namagudang,
				b.kodemerk,
				m.nama AS namamerk,
				0 as qtyawalsblm,
				0 as qtymasuksblm,
				0 as qtykeluarsblm,
				0 as qtypicksblm,
				0 AS qtyawal,
				0 AS qtymasuk,
				0 AS qtykeluar,
				CASE
					WHEN pb.kodestatus <> 2 THEN pbd.qty
					ELSE 0
				END AS qtypick,
				b.kodesatuan AS satuan,
				bd.hargajual,
				bd.komisi
			FROM
				srvt_salesorder pb
				LEFT JOIN srvt_sodetail pbd on pb.nomor = pbd.nomor
				LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
				LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
				LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecabang
				LEFT JOIN glbm_gudang g ON g.kode = pb.lokasi
			WHERE
				pb.batal = false
				AND to_char(pb.tanggal, 'YYYYMMDD') >= '" . date('Ymd', strtotime($tglawal)) . "'
				AND to_char(pb.tanggal, 'YYYYMMDD') <= '" . date('Ymd', strtotime($tglakhir)) . "'
				AND b.kodemerk >= '" . $merkawal . "'
				AND b.kodemerk <= '" . $merkakhir . "'
				AND pb.kodecabang = '" . $cabang . "'
				AND pb.lokasi = '" . $gudang . "'
			UNION ALL
			SELECT
				pbd.kodebarang,
				b.nama as namabarang,
				pb.kodecabang,
				g.nama AS namagudang,
				b.kodemerk,
				m.nama AS namamerk,
				0 AS qtyawalsblm,
				0 AS qtymasuksblm,
				0 AS qtykeluarsblm,
				0 AS qtypicksblm,
				0 as qtyawal,
				0 as qtymasuk,
				pbd.qty as qtykeluar,
				0 as qtypick,
				b.kodesatuan AS satuan,
				bd.hargajual,
				bd.komisi
			FROM
				srvt_invoice pb
				LEFT JOIN srvt_invoicedetail pbd ON pb.nomor = pbd.nomor
				LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
				LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
				LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecabang
				LEFT JOIN glbm_gudang g ON g.kode = pb.lokasi
			WHERE
				pb.batal = false
				AND to_char(pb.tanggal, 'YYYYMMDD') >= '" . date('Ymd', strtotime($tglawal)) . "'
				AND to_char(pb.tanggal, 'YYYYMMDD') <= '" . date('Ymd', strtotime($tglakhir)) . "'
				AND b.kodemerk >= '" . $merkawal . "'
				AND b.kodemerk <= '" . $merkakhir . "'
				AND pb.kodecabang = '" . $cabang . "'
				AND pb.lokasi = '" . $gudang . "'
			UNION ALL
			SELECT
				pbd.kodebarang,
				b.nama as namabarang,
				pb.kodecabang,
				g.nama AS namagudang,
				b.kodemerk,
				m.nama AS namamerk,
				0 as qtyawalsblm,
				0 as qtymasuksblm,
				0 as qtykeluarsblm,
				0 as qtypicksblm,
				0 AS qtyawal,
				CASE
					WHEN pb.jenis = '2' THEN pbd.qty
					ELSE 0
				END AS qtymasuk,
				CASE
					WHEN pb.jenis <> '2' THEN pbd.qty
					ELSE 0
				END AS qtykeluar,
				0 AS qtypick,
				b.kodesatuan AS satuan,
				bd.hargajual,
				bd.komisi
			FROM
				srvt_bag pb
				LEFT JOIN srvt_bagdetail pbd on pb.nomor = pbd.nomor
				LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
				LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
				LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecabang
				LEFT JOIN glbm_gudang g ON g.kode = pb.lokasi
			WHERE
				pb.batal = false
				AND approve = true
				AND to_char(pb.tanggal, 'YYYYMMDD') >= '" . date('Ymd', strtotime($tglawal)) . "'
				AND to_char(pb.tanggal, 'YYYYMMDD') <= '" . date('Ymd', strtotime($tglakhir)) . "'
				AND b.kodemerk >= '" . $merkawal . "'
				AND b.kodemerk <= '" . $merkakhir . "'
				AND pb.kodecabang = '" . $cabang . "'
				AND pb.lokasi = '" . $gudang . "'
			UNION ALL
			SELECT
				pbd.kodebarang,
				b.nama as namabarang,
				pb.kodecbasal as kodecabang,
				g.nama AS namagudang,
				b.kodemerk,
				m.nama AS namamerk,
				0 as qtyawalsblm,
				0 as qtymasuksblm,
				0 as qtykeluarsblm,
				0 as qtypicksblm,
				0 AS qtyawal,
				0 AS qtymasuk,
				CASE WHEN pb.kodestatus = 2 THEN pbd.qty ELSE 0 END AS qtykeluar,
            	CASE WHEN pb.kodestatus <> 2 THEN pbd.qty ELSE 0 END AS qtypick,
				b.kodesatuan AS satuan,
				bd.hargajual,
				bd.komisi
			FROM
				srvt_transferstock pb
				LEFT JOIN srvt_transferstockdetail pbd on pb.nomor = pbd.nomor
				LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
				LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
				LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecbasal
				LEFT JOIN glbm_gudang g ON g.kode = pb.kodegdasal
			WHERE
				pb.batal = false
				AND to_char(pb.tanggal, 'YYYYMMDD') >= '" . date('Ymd', strtotime($tglawal)) . "'
				AND to_char(pb.tanggal, 'YYYYMMDD') <= '" . date('Ymd', strtotime($tglakhir)) . "'
				AND b.kodemerk >= '" . $merkawal . "'
				AND b.kodemerk <= '" . $merkakhir . "'
				AND pb.kodecbasal = '" . $cabang . "'
				AND pb.kodegdasal = '" . $gudang . "'
			UNION ALL
			SELECT
				pbd.kodebarang,
				b.nama as namabarang,
				pb.kodecbtujuan as kodecabang,
				g.nama AS namagudang,
				b.kodemerk,
				m.nama AS namamerk,
				0 as qtyawalsblm,
				0 as qtymasuksblm,
				0 as qtykeluarsblm,
				0 as qtypicksblm,
				0 AS qtyawal,
				pbd.qty AS qtymasuk,
				0 AS qtykeluar,
				0 AS qtypick,
				b.kodesatuan AS satuan,
				bd.hargajual,
				bd.komisi
			FROM
				srvt_penerimaantransferstock pb
				LEFT JOIN srvt_penerimaantransferstockdetail pbd on pb.nomor = pbd.nomor
				LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
				LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
				LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecbtujuan
				LEFT JOIN glbm_gudang g ON g.kode = pb.kodegdtujuan
			WHERE
				pb.batal = false
				AND to_char(pb.tanggal, 'YYYYMMDD') >= '" . date('Ymd', strtotime($tglawal)) . "'
				AND to_char(pb.tanggal, 'YYYYMMDD') <= '" . date('Ymd', strtotime($tglakhir)) . "'
				AND b.kodemerk >= '" . $merkawal . "'
				AND b.kodemerk <= '" . $merkakhir . "'
				AND pb.kodecbtujuan = '" . $cabang . "'
				AND pb.kodegdtujuan = '" . $gudang . "'
		) hasil_data
	GROUP BY
		hasil_data.kodebarang,
		hasil_data.namabarang,
		hasil_data.kodecabang,
		hasil_data.namagudang,
		hasil_data.kodemerk,
		hasil_data.satuan,
		hasil_data.namamerk,
		hasil_data.hargajual,
		hasil_data.komisi
	ORDER BY
		namamerk ASC,
		namabarang ASC")->result();
	}
	function GetDataPersedianPerGudangList($tglawal = "", $tglakhir = "", $merkawal = "", $merkakhir = "", $gudang = "", $cabang = "")
	{
		return $this->db->query("SELECT
		-- hasil_data.kodebarang,
		-- hasil_data.namabarang,
		-- hasil_data.kodecabang,
		-- hasil_data.namagudang,
		hasil_data.kodemerk,
		hasil_data.namamerk
	-- 	sum(hasil_data.qtyawalsblm) + SUM(hasil_data.qtymasuksblm) - SUM(hasil_data.qtykeluarsblm) - SUM(hasil_data.qtypicksblm) as qtyawal,
	-- 	sum(hasil_data.qtymasuk) as qtymasuk,
	-- 	sum(hasil_data.qtykeluar) as qtykeluar,
	-- 	sum(hasil_data.qtypick) as qtypick,
	-- 	(
	-- sum(hasil_data.qtyawalsblm) + SUM(hasil_data.qtymasuksblm) - SUM(hasil_data.qtykeluarsblm) - SUM(hasil_data.qtypicksblm) + sum(hasil_data.qtymasuk)
	-- ) - (
	-- ABS(
	-- SUM(hasil_data.qtykeluar) + SUM(hasil_data.qtypick)
	-- )
	-- ) AS stok,
	-- 	hasil_data.satuan,
	-- 	hasil_data.hargajual,
	-- 	hasil_data.komisi
	FROM(
				SELECT
					i.kodebarang,
					b.nama as namabarang,
					i.kodecabang,
					g.nama AS namagudang,
					b.kodemerk,
					m.nama AS namamerk,
					i.qtyawal as qtyawalsblm,
					0 AS qtymasuksblm,
					0 AS qtykeluarsblm,
					0 AS qtypicksblm,
					0 as qtyawal,
					0 as qtymasuk,
					0 as qtykeluar,
					0 as qtypick,
					b.kodesatuan AS satuan,
					bd.hargajual,
					bd.komisi
				FROM
					srvt_inventorystok i
					LEFT JOIN glbm_barang b ON b.kode = i.kodebarang
					LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
					LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = i.kodecabang
					LEFT JOIN glbm_gudang g ON g.kode = i.kodegudang
				WHERE
					periode = '202208'
					AND b.kodemerk >= '" . $merkawal . "'
					AND b.kodemerk <= '" . $merkakhir . "'
					AND i.kodecabang = '" . $cabang . "'
					AND i.kodegudang = '" . $gudang . "'
				UNION ALL
				SELECT
					pbd.kodebarang,
					b.nama as namabarang,
					pb.kodecabang,
					g.nama AS namagudang,
					b.kodemerk,
					m.nama AS namamerk,
					0 AS qtyawalsblm,
					pbd.qty AS qtymasuksblm,
					0 AS qtykeluarsblm,
					0 AS qtypicksblm,
					0 as qtyawal,
					0 as qtymasuk,
					0 as qtykeluar,
					0 as qtypick,
					b.kodesatuan AS satuan,
					bd.hargajual,
					bd.komisi
				FROM
					srvt_penerimaanbarang pb
					LEFT JOIN srvt_penerimaanbarangdetail pbd on pb.nomor = pbd.nomor
					LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
					LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
					LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecabang
					LEFT JOIN glbm_gudang g ON g.kode = pb.kodegudang
				WHERE
					pb.batal = false
					AND to_char(pb.tanggal, 'YYYYMMDD') < '" . date('Ymd', strtotime($tglawal)) . "'
					AND b.kodemerk >= '" . $merkawal . "'
					AND b.kodemerk <= '" . $merkakhir . "'
					AND pb.kodecabang = '" . $cabang . "'
					AND pb.kodegudang = '" . $gudang . "'
				UNION ALL
				SELECT
					pbd.kodebarang,
					b.nama as namabarang,
					pb.kodecabang,
					g.nama AS namagudang,
					b.kodemerk,
					m.nama AS namamerk,
					0 AS qtyawalsblm,
					pbd.qty AS qtymasuksblm,
					0 AS qtykeluarsblm,
					0 AS qtypicksblm,
					0 as qtyawal,
					0 as qtymasuk,
					0 as qtykeluar,
					0 as qtypick,
					b.kodesatuan AS satuan,
					bd.hargajual,
					bd.komisi
				FROM
					srvt_penerimaanbarang pb
					LEFT JOIN srvt_penerimaanbarangdetail pbd on pb.nomor = pbd.nomor
					LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
					LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
					LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecabang
					LEFT JOIN glbm_gudang g ON g.kode = pb.kodegudang
				WHERE
					pb.batal = true
					AND to_char(pb.tglbatal, 'YYYYMMDD') < '" . date('Ymd', strtotime($tglawal)) . "'
					AND b.kodemerk >= '" . $merkawal . "'
					AND b.kodemerk <= '" . $merkakhir . "'
					AND pb.kodecabang = '" . $cabang . "'
					AND pb.kodegudang = '" . $gudang . "'
				UNION ALL
				SELECT
					pbd.kodebarang,
					b.nama as namabarang,
					pb.kodecabang,
					g.nama AS namagudang,
					b.kodemerk,
					m.nama AS namamerk,
					0 AS qtyawalsblm,
					pbd.qty AS qtymasuksblm,
					0 AS qtykeluarsblm,
					0 AS qtypicksblm,
					0 as qtyawal,
					0 as qtymasuk,
					0 as qtykeluar,
					0 as qtypick,
					b.kodesatuan AS satuan,
					bd.hargajual,
					bd.komisi
				FROM
					srvt_returbarang pb
					LEFT JOIN srvt_returbarangdetail pbd on pb.nomor = pbd.nomor
					LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
					LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
					LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecabang
					LEFT JOIN glbm_gudang g ON g.kode = pb.lokasigudang
				WHERE
					to_char(pb.tanggal, 'YYYYMMDD') < '" . date('Ymd', strtotime($tglawal)) . "'
					AND b.kodemerk >= '" . $merkawal . "'
					AND b.kodemerk <= '" . $merkakhir . "'
					AND pb.kodecabang = '" . $cabang . "'
					AND pb.lokasigudang = '" . $gudang . "'
				UNION ALL
				SELECT
					pbd.kodebarang,
					b.nama as namabarang,
					pb.kodecabang,
					g.nama AS namagudang,
					b.kodemerk,
					m.nama AS namamerk,
					0 AS qtyawalsblm,
					0 AS qtymasuksblm,
					pbd.qty AS qtykeluarsblm,
					0 AS qtypicksblm,
					0 as qtyawal,
					0 as qtymasuk,
					0 as qtykeluar,
					0 as qtypick,
					b.kodesatuan AS satuan,
					bd.hargajual,
					bd.komisi
				FROM
					srvt_penerimaanbarang pb
					LEFT JOIN srvt_penerimaanbarangdetail pbd on pb.nomor = pbd.nomor
					LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
					LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
					LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecabang
					LEFT JOIN glbm_gudang g ON g.kode = pb.kodegudang
				WHERE
					pb.batal = true
					AND to_char(pb.tglbatal, 'YYYYMMDD') < '" . date('Ymd', strtotime($tglawal)) . "'
					AND b.kodemerk >= '" . $merkawal . "'
					AND b.kodemerk <= '" . $merkakhir . "'
					AND pb.kodecabang = '" . $cabang . "'
					AND pb.kodegudang = '" . $gudang . "'
				UNION ALL
				SELECT
					pbd.kodebarang,
					b.nama as namabarang,
					pb.kodecabang,
					g.nama AS namagudang,
					b.kodemerk,
					m.nama AS namamerk,
					0 AS qtyawalsblm,
					0 AS qtymasuksblm,
					0 AS qtykeluarsblm,
					CASE
						WHEN pb.kodestatus <> 2 THEN pbd.qty
						ELSE 0
					END AS qtypicksblm,
					0 as qtyawal,
					0 as qtymasuk,
					0 as qtykeluar,
					0 as qtypick,
					b.kodesatuan AS satuan,
					bd.hargajual,
					bd.komisi
				FROM
					srvt_salesorder pb
					LEFT JOIN srvt_sodetail pbd on pb.nomor = pbd.nomor
					LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
					LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
					LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecabang
					LEFT JOIN glbm_gudang g ON g.kode = pb.lokasi
				WHERE
					pb.batal = false
					AND to_char(pb.tanggal, 'YYYYMMDD') < '" . date('Ymd', strtotime($tglawal)) . "'
					AND b.kodemerk >= '" . $merkawal . "'
					AND b.kodemerk <= '" . $merkakhir . "'
					AND pb.kodecabang = '" . $cabang . "'
					AND pb.lokasi = '" . $gudang . "'
				UNION ALL
				SELECT
					pbd.kodebarang,
					b.nama as namabarang,
					pb.kodecabang,
					g.nama AS namagudang,
					b.kodemerk,
					m.nama AS namamerk,
					0 AS qtyawalsblm,
					0 AS qtymasuksblm,
					pbd.qty AS qtykeluarsblm,
					0 AS qtypicksblm,
					0 as qtyawal,
					0 as qtymasuk,
					0 as qtykeluar,
					0 as qtypick,
					b.kodesatuan AS satuan,
					bd.hargajual,
					bd.komisi
				FROM
					srvt_invoice pb
					LEFT JOIN srvt_invoicedetail pbd ON pb.nomor = pbd.nomor
					LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
					LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
					LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecabang
					LEFT JOIN glbm_gudang g ON g.kode = pb.lokasi
				WHERE
					pb.batal = false
					AND to_char(pb.tanggal, 'YYYYMMDD') < '" . date('Ymd', strtotime($tglawal)) . "'
					AND b.kodemerk >= '" . $merkawal . "'
					AND b.kodemerk <= '" . $merkakhir . "'
					AND pb.kodecabang = '" . $cabang . "'
					AND pb.lokasi = '" . $gudang . "'
				UNION ALL
				SELECT
					pbd.kodebarang,
					b.nama as namabarang,
					pb.kodecabang,
					g.nama AS namagudang,
					b.kodemerk,
					m.nama AS namamerk,
					0 AS qtyawalsblm,
					CASE
						WHEN pb.jenis = '2' THEN pbd.qty
						ELSE 0
					END AS qtymasuksblm,
					CASE
						WHEN pb.jenis <> '2' THEN pbd.qty
						ELSE 0
					END AS qtykeluarsblm,
					0 AS qtypicksblm,
					0 as qtyawal,
					0 as qtymasuk,
					0 as qtykeluar,
					0 as qtypick,
					b.kodesatuan AS satuan,
					bd.hargajual,
					bd.komisi
				FROM
					srvt_bag pb
					LEFT JOIN srvt_bagdetail pbd on pb.nomor = pbd.nomor
					LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
					LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
					LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecabang
					LEFT JOIN glbm_gudang g ON g.kode = pb.lokasi
				WHERE
					pb.batal = false
					AND approve = true
					AND to_char(pb.tanggal, 'YYYYMMDD') < '" . date('Ymd', strtotime($tglawal)) . "'
					AND b.kodemerk >= '" . $merkawal . "'
					AND b.kodemerk <= '" . $merkakhir . "'
					AND pb.kodecabang = '" . $cabang . "'
					AND pb.lokasi = '" . $gudang . "'
				UNION ALL
				SELECT
					pbd.kodebarang,
					b.nama as namabarang,
					pb.kodecbasal as kodecabang,
					g.nama AS namagudang,
					b.kodemerk,
					m.nama AS namamerk,
					0 AS qtyawalsblm,
					0 AS qtymasuksblm,
					CASE WHEN pb.kodestatus = 2 THEN pbd.qty ELSE 0 END AS qtykeluarsblm,
					CASE WHEN pb.kodestatus <> 2 THEN pbd.qty ELSE 0 END AS qtypicksblm,
					0 as qtyawal,
					0 as qtymasuk,
					0 as qtykeluar,
					0 as qtypick,
					b.kodesatuan AS satuan,
					bd.hargajual,
					bd.komisi
				FROM
					srvt_transferstock pb
					LEFT JOIN srvt_transferstockdetail pbd on pb.nomor = pbd.nomor
					LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
					LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
					LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecbasal
					LEFT JOIN glbm_gudang g ON g.kode = pb.kodegdasal
				WHERE
					pb.batal = false
					AND to_char(pb.tanggal, 'YYYYMMDD') < '" . date('Ymd', strtotime($tglawal)) . "'
					AND b.kodemerk >= '" . $merkawal . "'
					AND b.kodemerk <= '" . $merkakhir . "'
					AND pb.kodecbasal = '" . $cabang . "'
					AND pb.kodegdasal = '" . $gudang . "'
				UNION ALL
				SELECT
					pbd.kodebarang,
					b.nama as namabarang,
					pb.kodecbtujuan as kodecabang,
					g.nama AS namagudang,
					b.kodemerk,
					m.nama AS namamerk,
					0 AS qtyawalsblm,
					pbd.qty AS qtymasuksblm,
					0 AS qtykeluarsblm,
					0 AS qtypicksblm,
					0 as qtyawal,
					0 as qtymasuk,
					0 as qtykeluar,
					0 as qtypick,
					b.kodesatuan AS satuan,
					bd.hargajual,
					bd.komisi
				FROM
					srvt_penerimaantransferstock pb
					LEFT JOIN srvt_penerimaantransferstockdetail pbd on pb.nomor = pbd.nomor
					LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
					LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
					LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecbtujuan
					LEFT JOIN glbm_gudang g ON g.kode = pb.kodegdtujuan
				WHERE
					pb.batal = false
					AND to_char(pb.tanggal, 'YYYYMMDD') < '" . date('Ymd', strtotime($tglawal)) . "'
					AND b.kodemerk >= '" . $merkawal . "'
					AND b.kodemerk <= '" . $merkakhir . "'
					AND pb.kodecbtujuan = '" . $cabang . "'
					AND pb.kodegdtujuan = '" . $gudang . "'
			UNION ALL
			SELECT
				pbd.kodebarang,
				b.nama as namabarang,
				pb.kodecabang,
				g.nama AS namagudang,
				b.kodemerk,
				m.nama AS namamerk,
				0 as qtyawalsblm,
				0 as qtymasuksblm,
				0 as qtykeluarsblm,
				0 as qtypicksblm,
				0 AS qtyawal,
				pbd.qty AS qtymasuk,
				0 AS qtykeluar,
				0 AS qtypick,
				b.kodesatuan AS satuan,
				bd.hargajual,
				bd.komisi
			FROM
				srvt_penerimaanbarang pb
				LEFT JOIN srvt_penerimaanbarangdetail pbd on pb.nomor = pbd.nomor
				LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
				LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
				LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecabang
				LEFT JOIN glbm_gudang g ON g.kode = pb.kodegudang
			WHERE
				pb.batal = false
				AND to_char(pb.tanggal, 'YYYYMMDD') >= '" . date('Ymd', strtotime($tglawal)) . "'
				AND to_char(pb.tanggal, 'YYYYMMDD') <= '" . date('Ymd', strtotime($tglakhir)) . "'
				AND b.kodemerk >= '" . $merkawal . "'
				AND b.kodemerk <= '" . $merkakhir . "'
				AND pb.kodecabang = '" . $cabang . "'
				AND pb.kodegudang = '" . $gudang . "'
			UNION ALL
			SELECT
				pbd.kodebarang,
				b.nama as namabarang,
				pb.kodecabang,
				g.nama AS namagudang,
				b.kodemerk,
				m.nama AS namamerk,
				0 as qtyawalsblm,
				0 as qtymasuksblm,
				0 as qtykeluarsblm,
				0 as qtypicksblm,
				0 AS qtyawal,
				pbd.qty AS qtymasuk,
				0 AS qtykeluar,
				0 AS qtypick,
				b.kodesatuan AS satuan,
				bd.hargajual,
				bd.komisi
			FROM
				srvt_penerimaanbarang pb
				LEFT JOIN srvt_penerimaanbarangdetail pbd on pb.nomor = pbd.nomor
				LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
				LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
				LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecabang
				LEFT JOIN glbm_gudang g ON g.kode = pb.kodegudang
			WHERE
				pb.batal = true
				AND to_char(pb.tglbatal, 'YYYYMMDD') >= '" . date('Ymd', strtotime($tglawal)) . "'
				AND to_char(pb.tglbatal, 'YYYYMMDD') <= '" . date('Ymd', strtotime($tglakhir)) . "'
				AND b.kodemerk >= '" . $merkawal . "'
				AND b.kodemerk <= '" . $merkakhir . "'
				AND pb.kodecabang = '" . $cabang . "'
				AND pb.kodegudang = '" . $gudang . "'
			UNION ALL
			SELECT
				pbd.kodebarang,
				b.nama as namabarang,
				pb.kodecabang,
				g.nama AS namagudang,
				b.kodemerk,
				m.nama AS namamerk,
				0 as qtyawalsblm,
				0 as qtymasuksblm,
				0 as qtykeluarsblm,
				0 as qtypicksblm,
				0 AS qtyawal,
				pbd.qty AS qtymasuk,
				0 AS qtykeluar,
				0 AS qtypick,
				b.kodesatuan AS satuan,
				bd.hargajual,
				bd.komisi
			FROM
				srvt_returbarang pb
				LEFT JOIN srvt_returbarangdetail pbd on pb.nomor = pbd.nomor
				LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
				LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
				LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecabang
				LEFT JOIN glbm_gudang g ON g.kode = pb.lokasigudang
			WHERE
				to_char(pb.tanggal, 'YYYYMMDD') >= '" . date('Ymd', strtotime($tglawal)) . "'
				AND to_char(pb.tanggal, 'YYYYMMDD') <= '" . date('Ymd', strtotime($tglakhir)) . "'
				AND b.kodemerk >= '" . $merkawal . "'
				AND b.kodemerk <= '" . $merkakhir . "'
				AND pb.kodecabang = '" . $cabang . "'
				AND pb.lokasigudang = '" . $gudang . "'
			UNION ALL
			SELECT
				pbd.kodebarang,
				b.nama as namabarang,
				pb.kodecabang,
				g.nama AS namagudang,
				b.kodemerk,
				m.nama AS namamerk,
				0 as qtyawalsblm,
				0 as qtymasuksblm,
				0 as qtykeluarsblm,
				0 as qtypicksblm,
				0 AS qtyawal,
				0 AS qtymasuk,
				pbd.qty AS qtykeluar,
				0 AS qtypick,
				b.kodesatuan AS satuan,
				bd.hargajual,
				bd.komisi
			FROM
				srvt_penerimaanbarang pb
				LEFT JOIN srvt_penerimaanbarangdetail pbd on pb.nomor = pbd.nomor
				LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
				LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
				LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecabang
				LEFT JOIN glbm_gudang g ON g.kode = pb.kodegudang
			WHERE
				pb.batal = true
				AND to_char(pb.tglbatal, 'YYYYMMDD') >= '" . date('Ymd', strtotime($tglawal)) . "'
				AND to_char(pb.tglbatal, 'YYYYMMDD') <= '" . date('Ymd', strtotime($tglakhir)) . "'
				AND b.kodemerk >= '" . $merkawal . "'
				AND b.kodemerk <= '" . $merkakhir . "'
				AND pb.kodecabang = '" . $cabang . "'
				AND pb.kodegudang = '" . $gudang . "'
			UNION ALL
			SELECT
				pbd.kodebarang,
				b.nama as namabarang,
				pb.kodecabang,
				g.nama AS namagudang,
				b.kodemerk,
				m.nama AS namamerk,
				0 as qtyawalsblm,
				0 as qtymasuksblm,
				0 as qtykeluarsblm,
				0 as qtypicksblm,
				0 AS qtyawal,
				0 AS qtymasuk,
				0 AS qtykeluar,
				CASE
					WHEN pb.kodestatus <> 2 THEN pbd.qty
					ELSE 0
				END AS qtypick,
				b.kodesatuan AS satuan,
				bd.hargajual,
				bd.komisi
			FROM
				srvt_salesorder pb
				LEFT JOIN srvt_sodetail pbd on pb.nomor = pbd.nomor
				LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
				LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
				LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecabang
				LEFT JOIN glbm_gudang g ON g.kode = pb.lokasi
			WHERE
				pb.batal = false
				AND to_char(pb.tanggal, 'YYYYMMDD') >= '" . date('Ymd', strtotime($tglawal)) . "'
				AND to_char(pb.tanggal, 'YYYYMMDD') <= '" . date('Ymd', strtotime($tglakhir)) . "'
				AND b.kodemerk >= '" . $merkawal . "'
				AND b.kodemerk <= '" . $merkakhir . "'
				AND pb.kodecabang = '" . $cabang . "'
				AND pb.lokasi = '" . $gudang . "'
			UNION ALL
			SELECT
				pbd.kodebarang,
				b.nama as namabarang,
				pb.kodecabang,
				g.nama AS namagudang,
				b.kodemerk,
				m.nama AS namamerk,
				0 AS qtyawalsblm,
				0 AS qtymasuksblm,
				0 AS qtykeluarsblm,
				0 AS qtypicksblm,
				0 as qtyawal,
				0 as qtymasuk,
				pbd.qty as qtykeluar,
				0 as qtypick,
				b.kodesatuan AS satuan,
				bd.hargajual,
				bd.komisi
			FROM
				srvt_invoice pb
				LEFT JOIN srvt_invoicedetail pbd ON pb.nomor = pbd.nomor
				LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
				LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
				LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecabang
				LEFT JOIN glbm_gudang g ON g.kode = pb.lokasi
			WHERE
				pb.batal = false
				AND to_char(pb.tanggal, 'YYYYMMDD') >= '" . date('Ymd', strtotime($tglawal)) . "'
				AND to_char(pb.tanggal, 'YYYYMMDD') <= '" . date('Ymd', strtotime($tglakhir)) . "'
				AND b.kodemerk >= '" . $merkawal . "'
				AND b.kodemerk <= '" . $merkakhir . "'
				AND pb.kodecabang = '" . $cabang . "'
				AND pb.lokasi = '" . $gudang . "'
			UNION ALL
			SELECT
				pbd.kodebarang,
				b.nama as namabarang,
				pb.kodecabang,
				g.nama AS namagudang,
				b.kodemerk,
				m.nama AS namamerk,
				0 as qtyawalsblm,
				0 as qtymasuksblm,
				0 as qtykeluarsblm,
				0 as qtypicksblm,
				0 AS qtyawal,
				CASE
					WHEN pb.jenis = '2' THEN pbd.qty
					ELSE 0
				END AS qtymasuk,
				CASE
					WHEN pb.jenis <> '2' THEN pbd.qty
					ELSE 0
				END AS qtykeluar,
				0 AS qtypick,
				b.kodesatuan AS satuan,
				bd.hargajual,
				bd.komisi
			FROM
				srvt_bag pb
				LEFT JOIN srvt_bagdetail pbd on pb.nomor = pbd.nomor
				LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
				LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
				LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecabang
				LEFT JOIN glbm_gudang g ON g.kode = pb.lokasi
			WHERE
				pb.batal = false
				AND approve = true
				AND to_char(pb.tanggal, 'YYYYMMDD') >= '" . date('Ymd', strtotime($tglawal)) . "'
				AND to_char(pb.tanggal, 'YYYYMMDD') <= '" . date('Ymd', strtotime($tglakhir)) . "'
				AND b.kodemerk >= '" . $merkawal . "'
				AND b.kodemerk <= '" . $merkakhir . "'
				AND pb.kodecabang = '" . $cabang . "'
				AND pb.lokasi = '" . $gudang . "'
			UNION ALL
			SELECT
				pbd.kodebarang,
				b.nama as namabarang,
				pb.kodecbasal as kodecabang,
				g.nama AS namagudang,
				b.kodemerk,
				m.nama AS namamerk,
				0 as qtyawalsblm,
				0 as qtymasuksblm,
				0 as qtykeluarsblm,
				0 as qtypicksblm,
				0 AS qtyawal,
				0 AS qtymasuk,
				CASE WHEN pb.kodestatus = 2 THEN pbd.qty ELSE 0 END AS qtykeluar,
            	CASE WHEN pb.kodestatus <> 2 THEN pbd.qty ELSE 0 END AS qtypick,
				b.kodesatuan AS satuan,
				bd.hargajual,
				bd.komisi
			FROM
				srvt_transferstock pb
				LEFT JOIN srvt_transferstockdetail pbd on pb.nomor = pbd.nomor
				LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
				LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
				LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecbasal
				LEFT JOIN glbm_gudang g ON g.kode = pb.kodegdasal
			WHERE
				pb.batal = false
				AND to_char(pb.tanggal, 'YYYYMMDD') >= '" . date('Ymd', strtotime($tglawal)) . "'
				AND to_char(pb.tanggal, 'YYYYMMDD') <= '" . date('Ymd', strtotime($tglakhir)) . "'
				AND b.kodemerk >= '" . $merkawal . "'
				AND b.kodemerk <= '" . $merkakhir . "'
				AND pb.kodecbasal = '" . $cabang . "'
				AND pb.kodegdasal = '" . $gudang . "'
			UNION ALL
			SELECT
				pbd.kodebarang,
				b.nama as namabarang,
				pb.kodecbasal as kodecabang,
				g.nama AS namagudang,
				b.kodemerk,
				m.nama AS namamerk,
				0 as qtyawalsblm,
				0 as qtymasuksblm,
				0 as qtykeluarsblm,
				0 as qtypicksblm,
				0 AS qtyawal,
				0 AS qtymasuk,
				pbd.qty AS qtykeluar,
				0 AS qtypick,
				b.kodesatuan AS satuan,
				bd.hargajual,
				bd.komisi
			FROM
				srvt_penerimaantransferstock pb
				LEFT JOIN srvt_penerimaantransferstockdetail pbd on pb.nomor = pbd.nomor
				LEFT JOIN srvt_suratjalangudang sjgd on sjgd.nomor = pb.notransfer
				LEFT JOIN srvt_transferstock tf on tf.nomor = sjgd.noproses
				LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
				LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
				LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecbasal
				LEFT JOIN glbm_gudang g ON g.kode = pb.kodegdasal
			WHERE
				pb.batal = false
				AND to_char(tf.tanggal, 'YYYYMMDD') >= '" . date('Ymd', strtotime($tglawal)) . "'
				AND to_char(tf.tanggal, 'YYYYMMDD') <= '" . date('Ymd', strtotime($tglakhir)) . "'
				AND b.kodemerk >= '" . $merkawal . "'
				AND b.kodemerk <= '" . $merkakhir . "'
				AND pb.kodecbasal = '" . $cabang . "'
				AND pb.kodegdasal = '" . $gudang . "'
			UNION ALL
			SELECT
				pbd.kodebarang,
				b.nama as namabarang,
				pb.kodecbtujuan as kodecabang,
				g.nama AS namagudang,
				b.kodemerk,
				m.nama AS namamerk,
				0 as qtyawalsblm,
				0 as qtymasuksblm,
				0 as qtykeluarsblm,
				0 as qtypicksblm,
				0 AS qtyawal,
				pbd.qty AS qtymasuk,
				0 AS qtykeluar,
				0 AS qtypick,
				b.kodesatuan AS satuan,
				bd.hargajual,
				bd.komisi
			FROM
				srvt_penerimaantransferstock pb
				LEFT JOIN srvt_penerimaantransferstockdetail pbd on pb.nomor = pbd.nomor
				LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
				LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
				LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecbtujuan
				LEFT JOIN glbm_gudang g ON g.kode = pb.kodegdtujuan
			WHERE
				pb.batal = false
				AND to_char(pb.tanggal, 'YYYYMMDD') >= '" . date('Ymd', strtotime($tglawal)) . "'
				AND to_char(pb.tanggal, 'YYYYMMDD') <= '" . date('Ymd', strtotime($tglakhir)) . "'
				AND b.kodemerk >= '" . $merkawal . "'
				AND b.kodemerk <= '" . $merkakhir . "'
				AND pb.kodecbtujuan = '" . $cabang . "'
				AND pb.kodegdtujuan = '" . $gudang . "'
		) hasil_data
	GROUP BY
		hasil_data.kodemerk,
		hasil_data.namamerk
	ORDER BY
		namamerk ASC")->result();
	}

	function GetDataBarangKeluar($tglawal = "", $tglakhir = "", $gudang = "")
	{
		$this->db->where("tanggal >=", date('Y-m-d', strtotime($tglawal)));
		$this->db->where("tanggal <=", date('Y-m-d', strtotime($tglakhir)));
		$this->db->where("gudangasal", $gudang);
		$this->db->order_by("kodebarang", "ASC");

		return $this->db->get("rpt_barangkeluar")->result();
	}

	function GetDataBarangKeluarRow($tglawal = "", $tglakhir = "", $gudang = "")
	{
		$this->db->where("tanggal >=", date('Y-m-d', strtotime($tglawal)));
		$this->db->where("tanggal <=", date('Y-m-d', strtotime($tglakhir)));
		$this->db->where("gudangasal", $gudang);

		return $this->db->get("rpt_barangkeluarrow")->result();
	}

	function GetDataBarangKeluarList($tglawal = "", $tglakhir = "", $gudang = "")
	{
		$this->db->select("nomor");
		$this->db->where("tanggal >=", date('Y-m-d', strtotime($tglawal)));
		$this->db->where("tanggal <=", date('Y-m-d', strtotime($tglakhir)));
		$this->db->where("gudangasal", $gudang);
		$this->db->group_by("nomor");

		return $this->db->get("rpt_barangkeluar")->result();
	}

	function GetDataBarangKeluarSemuaRow($tglawal = "", $tglakhir = "", $cabang = "")
	{
		$this->db->where("tanggal >=", date('Y-m-d', strtotime($tglawal)));
		$this->db->where("tanggal <=", date('Y-m-d', strtotime($tglakhir)));
		$this->db->where("cabangasal", $cabang);

		return $this->db->get("rpt_barangkeluarrow")->result();
	}

	function GetDataBarangKeluarSemuaGudang($tglawal = "", $tglakhir = "", $cabang = "")
	{
		$this->db->where("tanggal >=", date('Y-m-d', strtotime($tglawal)));
		$this->db->where("tanggal <=", date('Y-m-d', strtotime($tglakhir)));
		$this->db->where("cabangasal", $cabang);
		$this->db->order_by("kodebarang", "ASC");

		return $this->db->get("rpt_barangkeluar")->result();
	}

	function GetDataBarangKeluarSemuaGudangList($tglawal = "", $tglakhir = "", $cabang = "")
	{
		$this->db->select("nomor");
		$this->db->where("tanggal >=", date('Y-m-d', strtotime($tglawal)));
		$this->db->where("tanggal <=", date('Y-m-d', strtotime($tglakhir)));
		$this->db->where("cabangasal", $cabang);
		$this->db->group_by("nomor");

		return $this->db->get("rpt_barangkeluar")->result();
	}

	function LaporanKartuStok($tglawal = "", $tglakhir = "", $barang = "", $gudang = "")
	{
		$this->db->where("tanggal >=", date('Y-m-d', strtotime($tglawal)));
		$this->db->where("tanggal <=", date('Y-m-d', strtotime($tglakhir)));
		$this->db->where("kodebarang", $barang);
		$this->db->where("kodegudang", $gudang);

		return $this->db->get("rpt_kartustokunioin")->result();
	}

	function LaporanKartuStokPajak($tglawal = "", $tglakhir = "", $barang = "", $gudang = "")
	{
		$this->db->where("tanggal >=", date('Y-m-d', strtotime($tglawal)));
		$this->db->where("tanggal <=", date('Y-m-d', strtotime($tglakhir)));
		$this->db->where("kodebarang", $barang);
		$this->db->where("kodegudang", $gudang);

		return $this->db->get("rpt_kartustokunionpajak")->result();
	}

	function GetDataKartuStok($tglawal = "", $tglakhir = "", $barang = "", $gudang = "")
	{
		return $this->db->query("SELECT
		hasil_data.kodebarang,
		hasil_data.namabarang,
		hasil_data.kodecabang,
		hasil_data.namagudang,
		hasil_data.kodemerk,
		hasil_data.namamerk,
		sum(hasil_data.qtyawalsblm) as qtyawal,
		sum(hasil_data.qtymasuk) as qtymasuk,
		sum(hasil_data.qtykeluar) as qtykeluar,
		sum(hasil_data.qtypick) as qtypick,
		(
		sum(hasil_data.qtyawalsblm) + SUM(hasil_data.qtymasuksblm) - SUM(hasil_data.qtykeluarsblm) - SUM(hasil_data.qtypicksblm) + sum(hasil_data.qtymasuk)
		) - (
		ABS(
		SUM(hasil_data.qtykeluar) + SUM(hasil_data.qtypick)
		)
		) AS stok,
		hasil_data.satuan,
		hasil_data.hargajual,
		hasil_data.komisi
	FROM(
				SELECT
					i.kodebarang,
					b.nama as namabarang,
					i.kodecabang,
					g.nama AS namagudang,
					b.kodemerk,
					m.nama AS namamerk,
					i.qtyawal as qtyawalsblm,
					0 AS qtymasuksblm,
					0 AS qtykeluarsblm,
					0 AS qtypicksblm,
					0 as qtyawal,
					0 as qtymasuk,
					0 as qtykeluar,
					0 as qtypick,
					b.kodesatuan AS satuan,
					bd.hargajual,
					bd.komisi
				FROM
					srvt_inventorystok i
					LEFT JOIN glbm_barang b ON b.kode = i.kodebarang
					LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
					LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = i.kodecabang
					LEFT JOIN glbm_gudang g ON g.kode = i.kodegudang
				WHERE
					periode = '202208'
					AND i.kodebarang = '" . $barang . "'
					AND i.kodegudang = '" . $gudang . "'
				UNION ALL
				SELECT
					pbd.kodebarang,
					b.nama as namabarang,
					pb.kodecabang,
					g.nama AS namagudang,
					b.kodemerk,
					m.nama AS namamerk,
					0 AS qtyawalsblm,
					pbd.qty AS qtymasuksblm,
					0 AS qtykeluarsblm,
					0 AS qtypicksblm,
					0 as qtyawal,
					0 as qtymasuk,
					0 as qtykeluar,
					0 as qtypick,
					b.kodesatuan AS satuan,
					bd.hargajual,
					bd.komisi
				FROM
					srvt_penerimaanbarang pb
					LEFT JOIN srvt_penerimaanbarangdetail pbd on pb.nomor = pbd.nomor
					LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
					LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
					LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecabang
					LEFT JOIN glbm_gudang g ON g.kode = pb.kodegudang
				WHERE
					pb.batal = false
					AND to_char(pb.tanggal, 'YYYYMMDD') < '" . date('Ymd', strtotime($tglawal)) . "'
					AND pbd.kodebarang = '" . $barang . "'
					AND pb.kodegudang = '" . $gudang . "'
				UNION ALL
				SELECT
					pbd.kodebarang,
					b.nama as namabarang,
					pb.kodecabang,
					g.nama AS namagudang,
					b.kodemerk,
					m.nama AS namamerk,
					0 AS qtyawalsblm,
					pbd.qty AS qtymasuksblm,
					0 AS qtykeluarsblm,
					0 AS qtypicksblm,
					0 as qtyawal,
					0 as qtymasuk,
					0 as qtykeluar,
					0 as qtypick,
					b.kodesatuan AS satuan,
					bd.hargajual,
					bd.komisi
				FROM
					srvt_penerimaanbarang pb
					LEFT JOIN srvt_penerimaanbarangdetail pbd on pb.nomor = pbd.nomor
					LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
					LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
					LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecabang
					LEFT JOIN glbm_gudang g ON g.kode = pb.kodegudang
				WHERE
					pb.batal = true
					AND to_char(pb.tglbatal, 'YYYYMMDD') < '" . date('Ymd', strtotime($tglawal)) . "'
					AND pbd.kodebarang = '" . $barang . "'
					AND pb.kodegudang = '" . $gudang . "'
				UNION ALL
				SELECT
					pbd.kodebarang,
					b.nama as namabarang,
					pb.kodecabang,
					g.nama AS namagudang,
					b.kodemerk,
					m.nama AS namamerk,
					0 AS qtyawalsblm,
					pbd.qty AS qtymasuksblm,
					0 AS qtykeluarsblm,
					0 AS qtypicksblm,
					0 as qtyawal,
					0 as qtymasuk,
					0 as qtykeluar,
					0 as qtypick,
					b.kodesatuan AS satuan,
					bd.hargajual,
					bd.komisi
				FROM
					srvt_returbarang pb
					LEFT JOIN srvt_returbarangdetail pbd on pb.nomor = pbd.nomor
					LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
					LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
					LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecabang
					LEFT JOIN glbm_gudang g ON g.kode = pb.lokasigudang
				WHERE
					to_char(pb.tanggal, 'YYYYMMDD') < '" . date('Ymd', strtotime($tglawal)) . "'
					AND pbd.kodebarang = '" . $barang . "'
					AND pb.lokasigudang = '" . $gudang . "'
				UNION ALL
				SELECT
					pbd.kodebarang,
					b.nama as namabarang,
					pb.kodecabang,
					g.nama AS namagudang,
					b.kodemerk,
					m.nama AS namamerk,
					0 AS qtyawalsblm,
					0 AS qtymasuksblm,
					pbd.qty AS qtykeluarsblm,
					0 AS qtypicksblm,
					0 as qtyawal,
					0 as qtymasuk,
					0 as qtykeluar,
					0 as qtypick,
					b.kodesatuan AS satuan,
					bd.hargajual,
					bd.komisi
				FROM
					srvt_penerimaanbarang pb
					LEFT JOIN srvt_penerimaanbarangdetail pbd on pb.nomor = pbd.nomor
					LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
					LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
					LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecabang
					LEFT JOIN glbm_gudang g ON g.kode = pb.kodegudang
				WHERE
					pb.batal = true
					AND to_char(pb.tglbatal, 'YYYYMMDD') < '" . date('Ymd', strtotime($tglawal)) . "'
					AND pbd.kodebarang = '" . $barang . "'
					AND pb.kodegudang = '" . $gudang . "'
				UNION ALL
				SELECT
					pbd.kodebarang,
					b.nama as namabarang,
					pb.kodecabang,
					g.nama AS namagudang,
					b.kodemerk,
					m.nama AS namamerk,
					0 AS qtyawalsblm,
					0 AS qtymasuksblm,
					0 AS qtykeluarsblm,
					CASE
						WHEN pb.kodestatus <> 2 THEN pbd.qty
						ELSE 0
					END AS qtypicksblm,
					0 as qtyawal,
					0 as qtymasuk,
					0 as qtykeluar,
					0 as qtypick,
					b.kodesatuan AS satuan,
					bd.hargajual,
					bd.komisi
				FROM
					srvt_salesorder pb
					LEFT JOIN srvt_sodetail pbd on pb.nomor = pbd.nomor
					LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
					LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
					LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecabang
					LEFT JOIN glbm_gudang g ON g.kode = pb.lokasi
				WHERE
					pb.batal = false
					AND to_char(pb.tanggal, 'YYYYMMDD') < '" . date('Ymd', strtotime($tglawal)) . "'
					AND pbd.kodebarang = '" . $barang . "'
					AND pb.lokasi = '" . $gudang . "'
				UNION ALL
				SELECT
					pbd.kodebarang,
					b.nama as namabarang,
					pb.kodecabang,
					g.nama AS namagudang,
					b.kodemerk,
					m.nama AS namamerk,
					0 AS qtyawalsblm,
					0 AS qtymasuksblm,
					pbd.qty AS qtykeluarsblm,
					0 AS qtypicksblm,
					0 as qtyawal,
					0 as qtymasuk,
					0 as qtykeluar,
					0 as qtypick,
					b.kodesatuan AS satuan,
					bd.hargajual,
					bd.komisi
				FROM
					srvt_invoice pb
					LEFT JOIN srvt_invoicedetail pbd ON pb.nomor = pbd.nomor
					LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
					LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
					LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecabang
					LEFT JOIN glbm_gudang g ON g.kode = pb.lokasi
				WHERE
					pb.batal = false
					AND to_char(pb.tanggal, 'YYYYMMDD') < '" . date('Ymd', strtotime($tglawal)) . "'
					AND pbd.kodebarang = '" . $barang . "'
					AND pb.lokasi = '" . $gudang . "'
				UNION ALL
				SELECT
					pbd.kodebarang,
					b.nama as namabarang,
					pb.kodecabang,
					g.nama AS namagudang,
					b.kodemerk,
					m.nama AS namamerk,
					0 AS qtyawalsblm,
					CASE
						WHEN pb.jenis = '2' THEN pbd.qty
						ELSE 0
					END AS qtymasuksblm,
					CASE
						WHEN pb.jenis <> '2' THEN pbd.qty
						ELSE 0
					END AS qtykeluarsblm,
					0 AS qtypicksblm,
					0 as qtyawal,
					0 as qtymasuk,
					0 as qtykeluar,
					0 as qtypick,
					b.kodesatuan AS satuan,
					bd.hargajual,
					bd.komisi
				FROM
					srvt_bag pb
					LEFT JOIN srvt_bagdetail pbd on pb.nomor = pbd.nomor
					LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
					LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
					LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecabang
					LEFT JOIN glbm_gudang g ON g.kode = pb.lokasi
				WHERE
					pb.batal = false
					AND approve = true
					AND to_char(pb.tanggal, 'YYYYMMDD') < '" . date('Ymd', strtotime($tglawal)) . "'
					AND pbd.kodebarang = '" . $barang . "'
					AND pb.lokasi = '" . $gudang . "'
				UNION ALL
				SELECT
					pbd.kodebarang,
					b.nama as namabarang,
					pb.kodecbasal as kodecabang,
					g.nama AS namagudang,
					b.kodemerk,
					m.nama AS namamerk,
					0 AS qtyawalsblm,
					0 AS qtymasuksblm,
					0 AS qtykeluarsblm,
					pbd.qty AS qtypicksblm,
					0 as qtyawal,
					0 as qtymasuk,
					0 as qtykeluar,
					0 as qtypick,
					b.kodesatuan AS satuan,
					bd.hargajual,
					bd.komisi
				FROM
					srvt_transferstock pb
					LEFT JOIN srvt_transferstockdetail pbd on pb.nomor = pbd.nomor
					LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
					LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
					LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecbasal
					LEFT JOIN glbm_gudang g ON g.kode = pb.kodegdasal
				WHERE
					pb.batal = false
					AND pb.status = false
					AND to_char(pb.tanggal, 'YYYYMMDD') < '" . date('Ymd', strtotime($tglawal)) . "'
					AND pbd.kodebarang = '" . $barang . "'
					AND pb.kodegdasal = '" . $gudang . "'
				UNION ALL
				SELECT
					pbd.kodebarang,
					b.nama as namabarang,
					pb.kodecbasal as kodecabang,
					g.nama AS namagudang,
					b.kodemerk,
					m.nama AS namamerk,
					0 AS qtyawalsblm,
					0 AS qtymasuksblm,
					pbd.qty AS qtykeluarsblm,
					0 AS qtypicksblm,
					0 as qtyawal,
					0 as qtymasuk,
					0 as qtykeluar,
					0 as qtypick,
					b.kodesatuan AS satuan,
					bd.hargajual,
					bd.komisi
				FROM
					srvt_penerimaantransferstock pb
					LEFT JOIN srvt_penerimaantransferstockdetail pbd on pb.nomor = pbd.nomor
					LEFT JOIN srvt_suratjalangudang sjgd on sjgd.nomor = pb.notransfer
					LEFT JOIN srvt_transferstock tf on tf.nomor = sjgd.noproses
					LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
					LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
					LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecbasal
					LEFT JOIN glbm_gudang g ON g.kode = pb.kodegdasal
				WHERE
					pb.batal = false
					AND to_char(tf.tanggal, 'YYYYMMDD') < '" . date('Ymd', strtotime($tglawal)) . "'
					AND pbd.kodebarang = '" . $barang . "'
					AND pb.kodegdasal = '" . $gudang . "'
				UNION ALL
				SELECT
					pbd.kodebarang,
					b.nama as namabarang,
					pb.kodecbtujuan as kodecabang,
					g.nama AS namagudang,
					b.kodemerk,
					m.nama AS namamerk,
					0 AS qtyawalsblm,
					pbd.qty AS qtymasuksblm,
					0 AS qtykeluarsblm,
					0 AS qtypicksblm,
					0 as qtyawal,
					0 as qtymasuk,
					0 as qtykeluar,
					0 as qtypick,
					b.kodesatuan AS satuan,
					bd.hargajual,
					bd.komisi
				FROM
					srvt_penerimaantransferstock pb
					LEFT JOIN srvt_penerimaantransferstockdetail pbd on pb.nomor = pbd.nomor
					LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
					LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
					LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecbtujuan
					LEFT JOIN glbm_gudang g ON g.kode = pb.kodegdtujuan
				WHERE
					pb.batal = false
					AND to_char(pb.tanggal, 'YYYYMMDD') < '" . date('Ymd', strtotime($tglawal)) . "'
					AND pbd.kodebarang = '" . $barang . "'
					AND pb.kodegdtujuan = '" . $gudang . "'
			UNION ALL
			SELECT
				pbd.kodebarang,
				b.nama as namabarang,
				pb.kodecabang,
				g.nama AS namagudang,
				b.kodemerk,
				m.nama AS namamerk,
				0 as qtyawalsblm,
				0 as qtymasuksblm,
				0 as qtykeluarsblm,
				0 as qtypicksblm,
				0 AS qtyawal,
				pbd.qty AS qtymasuk,
				0 AS qtykeluar,
				0 AS qtypick,
				b.kodesatuan AS satuan,
				bd.hargajual,
				bd.komisi
			FROM
				srvt_penerimaanbarang pb
				LEFT JOIN srvt_penerimaanbarangdetail pbd on pb.nomor = pbd.nomor
				LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
				LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
				LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecabang
				LEFT JOIN glbm_gudang g ON g.kode = pb.kodegudang
			WHERE
				pb.batal = false
				AND to_char(pb.tanggal, 'YYYYMMDD') >= '" . date('Ymd', strtotime($tglawal)) . "'
				AND to_char(pb.tanggal, 'YYYYMMDD') <= '" . date('Ymd', strtotime($tglakhir)) . "'
				AND pbd.kodebarang = '" . $barang . "'
				AND pb.kodegudang = '" . $gudang . "'
			UNION ALL
			SELECT
				pbd.kodebarang,
				b.nama as namabarang,
				pb.kodecabang,
				g.nama AS namagudang,
				b.kodemerk,
				m.nama AS namamerk,
				0 as qtyawalsblm,
				0 as qtymasuksblm,
				0 as qtykeluarsblm,
				0 as qtypicksblm,
				0 AS qtyawal,
				pbd.qty AS qtymasuk,
				0 AS qtykeluar,
				0 AS qtypick,
				b.kodesatuan AS satuan,
				bd.hargajual,
				bd.komisi
			FROM
				srvt_penerimaanbarang pb
				LEFT JOIN srvt_penerimaanbarangdetail pbd on pb.nomor = pbd.nomor
				LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
				LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
				LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecabang
				LEFT JOIN glbm_gudang g ON g.kode = pb.kodegudang
			WHERE
				pb.batal = true
				AND to_char(pb.tglbatal, 'YYYYMMDD') >= '" . date('Ymd', strtotime($tglawal)) . "'
				AND to_char(pb.tglbatal, 'YYYYMMDD') <= '" . date('Ymd', strtotime($tglakhir)) . "'
				AND pbd.kodebarang = '" . $barang . "'
				AND pb.kodegudang = '" . $gudang . "'
			UNION ALL
			SELECT
				pbd.kodebarang,
				b.nama as namabarang,
				pb.kodecabang,
				g.nama AS namagudang,
				b.kodemerk,
				m.nama AS namamerk,
				0 as qtyawalsblm,
				0 as qtymasuksblm,
				0 as qtykeluarsblm,
				0 as qtypicksblm,
				0 AS qtyawal,
				pbd.qty AS qtymasuk,
				0 AS qtykeluar,
				0 AS qtypick,
				b.kodesatuan AS satuan,
				bd.hargajual,
				bd.komisi
			FROM
				srvt_returbarang pb
				LEFT JOIN srvt_returbarangdetail pbd on pb.nomor = pbd.nomor
				LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
				LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
				LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecabang
				LEFT JOIN glbm_gudang g ON g.kode = pb.lokasigudang
			WHERE
				to_char(pb.tanggal, 'YYYYMMDD') >= '" . date('Ymd', strtotime($tglawal)) . "'
				AND to_char(pb.tanggal, 'YYYYMMDD') <= '" . date('Ymd', strtotime($tglakhir)) . "'
				AND pbd.kodebarang = '" . $barang . "'
				AND pb.lokasigudang = '" . $gudang . "'
			UNION ALL
			SELECT
				pbd.kodebarang,
				b.nama as namabarang,
				pb.kodecabang,
				g.nama AS namagudang,
				b.kodemerk,
				m.nama AS namamerk,
				0 as qtyawalsblm,
				0 as qtymasuksblm,
				0 as qtykeluarsblm,
				0 as qtypicksblm,
				0 AS qtyawal,
				0 AS qtymasuk,
				pbd.qty AS qtykeluar,
				0 AS qtypick,
				b.kodesatuan AS satuan,
				bd.hargajual,
				bd.komisi
			FROM
				srvt_penerimaanbarang pb
				LEFT JOIN srvt_penerimaanbarangdetail pbd on pb.nomor = pbd.nomor
				LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
				LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
				LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecabang
				LEFT JOIN glbm_gudang g ON g.kode = pb.kodegudang
			WHERE
				pb.batal = true
				AND to_char(pb.tglbatal, 'YYYYMMDD') >= '" . date('Ymd', strtotime($tglawal)) . "'
				AND to_char(pb.tglbatal, 'YYYYMMDD') <= '" . date('Ymd', strtotime($tglakhir)) . "'
				AND pbd.kodebarang = '" . $barang . "'
				AND pb.kodegudang = '" . $gudang . "'
			UNION ALL
			SELECT
				pbd.kodebarang,
				b.nama as namabarang,
				pb.kodecabang,
				g.nama AS namagudang,
				b.kodemerk,
				m.nama AS namamerk,
				0 as qtyawalsblm,
				0 as qtymasuksblm,
				0 as qtykeluarsblm,
				0 as qtypicksblm,
				0 AS qtyawal,
				0 AS qtymasuk,
				0 AS qtykeluar,
				CASE
					WHEN pb.kodestatus <> 2 THEN pbd.qty
					ELSE 0
				END AS qtypick,
				b.kodesatuan AS satuan,
				bd.hargajual,
				bd.komisi
			FROM
				srvt_salesorder pb
				LEFT JOIN srvt_sodetail pbd on pb.nomor = pbd.nomor
				LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
				LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
				LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecabang
				LEFT JOIN glbm_gudang g ON g.kode = pb.lokasi
			WHERE
				pb.batal = false
				AND to_char(pb.tanggal, 'YYYYMMDD') >= '" . date('Ymd', strtotime($tglawal)) . "'
				AND to_char(pb.tanggal, 'YYYYMMDD') <= '" . date('Ymd', strtotime($tglakhir)) . "'
				AND pbd.kodebarang = '" . $barang . "'
				AND pb.lokasi = '" . $gudang . "'
			UNION ALL
			SELECT
				pbd.kodebarang,
				b.nama as namabarang,
				pb.kodecabang,
				g.nama AS namagudang,
				b.kodemerk,
				m.nama AS namamerk,
				0 AS qtyawalsblm,
				0 AS qtymasuksblm,
				0 AS qtykeluarsblm,
				0 AS qtypicksblm,
				0 as qtyawal,
				0 as qtymasuk,
				pbd.qty as qtykeluar,
				0 as qtypick,
				b.kodesatuan AS satuan,
				bd.hargajual,
				bd.komisi
			FROM
				srvt_invoice pb
				LEFT JOIN srvt_invoicedetail pbd ON pb.nomor = pbd.nomor
				LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
				LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
				LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecabang
				LEFT JOIN glbm_gudang g ON g.kode = pb.lokasi
			WHERE
				pb.batal = false
				AND to_char(pb.tanggal, 'YYYYMMDD') >= '" . date('Ymd', strtotime($tglawal)) . "'
				AND to_char(pb.tanggal, 'YYYYMMDD') <= '" . date('Ymd', strtotime($tglakhir)) . "'
				AND pbd.kodebarang = '" . $barang . "'
				AND pb.lokasi = '" . $gudang . "'
			UNION ALL
			SELECT
				pbd.kodebarang,
				b.nama as namabarang,
				pb.kodecabang,
				g.nama AS namagudang,
				b.kodemerk,
				m.nama AS namamerk,
				0 as qtyawalsblm,
				0 as qtymasuksblm,
				0 as qtykeluarsblm,
				0 as qtypicksblm,
				0 AS qtyawal,
				CASE
					WHEN pb.jenis = '2' THEN pbd.qty
					ELSE 0
				END AS qtymasuk,
				CASE
					WHEN pb.jenis <> '2' THEN pbd.qty
					ELSE 0
				END AS qtykeluar,
				0 AS qtypick,
				b.kodesatuan AS satuan,
				bd.hargajual,
				bd.komisi
			FROM
				srvt_bag pb
				LEFT JOIN srvt_bagdetail pbd on pb.nomor = pbd.nomor
				LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
				LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
				LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecabang
				LEFT JOIN glbm_gudang g ON g.kode = pb.lokasi
			WHERE
				pb.batal = false
				AND approve = true
				AND to_char(pb.tanggal, 'YYYYMMDD') >= '" . date('Ymd', strtotime($tglawal)) . "'
				AND to_char(pb.tanggal, 'YYYYMMDD') <= '" . date('Ymd', strtotime($tglakhir)) . "'
				AND pbd.kodebarang = '" . $barang . "'
				AND pb.lokasi = '" . $gudang . "'
			UNION ALL
			SELECT
				pbd.kodebarang,
				b.nama as namabarang,
				pb.kodecbasal as kodecabang,
				g.nama AS namagudang,
				b.kodemerk,
				m.nama AS namamerk,
				0 as qtyawalsblm,
				0 as qtymasuksblm,
				0 as qtykeluarsblm,
				0 as qtypicksblm,
				0 AS qtyawal,
				0 AS qtymasuk,
				0 AS qtykeluar,
				pbd.qty AS qtypick,
				b.kodesatuan AS satuan,
				bd.hargajual,
				bd.komisi
			FROM
				srvt_transferstock pb
				LEFT JOIN srvt_transferstockdetail pbd on pb.nomor = pbd.nomor
				LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
				LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
				LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecbasal
				LEFT JOIN glbm_gudang g ON g.kode = pb.kodegdasal
			WHERE
				pb.batal = false
				AND pb.status = false
				AND to_char(pb.tanggal, 'YYYYMMDD') >= '" . date('Ymd', strtotime($tglawal)) . "'
				AND to_char(pb.tanggal, 'YYYYMMDD') <= '" . date('Ymd', strtotime($tglakhir)) . "'
				AND pbd.kodebarang = '" . $barang . "'
				AND pb.kodegdasal = '" . $gudang . "'
			UNION ALL
			SELECT
				pbd.kodebarang,
				b.nama as namabarang,
				pb.kodecbasal as kodecabang,
				g.nama AS namagudang,
				b.kodemerk,
				m.nama AS namamerk,
				0 as qtyawalsblm,
				0 as qtymasuksblm,
				0 as qtykeluarsblm,
				0 as qtypicksblm,
				0 AS qtyawal,
				0 AS qtymasuk,
				pbd.qty AS qtykeluar,
				0 AS qtypick,
				b.kodesatuan AS satuan,
				bd.hargajual,
				bd.komisi
			FROM
				srvt_penerimaantransferstock pb
				LEFT JOIN srvt_penerimaantransferstockdetail pbd on pb.nomor = pbd.nomor
				LEFT JOIN srvt_suratjalangudang sjgd on sjgd.nomor = pb.notransfer
				LEFT JOIN srvt_transferstock tf on tf.nomor = sjgd.noproses
				LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
				LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
				LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecbasal
				LEFT JOIN glbm_gudang g ON g.kode = pb.kodegdasal
			WHERE
				pb.batal = false
				AND to_char(tf.tanggal, 'YYYYMMDD') >= '" . date('Ymd', strtotime($tglawal)) . "'
				AND to_char(tf.tanggal, 'YYYYMMDD') <= '" . date('Ymd', strtotime($tglakhir)) . "'
				AND pbd.kodebarang = '" . $barang . "'
				AND pb.kodegdasal = '" . $gudang . "'
			UNION ALL
			SELECT
				pbd.kodebarang,
				b.nama as namabarang,
				pb.kodecbtujuan as kodecabang,
				g.nama AS namagudang,
				b.kodemerk,
				m.nama AS namamerk,
				0 as qtyawalsblm,
				0 as qtymasuksblm,
				0 as qtykeluarsblm,
				0 as qtypicksblm,
				0 AS qtyawal,
				pbd.qty AS qtymasuk,
				0 AS qtykeluar,
				0 AS qtypick,
				b.kodesatuan AS satuan,
				bd.hargajual,
				bd.komisi
			FROM
				srvt_penerimaantransferstock pb
				LEFT JOIN srvt_penerimaantransferstockdetail pbd on pb.nomor = pbd.nomor
				LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
				LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
				LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecbtujuan
				LEFT JOIN glbm_gudang g ON g.kode = pb.kodegdtujuan
			WHERE
				pb.batal = false
				AND to_char(pb.tanggal, 'YYYYMMDD') >= '" . date('Ymd', strtotime($tglawal)) . "'
				AND to_char(pb.tanggal, 'YYYYMMDD') <= '" . date('Ymd', strtotime($tglakhir)) . "'
				AND pbd.kodebarang = '" . $barang . "'
				AND pb.kodegdtujuan = '" . $gudang . "'
		) hasil_data
	GROUP BY
		hasil_data.kodebarang,
		hasil_data.namabarang,
		hasil_data.kodecabang,
		hasil_data.namagudang,
		hasil_data.kodemerk,
		hasil_data.satuan,
		hasil_data.namamerk,
		hasil_data.hargajual,
		hasil_data.komisi
	ORDER BY
		namamerk ASC,
		namabarang ASC")->row();
		// $this->db->where("periode =", date('Ym', strtotime($tglawal)));
		// $this->db->where("kodebarang", $barang);
		// $this->db->where("kodegudang", $gudang);

		// return $this->db->get("rpt_kartustok")->row();
	}


	function GetDataKartuStokMerk($tglawal = "", $tglakhir = "", $merkawal = "", $merkakhir = "", $cabang = "")
	{
		return $this->db->query("SELECT
		hasil_data.kodebarang,
		hasil_data.namabarang,
		hasil_data.kodecabang,
		hasil_data.kodemerk,
		hasil_data.namamerk,
		sum(hasil_data.qtyawalsblm) + SUM(hasil_data.qtymasuksblm) - SUM(hasil_data.qtykeluarsblm) - SUM(hasil_data.qtypicksblm) as qtyawal,
		sum(hasil_data.qtymasuk) as qtymasuk,
		sum(hasil_data.qtykeluar) as qtykeluar,
		sum(hasil_data.qtypick) as qtypick,
		hasil_data.satuan
	FROM(
			SELECT
				i.kodebarang,
				b.nama as namabarang,
				i.kodecabang,
				b.kodemerk,
				m.nama AS namamerk,
				i.qtyawal as qtyawalsblm,
				0 AS qtymasuksblm,
				0 AS qtykeluarsblm,
				0 AS qtypicksblm,
				0 as qtyawal,
				0 as qtymasuk,
				0 as qtykeluar,
				0 as qtypick,
				b.kodesatuan AS satuan
			FROM
				srvt_inventorystok i
				LEFT JOIN glbm_barang b ON b.kode = i.kodebarang
				LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
			WHERE
				periode = '202208'
				AND b.kodemerk >= '" . $merkawal . "'
				AND b.kodemerk <= '" . $merkakhir . "'
				AND i.kodecabang = '" . $cabang . "'
			UNION ALL
			SELECT
				pbd.kodebarang,
				b.nama as namabarang,
				pb.kodecabang,
				b.kodemerk,
				m.nama AS namamerk,
				0 AS qtyawalsblm,
				pbd.qty AS qtymasuksblm,
				0 AS qtykeluarsblm,
				0 AS qtypicksblm,
				0 as qtyawal,
				0 as qtymasuk,
				0 as qtykeluar,
				0 as qtypick,
				b.kodesatuan AS satuan
			FROM
				srvt_penerimaanbarang pb
				LEFT JOIN srvt_penerimaanbarangdetail pbd on pb.nomor = pbd.nomor
				LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
				LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
			WHERE
				pb.batal = false
				AND to_char(pb.tanggal, 'YYYYMMDD') < '" . date('Ymd', strtotime($tglawal)) . "'
				AND b.kodemerk >= '" . $merkawal . "'
				AND b.kodemerk <= '" . $merkakhir . "'
				AND pb.kodecabang = '" . $cabang . "'
			UNION ALL
			SELECT
				pbd.kodebarang,
				b.nama as namabarang,
				pb.kodecabang,
				b.kodemerk,
				m.nama AS namamerk,
				0 AS qtyawalsblm,
				pbd.qty AS qtymasuksblm,
				0 AS qtykeluarsblm,
				0 AS qtypicksblm,
				0 as qtyawal,
				0 as qtymasuk,
				0 as qtykeluar,
				0 as qtypick,
				b.kodesatuan AS satuan
			FROM
				srvt_penerimaanbarang pb
				LEFT JOIN srvt_penerimaanbarangdetail pbd on pb.nomor = pbd.nomor
				LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
				LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
			WHERE
				pb.batal = true
				AND to_char(pb.tglbatal, 'YYYYMMDD') < '" . date('Ymd', strtotime($tglawal)) . "'
				AND b.kodemerk >= '" . $merkawal . "'
				AND b.kodemerk <= '" . $merkakhir . "'
				AND pb.kodecabang = '" . $cabang . "'
			UNION ALL
			SELECT
				pbd.kodebarang,
				b.nama as namabarang,
				pb.kodecabang,
				b.kodemerk,
				m.nama AS namamerk,
				0 AS qtyawalsblm,
				pbd.qty AS qtymasuksblm,
				0 AS qtykeluarsblm,
				0 AS qtypicksblm,
				0 as qtyawal,
				0 as qtymasuk,
				0 as qtykeluar,
				0 as qtypick,
				b.kodesatuan AS satuan
			FROM
				srvt_returbarang pb
				LEFT JOIN srvt_returbarangdetail pbd on pb.nomor = pbd.nomor
				LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
				LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
			WHERE
				to_char(pb.tanggal, 'YYYYMMDD') < '" . date('Ymd', strtotime($tglawal)) . "'
				AND b.kodemerk >= '" . $merkawal . "'
				AND b.kodemerk <= '" . $merkakhir . "'
				AND pb.kodecabang = '" . $cabang . "'
			UNION ALL
			SELECT
				pbd.kodebarang,
				b.nama as namabarang,
				pb.kodecabang,
				b.kodemerk,
				m.nama AS namamerk,
				0 AS qtyawalsblm,
				0 AS qtymasuksblm,
				pbd.qty AS qtykeluarsblm,
				0 AS qtypicksblm,
				0 as qtyawal,
				0 as qtymasuk,
				0 as qtykeluar,
				0 as qtypick,
				b.kodesatuan AS satuan
			FROM
				srvt_penerimaanbarang pb
				LEFT JOIN srvt_penerimaanbarangdetail pbd on pb.nomor = pbd.nomor
				LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
				LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
			WHERE
				pb.batal = true
				AND to_char(pb.tglbatal, 'YYYYMMDD') < '" . date('Ymd', strtotime($tglawal)) . "'
				AND b.kodemerk >= '" . $merkawal . "'
				AND b.kodemerk <= '" . $merkakhir . "'
				AND pb.kodecabang = '" . $cabang . "'
			UNION ALL
			SELECT
				pbd.kodebarang,
				b.nama as namabarang,
				pb.kodecabang,
				b.kodemerk,
				m.nama AS namamerk,
				0 AS qtyawalsblm,
				0 AS qtymasuksblm,
				0 AS qtykeluarsblm,
				CASE
					WHEN pb.kodestatus <> 2 THEN pbd.qty
					ELSE 0
				END AS qtypicksblm,
				0 as qtyawal,
				0 as qtymasuk,
				0 as qtykeluar,
				0 as qtypick,
				b.kodesatuan AS satuan
			FROM
				srvt_salesorder pb
				LEFT JOIN srvt_sodetail pbd on pb.nomor = pbd.nomor
				LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
				LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
			WHERE
				pb.batal = false
				AND to_char(pb.tanggal, 'YYYYMMDD') < '" . date('Ymd', strtotime($tglawal)) . "'
				AND b.kodemerk >= '" . $merkawal . "'
				AND b.kodemerk <= '" . $merkakhir . "'
				AND pb.kodecabang = '" . $cabang . "'
			UNION ALL
			SELECT
            pbd.kodebarang,
            b.nama as namabarang,
            pb.kodecabang,
            b.kodemerk,
            m.nama AS namamerk,
            0 AS qtyawalsblm,
            0 AS qtymasuksblm,
            pbd.qty AS qtykeluarsblm,
            0 AS qtypicksblm,
            0 as qtyawal,
            0 as qtymasuk,
            0 as qtykeluar,
            0 as qtypick,
            b.kodesatuan AS satuan
        FROM
            srvt_invoice pb
            LEFT JOIN srvt_invoicedetail pbd ON pb.nomor = pbd.nomor
            LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
            LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
        WHERE
            pb.batal = false
            AND to_char(pb.tanggal, 'YYYYMMDD') < '" . date('Ymd', strtotime($tglawal)) . "'
            AND b.kodemerk >= '" . $merkawal . "'
            AND b.kodemerk <= '" . $merkakhir . "'
            AND pb.kodecabang = '" . $cabang . "'
        UNION ALL
			SELECT
				pbd.kodebarang,
				b.nama as namabarang,
				pb.kodecabang,
				b.kodemerk,
				m.nama AS namamerk,
				0 AS qtyawalsblm,
				CASE
					WHEN pb.jenis = '2' THEN pbd.qty
					ELSE 0
				END AS qtymasuksblm,
				CASE
					WHEN pb.jenis <> '2' THEN pbd.qty
					ELSE 0
				END AS qtykeluarsblm,
				0 AS qtypicksblm,
				0 as qtyawal,
				0 as qtymasuk,
				0 as qtykeluar,
				0 as qtypick,
				b.kodesatuan AS satuan
			FROM
				srvt_bag pb
				LEFT JOIN srvt_bagdetail pbd on pb.nomor = pbd.nomor
				LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
				LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
			WHERE
				pb.batal = false
				AND approve = true
				AND to_char(pb.tanggal, 'YYYYMMDD') < '" . date('Ymd', strtotime($tglawal)) . "'
				AND b.kodemerk >= '" . $merkawal . "'
				AND b.kodemerk <= '" . $merkakhir . "'
				AND pb.kodecabang = '" . $cabang . "'
			UNION ALL
			SELECT
				pbd.kodebarang,
				b.nama as namabarang,
				pb.kodecbasal as kodecabang,
				b.kodemerk,
				m.nama AS namamerk,
				0 AS qtyawalsblm,
				0 AS qtymasuksblm,
				CASE WHEN pb.kodestatus = 2 THEN pbd.qty ELSE 0 END AS qtykeluarsblm,
				CASE WHEN pb.kodestatus <> 2 THEN pbd.qty ELSE 0 END AS qtypicksblm,
				0 as qtyawal,
				0 as qtymasuk,
				0 as qtykeluar,
				0 as qtypick,
				b.kodesatuan AS satuan
			FROM
				srvt_transferstock pb
				LEFT JOIN srvt_transferstockdetail pbd on pb.nomor = pbd.nomor
				LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
				LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
			WHERE
				pb.batal = false
				AND to_char(pb.tanggal, 'YYYYMMDD') < '" . date('Ymd', strtotime($tglawal)) . "'
				AND b.kodemerk >= '" . $merkawal . "'
				AND b.kodemerk <= '" . $merkakhir . "'
				AND pb.kodecbasal = '" . $cabang . "'
			UNION ALL
			SELECT
				pbd.kodebarang,
				b.nama as namabarang,
				pb.kodecbtujuan as kodecabang,
				b.kodemerk,
				m.nama AS namamerk,
				0 AS qtyawalsblm,
				pbd.qty AS qtymasuksblm,
				0 AS qtykeluarsblm,
				0 AS qtypicksblm,
				0 as qtyawal,
				0 as qtymasuk,
				0 as qtykeluar,
				0 as qtypick,
				b.kodesatuan AS satuan
			FROM
				srvt_penerimaantransferstock pb
				LEFT JOIN srvt_penerimaantransferstockdetail pbd on pb.nomor = pbd.nomor
				LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
				LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
			WHERE
				pb.batal = false
				AND to_char(pb.tanggal, 'YYYYMMDD') < '" . date('Ymd', strtotime($tglawal)) . "'
				AND b.kodemerk >= '" . $merkawal . "'
				AND b.kodemerk <= '" . $merkakhir . "'
				AND pb.kodecbtujuan = '" . $cabang . "'
			UNION ALL
			SELECT
				pbd.kodebarang,
				b.nama as namabarang,
				pb.kodecabang,
				b.kodemerk,
				m.nama AS namamerk,
				0 as qtyawalsblm,
				0 as qtymasuksblm,
				0 as qtykeluarsblm,
				0 as qtypicksblm,
				0 AS qtyawal,
				pbd.qty AS qtymasuk,
				0 AS qtykeluar,
				0 AS qtypick,
				b.kodesatuan AS satuan
			FROM
				srvt_penerimaanbarang pb
				LEFT JOIN srvt_penerimaanbarangdetail pbd on pb.nomor = pbd.nomor
				LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
				LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
			WHERE
				pb.batal = false
				AND to_char(pb.tanggal, 'YYYYMMDD') >= '" . date('Ymd', strtotime($tglawal)) . "'
				AND to_char(pb.tanggal, 'YYYYMMDD') <= '" . date('Ymd', strtotime($tglakhir)) . "'
				AND b.kodemerk >= '" . $merkawal . "'
				AND b.kodemerk <= '" . $merkakhir . "'
				AND pb.kodecabang = '" . $cabang . "'
			UNION ALL
			SELECT
				pbd.kodebarang,
				b.nama as namabarang,
				pb.kodecabang,
				b.kodemerk,
				m.nama AS namamerk,
				0 as qtyawalsblm,
				0 as qtymasuksblm,
				0 as qtykeluarsblm,
				0 as qtypicksblm,
				0 AS qtyawal,
				pbd.qty AS qtymasuk,
				0 AS qtykeluar,
				0 AS qtypick,
				b.kodesatuan AS satuan
			FROM
				srvt_penerimaanbarang pb
				LEFT JOIN srvt_penerimaanbarangdetail pbd on pb.nomor = pbd.nomor
				LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
				LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
			WHERE
				pb.batal = true
				AND to_char(pb.tglbatal, 'YYYYMMDD') >= '" . date('Ymd', strtotime($tglawal)) . "'
				AND to_char(pb.tglbatal, 'YYYYMMDD') <= '" . date('Ymd', strtotime($tglakhir)) . "'
				AND b.kodemerk >= '" . $merkawal . "'
				AND b.kodemerk <= '" . $merkakhir . "'
				AND pb.kodecabang = '" . $cabang . "'
			UNION ALL
			SELECT
				pbd.kodebarang,
				b.nama as namabarang,
				pb.kodecabang,
				b.kodemerk,
				m.nama AS namamerk,
				0 as qtyawalsblm,
				0 as qtymasuksblm,
				0 as qtykeluarsblm,
				0 as qtypicksblm,
				0 AS qtyawal,
				pbd.qty AS qtymasuk,
				0 AS qtykeluar,
				0 AS qtypick,
				b.kodesatuan AS satuan
			FROM
				srvt_returbarang pb
				LEFT JOIN srvt_returbarangdetail pbd on pb.nomor = pbd.nomor
				LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
				LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
			WHERE
				to_char(pb.tanggal, 'YYYYMMDD') >= '" . date('Ymd', strtotime($tglawal)) . "'
				AND to_char(pb.tanggal, 'YYYYMMDD') <= '" . date('Ymd', strtotime($tglakhir)) . "'
				AND b.kodemerk >= '" . $merkawal . "'
				AND b.kodemerk <= '" . $merkakhir . "'
				AND pb.kodecabang = '" . $cabang . "'
			UNION ALL
			SELECT
				pbd.kodebarang,
				b.nama as namabarang,
				pb.kodecabang,
				b.kodemerk,
				m.nama AS namamerk,
				0 as qtyawalsblm,
				0 as qtymasuksblm,
				0 as qtykeluarsblm,
				0 as qtypicksblm,
				0 AS qtyawal,
				0 AS qtymasuk,
				pbd.qty AS qtykeluar,
				0 AS qtypick,
				b.kodesatuan AS satuan
			FROM
				srvt_penerimaanbarang pb
				LEFT JOIN srvt_penerimaanbarangdetail pbd on pb.nomor = pbd.nomor
				LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
				LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
			WHERE
				pb.batal = true
				AND to_char(pb.tglbatal, 'YYYYMMDD') >= '" . date('Ymd', strtotime($tglawal)) . "'
				AND to_char(pb.tglbatal, 'YYYYMMDD') <= '" . date('Ymd', strtotime($tglakhir)) . "'
				AND b.kodemerk >= '" . $merkawal . "'
				AND b.kodemerk <= '" . $merkakhir . "'
				AND pb.kodecabang = '" . $cabang . "'
			UNION ALL
			SELECT
				pbd.kodebarang,
				b.nama as namabarang,
				pb.kodecabang,
				b.kodemerk,
				m.nama AS namamerk,
				0 as qtyawalsblm,
				0 as qtymasuksblm,
				0 as qtykeluarsblm,
				0 as qtypicksblm,
				0 AS qtyawal,
				0 AS qtymasuk,
				0 AS qtykeluar,
				CASE
					WHEN pb.kodestatus <> 2 THEN pbd.qty
					ELSE 0
				END AS qtypick,
				b.kodesatuan AS satuan
			FROM
				srvt_salesorder pb
				LEFT JOIN srvt_sodetail pbd on pb.nomor = pbd.nomor
				LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
				LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
			WHERE
				pb.batal = false
				AND to_char(pb.tanggal, 'YYYYMMDD') >= '" . date('Ymd', strtotime($tglawal)) . "'
				AND to_char(pb.tanggal, 'YYYYMMDD') <= '" . date('Ymd', strtotime($tglakhir)) . "'
				AND b.kodemerk >= '" . $merkawal . "'
				AND b.kodemerk <= '" . $merkakhir . "'
				AND pb.kodecabang = '" . $cabang . "'
				UNION ALL
        SELECT
            pbd.kodebarang,
            b.nama as namabarang,
            pb.kodecabang,
            b.kodemerk,
            m.nama AS namamerk,
            0 AS qtyawalsblm,
            0 AS qtymasuksblm,
            0 AS qtykeluarsblm,
            0 AS qtypicksblm,
            0 as qtyawal,
            0 as qtymasuk,
            pbd.qty as qtykeluar,
            0 as qtypick,
            b.kodesatuan AS satuan
        FROM
            srvt_invoice pb
            LEFT JOIN srvt_invoicedetail pbd ON pb.nomor = pbd.nomor
            LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
            LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
        WHERE
            pb.batal = false
            AND to_char(pb.tanggal, 'YYYYMMDD') >= '" . date('Ymd', strtotime($tglawal)) . "'
            AND to_char(pb.tanggal, 'YYYYMMDD') <= '" . date('Ymd', strtotime($tglakhir)) . "'
            AND b.kodemerk >= '" . $merkawal . "'
            AND b.kodemerk <= '" . $merkakhir . "'
            AND pb.kodecabang = '" . $cabang . "'
			UNION ALL
			SELECT
				pbd.kodebarang,
				b.nama as namabarang,
				pb.kodecabang,
				b.kodemerk,
				m.nama AS namamerk,
				0 as qtyawalsblm,
				0 as qtymasuksblm,
				0 as qtykeluarsblm,
				0 as qtypicksblm,
				0 AS qtyawal,
				CASE
					WHEN pb.jenis = '2' THEN pbd.qty
					ELSE 0
				END AS qtymasuk,
				CASE
					WHEN pb.jenis <> '2' THEN pbd.qty
					ELSE 0
				END AS qtykeluar,
				0 AS qtypick,
				b.kodesatuan AS satuan
			FROM
				srvt_bag pb
				LEFT JOIN srvt_bagdetail pbd on pb.nomor = pbd.nomor
				LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
				LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
			WHERE
				pb.batal = false
				AND approve = true
				AND to_char(pb.tanggal, 'YYYYMMDD') >= '" . date('Ymd', strtotime($tglawal)) . "'
				AND to_char(pb.tanggal, 'YYYYMMDD') <= '" . date('Ymd', strtotime($tglakhir)) . "'
				AND b.kodemerk >= '" . $merkawal . "'
				AND b.kodemerk <= '" . $merkakhir . "'
				AND pb.kodecabang = '" . $cabang . "'
			UNION ALL
			SELECT
				pbd.kodebarang,
				b.nama as namabarang,
				pb.kodecbasal as kodecabang,
				b.kodemerk,
				m.nama AS namamerk,
				0 as qtyawalsblm,
				0 as qtymasuksblm,
				0 as qtykeluarsblm,
				0 as qtypicksblm,
				0 AS qtyawal,
				0 AS qtymasuk,
				CASE WHEN pb.kodestatus = 2 THEN pbd.qty ELSE 0 END AS qtykeluar,
            	CASE WHEN pb.kodestatus <> 2 THEN pbd.qty ELSE 0 END AS qtypick,
				b.kodesatuan AS satuan
			FROM
				srvt_transferstock pb
				LEFT JOIN srvt_transferstockdetail pbd on pb.nomor = pbd.nomor
				LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
				LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
			WHERE
				pb.batal = false
				AND to_char(pb.tanggal, 'YYYYMMDD') >= '" . date('Ymd', strtotime($tglawal)) . "'
				AND to_char(pb.tanggal, 'YYYYMMDD') <= '" . date('Ymd', strtotime($tglakhir)) . "'
				AND b.kodemerk >= '" . $merkawal . "'
				AND b.kodemerk <= '" . $merkakhir . "'
				AND pb.kodecbasal = '" . $cabang . "'
			UNION ALL
			SELECT
				pbd.kodebarang,
				b.nama as namabarang,
				pb.kodecbtujuan as kodecabang,
				b.kodemerk,
				m.nama AS namamerk,
				0 as qtyawalsblm,
				0 as qtymasuksblm,
				0 as qtykeluarsblm,
				0 as qtypicksblm,
				0 AS qtyawal,
				pbd.qty AS qtymasuk,
				0 AS qtykeluar,
				0 AS qtypick,
				b.kodesatuan AS satuan
			FROM
				srvt_penerimaantransferstock pb
				LEFT JOIN srvt_penerimaantransferstockdetail pbd on pb.nomor = pbd.nomor
				LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
				LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
			WHERE
				pb.batal = false
				AND to_char(pb.tanggal, 'YYYYMMDD') >= '" . date('Ymd', strtotime($tglawal)) . "'
				AND to_char(pb.tanggal, 'YYYYMMDD') <= '" . date('Ymd', strtotime($tglakhir)) . "'
				AND b.kodemerk >= '" . $merkawal . "'
				AND b.kodemerk <= '" . $merkakhir . "'
				AND pb.kodecbtujuan = '" . $cabang . "'
		) hasil_data
	GROUP BY
		hasil_data.kodebarang,
		hasil_data.namabarang,
		hasil_data.kodecabang,
		hasil_data.kodemerk,
		hasil_data.satuan,
		hasil_data.namamerk
	ORDER BY
		namamerk ASC,
		namabarang ASC")->result();
	}

	function GetDataKartuStokMerkPajak($tglawal = "", $tglakhir = "", $merkawal = "", $merkakhir = "", $cabang = "")
	{
		return $this->db->query("SELECT
		hasil_data.kodebarang,
		hasil_data.namabarang,
		hasil_data.kodecabang,
		hasil_data.kodemerk,
		hasil_data.namamerk,
		sum(hasil_data.qtyawal) as qtyawal,
		sum(hasil_data.qtymasuk) as qtymasuk,
		sum(hasil_data.qtykeluar) as qtykeluar,
		sum(hasil_data.qtypick) as qtypick,
		hasil_data.satuan
	FROM(
			SELECT
				i.kodebarang,
				b.nama as namabarang,
				i.kodecabang,
				b.kodemerk,
				m.nama AS namamerk,
				i.qtyawal,
				0 AS qtymasuk,
				0 AS qtykeluar,
				0 AS qtypick,
				b.kodesatuan AS satuan
			FROM
				srvt_inventorystok i
				LEFT JOIN glbm_barang b ON b.kode = i.kodebarang
				LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
			WHERE
				periode = '" . date('Ym', strtotime($tglawal)) . "'
				AND b.kodemerk >= '" . $merkawal . "'
				AND b.kodemerk <= '" . $merkakhir . "'
				AND i.kodecabang = '" . $cabang . "'
			UNION ALL
			SELECT
				pbd.kodebarang,
				b.nama as namabarang,
				pb.kodecabang,
				b.kodemerk,
				m.nama AS namamerk,
				0 AS qtyawal,
				pbd.qty AS qtymasuk,
				0 AS qtykeluar,
				0 AS qtypick,
				b.kodesatuan AS satuan
			FROM
				srvt_penerimaanbarang pb
				LEFT JOIN srvt_penerimaanbarangdetail pbd on pb.nomor = pbd.nomor
				LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
				LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
			WHERE
				pb.batal = false
				AND to_char(pb.tanggal, 'YYYYMM') >= '" . date('Ym', strtotime($tglawal)) . "'
				AND to_char(pb.tanggal, 'YYYYMM') <= '" . date('Ym', strtotime($tglakhir)) . "'
				AND b.kodemerk >= '" . $merkawal . "'
				AND b.kodemerk <= '" . $merkakhir . "'
				AND pb.kodecabang = '" . $cabang . "'
			UNION ALL
			SELECT
				pbd.kodebarang,
				b.nama as namabarang,
				pb.kodecabang,
				b.kodemerk,
				m.nama AS namamerk,
				0 AS qtyawal,
				pbd.qty AS qtymasuk,
				0 AS qtykeluar,
				0 AS qtypick,
				b.kodesatuan AS satuan
			FROM
				srvt_penerimaanbarang pb
				LEFT JOIN srvt_penerimaanbarangdetail pbd on pb.nomor = pbd.nomor
				LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
				LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
			WHERE
				pb.batal = true
				AND to_char(pb.tglbatal, 'YYYYMM') >= '" . date('Ym', strtotime($tglawal)) . "'
				AND to_char(pb.tglbatal, 'YYYYMM') <= '" . date('Ym', strtotime($tglakhir)) . "'
				AND b.kodemerk >= '" . $merkawal . "'
				AND b.kodemerk <= '" . $merkakhir . "'
				AND pb.kodecabang = '" . $cabang . "'
			UNION ALL
			SELECT
				pbd.kodebarang,
				b.nama as namabarang,
				pb.kodecabang,
				b.kodemerk,
				m.nama AS namamerk,
				0 AS qtyawal,
				0 AS qtymasuk,
				pbd.qty AS qtykeluar,
				0 AS qtypick,
				b.kodesatuan AS satuan
			FROM
				srvt_penerimaanbarang pb
				LEFT JOIN srvt_penerimaanbarangdetail pbd on pb.nomor = pbd.nomor
				LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
				LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
			WHERE
				pb.batal = true
				AND to_char(pb.tglbatal, 'YYYYMM') >= '" . date('Ym', strtotime($tglawal)) . "'
				AND to_char(pb.tglbatal, 'YYYYMM') <= '" . date('Ym', strtotime($tglakhir)) . "'
				AND b.kodemerk >= '" . $merkawal . "'
				AND b.kodemerk <= '" . $merkakhir . "'
				AND pb.kodecabang = '" . $cabang . "'
			UNION ALL
			SELECT
				pbd.kodebarang,
				b.nama as namabarang,
				pb.kodecabang,
				b.kodemerk,
				m.nama AS namamerk,
				0 AS qtyawal,
				0 AS qtymasuk,
				pbd.qty as qtykeluar,
				0 AS qtypick,
				b.kodesatuan AS satuan
			FROM
				srvt_invoice pb
				LEFT JOIN srvt_invoicedetail pbd on pb.nomor = pbd.nomor
				LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
				LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
			WHERE
				pb.batal = false
				AND to_char(pb.tanggal, 'YYYYMM') >= '" . date('Ym', strtotime($tglawal)) . "'
				AND to_char(pb.tanggal, 'YYYYMM') <= '" . date('Ym', strtotime($tglakhir)) . "'
				AND b.kodemerk >= '" . $merkawal . "'
				AND b.kodemerk <= '" . $merkakhir . "'
				AND pb.kodecabang = '" . $cabang . "'
			UNION ALL
			SELECT
				pbd.kodebarang,
				b.nama as namabarang,
				pb.kodecabang,
				b.kodemerk,
				m.nama AS namamerk,
				0 AS qtyawal,
				CASE
					WHEN pb.jenis = '2' THEN pbd.qty
					ELSE 0
				END AS qtymasuk,
				CASE
					WHEN pb.jenis <> '2' THEN pbd.qty
					ELSE 0
				END AS qtykeluar,
				0 AS qtypick,
				b.kodesatuan AS satuan
			FROM
				srvt_bag pb
				LEFT JOIN srvt_bagdetail pbd on pb.nomor = pbd.nomor
				LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
				LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
			WHERE
				pb.batal = false
				AND approve = true
				AND to_char(pb.tanggal, 'YYYYMM') >= '" . date('Ym', strtotime($tglawal)) . "'
				AND to_char(pb.tanggal, 'YYYYMM') <= '" . date('Ym', strtotime($tglakhir)) . "'
				AND b.kodemerk >= '" . $merkawal . "'
				AND b.kodemerk <= '" . $merkakhir . "'
				AND pb.kodecabang = '" . $cabang . "'
			UNION ALL
			SELECT
				pbd.kodebarang,
				b.nama as namabarang,
				pb.kodecbasal as kodecabang,
				b.kodemerk,
				m.nama AS namamerk,
				0 AS qtyawal,
				0 AS qtymasuk,
				pbd.qty AS qtymasuk,
				0 AS qtypick,
				b.kodesatuan AS satuan
			FROM
				srvt_transferstock pb
				LEFT JOIN srvt_transferstockdetail pbd on pb.nomor = pbd.nomor
				LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
				LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
			WHERE
				pb.batal = false
				AND status = true
				AND to_char(pb.tanggal, 'YYYYMM') >= '" . date('Ym', strtotime($tglawal)) . "'
				AND to_char(pb.tanggal, 'YYYYMM') <= '" . date('Ym', strtotime($tglakhir)) . "'
				AND b.kodemerk >= '" . $merkawal . "'
				AND b.kodemerk <= '" . $merkakhir . "'
				AND pb.kodecbasal = '" . $cabang . "'
			UNION ALL
			SELECT
				pbd.kodebarang,
				b.nama as namabarang,
				pb.kodecbtujuan as kodecabang,
				b.kodemerk,
				m.nama AS namamerk,
				0 AS qtyawal,
				pbd.qty AS qtymasuk,
				0 AS qtykeluar,
				0 AS qtypick,
				b.kodesatuan AS satuan
			FROM
				srvt_penerimaantransferstock pb
				LEFT JOIN srvt_penerimaantransferstockdetail pbd on pb.nomor = pbd.nomor
				LEFT JOIN srvt_suratjalangudang sjg on sjg.nomor = pb.notransfer
				LEFT JOIN srvt_transferstock tf on tf.nomor = sjg.noproses
				LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
				LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
			WHERE
				pb.batal = false
				AND to_char(tf.tanggal, 'YYYYMM') >= '" . date('Ym', strtotime($tglawal)) . "'
				AND to_char(tf.tanggal, 'YYYYMM') <= '" . date('Ym', strtotime($tglakhir)) . "'
				AND b.kodemerk >= '" . $merkawal . "'
				AND b.kodemerk <= '" . $merkakhir . "'
				AND pb.kodecbtujuan = '" . $cabang . "'
				UNION ALL
				SELECT
				pbd.kodebarang,
				b.nama as namabarang,
				pb.kodecabang,
				b.kodemerk,
				m.nama as namamerk,
				0 as qtyawal,
				pbd.qty as qtymasuk,
				0 as qtykeluar,
				0 as qtypick,
				b.kodesatuan as kodesatuan
				FROM srvt_returbarang pb
				LEFT JOIN srvt_returbarangdetail pbd on pbd.nomor = pb.nomor
				LEFT JOIN glbm_barang b on b.kode = pbd.kodebarang
				LEFT JOIN glbm_merk m on m.kode = b.kodemerk
				 WHERE
				pb.batal = false
				AND to_char(pb.tanggal, 'YYYYMM') >= '" . date('Ym', strtotime($tglawal)) . "'
				AND to_char(pb.tanggal, 'YYYYMM') <= '" . date('Ym', strtotime($tglakhir)) . "'
				AND b.kodemerk >= '" . $merkawal . "'
				AND b.kodemerk <= '" . $merkakhir . "'
				AND pb.kodecabang = '" . $cabang . "'
		) hasil_data
	GROUP BY
		hasil_data.kodebarang,
		hasil_data.namabarang,
		hasil_data.kodecabang,
		hasil_data.kodemerk,
		hasil_data.satuan,
		hasil_data.namamerk
	ORDER BY
		namamerk ASC,
		namabarang ASC")->result();
	}

	function GetDataKartuStokMerkPerGudang($tglawal = "", $tglakhir = "", $merkawal = "", $merkakhir = "", $gudang = "", $cabang = "")
	{
		return $this->db->query("SELECT
		hasil_data.kodebarang,
		hasil_data.namabarang,
		hasil_data.kodecabang,
		hasil_data.namagudang,
		hasil_data.kodemerk,
		hasil_data.namamerk,
		sum(hasil_data.qtyawalsblm) + SUM(hasil_data.qtymasuksblm) - SUM(hasil_data.qtykeluarsblm) - SUM(hasil_data.qtypicksblm) as qtyawal,
		sum(hasil_data.qtymasuk) as qtymasuk,
		sum(hasil_data.qtykeluar) as qtykeluar,
		sum(hasil_data.qtypick) as qtypick,
		(
	sum(hasil_data.qtyawalsblm) + SUM(hasil_data.qtymasuksblm) - SUM(hasil_data.qtykeluarsblm) - SUM(hasil_data.qtypicksblm) + sum(hasil_data.qtymasuk)
	) - (
	ABS(
	SUM(hasil_data.qtykeluar) + SUM(hasil_data.qtypick)
	)
	) AS stok,
		hasil_data.satuan,
		hasil_data.hargajual,
		hasil_data.komisi
	FROM(SELECT
			i.kodebarang,
			b.nama as namabarang,
			i.kodecabang,
			g.nama AS namagudang,
			b.kodemerk,
			m.nama AS namamerk,
			i.qtyawal as qtyawalsblm,
			0 AS qtymasuksblm,
			0 AS qtykeluarsblm,
			0 AS qtypicksblm,
			0 as qtyawal,
			0 as qtymasuk,
			0 as qtykeluar,
			0 as qtypick,
			b.kodesatuan AS satuan,
			bd.hargajual,
			bd.komisi
		FROM
			srvt_inventorystok i
			LEFT JOIN glbm_barang b ON b.kode = i.kodebarang
			LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
			LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = i.kodecabang
			LEFT JOIN glbm_gudang g ON g.kode = i.kodegudang
		WHERE
			periode = '202208'
			AND b.kodemerk >= '" . $merkawal . "'
			AND b.kodemerk <= '" . $merkakhir . "'
			AND i.kodecabang = '" . $cabang . "'
			AND i.kodegudang = '" . $gudang . "'
		UNION ALL
		SELECT
			pbd.kodebarang,
			b.nama as namabarang,
			pb.kodecabang,
			g.nama AS namagudang,
			b.kodemerk,
			m.nama AS namamerk,
			0 AS qtyawalsblm,
			pbd.qty AS qtymasuksblm,
			0 AS qtykeluarsblm,
			0 AS qtypicksblm,
			0 as qtyawal,
			0 as qtymasuk,
			0 as qtykeluar,
			0 as qtypick,
			b.kodesatuan AS satuan,
			bd.hargajual,
			bd.komisi
		FROM
			srvt_penerimaanbarang pb
			LEFT JOIN srvt_penerimaanbarangdetail pbd on pb.nomor = pbd.nomor
			LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
			LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
			LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecabang
			LEFT JOIN glbm_gudang g ON g.kode = pb.kodegudang
		WHERE
			pb.batal = false
			AND to_char(pb.tanggal, 'YYYYMMDD') < '" . date('Ymd', strtotime($tglawal)) . "'
			AND b.kodemerk >= '" . $merkawal . "'
			AND b.kodemerk <= '" . $merkakhir . "'
			AND pb.kodecabang = '" . $cabang . "'
			AND pb.kodegudang = '" . $gudang . "'
		UNION ALL
		SELECT
			pbd.kodebarang,
			b.nama as namabarang,
			pb.kodecabang,
			g.nama AS namagudang,
			b.kodemerk,
			m.nama AS namamerk,
			0 AS qtyawalsblm,
			pbd.qty AS qtymasuksblm,
			0 AS qtykeluarsblm,
			0 AS qtypicksblm,
			0 as qtyawal,
			0 as qtymasuk,
			0 as qtykeluar,
			0 as qtypick,
			b.kodesatuan AS satuan,
			bd.hargajual,
			bd.komisi
		FROM
			srvt_penerimaanbarang pb
			LEFT JOIN srvt_penerimaanbarangdetail pbd on pb.nomor = pbd.nomor
			LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
			LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
			LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecabang
			LEFT JOIN glbm_gudang g ON g.kode = pb.kodegudang
		WHERE
			pb.batal = true
			AND to_char(pb.tglbatal, 'YYYYMMDD') < '" . date('Ymd', strtotime($tglawal)) . "'
			AND b.kodemerk >= '" . $merkawal . "'
			AND b.kodemerk <= '" . $merkakhir . "'
			AND pb.kodecabang = '" . $cabang . "'
			AND pb.kodegudang = '" . $gudang . "'
		UNION ALL
		SELECT
			pbd.kodebarang,
			b.nama as namabarang,
			pb.kodecabang,
			g.nama AS namagudang,
			b.kodemerk,
			m.nama AS namamerk,
			0 AS qtyawalsblm,
			pbd.qty AS qtymasuksblm,
			0 AS qtykeluarsblm,
			0 AS qtypicksblm,
			0 as qtyawal,
			0 as qtymasuk,
			0 as qtykeluar,
			0 as qtypick,
			b.kodesatuan AS satuan,
			bd.hargajual,
			bd.komisi
		FROM
			srvt_returbarang pb
			LEFT JOIN srvt_returbarangdetail pbd on pb.nomor = pbd.nomor
			LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
			LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
			LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecabang
			LEFT JOIN glbm_gudang g ON g.kode = pb.lokasigudang
		WHERE
			to_char(pb.tanggal, 'YYYYMMDD') < '" . date('Ymd', strtotime($tglawal)) . "'
			AND b.kodemerk >= '" . $merkawal . "'
			AND b.kodemerk <= '" . $merkakhir . "'
			AND pb.kodecabang = '" . $cabang . "'
			AND pb.lokasigudang = '" . $gudang . "'
		UNION ALL
		SELECT
			pbd.kodebarang,
			b.nama as namabarang,
			pb.kodecabang,
			g.nama AS namagudang,
			b.kodemerk,
			m.nama AS namamerk,
			0 AS qtyawalsblm,
			0 AS qtymasuksblm,
			pbd.qty AS qtykeluarsblm,
			0 AS qtypicksblm,
			0 as qtyawal,
			0 as qtymasuk,
			0 as qtykeluar,
			0 as qtypick,
			b.kodesatuan AS satuan,
			bd.hargajual,
			bd.komisi
		FROM
			srvt_penerimaanbarang pb
			LEFT JOIN srvt_penerimaanbarangdetail pbd on pb.nomor = pbd.nomor
			LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
			LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
			LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecabang
			LEFT JOIN glbm_gudang g ON g.kode = pb.kodegudang
		WHERE
			pb.batal = true
			AND to_char(pb.tglbatal, 'YYYYMMDD') < '" . date('Ymd', strtotime($tglawal)) . "'
			AND b.kodemerk >= '" . $merkawal . "'
			AND b.kodemerk <= '" . $merkakhir . "'
			AND pb.kodecabang = '" . $cabang . "'
			AND pb.kodegudang = '" . $gudang . "'
		UNION ALL
		SELECT
			pbd.kodebarang,
			b.nama as namabarang,
			pb.kodecabang,
			g.nama AS namagudang,
			b.kodemerk,
			m.nama AS namamerk,
			0 AS qtyawalsblm,
			0 AS qtymasuksblm,
			0 AS qtykeluarsblm,
			CASE
				WHEN pb.kodestatus <> 2 THEN pbd.qty
				ELSE 0
			END AS qtypicksblm,
			0 as qtyawal,
			0 as qtymasuk,
			0 as qtykeluar,
			0 as qtypick,
			b.kodesatuan AS satuan,
			bd.hargajual,
			bd.komisi
		FROM
			srvt_salesorder pb
			LEFT JOIN srvt_sodetail pbd on pb.nomor = pbd.nomor
			LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
			LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
			LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecabang
			LEFT JOIN glbm_gudang g ON g.kode = pb.lokasi
		WHERE
			pb.batal = false
			AND to_char(pb.tanggal, 'YYYYMMDD') < '" . date('Ymd', strtotime($tglawal)) . "'
			AND b.kodemerk >= '" . $merkawal . "'
			AND b.kodemerk <= '" . $merkakhir . "'
			AND pb.kodecabang = '" . $cabang . "'
			AND pb.lokasi = '" . $gudang . "'
		UNION ALL
		SELECT
			pbd.kodebarang,
			b.nama as namabarang,
			pb.kodecabang,
			g.nama AS namagudang,
			b.kodemerk,
			m.nama AS namamerk,
			0 AS qtyawalsblm,
			0 AS qtymasuksblm,
			pbd.qty AS qtykeluarsblm,
			0 AS qtypicksblm,
			0 as qtyawal,
			0 as qtymasuk,
			0 as qtykeluar,
			0 as qtypick,
			b.kodesatuan AS satuan,
			bd.hargajual,
			bd.komisi
		FROM
			srvt_invoice pb
			LEFT JOIN srvt_invoicedetail pbd ON pb.nomor = pbd.nomor
			LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
			LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
			LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecabang
			LEFT JOIN glbm_gudang g ON g.kode = pb.lokasi
		WHERE
			pb.batal = false
			AND to_char(pb.tanggal, 'YYYYMMDD') < '" . date('Ymd', strtotime($tglawal)) . "'
			AND b.kodemerk >= '" . $merkawal . "'
			AND b.kodemerk <= '" . $merkakhir . "'
			AND pb.kodecabang = '" . $cabang . "'
			AND pb.lokasi = '" . $gudang . "'
		UNION ALL
		SELECT
			pbd.kodebarang,
			b.nama as namabarang,
			pb.kodecabang,
			g.nama AS namagudang,
			b.kodemerk,
			m.nama AS namamerk,
			0 AS qtyawalsblm,
			CASE
				WHEN pb.jenis = '2' THEN pbd.qty
				ELSE 0
			END AS qtymasuksblm,
			CASE
				WHEN pb.jenis <> '2' THEN pbd.qty
				ELSE 0
			END AS qtykeluarsblm,
			0 AS qtypicksblm,
			0 as qtyawal,
			0 as qtymasuk,
			0 as qtykeluar,
			0 as qtypick,
			b.kodesatuan AS satuan,
			bd.hargajual,
			bd.komisi
		FROM
			srvt_bag pb
			LEFT JOIN srvt_bagdetail pbd on pb.nomor = pbd.nomor
			LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
			LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
			LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecabang
			LEFT JOIN glbm_gudang g ON g.kode = pb.lokasi
		WHERE
			pb.batal = false
			AND approve = true
			AND to_char(pb.tanggal, 'YYYYMMDD') < '" . date('Ymd', strtotime($tglawal)) . "'
			AND b.kodemerk >= '" . $merkawal . "'
			AND b.kodemerk <= '" . $merkakhir . "'
			AND pb.kodecabang = '" . $cabang . "'
			AND pb.lokasi = '" . $gudang . "'
		UNION ALL
		SELECT
			pbd.kodebarang,
			b.nama as namabarang,
			pb.kodecbasal as kodecabang,
			g.nama AS namagudang,
			b.kodemerk,
			m.nama AS namamerk,
			0 AS qtyawalsblm,
			0 AS qtymasuksblm,
			CASE WHEN pb.kodestatus = 2 THEN pbd.qty ELSE 0 END AS qtykeluarsblm,
			CASE WHEN pb.kodestatus <> 2 THEN pbd.qty ELSE 0 END AS qtypicksblm,
			0 as qtyawal,
			0 as qtymasuk,
			0 as qtykeluar,
			0 as qtypick,
			b.kodesatuan AS satuan,
			bd.hargajual,
			bd.komisi
		FROM
			srvt_transferstock pb
			LEFT JOIN srvt_transferstockdetail pbd on pb.nomor = pbd.nomor
			LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
			LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
			LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecbasal
			LEFT JOIN glbm_gudang g ON g.kode = pb.kodegdasal
		WHERE
			pb.batal = false
			AND to_char(pb.tanggal, 'YYYYMMDD') < '" . date('Ymd', strtotime($tglawal)) . "'
			AND b.kodemerk >= '" . $merkawal . "'
			AND b.kodemerk <= '" . $merkakhir . "'
			AND pb.kodecbasal = '" . $cabang . "'
			AND pb.kodegdasal = '" . $gudang . "'
		UNION ALL
		SELECT
			pbd.kodebarang,
			b.nama as namabarang,
			pb.kodecbtujuan as kodecabang,
			g.nama AS namagudang,
			b.kodemerk,
			m.nama AS namamerk,
			0 AS qtyawalsblm,
			pbd.qty AS qtymasuksblm,
			0 AS qtykeluarsblm,
			0 AS qtypicksblm,
			0 as qtyawal,
			0 as qtymasuk,
			0 as qtykeluar,
			0 as qtypick,
			b.kodesatuan AS satuan,
			bd.hargajual,
			bd.komisi
		FROM
			srvt_penerimaantransferstock pb
			LEFT JOIN srvt_penerimaantransferstockdetail pbd on pb.nomor = pbd.nomor
			LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
			LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
			LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecbtujuan
			LEFT JOIN glbm_gudang g ON g.kode = pb.kodegdtujuan
		WHERE
			pb.batal = false
			AND to_char(pb.tanggal, 'YYYYMMDD') < '" . date('Ymd', strtotime($tglawal)) . "'
			AND b.kodemerk >= '" . $merkawal . "'
			AND b.kodemerk <= '" . $merkakhir . "'
			AND pb.kodecbtujuan = '" . $cabang . "'
			AND pb.kodegdtujuan = '" . $gudang . "'
	UNION ALL
	SELECT
		pbd.kodebarang,
		b.nama as namabarang,
		pb.kodecabang,
		g.nama AS namagudang,
		b.kodemerk,
		m.nama AS namamerk,
		0 as qtyawalsblm,
		0 as qtymasuksblm,
		0 as qtykeluarsblm,
		0 as qtypicksblm,
		0 AS qtyawal,
		pbd.qty AS qtymasuk,
		0 AS qtykeluar,
		0 AS qtypick,
		b.kodesatuan AS satuan,
		bd.hargajual,
		bd.komisi
	FROM
		srvt_penerimaanbarang pb
		LEFT JOIN srvt_penerimaanbarangdetail pbd on pb.nomor = pbd.nomor
		LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
		LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
		LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecabang
		LEFT JOIN glbm_gudang g ON g.kode = pb.kodegudang
	WHERE
		pb.batal = false
		AND to_char(pb.tanggal, 'YYYYMMDD') >= '" . date('Ymd', strtotime($tglawal)) . "'
		AND to_char(pb.tanggal, 'YYYYMMDD') <= '" . date('Ymd', strtotime($tglakhir)) . "'
		AND b.kodemerk >= '" . $merkawal . "'
		AND b.kodemerk <= '" . $merkakhir . "'
		AND pb.kodecabang = '" . $cabang . "'
		AND pb.kodegudang = '" . $gudang . "'
	UNION ALL
	SELECT
		pbd.kodebarang,
		b.nama as namabarang,
		pb.kodecabang,
		g.nama AS namagudang,
		b.kodemerk,
		m.nama AS namamerk,
		0 as qtyawalsblm,
		0 as qtymasuksblm,
		0 as qtykeluarsblm,
		0 as qtypicksblm,
		0 AS qtyawal,
		pbd.qty AS qtymasuk,
		0 AS qtykeluar,
		0 AS qtypick,
		b.kodesatuan AS satuan,
		bd.hargajual,
		bd.komisi
	FROM
		srvt_penerimaanbarang pb
		LEFT JOIN srvt_penerimaanbarangdetail pbd on pb.nomor = pbd.nomor
		LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
		LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
		LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecabang
		LEFT JOIN glbm_gudang g ON g.kode = pb.kodegudang
	WHERE
		pb.batal = true
		AND to_char(pb.tglbatal, 'YYYYMMDD') >= '" . date('Ymd', strtotime($tglawal)) . "'
		AND to_char(pb.tglbatal, 'YYYYMMDD') <= '" . date('Ymd', strtotime($tglakhir)) . "'
		AND b.kodemerk >= '" . $merkawal . "'
		AND b.kodemerk <= '" . $merkakhir . "'
		AND pb.kodecabang = '" . $cabang . "'
		AND pb.kodegudang = '" . $gudang . "'
	UNION ALL
	SELECT
		pbd.kodebarang,
		b.nama as namabarang,
		pb.kodecabang,
		g.nama AS namagudang,
		b.kodemerk,
		m.nama AS namamerk,
		0 as qtyawalsblm,
		0 as qtymasuksblm,
		0 as qtykeluarsblm,
		0 as qtypicksblm,
		0 AS qtyawal,
		pbd.qty AS qtymasuk,
		0 AS qtykeluar,
		0 AS qtypick,
		b.kodesatuan AS satuan,
		bd.hargajual,
		bd.komisi
	FROM
		srvt_returbarang pb
		LEFT JOIN srvt_returbarangdetail pbd on pb.nomor = pbd.nomor
		LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
		LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
		LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecabang
		LEFT JOIN glbm_gudang g ON g.kode = pb.lokasigudang
	WHERE
		to_char(pb.tanggal, 'YYYYMMDD') >= '" . date('Ymd', strtotime($tglawal)) . "'
		AND to_char(pb.tanggal, 'YYYYMMDD') <= '" . date('Ymd', strtotime($tglakhir)) . "'
		AND b.kodemerk >= '" . $merkawal . "'
		AND b.kodemerk <= '" . $merkakhir . "'
		AND pb.kodecabang = '" . $cabang . "'
		AND pb.lokasigudang = '" . $gudang . "'
	UNION ALL
	SELECT
		pbd.kodebarang,
		b.nama as namabarang,
		pb.kodecabang,
		g.nama AS namagudang,
		b.kodemerk,
		m.nama AS namamerk,
		0 as qtyawalsblm,
		0 as qtymasuksblm,
		0 as qtykeluarsblm,
		0 as qtypicksblm,
		0 AS qtyawal,
		0 AS qtymasuk,
		pbd.qty AS qtykeluar,
		0 AS qtypick,
		b.kodesatuan AS satuan,
		bd.hargajual,
		bd.komisi
	FROM
		srvt_penerimaanbarang pb
		LEFT JOIN srvt_penerimaanbarangdetail pbd on pb.nomor = pbd.nomor
		LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
		LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
		LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecabang
		LEFT JOIN glbm_gudang g ON g.kode = pb.kodegudang
	WHERE
		pb.batal = true
		AND to_char(pb.tglbatal, 'YYYYMMDD') >= '" . date('Ymd', strtotime($tglawal)) . "'
		AND to_char(pb.tglbatal, 'YYYYMMDD') <= '" . date('Ymd', strtotime($tglakhir)) . "'
		AND b.kodemerk >= '" . $merkawal . "'
		AND b.kodemerk <= '" . $merkakhir . "'
		AND pb.kodecabang = '" . $cabang . "'
		AND pb.kodegudang = '" . $gudang . "'
	UNION ALL
	SELECT
		pbd.kodebarang,
		b.nama as namabarang,
		pb.kodecabang,
		g.nama AS namagudang,
		b.kodemerk,
		m.nama AS namamerk,
		0 as qtyawalsblm,
		0 as qtymasuksblm,
		0 as qtykeluarsblm,
		0 as qtypicksblm,
		0 AS qtyawal,
		0 AS qtymasuk,
		0 AS qtykeluar,
		CASE
			WHEN pb.kodestatus <> 2 THEN pbd.qty
			ELSE 0
		END AS qtypick,
		b.kodesatuan AS satuan,
		bd.hargajual,
		bd.komisi
	FROM
		srvt_salesorder pb
		LEFT JOIN srvt_sodetail pbd on pb.nomor = pbd.nomor
		LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
		LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
		LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecabang
		LEFT JOIN glbm_gudang g ON g.kode = pb.lokasi
	WHERE
		pb.batal = false
		AND to_char(pb.tanggal, 'YYYYMMDD') >= '" . date('Ymd', strtotime($tglawal)) . "'
		AND to_char(pb.tanggal, 'YYYYMMDD') <= '" . date('Ymd', strtotime($tglakhir)) . "'
		AND b.kodemerk >= '" . $merkawal . "'
		AND b.kodemerk <= '" . $merkakhir . "'
		AND pb.kodecabang = '" . $cabang . "'
		AND pb.lokasi = '" . $gudang . "'
	UNION ALL
	SELECT
		pbd.kodebarang,
		b.nama as namabarang,
		pb.kodecabang,
		g.nama AS namagudang,
		b.kodemerk,
		m.nama AS namamerk,
		0 AS qtyawalsblm,
		0 AS qtymasuksblm,
		0 AS qtykeluarsblm,
		0 AS qtypicksblm,
		0 as qtyawal,
		0 as qtymasuk,
		pbd.qty as qtykeluar,
		0 as qtypick,
		b.kodesatuan AS satuan,
		bd.hargajual,
		bd.komisi
	FROM
		srvt_invoice pb
		LEFT JOIN srvt_invoicedetail pbd ON pb.nomor = pbd.nomor
		LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
		LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
		LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecabang
		LEFT JOIN glbm_gudang g ON g.kode = pb.lokasi
	WHERE
		pb.batal = false
		AND to_char(pb.tanggal, 'YYYYMMDD') >= '" . date('Ymd', strtotime($tglawal)) . "'
		AND to_char(pb.tanggal, 'YYYYMMDD') <= '" . date('Ymd', strtotime($tglakhir)) . "'
		AND b.kodemerk >= '" . $merkawal . "'
		AND b.kodemerk <= '" . $merkakhir . "'
		AND pb.kodecabang = '" . $cabang . "'
		AND pb.lokasi = '" . $gudang . "'
	UNION ALL
	SELECT
		pbd.kodebarang,
		b.nama as namabarang,
		pb.kodecabang,
		g.nama AS namagudang,
		b.kodemerk,
		m.nama AS namamerk,
		0 as qtyawalsblm,
		0 as qtymasuksblm,
		0 as qtykeluarsblm,
		0 as qtypicksblm,
		0 AS qtyawal,
		CASE
			WHEN pb.jenis = '2' THEN pbd.qty
			ELSE 0
		END AS qtymasuk,
		CASE
			WHEN pb.jenis <> '2' THEN pbd.qty
			ELSE 0
		END AS qtykeluar,
		0 AS qtypick,
		b.kodesatuan AS satuan,
		bd.hargajual,
		bd.komisi
	FROM
		srvt_bag pb
		LEFT JOIN srvt_bagdetail pbd on pb.nomor = pbd.nomor
		LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
		LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
		LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecabang
		LEFT JOIN glbm_gudang g ON g.kode = pb.lokasi
	WHERE
		pb.batal = false
		AND approve = true
		AND to_char(pb.tanggal, 'YYYYMMDD') >= '" . date('Ymd', strtotime($tglawal)) . "'
		AND to_char(pb.tanggal, 'YYYYMMDD') <= '" . date('Ymd', strtotime($tglakhir)) . "'
		AND b.kodemerk >= '" . $merkawal . "'
		AND b.kodemerk <= '" . $merkakhir . "'
		AND pb.kodecabang = '" . $cabang . "'
		AND pb.lokasi = '" . $gudang . "'
	UNION ALL
	SELECT
		pbd.kodebarang,
		b.nama as namabarang,
		pb.kodecbasal as kodecabang,
		g.nama AS namagudang,
		b.kodemerk,
		m.nama AS namamerk,
		0 as qtyawalsblm,
		0 as qtymasuksblm,
		0 as qtykeluarsblm,
		0 as qtypicksblm,
		0 AS qtyawal,
		0 AS qtymasuk,
		CASE WHEN pb.kodestatus = 2 THEN pbd.qty ELSE 0 END AS qtykeluar,
		CASE WHEN pb.kodestatus <> 2 THEN pbd.qty ELSE 0 END AS qtypick,
		b.kodesatuan AS satuan,
		bd.hargajual,
		bd.komisi
	FROM
		srvt_transferstock pb
		LEFT JOIN srvt_transferstockdetail pbd on pb.nomor = pbd.nomor
		LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
		LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
		LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecbasal
		LEFT JOIN glbm_gudang g ON g.kode = pb.kodegdasal
	WHERE
		pb.batal = false
		AND to_char(pb.tanggal, 'YYYYMMDD') >= '" . date('Ymd', strtotime($tglawal)) . "'
		AND to_char(pb.tanggal, 'YYYYMMDD') <= '" . date('Ymd', strtotime($tglakhir)) . "'
		AND b.kodemerk >= '" . $merkawal . "'
		AND b.kodemerk <= '" . $merkakhir . "'
		AND pb.kodecbasal = '" . $cabang . "'
		AND pb.kodegdasal = '" . $gudang . "'
	UNION ALL
	SELECT
		pbd.kodebarang,
		b.nama as namabarang,
		pb.kodecbtujuan as kodecabang,
		g.nama AS namagudang,
		b.kodemerk,
		m.nama AS namamerk,
		0 as qtyawalsblm,
		0 as qtymasuksblm,
		0 as qtykeluarsblm,
		0 as qtypicksblm,
		0 AS qtyawal,
		pbd.qty AS qtymasuk,
		0 AS qtykeluar,
		0 AS qtypick,
		b.kodesatuan AS satuan,
		bd.hargajual,
		bd.komisi
	FROM
		srvt_penerimaantransferstock pb
		LEFT JOIN srvt_penerimaantransferstockdetail pbd on pb.nomor = pbd.nomor
		LEFT JOIN glbm_barang b ON b.kode = pbd.kodebarang
		LEFT JOIN glbm_merk m ON m.kode = b.kodemerk
		LEFT JOIN glbm_barangdetail bd ON bd.kodebarang = b.kode AND bd.kodecabang = pb.kodecbtujuan
		LEFT JOIN glbm_gudang g ON g.kode = pb.kodegdtujuan
	WHERE
		pb.batal = false
		AND to_char(pb.tanggal, 'YYYYMMDD') >= '" . date('Ymd', strtotime($tglawal)) . "'
		AND to_char(pb.tanggal, 'YYYYMMDD') <= '" . date('Ymd', strtotime($tglakhir)) . "'
		AND b.kodemerk >= '" . $merkawal . "'
		AND b.kodemerk <= '" . $merkakhir . "'
		AND pb.kodecbtujuan = '" . $cabang . "'
		AND pb.kodegdtujuan = '" . $gudang . "'
	) hasil_data
		GROUP BY
		hasil_data.kodebarang,
		hasil_data.namabarang,
		hasil_data.kodecabang,
		hasil_data.namagudang,
		hasil_data.kodemerk,
		hasil_data.satuan,
		hasil_data.namamerk,
		hasil_data.hargajual,
		hasil_data.komisi
		ORDER BY
		namamerk ASC,
		namabarang ASC")->result();
	}

	function LaporanTransferStokPergudang($tglawal = "", $tglakhir = "", $gudang = "")
	{
		$this->db->where("tanggal >=", date('Y-m-d', strtotime($tglawal)));
		$this->db->where("tanggal <=", date('Y-m-d', strtotime($tglakhir)));
		$this->db->where("kodegdasal", $gudang);

		return $this->db->get("rpt_transferstoknew")->result();
	}

	function LaporanTransferStokPergudangRow($tglawal = "", $tglakhir = "", $gudang = "")
	{
		$this->db->where("tanggal >=", date('Y-m-d', strtotime($tglawal)));
		$this->db->where("tanggal <=", date('Y-m-d', strtotime($tglakhir)));
		$this->db->where("kodegdasal", $gudang);

		return $this->db->get("rpt_transferstoknew")->result();
	}

	function LaporanTransferStokPergudangList($tglawal = "", $tglakhir = "", $gudang = "")
	{
		$this->db->select("nomor");
		$this->db->select("nomorsj");
		$this->db->select("tanggal");
		$this->db->select("namasupplier");
		$this->db->where("tanggal >=", date('Y-m-d', strtotime($tglawal)));
		$this->db->where("tanggal <=", date('Y-m-d', strtotime($tglakhir)));
		$this->db->where("kodegdasal", $gudang);
		$this->db->group_by("nomor");
		$this->db->group_by("nomorsj");
		$this->db->group_by("tanggal");
		$this->db->group_by("namasupplier");

		return $this->db->get("rpt_transferstoknew")->result();
	}

	#Penerimaan Transfer Stok
	function LaporanPenerimaanTransferStokPergudang($tglawal = "", $tglakhir = "", $gudang = "")
	{
		$this->db->where("tanggal >=", date('Y-m-d', strtotime($tglawal)));
		$this->db->where("tanggal <=", date('Y-m-d', strtotime($tglakhir)));
		$this->db->where("kodegdtujuan", $gudang);

		return $this->db->get("rpt_penerimaantfnew")->result();
	}

	function LaporanPenerimaanTransferStokPergudangRow($tglawal = "", $tglakhir = "", $gudang = "")
	{
		$this->db->where("tanggal >=", date('Y-m-d', strtotime($tglawal)));
		$this->db->where("tanggal <=", date('Y-m-d', strtotime($tglakhir)));
		$this->db->where("kodegdtujuan", $gudang);

		return $this->db->get("rpt_penerimaantfnew")->result();
	}

	function LaporanPenerimaanTransferStokPergudangList($tglawal = "", $tglakhir = "", $gudang = "")
	{
		$this->db->select("nomorpo");
		// $this->db->select("nomor");
		$this->db->select("tanggal");
		$this->db->select("namasupplier");
		$this->db->where("tanggal >=", date('Y-m-d', strtotime($tglawal)));
		$this->db->where("tanggal <=", date('Y-m-d', strtotime($tglakhir)));
		$this->db->where("kodegdtujuan", $gudang);
		$this->db->group_by("nomorpo");
		// $this->db->group_by("nomor");
		$this->db->group_by("tanggal");
		$this->db->group_by("namasupplier");

		return $this->db->get("rpt_penerimaantfnew")->result();
	}
}
