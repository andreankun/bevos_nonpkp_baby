<?php
class LaporanKeuangan_model extends CI_Model
{
	function Pelanggan ($pelanggan){
		return $this->db->query("SELECT *FROM glbm_customer WHERE nomor = '".$pelanggan."'")->row();
	}
	function GetDataLaporanPiutang($tglawal = "", $tglakhir = "", $cabang = "")
	{
		return $this->db->query("SELECT 
		p.nomor,
		p.tanggal,
		p.kodecabang,
		p.nilaipiutang,
		p.nilaialokasi + p.nilaipenerimaan as terbayar,
		p.nopelanggan,
		c.nama as namapelanggan,
		c.nama_toko as namatoko,
		cd.namasalesman
		FROM srvt_piutang p
		LEFT JOIN glbm_customer c on c.nomor = p.nopelanggan
		LEFT JOIN glbm_customerdetail cd on cd.nomor = p.nopelanggan
		WHERE batal = false and tanggal BETWEEN '" . $tglawal . "' and '" . $tglakhir . "' and p.kodecabang = '" . $cabang . "'")->result();
	}

	function GetDataLaporanPiutangRow($tglawal = "", $tglakhir = "", $cabang = "")
	{
		return $this->db->query("SELECT
		p.nomor,
		p.tanggal,
		p.nilaipiutang,
		p.nilaialokasi + p.nilaipenerimaan as terbayar,
		p.nopelanggan,
		p.kodecabang,
		c.nama,
		c.nama_toko,
		cd.namasalesman
		FROM srvt_piutang p
		LEFT join glbm_customer c on c.nomor = p.nopelanggan
		LEFT join glbm_customerdetail cd on cd.nomor = p.nopelanggan
		where p.batal = false and p.tanggal BETWEEN '" . $tglawal . "' and '" . $tglakhir . "' and p.kodecabang = '" . $cabang . "'
		")->row();
	}

	function LaporanPiutangJatuhTempo($tglawal = "", $tglakhir = "", $cabang = "")
	{
		$this->db->select("nama");
		$this->db->select("nama_toko");
		$this->db->select("nopelanggan");
		$this->db->where("tgljthtempo >=", $tglawal);
		$this->db->where("tgljthtempo <=", $tglakhir);
		$this->db->where("kodecabang", $cabang);
		$this->db->group_by("nama");
		$this->db->group_by("nama_toko");
		$this->db->group_by("nopelanggan");
		$this->db->order_by("nama_toko", "ASC");

		return $this->db->get("rpt_laporanpiutangjatuhtempo")->result();
	}

	function LaporanPiutangJatuhTempoList($tglawal = "", $tglakhir = "", $cabang = "")
	{
		// $this->db->select("tanggal");
		$this->db->where("tgljthtempo >=", $tglawal);
		$this->db->where("tgljthtempo <=", $tglakhir);
		$this->db->where("kodecabang", $cabang);


		return $this->db->get("rpt_laporanpiutangjatuhtempo")->result();
	}
	// Deposit Pelanggan
	function LaporanDepositPelanggan($cabang = "", $tglawal = "", $tglakhir = "")
	{
		return $this->db->query("SELECT
		hasil_data.nomorcustomer,
		sum(hasil_data.debit) AS debit,
		sum(hasil_data.kredit) AS kredit,
		hasil_data.namapelanggan,
		hasil_data.namatoko,
		hasil_data.kodecabang
	FROM
		(
			SELECT
				hd.nomorcustomer,
				sum(hd.debit) AS debit,
				sum(hd.kredit) AS kredit,
				c.nama AS namapelanggan,
				c.nama_toko AS namatoko,
				c.kodecabang
			FROM
				(
					trnt_historydepositcustomer hd
					LEFT JOIN glbm_customer c ON (((c.nomor) :: text = (hd.nomorcustomer) :: text))
				)
				WHERE
	TO_CHAR(hd.tanggal,'YYYYMMDD') >= '" . date('Ymd', strtotime($tglawal)) . "'
	AND TO_CHAR(hd.tanggal,'YYYYMMDD') <= '" . date('Ymd', strtotime($tglakhir)) . "'
	AND c.kodecabang = '" . $cabang . "'
			GROUP BY
				hd.nomorcustomer,
				c.nama,
				c.nama_toko,
				c.kodecabang
		) hasil_data
	GROUP BY
		hasil_data.nomorcustomer,
		hasil_data.namapelanggan,
		hasil_data.namatoko,
		hasil_data.kodecabang
		ORDER BY
		hasil_data.namatoko;")->result();
		// $this->db->where("kodecabang", $cabang);
		// $this->db->order_by("namapelanggan", "ASC");

		// return $this->db->get("rpt_saldopelanggan")->result();
	}
	// End Deposit Pelanggan

	// PDF Pelanggan Tidak Aktif
	function LaporanPelangganTidakAktif($cabang = "", $tglawal = "", $tglakhir = "")
	{
		return $this->db->query("SELECT nomor,
	kodecabang,
	nama as namapelanggan,
	nama_toko as namatoko
	FROM glbm_customer 
	WHERE aktif = FALSE and kodecabang = '" . $cabang . "' ORDER BY namatoko ASC")->result();
	}

	function LaporanPelangganTidakAktifRow($cabang = "", $tglawal = "", $tglakhir = "")
	{
		return $this->db->query("SELECT nomor,
		kodecabang,
		nama as namapelanggan,
		nama_toko as namatoko
		FROM glbm_customer 
		WHERE aktif = FALSE and kodecabang = '" . $cabang . "' ORDER BY nama_toko ASC")->row();
	}
	// End PDF Pelanggan tidak aktif

	// Outstanding Invoice
	function LaporanInvoiceOutstanding($tglawal = "", $tglakhir = "", $cabang = "")
	{
		$this->db->select("namatoko");
		$this->db->select("nama");
		$this->db->select("nopelanggan");
		$this->db->where("tanggal >=", $tglawal);
		$this->db->where("tanggal <=", $tglakhir);
		$this->db->where("kodecabang", $cabang);
		$this->db->group_by("namatoko");
		$this->db->group_by("nama");
		$this->db->group_by("nopelanggan");
		$this->db->order_by("namatoko", "ASC");

		return $this->db->get("rpt_invoiceoutstanding")->result();
	}

	function LaporanInvoiceOutstandingList($tglawal = "", $tglakhir = "", $cabang = "")
	{
		$this->db->where("tanggal >=", $tglawal);
		$this->db->where("tanggal <=", $tglakhir);
		$this->db->where("kodecabang", $cabang);

		return $this->db->get("rpt_invoiceoutstanding")->result();
	}
	// end Outstanding Invoice

	public function LaporanJenisPembayaran($tglawal = "", $tglakhir = "", $cabang = "")
	{
		$this->db->where("tanggalpenerimaan >=", $tglawal);
		$this->db->where("tanggalpenerimaan <=", $tglakhir);
		$this->db->where("kodecabang", $cabang);

		return $this->db->get("rpt_jenispemabayartunai")->result();
	}

	public function LaporanJenisPemabayarKredit($tglawal = "", $tglakhir = "", $cabang = "")
	{
		$this->db->where("tanggalpenerimaan >=", $tglawal);
		$this->db->where("tanggalpenerimaan <=", $tglakhir);
		$this->db->where("kodecabang", $cabang);

		return $this->db->get("rpt_jenispemabayarkredit")->result();
	}

	#Laporan Kartu Piutang Usaha#
	function GetDataKartuPiutangUsahaList($tglawal = "", $tglakhir = "", $cabang = "")
	{
		return $this->db->query("SELECT
		hasil.nopelanggan,
		hasil.kodecabang,
		hasil.namapelanggan,
		hasil.nama_toko,
		SUM(hasil.saldoawal) AS saldoawal,
		SUM(hasil.nilaipiutang) AS nilaipiutang,
		SUM(hasil.nilaipembulatan) AS nilaipembulatan,
		SUM(hasil.terbayar) AS terbayar
	FROM(
			SELECT
				p.nopelanggan,
				p.kodecabang,
				c.nama AS namapelanggan,
				c.nama_toko,
				SUM(p.nilaipiutang) AS saldoawal,
				0 AS nilaipiutang,
				0 AS nilaipembulatan,
				0 AS terbayar
			FROM
				srvt_piutang p
				LEFT JOIN glbm_customer c ON c.nomor = p.nopelanggan
			WHERE
				p.batal = false
				AND to_char(p.tanggal, 'YYYYMMDD') < '" . date('Ymd', strtotime($tglawal)) . "'
				AND p.kodecabang = '" . $cabang . "'
			GROUP BY
				p.nopelanggan,
				p.kodecabang,
				c.nama,
				c.nama_toko
			UNION ALL
			SELECT
				p.nopelanggan,
				p.kodecabang,
				c.nama AS namapelanggan,
				c.nama_toko,
				0 AS saldoawal,
				SUM(p.nilaipiutang) AS nilaipiutang,
				0 AS nilaipembulatan,
				0 AS terbayar
			FROM
				srvt_piutang p
				LEFT JOIN glbm_customer c ON c.nomor = p.nopelanggan
			WHERE
				p.batal = false
				AND to_char(p.tanggal, 'YYYYMMDD') >= '" . date('Ymd', strtotime($tglawal)) . "'
				AND to_char(p.tanggal, 'YYYYMMDD') <= '" . date('Ymd', strtotime($tglakhir)) . "'
				AND p.kodecabang = '" . $cabang . "'
			GROUP BY
				p.nopelanggan,
				p.kodecabang,
				c.nama,
				c.nama_toko
			UNION ALL
			SELECT
				p.nopelanggan,
				p.kodecabang,
				c.nama AS namapelanggan,
				c.nama_toko,
				0 AS saldoawal,
				0 AS nilaipiutang,
				sum(a.nilaipembulatan) AS nilaipembulatan,
				sum(a.terbayar) AS terbayar
			FROM
				srvt_piutang p
				LEFT JOIN srvt_invoice inv ON inv.nomor :: text = p.nomor :: text
				LEFT JOIN srvt_salesorder so ON so.nomor = inv.nomorso
				LEFT JOIN (
					SELECT
						pud.noreferensi,
						pud.nomorpenerimaan,
						pud.nocustomer,
						sum(pud.nilaipenerimaan + pud.nilaialokasi) AS terbayar,
						sum(pud.nilaipembulatan) AS nilaipembulatan
					FROM
						trnt_penerimaanuangdetail pud
					GROUP BY
						pud.noreferensi,
						pud.nomorpenerimaan,
						pud.nocustomer
				) a ON a.noreferensi :: text = inv.nomorso :: text
				LEFT JOIN trnt_penerimaanuang pu ON pu.nomor :: text = a.nomorpenerimaan :: text
				LEFT JOIN glbm_customer c ON c.nomor :: text = p.nopelanggan :: text
				LEFT JOIN glbm_cabang cb ON cb.kode :: text = p.kodecabang :: text
			WHERE
				inv.batal = false
				AND p.batal = false
				AND pu.batal = false
				AND to_char(pu.tanggal, 'YYYYMMDD') >= '" . date('Ymd', strtotime($tglawal)) . "'
				AND to_char(pu.tanggal, 'YYYYMMDD') <= '" . date('Ymd', strtotime($tglakhir)) . "'
				AND p.kodecabang = '" . $cabang . "'
			GROUP BY
				p.kodecabang,
				cb.nama,
				p.nopelanggan,
				c.nama,
				c.nama_toko
			UNION ALL
			SELECT
				p.nopelanggan,
				p.kodecabang,
				c.nama AS namapelanggan,
				c.nama_toko,
				0 AS saldoawal,
				0 AS nilaipiutang,
				sum(a.nilaipembulatan) AS nilaipembulatan,
				sum(a.terbayar) AS terbayar
			FROM
				srvt_piutang p
				LEFT JOIN srvt_invoice inv ON inv.nomor :: text = p.nomor :: text
				LEFT JOIN srvt_salesorder so ON so.nomor = inv.nomorso
				LEFT JOIN (
					SELECT
						pud.noreferensi,
						pud.nomorpenerimaan,
						pud.nocustomer,
						sum(pud.nilaipenerimaan + pud.nilaialokasi) AS terbayar,
						sum(pud.nilaipembulatan) AS nilaipembulatan
					FROM
						trnt_penerimaanuangdetail pud
					GROUP BY
						pud.noreferensi,
						pud.nomorpenerimaan,
						pud.nocustomer
				) a ON a.noreferensi :: text = p.nomor :: text
				LEFT JOIN trnt_penerimaanuang pu ON pu.nomor :: text = a.nomorpenerimaan :: text
				LEFT JOIN glbm_customer c ON c.nomor :: text = p.nopelanggan :: text
				LEFT JOIN glbm_cabang cb ON cb.kode :: text = p.kodecabang :: text
			WHERE
				p.batal = false
				AND pu.batal = false
				AND to_char(pu.tanggal, 'YYYYMMDD') >= '" . date('Ymd', strtotime($tglawal)) . "'
				AND to_char(pu.tanggal, 'YYYYMMDD') <= '" . date('Ymd', strtotime($tglakhir)) . "'
				AND p.kodecabang = '" . $cabang . "'
			GROUP BY
				p.kodecabang,
				cb.nama,
				p.nopelanggan,
				c.nama,
				c.nama_toko
		) hasil
	GROUP BY
		hasil.nopelanggan,
		hasil.kodecabang,
		hasil.namapelanggan,
		hasil.nama_toko
	ORDER BY
		hasil.nama_toko")->result();
	}

	function GetDataKartuPiutangUsaha($tglawal = "", $tglakhir = "", $cabang = "")
	{
		return $this->db->query("SELECT
		hasil.nomor,
		hasil.nopelanggan,
		hasil.tanggal,
		hasil.tgljthtempo,
		hasil.nilaipiutang,
		hasil.nilaipenerimaan,
		hasil.nilaialokasi,
		sum(hasil.terbayar) as terbayar
		FROM(
			SELECT
				p.nomor,
				p.nopelanggan,
				p.tanggal,
				p.tgljthtempo,
				p.nilaipiutang,
				case WHEN p.nilaipenerimaan <> 0 THEN p.nilaipenerimaan ELSE 0 END as nilaipenerimaan,
				case WHEN p.nilaialokasi <> 0 THEN p.nilaialokasi ELSE 0 END as nilaialokasi,
				0 as terbayar
			FROM
				srvt_piutang p
			WHERE
				p.batal = false
				and to_char(p.tanggal, 'YYYYMMMDD') <= '" . date('Ymd', strtotime($tglawal)) . "'
				and p.kodecabang = '" . $cabang . "'
				GROUP BY
				p.nomor,
				p.nopelanggan,
				p.nilaipiutang
			UNION ALL
			SELECT
				p.nomor,
				p.nopelanggan,
				p.tanggal,
				p.tgljthtempo,
				p.nilaipiutang,
				case WHEN p.nilaipenerimaan <> 0 THEN p.nilaipenerimaan ELSE 0 END as nilaipenerimaan,
				case WHEN p.nilaialokasi <> 0 THEN p.nilaialokasi ELSE 0 END as nilaialokasi,
				0 as terbayar
			FROM
				srvt_piutang p
			WHERE
				p.batal = false
				and to_char(p.tanggal, 'YYYYMMMDD') >= '" . date('Ymd', strtotime($tglawal)) . "'
				AND to_char(p.tanggal, 'YYYYMMDD') <= '" . date('Ymd', strtotime($tglakhir)) . "'
				and p.kodecabang = '" . $cabang . "'
				 GROUP BY
				p.nomor,
				p.nopelanggan,
				p.nilaipiutang
			UNION ALL
			SELECT
				p.nomor,
				p.nopelanggan,
				p.tanggal,
				p.tgljthtempo,
				p.nilaipiutang,
				p.nilaipenerimaan,
				p.nilaialokasi,
				a.terbayar
			FROM
				srvt_piutang p
				LEFT JOIN srvt_invoice inv on inv.nomor = p.nomor
				LEFT JOIN srvt_salesorder so on so.nomor = inv.nomorso
				LEFT JOIN(
					SELECT
						pud.noreferensi,
						pud.nomorpenerimaan,
						case when pud.nilaipembulatan < 0 then COALESCE(
							sum(pud.nilaipenerimaan) + sum(pud.nilaialokasi) + sum(abs(pud.nilaipembulatan))
						) else COALESCE(
							sum(pud.nilaipenerimaan) + sum(pud.nilaialokasi) + sum(abs(pud.nilaipembulatan))
						) end as terbayar
					FROM
						trnt_penerimaanuangdetail pud
					GROUP BY
						pud.noreferensi,
						pud.nomorpenerimaan
				) a on a.noreferensi = inv.nomorso
				LEFT JOIN trnt_penerimaanuang pu on pu.nomor = a.nomorpenerimaan
			WHERE
				inv.batal = FALSE
				and p.batal = FALSE
				and pu.batal = FALSE
				and to_char(pu.tanggal, 'YYYYMMDD') >= '" . date('Ymd', strtotime($tglawal)) . "'
				AND to_char(pu.tanggal, 'YYYYMMDD') <= '" . date('Ymd', strtotime($tglakhir)) . "'
				and p.kodecabang = '" . $cabang . "'
			GROUP BY
				p.nomor,
				p.nopelanggan,
				p.nilaipenerimaan,
				p.nilaipiutang,
				p.nilaialokasi,
				a.terbayar
			UNION ALL
			SELECT
				p.nomor,
				p.nopelanggan,
				p.tanggal,
				p.tgljthtempo,
				p.nilaipiutang,
				p.nilaipenerimaan,
				p.nilaialokasi,
				a.terbayar
			FROM
				srvt_piutang p
				LEFT JOIN(
					SELECT
						pud.noreferensi,
						pud.nomorpenerimaan,
						COALESCE(pud.nilaipenerimaan + pud.nilaialokasi + pud.nilaipembulatan, 0) as terbayar
					FROM
						trnt_penerimaanuangdetail pud
					GROUP BY
						pud.noreferensi,
						pud.nomorpenerimaan
				) a on a.noreferensi = p.nomor
				LEFT JOIN trnt_penerimaanuang pu on pu.nomor = a.nomorpenerimaan
			WHERE p.batal = FALSE
				and pu.batal = FALSE
				and to_char(pu.tanggal, 'YYYYMMDD') >= '" . date('Ymd', strtotime($tglawal)) . "'
				AND to_char(pu.tanggal, 'YYYYMMDD') <= '" . date('Ymd', strtotime($tglakhir)) . "'
				and p.kodecabang = '" . $cabang . "'
			GROUP BY
				p.nomor,
				p.nopelanggan,
				p.nilaipenerimaan,
				p.nilaipiutang,
				p.nilaialokasi,
				a.terbayar
		) hasil
	GROUP BY
		hasil.nomor,
		hasil.nopelanggan,
		hasil.tanggal,
		hasil.tgljthtempo,
		hasil.nilaipiutang,
		hasil.nilaipenerimaan,
		hasil.nilaialokasi
		order by hasil.nomor asc
			")->result();
	}
	#End Of Laporan Kartu Piutang Usaha#

	#Laporan Piutang Per Pelanggan#
	function GetDataPiutangPerPelangganRow($tglawal = "", $tglakhir = "", $cabang = "", $pelanggan = "")
	{
		// $this->db->select("nama_toko");
		$this->db->where("tanggal >=", $tglawal);
		$this->db->where("tanggal <=", $tglakhir);
		$this->db->where("kodecabang", $cabang);
		$this->db->where("nopelanggan", $pelanggan);
		// $this->db->group_by("nama_toko");

		return $this->db->get("rpt_piutangperpelanggan")->row();
	}

	function GetDataPiutangPerPelanggan($tglawal = "", $tglakhir = "", $cabang = "", $pelanggan = "")
	{
		$this->db->where("tanggal >=", $tglawal);
		$this->db->where("tanggal <=", $tglakhir);
		$this->db->where("kodecabang", $cabang);
		$this->db->where("nopelanggan", $pelanggan);

		return $this->db->get("rpt_piutangperpelanggan")->result();
	}
	#End Of Laporan Piutang Per Pelanggan#

	#Laporan Daftar Piutang#
	function GetGataDaftarPiutangRow($tglawal = "", $tglakhir = "", $cabang = "")
	{
		// $this->db->select("nama_toko");
		$this->db->where("tanggal >=", $tglawal);
		$this->db->where("tanggal <=", $tglakhir);
		$this->db->where("kodecabang", $cabang);
		// $this->db->group_by("nama_toko");

		return $this->db->get("rpt_daftarpiutang")->row();
	}

	function GetGataDaftarPiutang($tglawal = "", $tglakhir = "", $cabang = "")
	{
		return $this->db->query("SELECT
		hasil.nopelanggan,
		hasil.kodecabang,
		hasil.namapelanggan,
		hasil.nama_toko,
		SUM(hasil.saldoawal) - SUM(hasil.saldopengurangan) AS saldoawal,
		SUM(hasil.nilaipiutang) AS nilaipiutang,
		SUM(hasil.nilaipembulatan) AS nilaipembulatan,
		SUM(hasil.terbayar) AS terbayar
	FROM(
			SELECT
				p.nopelanggan,
				p.kodecabang,
				c.nama AS namapelanggan,
				c.nama_toko,
				SUM(p.nilaipiutang) AS saldoawal,
				0 AS saldopengurangan,
				0 AS nilaipiutang,
				0 AS nilaipembulatan,
				0 AS terbayar
			FROM
				srvt_piutang p
				LEFT JOIN glbm_customer c ON c.nomor = p.nopelanggan
			WHERE
				p.batal = false
				AND to_char(p.tanggal, 'YYYYMMDD') < '" . date('Ymd', strtotime($tglawal)) . "'
				AND p.kodecabang = '" . $cabang . "'
			GROUP BY
				p.nopelanggan,
				p.kodecabang,
				c.nama,
				c.nama_toko
			UNION ALL
			SELECT
            p.nopelanggan,
            p.kodecabang,
            c.nama AS namapelanggan,
            c.nama_toko,
            0 AS saldoawal,
            sum(a.nilaipembulatan) + sum(a.terbayar) AS saldopengurangan,
            0 AS nilaipiutang,
            0 AS nilaipembulatan,
            0 AS terbayar
        FROM
            srvt_piutang p
            LEFT JOIN srvt_invoice inv ON inv.nomor :: text = p.nomor :: text
            LEFT JOIN srvt_salesorder so ON so.nomor = inv.nomorso
            LEFT JOIN (
                SELECT
                    pud.noreferensi,
                    pud.nomorpenerimaan,
                    pud.nocustomer,
                    sum(pud.nilaipenerimaan + pud.nilaialokasi) AS terbayar,
                    sum(pud.nilaipembulatan) AS nilaipembulatan
                FROM
                    trnt_penerimaanuangdetail pud
                GROUP BY
                    pud.noreferensi,
                    pud.nomorpenerimaan,
                    pud.nocustomer
            ) a ON a.noreferensi :: text = p.nomor :: text
            LEFT JOIN trnt_penerimaanuang pu ON pu.nomor :: text = a.nomorpenerimaan :: text
            LEFT JOIN glbm_customer c ON c.nomor :: text = p.nopelanggan :: text
            LEFT JOIN glbm_cabang cb ON cb.kode :: text = p.kodecabang :: text
        WHERE
            p.batal = false
            AND pu.batal = false
            AND to_char(pu.tanggal, 'YYYYMM') < '" . date('Ym', strtotime($tglawal)) . "'
            AND p.kodecabang = '" . $cabang . "'
        GROUP BY
            p.kodecabang,
            cb.nama,
            p.nopelanggan,
            c.nama,
            c.nama_toko
        UNION ALL
			SELECT
				p.nopelanggan,
				p.kodecabang,
				c.nama AS namapelanggan,
				c.nama_toko,
				0 AS saldoawal,
				0 AS saldopengurangan,
				SUM(p.nilaipiutang) AS nilaipiutang,
				0 AS nilaipembulatan,
				0 AS terbayar
			FROM
				srvt_piutang p
				LEFT JOIN glbm_customer c ON c.nomor = p.nopelanggan
			WHERE
				p.batal = false
				AND to_char(p.tanggal, 'YYYYMMDD') >= '" . date('Ymd', strtotime($tglawal)) . "'
				AND to_char(p.tanggal, 'YYYYMMDD') <= '" . date('Ymd', strtotime($tglakhir)) . "'
				AND p.kodecabang = '" . $cabang . "'
			GROUP BY
				p.nopelanggan,
				p.kodecabang,
				c.nama,
				c.nama_toko
			UNION ALL
			SELECT
				p.nopelanggan,
				p.kodecabang,
				c.nama AS namapelanggan,
				c.nama_toko,
				0 AS saldoawal,
				0 AS saldopengurangan,
				0 AS nilaipiutang,
				sum(a.nilaipembulatan) AS nilaipembulatan,
				sum(a.terbayar) AS terbayar
			FROM
				srvt_piutang p
				LEFT JOIN srvt_invoice inv ON inv.nomor :: text = p.nomor :: text
				LEFT JOIN srvt_salesorder so ON so.nomor = inv.nomorso
				LEFT JOIN (
					SELECT
						pud.noreferensi,
						pud.nomorpenerimaan,
						pud.nocustomer,
						sum(pud.nilaipenerimaan + pud.nilaialokasi) AS terbayar,
						sum(pud.nilaipembulatan) AS nilaipembulatan
					FROM
						trnt_penerimaanuangdetail pud
					GROUP BY
						pud.noreferensi,
						pud.nomorpenerimaan,
						pud.nocustomer
				) a ON a.noreferensi :: text = inv.nomorso :: text
				LEFT JOIN trnt_penerimaanuang pu ON pu.nomor :: text = a.nomorpenerimaan :: text
				LEFT JOIN glbm_customer c ON c.nomor :: text = p.nopelanggan :: text
				LEFT JOIN glbm_cabang cb ON cb.kode :: text = p.kodecabang :: text
			WHERE
				inv.batal = false
				AND p.batal = false
				AND pu.batal = false
				AND to_char(pu.tanggal, 'YYYYMMDD') >= '" . date('Ymd', strtotime($tglawal)) . "'
				AND to_char(pu.tanggal, 'YYYYMMDD') <= '" . date('Ymd', strtotime($tglakhir)) . "'
				AND p.kodecabang = '" . $cabang . "'
			GROUP BY
				p.kodecabang,
				cb.nama,
				p.nopelanggan,
				c.nama,
				c.nama_toko
			UNION ALL
			SELECT
				p.nopelanggan,
				p.kodecabang,
				c.nama AS namapelanggan,
				c.nama_toko,
				0 AS saldoawal,
				0 AS saldopengurangan,
				0 AS nilaipiutang,
				sum(a.nilaipembulatan) AS nilaipembulatan,
				sum(a.terbayar) AS terbayar
			FROM
				srvt_piutang p
				LEFT JOIN srvt_invoice inv ON inv.nomor :: text = p.nomor :: text
				LEFT JOIN srvt_salesorder so ON so.nomor = inv.nomorso
				LEFT JOIN (
					SELECT
						pud.noreferensi,
						pud.nomorpenerimaan,
						pud.nocustomer,
						sum(pud.nilaipenerimaan + pud.nilaialokasi) AS terbayar,
						sum(pud.nilaipembulatan) AS nilaipembulatan
					FROM
						trnt_penerimaanuangdetail pud
					GROUP BY
						pud.noreferensi,
						pud.nomorpenerimaan,
						pud.nocustomer
				) a ON a.noreferensi :: text = p.nomor :: text
				LEFT JOIN trnt_penerimaanuang pu ON pu.nomor :: text = a.nomorpenerimaan :: text
				LEFT JOIN glbm_customer c ON c.nomor :: text = p.nopelanggan :: text
				LEFT JOIN glbm_cabang cb ON cb.kode :: text = p.kodecabang :: text
			WHERE
				p.batal = false
				AND pu.batal = false
				AND to_char(pu.tanggal, 'YYYYMMDD') >= '" . date('Ymd', strtotime($tglawal)) . "'
				AND to_char(pu.tanggal, 'YYYYMMDD') <= '" . date('Ymd', strtotime($tglakhir)) . "'
				AND p.kodecabang = '" . $cabang . "'
			GROUP BY
				p.kodecabang,
				cb.nama,
				p.nopelanggan,
				c.nama,
				c.nama_toko
		) hasil
	GROUP BY
		hasil.nopelanggan,
		hasil.kodecabang,
		hasil.namapelanggan,
		hasil.nama_toko
	ORDER BY
		hasil.nama_toko")->result();
		// 	$this->db->where("tanggal >=", $tglawal);
		// 	$this->db->where("tanggal <=", $tglakhir);
		// 	$this->db->where("kodecabang", $cabang);

		// 	return $this->db->get("rpt_daftarpiutang")->result();
	}
	#End Of Laporan Daftar Piutang#

	#Laporan Rekap Pembayaran#
	function GetDatRekapPembayaranList($tglawal = "", $tglakhir = "", $cabang = "")
	{
		$this->db->select("kodeaccount");
		$this->db->select("accountalokasi");
		$this->db->select("nilaialokasi");
		$this->db->where("tglbayar >=", $tglawal);
		$this->db->where("tglbayar <=", $tglakhir);
		$this->db->where("kodecabang", $cabang);
		$this->db->group_by("kodeaccount");
		$this->db->group_by("accountalokasi");
		$this->db->group_by("nilaialokasi");

		return $this->db->get("rpt_rekappembayaranunion")->row();
	}

	function GetDatRekapPembayaran($tglawal = "", $tglakhir = "", $cabang = "")
	{
		$this->db->where("tglbayar >=", $tglawal);
		$this->db->where("tglbayar <=", $tglakhir);
		$this->db->where("kodecabang", $cabang);

		return $this->db->get("rpt_rekappembayaranunion")->result();
	}
	function GetDatRekapPembayaranRow($tglawal = "", $tglakhir = "", $cabang = "")
	{
		$this->db->where("tglbayar >=", $tglawal);
		$this->db->where("tglbayar <=", $tglakhir);
		$this->db->where("kodecabang", $cabang);

		return $this->db->get("rpt_rekappembayaranunion")->row();
	}
	#End Of Laporan Rekap Pembayaran#

	#Laporan PembayaranPerPelanggan#
	function GetDatPembayaranPerPelangganList($tglawal = "", $tglakhir = "", $cabang = "", $pelanggan = "")
	{
		$this->db->select("nomor");
		$this->db->select("nocustomer");
		$this->db->select("namacustomer");
		$this->db->select("tanggal");
		$this->db->select("nilaipenerimaan");
		$this->db->select("nilaialokasi");
		$this->db->select("nilaipembulatan");
		$this->db->where("tanggal >=", $tglawal);
		$this->db->where("tanggal <=", $tglakhir);
		$this->db->where("kodecabang", $cabang);
		$this->db->where("nocustomer", $pelanggan);
		$this->db->group_by("nomor");
		$this->db->group_by("nocustomer");
		$this->db->group_by("namacustomer");
		$this->db->group_by("tanggal");
		$this->db->group_by("nilaipenerimaan");
		$this->db->group_by("nilaialokasi");
		$this->db->group_by("nilaipembulatan");
		$this->db->order_by("tanggal", "ASC");
		$this->db->order_by("nomor", "ASC");

		return $this->db->get("rpt_headerpembayaranperpelanggan")->result();
	}

	function GetDatPembayaranPerPelanggan($tglawal = "", $tglakhir = "", $cabang = "", $pelanggan = "")
	{
		$this->db->where("tanggal >=", $tglawal);
		$this->db->where("tanggal <=", $tglakhir);
		$this->db->where("kodecabang", $cabang);
		$this->db->where("nocustomer", $pelanggan);

		return $this->db->get("rpt_pembayaranperpelanggan")->result();
	}
	function GetDatPembayaranPerPelangganRow($tglawal = "", $tglakhir = "", $cabang = "", $pelanggan = "")
	{
		$this->db->where("tanggal >=", $tglawal);
		$this->db->where("tanggal <=", $tglakhir);
		$this->db->where("kodecabang", $cabang);
		$this->db->where("nocustomer", $pelanggan);

		return $this->db->get("rpt_pembayaranperpelanggan")->row();
	}
	#End Of Laporan PembayaranPerPelanggan#

	#Laporan Rincian Pembayaran#
	function GetDatPembayaranList($tglawal = "", $tglakhir = "", $cabang = "")
	{
		$this->db->select("nomor");
		$this->db->where("tanggal >=", $tglawal);
		$this->db->where("tanggal <=", $tglakhir);
		$this->db->where("kodecabang", $cabang);
		$this->db->group_by("nomor");

		return $this->db->get("rpt_pembayaranperpelanggan")->result();
	}

	function GetDatPembayaran($tglawal = "", $tglakhir = "", $cabang = "")
	{
		$this->db->where("tanggal >=", $tglawal);
		$this->db->where("tanggal <=", $tglakhir);
		$this->db->where("kodecabang", $cabang);

		return $this->db->get("rpt_pembayaranperpelanggan")->result();
	}
	function GetDatPembayaranRow($tglawal = "", $tglakhir = "", $cabang = "")
	{
		$this->db->where("tanggal >=", $tglawal);
		$this->db->where("tanggal <=", $tglakhir);
		$this->db->where("kodecabang", $cabang);

		return $this->db->get("rpt_pembayaranperpelanggan")->row();
	}
	#End Of Laporan Rincian Pembayaran#
}
