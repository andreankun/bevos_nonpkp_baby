<?php

class Laporan_model extends CI_Model
{
    function Cabang($cabang = "")
    {
        return $this->db->query("SELECT * FROM glbm_cabang WHERE kode = '" . $cabang . "'")->row();
    }

    function Pelanggan($nomor = "")
    {
        return $this->db->query("SELECT * FROM glbm_customer WHERE nomor = '" . $nomor . "'")->row();
    }

    function gudang($gudang = "")
    {
        return $this->db->query("SELECT * FROM glbm_gudang WHERE kode = '" . $gudang . "'")->row();
    }

    function Konfigurasi()
    {
        return $this->db->query("SELECT * FROM stpm_konfigurasi")->row();
    }

    function GetMerk($merkawal = "")
    {
        return $this->db->query("SELECT *FROM glbm_merk WHERE kode = '" . $merkawal . "'")->row();
    }

    function GetMerkAkhir($merkawal = "")
    {
        return $this->db->query("SELECT *FROM glbm_merk WHERE kode = '" . $merkawal . "'")->row();
    }

    function GetDataPenjualanTerlaris($tglawal = "", $tglakhir = "", $cabang = "", $merkawal = "", $merkakhir = ""){
		return $this->db->query("
		SELECT
		hasil.kodebarang,
		hasil.namabarang,
		hasil.qty,
		hasil.kodemerk,
		hasil.namamerk,
		hasil.hargajual
		FROM(
		SELECT
		id.kodebarang,
		id.namabarang,
		sum(id.qty) as qty,
		b.kodemerk,
		m.nama as namamerk,
		bd.hargajual
		FROM srvt_invoice i
		LEFT JOIN srvt_invoicedetail id on id.nomor = i.nomor
		LEFT JOIN glbm_barang b on b.kode = id.kodebarang
		LEFT JOIN glbm_merk m on m.kode = b.kodemerk
		LEFT JOIN glbm_barangdetail bd on bd.kodebarang = id.kodebarang AND bd.kodecabang = i.kodecabang
		WHERE i.tanggal >= '".date('Y-m-d', strtotime($tglawal))."' AND i.tanggal <= '".date('Y-m-d', strtotime($tglakhir))."' AND i.kodecabang = '".$cabang."' AND b.kodemerk >= '".$merkawal."' AND b.kodemerk <= '".$merkakhir."' AND i.batal = false
		GROUP BY
		id.kodebarang,
		id.namabarang,
		b.kodemerk,
		m.nama,
		bd.hargajual
		)hasil
		ORDER BY
		hasil.qty DESC
		")->result();
	}

    function GetDataPenjualanPerPelanggan($tglawal = "", $tglakhir = "", $nomor = "", $cabang = "")
    {
        $this->db->where("tanggal >=", $tglawal);
        $this->db->where("tanggal <=", $tglakhir);
        $this->db->where("nopelanggan", $nomor);
        $this->db->where("kodecabang", $cabang);

        return $this->db->get("rpt_penjualanperpelanggan")->result();
    }

    function RekapPenjualanPelanggan($tglawal = "", $tglakhir = "", $cabang = "")
    {
        return $this->db->query("SELECT
        hasil.nopelanggan,
        hasil.nama_toko,
        hasil.namapelanggan,
        sum(hasil.subtotal) AS subtotal,
        sum(hasil.discbrgitem) AS discbrgitem,
        sum(hasil.discpaketitem) AS discpaketitem,
        sum(hasil.nilaicash) AS nilaicash,
        sum(hasil.nilaicustomer) AS nilaicustomer,
        sum(hasil.dpp) AS dpp,
        hasil.kodecabang,
        hasil.namacabang
        FROM
        (
            SELECT
                i.nopelanggan,
                c.nama_toko,
                c.nama AS namapelanggan,
                a.subtotal,
                a.discbrgitem,
                a.discpaketitem,
                i.nilaicash,
                i.nilaicustomer,
                i.dpp,
                i.kodecabang,
                cb.nama AS namacabang
            FROM
                srvt_invoice i
                LEFT JOIN (
                    SELECT
                        id.nomor,
                        sum(id.qty * id.harga) AS subtotal,
                        sum(id.discbrgitem) AS discbrgitem,
                        sum(id.discpaketitem) AS discpaketitem
                    FROM
                        srvt_invoicedetail id
                    GROUP BY
                        id.nomor
                ) a ON i.nomor :: text = a.nomor :: text
                LEFT JOIN glbm_cabang cb ON cb.kode :: text = i.kodecabang :: text
                LEFT JOIN glbm_customer c ON c.nomor :: text = i.nopelanggan :: text
            WHERE
                i.batal = false
                AND TO_CHAR(i.tanggal,'YYYYMMDD') >= '" . date('Ymd', strtotime($tglawal)) . "'
                AND TO_CHAR(i.tanggal,'YYYYMMDD') <= '" . date('Ymd', strtotime($tglakhir)) . "'
                AND i.kodecabang = '" . $cabang . "'
            GROUP BY
                i.nopelanggan,
                c.nama_toko,
                c.nama,
                i.dpp,
                a.subtotal,
                a.discbrgitem,
                a.discpaketitem,
                i.nilaicash,
                i.nilaicustomer,
                i.kodecabang,
                cb.nama
            ORDER BY
                i.nopelanggan
        ) hasil
    GROUP BY
        hasil.nopelanggan,
        hasil.nama_toko,
        hasil.namapelanggan,
        hasil.kodecabang,
        hasil.namacabang
    ORDER BY
        hasil.nama_toko;")->result();
    }

    // function RekapPenjualanPelangganRow($tglawal = "", $tglakhir = "", $cabang = "")
    // {
    //     $this->db->where("tanggal >=", $tglawal);
    //     $this->db->where("tanggal <=", $tglakhir);
    //     $this->db->where("kodecabang", $cabang);

    //     return $this->db->get("rpt_rekappenjualanpelanggan")->row();
    // }

    function GetDataReturPenjualanPerTanggal($tglawal = "", $tglakhir = "", $cabang = "")
    {
        $this->db->where("tanggal >=", $tglawal);
        $this->db->where("tanggal <=", $tglakhir);
        $this->db->where("kodecabang", $cabang);

        return $this->db->get("rpt_returpertanggal")->result();
    }

    function GetDataReturPenjualanPerTanggalList($tglawal = "", $tglakhir = "", $cabang = "")
    {
        $this->db->select("tanggal");
        $this->db->where("tanggal >=", $tglawal);
        $this->db->where("tanggal <=", $tglakhir);
        $this->db->where("kodecabang", $cabang);
        $this->db->group_by("tanggal");

        return $this->db->get("rpt_returpertanggal")->result();
    }

    function GetDataPenjualanPerPelangganRow($tglawal = "", $tglakhir = "", $nomor = "", $cabang = "")
    {
        $this->db->where("tanggal >=", $tglawal);
        $this->db->where("tanggal <=", $tglakhir);
        $this->db->where("nopelanggan", $nomor);
        $this->db->where("kodecabang", $cabang);

        return $this->db->get("rpt_penjualanperpelanggan")->row();
    }

    function GetDataPenjualanPerPelangganList($tglawal = "", $tglakhir = "", $nomor = "", $cabang = "")
    {
        $this->db->select("tanggal");
        $this->db->where("tanggal >=", $tglawal);
        $this->db->where("tanggal <=", $tglakhir);
        $this->db->where("nopelanggan", $nomor);
        $this->db->where("kodecabang", $cabang);
        $this->db->group_by("tanggal");

        return $this->db->get("rpt_penjualanperpelanggan")->result();
    }

    function GetDataSalesOrder($tglawal = "", $tglakhir = "", $cabang = "")
    {
        $this->db->where("tanggal >=", $tglawal);
        $this->db->where("tanggal <=", $tglakhir);
        $this->db->where("kodecabang", $cabang);

        return $this->db->get("rpt_salesorder")->result();
    }

    function GetDataSalesOrderHeader($tglawal = "", $tglakhir = "", $cabang = "")
    {
        $this->db->where("tanggal >=", $tglawal);
        $this->db->where("tanggal <=", $tglakhir);
        $this->db->where("kodecabang", $cabang);

        return $this->db->get("rpt_salesorderdetail1")->result();
    }

    function GetDataSalesOrderList($tglawal = "", $tglakhir = "", $cabang = "")
    {
        $this->db->select("nomor");
        $this->db->where("tanggal >=", $tglawal);
        $this->db->where("tanggal <=", $tglakhir);
        $this->db->where("kodecabang", $cabang);
        $this->db->group_by("nomor");

        return $this->db->get("rpt_salesorderdetail2")->result();
    }

    function GetDataSalesOrderDetail($tglawal = "", $tglakhir = "", $cabang = "")
    {
        $this->db->where("tanggal >=", $tglawal);
        $this->db->where("tanggal <=", $tglakhir);
        $this->db->where("kodecabang", $cabang);

        return $this->db->get("rpt_salesorderdetail2")->result();
    }
    function GetDataReturHeader($tglawal = "", $tglakhir = "", $cabang = "")
    {
        $this->db->where("tanggal >=", $tglawal);
        $this->db->where("tanggal <=", $tglakhir);
        $this->db->where("kodecabang", $cabang);

        return $this->db->get("rpt_returdetail1")->result();
    }

    function GetDataReturList($tglawal = "", $tglakhir = "", $cabang = "")
    {
        $this->db->select("nomor");
        $this->db->where("tanggal >=", $tglawal);
        $this->db->where("tanggal <=", $tglakhir);
        $this->db->where("kodecabang", $cabang);
        $this->db->group_by("nomor");

        return $this->db->get("rpt_returdetail2")->result();
    }

    function GetDataReturDetail($tglawal = "", $tglakhir = "", $cabang = "")
    {
        $this->db->where("tanggal >=", $tglawal);
        $this->db->where("tanggal <=", $tglakhir);
        $this->db->where("kodecabang", $cabang);

        return $this->db->get("rpt_returdetail2")->result();
    }

    function GetPenjualanDetailPerPelangganHeader($tglawal = "", $tglakhir = "", $cabang = "", $pelanggan = "")
    {
        $this->db->where("tanggal >=", $tglawal);
        $this->db->where("tanggal <=", $tglakhir);
        $this->db->where("kodecabang", $cabang);
        $this->db->where("nopelanggan", $pelanggan);

        return $this->db->get("rpt_penjualandetailperpelanggan1")->result();
    }

    function GetPenjualanDetailPerPelangganList($tglawal = "", $tglakhir = "", $cabang = "", $pelanggan = "")
    {
        $this->db->select("nomor");
        $this->db->where("tanggal >=", $tglawal);
        $this->db->where("tanggal <=", $tglakhir);
        $this->db->where("kodecabang", $cabang);
        $this->db->where("nopelanggan", $pelanggan);
        $this->db->group_by("nomor");

        return $this->db->get("rpt_penjualandetailperpelanggan2")->result();
    }

    function GetPenjualanDetailPerPelangganDetail($tglawal = "", $tglakhir = "", $cabang = "", $pelanggan = "")
    {
        $this->db->where("tanggal >=", $tglawal);
        $this->db->where("tanggal <=", $tglakhir);
        $this->db->where("kodecabang", $cabang);
        $this->db->where("nopelanggan", $pelanggan);

        return $this->db->get("rpt_penjualandetailperpelanggan2")->result();
    }

    function GetPenjualanPromoHeader($tglawal = "", $tglakhir = "", $cabang = "")
    {
        $this->db->where("tanggal >=", $tglawal);
        $this->db->where("tanggal <=", $tglakhir);
        $this->db->where("kodecabang", $cabang);

        return $this->db->get("rpt_penjualanpromo1")->result();
    }

    function GetPenjualanPromoList($tglawal = "", $tglakhir = "", $cabang = "")
    {
        $this->db->select("nomor");
        $this->db->where("tanggal >=", $tglawal);
        $this->db->where("tanggal <=", $tglakhir);
        $this->db->where("kodecabang", $cabang);
        $this->db->group_by("nomor");

        return $this->db->get("rpt_penjualanpromo2")->result();
    }

    function GetPenjualanPromoDetail($tglawal = "", $tglakhir = "", $cabang = "")
    {
        $this->db->where("tanggal >=", $tglawal);
        $this->db->where("tanggal <=", $tglakhir);
        $this->db->where("kodecabang", $cabang);

        return $this->db->get("rpt_penjualanpromo2")->result();
    }

    function PenjualanSemuaSalesman($tglawal = "", $tglakhir = "", $cabang = "")
    {
        $this->db->where("tanggal >=", $tglawal);
        $this->db->where("tanggal <=", $tglakhir);
        $this->db->where("kodecabang", $cabang);

        return $this->db->get("rpt_penjualansemuasales")->result();
    }

    function PenjualanSemuaSalesmanList($tglawal = "", $tglakhir = "", $cabang = "")
    {
        $this->db->select("kodesalesman");
        $this->db->select("namasalesman");
        $this->db->where("tanggal >=", $tglawal);
        $this->db->where("tanggal <=", $tglakhir);
        $this->db->where("kodecabang", $cabang);
        $this->db->group_by("kodesalesman");
        $this->db->group_by("namasalesman");

        return $this->db->get("rpt_penjualansemuasales")->result();
    }

    function PenjualanPerSalesman($tglawal = "", $tglakhir = "", $cabang = "", $salesman = "")
    {
        $this->db->where("tanggal >=", $tglawal);
        $this->db->where("tanggal <=", $tglakhir);
        $this->db->where("kodecabang", $cabang);
        $this->db->where("kodesalesman", $salesman);

        return $this->db->get("rpt_penjualanpersales")->result();
    }

    function PenjualanPerSalesmanRow($tglawal = "", $tglakhir = "", $cabang = "", $salesman = "")
    {
        $this->db->where("tanggal >=", $tglawal);
        $this->db->where("tanggal <=", $tglakhir);
        $this->db->where("kodecabang", $cabang);
        $this->db->where("kodesalesman", $salesman);

        return $this->db->get("rpt_penjualanpersales")->row();
    }

    function PenjualanPerSalesmanList($tglawal = "", $tglakhir = "", $cabang = "", $salesman = "")
    {
        $this->db->select("kodesalesman");
        $this->db->select("namasalesman");
        $this->db->where("tanggal >=", $tglawal);
        $this->db->where("tanggal <=", $tglakhir);
        $this->db->where("kodecabang", $cabang);
        $this->db->where("kodesalesman", $salesman);
        $this->db->group_by("kodesalesman");
        $this->db->group_by("namasalesman");

        return $this->db->get("rpt_penjualanpersales")->result();
    }

    function RekapPenjualanPerPelanggan($tglawal = "", $tglakhir = "",  $nopelanggan = "", $cabang = "")
    {
        $this->db->where("tanggal >=", $tglawal);
        $this->db->where("tanggal <=", $tglakhir);
        $this->db->where("kodecabang", $cabang);
        $this->db->where("nopelanggan", $nopelanggan);
        $this->db->order_by("nomor", "asc");
        return $this->db->get("rpt_rekappenjualanperpelanggan")->result();
    }

    function RekapPenjualanPerPelangganRow($tglawal = "", $tglakhir = "",  $nopelanggan = "", $cabang = "")
    {
        $this->db->where("tanggal >=", $tglawal);
        $this->db->where("tanggal <=", $tglakhir);
        $this->db->where("kodecabang", $cabang);
        $this->db->where("nopelanggan", $nopelanggan);

        return $this->db->get("rpt_rekappenjualanperpelanggan")->row();
    }

    function RekapPenjualanPerPelangganList($tglawal = "", $tglakhir = "",  $nopelanggan = "", $cabang = "")
    {
        $this->db->select("nomor");
        $this->db->where("tanggal >=", $tglawal);
        $this->db->where("tanggal <=", $tglakhir);
        $this->db->where("kodecabang", $cabang);
        $this->db->where("nopelanggan", $nopelanggan);
        $this->db->group_by("nomor");

        return $this->db->get("rpt_rekappenjualanperpelanggan")->result();
    }

    function RekapPenjualanPerPelangganListPdf($tglawal = "", $tglakhir = "",  $nopelanggan = "", $cabang = "")
    {
        $this->db->select("jenisjual");
        $this->db->where("tanggal >=", $tglawal);
        $this->db->where("tanggal <=", $tglakhir);
        $this->db->where("kodecabang", $cabang);
        $this->db->where("nopelanggan", $nopelanggan);
        $this->db->group_by("jenisjual");

        return $this->db->get("rpt_rekappenjualanperpelanggan")->result();
    }

    function GetPenjualanSemuaPelanggan($tglawal = "", $tglakhir = "", $cabang = "")
    {
        $this->db->where("tanggal >=", $tglawal);
        $this->db->where("tanggal <=", $tglakhir);
        $this->db->where("kodecabang", $cabang);
        $this->db->order_by('nama_toko', 'asc');
        $this->db->order_by('namapelanggan', 'asc');

        return $this->db->get("rpt_rekappenjualansemuapelanggan")->result();
    }

    function GetRekapRetur($tglawal = "", $tglakhir = "", $cabang = "")
    {
        $this->db->where("tanggal >=", $tglawal);
        $this->db->where("tanggal <=", $tglakhir);
        $this->db->where("kodecabang", $cabang);

        return $this->db->get("rpt_rekapretur")->result();
    }

    // function GetPenjualanSemuaPelangganList($tglawal = "", $tglakhir = "", $cabang = "")
    // {
    //     $this->db->select("nomor");
    //     $this->db->where("tanggal >=", $tglawal);
    //     $this->db->where("tanggal <=", $tglakhir);
    //     $this->db->where("kodecabang", $cabang);
    //     $this->db->group_by("nomor");

    //     return $this->db->get("rpt_rekappenjualansemuapelanggan2")->result();
    // }

    // function GetPenjualanSemuaPelangganDetail($tglawal = "", $tglakhir = "", $cabang = "")
    // {
    //     $this->db->where("tanggal >=", $tglawal);
    //     $this->db->where("tanggal <=", $tglakhir);
    //     $this->db->where("kodecabang", $cabang);

    //     return $this->db->get("rpt_rekappenjualansemuapelanggan2")->result();
    // }

    function GetRekapPenjualanPerGudang($tglawal = "", $tglakhir = "", $gudang = "")
    {
        $this->db->where("tanggal >=", $tglawal);
        $this->db->where("tanggal <=", $tglakhir);
        $this->db->where("lokasi", $gudang);
        $this->db->order_by('nomorso', 'asc');

        return $this->db->get("rpt_rekappenjualanpergudang")->result();
    }

    function GetDataPenjualanPerPelangganPerBarang($tglawal = "", $tglakhir = "", $nomor = "", $cabang = "", $kodebarang = "")
    {
        $this->db->select('tanggal');
        $this->db->where("tanggal >=", $tglawal);
        $this->db->where("tanggal <=", $tglakhir);
        $this->db->where("nopelanggan", $nomor);
        $this->db->where("kodecabang", $cabang);
        $this->db->where("kodebarang", $kodebarang);
        $this->db->group_by('tanggal');

        return $this->db->get("rpt_penjualanperpelangganperbarang")->result();
    }

    function GetDataPenjualanPerPelangganPerBarangDetail($tglawal = "", $tglakhir = "", $nomor = "", $cabang = "", $kodebarang = "")
    {
        $this->db->where("tanggal >=", $tglawal);
        $this->db->where("tanggal <=", $tglakhir);
        $this->db->where("nopelanggan", $nomor);
        $this->db->where("kodecabang", $cabang);
        $this->db->where("kodebarang", $kodebarang);
        $this->db->order_by('nomor', 'asc');

        return $this->db->get("rpt_penjualanperpelangganperbarang")->result();
    }

    function GetDataPenjualanPerPelangganPerBarangRow($tglawal = "", $tglakhir = "", $nomor = "", $cabang = "", $kodebarang = "")
    {
        $this->db->where("tanggal >=", $tglawal);
        $this->db->where("tanggal <=", $tglakhir);
        $this->db->where("nopelanggan", $nomor);
        $this->db->where("kodecabang", $cabang);
        $this->db->where("kodebarang", $kodebarang);
        $this->db->order_by('nomor', 'asc');

        return $this->db->get("rpt_penjualanperpelangganperbarang")->row();
    }

    #Laporan Komisi Sales NETT#
    function GetDataKomisiSalesNettList($tglawal = "", $tglakhir = "", $cabang = "", $sales = "")
    {
        $this->db->select("nopelanggan");
        $this->db->where("tglbayar >=", $tglawal);
        $this->db->where("tglbayar <=", $tglakhir);
        $this->db->where("kodecabangbd", $cabang);
        $this->db->where("kodecabanginv", $cabang);
        $this->db->where("kodesalesman", $sales);
        $this->db->group_by("nopelanggan");

        return $this->db->get("rpt_komisisalesnett")->result();
    }

    function GetDataKomisiSalesNett($tglawal = "", $tglakhir = "", $cabang = "", $sales = "")
    {
        $this->db->where("tglbayar >=", $tglawal);
        $this->db->where("tglbayar <=", $tglakhir);
        $this->db->where("kodecabangbd", $cabang);
        $this->db->where("kodecabanginv", $cabang);
        // $this->db->where("kodecabang", $cabang);
        $this->db->where("kodesalesman", $sales);

        return $this->db->get("rpt_komisisalesnett")->result();
    }
    function GetDataKomisiSalesNettRow($tglawal = "", $tglakhir = "", $cabang = "", $sales = "")
    {
        $this->db->where("tglbayar >=", $tglawal);
        $this->db->where("tglbayar <=", $tglakhir);
        $this->db->where("kodecabangbd", $cabang);
        $this->db->where("kodecabanginv", $cabang);
        // $this->db->where("kodecabang", $cabang);
        $this->db->where("kodesalesman", $sales);

        return $this->db->get("rpt_komisisalesnett")->row();
    }
    #End Of Laporan Komisi Sales NETT#

    #Laporan Komisi Sales Proyeksi#
    function GetDataKomisiSalesProyeksiList($tglawal = "", $tglakhir = "", $cabang = "", $sales = "")
    {
        $this->db->select("nopelanggan");
        $this->db->where("tanggal >=", $tglawal);
        $this->db->where("tanggal <=", $tglakhir);
        $this->db->where("kodecabang", $cabang);
        $this->db->where("kodesalesman", $sales);
        $this->db->group_by("nopelanggan");

        return $this->db->get("rpt_komisiproyeksisales")->result();
    }

    function GetDataKomisiSalesProyeksi($tglawal = "", $tglakhir = "", $cabang = "", $sales = "")
    {
        $this->db->where("tanggal >=", $tglawal);
        $this->db->where("tanggal <=", $tglakhir);
        $this->db->where("kodecabang", $cabang);
        $this->db->where("kodesalesman", $sales);

        return $this->db->get("rpt_komisiproyeksisales")->result();
    }
    function GetDataKomisiSalesProyeksiRow($tglawal = "", $tglakhir = "", $cabang = "", $sales = "")
    {
        $this->db->where("tanggal >=", $tglawal);
        $this->db->where("tanggal <=", $tglakhir);
        $this->db->where("kodecabang", $cabang);
        $this->db->where("kodesalesman", $sales);

        return $this->db->get("rpt_komisiproyeksisales")->row();
    }
    #End Of Laporan Komisi Sales Proyeksi#

    #Laporan Komisi Sales Hangus#
    function GetDataKomisiSalesHangusList($tglawal = "", $tglakhir = "", $cabang = "", $sales = "")
    {
        $this->db->select("nopelanggan");
        $this->db->where("tanggal >=", $tglawal);
        $this->db->where("tanggal <=", $tglakhir);
        $this->db->where("kodecabanginv", $cabang);
        $this->db->where("kodecabangbd", $cabang);
        $this->db->where("kodesalesman", $sales);
        $this->db->group_by("nopelanggan");

        return $this->db->get("rpt_komisisaleshangus")->result();
    }

    function GetDataKomisiSalesHangus($tglawal = "", $tglakhir = "", $cabang = "", $sales = "")
    {
        $this->db->where("tanggal >=", $tglawal);
        $this->db->where("tanggal <=", $tglakhir);
        $this->db->where("kodecabanginv", $cabang);
        $this->db->where("kodecabangbd", $cabang);
        $this->db->where("kodesalesman", $sales);

        return $this->db->get("rpt_komisisaleshangus")->result();
    }
    function GetDataKomisiSalesHangusRow($tglawal = "", $tglakhir = "", $cabang = "", $sales = "")
    {
        $this->db->where("tanggal >=", $tglawal);
        $this->db->where("tanggal <=", $tglakhir);
        $this->db->where("kodecabanginv", $cabang);
        $this->db->where("kodecabangbd", $cabang);
        $this->db->where("kodesalesman", $sales);

        return $this->db->get("rpt_komisisaleshangus")->row();
    }
    #End Of Laporan Komisi Sales Hangus#

    function LaporanPenjualanDetail($tglawal = "", $tglakhir = "", $cabang = "")
    {
        $this->db->where("tanggal >=", $tglawal);
        $this->db->where("tanggal <=", $tglakhir);
        $this->db->where("kodecabang", $cabang);

        return $this->db->get("rpt_penjualandetail")->result();
    }

    function LaporanPenjualanDetailRow($tglawal = "", $tglakhir = "", $cabang = "")
    {
        $this->db->where("tanggal >=", $tglawal);
        $this->db->where("tanggal <=", $tglakhir);
        $this->db->where("kodecabang", $cabang);

        return $this->db->get("rpt_penjualandetailrow")->result();
    }

    function LaporanPenjualanDetailList($tglawal = "", $tglakhir = "", $cabang = "")
    {
        $this->db->select("nomor");
        $this->db->where("tanggal >=", $tglawal);
        $this->db->where("tanggal <=", $tglakhir);
        $this->db->where("kodecabang", $cabang);
        $this->db->group_by("nomor");

        return $this->db->get("rpt_penjualandetail")->result();
    }

    function LaporanPenjualanDetailAllCabang($tglawal = "", $tglakhir = "")
    {
        $this->db->where("tanggal >=", $tglawal);
        $this->db->where("tanggal <=", $tglakhir);
        // $this->db->where("kodecabang", $cabang);

        return $this->db->get("rpt_penjualandetail")->result();
    }

    function LaporanPenjualanDetailAllCabangRow($tglawal = "", $tglakhir = "")
    {
        $this->db->where("tanggal >=", $tglawal);
        $this->db->where("tanggal <=", $tglakhir);
        // $this->db->where("kodecabang", $cabang);

        return $this->db->get("rpt_penjualandetailrow")->result();
    }

    function LaporanPenjualanDetailAllCabangList($tglawal = "", $tglakhir = "")
    {
        $this->db->select("nomor");
        $this->db->where("tanggal >=", $tglawal);
        $this->db->where("tanggal <=", $tglakhir);
        // $this->db->where("kodecabang", $cabang);
        $this->db->group_by("nomor");

        return $this->db->get("rpt_penjualandetail")->result();
    }

    function LaporanRekapPenjualan($tglawal = "", $tglakhir = "", $cabang = "")
    {
        $this->db->select("jenisjual");
        $this->db->where("tanggal >=", $tglawal);
        $this->db->where("tanggal <=", $tglakhir);
        $this->db->where("kodecabang", $cabang);
        $this->db->group_by("jenisjual");

        return $this->db->get("rpt_rekappenjualan")->result();
    }

    function LaporanRekapPenjualanList($tglawal = "", $tglakhir = "", $cabang = "")
    {
        $this->db->where("tanggal >=", $tglawal);
        $this->db->where("tanggal <=", $tglakhir);
        $this->db->where("kodecabang", $cabang);
        $this->db->order_by("nomor", "ASC");

        return $this->db->get("rpt_rekappenjualan")->result();
    }

    function LaporanRekapPenjualanAllCabang($tglawal = "", $tglakhir = "")
    {
        $this->db->select("jenisjual");
        $this->db->where("tanggal >=", $tglawal);
        $this->db->where("tanggal <=", $tglakhir);
        // $this->db->where("kodecabang", $cabang);
        $this->db->group_by("jenisjual");

        return $this->db->get("rpt_rekappenjualan")->result();
    }

    function LaporanRekapPenjualanAllCabangList($tglawal = "", $tglakhir = "")
    {
        $this->db->where("tanggal >=", $tglawal);
        $this->db->where("tanggal <=", $tglakhir);
        // $this->db->where("kodecabang", $cabang);
        $this->db->order_by("nomor", "ASC");

        return $this->db->get("rpt_rekappenjualan")->result();
    }

    function LaporanPenjualanPertanggal($tglawal = "", $tglakhir, $cabang = "")
    {
        $this->db->select("tanggal");
        $this->db->where("tanggal >=", $tglawal);
        $this->db->where("tanggal <=", $tglakhir);
        $this->db->where("kodecabang", $cabang);
        $this->db->group_by("tanggal");

        return $this->db->get("rpt_laporanpenjualanpertanggal")->result();
    }

    function LaporanPenjualanPertanggalList($tglawal = "", $tglakhir, $cabang = "")
    {
        $this->db->where("tanggal >=", $tglawal);
        $this->db->where("tanggal <=", $tglakhir);
        $this->db->where("kodecabang", $cabang);

        return $this->db->get("rpt_laporanpenjualanpertanggal")->result();
    }

    function LaporanPenjualanPerkategori($tglawal = "", $tglakhir = "", $cabang = "", $merkawal = "", $merkakhir)
    {
        $this->db->select("kodemerk");
        $this->db->select("namamerk");
        $this->db->where("tanggal >=", $tglawal);
        $this->db->where("tanggal <=", $tglakhir);
        $this->db->where("kodecabang", $cabang);
        $this->db->where("kodemerk >=", $merkawal);
        $this->db->where("kodemerk <=", $merkakhir);
        $this->db->group_by("kodemerk");
        $this->db->group_by("namamerk");

        return $this->db->get("rpt_penjualaperkategori")->result();
    }

    function LaporanPenjualanPerkategoriList($tglawal = "", $tglakhir = "", $cabang = "", $merkawal = "", $merkakhir)
    {
        $this->db->where("tanggal >=", $tglawal);
        $this->db->where("tanggal <=", $tglakhir);
        $this->db->where("kodecabang", $cabang);
        $this->db->where("kodemerk >=", $merkawal);
        $this->db->where("kodemerk <=", $merkakhir);

        return $this->db->get("rpt_penjualaperkategori")->result();
    }
    #End Of Laporan Komisi Sales NETT#
    #Laporan Komisi Sales#
    function GetDataKomisiSales($tglawal = "", $tglakhir = "", $cabang = "", $sales = "")
    {
        $this->db->where("tglbayar >=", $tglawal);
        $this->db->where("tglbayar <=", $tglakhir);
        $this->db->where("kodecabang", $cabang);
        // $this->db->where("kodecbgbarang", $cabang);
        $this->db->where("kodesalesman", $sales);

        return $this->db->get("rpt_komisisales")->result();
    }

    function GetDataKomisiSalesRow($tglawal = "", $tglakhir = "", $cabang = "", $sales = "")
    {
        $this->db->where("tglbayar >=", $tglawal);
        $this->db->where("tglbayar <=", $tglakhir);
        $this->db->where("kodecabang", $cabang);
        // $this->db->where("kodecbgbarang", $cabang);
        $this->db->where("kodesalesman", $sales);

        return $this->db->get("rpt_komisisales")->row();
    }
    #End Of Komisi Sales#

    function GetOutstandingSalesOrder($cabang)
    {
        if (empty($cabang)) {
            return $this->db->query("SELECT
            so.*,
            c.nama AS namacabang
            FROM srvt_salesorder so
            LEFT JOIN glbm_cabang c ON c.kode = so.kodecabang
            WHERE so.batal = false AND so.kodestatus < 2")->result();
        } else {
            return $this->db->query("SELECT
            so.*,
            c.nama AS namacabang
            FROM srvt_salesorder so
            LEFT JOIN glbm_cabang c ON c.kode = so.kodecabang
            WHERE so.batal = false AND so.kodestatus < 2 AND so.kodecabang = '" . $cabang . "'")->result();
        }
    }
}
