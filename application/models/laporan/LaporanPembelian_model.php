<?php

class LaporanPembelian_model extends CI_Model
{

	function Cabang ($cabang = ""){
		return $this->db->query("SELECT *FROM glbm_cabang WHERE kode = '".$cabang."'")->row();
	}

	function Gudang ($gudang = ""){
		return $this->db->query("SELECT *FROM glbm_gudang WHERE kode = '".$gudang."'")->row();
	}

	function LaporanPenerimaanBarangPergudang($tglawal = "", $tglakhir = "", $gudang = ""){
		$this->db->where("tanggal >=", date('Y-m-d', strtotime($tglawal)));
		$this->db->where("tanggal <=", date('Y-m-d', strtotime($tglakhir)));
		$this->db->where("kodegudang", $gudang);

		return $this->db->get("rpt_penerimaanbarangunioin")->result();
	}

	function LaporanPenerimaanBarangPergudangRow($tglawal = "", $tglakhir = "", $gudang = ""){
		$this->db->where("tanggal >=", date('Y-m-d', strtotime($tglawal)));
		$this->db->where("tanggal <=", date('Y-m-d', strtotime($tglakhir)));
		$this->db->where("kodegudang", $gudang);

		return $this->db->get("rpt_penerimaanbarangunioin")->result();
	}

	function LaporanPenerimaanBarangPergudangList($tglawal = "", $tglakhir = "", $gudang = ""){
		$this->db->select("nomorpo");
		$this->db->select("nomor");
		$this->db->select("tanggal");
		$this->db->select("namasupplier");
		$this->db->where("tanggal >=", date('Y-m-d', strtotime($tglawal)));
		$this->db->where("tanggal <=", date('Y-m-d', strtotime($tglakhir)));
		$this->db->where("kodegudang", $gudang);
		$this->db->group_by("nomor");
		$this->db->group_by("nomorpo");
		$this->db->group_by("tanggal");
		$this->db->group_by("namasupplier");
		$this->db->order_by("tanggal");

		return $this->db->get("rpt_penerimaanbarangunioin")->result();
	}

	function LaporanPenerimaanBarangAll($tglawal = "", $tglakhir = "", $gudang = ""){
		$this->db->where("tanggal >=", date('Y-m-d', strtotime($tglawal)));
		$this->db->where("tanggal <=", date('Y-m-d', strtotime($tglakhir)));
		$this->db->where("kodecabang", $gudang);

		return $this->db->get("rpt_penerimaanbarangunioin")->result();
	}

	function LaporanPenerimaanBarangAllRow($tglawal = "", $tglakhir = "", $gudang = ""){
		$this->db->where("tanggal >=", date('Y-m-d', strtotime($tglawal)));
		$this->db->where("tanggal <=", date('Y-m-d', strtotime($tglakhir)));
		$this->db->where("kodecabang", $gudang);

		return $this->db->get("rpt_penerimaanbarangunioin")->result();
	}

	function LaporanPenerimaanBarangAllList($tglawal = "", $tglakhir = "", $gudang = ""){
		$this->db->select("nomorpo");
		$this->db->select("nomor");
		$this->db->select("tanggal");
		$this->db->select("namasupplier");
		$this->db->where("tanggal >=", date('Y-m-d', strtotime($tglawal)));
		$this->db->where("tanggal <=", date('Y-m-d', strtotime($tglakhir)));
		$this->db->where("kodecabang", $gudang);
		$this->db->group_by("nomor");
		$this->db->group_by("nomorpo");
		$this->db->group_by("tanggal");
		$this->db->group_by("namasupplier");
		$this->db->order_by("tanggal");

		return $this->db->get("rpt_penerimaanbarangunioin")->result();
	}

	function LaporanImportList($tglawal = "", $tglakhir = "", $cabang = ""){
		$this->db->select("nomor");
		$this->db->where("tanggal >=", date('Y-m-d', strtotime($tglawal)));
		$this->db->where("tanggal <=", date('Y-m-d', strtotime($tglakhir)));
		$this->db->where("kodecbasal", $cabang);
		$this->db->group_by("nomor");

		return $this->db->get("rpt_baranginmportlist")->result();
	}

	function LaporanImportRow($tglawal = "", $tglakhir = "", $cabang = ""){
		$this->db->where("tanggal >=", date('Y-m-d', strtotime($tglawal)));
		$this->db->where("tanggal <=", date('Y-m-d', strtotime($tglakhir)));
		$this->db->where("kodecbasal", $cabang);

		return $this->db->get("rpt_baranginmport")->result();
	}

	function LaporanImport($tglawal = "", $tglakhir = "", $cabang = ""){
		$this->db->where("tanggal >=", date('Y-m-d', strtotime($tglawal)));
		$this->db->where("tanggal <=", date('Y-m-d', strtotime($tglakhir)));
		$this->db->where("kodecbasal", $cabang);

		return $this->db->get("rpt_baranginmportlist")->result();
	}
}
