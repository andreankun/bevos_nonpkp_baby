<?php

class FakturPajak_model extends CI_Model
{
    public function GetMaxNomor($nomor = "")
    {
        $this->db->select_max('nomor');
        $this->db->where("left(nomor,0)", $nomor);
        return $this->db->get("srvt_fakturpajak")->row();
    }

    function SaveData($data = "")
    {
        return $this->db->insert('srvt_fakturpajak', $data);
    }

    function DataFakturPajak($nomor = "")
    {
        return $this->db->query("SELECT * FROM cari_fakturpajak WHERE nomor = '" . $nomor . "' AND batal = false")->result();
    }

    function UpdateDataFakturPajak($nomor = "", $data = "")
    {
        $this->db->where('nomor', $nomor);
        return $this->db->update('srvt_registrasinomorfakturpajak', $data);
    }

    function UpdateDataFaktur($nomor = "", $data = "")
    {
        $this->db->where('nomor', $nomor);
        return $this->db->update('srvt_invoice', $data);
    }
}
