<?php

class PencatatanOngkir_model extends CI_Model
{
	public function GetMaxNomor($nomor = "")
	{
		$this->db->select('nomor');
		$this->db->where("SUBSTRING(nomor,1,3)", $nomor);
		$this->db->order_by("SUBSTRING(nomor,6,13) DESC");
		$this->db->limit(1);		
		return $this->db->get("srvt_piutang")->row();
	}

    function SaveData($data = "")
    {
        return $this->db->insert('srvt_ongkir', $data);
    }

	function DataCustomer($nomor = ""){
		return $this->db->query("SELECT *FROM cari_customer1 WHERE nomor = '".$nomor."'")->row();
	}

	function SaveDataAR($data = "")
	{
		return $this->db->insert('srvt_piutang', $data);
	}

	function SaveDataSJ($data = ""){
		return $this->db->insert('srvt_suratjalan', $data);
	}

    function DataPencatatanOngkir($nomor = "")
    {
        return $this->db->query("SELECT * FROM cari_ongkir WHERE nomor = '" . $nomor . "' AND batal = false")->result();
    }

    function UpdateData($nomor = "", $data = "")
    {
        $this->db->where('nomor', $nomor);
        return $this->db->update('srvt_ongkir', $data);
    }

    function UpdatePiutang($nomor = "", $data = "")
    {
        $this->db->where('nomor', $nomor);
        return $this->db->update('srvt_piutang', $data);
    }
}
