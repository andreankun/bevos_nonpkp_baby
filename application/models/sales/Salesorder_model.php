<?php

class Salesorder_model extends CI_Model
{
	public function GetMaxNomor($nomor = "")
	{
		$this->db->select_max('nomor');
		$this->db->where("left(nomor,10)", $nomor);
		return $this->db->get("srvt_salesorder")->row();
	}

	function CekSO($nomor = "")
	{
		return $this->db->query("SELECT * FROM srvt_salesorder  WHERE nomor = '" . $nomor . "'")->result();
	}

	function DataRacikan($kode = "")
	{
		return $this->db->query("SELECT * FROM glbm_promo  WHERE kode = '" . $kode . "'")->result();
	}

	function DataCustomer($nomor = ""){
		return $this->db->query("SELECT *FROM cari_kelompokpelanggan WHERE nomor = '".$nomor."'")->result();
	}

	function DataFreeBarang($kodebarang = "", $cabang = "", $kode){
		return $this->db->query("SELECT *FROM cari_barangfree WHERE barangfree = '".$kodebarang."' AND kodecabang = '".$cabang."' AND kodebarang = '".$kode."'")->result();
	}

	function LoadOutStandingAR($nomor = "")
	{
		return $this->db->query("SELECT
		so.nopelanggan,
		COALESCE(SUM(so.grandtotal),0) - COALESCE(SUM(p.nilaialokasi),0) - COALESCE(SUM(p.nilaipenerimaan),0) outstandingar
		FROM srvt_salesorder so
		LEFT JOIN srvt_invoice i ON i.nomorso = so.nomor
		LEFT JOIN srvt_piutang p ON p.nomor = i.nomor
		WHERE so.batal = false AND so.nopelanggan = '" . $nomor . "'
		GROUP BY so.nopelanggan ")->result();
	}

	function MaxUrutan($nomor = "")
	{
		return $this->db->query("SELECT COALESCE(MAX(revisike),0) revisike 
		FROM srvt_salesorderhistory  		
		WHERE batal = false AND nomor = '" . $nomor . "'")->result();
	}


	function CekSODetail($kodebarang = "", $nomor = "")
	{
		return $this->db->query("SELECT * FROM srvt_sodetail WHERE kodebarang = '" . $kodebarang . "' AND nomor = '" . $nomor . "'")->result();
	}

	function UpdateDataInventory($table = "", $setdata = "", $where = "")
	{
		// return $this->db->set($dataupdate);
		// $this->db->where($where);
		// $this->db->update($table);

		return $this->db->query("UPDATE " . $table . " SET " . $setdata . " WHERE " . $where);
	}

	function InsertData($table = "", $data = array())
	{
		// print_r($data);
		// die();
		return $this->db->insert($table, $data);
	}

	function DataBarang($kode = "", $jenisracikan = "", $cabang = "", $gudang = "", $sales = "")
	{
		$periode = date('Y') . date('m');
		$querydata = "";
		if ($jenisracikan == "") {
			$querydata = "SELECT * FROM cari_barangsales WHERE kodebarang = '" . $kode . "' AND periode = '" . $periode . "' AND kodecabang = '" . $cabang . "' AND kodegudang = '" . $gudang . "' AND kodesales = '" . $sales . "'";
		} else {
			$querydata = "SELECT * FROM cari_barangsalesracikan WHERE kodebarang = '" . $kode . "' AND periode = '" . $periode . "' AND kodecabang = '" . $cabang . "' AND kodegudang = '" . $gudang . "' AND kodesales = '" . $sales . "'";
		}
		return $this->db->query($querydata)->result();
	}

	function DataSO($nomor = "")
	{
		return $this->db->query("SELECT SO.*, P.nama as namaracikan FROM srvt_salesorder SO
		LEFT JOIN glbm_promo P ON P.kode = SO.jenisdisc
		WHERE SO.nomor = '" . $nomor . "'")->result();
	}


	function DataInventory($periode = "", $kodegudang = "", $kodebarang = "")
	{
		return $this->db->query("SELECT * FROM cari_inventorystock WHERE periode = '" . $periode . "' AND kodegudang = '" . $kodegudang . "' AND kodebarang = '" . $kodebarang . "'")->row();
	}

	function GetRowGrupCustomer($nomor = "")
	{
		return $this->db->query("SELECT * FROM glbm_customer WHERE nomor = '" . $nomor . "' ")->row();
	}

	function DataSODetail($nomor = "")
	{
		return $this->db->query("SELECT *FROM srvt_sodetail WHERE nomor = '" . $nomor . "'")->result();
	}

	function DataHarga($nomor = "", $kodecabang = ""){
		return $this->db->query("SELECT *FROM glbm_barangdetail2 WHERE hargajual = '".$nomor."' AND kodecabang = '".$kodecabang."'")->result();
	}

	function SaveData($data = "")
	{
		return $this->db->insert('srvt_salesorder', $data);
	}

	function SaveDataDetail($data = "")
	{
		return $this->db->insert('srvt_sodetail', $data);
	}

	function UpdateData($nomor = "", $data = "")
	{
		$this->db->where('nomor', $nomor);
		return $this->db->update('srvt_salesorder', $data);
	}

	function UpdateDataDetail($nomor = "", $data = "")
	{
		$this->db->where('nomor', $nomor);
		return $this->db->update('srvt_sodetail', $data);
	}

	function DeleteDetail($nomor = "")
	{
		$this->db->where('nomor', $nomor);
		return $this->db->delete('srvt_sodetail');
	}

	function CariJenisCustomer($nomor = "")
	{
		return $this->db->query("SELECT c.*, gc.tokosendiri,gc.diskon,gc.discountatas 
		FROM glbm_customer c 
		LEFT JOIN glbm_grupcustomer gc ON gc.kode = c.gruppelanggan WHERE c.nomor = '" . $nomor . "'")->result();
	}

	function GetRowGroupDiskon($nomor)
	{
		return $this->db->query("SELECT *FROM glbm_customer where nomor = '" . $nomor . "'")->row();
	}

	function CekDiskonPromo($kodebarang = "", $jenisracikan = "", $cabang = "")
	{
		return $this->db->query("SELECT p.kode as kodepromo,p.nama as namapromo,pd.kodebarang,pd.namabarang, p.minorder,p.diskon ,p.kodecabang 
		FROM glbm_promo p 
		LEFT JOIN glbm_promodetail pd on pd.kode = p.kode 
		WHERE p.aktif = true AND pd.kodebarang = '" . $kodebarang . "' AND p.kode = '" . $jenisracikan . "'AND p.kodecabang  = '" . $cabang . "'")->result();
	}

	// function CekDiskonRacikanPromo($kodebarang = "", $jenisracikan = "", $cabang = "", $minorder = 0)
	// {
	// 	return $this->db->query("SELECT p.kode as kodepromo,p.nama as namapromo,pd.kodebarang,pd.namabarang, p.minorder,p.diskon ,p.kodecabang 
	// 	FROM glbm_promo p 
	// 	LEFT JOIN glbm_promodetail pd on pd.kode = p.kode 
	// 	WHERE p.aktif = true AND pd.kodebarang = '" . $kodebarang . "' AND p.kode = '" . $jenisracikan . "'AND p.kodecabang  = '" . $cabang . "' AND p.minorder >= '" . $minorder . "' AND COALESCE(pd.kodebarang,'') <> ''")->result();
	// }
	function GetRowDiskonLevelGroup($noreferensi, $cabang){
		return $this->db->query("SELECT *FROM glbm_leveldiskon WHERE noreferensi = '".$noreferensi."' AND kodecabang = '".$cabang."'")->row();
	}
	function CekDiskonGroup($where){
		$this->db->where($where);
		return $this->db->get('glbm_grupcustomer')->result();
	} 
	function CekDiskonLevelGroup($kodebarang = "", $cabang = "", $qty = 0, $noreferensi = ""){
		return $this->db->query("SELECT *FROM glbm_leveldiskon WHERE aktif = true AND kodebarang = '".$kodebarang."' AND kodecabang = '".$cabang."' AND kondisi1 <= '".$qty."' AND kondisi2 >= '".$qty."' AND noreferensi = '".$noreferensi."'")->result();
	}

	function CekDiskonRacikanPromo($jenisracikan = "", $cabang = "", $minorder = 0)
	{
		return $this->db->query("SELECT *
		FROM glbm_promo
		WHERE aktif = true  AND kode = '" . $jenisracikan . "'AND kodecabang  = '" . $cabang . "' AND minorder <= '" . $minorder . "' ")->result();
	}

	function CekDiskon($nomor = "", $kodebarang = "")
	{
		return $this->db->query("SELECT dk.nomorpelanggan, dk.diskon, dkd.kodebarang FROM glbm_diskonkhusus dk
		LEFT JOIN glbm_diskonkhsusdetail dkd ON dkd.nomorpelanggan = dk.nomorpelanggan
		WHERE aktif = true AND dk.nomorpelanggan = '" . $nomor . "' AND dkd.kodebarang = '" . $kodebarang . "'")->result();
	}

	// function CekDiskonBarang($kodebarang = "")
	// {
	// 	return $this->db->query("SELECT * FROM glbm_leveldiskon		
	// 	WHERE aktif = true AND kodebarang = '" . $kodebarang . "' ")->result();
	// }

	function CekDiskonKelompoAJ($kodebarang = "")
	{
		return $this->db->query("SELECT p.kode,p.nama as namakelompok, pd.kodebarang,p.diskon 
		FROM glbm_kelompokaj p 
		LEFT JOIN glbm_kelompokajdetail pd on pd.kode = p.kode 
		WHERE p.aktif = true AND pd.kodebarang = '" . $kodebarang . "'")->result();
	}
	function CekDiskonPelangganKategori($kodebarang = "", $nopelanggan = "")
	{
		return $this->db->query("SELECT
		dpk.nopelanggan,
		dpkd.kodemerk,
		b.kode as kodebarang,
		b.nama as namabarang,
		dpk.diskon
	FROM
		glbm_discpelanggankategori dpk
		LEFT JOIN glbm_discpelanggankategoridetail dpkd ON dpkd.nopelanggan = dpk.nopelanggan
		LEFT JOIN glbm_barang b ON b.kodemerk = dpkd.kodemerk
		WHERE dpk.aktif = true  AND dpk.nopelanggan = '" . $nopelanggan . "' AND b.kode = '" . $kodebarang . "'")->result();
	}

	function CekDiskonLevel($kodebarang = "", $cabang = "", $qty = 0)
	{
		return $this->db->query("SELECT *
		FROM glbm_leveldiskon		
		WHERE aktif = true  AND  kodebarang = '" . $kodebarang . "' AND kodecabang = '" . $cabang . "' AND kondisi1 <= '" . $qty . "' AND kondisi2 >= '" . $qty . "'")->result();
	}
}
