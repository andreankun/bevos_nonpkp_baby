<?php

class Suratjalan_model extends CI_Model
{
	public function GetMaxNomor($nomor = "")
	{
		$this->db->select_max('nomor');
		$this->db->where("left(nomor,9)", $nomor);
		return $this->db->get("srvt_suratjalan")->row();
	}

	function CekSJ($nomor = "")
	{
		return $this->db->query("SELECT * FROM srvt_suratjalan WHERE nomor = '" . $nomor . "'")->result();
	}

	function DataSJ($nomor = "")
	{
		return $this->db->query("SELECT * FROM cari_suratjalan WHERE nomor = '" . $nomor . "'")->result();
	}

	function DataSJDetail($nomor = "")
	{
		return $this->db->query("SELECT * FROM srvt_suratjalandetail WHERE nomor = '" . $nomor . "'")->result();
	}

	function DataInvoice($nomor = "")
	{
		return $this->db->query("SELECT * FROM srvt_invoice WHERE nomor = '" . $nomor . "'")->result();
	}

	function DataPacking($nomor = "")
	{
		return $this->db->query("SELECT nopacking FROM srvt_invoice WHERE nomor = '" . $nomor . "'")->row();
	}

	function SaveData($data = "")
	{
		return $this->db->insert('srvt_suratjalan', $data);
	}

	function SaveDataDetail($data = "")
	{
		return $this->db->insert('srvt_suratjalandetail', $data);
	}

	function UpdateDataPacking($nomor = "", $data = "")
	{
		$this->db->where('nomor', $nomor);
		return $this->db->update('srvt_packingbarang', $data);
	}

	function UpdateData($data = "", $nomor = "")
	{
		$this->db->where('nomor', $nomor);
		return $this->db->update('srvt_suratjalan', $data);
	}

	function UpdateDataDetail($data = "", $nomor = "")
	{
		$this->db->where('nomor', $nomor);
		return $this->db->update('srvt_suratjalandetail', $data);
	}

	function DeleteDetail($nomor = "")
	{
		$this->db->where('nomor', $nomor);
		return $this->db->delete('srvt_suratjalandetail');
	}
}
