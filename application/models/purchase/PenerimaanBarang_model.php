<?php

class PenerimaanBarang_model extends CI_Model
{
    public function GetMaxNomor($nomor = "")
    {
        $this->db->select_max('nomor');
        $this->db->where("left(nomor,10)", $nomor);
        return $this->db->get("srvt_penerimaanbarang")->row();
    }

    function CekGudang($kode = "")
    {
        return $this->db->query("SELECT kodegudang FROM srvt_penerimaanbarang WHERE kodegudang = '" . $kode . "' ")->row();
    }

    function DataPenerimaanBarang($nomor = "")
    {
        return $this->db->query("SELECT * FROM srvt_penerimaanbarang WHERE nomor = '" . $nomor . "' ")->result();
    }

    function DataPenerimaanBarangDetail($nomor = "")
    {
        return $this->db->query("SELECT * FROM srvt_penerimaanbarangdetail WHERE nomor = '" . $nomor . "' ")->result();
    }

    function SaveData($data = "")
    {
        $this->db->insert('srvt_penerimaanbarang', $data);
    }

    function SaveDataHutang($data = "")
    {
        $this->db->insert('srvt_hutang', $data);
    }

    function SaveDataDetail($data = "")
    {
        $this->db->insert('srvt_penerimaanbarangdetail', $data);
    }

    function UpdateData($nomor = "", $data = "")
    {
        $this->db->where('nomor', $nomor);
        return $this->db->update('srvt_penerimaanbarang', $data);
    }

    function UpdateDataHutang($nomor = "", $data = "")
    {
        $this->db->where('nomor', $nomor);
        return $this->db->update('srvt_hutang', $data);
    }

    function UpdateDataDetail($nomor = "", $data = "")
    {
        $this->db->where('nomor', $nomor);
        return $this->db->update('srvt_penerimaanbarangdetail', $data);
    }

    function DeletePenerimaanBarang($nomor = "")
    {
        $this->db->where('nomor', $nomor);
        return $this->db->delete('srvt_penerimaanbarangdetail');
    }
    function UpdateDataInventory($table = "", $setdata = "", $where = "")
    {
        // return $this->db->set($dataupdate);
        // $this->db->where($where);
        // $this->db->update($table);

        return $this->db->query("UPDATE " . $table . " SET " . $setdata . " WHERE " . $where);
    }
}
