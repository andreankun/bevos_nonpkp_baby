<?php

class Global_model extends CI_Model
{
    function GetDataGlobal($table = "", $where = array())
    {
        $this->db->where($where);
        return $this->db->get($table)->result();
    }

    function GetDataGlobalAndNotIN($table = "", $where = array(), $wherenotin = "")
    {
        $this->db->where($where);
        $this->db->where('kodegudang !=', $wherenotin);
        return $this->db->get($table)->result();
    }

    function UpdateDataGlobal($table = "", $dataupdate = array(), $where = array())
    {
        $this->db->where($where);
        return $this->db->update($table, $dataupdate);
    }
    function DeleteDataGlobal($table = "",  $where = array())
    {
        $this->db->where($where);
        return $this->db->delete($table);
    }

    function InsertDataGlobal($table = "", $data = array())
    {
        // print_r($data);
        // die();
        return $this->db->insert($table, $data);
    }


    function InsertStockBarangPeriodeBaru($periodelama = "", $periode = "", $kodebarang = "", $kodecabang = "", $kodegudang = "")
    {
        return $this->db->query("INSERT INTO srvt_inventorystok 
		SELECT '" . $periode . "' as periode,kodecabang,kodegudang,kodebarang,(qtyawal+qtymasuk-qtykeluar) as qtyawal, 0 as qtymasuk, 0 as qtykeluar, qtypick, 0 as cogs, now() as tglsimpan,'fbs' as pemakai FROM srvt_inventorystok WHERE periode = '" . $periodelama . "' AND kodebarang = '" . $kodebarang . "' AND kodecabang = '" . $kodecabang . "' AND kodegudang = '" . $kodegudang . "'");
    }
}
