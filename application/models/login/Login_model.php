<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Login_model extends CI_Model
{
    private $_table = "stpm_login";
    private $_tableKonfig = "stpm_konfigurasi";

    public $id;
    public $username;

    public function rules()
    {
        return [
            [
                'field' => 'username',
                'label' => 'username',
                'rules' => 'required'
            ]
        ];
    }

    function Login($username = "", $password = "")
    {
        $this->db->where('username', $username);
        $this->db->where('password', $password);
        return $this->db->get($this->_table)->result();
    }

    public function getlog($username, $password)
    {
        $this->db->select('*');
        $this->db->where('username', $username);
        $this->db->where('password', $password);
        $this->db->where('aktif', true);
        return $this->db->get($this->_table)->row();
    }

    public function getkonfigurasi($kodecabang)
    {
        $this->db->select('*');
        $this->db->where('kodecabang', $kodecabang);
        return $this->db->get($this->_tableKonfig)->row();
    }

    function CekStock($periode = "")
    {
        return $this->db->query("SELECT * FROM srvt_inventorystok WHERE periode = '" . $periode . "' ")->result();
    }
}
