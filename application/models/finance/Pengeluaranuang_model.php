<?php

class Pengeluaranuang_model extends CI_Model
{
  public function GetMaxNomor($nomor = "")
  {
    $this->db->select('nomor');
    $this->db->where("left(nomor,13)", $nomor);
    return $this->db->get("trnt_pengeluaranuang")->row();
  }
  // public function GetMaxNomor($nomor = "")
  // {
  //   $this->db->select('nomor');
  //   $this->db->where("SUBSTRING(nomor,4,10)", $nomor);
  //   $this->db->order_by("SUBSTRING(nomor,4,16) DESC");
  //   $this->db->limit(1);
  //   return $this->db->get("trnt_pengeluaranuang")->row();
  // }
  function DataPengeluaranUang($nomor = "")
  {
    return $this->db->query("SELECT PU.*, OK.nama AS namapengeluaran
    FROM trnt_pengeluaranuang PU
    LEFT JOIN stpm_otorisasikasir OK ON OK.kode = PU.jenispengeluaran
    WHERE PU.nomor = '" . $nomor . "' ")->result();
  }

  public function DataPengeluaranUangDetail($nomor)
  {
    return $this->db->query("SELECT PUD.*, GA.nama AS namaaccount 
        FROM trnt_pengeluaranuang PU 
        LEFT JOIN trnt_pengeluaranuangdetail PUD ON PUD.nomorpengeluaran = PU.nomor 
        LEFT JOIN glbm_account GA ON GA.nomor = PUD.kodeaccount
    WHERE PUD.nomorpengeluaran = '" . $nomor . "' ")->result();
  }

  function DataJenisPengeluaran($kode = "")
  {
    return $this->db->query("SELECT * FROM stpm_otorisasikasir WHERE kode = '" . $kode . "' ")->result();
  }

  function DataDepartemen($kode = "")
  {
    return $this->db->query("SELECT * FROM glbm_departement WHERE kode = '" . $kode . "' ")->result();
  }

  function DataHutang($nomor = "")
  {
    return $this->db->query("SELECT H.*, S.nama AS namasupplier FROM srvt_hutang H LEFT JOIN glbm_supplier S ON S.nomor = H.nomorsupplier WHERE H.nomor = '" . $nomor . "' ")->result();
  }

  function DataAccount($nomor = "")
  {
    return $this->db->query("SELECT * FROM glbm_account WHERE nomor = '" . $nomor . "' ")->result();
  }

  function DataAccountLain($nomor = "")
  {
    return $this->db->query("SELECT * FROM glbm_accountlainlain WHERE nomor = '" . $nomor . "' ")->result();
  }
  function DataRetur($nomor = "")
  {
    return $this->db->query("SELECT * FROM srvt_returbarang WHERE nomor = '" . $nomor . "' ")->result();
  }

  function LoadDataCustomer($nomor = "")
  {
    return $this->db->query("SELECT * FROM glbm_customer WHERE nomor = '" . $nomor . "' ")->result();
  }


  function DataHutangOnly($noreferensi = "")
  {
    return $this->db->query("SELECT * FROM srvt_hutang  WHERE nomor = '" . $noreferensi . "' ")->row();
  }

  public function savePengeluaranUang($data)
  {
    $this->db->insert('trnt_pengeluaranuang', $data);
  }

  public function savePengeluaranUangDetail($data)
  {
    $this->db->insert('trnt_pengeluaranuangdetail', $data);
  }

  public function updateHutang($data = "", $nomor = "")
  {
    $this->db->where('nomor', $nomor);
    $this->db->update('srvt_hutang', $data);
  }

  public function updateReturBarang($data = "", $nomor = "")
  {
    $this->db->where('nomor', $nomor);
    $this->db->update('srvt_returbarang', $data);
  }
  public function saveHutang($data)
  {
    $this->db->insert('srvt_hutang', $data);
  }

  function CekPengeluaran($noreferensi = "")
  {
    return $this->db->query("SELECT * FROM srvt_hutang  WHERE nomor = '" . $noreferensi . "' AND nilaipembayaran <> 0 ")->row();
  }

  function CancelTransaksi($data = "", $nomor = "")
  {
    $this->db->where('nomor', $nomor);
    return $this->db->update('trnt_pengeluaranuang', $data);
  }
}
