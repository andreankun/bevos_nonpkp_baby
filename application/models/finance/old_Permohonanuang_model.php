<?php

class PermohonanUang_model extends CI_Model
{
  public function GetMaxNomor($nomor = "")
  {
    $this->db->select_max('nomor');
    $this->db->where("left(nomor,10)", $nomor);
    return $this->db->get("trnt_permohonanuang")->row();
  }
  function DataPermohonanUang($nomor = "")
  {
    return $this->db->query("SELECT PU.*, OK.nama AS namapengeluaran
    FROM trnt_permohonanuang PU
    LEFT JOIN stpm_otorisasikasir OK ON OK.kode = PU.jenispengeluaran
    WHERE PU.nomor = '" . $nomor . "' ")->result();
  }

  public function DataPermohonanUangDetail($nomor)
  {
    return $this->db->query("SELECT PUD.*, GA.nama AS namaaccount 
        FROM trnt_permohonanuang PU 
        LEFT JOIN trnt_permohonanuangdetail PUD ON PUD.nomorpermohonan = PU.nomor 
        LEFT JOIN glbm_account GA ON GA.nomor = PUD.kodeaccount AND PU.kodecabang = GA.kode_cabang 
    WHERE PUD.nomorpermohonan = '" . $nomor . "' ")->result();
  }

  function DataJenisPengeluaran($kode = "")
  {
    return $this->db->query("SELECT * FROM stpm_otorisasikasir WHERE kode = '" . $kode . "' ")->result();
  }

  function DataDepartemen($kode = "")
  {
    return $this->db->query("SELECT * FROM glbm_departement WHERE kode = '" . $kode . "' ")->result();
  }

  function DataHutang($nomor = "")
  {
    return $this->db->query("SELECT H.*, S.nama AS namasupplier FROM srvt_hutang H LEFT JOIN glbm_supplier S ON S.nomor = H.nomorsupplier WHERE H.nomor = '" . $nomor . "' ")->result();
  }

  function DataAccount($nomor = "")
  {
    return $this->db->query("SELECT * FROM glbm_account WHERE nomor = '" . $nomor . "' ")->result();
  }

  function DataAccountLain($nomor = "")
  {
    return $this->db->query("SELECT * FROM glbm_accountlainlain WHERE nomor = '" . $nomor . "' ")->result();
  }

  function DataHutangOnly($noreferensi = "")
  {
    return $this->db->query("SELECT * FROM srvt_hutang  WHERE nomor = '" . $noreferensi . "' ")->row();
  }

  public function savePermohonanUang($data)
  {
    $this->db->insert('trnt_permohonanuang', $data);
  }

  public function savePermohonanUangDetail($data)
  {
    $this->db->insert('trnt_permohonanuangdetail', $data);
  }

  public function updateHutang($data = "", $nomor = "")
  {
    $this->db->where('nomor', $nomor);
    $this->db->update('srvt_hutang', $data);
  }

  public function saveHutang($data)
  {
    $this->db->insert('srvt_hutang', $data);
  }

  function CekPengeluaran($noreferensi = "")
  {
    return $this->db->query("SELECT * FROM srvt_hutang  WHERE nomor = '" . $noreferensi . "' AND nilaipembayaran <> 0 ")->row();
  }

  function CancelTransaksi($data = "", $nomor = "")
  {
    $this->db->where('nomor', $nomor);
    return $this->db->update('trnt_permohonanuang', $data);
  }
}
