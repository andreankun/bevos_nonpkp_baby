<?php

class Penerimaanuang_model extends CI_Model
{
  public function GetMaxNomor($nomor = "")
  {
    $this->db->select('nomor');
    $this->db->where('left(nomor,13)', $nomor);
    $this->db->order_by('nomor', 'desc');
    return $this->db->get("trnt_penerimaanuang")->row();
  }
  // public function GetMaxNomor($nomor = "")
  // {
  //   $this->db->select('nomor');
  //   $this->db->where("SUBSTRING(nomor,4,10)", $nomor);
  //   $this->db->order_by("SUBSTRING(nomor,4,13) DESC");
  //   $this->db->limit(1);
  //   return $this->db->get("trnt_penerimaanuang")->row();
  // }
  function DataPenerimaanUang($nomor = "")
  {
    return $this->db->query("SELECT PU.*, OK.nama AS namapenerimaan FROM trnt_penerimaanuang PU 
    LEFT JOIN stpm_otorisasikasir OK on OK.kode = PU.jenispenerimaan
    WHERE PU.nomor = '" . $nomor . "' ")->result();
  }

  public function DataPenerimaanUangDetail($nomor)
  {
    return $this->db->query("SELECT PD.*, GA.nama AS namaaccount 
    FROM trnt_penerimaanuang P 
    LEFT JOIN trnt_penerimaanuangdetail PD ON P.nomor = PD.nomorpenerimaan 
    LEFT JOIN glbm_account GA ON GA.nomor = PD.kodeaccount 
    WHERE PD.nomorpenerimaan = '" . $nomor . "'")->result();
  }

  function DataJenisPenerimaan($kode = "")
  {
    return $this->db->query("SELECT * FROM stpm_otorisasikasir WHERE kode = '" . $kode . "' ")->result();
  }

  function DataSO($nomor = "")
  {
    return $this->db->query("SELECT SO.*, c.alamat FROM srvt_salesorder SO
    LEFT JOIN glbm_customer c ON c.nomor = SO.nopelanggan
    WHERE SO.nomor = '" . $nomor . "' ")->result();
  }

  // public function DataPiutang($nomor = "")
  // {
  //   return $this->db->query("SELECT P.*, C.nama_toko AS namapelanggan, SO.nilaiuangmuka
  //   FROM srvt_piutang P
  //   LEFT JOIN glbm_customer C ON C.nomor = P.nopelanggan
  //   LEFT JOIN srvt_salesorder SO ON SO.nopelanggan = C.nomor
  //   WHERE P.nomor = '" . $nomor . "' ")->result();
  // }
  public function DataPiutang($nomor = "")
  {
    return $this->db->query("SELECT P.*, C.nama_toko AS namapelanggan
    FROM srvt_piutang P
    LEFT JOIN glbm_customer C ON C.nomor = P.nopelanggan
    -- LEFT JOIN srvt_salesorder SO ON SO.nopelanggan = C.nomor
    WHERE P.nomor = '" . $nomor . "' ")->result();
  }

	public function DataOngkir($nomor = "")
  {
    return $this->db->query("SELECT P.*, C.nama_toko AS namapelanggan
    FROM cari_ongkir P
    LEFT JOIN glbm_customer C ON C.nomor = P.nopelanggan
    -- LEFT JOIN srvt_salesorder SO ON SO.nopelanggan = C.nomor
    WHERE P.nomor = '" . $nomor . "' ")->result();
  }

  public function DataOrderOnly($noreferensi = "")
  {
    $this->db->where('nomor', $noreferensi);
    return $this->db->get('srvt_salesorder')->row();
  }

  public function DataPiutangOnly($noreferensi = "")
  {
    $this->db->where('nomor', $noreferensi);
    return $this->db->get('srvt_piutang')->row();
  }

  function DataDepartement($kode = "")
  {
    return $this->db->query("SELECT * FROM glbm_departement WHERE kode = '" . $kode . "' ")->result();
  }

  function SisaDeposit($nomor = "")
  {
    return $this->db->query("SELECT nomorcustomer,SUM(debit-kredit) as sisadeposit 
    FROM trnt_historydepositcustomer WHERE nomorcustomer = '" . $nomor . "' GROUP BY nomorcustomer")->result();
  }

  // function DataAccount($nomor = "")
  // {
  //   return $this->db->query("SELECT * FROM glbm_account WHERE nomor = '" . $nomor . "' ")->result();
  // }

  function DataAccount($nomor = "")
  {
    return $this->db->query("SELECT * FROM glbm_account WHERE nomor = '" . $nomor . "' ")->result();
  }

  function DataAccountLain($nomor = "")
  {
    return $this->db->query("SELECT * FROM glbm_accountlainlain WHERE nomor = '" . $nomor . "' ")->result();
  }

  public function savePenerimaanUang($request)
  {
    $this->db->insert('trnt_penerimaanuang', $request);
  }

  public function savePenerimaanUangDetail($request)
  {
    $this->db->insert('trnt_penerimaanuangdetail', $request);
  }

  public function updateRO($data = "", $nomor = "")
  {
    $this->db->where('nomor', $nomor);
    return $this->db->update('srvt_salesorder', $data);
  }

  public function updatePiutang($data = "", $nomor = "")
  {
    $this->db->where('nomor', $nomor);
    $this->db->update('srvt_piutang', $data);
  }

  function CekFaktur($nomor = "")
  {
    return $this->db->query("SELECT RO.* FROM srvt_realeaseorder RO
    LEFT JOIN srvt_salesorder SO ON SO.nomorso = RO.nomorso
    WHERE RO.nomorro = '" . $nomor . "' AND SO.status = 2 AND RO.batal = false ")->result();
  }

  function CekBatalFaktur($nomor = "")
  {
    return $this->db->query("SELECT * FROM srvt_faktur
    WHERE nomorfaktur = '" . $nomor . "' AND batal = true ")->result();
  }

  public function updateBatalRO($data = "", $nomor = "")
  {
    $this->db->where('nomorro', $nomor);
    return $this->db->update('srvt_realeaseorder', $data);
  }

  public function updateBatalPiutang($data = "", $nomor = "")
  {
    $this->db->where('nomor', $nomor);
    $this->db->update('srvt_piutang', $data);
  }

  function CancelTransaksi($data = "", $nomor = "")
  {
    $this->db->where('nomor', $nomor);
    return $this->db->update('trnt_penerimaanuang', $data);
  }

  public function UpdateData($data = "", $nomor = "")
  {
    $this->db->where('nomor', $nomor);
    return $this->db->update('trnt_penerimaanuang', $data);
  }
}
